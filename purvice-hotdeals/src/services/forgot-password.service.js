import Env from '../environments/environment.js';
import axios from 'axios';

export const ForgotPasswordService = {
    forgotPassword
};

function forgotPassword(user){
return axios.post(Env.baseUrl + '/forgot',user,{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*"
      },
    })
    .then((response) => {
        return response.data;
    })
}
