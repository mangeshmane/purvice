import Env from '../environments/environment.js';
import axios from 'axios';


export const aboutUsService = {
		getVideoStatus
};


function getVideoStatus(){
	return axios.get(Env.baseUrl + '/setting/search',{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*"
      },
    })
    .then((response) => {
        return response.data;
    })
}