
import Env from '../environments/environment.js';
import axios from 'axios';
import cookie from 'react-cookies';


export const BannerService = {
    bannerSave,
    getAllBanner,
    getAllBannerWithEnable,
    bannerUpdate,
    deleteBanner
};

function bannerSave(banner){
    debugger
return axios.post(Env.baseUrl + '/banner/bannersave',banner,{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*",
        "Authorization":"Bearer "+cookie.load("token"),
      },
    })
    .then((response) => {
        return response.data;
    })
}

function getAllBanner(){
    debugger
    return axios.get(Env.baseUrl + '/banner/all',{
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
        })
        .then((response) => {
            return response.data;
        })
    }

    function getAllBannerWithEnable(){
        debugger
        return axios.get(Env.baseUrl + '/banner/fetchByEnable',{
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin":"*"
              },
            })
            .then((response) => {
                return response.data;
            })
        } 

        function bannerUpdate(banner){
            debugger
        return axios.post(Env.baseUrl + '/banner/update',banner,{
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin":"*",
                "Authorization":"Bearer "+cookie.load("token"),
              },
            })
            .then((response) => {
                return response.data;
            })
        }  

        function deleteBanner(bannerId){
            debugger
            return axios.get(Env.baseUrl + '/banner/delete?bannerId='+bannerId,{
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin":"*"
                  },
                })
                .then((response) => {
                    return response.data;
                })
            } 
 
    