import Env from '../environments/environment.js';
import axios from 'axios';
import cookie from 'react-cookies';

export const myProfileService = {
    getUser,
    updateUser,
    updatePassword
};

function getUser(userId){
    return axios.get(Env.baseUrl + '/user/'+userId,{
        headers: {
            "Content-Type": "application/json",
            "Authorization":"Bearer "+cookie.load("token"),
            "Access-Control-Allow-Origin":"*"
          },
        })
        .then((response) => {
            return response.data;
        })
    }

function updateUser(data) {
    return axios.put(Env.baseUrl + '/user/update', data ,{
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin":"*",
            "Authorization":"Bearer "+cookie.load("token")
          },
    }).then((response) => {
            return response.data;
    })
}

function updatePassword(data) {
    return axios.post(Env.baseUrl + '/user/changePassword', data ,{
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin":"*",
            "Authorization":"Bearer "+cookie.load("token")
          },
    }).then((response) => {
            return response.data;
    })
}

