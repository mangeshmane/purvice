import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
// import { renderRoutes } from 'react-router-config';
import Loadable from 'react-loadable';
import './App.scss';
import axios from 'axios';
import cookie from 'react-cookies';
const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const DefaultLayout = Loadable({
  loader: () => import('./containers/DefaultLayout'),
  loading
});

// Pages
const Login = Loadable({
  loader: () => import('./views/Pages/Login'),
  loading
});

const ForgotPassword = Loadable({
  loader: () => import('./views/Pages/ForgotPassword'),
  loading
});

const ResetPassword = Loadable({
  loader: () => import('./views/Pages/ResetPassword'),
  loading
});

const Verify = Loadable({
  loader: () => import('./views/Pages/Verify'),
  loading
});

const Register = Loadable({
  loader: () => import('./views/Pages/Register'),
  loading
});

const Page404 = Loadable({
  loader: () => import('./views/Pages/Page404'),
  loading
});

const Page500 = Loadable({
  loader: () => import('./views/Pages/Page500'),
  loading
});

class App extends Component {

  render() {
    return (
      <HashRouter>
          <Switch>
            <Route exact path="/login" name="Login Page" component={Login} />
            <Route exact path="/forgot-password" name="Forgot Password Page" component={ForgotPassword} />
            <Route exact path="/reset-password" name="Forgot Password Page" component={ResetPassword} />
            <Route exact path="/verify/:token" name="Verify Page" component={Verify} />
            <Route exact path="/register" name="Register Page" component={Register} />
            <Route exact path="/404" name="Page 404" component={Page404} />
            <Route exact path="/500" name="Page 500" component={Page500} />
            <Route path="/" name="Home" component={DefaultLayout} />
          </Switch>
      </HashRouter>
    );
  }
}
axios.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  debugger
  if(error.response !== undefined){
    if(error.response.status === 401){
      cookie.remove('username', { path: '/' })
      cookie.remove('token', { path: '/' })
      cookie.remove('user', { path: '/' })
      cookie.remove('redirectPath', { path: '/' })
      localStorage.setItem("auth_token",null)
      window.location = '#/login'
    } else if (error.response.status === 500 || error.response.status ===502){
      window.location = '#/500'
    } else if (error.response.status === 404 || error.response.status ===404){
      window.location = '#/400'
    }
  }else{
    window.location = '#/500'
  }
  
  return Promise.reject(error);
});

export default App;
