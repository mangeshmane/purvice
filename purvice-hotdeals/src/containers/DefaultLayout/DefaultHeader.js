import React, { Component } from 'react';
import { Badge, Nav, NavItem, NavLink,Popover, PopoverHeader, PopoverBody,DropdownToggle,DropdownItem,DropdownMenu } from 'reactstrap';
import PropTypes from 'prop-types';
import { HashRouter, Route, Switch,Link } from 'react-router-dom';
import cookie from 'react-cookies';
import { AppNavbarBrand, AppSidebarToggler,AppHeaderDropdown } from '@coreui/react';
import logo from '../../assets/img/brand/logo_purvice.png'
import { UncontrolledTooltip } from 'reactstrap';
//import sygnet from '../../assets/img/brand/sygnet.svg'
import { dealAlertService } from '../../services/deal-alert.service';
import './DefaultLayout.css'
import $ from "jquery";
var defaultHeaderRef;
const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      popoverOpen: false,
      loggedInUser:false,
      userName:'',
      firstInitial:'',
      searchValue :'',
      addBeautyTitle :'Add Deal Alert',
      addTravelTitle :'Add Deal Alert',
      addHomeTitle :'Add Deal Alert',
      addElectronicsTitle :'Add Deal Alert',
      addBooksTitle :'Add Deal Alert',
      addClothingTitle :'Add Deal Alert',
      isAddDealalert: false,
      toggleCSSHot : '',
      toggleCSSDeal : '',
      toggleCSSTravel : ''
    };
    this.handleSearch = this.handleSearch.bind(this)
    this.onMouseOverCSSHotDeal = this.onMouseOverCSSHotDeal.bind(this)
    this.onMouseOverCSSDealAlert = this.onMouseOverCSSDealAlert.bind(this)
    this.onMouseOverCSSTravel = this.onMouseOverCSSTravel.bind(this)
    this.onMouseOutCSS = this.onMouseOutCSS.bind(this)
    this.login = this.login.bind(this)
    this.signUp = this.signUp.bind(this)
  }
  
  componentDidMount(){
    // var user = cookie.load("_id")
	  defaultHeaderRef = this;
    var name = cookie.load("username")
    if(name != undefined){
      this.setState({loggedInUser : true})
      this.setState({userName : name})
      this.state.firstInitial = name.split(" ")[0].charAt(0)+name.split(" ")[1].charAt(0);
      this.setState({firstInitial: this.state.firstInitial});
    }
    
      $(".global-search-input-box").keypress(function(e){
    	 if (e.keyCode == 13) {
    		 if(window.location.hash !== "#/hotdeals"){
    			 window.location = '#/hotdeals';
        	 }
    	 }
	  });
	  
	  $(".global-search-button").click(function(e){
		  if(window.location.hash !== "#/hotdeals"){
			  window.location = '#/hotdeals';
		  }
	  })
	  
	$("body").mouseup(function(e){
		var subject = $(".popover"); 
		if(e.target.id != subject.attr('id') && !subject.has(e.target).length){
			defaultHeaderRef.setState({popoverOpen: false});
		}
	});
  }

  onMouseOverCSSTravel(e){
    this.state.toggleCSSTravel = 'header-menu-hover'
    this.setState({toggleCSSTravel : 'header-menu-hover'})
  }

  onMouseOverCSSHotDeal(e){
    this.state.toggleCSSHot = 'header-menu-hover'
    this.setState({toggleCSSHot : 'header-menu-hover'})
  }

  onMouseOverCSSDealAlert(e){
    this.state.toggleCSSDeal = 'header-menu-hover'
    this.setState({toggleCSSDeal : 'header-menu-hover'})
  }
  
  handleSearch(e){
    this.setState({searchValue :e.target.value})
  }
  logout() {
    cookie.remove('username', { path: '/' })
    cookie.remove('token', { path: '/' })
    cookie.remove('user', { path: '/' })
    cookie.remove('redirectPath', { path: '/' })
    localStorage.setItem("auth_token",null)
    window.location = '#/login';
  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  login = ()=>{
    window.location = '#/login';
  }
  
  signUp = ()=>{
    window.location = '#/register';
  }
  
  addDealAlert = (categoryName)=>{
    var finalAlertData = [];
    var dealAlertdata = {};
    var userData = cookie.load("user")
    if(userData != undefined){
        dealAlertService.getDealCategoriesByName(categoryName).then(data => {
          if(data.message === 'SUCCESS'){
            dealAlertdata = {
              categoryName : data.result[0].categoryName,
              alertTitle :"Any "+data.result[0].categoryName,
              categoryId : data.result[0].id,
              frequencies: "Instantly",
              rating: 3,
              notificationMethod : "Email",
              createdBy : "category",
              keyword: "category",
              userId : userData.id,
              userName : this.state.userName,
              name: ""
            }
            finalAlertData.push(dealAlertdata);
            dealAlertService.dealAlertSave(finalAlertData).then(data => {
              if(data.message === 'SUCCESS'){
              if(categoryName=='Beauty')
               this.setState({addBeautyTitle: 'Alert Added'});
              if(categoryName=='Travel')
                this.setState({addTravelTitle: 'Alert Added'});
              if(categoryName=='Home')
                this.setState({addHomeTitle: 'Alert Added'});
              if(categoryName=='Electronics')
               this.setState({addElectronicsTitle: 'Alert Added'});
              if(categoryName=='Books')
                this.setState({addBooksTitle: 'Alert Added'});
              if(categoryName=='Clothing')
                this.setState({addClothingTitle: 'Alert Added'});
            }
               this.setState({isAddDealalert: true});
            });
        }
      });
    }else{
      window.location = '#/login';
    }
  }

  dealAlert = ()=>{
    window.location = "#/deal-alert";
    this.setState({popoverOpen: false});
  }

  onMouseOutCSS(e){
    this.state.toggleCSSHot = this.state.toggleCSSDeal= this.state.toggleCSSTravel =  ''
    this.setState({toggleCSSHot : ''})
    this.setState({toggleCSSDeal : ''})
    this.setState({toggleCSSTravel : ''})
  }
  render() {
    const { children, ...attributes } = this.props;

    return (

       <div>
        
      </div> 
    );
  }

goToLoginPage(){
    window.location= "#/login";
}
goToSignUp(){
  window.location= "#/register";
}
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
