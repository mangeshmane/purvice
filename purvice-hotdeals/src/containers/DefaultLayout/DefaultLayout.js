import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import cookie from 'react-cookies';
import $ from "jquery";
import ENV from '../../environments/environment.js';
import { Row } from 'reactstrap';
import {
  AppAside,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav
} from '@coreui/react';
// sidebar nav config
import navigation from '../../_nav';
// routes config
import routes from '../../routes';
import './DefaultLayout.css'
import { hotDealService } from '../../services/hot-deal.service';

const DefaultAside = React.lazy(() => import('./DefaultAside'));
 const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

class DefaultLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedInUser: false,
      tempDealData: [],
      dealData: [],
      shouldHide: false,
      firstInitial: '',
      selectOnSuggession: '',
      pageNumber: 0,
      pageSize: 10,
      searchTextByFilter: '',
      totalSuggesionrecord: 0,
      width: 0,
      height: 0,
      isAdmin: false
    };
    this.onSuggession = this.onSuggession.bind(this);
    this.loadOnDealData = this.loadOnDealData.bind(this);
    this.onSuggessionLoadData = this.onSuggessionLoadData.bind(this);
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    //this.loadOnDealData();
  }
  debugger
  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
    var name = cookie.load("username")
    var nameData = cookie.load("user")
  if(nameData != undefined){
    if(nameData.roles[0] == "ADMIN"){
      this.state.isAdmin = true;
      this.setState({isAdmin: true})
    }
  }
    if (name != undefined) {
      this.setState({ loggedInUser: true })
      this.setState({ userName: name })
      this.state.firstInitial = name.split(" ")[0].charAt(0) + name.split(" ")[1].charAt(0);
      this.setState({ firstInitial: this.state.firstInitial });
    }
    var that = this;
    $(".global-search-input-box").keypress(function (e) {
      that.setState({ shouldHide: false });
      if (e.keyCode == 13) {
        localStorage.setItem("isKeyPress", "true");
        $("#psearch .close").click()
        $("#pSearch .close").click()
        if (window.location.hash !== "#/hotdeals") {
          window.location = '#/hotdeals';
        }
      } else {
        localStorage.setItem("isKeyPress", "false");
      }
    });

    $(".global-search-button").click(function (e) {

      if (localStorage.getItem("isKeyPress") === 'false') {
        that.setState({ shouldHide: false });
        if (window.location.hash !== "#/hotdeals") {
          window.location = '#/hotdeals';
        }
      }
    })
  }

  loadOnDealData(param) {
    if (param.pageNumber === 1)
      this.setState({ tempDealData: [] });
    hotDealService.getDealBySearchText(param).then(dealsResponse => {
      this.setState({ dealData: dealsResponse.result });
      this.setState({ pageNumber: this.state.pageNumber });
      this.setState({ totalSuggesionrecord: dealsResponse.tolalRecord });
      this.onSuggessionLoadData(this.state.searchTextByFilter, param.getText);
    })
  }
  onSuggession(event) {
    if (this.state.searchTextByFilter !== event.target.value) {
      this.state.pageNumber = 0;
      this.setState({ pageNumber: 0 });
    }
    if (event.target.value.length >= 4) {
      this.state.searchTextByFilter = event.target.value;
      let param = {};
      param.pageNumber = this.state.pageNumber ? this.state.pageNumber + 1 : 1;
      this.state.pageNumber = param.pageNumber;
      this.setState({ pageNumber: param.pageNumber });
      param.pageSize = this.state.pageSize ? this.state.pageSize : ENV.defaultPageSize;
      param.searchText = this.state.searchTextByFilter ? this.state.searchTextByFilter : '';
      param.getText = 'onSuggession';
      this.loadOnDealData(param);
    } else {
      this.setState({ tempDealData: [] });
      this.setState({ shouldHide: false });
    }
  }

  onSuggessionLoadData(value, text) {
    let searchText = value;
    if (searchText != "") {
      // let data= this.state.dealData.filter(obj => {
      //   let key = obj.category.toLowerCase();
      //   if (key.includes(searchText.toLowerCase())) {
      //     let parts = obj.category.split(new RegExp(`(${searchText})`, 'gi'));
      //     obj.path = <span> { parts.map((part, i) =>
      //         <span key={i} style={part.toLowerCase() === searchText.toLowerCase() ? { fontWeight: 'bold', 'text-decoration': 'underline' } : {} }>
      //             { part }
      //         </span>)
      //     } </span>;
      //    // obj.path = obj.path.substring(0,40);
      //     if(text === "handleScroll"){
      //       this.state.tempDealData.push(obj);
      //     }
      //     return obj;
      //   }
      // })
      if (text === "handleScroll") {
        this.state.tempDealData = this.state.dealData;
      }

      if (text === "onSuggession") {
        this.setState({ tempDealData: [] });
        this.state.tempDealData = this.state.dealData;
      }

      if (this.state.tempDealData.length > 0) {
        this.setState(this.state);
        this.setState({ shouldHide: true });
      } else {
        this.setState(this.state);
        this.setState({ shouldHide: false });
      }
    } else {
      this.state.tempDealData = [];
      this.setState(this.state);
      this.setState({ shouldHide: false });
    }

  }

  signOut(e) {
    e.preventDefault()
    this.props.history.push('/login');
  }

  hotDeal = () => {
    this.props.history.push('/hotdeals');
  }

  dealAlert = () => {
    this.props.history.push('/deal-alert');
  }

  travel = () => {
    //this.props.history.push('/hotdeals')
  }

  login = () => {
    window.location = '#/login';
  }

  signUp = () => {
    window.location = '#/register';
  }

  redirectToHotDeals = () => {
    if (window.location.hash.split("?")[0] !== "#/hotdeals") {
      window.location = '#/hotdeals';
    } else {
      this.props.history.push('');
      window.location.reload();
    }
  }

  dealAlert = () => {
    if (window.location.hash.split("?")[0] !== "#/deal-alert") {
      window.location = '#/deal-alert';
    } else {
      window.location.reload();
    }
  }

  clickOnSugesion = (data) => {
    $("#global-search-input-box").val(data.currentTarget.innerText.split("\n")[0]);
    this.setState({ shouldHide: false });
    localStorage.setItem("selectOnSuggession", "selectSugessionItem");
    localStorage.setItem("value", $("#global-search-input-box").val());
    if (window.location.hash.split("?")[0] !== "#/hotdeals") {
      window.location = '#/hotdeals';
    } else {
      window.location.reload();
    }
  }
  logout() {
    cookie.remove('username', { path: '/' })
    cookie.remove('token', { path: '/' })
    cookie.remove('user', { path: '/' })
    cookie.remove('redirectPath', { path: '/' })
    localStorage.setItem("auth_token", null)
    window.location = '#/login';
  }

  handleScroll = e => {
    let element = e.target
    if (element.scrollHeight - element.scrollTop === element.clientHeight) {
      //let record = this.state.totalSuggesionrecord
      //record = record - 10;
      let param = {};
      param.pageNumber = this.state.pageNumber + 1;
      this.setState({ pageNumber: param.pageNumber });
      param.pageSize = this.state.pageSize ? this.state.pageSize : ENV.defaultPageSize;
      param.searchText = this.state.searchTextByFilter ? this.state.searchTextByFilter : '';
      param.getText = 'handleScroll';
      this.loadOnDealData(param);
    }
  }

  render() {
    return (
      <div style={{ backgroundColor: '#fff' }} >

        <div id="p-topheader" className="fixed-top">
          <div className="navbar">
            <a className="navbar-brand" onClick={this.redirectToHotDeals.bind(this)}>
              <img src={require('../../assets/img/logo_purvice.png')} width="160" alt="Logo" />
            </a>
            <div className="p-topsearchbar d-none d-lg-block">
              <form className="form-inline">
                <input type="search" name="q" id="global-search-input-box" className="ng-pristine ng-valid ng-touched form-control p-input global-search-input-box" placeholder="Search a product from multiple Stores" aria-label="Search" />
                <button className="btn btn-warning global-search-button" type="submit"><i className="fas fa-search"></i></button>

                <ul className={this.state.shouldHide ? 'dropDownTextList' : 'dropDownTextLists'} onScroll={this.handleScroll}>
                  {this.state.tempDealData.map((item, index) => (
                    <li style={{ 'cursor': 'pointer' }}>
                      <div style={{ 'display': 'inline' }} onClick={this.clickOnSugesion.bind(this)}>
                        <span className="mt-3 forum-keyword-setting">{item.text}</span><br />
                        <span className="forum-keyword-setting p-fromlink" style={{ 'color': '#20a8d8' }}>from {item.source}</span><hr />
                      </div>
                    </li>
                  ))}
                </ul>
              </form>
            </div>
            <div className="p-action" hidden={this.state.loggedInUser}>
            { this.state.width < 923 &&
               <div class="dropdown" style={{'display':'inline-block'}} >
                <a className="mr-2 p-none-btn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span className="p-icon">
                  <i className="fas fa-search"></i>
                </span>
              </a>
                <div class="dropdown-menu header-search-wrapper" aria-labelledby="dropdownMenuButton">
                <input autoComplete="off" id="global-search-input-box" className="ng-pristine ng-valid ng-touched form-control p-input global-search-input-box" type="search" placeholder="Search a product from multiple Stores" aria-label="Search"/>
                </div>
              </div>
            }

              {/* <ul className={this.state.shouldHide ? 'dropDownTextList1' : 'dropDownTextLists1'}>
              {this.state.tempDealData.map((item, index) => (
                <li style={{ 'cursor':'pointer' }}>
                  <div style={{ 'display': 'inline' }} onClick={this.clickOnSugesion.bind(this)}>
                    <span className="mt-3 forum-keyword-setting">{item.text.substring(1, 40)}</span><br />
                    <span className="forum-keyword-setting p-fromlink" style={{ 'color': '#20a8d8' }}>from {item.source}</span><hr/>
                  </div>
                </li>
              ))}
            </ul>  */}

              <a className="mr-2" onClick={e => window.location = '#/register'}>
                <span className="p-icon">
                  <i className="fas fa-user"></i>
                </span>
                <span className="p-icontext ml-1 d-none d-lg-inline-block">Sign Up Free</span>
              </a>
              <a onClick={e => window.location = '#/login'}>
                <span className="p-icon">
                  <i className="fas fa-sign-in-alt"></i>
                </span>
                <span className="p-icontext ml-1 d-none d-lg-inline-block">Login</span>
              </a>
            </div>
            <div hidden={!this.state.loggedInUser} >
              {/* <a className="mr-2 p-none-btn" id="searchin" href="#" data-toggle="modal" data-target="#psearch">
                <span className="p-icon">
                  <i className="fas fa-search"></i>
                </span>
              </a> */}

              { this.state.width < 1024 &&
               <div class="dropdown" style={{'display':'inline-block'}} >
                <a className="mr-2 p-none-btn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span className="p-icon">
                  <i className="fas fa-search"></i>
                </span>
              </a>
                <div class="dropdown-menu header-search-wrapper align-mobile" aria-labelledby="dropdownMenuButton">
                <input autoComplete="off" id="global-search-input-box" className="ng-pristine ng-valid ng-touched form-control p-input global-search-input-box" type="search" placeholder="Search a product from multiple Stores" aria-label="Search"/>
                </div>
              </div>
            }
                {/* <ul className={this.state.shouldHide ? 'dropDownTextList1' : 'dropDownTextLists1'}>
            {this.state.tempDealData.map((item, index) => (
              <li style={{ 'cursor':'pointer' }}>
                  <div style={{ 'display': 'inline' }} onClick={this.clickOnSugesion.bind(this)}>
                    <span className="mt-3 forum-keyword-setting">{item.text.substring(1, 40)}</span><br />
                    <span className="forum-keyword-setting p-fromlink" style={{ 'color': '#20a8d8' }}>from {item.source}</span><hr/>
                  </div>
              </li>
            ))}
          </ul>  */}
             <div className="dropdown" style={{'display':'inline-block'}}>
             <a href="#" className="dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{ 'color': '#fff' }}>
                <span className="dot-circle" id="Tooltip">
                  <h5 style={{ 'margin-top': '0.6em', 'color': '#fff', 'margin-left': '-2px' }}>{this.state.firstInitial}</h5>
                </span>
              </a>
              <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a className="dropdown-item" onClick={e => window.location = '#/myprofile'}><i className="fa fa-lg fa-user"></i> My Profile</a>
                { this.state.isAdmin &&
                  <a className="dropdown-item" onClick={e => window.location = '#/banner'}><i className="fa fa-picture-o"></i> Manage Banners</a>
                }
                <a className="dropdown-item" onClick={this.dealAlert}><i className="fa fa-lg fa-bell-o"></i> My Deals Alert</a>
                <a className="dropdown-item" onClick={this.logout.bind(this)}><i className="fa fa-lg fa-sign-out"></i> Logout</a>
              </div>
             </div>
            </div>
          </div>
        </div>


        <nav className="navbar navbar-expand-lg navbar-light">
          {/* <button className="navbar-toggler pl-0 pr-0" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"> */}
          <Row>
            <a className="nav-item nav-link nav-link-mobile active" onClick={this.redirectToHotDeals.bind(this)}>Hot Deals</a>
            <a className="nav-item nav-link nav-link-mobile" onClick={this.dealAlert}>Deal Alerts</a>
            <a className="nav-item nav-link nav-link-mobile" href="https://travel.purvice.com/">Travel</a>
          </Row>
          {/* </button> */}

        </nav>
        <div>
          <AppSidebar fixed display="d-lg-none d-xl-block">
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
              <AppSidebarNav navConfig={navigation} {...this.props} />
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            {/* <Container> */}
            <Suspense fallback={this.loading()}>
              <Switch>
                {routes.map((route, idx) => {
                  return route.component ? (
                    <Route
                      key={idx}
                      path={route.path}
                      exact={route.exact}
                      name={route.name}
                      render={props => (
                        <route.component {...props} />
                      )} />
                  ) : (null);
                })}
                <Redirect from="/" to="/hotdeals" />
              </Switch>
            </Suspense>
            {/* </Container> */}
          </main>
          <AppAside fixed>
            <Suspense fallback={this.loading()}>
              <DefaultAside />
            </Suspense>
          </AppAside>
        </div>

        <Suspense fallback={this.loading()}>
          <DefaultFooter />
        </Suspense>
      </div>
    );
  }
}

export default DefaultLayout;
