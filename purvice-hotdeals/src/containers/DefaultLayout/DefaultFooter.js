import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row } from 'antd';
import {
  Button,
  Card,
  CardBody,
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
  DropdownMenu,
  DropdownItem, Container,
  Col } from 'reactstrap';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultFooter extends Component {
	
  constructor(props) {
    super(props);
    this.state = {
      isClose: true,
    };
    this.closePopup = this.closePopup.bind(this)
  }
 
  closePopup(e){
    this.setState({isClose: false})
  }
	componentWillMount() {
    var that = this;
  setInterval(function () {
    that.setState({isClose: false})
  }, 9000);
  }
 
 render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <div>
      <section className="main-footer" id="main-footer" style={{'font-size':'12px'}}>
          <div  className="container clearfix">
            <div className="float-left">	Copyright © {new Date().getFullYear()} purvice,Inc. All Rights Reserved.</div>
            <div className="float-right">
        		   {/* <a className="mr-3" onClick = {() =>window.location='#/about-us'}>About Us</a> 
    	         <a className="mr-3" onClick = {() =>window.location='#/faq'}>FAQ</a> */}
    	         <a className="mr-3" onClick = {() =>window.location='#/privacy'}>Privacy</a>
    	         <a className="mr-3" onClick = {() =>window.location='#/terms'}>Terms</a>
    	         <a onClick = {() =>window.location='#/copyright'}>Copyright</a>
            </div>
        </div> 
      </section>
      <div class="pop-container">
        {this.state.isClose &&
          <div class="popup">
               <a class="close"  onClick={this.closePopup.bind(this)}>×</a>
               <p className="mb-0">Prices shown on the Deals are subject to change.</p>
            </div>
           }
           </div>
          </div>
    );
  }
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;
