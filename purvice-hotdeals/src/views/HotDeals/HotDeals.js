import React, { Component } from 'react';
import ENV from '../../environments/environment.js';
import $ from "jquery";
import { ToastContainer } from 'react-toastify';
import HotDealNavBar from './HotDealNavBar';
import HotDealCard from './HotDealCard';
import 'react-toastify/dist/ReactToastify.css';
import { toast } from 'react-toastify';
import { Button, Card, CardBody, Col, FormGroup, Input, InputGroup, Label, Row, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { hotDealService } from '../../services/hot-deal.service';
import './HotDeals.css';
import cookie from 'react-cookies';
import { UncontrolledTooltip } from 'reactstrap';

var divStyle = {
	color: "red",
	marginTop: "0%"
};

var hotDealComponentRef;

class HotDeals extends Component {

	loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

	constructor(props) {
		super(props);
		hotDealComponentRef = this;
		hotDealComponentRef.navBarRef = React.createRef();
		hotDealComponentRef.toggle = hotDealComponentRef.toggle.bind(this);
		hotDealComponentRef.updateFavDeal = hotDealComponentRef.updateFavDeal.bind(this);
		hotDealComponentRef.deleteFromCookies = hotDealComponentRef.deleteFromCookies.bind(this);
		hotDealComponentRef.loadMoreHotDeals = hotDealComponentRef.loadMoreHotDeals.bind(this);
		hotDealComponentRef.updateDeal = hotDealComponentRef.updateDeal.bind(this);
		hotDealComponentRef.updateFavDealFeedback = hotDealComponentRef.updateFavDealFeedback.bind(this);
		hotDealComponentRef.refreshFav = hotDealComponentRef.refreshFav.bind(this);
		hotDealComponentRef.state = hotDealComponentRef.getDefaultState();
		hotDealComponentRef.toggleModel = hotDealComponentRef.toggleModel.bind(this);
		hotDealComponentRef.handleChange = hotDealComponentRef.handleChange.bind(this);
		// hotDealComponentRef.loadMoreHotDeals();
		hotDealComponentRef.getAllDealCategory();
	}

	toggleModel() {
		this.setState({
			feedbackModal: !this.state.feedbackModal,
		});
	}
	cleare = () => {
		this.setState({
			feedbackModal: !this.state.feedbackModal,
		});
		this.setState({ emailState: false });
		this.setState({ emailStateInvalid: false });
		this.setState({ spinner: false });
	}
	handleChange(e) {
		const { name, value } = e.target;
		if (name === "email") {
			if (value === "") {
				this.setState({ emailState: true });
				this.setState({ emailStateInvalid: false });
			} else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
				this.setState({ emailStateInvalid: true });
				this.setState({ emailState: false });
			} else {
				this.setState({ emailStateInvalid: false });
				this.setState({ emailState: false });
			}
		}
		if (name === "name") {
			if (value === "") {
				this.setState({ firstname: true });
			} else {
				this.setState({ firstname: false });
			}
		}
		if (name == "message") {
			if (value === "") {
				this.setState({ isMessage: true });
			} else {
				this.setState({ isMessage: false });
			}
		}
	}
	getDefaultState() {
		return {
			mListOfData: [],
			collapse: false,
			favDeals: [],
			pageNumber: 0,
			pageSize: ENV.defaultPageSize,
			hasNextPage: true,
			loadingState: false,
			globalSearchText: $(".global-search-input-box").val(),
			globalScrollDisable: false,
			comparedTime: 0,
			DealFeedbacksList: [],
			storeFilter: '',
			ratingFilterData: '',
			favLoadStateFlag: false,
			getDealFeedbacksFlag: false,
			dealCategoryId: 0,
			feedbackModal: false,
			emailStateInvalid: false,
			emailState: false,
			name: '',
			email: '',
			message: '',
			firstname: false,
			spinner: false,
			createCacheKey: '',
			loadingStateScrapper: false,
			isTimeOut: false,
			initMListOfData: [],
			scrapperListOfData: {},
		};
	}

	componentWillMount() {
		hotDealComponentRef = this;
		setInterval(function () {
			if(!hotDealComponentRef.state.isTimeOut){
				window.location.reload();
			}
		}, 600000)
	}

	componentDidMount() {
		hotDealComponentRef = this;
		hotDealComponentRef.state = hotDealComponentRef.getDefaultState();

		for(var i = 0; i < 20; i++){
			var temp = {
				name:'A'+i,
				id: i
			}
			hotDealComponentRef.state.initMListOfData.push(temp);
		}

		let url = this.props.location.search.split('?')
		if (url.length > 1) {
			var urlData = url[1].split("&");
			for (var i in urlData) {
				let data = urlData[i].split("=")
				if (data[0] === "pageNumber") {
					hotDealComponentRef.state.pageNumber = Number(data[1]) - 1
				}
				if (data[0] === "pageSize") {
					hotDealComponentRef.state.pageSize = Number(data[1])
				}
				if (data[0] === "frmSearchParam") {
					var search = data[1].replace("%20", " ");
					hotDealComponentRef.state.globalSearchText = search
					hotDealComponentRef.navBarRef.current.onLoadGLobalSearchURL(search)
				}
				if (data[0] === "source") {
					var stores = data[1].split(":")
					hotDealComponentRef.state.storeFilter = stores[1]
					hotDealComponentRef.navBarRef.current.onLoadURL(stores[1])
				}
				if (data[0] === "ratingFilterData") {
					var ratingFilters = data[1].replace("%20", " ");
					hotDealComponentRef.state.ratingFilterData = ratingFilters
					hotDealComponentRef.navBarRef.current.onLoadURL(ratingFilters)
				}
				if (data[0] === "categories") {
					var categories = data[1].split(":")
					hotDealComponentRef.state.categoryFilter = categories[1]
					hotDealComponentRef.navBarRef.current.onLoadCategoryURL(categories[1])
				}
				if (data[0] === "dealCategoryId") {
					var dealCat = data[1].split(":")
					hotDealComponentRef.state.dealCategoryId = dealCat[1]
					hotDealComponentRef.navBarRef.current.onLoadDealCategoryURL(dealCat[1])
				}
			}
		}

		hotDealComponentRef.loadMoreHotDeals();

		setTimeout(function () { hotDealComponentRef.updateStateAfterFavDealEventExec(); }, 3000);

		$(".global-search-button").click(function (e) {
			let flag = window.location.hash.search('#/hotdeals')
			if (flag !== -1) {
			  if(localStorage.getItem("isKeyPress") === 'false'){
				hotDealComponentRef.state.globalSearchText = $(".global-search-input-box").val();
				hotDealComponentRef.state.mListOfData = [];
				hotDealComponentRef.resetPageSettings();
				hotDealComponentRef.clearOtherFilterExcludingGlobal();
				hotDealComponentRef.loadMoreHotDeals();
				if (hotDealComponentRef.navBarRef.current != null) {
					hotDealComponentRef.setState({ globalSearchText: '' })
					hotDealComponentRef.navBarRef.current.onGlobalSearch()
				}
			 }
			}
		})

		$(".global-search-input-box").keypress(function (e) {
			
			hotDealComponentRef.state.globalSearchText = e.target.value;
			let flag = window.location.hash.search('#/hotdeals')
			if (flag !== -1) {
				if (e.keyCode == 13) {
					hotDealComponentRef.state.mListOfData = [];
					hotDealComponentRef.resetPageSettings();
					hotDealComponentRef.clearOtherFilterExcludingGlobal()
					hotDealComponentRef.loadMoreHotDeals();
					if (hotDealComponentRef.navBarRef.current != null) {
						hotDealComponentRef.setState({ globalSearchText: '' })
						hotDealComponentRef.navBarRef.current.onGlobalSearch()
					}
				}
			}
		});

		// if (localStorage.getItem("selectOnSuggession") === 'selectSugessionItem') {
		// 	
		// 	let flag = window.location.hash.search('#/hotdeals')
		// 	if (flag !== -1) {
		// 		hotDealComponentRef.state.globalSearchText = localStorage.getItem("value");
		// 		hotDealComponentRef.state.mListOfData = [];
		// 		hotDealComponentRef.resetPageSettings();
		// 		hotDealComponentRef.clearOtherFilterExcludingGlobal();
		// 		hotDealComponentRef.loadMoreHotDeals();
		// 		if (hotDealComponentRef.navBarRef.current != null) {
		// 			hotDealComponentRef.setState({ globalSearchText: '' })
		// 			hotDealComponentRef.navBarRef.current.onGlobalSearch()
		// 		}
		// 		localStorage.removeItem("selectOnSuggession");
		// 		localStorage.removeItem("value");
		// 	}
		// }

		$(window).scroll(function () {
			let flag = window.location.hash.search('#/hotdeals')
			if (flag !== -1 && !hotDealComponentRef.state.globalScrollDisable) {
				if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
					if (!hotDealComponentRef.state.loadingState) {
						hotDealComponentRef.loadMoreHotDeals();
					}

				}
			}
		});

		//hotDealComponentRef.getAllDealCategory();
	}

	clearOtherFilterExcludingGlobal() {
		hotDealComponentRef.state.ratingsFilter = "";
		hotDealComponentRef.state.storeFilter = [];
		hotDealComponentRef.state.ratingFilterData = [];
		hotDealComponentRef.state.categoryFilter = [];
		hotDealComponentRef.state.dealCategoryId = 0;

		if (hotDealComponentRef.navBarRef.current) {
			hotDealComponentRef.navBarRef.current.state.selectedRatings = ""
			hotDealComponentRef.navBarRef.current.state.selectedCategories = [];
			hotDealComponentRef.navBarRef.current.state.selectedStores = [];
			hotDealComponentRef.navBarRef.current.state.selectedCategoryFilter = [];
			hotDealComponentRef.navBarRef.current.state.selectedTopDeal = "";
			hotDealComponentRef.navBarRef.current.state.selectedFilter = [];
			hotDealComponentRef.navBarRef.current.state.selectedTopDealFilter = false;
		}
	}

	clearGlobalFilter() {
		hotDealComponentRef.state.globalSearchText = "";
	}

	loadMoreHotDeals() {
		try {
			if (hotDealComponentRef.state.hasNextPage) {
				let param = {};
				hotDealComponentRef.state.createCacheKey = "";
				param.pageNumber = hotDealComponentRef.state ? hotDealComponentRef.state.pageNumber + 1 : 1;
				param.pageSize = hotDealComponentRef.state ? hotDealComponentRef.state.pageSize : ENV.defaultPageSize;

				if (hotDealComponentRef.state.globalSearchText && hotDealComponentRef.state.globalSearchText != "") {
					param.frmSearchParam = hotDealComponentRef.state.globalSearchText;
					hotDealComponentRef.state.isTimeOut = true;
					hotDealComponentRef.setState({ isTimeOut: true })
				}

				if (hotDealComponentRef.state.ratingsFilter && hotDealComponentRef.state.ratingsFilter != "") {
					param.customerReviewAverage = hotDealComponentRef.state.ratingsFilter;
					hotDealComponentRef.state.isTimeOut = true;
					hotDealComponentRef.setState({ isTimeOut: true })
				}

				if (hotDealComponentRef.state.storeFilter && hotDealComponentRef.state.storeFilter != "") {
					param.source = "in:" + hotDealComponentRef.state.storeFilter;
					hotDealComponentRef.state.isTimeOut = true;
					hotDealComponentRef.setState({ isTimeOut: true })
				}

				if (hotDealComponentRef.state.ratingFilterData && hotDealComponentRef.state.ratingFilterData != "") {
					param.ratingFilterData = hotDealComponentRef.state.ratingFilterData;
					hotDealComponentRef.state.isTimeOut = true;
					hotDealComponentRef.setState({ isTimeOut: true })
				}

				if (hotDealComponentRef.state.categoryFilter && hotDealComponentRef.state.categoryFilter != "") {
					param.categoryId = "in:" + hotDealComponentRef.state.categoryFilter;
					hotDealComponentRef.state.isTimeOut = true;
					hotDealComponentRef.setState({ isTimeOut: true })
				}

				if (hotDealComponentRef.state.dealCategoryId && hotDealComponentRef.state.dealCategoryId != 0) {
					param.dealCategoryId = "eq:" + hotDealComponentRef.state.dealCategoryId;
					hotDealComponentRef.state.isTimeOut = true;
					hotDealComponentRef.setState({ isTimeOut: true })
				}
				if (hotDealComponentRef.state.globalSearchText && hotDealComponentRef.state.globalSearchText !== "" && param.pageNumber === 1) {
					hotDealComponentRef.state.loadingStateScrapper = true;
					hotDealComponentRef.setState({loadingStateScrapper: true});
				}
				hotDealComponentRef.setState(hotDealComponentRef.state);

				// show loading icon
				let stateCopy = Object.assign({}, hotDealComponentRef.state);
				stateCopy.loadingState = true;

				hotDealComponentRef.setState(stateCopy);

				this.state.globalScrollDisable = true;
				var user = cookie.load("user")
				hotDealService.getDeals(param).then(deals => {
					if(deals.data.length <= 0){
						hotDealComponentRef.state.loadingStateScrapper = false;
						hotDealComponentRef.setState({loadingStateScrapper: false});
					}

					let currentTime = new Date().getTime();
					let crownTime = deals.detailedMessage.split('=')
					if (crownTime.length > 1) {
						let comparedTime = Math.abs(currentTime - crownTime[1]);
						hotDealComponentRef.state.comparedTime = comparedTime
						if (hotDealComponentRef.navBarRef.current !== null) {
							hotDealComponentRef.navBarRef.current.setTime(comparedTime)
						} else {
							window.location.reload()
						}

					}

					if (user !== null && user !== "" && user !== undefined) {
						this.getDealFeedback(stateCopy, deals, user)
					} else {
						this.manageDealCards(stateCopy, deals)
					}
					hotDealComponentRef.state.isTimeOut = false;
					hotDealComponentRef.setState({ isTimeOut: false })
				});


				if (hotDealComponentRef.state.globalSearchText && hotDealComponentRef.state.globalSearchText !== "" && param.pageNumber === 1) {
					//hotDealComponentRef.state.loadingStateScrapper = true;
					hotDealService.getScraperDeals(param).then(scrapDeals => {

						hotDealComponentRef.state.scrapperListOfData = scrapDeals;
						hotDealComponentRef.setState({scrapperListOfData: scrapDeals});

						hotDealComponentRef.state.mListOfData.forEach(element => {
							hotDealComponentRef.state.scrapperListOfData.data.push(element);
						});
						localStorage.setItem("localScrapperListOfData", JSON.stringify(hotDealComponentRef.state.scrapperListOfData));
						if (user !== null && user !== "" && user !== undefined) {
							this.getDealFeedback(stateCopy, scrapDeals, user)
						} else {
							this.manageDealCards(stateCopy, scrapDeals)
						}
						hotDealComponentRef.state.loadingStateScrapper = false;
						hotDealComponentRef.setState({ loadingStateScrapper: false })
						hotDealComponentRef.state.isTimeOut = false;
						hotDealComponentRef.setState({ isTimeOut: false })
					});
				}

			}
		} catch (error) {
			console.log(error)
		}

	}

	getDealFeedback(stateCopy, deals, user) {
		
		if (!stateCopy.getDealFeedbacksFlag) {
			hotDealService.getDealFeedbacks(user.id).then(DealFeedbacks => {
				stateCopy.pageNumber = deals.pageNumber;
				stateCopy.pageSize = deals.pageSize;
				stateCopy.hasNextPage = deals.hasNextPage;
				stateCopy.loadingState = false;
				let feedbackList = hotDealComponentRef.state.DealFeedbacksList;
				if (feedbackList.length === 0) {
					hotDealComponentRef.state.DealFeedbacksList = DealFeedbacks.result
					hotDealComponentRef.setState({ feedbackList: DealFeedbacks.result })
					feedbackList = hotDealComponentRef.state.DealFeedbacksList;
				}
				if (feedbackList.length > 0) {
					for (var j = 0; j < deals.data.length; j++) {
						for (var k = 0; k < feedbackList.length; k++) {
							if (feedbackList[k].deal.id === deals.data[j].id) {
								deals.data[j].feedback = feedbackList[k]
							}
						}
					}
				}  
				stateCopy.getDealFeedbacksFlag = true
				let myFavDealsList = [];
				if ((localStorage.getItem("myFavDealsList") !== undefined && localStorage.getItem("myFavDealsList") !== "")) {
					myFavDealsList = JSON.parse(localStorage.getItem("myFavDealsList"));
					let ids = []
					for (var i in myFavDealsList) {
						if (myFavDealsList[i].id !== undefined) {
							ids.push(myFavDealsList[i].id)
						}
					}
					
					if (ids.length >= 1 && !stateCopy.favLoadStateFlag) {
						hotDealService.getDealsByIds(ids).then(FavResponse => {
							for (var i in myFavDealsList) {
								for (var j in FavResponse.result) {
									if (myFavDealsList[i].id === FavResponse.result[j].id) {
										myFavDealsList[i] = FavResponse.result[j]
										for (var m in feedbackList) {
											if (feedbackList[m].deal.id === myFavDealsList[i].id) {
												myFavDealsList[i].feedback = feedbackList[m]
											}
										}
									}
								}
							}
							stateCopy.favDeals = myFavDealsList;
							stateCopy.favLoadStateFlag = true
							this.assignDealsToState(deals, myFavDealsList, stateCopy)
						})
					} else {
						this.assignDealsToState(deals, myFavDealsList, stateCopy)
					}
				} else {
					this.assignDealsToState(deals, myFavDealsList, stateCopy)
				}
			});
		} else {
			this.manageDealCards(stateCopy, deals)
		}

	}

	manageDealCards(stateCopy, deals) {
		
		stateCopy.pageNumber = deals.pageNumber;
		stateCopy.pageSize = deals.pageSize;
		stateCopy.hasNextPage = deals.hasNextPage;
		stateCopy.loadingState = false;
		let myFavDealsList = []; 
		if ((localStorage.getItem("myFavDealsList") !== undefined && localStorage.getItem("myFavDealsList") !== "")) {
			myFavDealsList = JSON.parse(localStorage.getItem("myFavDealsList"));
			let ids = []
			for (var i in myFavDealsList) {
				if (myFavDealsList[i].id !== undefined) {
					ids.push(myFavDealsList[i].id)
				}
			}
			if (ids.length >= 1 && !stateCopy.favLoadStateFlag) {
				hotDealService.getDealsByIds(ids).then(FavResponse => {
					for (var i in myFavDealsList) {
						for (var j in FavResponse.result) {
							if (myFavDealsList[i].id === FavResponse.result[j].id) {
								myFavDealsList[i] = FavResponse.result[j]
							}
						}
					}
					//this.props.refreshFav(myFavDealsList)
					stateCopy.favDeals = myFavDealsList;
					stateCopy.favLoadStateFlag = true
					this.assignDealsToState(deals, myFavDealsList, stateCopy)
				})
			} else {
				this.assignDealsToState(deals, myFavDealsList, stateCopy)
			}
		} else {
			this.assignDealsToState(deals, myFavDealsList, stateCopy)
		}
	}

	assignDealsToState(deals, myFavDealsList, stateCopy) {
		
		if (deals.data != null) {
			for (var i = 0; i < deals.data.length; i++) {
				let isActive = false;
				if (myFavDealsList != null) {
					for (var j = 0; j < myFavDealsList.length; j++) {
						//	if(myFavDealsList[j].id ===null){
						if (deals.data[i].uniqueId === myFavDealsList[j].uniqueId && deals.data[i].text === myFavDealsList[j].text) {
							isActive = true;
							myFavDealsList[j] = deals.data[i]
							break;
						}
						//	}
					}
				}
				deals.data[i].active = isActive;
				stateCopy.mListOfData.push(deals.data[i]);
				stateCopy.favDeals = myFavDealsList;   
				localStorage.setItem("myFavDealsList", JSON.stringify(myFavDealsList));
			}
		}
		this.setState({ state: stateCopy });
		hotDealComponentRef.setState(stateCopy);
		this.state.globalScrollDisable = false;
	}

	toggle() {
		hotDealService.getDealsByIds(JSON.parse(hotDealComponentRef.getCookie("myFavDealsList"))).then(dealsResponse => {
			let stateCopy = Object.assign({}, hotDealComponentRef.state);
			stateCopy.favDeals = dealsResponse.result;
			stateCopy = !stateCopy.collapse;
			hotDealComponentRef.setState(stateCopy);
		})
	}

	updateDeal(deal, types) {
		deal.feedback = {}
		if (types === 'disLike') {
			deal.feedback.disLikes = true
			deal.feedback.likes = false
		} else {
			deal.feedback.likes = true
			deal.feedback.disLikes = false
		}
		let deals = hotDealComponentRef.state.mListOfData
		let stateCopy = Object.assign({}, hotDealComponentRef.state);
		for (var i in deals) {
			if (deals[i].uniqueId === deal.uniqueId) {
				stateCopy.mListOfData[i] = deal
				break
			}
		}
		hotDealComponentRef.setState(stateCopy);
		if (deal.active && !deal.favFlag) {
			this.updateFavDealFeedback(deal, types)
		}
	}

	updateFavDealFeedback(deal, types) {
		let favDeals = hotDealComponentRef.state.favDeals
		deal.feedback = {}
		deal.favFlag = true
		if (types === 'disLike') {
			deal.feedback.disLikes = true
			deal.feedback.likes = false
		} else {
			deal.feedback.likes = true
			deal.feedback.disLikes = false
		}
		let stateCopy = Object.assign({}, hotDealComponentRef.state);
		for (var i in favDeals) {
			if (favDeals[i].uniqueId === deal.uniqueId) {
				stateCopy.favDeals[i] = deal
				break
			}
		}
		hotDealComponentRef.setState(stateCopy);
		this.updateDeal(deal, types)
	}
	onClickFeedback = () => {
		this.setState({
			feedbackModal: true
		});
		this.setState({
			spinner: false
		});

	}
	refreshFav(favList) {
	}
	submitFeedback(data) {
		var feedbackData = {
			name: $("#fname").val(),
			email: $("#femail").val(),
			comment: $("#fmessage").val(),
		}
		if (feedbackData.name === "") {
			this.setState({ firstname: true });
		} else if (feedbackData.email === "") {
			this.setState({ emailState: true });
		} else if (feedbackData.comment === "") {
			this.setState({ isMessage: true });
		} else {
			this.setState({ spinner: true });
			hotDealService.sendDealFeedback(feedbackData).then(feedback => {
				toast.success(feedback.detailedMessage, { position: toast.POSITION.TOP_RIGHT });
				this.setState({ feedbackModal: false });
				this.setState({ spinner: false });
			});
		}
	}
	render() {
		return (<React.Fragment>
			<div className="grid" ref="iScroll" >
				<ToastContainer autoClose={2500} />
				<div style={{ 'background-color': '#fff' }}>
					<HotDealNavBar ref={hotDealComponentRef.navBarRef} getCookie={hotDealComponentRef.getCookie} deleteFromCookies={hotDealComponentRef.deleteFromCookies} favDeals={hotDealComponentRef.state.favDeals} onRatingChange={this.onRatingChange} onStoreChange={this.onStoreChange} onStoreScrapperFilterChange={this.onStoreScrapperFilterChange} onDealCategoryChange={this.onDealCategoryChange} onDealRatingChange={this.onDealRatingChange} onCategoryChange={this.onCategoryChange} hotDealComponentRef={this} updateFavDealFeedback={hotDealComponentRef.updateFavDealFeedback} refreshFav={hotDealComponentRef.refreshFav} scrapperListOfData={hotDealComponentRef.state.scrapperListOfData}></HotDealNavBar>
				</div>
				<div class="icon-bar" onClick={this.onClickFeedback} id="Tooltip">
					<a class="facebook" style={{ 'cursor': 'pointer' }} ><i class="fas fa-comments" style={{ 'margin-right': '-25px' }}></i><span className="deal-feedback-text mb-4 mt-2">Feedback</span></a>

					<UncontrolledTooltip placement="top" target="Tooltip" style={{ backgroundColor: 'black', color: 'white', opacity: '1' }}>
						<a>Feedback</a>
					</UncontrolledTooltip>

				</div>
				<div className="p-hotdealcards">
					<div className="container">
						<div className="row">
							{
								hotDealComponentRef.state.mListOfData.map((item, index) => (
									<Col sm="12" md="3" lg="2" className="p-cardwidth">
										<HotDealCard keys={index} deal={item} onFavIconClick={hotDealComponentRef.updateFavDeal} updateDeal={hotDealComponentRef.updateDeal} />
									</Col>
								))
							}
							{!hotDealComponentRef.state.loadingState && hotDealComponentRef.state.mListOfData.length == 0 && !this.state.loadingStateScrapper
								?
								<Col className="mt-2 mb-4">
									<div className="fav-result">Sorry, No deals found !</div>
								</Col>
								: null
							}
							{false
								?
								<Col className="mt-2 mb-4">
									<div className="fav-result">Sorry, No deals found 2!</div>
								</Col>
								: null
							}
							{this.state.loadingState && !this.state.loadingStateScrapper ?
								hotDealComponentRef.state.initMListOfData.map((item, index) => (
									<Col sm="12" md="3" lg="2" className="p-cardwidth">
										<section id="p-hotdealcardsproducts">
											<div className="card hot-deal-card">
												<div className="hot-deal-images-loader">
													<div className="loaderDiv"></div>
												</div>
												<div className="card-body hot-deal-card-body">
													<div className="txtLoader"></div>
													<div className="animated-background">
														<div className="background-masker content-top"></div>
														<div className="background-masker content-first-end"></div>
														<div className="background-masker content-second-line"></div>
														<div className="background-masker content-second-end"></div>
														<div className="background-masker content-third-line"></div>
														<div className="background-masker content-third-end"></div>
													</div>
												</div>
											</div>
										</section>
									</Col>
								))  : null
							}
							{this.state.loadingStateScrapper ? 
								hotDealComponentRef.state.initMListOfData.map((item, index) => (
									<Col sm="12" md="3" lg="2" className="p-cardwidth">
										<section id="p-hotdealcardsproducts">
											<div className="card hot-deal-card">
												<div className="hot-deal-images-loader">
													<div className="loaderDiv"></div>
												</div>
												<div className="card-body hot-deal-card-body">
													<div className="txtLoader"></div>
													<div className="animated-background">
														<div className="background-masker content-top"></div>
														<div className="background-masker content-first-end"></div>
														<div className="background-masker content-second-line"></div>
														<div className="background-masker content-second-end"></div>
														<div className="background-masker content-third-line"></div>
														<div className="background-masker content-third-end"></div>
													</div>
												</div>
											</div>
										</section>
									</Col>
								)) : null
							}
						</div>
					</div>
				</div>

				{/* feedback deal alert Modal  */}
				<Modal isOpen={this.state.feedbackModal} toggle={this.toggleModel} className={this.props.className}>
					<ModalHeader toggle={this.toggleModel}>Leave Feedback Form</ModalHeader>
					<ModalBody>
						<Row>
							<Col>
								<Card style={{ 'border': 'none' }}>
									<CardBody>

										<Row>
											<Col>
												<FormGroup>
													<Label htmlFor="name">Name</Label>
													<Input id="fname" name="name" placeholder="Enter your name" onChange={hotDealComponentRef.handleChange} />
												</FormGroup>
												{this.state.firstname &&
													<InputGroup className="mb-3">
														<div style={divStyle}>
															Please enter name.
                          </div>
													</InputGroup>
												}
											</Col>
										</Row>
										<Row>
											<Col>
												<FormGroup>
													<Label htmlFor="email">Email</Label>
													<Input placeholder="Email Address" id="femail" name="email" autoComplete="email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" onChange={hotDealComponentRef.handleChange} />
												</FormGroup>
												{this.state.emailStateInvalid &&
													<FormGroup className="mb-3">
														<div style={divStyle}>
															Please enter valid email address.
                          </div>
													</FormGroup>
												}

												{this.state.emailState &&
													<FormGroup className="mb-3">
														<div style={divStyle}>
															Please enter email address.
                          </div>
													</FormGroup>
												}
											</Col>
										</Row>
										<Row>
											<Col>
												<FormGroup>
													<Label htmlFor="message">Message</Label>
													<Input type="textarea" name="message" id="fmessage" rows="3"
														placeholder="Leave your feedback message here..." onChange={hotDealComponentRef.handleChange} />
												</FormGroup>
												{this.state.isMessage &&
													<InputGroup className="mb-3">
														<div style={divStyle}>
															Please enter message.
                          </div>
													</InputGroup>
												}
											</Col>
										</Row>

									</CardBody>
								</Card>
							</Col>
						</Row>
					</ModalBody>
					<ModalFooter>
						<Button color="primary" onClick={(e) => hotDealComponentRef.submitFeedback(hotDealComponentRef.state.feedbackData)}><i className="fa fa-spinner fa-spin  float-right mt-1 ml-2" hidden={!this.state.spinner}></i>Submit</Button>
						<Button color="secondary" onClick={hotDealComponentRef.cleare}>Cancel</Button>
					</ModalFooter>
				</Modal>
			</div>
		</React.Fragment>);
	}

	updateFavDeal(deal) {
		let myFavDealsList = [];
		let isDealExist = true;
		if (deal.isFav) {
			//add to cookie
			if (localStorage.getItem("myFavDealsList")) {
				myFavDealsList = JSON.parse(localStorage.getItem("myFavDealsList"));
				if (myFavDealsList === null) {
					myFavDealsList = [];
				}
			}
			for (var i in myFavDealsList) {
				if (myFavDealsList[i].uniqueId === deal.uniqueId) {
					isDealExist = false;
				}
			}
			if (isDealExist) {
				myFavDealsList.push(deal);
				localStorage.setItem("myFavDealsList", JSON.stringify(myFavDealsList));
			} else {
				toast.warning("Deal Already added to My Favrouites.", { position: toast.POSITION.TOP_RIGHT });
			}
		} else {
			//remove from cookie
			if (localStorage.getItem("myFavDealsList")) {
				myFavDealsList = JSON.parse(localStorage.getItem("myFavDealsList"));
				let dealList = []
				for (var i in myFavDealsList) {
					if (myFavDealsList[i].uniqueId === deal.uniqueId && myFavDealsList[i].text === deal.text) {
					} else {
						dealList.push(myFavDealsList[i])
					}
				}
				myFavDealsList = dealList
				localStorage.setItem("myFavDealsList", JSON.stringify(myFavDealsList));
			}
		}
		hotDealComponentRef.state.favDeals = JSON.parse(localStorage.getItem("myFavDealsList"));

		//this will update the view
		hotDealComponentRef.updateStateAfterFavDealEventExec();
	}

	getCookie(cname) {
		let name = cname + "=";
		let decodedCookie = decodeURIComponent(document.cookie);
		let ca = decodedCookie.split(';');
		for (let i = 0; i < ca.length; i++) {
			let c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}

	setCookie(cname, cvalue, exdays) {
		let d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		let expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

	deleteFromCookies(cname, uniqueId, text) {
		let myFavDealsList = JSON.parse(localStorage.getItem("myFavDealsList"));

		//remove from hot deal page
		let stateCopy = Object.assign({}, hotDealComponentRef.state);
		for (var i = 0; i < stateCopy.mListOfData.length; i++) {
			if (uniqueId === stateCopy.mListOfData[i].uniqueId && text === stateCopy.mListOfData[i].text) {
				stateCopy.mListOfData[i].active = false;
				break;
			}
		}

		hotDealComponentRef.setState(stateCopy);

		//remove deal from cookie
		let dealList = []
		for (var i in myFavDealsList) {
			if (myFavDealsList[i].uniqueId === uniqueId && myFavDealsList[i].text === text) {
			} else {
				dealList.push(myFavDealsList[i])
			}
		}
		myFavDealsList = dealList
		localStorage.setItem("myFavDealsList", JSON.stringify(myFavDealsList));
		//this will update the view
		hotDealComponentRef.updateStateAfterFavDealEventExec();
	}

	updateStateAfterFavDealEventExec() {
		let favDealString = localStorage.getItem("myFavDealsList");
		if (favDealString && favDealString.length > 0) {
			let stateCopy = Object.assign({}, hotDealComponentRef.state);
			stateCopy.favDeals = JSON.parse(favDealString);
			hotDealComponentRef.setState(stateCopy);
		}
	}

	onRatingChange(event) {
		hotDealComponentRef = this.hotDealComponentRef;
		hotDealComponentRef.resetPageSettings();
		hotDealComponentRef.clearGlobalFilter();
		hotDealComponentRef.state.mListOfData = [];
		hotDealComponentRef.state.ratingsFilter = event.target.value;
		hotDealComponentRef.loadMoreHotDeals();
	}

	onDealRatingChange(event) {
		hotDealComponentRef = this.hotDealComponentRef;
		localStorage.removeItem("localScrapperListOfData");
		hotDealComponentRef.resetPageSettings();
		hotDealComponentRef.clearGlobalFilter();
		hotDealComponentRef.state.mListOfData = [];
		hotDealComponentRef.state.ratingFilterData = event;
		hotDealComponentRef.loadMoreHotDeals();
	}

	onStoreChange(event) {
		hotDealComponentRef = this.hotDealComponentRef;
		localStorage.removeItem("localScrapperListOfData");
		hotDealComponentRef.resetPageSettings();
		hotDealComponentRef.clearGlobalFilter();
		hotDealComponentRef.state.mListOfData = [];
		hotDealComponentRef.state.storeFilter = event;
		hotDealComponentRef.loadMoreHotDeals();
	}
	onStoreScrapperFilterChange(event){
		hotDealComponentRef = this.hotDealComponentRef;
		var eventData = [];
		var storeFilterData = []
		for(var evtIndex = 0; evtIndex < event.length; evtIndex++){
			var str = event[evtIndex].split(",");
			for(var strIndex = 0; strIndex < str.length; strIndex++){
				eventData.push(str[strIndex]);
			}
			storeFilterData.push(event[evtIndex]);
		}
		hotDealComponentRef.state.storeFilter = storeFilterData;
		var user = cookie.load("user");
		var dealData = JSON.parse(localStorage.getItem("localScrapperListOfData"));
		var dealDisplay = [];
		var dealDisplayList = {};
		for (var i=0; i < dealData.data.length; i++) {
		  for(var j=0; j < eventData.length; j++){
			if (dealData.data[i].source === eventData[j]) {
				dealDisplay.push(dealData.data[i]);
			}
		  }
		}
		if(event.length > 0){
			dealDisplayList["data"] = dealDisplay;
			dealDisplayList["hasNextPage"] = dealData.hasNextPage;
			dealDisplayList["pageNumber"] = dealData.pageNumber;
			dealDisplayList["pageSize"] = dealData.pageSize;
		}
		else{
			dealDisplayList = dealData;
		}

		hotDealComponentRef.state.mListOfData = [];
		var stateCopy = Object.assign({}, hotDealComponentRef.state);
		if (user !== null && user !== "" && user !== undefined) {
			hotDealComponentRef.getDealFeedback(stateCopy, dealDisplayList, user)
		} else {
			hotDealComponentRef.manageDealCards(stateCopy, dealDisplayList)
		}

	}

	onDealCategoryChange(event) {
		hotDealComponentRef = this.hotDealComponentRef;
		localStorage.removeItem("localScrapperListOfData");
		hotDealComponentRef.resetPageSettings();
		hotDealComponentRef.clearGlobalFilter();
		hotDealComponentRef.state.mListOfData = [];
		hotDealComponentRef.state.dealCategoryId = event;
		hotDealComponentRef.loadMoreHotDeals();
	}

	onCategoryChange(event) {
		hotDealComponentRef = this.hotDealComponentRef;
		localStorage.removeItem("localScrapperListOfData");
		hotDealComponentRef.resetPageSettings();
		hotDealComponentRef.clearGlobalFilter();
		hotDealComponentRef.state.mListOfData = [];
		hotDealComponentRef.state.categoryFilter = event;
		hotDealComponentRef.loadMoreHotDeals();
	}

	resetPageSettings() {
		hotDealComponentRef.state.pageNumber = 0;
		hotDealComponentRef.state.pageSize = ENV.defaultPageSize;
		hotDealComponentRef.state.hasNextPage = true;
	}

	getFavList() {
		return hotDealComponentRef.state.favDeals;
	}

	getAllDealCategory() {
		hotDealService.getAllDealCategorys().then((response) => {
			this.state.allCategories = response.result;
			this.state = this.state;
		})
	}

}

export default HotDeals;
