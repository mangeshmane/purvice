import React, { Component } from 'react';
import { UncontrolledTooltip, Col, Row } from 'reactstrap';
import ReactStars from 'react-stars'
import cookie from 'react-cookies';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { toast } from 'react-toastify';
import {
	FacebookIcon,
	GooglePlusIcon,
	EmailIcon,
	FacebookShareButton,
	GooglePlusShareButton,
	EmailShareButton,
  } from 'react-share';
  import { hotDealService } from '../../services/hot-deal.service';
class HotDealCard extends React.Component {
	constructor(props) {
	      super(props);
	      this.toggle = this.toggle.bind(this);
	      this.state = {
				  active: this.props.deal.active,
				  tooltipOpen: false,
				  hoverFlag :false,
				  updateFlag: true
	      };
		  this.remove = this.remove.bind(this)
		  this.saveDealFeedback = this.saveDealFeedback.bind(this)
		  this.toggleViewTrue = this.toggleViewTrue.bind(this)
		  this.toggleViewFalse = this.toggleViewFalse.bind(this)
		  this.onUrlCopy = this.onUrlCopy.bind(this)
	}
	
	toggleTooltip() {
		this.setState({
		  tooltipOpen: !this.state.tooltipOpen,
		});
	  }
	  saveDealFeedback(types, e){
		  var updateFlag = true
		  var user = cookie.load("user")
		  if(user === null || user === "" || user === undefined){
			  window.location = "#/login"
		  }else{
			  let updateFlag = this.state.updateFlag
			  if(updateFlag){
				this.state.updateFlag = false
				this.setState({updateFlag :false})
				var query = "feedbackType="+types+"&userId="+user.id + "&updatefeedbackFlag=" +false;
				let deal = this.props.deal
				if(deal.feedback !==undefined){
					if(types === 'disLike' && deal.feedback.disLikes){
					  updateFlag = false;
					}
					if(types === 'like' && deal.feedback.likes){
					  updateFlag = false;
					}
					if(types=== 'like' && deal.feedback.disLikes){
					  query = "feedbackType="+types+"&userId="+user.id + "&updatefeedbackFlag=" +true;
					}
					if(types=== 'disLike' && deal.feedback.likes){
					  query = "feedbackType="+types+"&userId="+user.id + "&updatefeedbackFlag=" +true;
					}
				}
				if(updateFlag){
				  hotDealService.saveDealFeedback(query, this.props.deal).then(dealsResponse => {
					this.state.updateFlag = true
					this.setState({updateFlag :true})
					if(this.props.deal.active){
						dealsResponse.result.active = this.props.deal.active 
					}
					let myFavDealsList = [];
					if((localStorage.getItem("myFavDealsList") !== undefined && localStorage.getItem("myFavDealsList") !== "")){
						myFavDealsList = JSON.parse(localStorage.getItem("myFavDealsList"));
						for(var i in myFavDealsList){
							//for(var j in FavResponse.result){
								if(myFavDealsList[i].uniqueId === dealsResponse.result.uniqueId){
									myFavDealsList[i] = dealsResponse.result;
									localStorage.setItem("myFavDealsList", JSON.stringify(myFavDealsList));
								}
							//}
						}
					}
					  this.props.updateDeal(dealsResponse.result,types)
				  })
				}
			  }
		  }	  
	  }
	  toggleViewTrue(e){
		  this.state.hoverFlag = true;
		  this.setState({hoverFlag : true})
	  }

	  toggleViewFalse(e){
		this.state.hoverFlag = false;
		this.setState({hoverFlag : false})
	}
	onUrlCopy (e){
		toast.success("URL copied to clipboard.", { position: toast.POSITION.TOP_RIGHT });
	}
	render() {
      return (
				<section id="p-hotdealcardsproducts">
    		  <div className="card hot-deal-card">			  
    		  	{this.props.deal.discountPercent > 0 &&
    		  		<div className="onSaleTriangle"><span className="salePrcntTxt">{ this.props.deal.discountPercent + '%' } Off</span></div>
				  }
				 
				<div>
					<ul className="p-socialmedia">
						<li className="social-media">
							<FacebookShareButton url= {this.props.deal.url} style={{cursor: 'pointer'}}>
							<FacebookIcon size={30} round={true} ></FacebookIcon>
							</FacebookShareButton>
						</li>
						{/* <li className="social-media">
							<GooglePlusShareButton url= {this.props.deal.url} style={{cursor: 'pointer '}}>
							<GooglePlusIcon size={30} round={true} ></GooglePlusIcon>
							</GooglePlusShareButton>
						</li> */}
						<li className="social-media">
							<EmailShareButton url= {this.props.deal.url} style={{cursor: 'pointer'}}>
							<EmailIcon size={30} round={true}></EmailIcon>
							</EmailShareButton>
						</li>
						<li className="social-media">
							<div class="circle">
								<CopyToClipboard text={this.props.deal.url} onCopy={this.onUrlCopy.bind(this)}>
									<i class="fa fa-link" aria-hidden="true"></i>
								</CopyToClipboard>
							</div>
						</li>
						
					</ul>
				</div>
		        <div className="hot-deal-images">
	        		<a href={(this.props.deal != null && this.props.deal.affliateLink != null) ? this.props.deal.affliateLink : this.props.deal.url} target="#" data-placement="top">
	        			<img className="imagesCss" src={this.props.deal.imageUrl} onError={(e) => {e.target.src=require("../../assets/img/default.png")}} alt="Card"/>	
	        		</a>
	        	</div>
		        <div className="card-body hot-deal-card-body">
		        <div>
		        	<div className="overlapcss">
						<div>
							<div className={this.props.deal.sourceFavIco != null ? 'brandDetails' : ''}>
								{this.props.deal.sourceFavIco != null &&
									<img className="brandImg img-responsive" src={require('../../assets/'+ this.props.deal.sourceFavIco )} alt={this.props.deal.source} /> 
								}
								{this.props.deal.sourceFavIco != null &&
									<span className="hot-deal-card-offer-type">{this.props.deal.offerType}</span>
								}
							</div>
						</div>
		        		
						{this.props.deal.sourceFavIco == null &&
						<div>
							<span className="brandText">{this.props.deal.source}</span>
						</div>
						}
		        	</div>
					{/* stars and thumbs */}
					<div className="clearfix">
						<div className="float-left">
							<ReactStars  count={5} size={18} color2={'#ffd700'} value = {this.props.deal.customerReviewAverage != null ? parseFloat(this.props.deal.customerReviewAverage) : 0} edit = {false} />
						</div>
						<div className="float-right">
						{!this.state.hoverFlag &&
							<div>
								{
									this.props.deal.feedback !==undefined ?
									<div>
										{
											(this.props.deal.feedback.likes) && 
											<div className="p-thumbs">
												<i class="far fa-thumbs-up" aria-hidden="true" style={{color: '#2dc937','cursor':'pointer'}} ></i>&nbsp;
												{this.props.deal.likes}
												<i class="far fa-thumbs-down pl-3" style={{color: '#FF0000','cursor':'pointer'}}  aria-hidden="true" onClick={this.saveDealFeedback.bind(this,'disLike')}></i>&nbsp;
												{this.props.deal.dislikes}
											</div>
										}
										{
											this.props.deal.feedback.disLikes &&
											<div className="p-thumbs">
												<i class="far fa-thumbs-up" aria-hidden="true" style={{color: '#2dc937','cursor':'pointer'}} onClick={this.saveDealFeedback.bind(this,'like')}></i>&nbsp;
												{this.props.deal.likes}
												<i class="far fa-thumbs-down pl-3" aria-hidden="true" style={{color: '#FF0000','cursor':'pointer'}}></i>&nbsp;
												{this.props.deal.dislikes}
											</div> 
										}
									</div> :
									<div>
									{
										this.props.deal.likes !== undefined &&
										<div className="p-thumbs">
											<i class="far fa-thumbs-up" aria-hidden="true" style={{color: '#2dc937','cursor':'pointer'}}  onClick={this.saveDealFeedback.bind(this,'like')}></i>&nbsp;
											{this.props.deal.likes}
											<i class="far fa-thumbs-down pl-3" style={{color: '#FF0000','cursor':'pointer'}} aria-hidden="true" onClick={this.saveDealFeedback.bind(this,'disLike')}></i>&nbsp;
											{this.props.deal.dislikes}
										</div>
									}
									</div>
								}
							</div>
							}
						</div>
					</div>
		        </div>
		      <div>
			        <a href={(this.props.deal != null && this.props.deal.affliateLink != null) ? this.props.deal.affliateLink : this.props.deal.url} target="#" data-placement="top" id={'Tooltip'+this.props.keys}>
			        	<div className="p-cardtext" dangerouslySetInnerHTML={{ __html: this.props.deal.text }}></div>
			        </a>
					<div className="p-myfavorites mt-2 text-center">
						
						{
							this.getHeartIconOrDeleteIcon()
						}
						{ this.props.deal.addToCartUrl != null  &&
							<button className="btn btn-sm btn-outline-primary float-right" ><a href={(this.props.deal.addToCartUrl)} target="#" className="ahover"><i class="fas fa-shopping-cart"></i> Add to Cart</a></button>
						}
						
					</div>
		      </div>
				{window.innerWidth >600 &&
					<UncontrolledTooltip placement="top" target={'Tooltip'+this.props.keys} style={{backgroundColor:'white', color:'black', opacity :'1'}}>
						<div  dangerouslySetInnerHTML={{ __html: this.props.deal.text }}></div>
					</UncontrolledTooltip>}
		      </div>
	      </div>
			</section>
      );
   }
	
   getHeartIconOrDeleteIcon(){
		var html;
	  if(this.props.deal.addToCartUrl != null){
		html = <button className="btn btn-sm btn-outline-primary float-left" onClick = { this.remove.bind(this)}><i className="fas fa-minus-circle"></i> Remove</button> ;
	  }else{
		html = <button className="btn btn-sm btn-outline-primary" onClick = { this.remove.bind(this)}><i className="fas fa-minus-circle"></i> Remove</button> ;
	  }
	   if(this.props.deleteIcon){
		}else{
			if(this.props.deal.addToCartUrl != null){
				html = <button className="btn btn-sm btn-outline-primary float-left" onClick = {() => this.toggle()}><i className={'fa fa-heart mr-1' + (this.props.deal.active ? 'active': null)}></i>&nbsp;Favorites</button> 
			}else{
				html = <button className="btn btn-sm btn-outline-primary" onClick = {() => this.toggle()}><i className={'fa fa-heart mr-1' + (this.props.deal.active ? 'active': null)}></i>&nbsp;Favorites</button> 
			}
		}
	   return html;
   }
   
	toggle(){
	   this.props.deal.active = !this.props.deal.active;
	   this.setState({ active: this.props.deal.active });
	   this.props.deal.isFav = true;
	   this.props.onFavIconClick(this.props.deal);
   }
	
	remove(){
	   this.props.deal.active = false;
	   this.setState({ active: this.props.deal.active });
	   this.props.deal.isFav = this.props.deal.active ;
	   this.props.onRemoveFavDealClick(this.props.deal);
	}
   
}
		        
export default HotDealCard;
