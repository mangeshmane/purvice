import Login from './Login';
import Page404 from './Page404';
import Page500 from './Page500';
import Register from './Register';
import Verify from './Verify';
import ForgotPassword from './ForgotPassword';
import ResetPassword from './ResetPassword';

export {
  Login, Page404, Page500, Register, Verify, ForgotPassword, ResetPassword
};