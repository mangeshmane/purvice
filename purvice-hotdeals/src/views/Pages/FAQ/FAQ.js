import React, { Component } from 'react';
import './FAQ.css';
import Env from '../../../environments/environment.js';

class FAQ extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
    	<React.Fragment>
	    	<section id="giftcards" className="top-padding-section">
	    	   <div className="content-width ">
	    	      <div className="row">
	    	         <div className="col">
	    	            <div className="cards">
	    	               <h2 className="section-title">Most Popular Questions (FAQ)</h2>
	    	               <div className="row">
	    	                  <div className="container">
	    	                     <ul className="questionList">
	    	                        <li onClick = {() =>this.onfaq('q1')}>What is your return policy?</li>
	    	                        <li onClick = {() =>this.onfaq('q2')}>What is Purvice Choice Card?</li>
	    	                        <li onClick = {() =>this.onfaq('q3')}>I forgot my Password, how do I reset?</li>
	    	                        <li onClick = {() =>this.onfaq('q4')}>How do I redeem a Purvice Card?</li>
	    	                        <li onClick = {() =>this.onfaq('q5')}>How do I redeem a gift card in-store?</li>
	    	                     </ul>
	    	                     <h5 className="faqquestion" id="q1">What is your return policy?</h5>
	    	                     <p className="static-page-text" > All purchases made on the web or through Purvice mobile apps are final.   
	    	                     </p>
	    	                     <h5 className="faqquestion" id="q2">What is Purvice Choice Card?</h5>
	    	                     <p className="static-page-text" > Purvice Choice Card lets you designate the gift card value to one or more gift cards from our wide array of offerings. This is perfect card for gift. 
	    	                     </p>
	    	                     <h5 className="faqquestion" id="q3">I forgot my Password, how do I reset?</h5>
	    	                     <p className="static-page-text" > On Login Screen, Click "Forget Password". This will walk you through the process of resetting the password.   
	    	                     </p>
	    	                     <h5 className="faqquestion" id="q4">How do I redeem a Purvice Card?</h5>
	    	                     <p className="static-page-text" > Please follow the link in the email. This will take you to website where you will be able to designate the purvice choice card amount among one or more cards.   
	    	                     </p>
	    	                     <h5 className="faqquestion" id="q5">How do I redeem a gift card in-store?</h5>
	    	                     <p className="static-page-text" > Please take the print out of the gift card. Please note that not all cards can be redeemed at store. Please read the terms and conditions as these vary from store to store.   
	    	                     </p>
	    	                  </div>
	    	               </div>
	    	            </div>
	    	         </div>
	    	      </div>
	    	   </div>
	    	</section>
		</React.Fragment>
    );
  }
  
  onfaq(q) {
	  let x = document.querySelector("#"+q);
	  if (x){
	        x.scrollIntoView();
	        var scrolledY = window.scrollY;
	        if(scrolledY){
	        	window.scroll(0, scrolledY - 80);
	        }
	  }
  }
}

export default FAQ;
