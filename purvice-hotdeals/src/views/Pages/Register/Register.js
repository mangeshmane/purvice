import React, { Component } from 'react';
import { Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import logo from '../../../assets/img/purvice_logo_blue.jpg';
import gift1 from '../../../assets/img/gift1.png';
import gift2 from '../../../assets/img/responsive.png';
import './register.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import Env from '../../../environments/environment.js';

var divStyle = {
  color: "red",
  marginTop: "0%"
};
class Register extends Component {
  toastId = null;
  constructor(props) {
    super(props);
    this.state = {
      error:'',
      firstname:false,
      lastname:false,
      passwordState:false,
      passwordStateInvalid:false,
      emailStateInvalid:false,
      emailState:false,
      spinner: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  logIn = ()=>{
    this.props.history.push('/login');
  }

  handleChange(e){
    const { name, value } = e.target;
    if(name === "firstName" ){
      if(value === ""){
      this.setState({firstname:true});
    }else{
      this.setState({firstname:false});
    }
  }
    if(name === "lastName"){
      if(value === ""){
      this.setState({lastname:true});
    }
    else{
    this.setState({lastname:false});
    }
  }
  if(name === "email"){
    if (value === "") {
    this.setState({emailState :true});
    this.setState({emailStateInvalid:false});

  }else if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)){
    this.setState({emailStateInvalid:true});
    this.setState({emailState :false});

  }else{
    this.setState({emailStateInvalid:false});
    this.setState({emailState:false});
  }
  }
  if(name === "password"){
    if(value === ""){
    this.setState({passwordState:true});
    this.setState({passwordStateInvalid:false});
  }else if(!/^\S*$/.test(value)){
    this.setState({passwordState:false});
    this.setState({passwordStateInvalid:true});
  }else{
    this.setState({passwordState:false});
    this.setState({passwordStateInvalid:false});
  }
  }
  this.setState({ [name]: value });
}

  handleSubmit(e){
    e.preventDefault();
    const { firstName, lastName, email, password, returnUrl } = this.state;

      if ((firstName && lastName && email && password)) {
      var data = {
        username:this.state.email,
        password:this.state.password,
        firstName:this.state.firstName,
        lastName:this.state.lastName
      }
      this.setState({spinner:true});
      axios.post(Env.baseUrl + '/register', data,{
        headers:  {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*"
        }
      }).then(function (response) {
        this.setState({spinner:false});
        if (response.status === 200) {
          if(response.data.message === 'SUCCESS'){
            if (!toast.isActive(this.toastId)) {
            this.toastId = toast.success("Success Please check your mail and verify your account.", { position: toast.POSITION.TOP_RIGHT });
          }
          setTimeout(function () { this.props.history.push('/login'); }.bind(this), 3000);
        }else{
          if(response.data.message === 'FAIL'){
            if(response.data.explanation === 'Email address already used.Please use different Email address.'){
            if (!toast.isActive(this.toastId)) 
            this.toastId=toast.error("Email Address Already Exists", { position: toast.POSITION.TOP_RIGHT });
            }
      }
    }
  }
      }.bind(this)).catch(err => {
          if (!toast.isActive(this.toastId)) {
            this.toastId = toast.error(err, { position: toast.POSITION.TOP_RIGHT });
          }
      })
    }else{
      if (!toast.isActive(this.toastId)) 
      this.toastId=toast.error("Please Enter Your Details", { position: toast.POSITION.TOP_RIGHT });
    }
    
  }

  goToHotDeals = ()=>{
    this.props.history.push('/hotdeals');
  }

  render() {
    return (
      <div className="mt-5 align-items-center">
      <ToastContainer autoClose = {2500}/>
        <Container>
          <Row className="justify-content-center">
            <Col sm="9" md="7" lg="5" xl="5" >
              <Card className="mx-4 mt-1">
                <CardBody className="p-4">
                  <Form>
                  <div style={{ display: 'flex', justifyContent: 'center' }}><img src={logo} style = {{height:'50px',cursor:'pointer'}} alt="logo" onClick = {this.goToHotDeals}/></div>
                  <h5 style={{ display: 'flex', justifyContent: 'center',marginTop:'0.5rem',marginBottom:'1rem' }}>
                  It's Easy to Get Started And It's Free!
                  </h5>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="First Name" name = "firstName" autoComplete="firstname" onChange = {this.handleChange} />
                    </InputGroup>
                    {this.state.firstname && 
                      <InputGroup className="mb-3">
                      <div style={divStyle}>
                          Please enter first name.
                          </div>
                      </InputGroup>
                      }
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="Last Name" name = "lastName" autoComplete="lastname" onChange = {this.handleChange} />
                    </InputGroup>
                    {this.state.lastname && 
                      <InputGroup className="mb-3">
                      <div style={divStyle}>
                          Please enter last name.
                          </div>
                      </InputGroup>
                      }
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>@</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="Email Address" name = "email" autoComplete="email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" onChange = {this.handleChange} />
                    </InputGroup>
                    {this.state.emailStateInvalid && 
                      <InputGroup className="mb-3">
                      <div style={divStyle}>
                          Please enter valid email address.
                          </div>
                      </InputGroup>
                      }
	
                       {this.state.emailState && 
                      <InputGroup className="mb-3">
                      <div style={divStyle}>
                          Please enter email address.
                          </div>
                      </InputGroup>
                      }

                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" placeholder="Password" name = "password" autoComplete="new-password" onChange = {this.handleChange} />
                    </InputGroup>
      
                    {this.state.passwordState && 
                      <InputGroup className="mb-3">
                      <div style={divStyle}>
                          Please enter password.
                          </div>
                      </InputGroup>
                      }

                      {this.state.passwordStateInvalid && 
                      <InputGroup className="mb-3">
                      <div style={divStyle}>
                          Please enter valid password.
                          </div>
                      </InputGroup>
                      }
                    <Button color="primary" type = "submit"  block style={{fontWeight: '600'}} onClick={this.handleSubmit}><i  className="fa fa-spinner fa-spin  float-right" hidden = {!this.state.spinner}></i>Sign Up With Email</Button>
                    
                <InputGroup className="mb-3 mt-2">
                <p className="noAccount">Already have an account? <a onClick = {this.logIn} style = {{color:'#53bbe2',fontWeight: '600'}}>Log in now.</a></p>
                </InputGroup>
                  </Form>
                </CardBody>
              </Card>
              {this.state.error &&
                        <div className={'alert alert-danger'}>{this.state.error}</div>
                    }
            </Col>

          </Row>

         <hr className="mt-5"/>
         <Row className="justify-content-center">
              <Col lg = "5">
            <p className="copy">Copyright © 2018 purvice,Inc. All Rights Reserved.</p>
            </Col>
            <Col lg = "7">
            <p className="footer-link">
	        	<a className="mr-1 link-hove"  onClick = {() =>this.props.history.push('/about-us')}>About Us</a>
	            <a className="mr-1 link-hove"  onClick = {() =>this.props.history.push('/faq')}>FAQ</a>
	            <a className="mr-1 link-hove"  onClick = {() =>this.props.history.push('/privacy')}>Privacy</a>
	            <a className="mr-1 link-hove"  onClick = {() =>this.props.history.push('/terms')}>Terms</a>
	            <a className="mr-1 link-hove"  onClick = {() =>this.props.history.push('/copyright')}>Copyright</a>
            </p>
            </Col>
            </Row>

        </Container>
      </div>
    );
  }
}

export default Register;
