import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import logo from '../../../assets/img/purvice_logo_blue.jpg';
import facebook from '../../../assets/img/social_login.png';
import google from '../../../assets/img/google.png';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import '../Login/login.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ForgotPasswordService } from '../../../services/forgot-password.service';
import cookie from 'react-cookies';

var divStyle = {
  color: "red",
  marginTop: "0%"
};

class ForgotPassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      submitted: false,
      emailState: false,
      userName: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.passwordForgot = this.passwordForgot.bind(this);
  }

  handleChange(e) {
    this.state.userName = e.target.value;
    const { name, value } = e.target;
    this.setState({ [name]: value });
    if (name === 'email') {
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
        this.setState({ emailState: false })
      } else {
        this.setState({ emailState: true })
      }
    }
  }

  register = () => {
    this.props.history.push('/register');
  }

  goToHotDeals = ()=>{
    this.props.history.push('/hotdeals');
  }
  passwordForgot(e){
    var user =   {
      username: this.state.userName
    }
    if (this.state.userName != "") {
      ForgotPasswordService.forgotPassword(user).then(data => {
        if (data.message === 'SUCCESS') {
          toast.success("Please check your mail to reset your password", { position: toast.POSITION.TOP_RIGHT });
        } else {
          toast.error(data.explanation, { position: toast.POSITION.TOP_RIGHT })
        }
      },
        error => this.setState({ error, loading: false })
      );


  }
}

  render() {
    const { email, submitted, emailState } = this.state;
    return(
      <div className="mt-5 align-items-center">
      <ToastContainer autoClose={2500} />
      <Container>
        <Row className="justify-content-center">
          <Col lg="4" md="5">
            <CardGroup>
              <Card className="p-3 login-section">
                <CardBody>
                  <div style={{ display: 'flex', justifyContent: 'center' }}><img src={logo} style={{ height: '50px' }} onClick = {this.goToHotDeals} /></div>
                  <h4 style={{ display: 'flex', justifyContent: 'center', marginTop: '0.5rem', marginBottom: '1rem' }}>
                  Forgot Your Password?
                </h4>
                <p style={{ display: 'flex', justifyContent: 'center', marginTop: '0.5rem', marginBottom: '1rem' }}>Enter the email address for your purvice account. We'll send a link to reset your password.</p>

                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-envelope-letter"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" onChange={this.handleChange} name="email" placeholder="Email Address" />
                    </InputGroup>

                      {submitted && !email &&
                        <InputGroup className="mb-3">
                          <div style={divStyle} hidden={this.state.emailState}>
                            Please enter email address.
                          </div>
                        </InputGroup>
                      }
                      {emailState &&
                        <InputGroup className="mb-3">
                          <div style={divStyle} >
                            Please enter valid email address.
                          </div>
                        </InputGroup>
                      }
         


                    <InputGroup className="mb-1">
                      <Button color="primary" block onClick={this.passwordForgot.bind(this)} style={{ fontWeight: '600' }}><i class="fa fa-spinner fa-spin  float-right" hidden={!this.state.spinner}></i>Reset Your Password</Button>
                    </InputGroup>      

                    <p className="noAccount">Don't have an account? <a style={{ color: '#53bbe2' }} onClick={this.register}>Sign up now.</a></p>
                </CardBody>
              </Card>
            </CardGroup>
          </Col>
        </Row>
        <hr className="mt-5"/>
        <Row className="justify-content-center">
          <Col lg="5">
            <p className="copy">Copyright © 2018 purvice,Inc. All Rights Reserved.</p>
          </Col>

          <Col lg="7">
            <p className="footer-link">
              <a className="mr-1"  onClick = {() =>this.props.history.push('/about-us')}>About Us</a>
              <a className="mr-1"  onClick = {() =>this.props.history.push('/faq')}>FAQ</a>
              <a className="mr-1"  onClick = {() =>this.props.history.push('/privacy')}>Privacy</a>
              <a className="mr-1"  onClick = {() =>this.props.history.push('/terms')}>Terms</a>
              <a className="mr-1"  onClick = {() =>this.props.history.push('/copyright')}>Copyright</a>
            </p>
          </Col>
        </Row>


      </Container>


    </div>


  );
  }
}

export default ForgotPassword;
