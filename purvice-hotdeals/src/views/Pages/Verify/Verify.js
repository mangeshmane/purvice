import React, { Component } from 'react';
import './verify.css';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { verifyService } from '../../../services/verify.service';
import logo from '../../../assets/img/purvice_logo_blue.jpg';



class Verify extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
          token: this.props.match.params.token,
          showSuccess:false,
          showError:false
        };
        this.verifyToken = this.verifyToken.bind(this);
        this.verifyToken();
      }


    verifyToken() {
      
      this.setState({ submitted: true });
      var data = {
        token:this.state.token
      }
      verifyService.verifyToken(data).then(user => {
          
        if(user.message === 'SUCCESS'){
            this.setState({showSuccess:true});
                toast.success("Verify Successful", { position: toast.POSITION.TOP_RIGHT });
        }else{
            this.setState({showError:true});
          toast.error("Verification fail", { position: toast.POSITION.TOP_RIGHT })
        }
        },
        error => this.setState({ error, loading: false })        
      );
  }

  render() {
      
    return (
        <div className="app ">
    <div className="login margin-top-100">
        <div className="container">
            <div className="row justify-content-center">

                <div className="col-lg-5 col-sm-12 col-xs-12 login-section">
                    <div className="mb-0">
                        <div className="card p-4">
                            <div className="card-block ">
                                <h1 className="login-title" onClick={this.goToHotDeals.bind(this)}><img className="logo_image logo_icon" src={logo}/></h1>
                                <form style={{textAlign: "center",marginTop:"25px"}}>
                                    {this.state.showSuccess && 
                                    <div>
                                    <h2  className="verify-message">Thanks !!!</h2>
                                    <h2  className="verify-message">Registering With purvice </h2>
                                    <h2  className="verify-message">Please Click Login to Start Buying Cards </h2>
                                    </div>
                                    }
                                    {this.state.showError && 
                                    <div>
                                    <h2  className="verify-message">This Token is Expire</h2>
                                    <h2  className="verify-message">Please Use Valid One</h2>
                                    </div>
                                    }
                                    <div className="row">
                                        <div hidden = {this.state.showError} className=" col-12">
                                            <button type="button" onClick={this.goToLoginPageVerify.bind(this)} className="btn btn-primary p login-button mt-1 ">Login</button>
                                        </div>

                                    </div>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

    <div id="login-footer">

        <div className="content-width">
            <div className="section-divider">
                <div className="divider-div"></div>
            </div>
            <p className="copy">Copyright © 2018 purvice,Inc. All Rights Reserved.</p>
            <p className="footer-link"><a  className="mr-1 link-hove ">About Us</a>
                <a  className="mr-1 link-hove ">FAQ</a>
                <a className="mr-1 link-hove">Privacy</a>
                <a className="mr-1 link-hove">Terms</a>
                <a className="mr-1 link-hove">Copyright</a>
            </p>
        </div>


    </div>
    </div>
    );
  }
  
    goToHotDeals(){
        window.location= "#/hotdeals";
    }
     goToLoginPageVerify(){
         window.location= "#/login";
   }
}

export default Verify;
