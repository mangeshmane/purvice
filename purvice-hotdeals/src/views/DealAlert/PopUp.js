import React, { Component } from 'react';
import './dealAlert.css';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

class PopUp extends Component {

    constructor(props) {
        super(props)
        this.state = {
            primary: false,
            message: "",
            header: "",
            parentData: {},
            forumKeywordFlag : false,
            editCategoryFlag : false
        }

        this.toggleCancel = this.toggleCancel.bind(this);
        this.togglePopup = this.togglePopup.bind(this);
        this.toggleOK = this.toggleOK.bind(this);
        this.toggleCancel = this.toggleCancel.bind(this);
        this.openpup = this.openpup.bind(this);
        this.appendKeyword = this.appendKeyword.bind(this);
        this.resetKeyword = this.resetKeyword.bind(this);
        this.resetCategory = this.resetCategory.bind(this);
        this.closePopup = this.closePopup.bind(this);
    }

    togglePopup(message, data) {
        this.state.message = message
        this.setState({
            primary: !this.state.primary,
        });
        this.state.parentData = data
    }
    toggleOK() {
        this.setState({
            primary: !this.state.primary,
        });
        if (this.state.parentData != undefined) {
            this.props.saveDealAlerts(this.state.parentData)
        }

    }
    toggleCancel() {
        this.setState({
            primary: !this.state.primary,
        });
    }

    openpup (message, data, editCategoryFlag) {
        //this.setState({forumKeywordFlag : true})
        if(data.key == "Category" || typeof data.key == "number"){
            this.state.header = "Replace Category";
        }else{
            this.state.header = "Append or Replace Keyword?";
        }
        this.state.message = message
        this.state.forumKeywordFlag = true
        this.setState({
            forumKeywordFlag: true,
        });
        this.state.parentData = data;
        this.state.editCategoryFlag = editCategoryFlag;
    }
    closePopup (){
        this.setState({
            forumKeywordFlag: !this.state.forumKeywordFlag,
        });
    }
    appendKeyword() {
        this.setState({
            forumKeywordFlag: !this.state.forumKeywordFlag,
        });
        if (this.state.parentData != undefined) {
            this.props.appendKeyword(this.state.parentData)
        }

    }
    resetKeyword() {
        this.setState({
            forumKeywordFlag: !this.state.forumKeywordFlag,
        });
        if (this.state.parentData != undefined) {
            this.props.resetKeyword(this.state.parentData)
        }
    }
    resetCategory() {
        this.setState({
            forumKeywordFlag: !this.state.forumKeywordFlag,
        });
        if (this.state.parentData != undefined) {
            this.props.resetCategory(this.state.parentData)
        }
    }

    render() {

        return (
            <div className="row">

                <Modal isOpen={this.state.primary}
                    className={'modal-primary ' + this.props.className}>
                    <ModalHeader toggle={this.toggleCancel}>Warning</ModalHeader>
                    <ModalBody>
                        {this.state.message}
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleOK}>OK</Button>{' '}
                        <Button color="secondary" onClick={this.toggleCancel}>Cancel</Button>
                    </ModalFooter>
                </Modal>

                 <Modal isOpen={this.state.forumKeywordFlag}
                    className={'modal-primary ' + this.props.className}>
                    <ModalHeader toggle={this.closePopup}>{this.state.header}</ModalHeader>
                    <ModalBody>
                        {this.state.message}
                    </ModalBody>
                    <ModalFooter>
                        { !this.state.editCategoryFlag &&
                        <div>
                            <Button color="primary" onClick={this.appendKeyword}>Append</Button>{' '}
                            <Button color="secondary" onClick={this.resetKeyword}>Replace</Button>
                        </div>
                        }
                        { this.state.editCategoryFlag &&
                        <div>
                            <Button color="secondary" onClick={this.resetCategory}>Replace</Button>
                        </div>
                        }
                    </ModalFooter>
                </Modal>
            </div>

        )
    }
}



export default PopUp;


