import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import FileBase64 from 'react-file-base64';
import { BannerService } from '../../../services/banner.service';
import { Button, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';

import './Banner.css';
import '../../HotDeals/HotDeals.css';

class Banner extends Component {
  toastId = null;
  constructor(props) {
    super(props);
    this.state = {
      files: {},
      bannerData: [],
      warning: false,
      delModal: false,
      deleteId: 0,
      isUpload: false,
      dataMessage: ""
    };

    this.onBannerClick = this.onBannerClick.bind(this)
    this.getAllBanner = this.getAllBanner.bind(this)
    this.uploadtoggle = this.uploadtoggle.bind(this)
    this.deltoggle = this.deltoggle.bind(this)
    this.getAllBanner();
  }
  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  getAllBanner() {

    BannerService.getAllBanner().then(bannerImage => {
      for (var i = 0; i < bannerImage.result.length; i++) {
        if (bannerImage.result[i].isEnable == "1") {
          bannerImage.result[i]["checked"] = true;
        } else {
          bannerImage.result[i]["checked"] = false;
        }
      }
      this.state.bannerData = bannerImage.result;
      this.setState({ bannerData: bannerImage.result });
    });
  }

  openDelModal(target) {

    this.setState({ deleteId: target.id });
    this.setState({ delModal: true });
  }

  deleteBanner = () => {

    BannerService.deleteBanner(this.state.deleteId).then(bannerImage => {
      for (var i = 0; i < this.state.bannerData.length; i++) {
        if (this.state.bannerData[i].id == this.state.deleteId) {
          this.state.bannerData.splice(i, 1);
        }
      }
      this.setState(this.state);
      this.deltoggle();
      this.toastId = toast.success("Banner delete successfully!", { position: toast.POSITION.TOP_RIGHT });
    });
  }

  onBannerClick({ target }) {

    var data = target.defaultValue
    var bannerData = {}
    for (var i = 0; i < this.state.bannerData.length; i++) {
      if (this.state.bannerData[i].id == data) {
        if (this.state.bannerData[i].isEnable == "1") {
          this.state.bannerData[i].isEnable = "0";
          this.state.bannerData[i].checked = false;
        } else {
          this.state.bannerData[i].isEnable = "1";
          this.state.bannerData[i].checked = true;
        }
        this.setState(this.state);
        bannerData = this.state.bannerData[i];
        break;
      }
    }
    BannerService.bannerUpdate(bannerData);
    this.toastId = toast.success("Banner updated successfully on deals page!", { position: toast.POSITION.TOP_RIGHT });
  }

  getFiles(files) {
    debugger
    this.setState({ files: files })
    if (Number(files.size.split(" ")[0]) < 16000) {
      this.setState({ isUpload: true });
      this.setState({ dataMessage: "" });
    } else {
      this.setState({ dataMessage: "You are selected " + files.size + " of banner! Please select less than 16000 KB banner" })
    }
  }

  uploadImageDB = () => {
    debugger
    var bannerData = {
      imageUrl: this.state.files.base64,
      imageName: this.state.files.name.split(".")[0],
      isEnable: '1',
      type: this.state.files.type
    }
    BannerService.bannerSave(bannerData).then(data => {
      if (data.message == "FAIL") {
        this.toastId = toast.error(data.explanation, { position: toast.POSITION.TOP_RIGHT });
      } else {
        if (data.result.isEnable == "1") {
          data.result["checked"] = true;
        } else {
          data.result["checked"] = false;
        }
        this.state.bannerData.push(data.result);
        this.setState(this.state);
        this.uploadtoggle();
        const div=<div className="animated fadeIn pt-1 text-center">Loading...</div>
        this.toastId = toast.success(data.explanation, { position: toast.POSITION.TOP_RIGHT });
      }
    });
  }

  uploadtoggle() {

    this.setState({
      warning: !this.state.warning,
    });
  }

  deltoggle() {

    this.setState({
      delModal: !this.state.delModal,
    });
  }

  uploadImageModal = () => {
    this.setState({
      warning: true,
    });
  }
  displayLoader = (files) => {
    debugger
    if (files.size > 10000) {
      this.loader();
    }
    else {
      this.setState({
        message: 'Please wait'
      });
    }
  }
  render() {
    return (
      <div className="container banner">
        <div className="row">
          <ToastContainer autoClose={2500} />
          <div className="container mb-4 mt-4 friend-details" style={{ borderTopColor: "#f9a71a", borderTopStyle: "solid", borderTopWidth: "7px" }}>
            <h2 className="mt-4">Banner Image</h2>
            <div className="mb-4" style={{ float: "right" }}>
              <Button color="primary" onClick={(e) => this.uploadImageModal()}>Upload Image</Button>
            </div>
            <table id="userTable" class="table text-center">
              <thead>
                <tr class="table-design">
                  <th>Enable</th>
                  <th>Image Name</th>
                  <th>Image</th>
                  <th>Type</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {
                  this.state.bannerData.map((item, index) => (
                    <tr>
                      <td><input type="checkbox" name="enable" value={item.id} checked={item.checked} onClick={this.onBannerClick} /></td>
                      <td>{item.imageName}</td>
                      <td>
                        <img src={item.imageUrl} style={{ height: "56px", width: "93px", borderRadius: '3px' }} alt="Upload profile image" />
                      </td>
                      <td>{item.type}</td>
                      <td>
                        <div data-toggle="tooltip" data-placement="top" title="Delete Banner" class="cursor">
                          <div onClick={() => this.openDelModal(item)}>
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                          </div>
                        </div>
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
          </div>
          {/* Upload Image Modal  */}
          <Modal isOpen={this.state.warning} toggle={this.uploadtoggle} className={this.props.className}>
            <ModalHeader toggle={this.uploadtoggle}>Upload Image</ModalHeader>
            <ModalBody>
              <FileBase64
                multiple={false}
                onDone={this.getFiles.bind(this)} />
              <div>
                <h6 style={{ color: 'red' }}>{this.state.dataMessage}</h6>
              </div>
            </ModalBody>
            <ModalFooter>
              {this.state.isUpload &&
                <Button color="primary" onClick={(e) => this.uploadImageDB()}  onWaiting={() => this.displayLoader(this.state.files)}>Upload</Button>
              }
              <Button color="secondary" onClick={this.uploadtoggle}>Cancel</Button>
            </ModalFooter>
          </Modal>

          {/* Delete conformation Modal  */}
          <Modal isOpen={this.state.delModal} toggle={this.deltoggle} className={this.props.className}>
            <ModalHeader toggle={this.deltoggle}>Delete Banner</ModalHeader>
            <ModalBody>
              Do you really want to delete this Banner ?
                        </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={(e) => this.deleteBanner()}>Delete</Button>
              <Button color="secondary" onClick={this.deltoggle}>Cancel</Button>
            </ModalFooter>
          </Modal>

        </div>
      </div>
    );
  }

}

export default Banner;
