var Env = {
  baseUrl: "http://localhost:8080/purvice",
  //baseUrl: "https://purvice.com/purvice",
  videoUrl: "https://www.youtube.com/embed/1nKl9hVUGkQ?autoplay=1",
  sharedUrl: "http://localhost:3000/",
  defaultPageSize: 100
}

export default Env;
