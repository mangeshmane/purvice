import React from 'react';
import DefaultLayout from './containers/DefaultLayout';

const HotDeals = React.lazy(() => import('./views/HotDeals/HotDeals'));
const DealAlert = React.lazy(() => import('./views/DealAlert/DealAlert'));
const MyProfile = React.lazy(() => import('./views/MyProfile/MyProfile'));
const Howwork = React.lazy(() => import('./views/Howwork/Howwork'));
const AboutUs = React.lazy(() => import('./views/Pages/AboutUs/AboutUs'));
const FAQ = React.lazy(() => import('./views/Pages/FAQ/FAQ'));
const Privacy = React.lazy(() => import('./views/Pages/Privacy/Privacy'));
const Terms = React.lazy(() => import('./views/Pages/Terms/Terms'));
const Copyright = React.lazy(() => import('./views/Pages/Copyright/Copyright'));
const Travel = React.lazy(() => import('./views/Travel/Travel'));
const Banner = React.lazy(() => import('./views/Admin/Banner/Banner'));

const routes = [
  { path: '/', exact: true, name: 'Home', component: DefaultLayout },
  { path: '/hotdeals', name: 'HotDeals', component: HotDeals },
  { path: '/deal-alert/:id', name: 'DealAlert', component: DealAlert },
  { path: '/deal-alert/', name: 'DealAlert', component: DealAlert },
  { path: '/myprofile', name: 'MyProfile', component: MyProfile },
  { path: '/howwork', name: 'Howwork', component: Howwork },
  { path: '/about-us', name: 'MyProfile', component: AboutUs },
  { path: '/faq', name: 'MyProfile', component: FAQ },
  { path: '/privacy', name: 'MyProfile', component: Privacy },
  { path: '/terms', name: 'MyProfile', component: Terms },
  { path: '/copyright', name: 'MyProfile', component: Copyright },
  { path: '/travel', name: 'DealAlert', component: Travel },
  { path: '/banner', name: 'Banner', component: Banner },
];

export default routes;
