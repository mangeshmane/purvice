server {
    server_name localhost;
    
    location / {
            proxy_redirect off;
            proxy_set_header host $host;
            proxy_set_header X-real-ip $remote_addr;
            proxy_set_header X-forward-for $proxy_add_x_forwarded_for;
            proxy_pass http://localhost:3000;
        }

     location /purvice {
    
            proxy_redirect off;
            proxy_set_header host $host;
            proxy_set_header X-real-ip $remote_addr;
            proxy_set_header X-forward-for $proxy_add_x_forwarded_for;
            #proxy_pass http://localhost:8080/purvice;
	    proxy_pass http://192.168.1.157:8383/purvice;
        }

}
