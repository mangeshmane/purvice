==================================================
    PURVICE APPLICATION SETUP DOCUMENTATION 
==================================================

1. Make sure you have an up-to-date version of Node.js installed on your system. 
If you don't have Node.js installed, you can install it from https://nodejs.org/en/download/

to check node.js is install or not type "node -v" anywhere in the terminal. if node.js is installed then it will show version of node.js

2. Open a terminal window (Mac) or a command window (Windows), and install Cordova and Ionic:
   Run the following command in terminal "npm install -g ionic cordova"
   Note:  On a Mac, you may have to use sudo depending on your system configuration

3. navigate to the folder where you want to setup project from terminal 
   clone the latest code from bitbucket by following command 
   git clone https://sidhHT@bitbucket.org/akscbus/purvice.git  
   this will clone the latest code from bitbucket in you project directory.

4. now navigate to purvice/purvice-mobile-app using terminal which is root directory for mobile app.  
   to navigate type command "cd purvice/purvice-mobile-app"

5. now install all the node.js dependencies by typing command "npm install"

6. start the ionic project by typing command "ionic serve"

project will start in browser on port for eg: 8100

hit the project url in browser : http://localhost:8100/ 
