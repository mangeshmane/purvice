import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

import { ViewChild } from '@angular/core';

import { AppContext } from '../../providers/appContext';

import { CardDescription } from '../card-description/card-description';

  

import { Loader } from '../loader/loader';

import * as _ from "lodash";

var that;

@Component({
  selector: 'card-listing',
  templateUrl: 'card-listing.html'
})
export class CardListing {
  // variable
  products: string = 'byStore';
  public currentUser: any;
  public navigation: any;
  public response: any;
  public categories: any;
  public catItem: any;
  public loadingDismiss: any;
  public collapseExpand: boolean = true;
  breedsLength;
  catLength;
  loading;
  searchItem = '';
  listingArray: any = [];
  showSearchBar = false;
  breeds: any[];
  byStore : any;
  showCategoryItem: any;
  currentPageClass = this;
  hideListing = false;
  allCards;
  alphaScrollItemTemplate: string = `
                                  <ion-row  class="item-listing" (click)="currentPageClass.setItem(item)">
                                    <ion-col col-6>
                                      <ion-thumbnail item-start>
                                        <img style="width:35%; margin-top: 3% !important;box-shadow: 6px 6px 11px #888888;" src="{{item.brand.imageUrls[0].imageURL}}">
                                      </ion-thumbnail>
                                    </ion-col>
                                    <ion-col col-6 style="margin-left: -36%;">
                                      <h2 class="item-heading">{{item.rewardName}}</h2>
                                      <p  *ngIf="item.valueType == 'VARIABLE_VALUE'"> $ {{item.minValue}}  - $ {{item.maxValue}} </p>
                                    </ion-col>
                                  </ion-row>
                                  <div class="list-item-border"></div>`;
  triggerAlphaScrollChange: number = 0;
  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController,
    public appContext: AppContext
  ) {
    that = this;
    this.getAllListing();
    this.getCategories();

  }

  ngOnInit() {
    this.byStore = true;
    this.appContext.getCurrentUser().then((data) => {
      this.currentUser = data;
      if (this.currentUser == null) {
        this.navigation = 'common';
      } else {
        this.navigation = 'user';
      }
    });
  }



  setItem(item) {
    this.appContext.input(item);
    console.log(item);
    this.navCtrl.push(CardDescription);
    // this.navCtrl.setRoot(CardDescription);
  }

  getAllListing() {
    let unique: any = [];
    // get items 
    this.loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    this.loading.present();
    this.appContext.getRequest('/item/search').subscribe(userData => {
that.allCards= _.cloneDeep(userData.result);
      _.forEach(userData.result, function (card, index) {
        // extract price 
        that.extractPrice(card);
        // end of extract price
      });
      unique = _.uniqBy(userData.result, function (e: any) {
        return e.brand.brandName;
      });

      _.forEach(unique, function (card, index) {
        let childData = [];
        childData = _.filter(userData.result, function (o: any) {
          if (o.brand.brandName == unique[index].brand.brandName)
            return o;
        });

        if (childData.length > 0) {
          card.childData = childData;
          card.childData.sort(that.sort)
        }
      })

      _.forEach(unique, function (card, index) {

        if (card.minValue == null) {
          card.minValue = card.childData[0].fixedValue;
          card.maxValue = card.childData[card.childData.length - 1].fixedValue;
        }

      })

      console.log('searching');
      this.breeds = unique;
      this.breedsLength = this.breeds.length;
      this.loading.dismiss();
    },
   error => console.log(this.loading.dismiss()),);
  }
 

  extractPrice(card) {
    if (card.minValue == null) {
      if (card.rewardName.indexOf("$") != -1) {
        var value = card.rewardName.substr(card.rewardName.indexOf("$") + 1);
        card.rewardName = card.rewardName.substring(0, card.rewardName.indexOf('$'));
        card.fixedValue = parseInt(value);
      }
      return card;
    } else
      return card;
  }

  sort(a, b) {

    if (a.fixedValue < b.fixedValue)
      return -1;
    if (a.fixedValue > b.fixedValue)
      return 1;
    return 0;
  }
  //  get Categories
  getCategories() {

    this.appContext.getRequest('/category/all').subscribe(userData => {
      this.categories = userData.result;
    });

  }

  // accordian
  data: Array<{ title: string, details: string, icon: string, showDetails: boolean }> = [];

  toggleDetails(data) {
        let unique: any = [];
    this.loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    console.log('collapse', data.id, 'data.id', this.showCategoryItem);
    if (data.id == this.showCategoryItem) {
      console.log('collapse', data.id, 'data.id', this.showCategoryItem);
      //  this.collapseExpand = false;
      console.log('collapseExpand', this.collapseExpand);
      if (this.collapseExpand) {
        this.collapseExpand = false;
      } else {
        this.collapseExpand = true;
      }
    } else {
      this.collapseExpand = true;
    }
    this.loading.present();
    this.appContext.getRequest('/item/search?categoryId=' + data.id).subscribe(userData => {
     
     _.forEach(userData.result, function (card, index) {
        // extract price 
        that.extractPrice(card);
        // end of extract price
      });
      unique = _.uniqBy(userData.result, function (e: any) {
        return e.brand.brandName;
      });

      _.forEach(unique, function (card, index) {
        let childData = [];
        childData = _.filter(userData.result, function (o: any) {
          if (o.brand.brandName == unique[index].brand.brandName)
            return o;
        });

        if (childData.length > 0) {
          card.childData = childData;
          card.childData.sort(that.sort)
        }
      })

      _.forEach(unique, function (card, index) {

        if (card.minValue == null) {
          card.minValue = card.childData[0].fixedValue;
          card.maxValue = card.childData[card.childData.length - 1].fixedValue;
        }

      })

 
    
       this.catItem = unique;
       this.catLength = this.catItem.length;
      this.loading.dismiss();
    },
     error => console.log(this.loading.dismiss()),);
    if (this.showCategoryItem) {
      this.showCategoryItem = data.id;
    } else {
      this.showCategoryItem = data.id;
      this.collapseExpand = true;
    }
  }


  filterItems(item) {
    console.log(item);
    this.byStore = false;
    if(!item){
      this.byStore = true;
    }
    console.log(this.searchItem);
    this.hideListing =  true;
    let unique: any = [];
 this.listingArray = this.searchCards(item);
     
 
            _.forEach(that.listingArray, function (card, index) {
        // extract price 
        that.extractPrice(card);
        // end of extract price
      });
      unique = _.uniqBy(that.listingArray, function (e: any) {
        return e.brand.brandName;
      });

      _.forEach(unique, function (card, index) {
        let childData = [];
        childData = _.filter(that.listingArray, function (o: any) {
          if (o.brand.brandName == unique[index].brand.brandName)
            return o;
        });

        if (childData.length > 0) {
          card.childData = childData;
          card.childData.sort(that.sort)
        }
      })

      _.forEach(unique, function (card, index) {

        if (card.minValue == null) {
          card.minValue = card.childData[0].fixedValue;
          card.maxValue = card.childData[card.childData.length - 1].fixedValue;
        }

      })
 
      this.breeds = unique;
      this.breedsLength = this.breeds.length;
    
  }

  showSearchToolbar() {
    this.showSearchBar = true;
  }
  hideSearchToolbar() {
    this.showSearchBar = false;
  }
  goToHome() {
    this.navCtrl.pop();
  }

 
  presentLoadingDefault(items) {
    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    loading.present();
    if (items) {
      loading.dismiss();
    }
  }
  unsetByStore(){
    this.byStore =  false;
    console.log(this.byStore);
  }
    setByStore(){
    // this.byStore =  true;
    // console.log(this.byStore);
  }
  onCancel(){
   //     this.getAllListing();
  }

  searchCards(search) {  //  search= search.charAt(0).toUpperCase() + search.slice(1);  
    let cards;
    if (search.length != 0) {
      cards = _.filter(that.allCards, function (o: any) {
        return o.brand.brandName.search(new RegExp(search, "i")) !== -1;
      });
    } else {
      cards = that.allCards;
    } return cards;
  }
}



