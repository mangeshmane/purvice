import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { AppContext } from '../../providers/appContext';

import { CardDescription } from '../card-description/card-description';

import { LoginOptions } from '../login-options/login-options';

import { PaymentFailed } from '../payment-failed/payment-failed';

import { PaymentSuccess } from '../payment-success/payment-success';

import { LoadingController } from 'ionic-angular';

import { Loader } from '../loader/loader';

import { Platform } from 'ionic-angular';

import {Keyboard} from 'ionic-native'; 

import { Network } from '@ionic-native/network';

import { AlertController } from 'ionic-angular';
declare var require: any;
declare var braintree: any;
declare var $: any;
declare var paypal: any;
var that;
@Component({
  selector: 'paypal-payment',
  templateUrl: 'paypal-payment.html'
})
export class PaypalPayment {

  // variables 
  listingItem;
  price;
  braintreeNonce;
  order;
	orderId;
	public billingAddress: any = {};
	currentUser: any = {};
	public states: any = [];
	paymentMethod;
	paymentLoader;
  public orderStatus = {
    "id": '',
    "paymentTransactionId": null,
    "paymentStatus": ""

  }; 
  constructor(private network: Network ,public plt: Platform, private alertCtrl: AlertController ,public navCtrl: NavController, public appContext: AppContext, public loadingCtrl: LoadingController) {
		that = this;
		plt.ready().then(() => {
      plt.registerBackButtonAction(() => this.myHandlerFunction());

		})
				let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
					that.paymentLoader.dismiss();
					that.paymentLoader = that.loadingCtrl.create({
						content: 'Loading...'
					});
					   	that.paymentLoader.dismiss();
						 that.paymentInitialization.dismiss();
					//	that.appContext.showToast('network was disconnected..');
		});

	}
	
  ngOnInit() {
		this.paymentMethod = 'card';
    that.paymentInitialization = this.loadingCtrl.create({
      content: 'Loading...'
		});
		 that.paymentLoader = that.loadingCtrl.create({
									content: 'Loading...'
	 	});
		that.paymentInitialization.present();
		this.getStates(231);
    this.order = this.appContext.getOrder();
    this.listingItem = this.appContext.output();
    this.price = this.appContext.getAmount();
    this.appContext.getToken().then((token) => {
      this.getNonce(token).then((braintreeToken) => {
        var button = document.querySelector('#submit-button');
        	function handleFieldEvent(event) {
				 
				var iconType = document.getElementsByClassName("icon-type")[0];
				iconType.className = "icon-type";
				if (event.card)
					$cardNumberContainer.addClass(event.card.type);
			}
			var $cardNumberContainer = $('#card-number');
			braintree.client.create({
				authorization:braintreeToken,
			}, function (err, clientInstance) {
				if (err) {
					console.error(err);
					 that.paymentInitialization.dismiss();
					 that.appContext.showToast('Network error');	
					return;
				}


				braintree.hostedFields.create({
					client: clientInstance,
					styles: {
						'input': {
							'font-size': '14px',
							'font-family': 'helvetica, tahoma, calibri, sans-serif',
							'color': '#3a3a3a'
						},
						':focus': {
							'color': 'black'
						}
					},
					fields: {
					number: {
						selector: '#card-number',
						placeholder: '1111 1111 1111 1111'
					},
					cvv: {
						selector: '#cvv',
						placeholder: '111'
					},
					expirationDate: {
						selector: '#expiration-date',
						placeholder: 'MM/YY'
					},
					// postalCode: {
					// 	selector: '#postal-code',
					// 	placeholder: '11111'
					// }
					}
				}, function (err, hostedFieldsInstance) {
					if (err) {
						console.error(err);
						return;
					}
					hostedFieldsInstance.on('validityChange', function (event) {
						
						var field = event.fields[event.emittedBy];

						if (field.isValid) {
							if (event.emittedBy === 'expirationDate') {
								$('#expiration-date').next('span').text('');

							} else if (event.emittedBy === 'number') {
								$('#card-number').next('span').text('');
							}
							else if (event.emittedBy === 'cvv') {
								$('#cvv').next('span').text('');
							}
						} else if (field.isPotentiallyValid) {
							$(field.container).parents('.form-group').removeClass('has-warning');
							$(field.container).parents('.form-group').removeClass('has-success');
							if (event.emittedBy === 'number') {
								$('#card-number').next('span').text('');
							}
						} else {
							$(field.container).parents('.form-group').addClass('has-warning');
							if (event.emittedBy === 'number') {
								$('#card-number').next('span').text('Looks like this card number has an error.');
							}
							else if (event.emittedBy === 'expirationDate') {
								$('#expiration-date').next('span').text('This date is not valid.');
							}
							else if (event.emittedBy === 'cvv') {
								$('#cvv').next('span').text('This CVV is not valid.');
							}
							// else if (event.emittedBy === 'postal-code') {
							// 	$('#cvv').next('span').text('This postal code is not valid.');
							// }
						}
					});
					 that.paymentInitialization.dismiss();
					hostedFieldsInstance.on('cardTypeChange', function (event) {
						console.log('entering card');		
						//document.getElementById("credit-card-number").style.background = "url(http://localhost:8100/assets/images/payment/jcb.png);"
						$('#credit-card-number').addClass('bg2');
				//		$cardNumberContainer.removeClass('visa master-card discover jcb american-expres diners-club maestro');
						if (event.cards.length === 1) {
							$('#card-type').text(event.cards[0].niceType);
							that.cardType = event.cards[0].type;
					//		$cardNumberContainer.addClass(event.cards[0].type);
						} else {
								that.cardType =null;
							$('#card-type').text('Card');
						}
          }); 
              
				 
					$('#cardForm').submit(function (event) {
						console.log('submit clicked ');
						   
								that.paymentLoader.present();
						event.preventDefault();
						var state = hostedFieldsInstance.getState();
						var formValid = Object.keys(state.fields).every(function (key) {
							return state.fields[key].isValid;
						});
 
						if (formValid) {
							hostedFieldsInstance.tokenize(function (err, payload) {
								if (err) {
									that.paymentLoader.dismiss();
											 that.paymentLoader = that.loadingCtrl.create({
													content: 'Loading...'
	 											});
									  that.appContext.showToast('Network error');	
									console.error(err);
									return;
								}
								// update user address 
								that.appContext.getToken().then((token) => {
								that.billingAddress.country = "US"
								that.appContext.billingAddress(that.billingAddress,token).subscribe(data => { })
								});
								// end of update user address
								var postalCode = $('#postal-code').val();
						 
								that.placeOrder(payload.nonce, that.order.price, that.billingAddress.zipCode).then((data) => {
									let responseData = data ? data : { processorResponseCode: "3000" };
									that.showStatus(responseData);
								})
							});
						} else {
								that.paymentLoader.dismiss();
											 that.paymentLoader = that.loadingCtrl.create({
													content: 'Loading...'
	 											});
							  that.appContext.showToast('Please Fill Up All The Fields Properly');	
						}
						
					});
				});

				braintree.paypalCheckout.create({
					client: clientInstance
				}, function (paypalCheckoutErr, paypalCheckoutInstance) {

					if (paypalCheckoutErr) {
						console.error('Error creating PayPal Checkout:', paypalCheckoutErr);
						return;
					}
					 
					paypal.Button.render({
						env: 'sandbox', // or 'production'
						commit: true, // This will add the transaction amount to the PayPal button

						payment: function () {
							return paypalCheckoutInstance.createPayment({
								flow: 'checkout',
								amount: that.order.price,
								currency: 'USD',
								locale: 'en_US',
							});
						},

						onAuthorize: function (data, actions) {
								that.paymentLoader.present();
							return paypalCheckoutInstance.tokenizePayment(data)
								.then(function (payload) {
									var postalCode = $('#postal-code').val();
									that.placeOrder(payload.nonce, that.order.price, that.billingAddress.zipCode).then((data) => {
										let responseData = data ? data : { processorResponseCode: "3000" };
										that.showStatus(responseData);
									})
								});
						},

						onCancel: function (data) {
								
							console.log('checkout.js payment cancelled', JSON.stringify(data));
							that.paymentLoader.dismiss();
								 that.paymentLoader = that.loadingCtrl.create({
													content: 'Loading...'
	 											});
						},

						onError: function (err) {
							console.error('checkout.js error', err);
						 	//that.paymentLoader.dismiss();
						}
					}, '#paypal-button').then(function () {
					});

				});

			});
        // braintree.dropin.create({ authorization: braintreeToken, container: '#dropin-container', paypal: { flow: 'checkout', amount: '10.00', currency: 'USD' } }, function (createErr, instance) {
        //    loading.dismiss();
        //   button.addEventListener('click', function () {
        //     instance.requestPaymentMethod(function (requestPaymentMethodErr, payload) {
        //       that.placeOrder(payload.nonce, that.price);
        //     });
        //   });
        // });
      });

    });


    this.appContext.getCurrentUser().then((data) => {
			    this.currentUser = data;
				
				  this.appContext.getToken().then((token) => {
   				this.appContext.getAddress(that.currentUser.id, token).subscribe(userData => {
					console.log(userData);
					if(userData.result)
					{
						this.billingAddress = userData.result;
					}
         
					});
       
    		  });
      if (data == null) {
          //loading.dismiss();
        this.appContext.showToast('You must log in first!!!');
        this.navCtrl.push(LoginOptions);

      }
    });
  }
  gotoHome() {
		this.appContext.input(undefined);
    this.navCtrl.push(Loader);
    this.navCtrl.setRoot(Loader);
  }

  placeOrder(nonce, price,postalCode) {
		 
    this.orderId = this.appContext.getOrderId();
    this.appContext.getToken().then((token) => {
      that.appContext.getPaymentStatus(nonce, that.order.price, token,postalCode).subscribe((data) => {
        console.log(this.order);
        this.order.paymentStatus = data.result.status;
        console.log(data);
        this.orderStatus.id = this.orderId;
        this.orderStatus.paymentTransactionId = data.result.id;
        this.orderStatus.paymentStatus = data.result.status;
        this.appContext.updateOrderStatus(this.orderStatus, token).subscribe((data) => {
					console.log(data);
					
				});
				
        if (data.result.status == 'SUBMITTED_FOR_SETTLEMENT' ||data.result.status == 'SETTLING') {
					 that.paymentLoader.dismiss();
							 that.paymentLoader = that.loadingCtrl.create({
													content: 'Loading...'
	 											});
						 Keyboard.close();
          this.navCtrl.push(PaymentSuccess);
        } else {
					 that.paymentLoader.dismiss();
							 that.paymentLoader = that.loadingCtrl.create({
													content: 'Loading...'
	 											});
					   Keyboard.close();
          this.navCtrl.push(PaymentFailed);
        }
      })
    });

	}
	
	  // placeOrder(nonce, price) {
		//  that.paymentLoader.dismiss();
      
    //  }

  getNonce(token) {
    return new Promise((resolve, reject) => {
      this.appContext.getNonce(token).subscribe((data) => {
        resolve(data.result);
      });

    });


  }


  closeKeyboard(){
		Keyboard.close();
		
	}
	
		getStates(id) {
				    this.appContext.getToken().then((token) => {
					return new Promise((resolve, reject) => {
							this.appContext.getStates(id,token).subscribe(data => {
								that.states = data.result;
								that.cities = [];
							//	this.disableStates = false;
								resolve(data);
							})
						})
       
      });
	
	}

	myHandlerFunction(){
		 this.gotoHome();
	}
 
 presentConfirm() {
  let alert = this.alertCtrl.create({
    title: 'Cancel transaction',
    message: 'Do you want to cancel transaction?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          	 this.gotoHome();
        }
      }
    ]
  });
  alert.present();
}
}

