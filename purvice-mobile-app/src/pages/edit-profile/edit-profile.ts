import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

import { ViewChild } from '@angular/core';

import { AppContext } from '../../providers/appContext';

import { Loader } from '../loader/loader';

import { MenuController } from 'ionic-angular';

import { Profile } from '../profile/profile';




@Component({
  selector: 'edit-profile',
  templateUrl: 'edit-profile.html'
})
export class EditProfile {


  // variable
  public currentUser: any;
  public navigation: any;
  public editProfile = false;
  public changePassword = false;
  public editUserData = {
    "id": '',
    "firstName": "",
    "lastName": "",
    "username": "",
    "password": "",
    "roles": []
  };
  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public appContext: AppContext,
    public menuCtrl: MenuController,

  ) { }

  ngOnInit() {
    this.currentUser = this.appContext.getCurrentUser();
    if (this.currentUser == null) {
      this.navigation = 'common';
    } else {
      this.navigation = 'user';
    }

    this.appContext.getCurrentUser().then((data) => {
      this.currentUser = data;
    });

  }
  // variables 
  goToProfile() {
    this.navCtrl.pop();
    //

  }


  showEditProfile() {
    this.editProfile = true;
  }

  showChangePassword() {
    this.changePassword = true;
  }

  editUser(currentUser) {
    this.editUserData.id = currentUser.id;
    this.editUserData.firstName = currentUser.firstName;
    this.editUserData.lastName = currentUser.lastName;
    this.editUserData.username = currentUser.username;
    this.editUserData.roles = currentUser.roles;
    this.appContext.getToken().then((token) => {
      this.appContext.editProfile(this.editUserData, token).subscribe(userData => {
        this.appContext.setCurrentUser(userData.result);
        this.appContext.showToast(userData.explanation);
         this.navCtrl.push(Loader);
         this.navCtrl.setRoot(Loader);
      });
    });


  }

}