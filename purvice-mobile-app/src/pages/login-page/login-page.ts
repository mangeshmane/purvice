import { Component, trigger, state, style, transition, animate, keyframes } from '@angular/core';

import { NavController } from 'ionic-angular';

import { Login } from '../../providers/login';

import { Loader } from '../loader/loader';

import { RegisterPage } from '../register-page/register-page';

import { LoginOptions } from '../login-options/login-options';

import { AppContext } from '../../providers/appContext';

import { LoadingController } from 'ionic-angular';

import { Slides } from 'ionic-angular';

import { ViewChild } from '@angular/core';

import { AlertController } from 'ionic-angular';


@Component({
  selector: 'page-login',
  templateUrl: 'login-page.html',
})
export class LoginPage {

  logoState: any = "in";
  cloudState: any = "in";
  loginState: any = "in";
  formState: any = "in";
  data: any = {};
  public loginData: boolean;
  public currentUser: any;
  navigation: any;
  tryTologin: any = true;
  loading;
  testCheckboxOpen;
  testCheckboxResult;
  forgotPasswordData : any = {};
  listingItem;
  ngOnInit() {

    this.currentUser = this.appContext.getCurrentUser();
    if (this.currentUser == null) {
      this.navigation = 'common';

    } else {
      this.navigation = 'user';

    }

     this.listingItem = this.appContext.output();
     
  }

  constructor(public navCtrl: NavController,
    public loginService: Login,
    public appContext: AppContext,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {

    this.appContext.isUserLoggedIn();
  }

  @ViewChild(Slides) slides: Slides;
  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Logging In...'
    });

    this.loading.present();

    // setTimeout(() => {
    //   this.loading.dismiss();
    // }, 2000);
  }


  doLogin(data, loginForm) {

    if (loginForm.valid) {
      if (this.tryTologin) {
        this.presentLoadingDefault();
      }

    }
    // localStorage.clear();
    this.loginService.doLoginCall(data).subscribe(userData => {
      this.tryTologin = false;
      if (userData.message == "SUCCESS") {
        this.appContext.setToken(userData.result.token);
        this.appContext.setCurrentUser(userData.result.user);
     //   this.navCtrl.push(Loader);
        this.appContext.input(this.listingItem);
        this.navCtrl.setRoot(Loader);
        this.loading.dismiss();
      } else {

        //explanation
        this.loading.dismiss();
        this.presentAlert(userData.explanation);
      }

    },
      error => console.log(this.loading.dismiss()),);
   
    // this.loginData = this.loginService.doLoginCall(data);


  }




  gotoRegistration() {

    this.navCtrl.push(RegisterPage);
  }

  gotoHome() {
    this.navCtrl.push(Loader);
  }

  // redirect to login options
  goToLoginOptions() {
    this.navCtrl.pop();
  }

  // call service to save current user
  // saveCurrentUser(user) {

  //   this.appContext.setCurrentUser(user);
  // }

  // show alert box for login errors
  presentAlert(explanation) {
    let alert = this.alertCtrl.create({
      title: explanation,
      buttons: ['Try Again']
    });
    alert.present();
  }
  // validate email address
  validateEmail(loginForm) {
    //  console.log(loginForm);
  }
   forgotPassword() {
    console.log('forgot password');
    this.showCheckbox();
  }



  showCheckbox() {
    let alert = this.alertCtrl.create();
    alert.setSubTitle('Enter the email associated with your account');

    alert.addInput({
      type: 'input',
      label: 'Alderaan',
    });
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        this.forgotPasswordData.username =  data[0];
        console.log('Checkbox data:', data[0]);
       
        this.testCheckboxOpen = false;
        this.testCheckboxResult = data;
        this.sendPassword(this.forgotPasswordData);
      }
    });
    alert.present();
  }

  sendPassword(forgotData){
    console.log('forgotData',forgotData);
           this.appContext.getToken().then((token) => {
              this.appContext.forgotPassword(forgotData,token).subscribe(userData => {
              this.appContext.showToast(userData.explanation);
              console.log(userData);
            });
        });
}




}

