
import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { AppContext } from '../../providers/appContext';

import { CardDescription } from '../card-description/card-description';

import { PaypalPayment } from '../paypal-payment/paypal-payment';

import { AlertController } from 'ionic-angular';

import { DatePicker } from '@ionic-native/date-picker';

import { Platform } from 'ionic-angular';

@Component({
  selector: 'order-summary',
  templateUrl: 'order-summary.html'
})
export class OrderSummary {
  // variables 
  listingItem;
  price;
  scheduled = false;
  navigateToFriend;
  orderUserData;
  data: any = {};
  order: any = {};
  isLoggedIN;
  date;
  orderScheduleDate;
  openDatepicker;
  userPlatform;
  userEmail;
  userData;
  iosDate;
  orderSelf = {
    "utid": "",
    "paymentTransactionId":null,
    "paymentStatus": "pending",
    "paymentMessage": "",
    "amount": null,
    "isscheduled": false,
    "orderType": "Self",
    "recipient": null
  };
  updateData = {
    "id": null,
    "emailId": ""
  };
  orderFriend =
  {
    "utid": "",
    "paymentTransactionId": null,
    "paymentStatus": "pending",
    "paymentMessage": "",
    "amount": null,
    "orderType": "Friend",
    "isscheduled": false,
    "friendName": "",
    "friendEmailId": "",
    "scheduledDate": null,
    "friendContact": 0,
    "friendMessage": "",
    "emailAttempt": "",
    "emailMessage": ""
  };

  constructor(public plt: Platform, public navCtrl: NavController, private datePicker: DatePicker, public appContext: AppContext, private alertCtrl: AlertController) {

  }
  ngOnInit() {
    this.data.isscheduled = null;
    console.log(this.plt.is, 'this.plt.is');
    if (this.plt.is('ios')) {
      this.userPlatform = 'ios';
    } else {
      this.userPlatform = 'android';
    }

     this.appContext.getCurrentUser().then((data) => {
          console.log('asdfsadfsadfsadfsadfsadfasdfasdfasdfasdf');
          console.log(data);
          this.userData = data
          this.userEmail = this.userData.emailId;
          console.log(this.userEmail);
     });

    this.data.friendMessage = `Hi,

I thought you would like this gift.`;
    this.navigateToFriend = this.appContext.setgiftFriend();
    if (this.navigateToFriend) {
      this.setSchedule(true);
    }
    this.isLoggedIN = this.appContext.isUserLoggedIn();
    this.listingItem = this.appContext.output();
    console.log(this.listingItem);
    this.price = this.appContext.getAmount();
    this.orderUserData = this.appContext.getUserData();
    if (this.orderUserData.scheduledDate) {
      this.scheduled = true;
      this.date = new Date(this.orderUserData.scheduledDate);
      var date = this.date;
      var day = date.getDate(); //Date of the month: 2 in our example
      var month = date.getMonth(); //Month of the Year: 0-based index, so 1 in our example
      var year = date.getFullYear() //Year: 2013
      var scheduleDate = year + '-' + (month + 1) + '-' + day;
      this.data.date = scheduleDate;
    }

    if (this.orderUserData) {
      this.data = this.orderUserData;
      console.log(this.date, 'orderdataq ')
      console.log(this.data.date = scheduleDate);
    }

  }
  datepicker(newDate) {
    if (this.userPlatform == 'ios') {
      this.iosopenDatePicker(newDate);
    } else {
      this.openDatePicker(newDate);
    }
  }
  goToDescription() {
    this.navCtrl.pop();
    // this.navCtrl.setRoot(CardDescription);
  }

  setSchedule(value) {
    this.scheduled = value;
  }

  addOrder(data) {
    console.log(data);
    // if(this.isLoggedIN){
    //   this.appContext.showToast('Please login first to continue...');
    // }
    if (this.navigateToFriend) {
      if (this.data.friendEmailId == undefined) {
        let alert = this.alertCtrl.create({
          title: 'Please enter email...',

          buttons: ['OK']
        });
        alert.present();
        return;

      }
      if (this.data.friendEmailId != undefined) {

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(data.friendEmailId)) {
          let alert = this.alertCtrl.create({
            title: 'Please enter valid email',

            buttons: ['OK']
          });
          alert.present();
          return;
        }
      }
    }
    if (data.date == null && (data.isscheduled == "true" || data.isscheduled == true)) {
      let alert = this.alertCtrl.create({
        title: 'Please select scheduled date',

        buttons: ['OK']
      });
      alert.present();
      return;
    }
    if (!this.navigateToFriend) {
      this.order = this.orderSelf;
      if(this.userEmail){
        this.order.recipient = this.userEmail;
      }else{
         this.order.recipient = this.data.email;
          this.appContext.getCurrentUser().then((data) => {

                console.log(this.userData = data);
                this.updateData.id = this.userData.id;
                 this.updateData.emailId =  this.data.email;;
                this.updateUserEmail(this.updateData);
          });
         
      }
     

    } else {
      this.order = this.orderFriend;
      this.order.friendName = this.data.friendName;
      this.order.friendEmailId = this.data.friendEmailId;
      this.order.friendContact = this.data.friendContact;
      this.order.friendMessage = this.data.friendMessage;
      if (data.isscheduled == true) {
        this.order.isscheduled = true;
        console.log(this.orderScheduleDate);
        this.order.scheduledDate = this.orderScheduleDate;
      } else {
        this.order.isscheduled = data.isscheduled;
      }
    }
    
    if(this.listingItem.childData != undefined && this.listingItem.valueType !="VARIABLE_VALUE"){
      this.order.utid = this.listingItem.childData[this.listingItem.utidIndex].utid;
    }else{
      this.order.utid = this.listingItem.utid;
    }
    this.order.amount = this.price;
    this.order.price = this.price;
    this.appContext.setAmount(this.price);
    this.appContext.setOrder(this.order);
    console.log(JSON.stringify(this.order));
    this.appContext.getToken().then((token) => {
      if (data.id == undefined) {
        this.order.scheduledDate = this.orderScheduleDate;
        this.appContext.createOrder(this.order, token).subscribe(userData => {
          console.log(userData);

          this.appContext.setOrderId(userData.result.id);
          this.appContext.showToast(userData.explanation);
        });
      } else {
        this.data.paymentTransactionId = null;
        this.data.scheduledDate = this.orderScheduleDate;
        this.appContext.updateOrder(this.data, token).subscribe(userData => {
          console.log(userData);
          this.data.paymentTransactionId = null;

          this.appContext.setOrderId(this.data.id);
          this.appContext.showToast(userData.explanation);
        });
      }

    });

    this.navCtrl.push(PaypalPayment);
  }

  openDatePicker(newDate) {
    console.log(newDate);
    if (newDate == undefined) {
      newDate = new Date();
    } else {
      newDate = new Date(newDate);
    }
    var today = new Date();
    var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
    this.datePicker.show({
      date: newDate,
      mode: 'date',
      minDate: tomorrow.valueOf(),
      allowOldDates: true,
      titleText: 'Select schedule date:',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    }).then(

      date => {

        var date = new Date(date);
        var day = date.getDate(); //Date of the month: 2 in our example
        var month = date.getMonth(); //Month of the Year: 0-based index, so 1 in our example
        var year = date.getFullYear() //Year: 2013
        var scheduleDate = year + '-' + (month + 1) + '-' + day;
        this.orderScheduleDate = scheduleDate;
        this.data.date = scheduleDate;

        console.log(scheduleDate, 'scheduleDate');
      },
      err => console.log('Error occurred while getting date: ', err)
      )
      ;
  }

   iosopenDatePicker(newDate) {
    console.log(newDate);
    var today = new Date();
    var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
    console.log('tomorrow',tomorrow)
    console.log('this.iosDat',this.iosDate)
    if (this.iosDate == undefined) {
      newDate = new Date();
    }else{
      newDate = this.iosDate;
    }
    console.log('newDat',newDate);
    this.datePicker.show({
      date: new Date(newDate),
      mode: 'date',
      minDate: tomorrow,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(

      date => {
        this.iosDate = date;
        console.log(date);
        var date = new Date(date);
        var day = date.getDate(); //Date of the month: 2 in our example
        var month = date.getMonth(); //Month of the Year: 0-based index, so 1 in our example
        var year = date.getFullYear() //Year: 2013
        var scheduleDate = year + '-' + (month + 1) + '-' + day;
        this.orderScheduleDate = scheduleDate;

        if(isNaN(year)){
           scheduleDate = null;
        }else{
          this.data.date = scheduleDate;
        }
        

        console.log(scheduleDate, 'scheduleDate');
      },
      err => console.log('Error occurred while getting date: ', err)
      )
      ;
  }

  updateUserEmail(data){
        this.appContext.getToken().then((token) => {
          this.appContext.updateUserEmail(data,token).subscribe((data) => {
           console.log(data);
           this.appContext.setCurrentUser(data.result);
         
          });
        });
  }


}

