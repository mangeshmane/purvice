import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

import { ViewChild } from '@angular/core';

import { LoginOptions } from '../login-options/login-options';

import { Loader } from '../loader/loader';

import { AppContext } from '../../providers/appContext';

import { PrivacyPolicy } from '../privacy-policy/privacy-policy';

import { Copyright } from '../copyright/copyright';

import { TermsOfService } from '../terms-of-service/terms-of-service';

import { Storage } from '@ionic/storage';

import { Profile } from '../profile/profile';

import { About } from '../about/about';

import { Orders } from '../orders/orders';

import { AuthProvider } from './../../providers/auth/auth';

import { GooglePlus } from 'ionic-native';

import * as _ from "lodash";
 
var that;
@Component({
  selector: 'user-navigation',
  templateUrl: 'user-navigation.html'
})
export class UserNavigation {

  currentUser: any;
  expandMenu = false;
  orders;
  userType;
  ordersCount = 0;
  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public appContext: AppContext,
    public storage: Storage,
    public authProvider: AuthProvider,
   

  ) {
    // get current user
    that = this;
    this.appContext.getCurrentUser().then((data) => {
      this.currentUser = data;
      this.userType = this.currentUser.roles[0];
    });
    //  this.presentLoadingDefault();
  }

  ngOnInit() {
    console.log("Showing the first page! navigation");
    this.getOrders();
   
  }

  // redirect to login page
  gotoLogin() {

    // this.rootPage = LoginOptions;
    this.navCtrl.push(LoginOptions);
  }
  //  go to home page
  goToHome() {
    this.navCtrl.push(Loader);
    this.navCtrl.setRoot(Loader);
  }
  // log out user
  logOut() {
    this.appContext.input(undefined);
    this.appContext.getCurrentUser().then((data) => {
      this.currentUser = data;

    if (this.currentUser != null) {
      if (this.currentUser.roles[0] == "SOCIAL_USER") {
          this.facebookLogout();
          this.googleLogout();
          this.clearStorage();
      }
      this.storage.clear().then(() => {
        console.log('Keys have been cleared');
      });
      this.clearStorage();

    }
    });

  }

  clearStorage(){
      this.storage.set('token', null);
      this.storage.set('currentUser', null);
      this.navCtrl.push(Loader);
      this.navCtrl.setRoot(Loader);
  }
  expandAbout() {

    if (this.expandMenu) {
      this.expandMenu = false;
    } else {
      this.expandMenu = true;
    }
  }

  showPrivacyPolicy() {
    this.navCtrl.push(PrivacyPolicy);
    // this.navCtrl.setRoot('PrivacyPolicy');
  }
  showCopyright() {
    this.navCtrl.push(Copyright);
  }

  showTermsOfService() {
    this.navCtrl.push(TermsOfService);
  }

  profileSettings() {
    this.navCtrl.push(Profile);
  }

  showAboutUs() {

    this.navCtrl.push(About);
  }

  showOrders() {

    this.navCtrl.push(Orders);
  }
   facebookLogout(): void {
    this.authProvider.facebookLogout().then(data=>{
      this.navCtrl.push(Loader);
      this.navCtrl.setRoot(Loader);
    });
    
  }
  googleLogout(){
   GooglePlus.logout().then(() => {
            this.navCtrl.push(Loader);
            this.navCtrl.setRoot(Loader);
        });
  }
  getOrders() {
    this.appContext.getToken().then((token) => {
      this.appContext.getCurrentUser().then((data: any) => {
        this.appContext.getOrdersCount('/order/count/' + data.id, token).subscribe(count => {
              this.ordersCount = count.result;
        });
      });
    });
  }


  // update orders count
  menuOpened(){
   this.getOrders();
  }
  
}

