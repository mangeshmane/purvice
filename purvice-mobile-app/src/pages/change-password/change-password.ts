import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

import { ViewChild } from '@angular/core';

import { AppContext } from '../../providers/appContext';

import { Loader } from '../loader/loader';

import { MenuController } from 'ionic-angular';

import { Profile } from '../profile/profile';



@Component({
  selector: 'change-password',
  templateUrl: 'change-password.html'
})
export class ChangePassword {


  // variable
  
  resetData: any = {};
  public currentUser: any;
  public navigation: any;
  public editProfile = false;
  public changePassword = false;
  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public appContext: AppContext,
    public menuCtrl: MenuController
  ) { }
 
  ngOnInit() {
 

    this.appContext.getCurrentUser().then((data) => {
      this.currentUser = data;
    });

  }
  // variables 
  goToProfile() {
   this.navCtrl.pop();
  }

showEditProfile(){
this.editProfile = true;
}

showChangePassword(){
this.changePassword = true;
}

resetPassword(resetData){
  resetData.username = this.currentUser.username;
           this.appContext.getToken().then((token) => {
              this.appContext.changePassword(resetData,token).subscribe(userData => {
              this.appContext.showToast(userData.explanation);
              this.navCtrl.push(Loader);
              this.navCtrl.setRoot(Loader);
            });
        });
 

}

}