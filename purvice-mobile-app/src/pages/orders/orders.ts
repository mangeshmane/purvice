

import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { AppContext } from '../../providers/appContext';

import { CardDescription } from '../card-description/card-description';

import { Loader } from '../loader/loader';

import { LoadingController } from 'ionic-angular';

import { OrderSummary } from '../order-summary/order-summary';

import { MenuController } from 'ionic-angular';

import { OrderDetails } from '../order-details/order-details';

import { AlertController } from 'ionic-angular';

import { LoginOptions } from '../login-options/login-options';

 

import * as _ from "lodash";
@Component({
  selector: 'user-order',
  templateUrl: 'orders.html'
})
export class Orders {
 
  // variables 
  listingItem;
  orders;
  loading;
  showCloud = false;
  constructor(public menuCtrl: MenuController,private alertCtrl: AlertController,public navCtrl: NavController, public appContext: AppContext, public loadingCtrl: LoadingController) {

  }
  ngOnInit() {
        this.orders = this.getOrders();
       
  }

  getOrders() {
    this.loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    this.loading.present();

    this.appContext.getToken().then((token) => {
       this.appContext.getCurrentUser().then((data:any) => {
               this.appContext.getOrders('/order/'+data.id , token).subscribe(userData => {
                  this.orders = userData.result;
                          if(this.orders){
                            this.orders = this.orders.reverse();
                          }
                          
                          console.log( this.orders );

                    this.orders = _.forEach(this.orders, function (order, index) {
                              // extract price 
                              var date = new Date(order.orderDate);
                              var day = date.getDate(); //Date of the month: 2 in our example
                              var month = date.getMonth(); //Month of the Year: 0-based index, so 1 in our example
                              var year = date.getFullYear() //Year: 2013
                              var scheduleDate = year+'-'+(month+1)+'-'+day;
                              order.orderDate = scheduleDate;
                            // end of extract price
                          });       
                           console.log( this.orders ); 
                  if(this.orders == null){
                    this.showCloud = true;
                  }
                  this.loading.dismiss();
                },
              error => {
                console.log('error getting order'),
                this.loading.dismiss();
                if(error.status === 401){
                  this.appContext.showToast('Session expired please login again');
                  this.navCtrl.push(LoginOptions);  
                }
              })
       });
      

    });
    
   this.menuCtrl.close();
   return this.orders;
 
  }

  deleteOrder(id) {
    console.log(id);
    this.appContext.getToken().then((token) => {
      this.appContext.deleteOrder(id,token).subscribe((data) => {
        console.log(data);
        this.getOrders();
      });
    });
  }
  goToHome() {
      this.appContext.input(undefined);
      this.navCtrl.push(Loader);
      this.navCtrl.setRoot(Loader);
  }
  goToCheckout(order) {
    if(order.paymentStatus == 'Completed'|| order.paymentStatus == 'Failed'){
      // GO TO ORDER DESCRIPTION PAGE
          this.appContext.input(order.item);
          this.appContext.setAmount(order.amount);
          this.appContext.setUserData(order);
          this.navCtrl.push(OrderDetails);  
          return;
    }
    if(order.orderType ==  "Friend"){
    this.appContext.getgiftFriend(true);
    }else{
    this.appContext.getgiftFriend(false);
    }
    this.appContext.input(order.item);
    this.appContext.setAmount(order.amount);
    this.appContext.setUserData(order);
     this.appContext.getCurrentUser().then((data) => {
          this.redirectUser(data,order.orderType);
     });
   // this.navCtrl.setRoot(OrderSummary);
  }
  redirectUser(data,ordertype){
    if(data.roles[0] == "USER" && ordertype  !=  "Friend"){
      this.appContext.setNewOrder(false);
      this.navCtrl.push(CardDescription);
    }else{
      this.appContext.setNewOrder(false);
      this.navCtrl.push(OrderSummary);
    }
    
  }
  presentConfirm(id) {
  let alert = this.alertCtrl.create({
    title: 'Confirm delete',
    message: 'Do you want to remove this order?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Remove',
        handler: () => {
          this.deleteOrder(id);
        }
      }
    ]
  });
  alert.present();
}





}

