import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

import { ViewChild } from '@angular/core';

import { AppContext } from '../../providers/appContext';

import { CardListing } from '../card-listing/card-listing';

import { AlertController } from 'ionic-angular';
  
import { OrderSummary } from '../order-summary/order-summary';

import { PaypalPayment } from '../paypal-payment/paypal-payment';

import { LoginOptions } from '../login-options/login-options';

import { Loader } from '../loader/loader';

import { Platform } from 'ionic-angular';

import * as _ from "lodash";

var that;
@Component({
  selector: 'card-description',
  templateUrl: 'card-description.html'
})
export class CardDescription {


  // variable
  public currentUser: any;
  public navigation: any;
  price;
  minValue;
  listingItem: any = {};
  public navigateToFriend;
  prices = [];
  priceIcreamentor = 0;
  priceDecreamentor = 0;
  order;
  orderUserData;
  data;
  utidIndex = 0;
  paymentLoader;
  orderStatus;
 
  constructor(public plt: Platform,public loadingCtrl: LoadingController, private alertCtrl: AlertController, public navCtrl: NavController, public appContext: AppContext) {
    that = this;
     plt.ready().then(() => {
          plt.registerBackButtonAction(() => this.myHandlerFunction());

    })
  }

  ngOnInit() {
    this.orderStatus = this.appContext.getNewOrder();
    console.log(this.orderStatus);
this.paymentLoader = this.loadingCtrl.create({
									content: 'Loading...'
	 	});
      this.order = {
    "utid": "",
    "paymentTransactionId": null,
    "paymentStatus": "pending",
    "paymentMessage": "",
    "amount": null,
    "isscheduled": false,
    "orderType": "Self",
    "recipient": null
  };
  
    this.listingItem = this.appContext.output();  
    console.log(this.listingItem);  
      _.forEach(this.listingItem.childData, function (card, index) {
        that.prices.push(card.fixedValue);
      });
       
    this.minValue = this.prices[0];
 
    if (this.listingItem.valueType == "VARIABLE_VALUE") {
      this.prices = ['5','10','25','50','100'];
      this.minValue = this.prices[0];
    }else{
      if(this.appContext.getAmount() != undefined && this.orderStatus==false)
      {
        this.minValue =  this.appContext.getAmount();
      }
    }
    this.orderUserData = this.appContext.getUserData();
    console.log('orderUserData',this.orderUserData);
    if (this.orderUserData) {
      this.data = this.orderUserData;
      
    }
    this.navigateToFriend = this.appContext.setgiftFriend();
    this.currentUser = this.appContext.getCurrentUser();
    if (this.currentUser == null) {
      this.navigation = 'common';
    } else {
      this.navigation = 'user';
    }

  }
  // variables 
  goToListingPage() {
    if(this.listingItem.redirect){
      this.appContext.input(undefined);
      this.navCtrl.push(Loader);
      this.navCtrl.setRoot(Loader);
    }else{
      this.navCtrl.pop();
    }
 
  }

 
    addPrice() {
     // debugger
    this.prices.length;
  
 
    if( this.prices.length-1 > this.priceIcreamentor){
        this.minValue = this.prices[this.priceIcreamentor + 1];
        this.utidIndex =  this.priceIcreamentor + 1;
       this.priceIcreamentor = this.priceIcreamentor+1;
    }
         
  
       
  }

  removePrice() {
    this.priceDecreamentor = this.priceIcreamentor;
    if(this.priceDecreamentor > 0)
    {
      this.minValue = this.prices[this.priceDecreamentor-1  ];
      this.utidIndex =  this.priceIcreamentor - 1;
      this.priceIcreamentor = this.priceDecreamentor;
      this.priceIcreamentor--;
    }
   
   

  }

  presentConfirm(data) {
   var redirect =  this.verifyToken();
    if(data == null){
       this.paymentLoader.dismiss();
       this.appContext.showToast('You must log in first!!!');
       this.listingItem.orderAmount = this.minValue;
       this.appContext.input(this.listingItem);
       
       this.navCtrl.push(LoginOptions);
        return;
    }
    if(data.roles[0] == "USER" && !this.navigateToFriend){
  
      this.appContext.setAmount(this.minValue);
      this.appContext.setUserData(false);
      this.addOrder(data);
    }else{
      this.appContext.setAmount(this.minValue);
      this.appContext.setUserData(false);
      this.listingItem.utidIndex = this.utidIndex;
      this.paymentLoader.dismiss();
      this.navCtrl.push(OrderSummary);
    }
      
  }

  checkoutConfirm() {
  this.paymentLoader.present();
        this.appContext.getCurrentUser().then((data) => {
           this.presentConfirm(data);
        });
    
  }


  addOrder(data) {
    this.order.recipient = data.emailId;
    this.order.amount = this.minValue;
    this.order.price = this.minValue;
    console.log(this.order);
    if(this.listingItem.childData != undefined && this.listingItem.valueType !="VARIABLE_VALUE"){
      this.order.utid = this.listingItem.childData[this.utidIndex].utid;
    }else{
      this.order.utid = this.listingItem.utid;
    }
    
    this.appContext.setAmount(this.price);
    this.appContext.setOrder(this.order);
    console.log(JSON.stringify(this.order));
    this.appContext.getToken().then((token) => {
      console.log(this.data);
      if(this.orderStatus == true){
        this.data = undefined;
      }
      if (this.data == undefined) {
        // this.order.scheduledDate = this.orderScheduleDate;
        this.appContext.createOrder(this.order, token).subscribe(userData => {
          console.log(userData);

          this.appContext.setOrderId(userData.result.id);
          this.appContext.showToast(userData.explanation);
         	this.paymentLoader.dismiss();
          this.navCtrl.push(PaypalPayment);
        });
      } else {
        this.data.paymentTransactionId = null;
        // this.data.scheduledDate = this.orderScheduleDate;
       
        this.appContext.updateOrder(this.data, token).subscribe(userData => {
          console.log(userData);
          this.data.paymentTransactionId = null;

          this.appContext.setOrderId(this.data.id);
         // this.appContext.showToast(userData.explanation);
         	this.paymentLoader.dismiss();
          this.navCtrl.push(PaypalPayment);
        });
      }

    });
 
  }

  verifyToken() {
    this.appContext.getToken().then((token) => {

      this.appContext.getCurrentUser().then((data: any) => {
        if (data != null) {


          this.appContext.getOrdersCount('/order/count/' + data.id, token).subscribe(count => {
             
          },
            error => {
              
                    this.paymentLoader.dismiss();
                    this.appContext.showToast('Session expired please login again');
                    this.listingItem.orderAmount = this.minValue;
                    this.appContext.input(this.listingItem);
                    
                    this.navCtrl.push(LoginOptions);
              
            })
        }
      });
    });
  }

  myHandlerFunction(){
    this.goToListingPage();
  }

}