 
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

 
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class SocialLogin {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SocialLogin');
  }

  googleLogin(): void {
 
  }

  facebookLogin(): void {
   
  }

}
