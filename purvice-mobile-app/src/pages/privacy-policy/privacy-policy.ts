
// import { RegistrationPage } from '../registration/registration';

import { Login } from '../../providers/login';

import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { MenuController } from 'ionic-angular';

import { LoginPage } from '../login-page/login-page';

import { Loader } from '../loader/loader';


@Component({
  selector: 'privacy-policy',
  templateUrl: 'privacy-policy.html',
})

export class PrivacyPolicy {


  constructor(
    public navCtrl: NavController,
    public loginService: Login,
    public menuCtrl: MenuController
  ) { }

  // redirect to login page
  gotoLogin() {

    this.navCtrl.push(LoginPage);
  }

  //  open navigation menu
  goToHome() {
    this.navCtrl.pop();

  }
  //  go to home page
  openNavigation() {
    // this.navCtrl.setRoot(LoginOptions);
    this.menuCtrl.open();
  }
} 