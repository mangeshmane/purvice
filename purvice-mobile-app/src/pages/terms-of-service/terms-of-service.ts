import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

import { ViewChild } from '@angular/core';

import { AppContext } from '../../providers/appContext';

import { Loader } from '../loader/loader';

@Component({
  selector: 'terms-of-service',
  templateUrl: 'terms-of-service.html'
})
export class TermsOfService {


  // variable
  public currentUser: any;
  public navigation: any;
  constructor(
              public loadingCtrl: LoadingController,
              public navCtrl: NavController,
              public appContext: AppContext
            ) { }

  ngOnInit() {
    this.currentUser = this.appContext.getCurrentUser();
    if (this.currentUser == null) {
      this.navigation = 'common';
    } else {
      this.navigation = 'user';
    }

  }
  // variables 
 
   goHome(){
      this.navCtrl.pop();
   }
 
 

}