import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Loader } from '../loader/loader';
import { Platform } from 'ionic-angular';
import { Orders } from '../orders/orders';

@Component({
  selector: 'payment-failed',
  templateUrl: 'payment-failed.html',
})
export class PaymentFailed {
  class1;
  constructor(public navCtrl: NavController,public plt: Platform) {
        plt.ready().then(() => {
          plt.registerBackButtonAction(() => this.myHandlerFunction());

        })
  }

	
ngOnInit(){
   this.class1 = 'animate';
}

reanimate(){
 this.class1 = 'animate';
}
  gotoHome() {
 
    this.navCtrl.push(Loader);
     
  }
  myOrders(){
   this.navCtrl.push(Orders);
  }
  myHandlerFunction(){
  	this.myOrders();
  }
}
 