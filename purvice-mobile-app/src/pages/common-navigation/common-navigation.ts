import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

import { ViewChild } from '@angular/core';

import { Slides } from 'ionic-angular';

import { LoginOptions } from '../login-options/login-options';

import { Loader } from '../loader/loader';

import { PrivacyPolicy } from '../privacy-policy/privacy-policy';

import { Copyright } from '../copyright/copyright';

import { TermsOfService } from '../terms-of-service/terms-of-service';

import { RegisterPage } from '../register-page/register-page';

import { AppContext } from '../../providers/appContext';

import { About } from '../about/about';
 
@Component({
  selector: 'common-navigation',
  templateUrl: 'common-navigation.html'
})
export class CommonNavigation {
 currentUser:any;
 expandMenu = false ;
  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController ,public appContext: AppContext) {
  //  this.presentLoadingDefault();
  }
  
    ngOnInit() {
   // console.log("Showing the first page!");
  }
 
// redirect to login page
  gotoLogin() {
  
      // this.rootPage = LoginOptions;
    this.navCtrl.push(LoginOptions);
    
  }
 //  go to home page
goToHome(){
    this.appContext.input(undefined);
    this.navCtrl.push(Loader);
    this.navCtrl.setRoot(Loader);
}

expandAbout(){
 if( this.expandMenu){
    this.expandMenu =  false;
 }else{
    this.expandMenu =  true;
 }
}

showPrivacyPolicy(){
this.navCtrl.push(PrivacyPolicy);
  // this.navCtrl.setRoot('PrivacyPolicy');
}
showCopyright(){
this.navCtrl.push(Copyright);
}

showTermsOfService(){
this.navCtrl.push(TermsOfService);
}

goToSignup(){
this.navCtrl.push(RegisterPage);
}
  showAboutUs() {

    this.navCtrl.push(About);
  }

}
