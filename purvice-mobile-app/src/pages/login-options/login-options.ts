
// import { RegistrationPage } from '../registration/registration';

import { Login } from '../../providers/login';

import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { MenuController } from 'ionic-angular';

import { LoginPage } from '../login-page/login-page';

import { Loader } from '../loader/loader';

import { AuthProvider } from './../../providers/auth/auth';

import { LoadingController } from 'ionic-angular';

import { GooglePlus } from 'ionic-native';

import { AppContext } from '../../providers/appContext';

@Component({
  selector: 'page-login-options',
  templateUrl: 'login-options.html',
})

export class LoginOptions {

  listingItem;
    ngOnInit() {
        this.listingItem = this.appContext.output();
    }
 
  constructor(
    public navCtrl: NavController,
    public loginService: Login,
    public menuCtrl: MenuController,
    public authProvider: AuthProvider,
    public loadingCtrl: LoadingController,
    public appContext: AppContext
    
  ) { }

  // redirect to login page
  gotoLogin() {

    this.navCtrl.push(LoginPage);
  }

  //  open navigation menu
  goToHome() {
    this.navCtrl.pop();
  }
  glogin() {
     let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    GooglePlus.login({})
      .then(res => {
         loading.present();
        console.log(JSON.stringify(res));
        this.authProvider.localGoogleLogin(res).then(data => {
          loading.dismiss();
          this.navCtrl.push(Loader);
          this.navCtrl.setRoot(Loader);

        })

      })
      .catch(err => { console.error(err);  alert('Please try again.');
    
      });
 
  }
  facebookLogin(): void {
    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });

    loading.present();

    this.authProvider.facebookLogin().then(data => {
      this.appContext.input(this.listingItem);
      this.navCtrl.push(Loader);
      this.navCtrl.setRoot(Loader);
      loading.dismiss();
    })

  }

  facebookLogout(): void {
    this.authProvider.facebookLogout().then(data => {
      this.appContext.input(this.listingItem);
      this.navCtrl.push(Loader);
      this.navCtrl.setRoot(Loader);
    });

  }

} 