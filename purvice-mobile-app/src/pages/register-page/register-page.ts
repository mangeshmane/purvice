import { Component, trigger, state, style, transition, animate, keyframes } from '@angular/core';

import { NavController } from 'ionic-angular';

// import { RegistrationPage } from '../registration/registration';

import { Login } from '../../providers/login';

import { Loader } from '../loader/loader';

import { LoginPage } from '../login-page/login-page';

import { LoginOptions } from '../login-options/login-options';

import { AppContext } from '../../providers/appContext';

import { AlertController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-register',
  templateUrl: 'register-page.html',

  animations: [

    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({transform: 'translate3d(0,2000px,0'}),
        animate('2000ms ease-in-out')
      ])
    ]),

    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({transform: 'translate3d(0,2000px,0)'}),
        animate('1000ms ease-in-out')
      ])
    ]),

    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({transform: 'translate3d(0,2000px,0)', offset: 0}),
          style({transform: 'translate3d(0,-20px,0)', offset: 0.9}),
          style({transform: 'translate3d(0,0,0)', offset: 1}) 
        ]))
      ])
    ]),

    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({opacity: 0}),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})
export class RegisterPage {

  logoState: any = "in";
  cloudState: any = "in";
  loginState: any = "in";
  formState: any = "in";
  data: any = {};

  ngOnInit() {
      //  console.log("Showing the first page!");
  }Loader

  constructor(
              public navCtrl: NavController,
              public loginService: Login,
              public appContext: AppContext,
              private alertCtrl: AlertController,
              public loadingCtrl: LoadingController,
             ) { }

  doLogin(data){
      this.loginService.doLoginCall(data);
  }

  createUser(data,form){

    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    loading.present();
    
        this.loginService.register(data).subscribe(registerData => {

      if (registerData.message=="SUCCESS") {
        // this.appContext.setToken(userData.result.token);
       
        this.presentAlert(registerData.explanation, 'Login');
          this.navCtrl.push(LoginPage);
          loading.dismiss();
        
      } else {
       this.presentAlert(registerData.explanation,'Try Again');
       loading.dismiss();
      }

    });
  }

 

  gotoHome(){
    this.navCtrl.push(Loader);
  }

 // redirect to login page
  gotoLogin() {
   
    this.navCtrl.push(LoginOptions);
  }

  // redirect to login options
  goToLoginOptions(){
    this.navCtrl.pop();
  }

  // show alert box for registration errors
  presentAlert(explanation, buttonName) {
  let alert = this.alertCtrl.create({
    title: explanation,
    buttons: [buttonName]
  });
  alert.present();
  }
}
