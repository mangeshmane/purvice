
import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { AppContext } from '../../providers/appContext';

import { CardDescription } from '../card-description/card-description';

import { PaypalPayment } from '../paypal-payment/paypal-payment';

import { AlertController } from 'ionic-angular';

import { BrowserTab } from '@ionic-native/browser-tab';

import * as _ from "lodash";

@Component({
  selector: 'order-details',
  templateUrl: 'order-details.html'
})
export class OrderDetails {
  // variables 
  listingItem;
  orderPrice;
  date;
  codeName;
  redemptionUrl;
  orderUserData;
  data: any = {};
  scheduled = false;
  scheduleDate ;
  url = '    ';
  collapseExpand = false;



  constructor(private browserTab: BrowserTab,public navCtrl: NavController, public appContext: AppContext, private alertCtrl: AlertController) {
 
  }
  ngOnInit() {

    this.listingItem = this.appContext.output();
    this.orderPrice = this.appContext.getAmount();
    this.orderUserData = this.appContext.getUserData();
    this.orderUserData.item =  this.extractPrice(this.orderUserData.item);
    if (this.orderUserData) {
      this.data = this.orderUserData;
      console.log(this.data);
      this.date = new Date(this.data.scheduledDate);
       this.date= this.date.toDateString();
        console.log('date',this.date)
        if(this.data.scheduledDate){
            this.scheduled = true;
            this.scheduleDate = this.date;
      }   
      
    }
     this.data.redemptionDetails = this.data.claimCode;
     if (_.includes(this.data.claimCode, "URL")) {            
            this.codeName =this.data.claimCode.substring(0, this.data.claimCode.indexOf("http"));
            let url = this.data.claimCode.substring(this.data.claimCode.indexOf(":") + 2);
            this.redemptionUrl = url.substring(0, url.indexOf(" "));
            url = url.substring(url.indexOf(" "));
            this.url = url;
            let urlName = this.redemptionUrl.length > 40 ? this.redemptionUrl.substring(0, 40) + '...' : this.redemptionUrl;            
            this.data.redemptionDetails = this.codeName + ' '  +urlName ;
           
          }
            
    console.log(this.hasWhiteSpace(this.url));      
  }
    extractPrice(card) {
    if (card.minValue == null) {
      var value = card.rewardName.substr(card.rewardName.indexOf("$") + 1);
      card.rewardName = card.rewardName.substring(0, card.rewardName.indexOf('$'));
      card.fixedValue = parseInt(value);
      return card;
    }
    else
      return card;
  }
  back() {
    this.navCtrl.pop();
    // this.navCtrl.setRoot(CardDescription);
  }

  Iframe(){
    this.browserTab.isAvailable()
        .then((isAvailable: boolean) => {

        if(isAvailable) {
            
            this.browserTab.openUrl(this.redemptionUrl);

        } else {

            // if custom tabs are not available you may  use InAppBrowser

        }

        });        
}

 hasWhiteSpace(s) {
  return /^\s+$/.test(s);
}
toggleDetails(){
  if(this.collapseExpand){
    this.collapseExpand = false;
  }else{
    this.collapseExpand = true;
  }

}

 

  
}



