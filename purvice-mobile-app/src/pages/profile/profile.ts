import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

import { ViewChild } from '@angular/core';

import { AppContext } from '../../providers/appContext';

import { Loader } from '../loader/loader';

import { MenuController } from 'ionic-angular';

import { EditProfile } from '../edit-profile/edit-profile';

import { ChangePassword } from '../change-password/change-password';


@Component({
  selector: 'user-profile',
  templateUrl: 'profile.html'
})
export class Profile {


  // variable
  public currentUser: any;
  public navigation: any;
  userType;
  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public appContext: AppContext,
    public menuCtrl: MenuController,
  ) { }

  ngOnInit() {
    this.currentUser = this.appContext.getCurrentUser();
    if (this.currentUser == null) {
      this.navigation = 'common';
    } else {
      this.navigation = 'user';
    }

    this.appContext.getCurrentUser().then((data) => {
      this.currentUser = data;
      this.userType = this.currentUser.roles[0];
    });

  }
  // variables 
  goToHome() {
    this.navCtrl.pop();
  }



  editProfile() {
    this.navCtrl.push(EditProfile);
    
  }
  changePassword() {
    this.navCtrl.push(ChangePassword);
   
  }
}