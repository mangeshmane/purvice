import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

import { ViewChild } from '@angular/core';

import { Slides } from 'ionic-angular';

import { LoginOptions } from '../login-options/login-options';

import { AppContext } from '../../providers/appContext';

import { CardListing } from '../card-listing/card-listing';

import { MenuController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { FacebookLoginPage } from '../facebook-login-page/facebook-login-page';

import { DatePicker } from '@ionic-native/date-picker';

import { CardDescription } from '../card-description/card-description';

import { Platform } from 'ionic-angular';

@Component({
  selector: 'page-loader',
  templateUrl: 'loader.html'
})
export class Loader {

  // variable
  public currentUser: any;
  public navigation: any;
  slideMargin;
  tab1;
  tab2;
  iosClass;
  listingItem: any = {};
  constructor(
    public plt: Platform,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public appContext: AppContext,
    public menuCtrl: MenuController,
    public storage: Storage,
    private datePicker: DatePicker, 
  ) {
    //this.presentLoadingDefault();
    this.tab1 = Loader;
    this.tab2 = Loader;
  }

  ngOnInit() {
        this.verifyToken();
        if (this.plt.is('ios')) {
          this.iosClass = 'ios';
        } else {
          this.iosClass = 'android';
        }
   
 
           this.redirectToCard();
      
      
    if (this.navCtrl.canGoBack()) {
      this.slideMargin = 'slider-main';
    }
    this.appContext.getCurrentUser().then((data) => {
      this.currentUser = data;
      if (this.currentUser == null) {
        this.navigation = 'common';
      } else {
        this.navigation = 'user';
      }
    });
  }
  @ViewChild(Slides) slides: Slides;
  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
    }, 300);
  }

  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <div class="custom-spinner-container">
        <div class="custom-spinner-box"></div>
      </div>`,
      duration: 5000
    });

    loading.onDidDismiss(() => {

    });

    loading.present();
  }


  // redirect to login page
  gotoLogin() {
    this.navCtrl.push(LoginOptions);
  }
  //  go to home page
  goToHome() {
    this.navCtrl.push(Loader);
  }

  // goToFrndListingPage() {
  //   this.appContext.getgiftFriend(true);
  //   this.navCtrl.push(CardListingFriend);
  //   this.navCtrl.setRoot(CardListingFriend);
  // }

  goToListingPage(status) {
    this.appContext.getgiftFriend(status);
    this.appContext.setNewOrder(true);
    this.navCtrl.push(CardListing);
  }

  goHome(){
    this.navCtrl.push(FacebookLoginPage);
    this.navCtrl.setRoot(FacebookLoginPage);
  }

  openMenu() {
    console.log('called');
    this.menuCtrl.open();
  }
  openDatepicker(){
    var data = 'Wed Aug 28 2024 13:06:00 GMT+0530 (IST)';
    console.log(new Date(data));
    this.datePicker.show({
      date: new Date(),
       
      minDate: new Date,
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => console.log('Got date: ', date),
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  redirectToCard(){
 this.listingItem = this.appContext.output();  
    if(this.listingItem){
            
            console.log('called');
            this.listingItem.redirect = true;
            this.appContext.input(this.listingItem);
            this.navCtrl.push(CardDescription);  
    }     
  }

     verifyToken() {
    this.appContext.getToken().then((token) => {
     
      this.appContext.getCurrentUser().then((data: any) => {
         if(data != null){
        
      
        this.appContext.getOrdersCount('/order/count/' + data.id, token).subscribe(count => {
            
        },
        error => {
          if(error.status === 401){
            this.appContext.showToast('Session expired please login again');
            this.navCtrl.push(LoginOptions);  
          }
        })
     } });
    });
  }
  
}
