import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';


import { AppContext } from '../../providers/appContext';

import { Loader } from '../loader/loader';


import { MenuController } from 'ionic-angular';




@Component({
  selector: 'copyright-page',
  templateUrl: 'copyright.html'
})
export class Copyright {


  // variable
  public currentUser: any;
  public navigation: any;
  constructor(
              public loadingCtrl: LoadingController,
              public navCtrl: NavController,
              public appContext: AppContext,
              public menuCtrl: MenuController
            ) { }

  ngOnInit() {
    this.currentUser = this.appContext.getCurrentUser();
    if (this.currentUser == null) {
      this.navigation = 'common';
    } else {
      this.navigation = 'user';
    }

  }
  // variables 
 goToHome() {
     this.navCtrl.pop();
 }

 
 

}