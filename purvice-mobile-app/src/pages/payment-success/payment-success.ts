import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Loader } from '../loader/loader';
import { Orders } from '../orders/orders';
import { Platform } from 'ionic-angular';

@Component({
  selector: 'payment-success',
  templateUrl: 'payment-success.html',
    
})
export class PaymentSuccess {
  class1;
  constructor(public navCtrl: NavController,public plt: Platform) {
    		plt.ready().then(() => {
          plt.registerBackButtonAction(() => this.myHandlerFunction());

        })

  }


  ngOnInit() {
    this.class1 = 'animate';
  }

  reanimate() {
    this.class1 = 'animate';
  }
  gotoHome() {
    this.navCtrl.push(Loader);
    this.navCtrl.setRoot(Loader);
  }
  myOrders(){
   this.navCtrl.push(Orders);
  }
  myHandlerFunction(){
			this.myOrders();
  }

  
}

