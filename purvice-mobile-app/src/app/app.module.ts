import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { Facebook } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';
import { SplashScreen} from "@ionic-native/splash-screen";
import { StatusBar} from "@ionic-native/status-bar";
import { FacebookLoginPage } from '../pages/facebook-login-page/facebook-login-page';
import { UserPage } from '../pages/user/user';
import { BrowserModule } from '@angular/platform-browser';



 
import { ErrorHandler } from '@angular/core';
import { IonicErrorHandler } from 'ionic-angular';
import { HomePage } from '../pages/home/home'; 
import { SocialLogin } from '../pages/login/login';
import { DatePicker } from '@ionic-native/date-picker';
import { Network } from '@ionic-native/network';
import { BrowserTab } from '@ionic-native/browser-tab';


// purvice


import { LoginPage } from '../pages/login-page/login-page';

// import { RegistrationPage } from '../pages/registration/registration';

import { Login } from '../providers/login';

import { AppContext } from '../providers/appContext';

import { Loader } from '../pages/loader/loader';

import { LoginOptions } from '../pages/login-options/login-options';

import { RegisterPage } from '../pages/register-page/register-page';

import { UserNavigation } from '../pages/user-navigation/user-navigation';

import { CommonNavigation } from '../pages/common-navigation/common-navigation';

import { CardListing } from '../pages/card-listing/card-listing';

import { CardDescription } from '../pages/card-description/card-description';

 

import { PrivacyPolicy } from '../pages/privacy-policy/privacy-policy';

import { TermsOfService } from '../pages/terms-of-service/terms-of-service';

import { Copyright } from '../pages/copyright/copyright';
 

import { IonicStorageModule } from '@ionic/storage';

import { About } from '../pages/about/about';

import { EditProfile } from '../pages/edit-profile/edit-profile';

import { ChangePassword } from '../pages/change-password/change-password';

import { HttpModule, RequestOptions, XHRBackend } from '@angular/http';

import { OrderSummary } from '../pages/order-summary/order-summary';
 
import { PaypalPayment } from '../pages/paypal-payment/paypal-payment';

import { Orders } from '../pages/orders/orders';

import { Profile } from '../pages/profile/profile';

import { PaymentFailed } from '../pages/payment-failed/payment-failed';

import { PaymentSuccess } from '../pages/payment-success/payment-success';

import { OrderDetails } from '../pages/order-details/order-details';

import { AuthProvider } from '../providers/auth/auth';
         
export const firebaseConfig = {
        apiKey: "AIzaSyCyPJ8TCmDZgPxqQczme9YziFlD_B0gMic",
        authDomain: "purvice-c4244.firebaseapp.com",
        databaseURL: "https://purvice-c4244.firebaseio.com",
        projectId: "purvice-c4244",
        storageBucket: "purvice-c4244.appspot.com",
        messagingSenderId: "1088182381079"
}

@NgModule({
  declarations: [
    MyApp,
    FacebookLoginPage,
    UserPage,

    HomePage,
    MyApp,
    LoginPage,
    Loader,
    RegisterPage,
    LoginOptions,
    UserNavigation,
    CommonNavigation,
    CardListing,
    CardDescription,
    HomePage,
 
    PrivacyPolicy,
    TermsOfService,
    About,
    Copyright,
    EditProfile,
    ChangePassword,
    OrderSummary,
    PaypalPayment,
    Orders,
    SocialLogin,
    Profile,
    PaymentFailed,
    PaymentSuccess,
    OrderDetails
  ],
  imports: [
    BrowserModule,
   
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FacebookLoginPage,
    UserPage,
       MyApp,
    HomePage,
     MyApp,
    Loader,
    LoginPage,
    RegisterPage,
    LoginOptions,
    UserNavigation,
    CommonNavigation,
    CardListing,
    CardDescription,
    HomePage,
 
    PrivacyPolicy,
    TermsOfService,
    About,
    Copyright,
    EditProfile,
    ChangePassword,
    OrderSummary,
    PaypalPayment,
    Orders,
    SocialLogin,
    Profile,
    PaymentFailed,
    PaymentSuccess,
    OrderDetails
  ],
  providers: [
    BrowserTab,
    Network,
    AuthProvider,
    StatusBar,
    SplashScreen,
    Facebook,
    NativeStorage,
  
    Login,
    AppContext,
    DatePicker,
    HttpModule
  ]
})
export class AppModule {}
