import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { SplashScreen} from "@ionic-native/splash-screen";
import { StatusBar} from "@ionic-native/status-bar";
import { FacebookLoginPage } from '../pages/facebook-login-page/facebook-login-page';
import { UserPage } from '../pages/user/user';
import { SocialLogin } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
// purvice 

import { Loader } from '../pages/loader/loader';

import { LoginPage } from '../pages/login-page/login-page';

import { LoginOptions } from '../pages/login-options/login-options';

import { RegisterPage } from '../pages/register-page/register-page';

import { UserNavigation } from '../pages/user-navigation/user-navigation';

import { CommonNavigation } from '../pages/common-navigation/common-navigation';

import { CardListing } from '../pages/card-listing/card-listing';

import { CardDescription } from '../pages/card-description/card-description';

 

import { PrivacyPolicy } from '../pages/privacy-policy/privacy-policy';

import { TermsOfService } from '../pages/terms-of-service/terms-of-service';

import { Copyright } from '../pages/copyright/copyright';

import { About } from '../pages/about/about';

 

import { EditProfile } from '../pages/edit-profile/edit-profile';

import { ChangePassword } from '../pages/change-password/change-password';

import { OrderSummary } from '../pages/order-summary/order-summary';

import { PaypalPayment } from '../pages/paypal-payment/paypal-payment';

import { Orders } from '../pages/orders/orders';

import { PaymentFailed } from '../pages/payment-failed/payment-failed';

import { PaymentSuccess } from '../pages/payment-success/payment-success';

import {MenuController } from 'ionic-angular';

import { OrderDetails } from '../pages/order-details/order-details';

@Component({
  template: `<ion-nav [root]="rootPage"></ion-nav>`
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;
  rootPage: any;

  constructor(
    platform: Platform,
    public nativeStorage: NativeStorage,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar
  ) {
    platform.ready().then(() => {
      this.rootPage = Loader;
      // Here we will check if the user is already logged in
      // because we don't want to ask users to log in each time they open the app
      let env = this;
      this.nativeStorage.getItem('user')
      .then( function (data) {
        // user is previously logged and we have his data
        // we will let him access the app
            this.navCtrl.push(Loader);
           // this.navCtrl.setRoot(Loader);
        env.splashScreen.hide();
      }, function (error) {
        //we don't have the user data so we will ask him to log in
         // this.navCtrl.push(Loader);
        //  this.navCtrl.setRoot(Loader);
        env.splashScreen.hide();
      });

      this.statusBar.styleDefault();
    });
  }
}
