import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { GlobalVariable } from '../global/global';

/*
  Generated class for the Login provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Login {

  // variables
  response: any;
  constructor(public http: Http) {
    console.log('Hello Login Provider');
  }
  // mock login

  doLoginCall(data) {
    var link = GlobalVariable.BASE_API_URL + '/authenticate';
    var postData = JSON.stringify(data);

    //    return this.response;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    //headers.append('authentication', `test`);
    let options = new RequestOptions({ headers: headers });
    this.response = this.http.post(link, data, options)
      .map((res: Response) => res.json());
    return this.response;
  }


  register(data) {
    var link = GlobalVariable.BASE_API_URL + '/register';
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    //headers.append('authentication', `test`);
    let options = new RequestOptions({ headers: headers });
    this.response = this.http.post(link, data, options)
      .map((res: Response) => res.json());
    return this.response;
  }
}

