// import { Facebook } from '@ionic-native/facebook';
 
 
import { Injectable } from '@angular/core';

import { AppContext } from '../appContext';
import { Loader } from '../../pages/loader/loader';
import { NavController } from 'ionic-angular';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { GlobalVariable } from '../../global/global';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
var that; 
@Injectable()
export class AuthProvider {
 public navCtrl: NavController;
 facebookUser: any = [];
 fullName;
 user = {
   "firstName": "",
   "lastName": "",
   "username": "",
   "password": "",
   "isVerified": true,
   "uniqueId": null,
   "emailId": null,
   "verify":true,
 };
 constructor(public http: Http,  
   public facebook: Facebook, public appContext: AppContext, private fb: Facebook) {
     that = this;
    }




   facebookLogin(): Promise<any> {
           return new Promise((resolve, reject) => {
                   let permissions = new Array<string>();
                   let nav = this.navCtrl;
                   let env = this;
                   //the permissions your facebook app needs from the user
                   permissions = ["public_profile"];


                   this.fb.login(permissions)
                     .then(function (response) {
                       let userId = response.authResponse.userID;
                       let params = new Array<string>();

                       //Getting name and gender properties
                       env.fb.api("/me?fields=name,gender,email", params)
                         .then(function (success) {
                           success.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
                           //now we have the users info, let's save it in the NativeStorage
                           console.log(success);
                           that.localFacebookLogin(success).subscribe((data) => {
                                 if (data.message == "SUCCESS") {
                                   that.appContext.setToken(data.result.token);
                                   console.log('facebook token', data.result.token);
                                   that.appContext.setCurrentUser(data.result.user);
                                 }
                                 if (data.message == "FAIL") {
                                   alert('Can not login with this account');
                                 }
                                 resolve(success);
                               });


                         })
                     }, function (error) {
                       console.log(error);
                     });
   })
 }



 facebookLogout() {
   return new Promise((resolve, reject) => {
     this.facebook.logout().then((response) => {
       console.log(response);
     });
   });
 }


 localFacebookLogin(success) {
   console.log(success);
   this.fullName = success.name.split(" ")
   this.user.firstName = this.fullName[0];
   this.user.lastName = this.fullName[1];
   this.user.username = success.id;
   this.user.password = success.id;
   this.user.uniqueId = success.id;
   this.user.emailId = success.email;
 

   let headers = new Headers();
   headers.append('Content-Type', 'application/json');
   headers.append('Access-Control-Allow-Origin', '*');
   let options = new RequestOptions({ headers: headers });
   return this.http.post(GlobalVariable.BASE_API_URL + '/login', JSON.stringify(this.user), options)
     .map((res: Response) => {
      
       return res.json()
     });

 }

 testPush() {
   this.navCtrl.push(Loader);
   this.navCtrl.setRoot(Loader);
 }

 socialLogin(user) {
   let headers = new Headers();
   headers.append('Content-Type', 'application/json');
   headers.append('Access-Control-Allow-Origin', '*');
   let options = new RequestOptions({ headers: headers });
   return this.http.post(GlobalVariable.BASE_API_URL + '/login', JSON.stringify(user), options)
     .map((res: Response) => {
    
       return res.json()
     });

 }

 setGoogleUser(success) {
   console.log(success);
   this.fullName = success.displayName.split(" ")
   this.user.firstName = this.fullName[0];
   this.user.lastName = this.fullName[1];
   this.user.username = success.userId;
   this.user.password = 'null';
   this.user.emailId = success.email;
   this.user.uniqueId = success.userId;
   console.log(JSON.stringify(this.user));

   let headers = new Headers();
   headers.append('Content-Type', 'application/json');
   headers.append('Access-Control-Allow-Origin', '*');
   let options = new RequestOptions({ headers: headers });
   return this.http.post(GlobalVariable.BASE_API_URL + '/login', JSON.stringify(this.user), options)
     .map((res: Response) => {
       console.log(res, 'login responce');
       console.log(res.json(), 'login responce 2');

       return res.json()
     });

 }


 localGoogleLogin(success) {
   console.log(success,'success');
   return new Promise((resolve, reject) => {
     this.setGoogleUser(success).subscribe((data) => {
       if (data.message == "SUCCESS") {
         that.appContext.setToken(data.result.token);
         console.log('facebook token', data.result.token);
         that.appContext.setCurrentUser(data.result.user);
       }
       if (data.message == "FAIL") {
         alert('Can not login with this account');
       }
       resolve(data);
     });
   });
 }
}



