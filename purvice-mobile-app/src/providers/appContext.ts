import { Injectable } from '@angular/core';

import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { GlobalVariable } from '../global/global';

import 'rxjs/add/operator/map';

import { Storage } from '@ionic/storage';

import { LoadingController } from 'ionic-angular';

import { ToastController } from 'ionic-angular';

/*
  Generated class for the Login provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AppContext {

    // variables 

    currentUser: any;
    isLoggedIn: any;
    public sharedData: any;
    response: any;
    Authorization: any;
    token: any;
    loading: any;
    showFriend;
    amountData;
    isNew;
    public userData : any;
    public order : any;
    public orderId : any;

    constructor(public http: Http, public storage: Storage, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
        //  console.log('Hello Login Provider');
        let headers = new Headers();
        // headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
    }

    setToken(token) {

        // save token in localstorage
        this.storage.set('token', token);

    }

    getToken() {
        return new Promise((resolve, reject) => {
            this.storage.get('token').then((val) => {
                this.token = val;
                resolve(this.token);
            });
        });


    }
    setNewOrder(isNew){
        this.isNew = isNew;
    }
    getNewOrder(){
        return this.isNew;
    }
    setCurrentUser(user) {
        console.log(user);
        this.storage.set('currentUser', user);
    }

    logoutUser() {

        this.storage.set('token', null);
        this.storage.set('currentUser', null);
    }

    getCurrentUser() {
        // get current user from local storage
        return new Promise((resolve, reject) => {
            this.storage.get('currentUser').then((val) => {
                this.currentUser = val;
                resolve(this.currentUser);
            });
        });
    }

    isUserLoggedIn() {
        // check if user is logged in 
        this.storage.get('token').then((val) => {
            this.isLoggedIn = true;
        });
        if (this.isLoggedIn) {
            this.isLoggedIn = true;
        } else {
            this.isLoggedIn = false;
        }
        return this.isLoggedIn;
    }

    // set single listing item 
    input(data) {
        this.sharedData = data;
    }



    // return single listing item 
    output() {
        return this.sharedData;
    }

        // set single listing item 
    setOrderId(orderId) {
        this.orderId = orderId;
    } 



    // return single listing item 
    getOrderId() {
        return this.orderId;
    }
    
    // set order
    setOrder(order) {
        this.order = order;
    }



    // return order
    getOrder() {
        return this.order;
    }
    // set setAmount
    setAmount(data) {
        this.amountData = data;
    }

    // return getAmount
    getAmount() {
        return this.amountData;
    }
    // 
    getgiftFriend(data) {
        this.showFriend = data;
    }
    setgiftFriend() {
        return this.showFriend;
    }


    setUserData(data){
          this.userData = data;
    }

    getUserData(){
          return this.userData;
    }

    getRequest(link) {
        let headers = new Headers();
        this.getToken().then((data) => {
            this.Authorization = data;
        });
        headers.append('Authorization', 'Bearer ' + this.Authorization);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        //headers.append('authentication', `test`);
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.get(GlobalVariable.BASE_API_URL + link, options)
            .map((res: Response) => {
                return res.json()
            });
        return this.response;
    }

     getOrders(link,token) {
        let headers = new Headers();
        
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        //headers.append('authentication', `test`);
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.get(GlobalVariable.BASE_API_URL + link, options)
            .map((res: Response) => {
                return res.json()
            });
        return this.response;
    }

    



    // loader 

    editProfile(ProfileData, token) {
        var link = GlobalVariable.BASE_API_URL + '/user/update';
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.put(link, ProfileData, options)
            .map((res: Response) => res.json());
        console.log('this.response', this.response);
        return this.response;
    }

    changePassword(resetData, token) {
        console.log(JSON.stringify(resetData));
        var link = GlobalVariable.BASE_API_URL + '/user/changePassword';
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.post(link, resetData, options)
            .map((res: Response) => res.json());
        console.log('this.response', this.response);
        return this.response;
    }

    forgotPassword(email, token) {
        console.log(JSON.stringify(email));
        var link = GlobalVariable.BASE_API_URL + '/forgot';
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.post(link, email, options)
            .map((res: Response) => res.json());
        console.log('this.response', this.response);
        return this.response;
    }

    createOrder(order, token){
        var link = GlobalVariable.BASE_API_URL + '/order/create';
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.post(link, order, options)
            .map((res: Response) => res.json());
        console.log('this.response', this.response);
        return this.response;
    }

        updateOrder(order, token){
        var link = GlobalVariable.BASE_API_URL + '/order/update';
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.put(link, order, options)
            .map((res: Response) => res.json());
        console.log('this.response', this.response);
        return this.response;
    }

    
    deleteOrder(id,token){
        var link = GlobalVariable.BASE_API_URL+'/order/'+id;
         let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token);
        // headers.append('Content-Type', 'application/json');
        // headers.append('Access-Control-Allow-Origin', '*');
       
      let options = new RequestOptions({ headers: headers });
        this.response = this.http.put(link,{}, options)
            .map((res: Response) => res.json());
        console.log('this.response', this.response);
        return this.response;
    
    }

    

    showToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }

    // get payment status
      getPaymentStatus(nounce, price,token,amount) {
         
        var link = GlobalVariable.BASE_API_URL + '/braintree/checkouts?payment_method_nonce=' + nounce + '&amount=' + price + '&postalcode=' + amount ;
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.post(link,{}, options)
            .map((res: Response) => res.json());
        console.log('this.response', this.response);
        return this.response;
    }

 
    getNonce(token) {
        let headers = new Headers();
 
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        //headers.append('authentication', `test`);
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.get(GlobalVariable.BASE_API_URL + '/braintree/getClientToken', options)
            .map((res: Response) => {
                return res.json()
            });
        return this.response;
    }


    updateOrderStatus(order,token){
        console.log(order);
        var link = GlobalVariable.BASE_API_URL + '/order/update';
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.put(link,order, options)
            .map((res: Response) => res.json());
        console.log('this.response', this.response);
        return this.response;
    }

        updateUserEmail(data,token){
        
        var link = GlobalVariable.BASE_API_URL + '/user/update-email';
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.put(link,data, options)
            .map((res: Response) => res.json());
        console.log('this.response', this.response);
        
        return this.response;
    }

    //   getStates(id) {
    // return this.http.get(environment.baseUrl + 'billing-address/states/' + id)
    //   .map((res: Response) => {
    //     return res.json()
    //   })
    // }

    getStates(id,token) {
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        //headers.append('authentication', `test`);
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.get(GlobalVariable.BASE_API_URL + '/billing-address/states/', options)
            .map((res: Response) => {
                return res.json()
            });
        return this.response;
    }

       billingAddress(data,token) {   
        var link = GlobalVariable.BASE_API_URL + '/billing-address/create';
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.post(link, data, options)
            .map((res: Response) => res.json());
        console.log('this.response', this.response);
        return this.response;
    }

    getAddress(id,token){

                let headers = new Headers();
                headers.append('Authorization', 'Bearer ' + token);
                headers.append('Content-Type', 'application/json');
                headers.append('Access-Control-Allow-Origin', '*');
                //headers.append('authentication', `test`);
                let options = new RequestOptions({ headers: headers });
                this.response = this.http.get(GlobalVariable.BASE_API_URL + '/billing-address/'+id,options)
                    .map((res: Response) => {
                        return res.json()
                    });
                return this.response;

    }

     getOrdersCount(link,token) {
        let headers = new Headers();
        
        headers.append('Authorization', 'Bearer ' + token);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        //headers.append('authentication', `test`);
        let options = new RequestOptions({ headers: headers });
        this.response = this.http.get(GlobalVariable.BASE_API_URL + link, options)
            .map((res: Response) => {
                return res.json()
            });
        return this.response;
    }
    

    

 
}