package com.giftcard.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

public class ItemRowMapper implements RowMapper<Item> {

	@Override
	public Item mapRow(ResultSet rs, int rowNum) throws SQLException {
		Map<Long, Item> map = new HashMap<Long, Item>();
		Long id = rs.getLong("brand_id");
		Item item = new Item();
		Brand brand = null;
		item.setId(rs.getLong("id"));
		item.setRewardName(rs.getString("rewardName"));
		item.setCreatedDate(rs.getString("createdDate"));
		item.setCurrencyCode(rs.getString("currencyCode"));
		item.setBrand(brand);
		item.setLastUpdateDate(rs.getString("lastUpdateDate"));
		item.setMaxValue(rs.getString("maxValue"));
		item.setMinValue(rs.getString("minValue"));
		item.setRewardType(rs.getString("rewardType"));
		item.setStatus(rs.getString("status"));
		item.setUtid(rs.getString("utid"));
		item.setValueType(rs.getString("valueType"));
		return item;
	}

}
