package com.giftcard.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class DealCronJob {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="cron_deal_count")
	private Long cronDealCount;

	@Column(name="save_deal_count")
	private Long saveDealCount;

	@Column(name="update_deal_count")
	private Long updateDealCount;

	@Column(name="created_date",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date createdDate;

	private String author;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCronDealCount() {
		return cronDealCount;
	}

	public void setCronDealCount(Long cronDealCount) {
		this.cronDealCount = cronDealCount;
	}

	public Long getSaveDealCount() {
		return saveDealCount;
	}

	public void setSaveDealCount(Long saveDealCount) {
		this.saveDealCount = saveDealCount;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Long getUpdateDealCount() {
		return updateDealCount;
	}

	public void setUpdateDealCount(Long updateDealCount) {
		this.updateDealCount = updateDealCount;
	}

	@Override
	public String toString() {
		return "DealCronJob [id=" + id + ", cronDealCount=" + cronDealCount + ", saveDealCount=" + saveDealCount
				+ ", updateDealCount=" + updateDealCount + ", createdDate=" + createdDate + ", author=" + author + "]";
	}


}
