package com.giftcard.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.realexpayments.remote.sdk.domain.payment.PaymentResponse;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Orders {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JsonIgnore
	@JoinColumn(name = "user_id")
	private User user;

	private String utid;
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name = "item_id")
	 */
	@Transient
	private Item item;

	private Date orderDate;

	private String paymentTransactionId;

	private String paymentStatus;

	private String paymentMessage;

	private double amount;

	private String orderType = "self"; // self or friend

	private boolean isscheduled; // default false

	private Date scheduledDate; // default orderDate

	private String friendName = null;

	private String friendEmailId = null;

	private String friendContact;

	private String friendMessage;

	private String emailAttempt = null;

	private String emailMessage = null;

	private String emailSubject;

	@JsonIgnore
	private String externalRefID;

	@JsonIgnore
	private Status status; // inprogress/cancelled/delivered/scheduled/deliveredFailed

	private String claimCode;
	
	private String paymentProcessorResponseCode;
	
	private String paymentrocessorResponseText;





	public static enum Status {
		INPROGRESS, SCHEDULED, COMPLETE, CANCELED, DELIVERED_FAILED
	}

	
	private String tangoOrderStatus;
	
	private String recipient;
	
	@JsonIgnore
	private String transaction;

	@JsonIgnore
	@Lob
	@Column(length = 1000000)
	private String tangoResponse;

	@Transient
	private PaymentResponse checkoutData;



	private String avsResponse;
	private String cvvResponse;
	private String ipAddress;

	
	public long getId() {
		return id;
	}

	public PaymentResponse getCheckoutData() {
		return checkoutData;
	}

	public void setCheckoutData(PaymentResponse checkoutData) {
		this.checkoutData = checkoutData;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getPaymentTransactionId() {
		return paymentTransactionId;
	}

	public void setPaymentTransactionId(String paymentTransactionId) {
		this.paymentTransactionId = paymentTransactionId;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentMessage() {
		return paymentMessage;
	}

	public void setPaymentMessage(String paymentMessage) {
		this.paymentMessage = paymentMessage;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public boolean isIsscheduled() {
		return isscheduled;
	}

	public void setIsscheduled(boolean isscheduled) {
		this.isscheduled = isscheduled;
	}

	public Date getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	public String getFriendName() {
		return friendName;
	}

	public void setFriendName(String friendName) {
		this.friendName = friendName;
	}

	public String getFriendEmailId() {
		return friendEmailId;
	}

	public void setFriendEmailId(String friendEmailId) {
		this.friendEmailId = friendEmailId;
	}

	public String getFriendContact() {
		return friendContact;
	}

	public void setFriendContact(String friendContact) {
		this.friendContact = friendContact;
	}

	public String getFriendMessage() {
		return friendMessage;
	}

	public void setFriendMessage(String friendMessage) {
		this.friendMessage = friendMessage;
	}

	public String getEmailAttempt() {
		return emailAttempt;
	}

	public void setEmailAttempt(String emailAttempt) {
		this.emailAttempt = emailAttempt;
	}

	public String getEmailMessage() {
		return emailMessage;
	}

	public void setEmailMessage(String emailMessage) {
		this.emailMessage = emailMessage;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUtid() {
		return utid;
	}

	public void setUtid(String utid) {
		this.utid = utid;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getExternalRefID() {
		return externalRefID;
	}

	public void setExternalRefID(String externalRefID) {
		this.externalRefID = externalRefID;
	}

	public String getTangoResponse() {
		return tangoResponse;
	}

	public void setTangoResponse(String tangoResponse) {
		this.tangoResponse = tangoResponse;
	}

	public String getTangoOrderStatus() {
		return tangoOrderStatus;
	}

	public void setTangoOrderStatus(String tangoOrderStatus) {
		this.tangoOrderStatus = tangoOrderStatus;
	}

	public String getClaimCode() {
		return claimCode;
	}

	public void setClaimCode(String claimCode) {
		this.claimCode = claimCode;
	}

	public String getPaymentProcessorResponseCode() {
		return paymentProcessorResponseCode;
	}

	public void setPaymentProcessorResponseCode(String paymentProcessorResponseCode) {
		this.paymentProcessorResponseCode = paymentProcessorResponseCode;
	}

	public String getPaymentrocessorResponseText() {
		return paymentrocessorResponseText;
	}

	public void setPaymentrocessorResponseText(String paymentrocessorResponseText) {
		this.paymentrocessorResponseText = paymentrocessorResponseText;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	public String getAvsResponse() {
		return avsResponse;
	}

	public void setAvsResponse(String avsResponse) {
		this.avsResponse = avsResponse;
	}

	public String getCvvResponse() {
		return cvvResponse;
	}

	public void setCvvResponse(String cvvResponse) {
		this.cvvResponse = cvvResponse;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

}
