package com.giftcard.model;


import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
public class Deal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="uniqueId", columnDefinition="TEXT")
	private String uniqueId;
	@Column(name="url", columnDefinition="TEXT")
	private String url;
	@Column(name="text", columnDefinition="TEXT")
	private String text;
	@Column(name="imageUrl", columnDefinition="TEXT")
	private String imageUrl;
	@Column(name="category", columnDefinition="TEXT")
	private String category;
	@Column(name="source", columnDefinition="TEXT")
	private String source;
	private int random;
	private double originalPrice;
	private double salePrice;
	private Date firstSeen;
	private boolean sdDeal;
	@Column(name="affliateLink", columnDefinition="TEXT")
	private String affliateLink;
	@Column(name="customerReviewAverage", columnDefinition="TEXT")
	private String customerReviewAverage;
	@Column(name="customerReviewCount", columnDefinition="TEXT")
	private String customerReviewCount;
	@Column(name="offerType", columnDefinition="TEXT")
	private String offerType;
	@Column(name="category_id")
	private Long categoryId;
	private String isExpire;
	//@Value("${some.key :0}")
	private Long likes = (long) 0;
	//@Value("${some.key :0}")
	private Long dislikes = (long) 0;

	private String isExist;
	
	private String addToCartUrl;
	
	@Transient
	private int location;

	@OneToMany(mappedBy = "deal", cascade = CascadeType.ALL)
	@JsonIgnore
	@Transient
	private List<DealFeedback> dealFeedback = new ArrayList<>();

	@JsonIgnore
	private static List<String> iListOfFavIcons = Arrays.asList("amazon","bestbuy","walmart","bestbuy","samsung","costco","newegg","target","costco","ebay","kmart","forever21","zara","zappos","nordstrom","hpb","barnesandnoble","kohls","homedepot","lowes");
	
	public Long getLikes() {
		return likes;
	}
	public void setLikes(Long likes) {
		this.likes = likes;
	}
	public Long getDislikes() {
		return dislikes;
	}
	public static List<String> getiListOfFavIcons() {
		return iListOfFavIcons;
	}
	public static void setiListOfFavIcons(List<String> iListOfFavIcons) {
		Deal.iListOfFavIcons = iListOfFavIcons;
	}
	public void setDislikes(Long dislikes) {
		this.dislikes = dislikes;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public int getRandom() {
		return random;
	}
	public void setRandom(int random) {
		this.random = random;
	}
	public double getOriginalPrice() {
		return originalPrice;
	}
	public void setOriginalPrice(double originalPrice) {
		this.originalPrice = originalPrice;
	}
	public double getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(double salePrice) {
		this.salePrice = salePrice;
	}
	public Date getFirstSeen() {
		return firstSeen;
	}
	public void setFirstSeen(Date firstSeen) {
		this.firstSeen = firstSeen;
	}
	public boolean isSdDeal() {
		return sdDeal;
	}
	public void setSdDeal(boolean sdDeal) {
		this.sdDeal = sdDeal;
	}
	public String getAffliateLink() {
		return affliateLink;
	}
	public void setAffliateLink(String affliateLink) {
		this.affliateLink = affliateLink;
	}
	public String getCustomerReviewAverage() {
		return customerReviewAverage;
	}
	public void setCustomerReviewAverage(String customerReviewAverage) {
		this.customerReviewAverage = customerReviewAverage;
	}
	public String getCustomerReviewCount() {
		return customerReviewCount;
	}
	public void setCustomerReviewCount(String customerReviewCount) {
		this.customerReviewCount = customerReviewCount;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getOfferType() {
		return offerType;
	}
	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getIsExpire() {
		return isExpire;
	}
	public void setIsExpire(String isExpire) {
		this.isExpire = isExpire;
	}

	public String getIsExist() {
		return isExist;
	}
	public void setIsExist(String isExist) {
		this.isExist = isExist;
	}
	
	public int getLocation() {
		return location;
	}
	public void setLocation(int location) {
		this.location = location;
	}
	
	public String getAddToCartUrl() {
		return addToCartUrl;
	}
	public void setAddToCartUrl(String addToCartUrl) {
		this.addToCartUrl = addToCartUrl;
	}
	
	public  int getDiscountPercent() {
		int mDiscountPct = (int)Math.round((this.getOriginalPrice() - this.getSalePrice()) * 100 / this.getOriginalPrice());

		if(mDiscountPct > 0) {
			return mDiscountPct;
		}
		return 0;

	}

	public String getSourceFavIco() {
		String mSourceFavIco = null;
		if(getSource() != null && getSource().contains(".com")) {
			for(String mItem: iListOfFavIcons) {
				if(getSource().contains(mItem)) {
					mSourceFavIco = "img/"+mItem + "_favicon.ico";
					break;
				}
			}
		}
		return mSourceFavIco;
	}

	public String getImgAlt(){
		String mHost = "Deal Logo";
		try {
			if(getUrl() != null && !getUrl().equals("")){
				URL mUrl = new URL(getUrl());
				mHost = mUrl.getHost();

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mHost;
	}
}
