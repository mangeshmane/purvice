package com.giftcard.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class DealEmailRelation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="deal_unique_id")
	private String dealUniqueId;
	
	@Column(name="user_id")
	private Long userId;
	
	@Column(name="email_send_flag")
	private String emailSendFlag;
	
	private String daily;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDealUniqueId() {
		return dealUniqueId;
	}

	public void setDealUniqueId(String dealUniqueId) {
		this.dealUniqueId = dealUniqueId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmailSendFlag() {
		return emailSendFlag;
	}

	public void setEmailSendFlag(String emailSendFlag) {
		this.emailSendFlag = emailSendFlag;
	}

	public String getDaily() {
		return daily;
	}

	public void setDaily(String daily) {
		this.daily = daily;
	}

	@Override
	public String toString() {
		return "DealEmailRelation [id=" + id + ", dealUniqueId=" + dealUniqueId + ", userId=" + userId
				+ ", emailSendFlag=" + emailSendFlag + ", daily=" + daily + "]";
	}

	
}
