package com.giftcard.model;

public class PaymentDetails {

	String globalOnePayURL;
	String globalOnePaySecret;
	int	globalOnePayTerminalId;
	
	public String getGlobalOnePayURL() {
		return globalOnePayURL;
	}
	public void setGlobalOnePayURL(String globalOnePayURL) {
		this.globalOnePayURL = globalOnePayURL;
	}
	public String getGlobalOnePaySecret() {
		return globalOnePaySecret;
	}
	public void setGlobalOnePaySecret(String globalOnePaySecret) {
		this.globalOnePaySecret = globalOnePaySecret;
	}
	public int getGlobalOnePayTerminalId() {
		return globalOnePayTerminalId;
	}
	public void setGlobalOnePayTerminalId(int globalOnePayTerminalId) {
		this.globalOnePayTerminalId = globalOnePayTerminalId;
	}
}
