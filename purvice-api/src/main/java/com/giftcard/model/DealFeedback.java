package com.giftcard.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class DealFeedback {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;


	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "user")
	private User user;

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "deal")
	private Deal deal;

	private boolean likes;

	private boolean disLikes;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Deal getDeal() {
		return deal;
	}

	public void setDeal(Deal deal) {
		this.deal = deal;
	}

	public boolean isLikes() {
		return likes;
	}

	public void setLikes(boolean likes) {
		this.likes = likes;
	}

	public boolean isDisLikes() {
		return disLikes;
	}

	public void setDisLikes(boolean disLikes) {
		this.disLikes = disLikes;
	}


}
