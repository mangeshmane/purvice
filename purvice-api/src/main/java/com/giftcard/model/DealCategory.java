package com.giftcard.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class DealCategory implements Serializable {

	private static final long serialVersionUID = 7156526077883281623L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name="category_name")
	private String categoryName;

	private String description;

	@Column(name="category_image")
	private String categoryImage;

	@Column(name="parent_id")
	private long parentId;

	private long priority;

	@Transient
	private long tempId;

	@Transient
	private String tree_path;


	public String getTree_path() {
		return tree_path;
	}

	public void setTree_path(String tree_path) {
		this.tree_path = tree_path;
	}

	public long getTempId() {
		return tempId;
	}

	public void setTempId(long tempId) {
		this.tempId = tempId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategoryImage() {
		return categoryImage;
	}

	public void setCategoryImage(String categoryImage) {
		this.categoryImage = categoryImage;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public long getPriority() {
		return priority;
	}

	public void setPriority(long priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "DealCategory [id=" + id + ", categoryName=" + categoryName + ", description=" + description
				+ ", categoryImage=" + categoryImage + ", parentId=" + parentId + ", priority=" + priority + ""
				+ ", tempId=" + tempId + ", tree_path=" + tree_path + "]";
	}


}
