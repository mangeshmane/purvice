package com.giftcard.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;


@Entity
public class Item {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String utid;
	
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Brand brand;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "category_item")
	private List<Category> category;
	
	/*@Transient
	@OneToMany(mappedBy = "item", cascade = CascadeType.ALL)
	private List<Orders> orders= new ArrayList<>();*/

	private String rewardName;
	
	private String currencyCode;
	
	private String status;
	
	private String valueType;
	
	private String rewardType;
	
	@Column(name="`minValue`")
	private String minValue;
	
	@Column(name="`maxValue`")
	private String maxValue;
	
	private String createdDate;
	
	private String lastUpdateDate;
	
	@OneToMany(mappedBy = "item", cascade = CascadeType.ALL)
	@Transient
	private List<ItemCountry> countries = new ArrayList<ItemCountry>();
	
	@Transient
	private ItemDenomination itemDenomination = new ItemDenomination();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUtid() {
		return utid;
	}

	public void setUtid(String utid) {
		this.utid = utid;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public String getRewardName() {
		return rewardName;
	}

	public void setRewardName(String rewardName) {
		this.rewardName = rewardName;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getValueType() {
		return valueType;
	}

	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

	public String getRewardType() {
		return rewardType;
	}

	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public List<ItemCountry> getCountries() {
		return countries;
	}

	public void setCountries(List<ItemCountry> countries) {
		this.countries = countries;
	}

	public List<Category> getCategory() {
		return category;
	}

	public void setCategory(List<Category> category) {
		this.category = category;
	}

	public ItemDenomination getItemDenomination() {
		return itemDenomination;
	}

	public void setItemDenomination(ItemDenomination itemDenomination) {
		this.itemDenomination = itemDenomination;
	}

	
}
