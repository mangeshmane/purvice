package com.giftcard.response;


import java.io.Serializable;
import java.util.List;


public class ResponseWrapper <T extends Object> implements Serializable{
	
	private static final long serialVersionUID = 1196298950278053149L;
	
	private T data;
	private ResponseStatus status;
	private String message;
	private String detailedMessage;
	private Long count;
	private Integer errorCode;
	private Integer pageNumber = null;
	private Integer pageSize = null;
	private Long totalNumberOfRecords = null;
	private Integer totalNumberOfPages = null;
	private Boolean hasNextPage = null;
	private Boolean hasPreviousPage = null;
	private String sortingCriteria = null;
	private List<T> list;
	
	//public static ResponseWrapper SUCCESS = new ResponseWrapper();
	
	public static final Integer ERROR_OTHER = 1;
	public static final Integer ERROR_UNAUTHORIZED = 2;
	/**
	 * Convenience method for populating "successful" state
	 */
	public void succeed (T data)
	{
		this.data = data;
		this.status=ResponseStatus.SUCCESS;
		this.message = null;
	}
	
	public void succeed (T data,String cityId,String cityName,String stateId,String stateName)
	{
		this.data = data;
		this.status=ResponseStatus.SUCCESS;
		this.message = null;
	}
	
	public void succeedWithCustomMessage(T data, String message)
	{
		this.data = data;
		this.status=ResponseStatus.SUCCESS;
		this.message = message;
	}
	public void succeedCreated (T data, String name)
	{
		this.data = data;
		this.status=ResponseStatus.SUCCESS;
		this.message = name + " created successfully.";
	}
	

	public void succeedUpdated(T data, String name)
	{
		this.data = data;
		this.status=ResponseStatus.SUCCESS;
		this.message = name + " updated successfully.";
	}
	
	public void succeedDeleted(T data, String name)
	{
		this.data = data;
		this.status=ResponseStatus.SUCCESS;
		this.message = name + " deleted successfully.";
	}
	
	/**
	 * Convenience method for populating "failed" state
	 */
	public void fail(T data, String explanation,
			Throwable ex) {
		this.setData(data);
		this.setStatus(ex == null ? ResponseStatus.FAIL : ResponseStatus.EXCEPTION);
	
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.status=ResponseStatus.SUCCESS;
		this.message = null;
		this.data = data;
	}

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetailedMessage() {
		return detailedMessage;
	}

	public void setDetailedMessage(String detailedMessage) {
		this.detailedMessage = detailedMessage;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
	

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Long getTotalNumberOfRecords() {
		return totalNumberOfRecords;
	}

	public void setTotalNumberOfRecords(Long totalNumberOfRecords) {
		this.totalNumberOfRecords = totalNumberOfRecords;
	}

	public Integer getTotalNumberOfPages() {
		return totalNumberOfPages;
	}

	public void setTotalNumberOfPages(Integer totalNumberOfPages) {
		this.totalNumberOfPages = totalNumberOfPages;
	}

	public Boolean getHasNextPage() {
		return hasNextPage;
	}

	public void setHasNextPage(Boolean hasNextPage) {
		this.hasNextPage = hasNextPage;
	}

	public Boolean getHasPreviousPage() {
		return hasPreviousPage;
	}

	public void setHasPreviousPage(Boolean hasPreviousPage) {
		this.hasPreviousPage = hasPreviousPage;
	}

	public String getSortingCriteria() {
		return sortingCriteria;
	}

	public void setSortingCriteria(String sortingCriteria) {
		this.sortingCriteria = sortingCriteria;
	}
	
}
