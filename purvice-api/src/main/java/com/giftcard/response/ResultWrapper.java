package com.giftcard.response;

import java.io.Serializable;

public class ResultWrapper <T extends Object> implements Serializable{

	private static final long serialVersionUID = 1196298950278053143L;
	
	private T result;
	private Result message;
	private String detailMessage;
	private Long pageNumber;
	private Long tolalRecord;
	private Long pageSize;
	
	/**
	 * Convenience method for populating "successful" state
	 */
	public void succeed (T result)
	{
		this.result = result;
		this.message=Result.SUCCESS;
		this.detailMessage = null;
	}
	
	public void succeed (T result,String cityId,String cityName,String stateId,String stateName)
	{
		this.result = result;
		this.message=Result.SUCCESS;
		this.detailMessage = null;
	}
	
	public void succeedWithCustomMessage(T result, String message)
	{
		this.result = result;
		this.message=Result.SUCCESS;
		this.detailMessage = message;
	}
	public void succeedCreated (T result, String name)
	{
		this.result = result;
		this.message=Result.SUCCESS;
		this.detailMessage = name + " created successfully.";
	}
	

	public void succeedUpdated(T result, String name)
	{
		this.result = result;
		this.message=Result.SUCCESS;
		this.detailMessage = name + " updated successfully.";
	}
	
	public void succeedDeleted(T result, String name)
	{
		this.result = result;
		this.message=Result.SUCCESS;
		this.detailMessage = name + " deleted successfully.";
	}
	
	/**
	 * Convenience method for populating "failed" state
	 */
	public void fail(T result, String explanation,
			Throwable ex) {
		this.setResult(result);
		this.setMessage(ex == null ? Result.FAIL : Result.EXCEPTION);
	
	}
	

	public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
	public Result getMessage() {
		return message;
	}
	public void setMessage(Result message) {
		this.message = message;
	}
	
	public String getExplanation() {
		return detailMessage;
	}
	public void setExplanation(String explanation) {
		this.detailMessage = explanation;
	}

	public Long getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Long pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Long getTolalRecord() {
		return tolalRecord;
	}

	public void setTolalRecord(Long tolalRecord) {
		this.tolalRecord = tolalRecord;
	}

	public Long getPageSize() {
		return pageSize;
	}

	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}
	
}