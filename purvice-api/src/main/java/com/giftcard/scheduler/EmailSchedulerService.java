package com.giftcard.scheduler;

public interface EmailSchedulerService {

	public void emailScheduler();

	public void emailSchedulerForDealAlert();

	public void emailSchedulerForDealAlertEveryHour();

	public void emailSchedulerForDealAlertEveryMidNight();
}
