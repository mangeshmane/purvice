package com.giftcard.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.giftcard.service.OrdersService;

@Service
@EnableScheduling
public class OrderStatusScheduler {

	@Autowired
	private OrdersService ordersService;
	// once in a day
		@Scheduled(cron = "0 0 0 * * ?") 
		@Transactional
		public void cronTaskForGetDate() {
			ordersService.updateOrderStatus();
		}
}