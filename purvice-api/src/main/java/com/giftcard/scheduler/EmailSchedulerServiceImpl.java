package com.giftcard.scheduler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.giftcard.deal.service.DealAlertService;
import com.giftcard.deal.service.DealEmailRelationService;
import com.giftcard.deal.service.DealService;
import com.giftcard.model.Deal;
import com.giftcard.model.DealAlert;
import com.giftcard.model.DealEmailRelation;
import com.giftcard.model.Orders;
import com.giftcard.model.User;
import com.giftcard.service.OrdersService;
import com.giftcard.service.UserService;

@Component
public class EmailSchedulerServiceImpl implements EmailSchedulerService {

	@Autowired
	public OrdersService orderService;

	@Autowired
	public UserService userService;

	@Autowired
	public DealAlertService dealAlertService;

	@Autowired
	public DealService dealService;

	@Autowired
	public DealEmailRelationService dealEmailRelationService;

	private static final Logger logger = LoggerFactory.getLogger(EmailSchedulerServiceImpl.class);

	@Override
	public void emailScheduler() {
		DateFormat formatter = new SimpleDateFormat("YYYY-MM-DD");

		Date input = new Date();
		Date setMin = DateUtils.setMinutes(input, 0);
		Date setSec = DateUtils.setSeconds(setMin, 0);
		String today = formatter.format(input);
		try {
			List<Orders> orders = orderService.findByIsscheduled(true);
			if (orders != null) {
				Iterator<Orders> itr = orders.iterator();
				Orders tempOrder = null;

				while (itr.hasNext()) {
					tempOrder = itr.next();
					try{
						if (tempOrder.isIsscheduled()) {
							if (tempOrder.getScheduledDate() != null || !tempOrder.getScheduledDate().equals("")) {
								String scheduledDate = formatter.format(tempOrder.getScheduledDate());
								if (scheduledDate.equals(today)) {
									User user = tempOrder.getUser();
									orderService.sendScheduledOrder(tempOrder);
								}

							}else{
								logger.debug("========Schedule date is null. Schedule date = "+tempOrder.getScheduledDate()+" and order id is "+tempOrder.getId());
							}
						}
					}catch(Exception e)
					{
						logger.error("error while sending mail to order id="+tempOrder.getId(), e);
					}

				}
			}
		} catch (Exception e) {
			logger.error("error in sending mail ", e);
		}

	}

	@Override
	public void emailSchedulerForDealAlert() {
		try {
			List<DealAlert> dealAlert = dealAlertService.findByDealAlert("0","Daily");
			if (dealAlert != null && dealAlert.size() != 0) {
				Iterator<DealAlert> itr = dealAlert.iterator();
				DealAlert tempDealAlert = null;

				while (itr.hasNext()) {
					tempDealAlert = itr.next();
				 if(tempDealAlert.getIsActive().equalsIgnoreCase("0")) {
					try{
						if (tempDealAlert.getIsActive().equalsIgnoreCase("0") && tempDealAlert.getFrequencies().equalsIgnoreCase("Daily")) {
							List<Deal> addLists = new ArrayList<Deal>();
							List<Deal> deals = dealService.findByCategoryId(tempDealAlert);
							List<DealEmailRelation> dealEmailRelation = dealEmailRelationService.findByUserIdAndDaily(tempDealAlert.getUserId());
							for (Deal deal : deals) {
								deal.setIsExist(new String("0"));
								dealService.saveDeal(deal);
								for (DealEmailRelation   tempDealEmailRelation: dealEmailRelation) {
									if(tempDealEmailRelation.getDealUniqueId().equalsIgnoreCase(deal.getUniqueId())){
										if(tempDealEmailRelation.getUserId() == tempDealAlert.getUserId()){
											deal.setIsExist(new String("1"));
											dealService.saveDeal(deal);
										}
									}
								}
							}

							for (Deal deal : deals) {
								if(deal.getIsExist().equals("1")){}
								else{
									addLists.add(deal);
								}
							}
							if(!addLists.isEmpty()){
								dealAlertService.sendDealAlertByDeals(tempDealAlert, deals);
								dealAlertService.setParameterAfterMail(tempDealAlert);
								dealEmailRelationService.setParameterAfterMailByDeal(addLists, tempDealAlert);
							}
						}
					}catch(Exception e)
					{
						logger.error("error while sending mail to order id="+tempDealAlert.getId(), e);
					}
				  }
				}
			}
		} catch (Exception e) {
			logger.error("error in sending mail ", e);
		}

	}

	@Override
	public void emailSchedulerForDealAlertEveryHour() {
		try {
			List<DealAlert> dealAlert = dealAlertService.findByDealAlert("0","Instantly");
			if (dealAlert != null && dealAlert.size() != 0) {
				try{
					for (DealAlert tempDealAlert : dealAlert) {
					 if(tempDealAlert.getIsActive().equalsIgnoreCase("0")) {
						List<Deal> addLists = new ArrayList<Deal>();
						List<Deal> deals = dealService.findByCategoryId(tempDealAlert);
						List<DealEmailRelation> dealEmailRelation = dealEmailRelationService.findByUserId(tempDealAlert.getUserId());
						for (Deal deal : deals) {
							deal.setIsExist(new String("0"));
							dealService.saveDeal(deal);
							for (DealEmailRelation   tempDealEmailRelation: dealEmailRelation) {
								if(tempDealEmailRelation.getDealUniqueId().equalsIgnoreCase(deal.getUniqueId())){
									if(tempDealEmailRelation.getUserId() == tempDealAlert.getUserId()){
										deal.setIsExist(new String("1"));
										dealService.saveDeal(deal);
									}
								}
							}
						}

						for (Deal deal : deals) {
							if(deal.getIsExist().equals("1")){}
							else{
								addLists.add(deal);
							}
						}
						if(!addLists.isEmpty()){
							dealAlertService.sendDealAlertByDeals(tempDealAlert, addLists);
							dealAlertService.setParameterAfterMail(tempDealAlert);
							dealEmailRelationService.setParameterAfterMailByDeal(addLists, tempDealAlert);
						}
					}
					}
				}catch(Exception e)
				{
					logger.error("error while sending mail to order id=", e);
				}
			}
		} catch (Exception e) {
			logger.error("error in sending mail ", e);
		}
	}

	@Override
	public void emailSchedulerForDealAlertEveryMidNight() {
		List<DealAlert> dealAlerts = new ArrayList<DealAlert>();
		dealAlerts = dealAlertService.findEmailFlagDealAlert();
		for (DealAlert dealAlert : dealAlerts) {
			dealAlert.setEmailSendFlag(new String("0"));
			dealAlertService.save(dealAlert);
		}
	}

}
