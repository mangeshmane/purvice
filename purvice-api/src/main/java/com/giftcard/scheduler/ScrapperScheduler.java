package com.giftcard.scheduler;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.giftcard.Application;
import com.giftcard.deal.service.DealService;
import com.giftcard.scrapper.job.ScrapperService;

@Service
@EnableScheduling
public class ScrapperScheduler {
	
	private static final Logger logger = LoggerFactory.getLogger(ScrapperScheduler.class);
	
	@Autowired
	private ScrapperService dealsService;

	@Value( "${scrapper.enable}" )
	private Boolean IS_SCRAPPER_ENABLE;

	@Autowired
	private DealService dealService;

	@Scheduled(cron = "* */30 * * * *")
	public void cronTaskForEveryHalfHour() {
		if(IS_SCRAPPER_ENABLE) {
			Application.setTime(new Date());
			dealsService.populateData();
		}else {
			logger.warn("Active scrapper!");
		}
	}

	@Scheduled(cron = "0 10 * * * *")
	public void deleteDealEveryThreeDay() {
		dealService.removeAllExpireDeal();
	}
	
	@Scheduled(cron = "0 0 0 * * ?")
	public void afterThreeDaySetExpire() {
		dealService.applyIsExpire();
	}

}
