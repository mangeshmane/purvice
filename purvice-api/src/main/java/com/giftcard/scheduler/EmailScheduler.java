package com.giftcard.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@EnableScheduling
public class EmailScheduler {

	@Autowired
	public EmailSchedulerService emailSchedulerService;

	@Scheduled(cron = "0 0 0 * * ?")
	public void cronTaskForGetDate() {
		emailSchedulerService.emailSchedulerForDealAlert();
	}

	@Scheduled(cron = "* */30 * * * *")
	public void cronTaskForEveryHour() {
		emailSchedulerService.emailSchedulerForDealAlertEveryHour();
	}
}
