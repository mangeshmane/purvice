package com.giftcard.scrapper.job;

import com.giftcard.deal.service.DealsServiceImpl;
import com.giftcard.deals.model.Product;
import com.giftcard.deals.model.ProductModel;
import com.giftcard.deals.model.SDModel;
import com.giftcard.deals.utility.CategoryDealCacheUtility;
import com.giftcard.model.Deal;
import com.giftcard.model.DealCategory;
import com.giftcard.model.DealCronJob;
import com.giftcard.scapper.helper.AWSHelper;
import com.giftcard.scapper.helper.BestBuyHelper;
import com.giftcard.scapper.helper.WalmartHelper;
import com.giftcard.scrappers.AbstractScraper;
import com.giftcard.scrappers.AmazonScraper;
import com.giftcard.scrappers.GenericScrapper;
import com.giftcard.service.DealCategoryService;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;
import java.util.Map.Entry;

@Service
public class ScrapperServiceImpl implements ScrapperService {
	// private Map<String,SDModel> mapOfData = null;
	
	@Autowired
	private DealsServiceImpl dealsServiceImpl;
	
	@Autowired
	private DealCategoryService dealCategoryService;
	
	private List<SDModel> iListOutput = null;
	private List<SDModel> iListOutputLatest = null;
	private static int NUM_PRODUCTS = Integer.parseInt(System.getProperty("num_products", "80"));
	private static final boolean IS_DEV = false;
	protected static final List<String> ORDERED_ENTRIES = Arrays.asList("www.amazon.com", "www.bestbuy.com",
			"www.target.com", "shop.nordstrom.com", "www.groupon.com", "www.walmart.com", "www.woot.com"

	);

	protected static final List<String> ORDERED_ENTRIES_LATEST = Arrays.asList("www.amazon.com", "www.bestbuy.com",
			"www.target.com", "shop.nordstrom.com", "www.walmart.com");
	private static DateFormat iDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	private static List<String> iListOfImageUrlsToIgnore = Arrays.asList(
			"https://images-na.ssl-images-amazon.com/images/G/01/error/500_503.png",
			"https://www.bhphotovideo.com/images/eduPartners/ajaxdpiNYC.gif",
			"https://ll-us-i5.wal.co/dfw/63fd9f59-a546/k2-_47005cc8-29da-4fa9-ac2c-45e102a55bf5.v1.png-004745309bee4db643a938892efdc3716397adde-crushed-203x50.png",
			"http://g-ecx.images-amazon.com/images/G/01/website/errors/503/generic.png",
			"http://deals.dell.com/Content/images/dell-logo.png",
			"https://images10.newegg.com/WebResource/Themes/2005/Nest/neLogoUS.1.png",
			"https://images-na.ssl-images-amazon.com/images/G/01/gno/sprites/nav-sprite-global_bluebeacon-V3-1x_optimized._CB516556901_.png",
			"https://www.costco.com/wcsstore/CostcoGLOBALSAS/images/Costco_Logo-1.png");



	private ScrapperServiceImpl() {
	}

	private ScrapperServiceImpl(SDModel m) {

	}

	private static final ScrapperServiceImpl iInstance = new ScrapperServiceImpl();

	public static ScrapperServiceImpl getInstance() {
		return iInstance;
	}
	
	public static void main(String[] args) {
		ScrapperServiceImpl.getInstance();

		// SDModel model = new SDModel();

		// model.setUrl("http://www.target.com/p/target-beauty-box-16-value/-/A-50190756?clkid=43aaf581Ne66bc6f75f306add868d0a69&lnm=79373&afid=Slickdeals+LLC&ref=tgt_adv_xasd0002");

		// findOGImageIfAvailable(model);
		// ScrapSDHelper.getInstance().populateData();
		// System.out.println("size:"+ScrapSDHelper.getInstance().getMapOfData().size());
		// System.out.println(getTrueUrl("http://slickdeals.net/?pid=78526604&lno=1&afsrc=1&trd=Buy+Now&pv=d287baa0699f11e5be7ae65625bd1c44&au=d283f028699f11e5be7ae65625bd1c44"));
	}

	public List<SDModel> implementSorting(Map<String, SDModel> mMapOfData, Comparator aComp) {
		List<SDModel> mOutput = new ArrayList<SDModel>();
		if (mMapOfData == null) {
			return mOutput;
		}

		int mSizeOfMap = mMapOfData.size();

		Map<String, List<SDModel>> mNewMap = new HashMap<String, List<SDModel>>();
		// Map<String, Integer> mNewMapCount = new HashMap<String, Integer>();
		for (Map.Entry<String, SDModel> mEntry : mMapOfData.entrySet()) {

			try {
				SDModel sd = mEntry.getValue();
				if (sd == null) {
					continue;
				}
				String mSource = sd.getSource();

				if (mNewMap.containsKey(mSource)) {
					mNewMap.get(mSource).add(sd);

				} else {
					List<SDModel> mList = new ArrayList<SDModel>();
					mList.add(sd);
					mNewMap.put(mSource, mList);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		List<String> listSource = new ArrayList<String>(mNewMap.keySet());
		Collections.sort(listSource, new CustomSorter());

		for (String mSource : listSource) {
			try {
				List<SDModel> mList = mNewMap.get(mSource);
				if (aComp != null) {
					Collections.sort(mList, aComp);
				} else {
					Collections.sort(mList);
					Collections.reverse(mList);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		for (int i = 0; i <= 400; i++) {
			// Assuming we want to show only 200 products from any vendor

			// for(Map.Entry<String, List<SDModel>> entry:mNewMap.entrySet()){
			for (String mSource : listSource) {
				try {
					List<SDModel> mList = mNewMap.get(mSource);
					if (i < mList.size()) {
						mOutput.add(mList.get(i));
						System.out.println(
								"@@@ACTEST==>>[" + mList.get(i).getSource() + "] product:" + mList.get(i).getText());
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		return mOutput;

	}
    @Override
	public void populateData() {
		System.out.println("@@@@@@@@ACTEST...came to populate Data....");
		Map<String, SDModel> tmpMapOfData = new HashMap();
		List<SDModel> mOutputList = new ArrayList<SDModel>();

		try {
			populateOtherScrappedPages();

			// findMostCurrentDeals(mOutputList);
			/*
			 * if(1==1){ mOutputList = implementSorting(tmpMapOfData);
			 * findMostCurrentDeals(mOutputList); iListOutput = mOutputList;
			 * 
			 * return; }
			 */

			// Validate.isTrue(args.length == 1, "usage: supply url to fetch");
			// String url =
			// "https://slickdeals.net/forums/forumdisplay.php?f=9&daysprune=7&order=desc&pp="+NUM_PRODUCTS+"&sort=lastpost&vote=10";
			String url = "https://slickdeals.net/forums/filtered/?f=9&daysprune=2&order=desc&pp=80&sort=lastpost&vote=10";
			// print("Fetching %s...", url);

			Document doc = Jsoup.connect(url).ignoreContentType(true).timeout(5000).userAgent("Mozilla")
					.ignoreHttpErrors(true).maxBodySize(10 * 1000 * 1000 * 1000).get();
			Elements links = doc.select("[href]");
			// Elements media = doc.select("[src]");
			// Elements imports = doc.select("link[href]");

			// print("\nMedia: (%d)", media.size());
			// for (Element src : media) {
			// if (src.tagName().equals("img"))
			// print(" * %s: <%s> %sx%s (%s)", src.tagName(),
			// src.attr("abs:src"), src.attr("width"), src.attr("height"),
			// trim(src.attr("alt"), 20));
			// else
			// print(" * %s: <%s>", src.tagName(), src.attr("abs:src"));
			// }
			//
			// print("\nImports: (%d)", imports.size());
			// for (Element link : imports) {
			// print(" * %s <%s> (%s)", link.tagName(), link.attr("abs:href"),
			// link.attr("rel"));
			// }

			// print("\nLinks: (%d)", links.size());
			int mCounter = 0;
			for (Element link : links) {

				// String mLink = "https://slickdeals.net"+link.attr("href");
				String mLink = link.attr("abs:href");

				if (mLink == null || !mLink.contains("/f/") || tmpMapOfData.containsKey(mLink)
				// || mLink.contains("?")
				// || link == null
				) {
					// System.out.println("@@@ACTEST ignoring");
					continue;
				}
				SDModel mModel = new SDModel();

				if (link == null || link.text() == null || link.text().equals("")
						|| link.text().contains("Revamped Slickdeals Search") || link.text().equals("Last Page")

				) {
					continue;
				}

				System.out.println("@@@ACTEST link.text():[" + link.text() + "] mLink:[" + mLink + "]");

				mModel.setText(link.text());
				tmpMapOfData.put(mLink, mModel);
				openLinkAndExtractData(mLink, mModel);
				findOGImageIfAvailable(mModel, mLink);

				if (!mModel.isImageUrlFound()) {
					openVendorAndTryToFindProductImage(mModel);
				}
				
				if(mModel.getImageUrl() != null && mModel.getImageUrl().contains("sd-facebook-5-2.png")) {
					// trying to remove the default slickdeals image from showin up in our app.
					mModel.setImageUrl(null);
				}
				

				mCounter++;

				if (IS_DEV) {
					if (mCounter > 2) {
						break;
					}
				}

				// print(" * a: <%s> (%s)", link.attr("abs:href"), trim(link.text(), 35));
			}

		/*	System.out.println("@@@@@@ACTEST tmpMapOfData.size():" + tmpMapOfData.size());

			Iterator<Map.Entry<String, SDModel>> iter = tmpMapOfData.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry<String, SDModel> entry = iter.next();
				System.out.println("text:" + entry.getValue().getText() + "\t\turl:" + entry.getValue().getUrl());
				if (entry.getValue().getUrl() == null || entry.getValue().getUrl().contains("slickdeals")
						|| entry.getValue().getUrl().contains(".uk") 
						|| entry.getValue().getImageUrl() == null
						|| entry.getValue().getImageUrl().equals("") 
						|| (entry.getValue().getText() != null && entry.getValue().getText().toLowerCase().contains("slickdeals"))
						|| entry.getValue().getSource() == null
						|| entry.getValue().getSource().equals("")

				) {
					iter.remove();
				}
			}

		} catch (Exception mExcep) {
			mExcep.printStackTrace();
		}


		try {
			mOutputList = implementSorting(tmpMapOfData, null);
			List<SDModel> aOutputListNew = new ArrayList<SDModel>();

			// Gather the existing product names to quickly match.
			Map<String, SDModel> mExistingProductMap = new HashMap<String, SDModel>();

			if (iListOutput != null) {
				for (SDModel s : iListOutput) {
					mExistingProductMap.put(s.getText(), s);
				}
			}

			for (SDModel s : mOutputList) {
				if (s == null || s.getText() == null || s.getImageUrl() == null || s.getSource() == null || s.isSdDeal()
						|| !ORDERED_ENTRIES_LATEST.contains(s.getSource())) {
					continue;
				}
				if (mExistingProductMap.containsKey(s.getText())) {
					// System.out.println("@@@@@@@@ACTEST ....seen first===>>>"+s.getText());
					s.setFirstSeen(mExistingProductMap.get(s.getText()).getFirstSeen());
				}

				aOutputListNew.add(s);
			}

			// setup affliate marketing links if needed
			for (SDModel s : mOutputList) {
				if (s == null || s.getSource() == null) {
					continue;
				}
				modifyURL(s);
			}

			Collections.sort(aOutputListNew, new DateSorter());
			Collections.sort(mOutputList, new DateSorter());

			if (aOutputListNew.size() >= 9) {
				iListOutputLatest = aOutputListNew.subList(0, 9);
			} else {
				iListOutputLatest = aOutputListNew;
			}
			
			iListOutput = mOutputList;
			saveDealsData(iListOutput,dealCronJob);
			System.out.println("@@@@@@@@@@ACTEST data refreshed at:[" + iDateFormat.format(Calendar.getInstance().getTime())
					+ "] Num of records:[" + iListOutput.size() + "]");		*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		DealCronJob dealCronJob = new DealCronJob();
		dealCronJob.setCreatedDate(new Date());
		dealCronJob.setAuthor("Slickdeals");
		saveDealScrapperData(tmpMapOfData, mOutputList, dealCronJob);
	}

    
    public void saveDealScrapperData(Map<String, SDModel> tmpMapOfData, List<SDModel> mOutputList, DealCronJob dealCronJob){
     try {
    	System.out.println("@@@@@@ACTEST tmpMapOfData.size():" + tmpMapOfData.size());

		Iterator<Map.Entry<String, SDModel>> iter = tmpMapOfData.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<String, SDModel> entry = iter.next();
			System.out.println("text:" + entry.getValue().getText() + "\t\turl:" + entry.getValue().getUrl());
			if (entry.getValue().getUrl() == null || entry.getValue().getUrl().contains("slickdeals")
					|| entry.getValue().getUrl().contains(".uk") 
					|| entry.getValue().getImageUrl() == null
					|| entry.getValue().getImageUrl().equals("") 
					|| (entry.getValue().getText() != null && entry.getValue().getText().toLowerCase().contains("slickdeals"))
					|| entry.getValue().getSource() == null
					|| entry.getValue().getSource().equals("")

			) {
				iter.remove();
			}
		}

	} catch (Exception mExcep) {
		mExcep.printStackTrace();
	}


	try {
		mOutputList = implementSorting(tmpMapOfData, null);
		List<SDModel> aOutputListNew = new ArrayList<SDModel>();

		// Gather the existing product names to quickly match.
		Map<String, SDModel> mExistingProductMap = new HashMap<String, SDModel>();

		if (iListOutput != null) {
			for (SDModel s : iListOutput) {
				mExistingProductMap.put(s.getText(), s);
			}
		}

		for (SDModel s : mOutputList) {
			if (s == null || s.getText() == null || s.getImageUrl() == null || s.getSource() == null || s.isSdDeal()
					|| !ORDERED_ENTRIES_LATEST.contains(s.getSource())) {
				continue;
			}
			if (mExistingProductMap.containsKey(s.getText())) {
				// System.out.println("@@@@@@@@ACTEST ....seen first===>>>"+s.getText());
				s.setFirstSeen(mExistingProductMap.get(s.getText()).getFirstSeen());
			}

			aOutputListNew.add(s);
		}

		// setup affliate marketing links if needed
		for (SDModel s : mOutputList) {
			if (s == null || s.getSource() == null) {
				continue;
			}
			modifyURL(s);
		}

		Collections.sort(aOutputListNew, new DateSorter());
		Collections.sort(mOutputList, new DateSorter());

		if (aOutputListNew.size() >= 9) {
			iListOutputLatest = aOutputListNew.subList(0, 9);
		} else {
			iListOutputLatest = aOutputListNew;
		}
		
		iListOutput = mOutputList;
		saveDealsData(iListOutput,dealCronJob);
		System.out.println("@@@@@@@@@@ACTEST data refreshed at:[" + iDateFormat.format(Calendar.getInstance().getTime())
				+ "] Num of records:[" + iListOutput.size() + "]");		
	} catch (Exception e) {
		e.printStackTrace();
	}
	
    }
    
	// This will setup affliate links as needed.

	private static void modifyURL(SDModel aModel) {

		if (aModel != null && aModel.getSource() != null && aModel.getUrl() != null) {
			String mOrigUrl = aModel.getUrl();
			String mNewUrl = null;
			if (aModel.getSource().toLowerCase().contains("walmart")) {
				mNewUrl = "https://linksynergy.walmart.com/deeplink?id=2dK3xt1T774&mid=2149&murl=" + mOrigUrl;
				aModel.setAffliateLink(mNewUrl);
				System.out.println("@@@@@@@@ACTEST modify link for walmart:" + mNewUrl);
			} else if (aModel.getSource().toLowerCase().contains("macys")) {
				mNewUrl = "https://click.linksynergy.com/deeplink?id=2dK3xt1T774&mid=3184&murl=" + mOrigUrl;
				aModel.setAffliateLink(mNewUrl);
				System.out.println("@@@@@@@@ACTEST modify link for macys:" + mNewUrl);
			} else if (aModel.getSource().toLowerCase().contains("rakuten")) {
				mNewUrl = "https://click.linksynergy.com/deeplink?id=2dK3xt1T774&mid=36342&murl=" + mOrigUrl;
				aModel.setAffliateLink(mNewUrl);
				System.out.println("@@@@@@@@ACTEST modify link for rakuten:" + mNewUrl);
			} else if (aModel.getSource().toLowerCase().contains("target")) {
				if (mOrigUrl != null) {
					mOrigUrl = mOrigUrl.replaceAll("&afid=Slickdeals_LLC", "");
				}
				mNewUrl = "https://goto.target.com/c/202406/81938/2092?u=" + mOrigUrl;
				aModel.setAffliateLink(mNewUrl);
				System.out.println("@@@@@@@@ACTEST modify link for rakuten:" + mNewUrl);
			}

		}

	}

	public void populateOtherScrappedPages() {

		/*
		 * populateSDModelFromOtherProductList(new WootScraper(),mMap);
		 * populateSDModelFromOtherProductList(new RedditScraper(),mMap);
		 * populateSDModelFromOtherProductList(new GrouponScraper(),mMap);
		 * populateSDModelFromOtherProductList(new WalmartScraper(),mMap);
		 * populateSDModelFromOtherProductList(new NordstromScraper(),mMap);
		 * populateSDModelFromOtherProductList(new MacysScraper(),mMap);
		 * populateSDModelFromOtherProductList(new TargetScraper(),mMap);
		 * populateSDModelFromOtherProductList(new BestbuyScraper(),mMap);
		 */
		try {
			//populateSDModelFromOtherProductList(new RedditScraper(), mMap);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		try {
			DealCronJob dealCronJob = new DealCronJob();
			dealCronJob.setCreatedDate(new Date());
			dealCronJob.setAuthor("Best Buy Deal Of Day");
			Map<String, SDModel> mMap = new HashMap();
			List<SDModel> mOutputList = new ArrayList<SDModel>();
			BestBuyHelper.populateDealOfDayAndDigitalInserts(mMap);
			saveDealScrapperData(mMap, mOutputList, dealCronJob);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			DealCronJob dealCronJob = new DealCronJob();
			dealCronJob.setCreatedDate(new Date());
			dealCronJob.setAuthor("Walmart");
			Map<String, SDModel> mMap = new HashMap();
			List<SDModel> mOutputList = new ArrayList<SDModel>();
			WalmartHelper.populateTrendingDeals(mMap);
			saveDealScrapperData(mMap, mOutputList, dealCronJob);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




		try {
			DealCronJob dealCronJob = new DealCronJob();
    		dealCronJob.setCreatedDate(new Date());
    		dealCronJob.setAuthor("Amazon");
    		Map<String, SDModel> mMap = new HashMap();
    		List<SDModel> mOutputList = new ArrayList<SDModel>();
			populateSDModelFromOtherProductList(new AmazonScraper(), mMap);
			saveDealScrapperData(mMap, mOutputList, dealCronJob);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			DealCronJob dealCronJob = new DealCronJob();
    		dealCronJob.setCreatedDate(new Date());
    		dealCronJob.setAuthor("Generic");
    		Map<String, SDModel> mMap = new HashMap();
    		List<SDModel> mOutputList = new ArrayList<SDModel>();
			populateSDModelFromGenericScrapper(mMap);
			saveDealScrapperData(mMap, mOutputList, dealCronJob);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
		try {
			//populateSDModelFromOtherProductList(new NordstromScraper(), mMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// commenting this out - as per logs this is not pulling any data - 06/14/2019

//		try {
//			DealCronJob dealCronJob = new DealCronJob();
//    		dealCronJob.setCreatedDate(new Date());
//    		dealCronJob.setAuthor("Best Buy Trending Deals");
//    		Map<String, SDModel> mMap = new HashMap();
//    		List<SDModel> mOutputList = new ArrayList<SDModel>();
//			BestBuyHelper.populateTrendingDeals(mMap);
//			saveDealScrapperData(mMap, mOutputList, dealCronJob);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}




	}

	
	private void populateSDModelFromGenericScrapper(Map<String, SDModel> mMap) {
		int mCounter = 0;
		try {
			System.out.println("populate SDModel from populateSDModelFromGenericScrapper");
			List<ProductModel> mList = GenericScrapper.parseAndPopulate(null, null);
			
			
			if (mList != null) {
				System.out.println("@@@@@@@@@@@@@@@@@@ACTEST....count===>>" + mList.size() + " ] ");
				for (ProductModel p : mList) {
					if (p == null) {
						continue;
					}
					String mLink = p.getDestUrl();

					if (p.getProductName() == null || p.getUrl() == null) {
						continue;
					}

					SDModel mSDModel = new SDModel(p);
					mMap.put(mLink, mSDModel);
					mCounter++;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Number of items added by Generic Scrapper:["+mCounter+"]");

	}	
	
	
	private void populateSDModelFromOtherProductList(AbstractScraper sp, Map<String, SDModel> mMap) {
		try {
			System.out.println("populateSDModelFromOtherProductList111111111111");
			List<Product> products = sp.productScraping();
			if (products != null) {
				System.out.println("@@@@@@@@@@@@@@@@@@ACTEST....count===>>" + products.size() + " ] " + sp.getClass());
				for (Product p : products) {
					if (p == null) {
						continue;
					}
					String mLink = p.getProductUrl();

					if (p.getProductName() == null || p.getImageUrl() == null) {
						continue;
					}

					SDModel mSDModel = new SDModel(p);
					mMap.put(mLink, mSDModel);
					//saveDeals(mSDModel);
					
				}
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
//	public void saveDeals(SDModel mSDModel ) throws SQLException, Exception{
//		DBUtil dbutil = new DBUtil();
//		Connection conn = null;
//		Statement stmt = null;
//		ResultSet rs = null;
//		String sql = null;
//		try {
//			conn = DBUtil.getDataSource().getConnection();
//			stmt = conn.createStatement();
//			PreparedStatement ps = conn.prepareStatement(
//					"INSERT INTO OHRI (uniqueId, offerType, text, url, imageUrl, category, source, affliateLink, customerReviewAverage, customerReviewCount, random, originalPrice, salePrice, firstSeen, sdDeal) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
//			ps.setString(1, mSDModel.getUniqueId());
//			ps.setString(2, mSDModel.getOfferType());
//			ps.setString(3, mSDModel.getText());
//			ps.setString(4, mSDModel.getUrl());
//			ps.setString(5, mSDModel.getImageUrl());
//			ps.setString(6, mSDModel.getCategory());
//			ps.setString(7, mSDModel.getSource());
//			ps.setString(8, mSDModel.getAffliateLink());
//			ps.setString(9, mSDModel.getCustomerReviewAverage());
//			ps.setString(10, mSDModel.getCustomerReviewCount());
//			ps.setInt(11, mSDModel.getRandom());
//			ps.setDouble(12, mSDModel.getOriginalPrice());
//			ps.setDouble(13, mSDModel.getSalePrice());
//			ps.setDate(14, (Date) mSDModel.getFirstSeen());
//			ps.setBoolean(15, mSDModel.isSdDeal()); 
//			ps.executeUpdate();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		finally {
//			DBUtil.close(rs,stmt, conn);
//		}
//	}
	

	private static void findOGImageIfAvailable(SDModel mModel, String aSlickdealUrl) {

		if (mModel == null || mModel.getUrl() == null) {
			return;
		}
		String mImgUrl;
		try {
			Document doc = Jsoup.connect(mModel.getUrl()).ignoreContentType(true).timeout(5000).userAgent("Mozilla")
					.ignoreHttpErrors(true).maxBodySize(10 * 1000 * 1000 * 1000).get();
			// Document doc =
			// Jsoup.connect("http://www.target.com/p/milk-bone-dog-snacks-large-160-oz/-/A-14721838?clkid=340bfe0eNe725a30bf3241f5680f7311d&lnm=79373&afid=Slickdeals+LLC&ref=tgt_adv_xasd0002").get();
			Elements metalinks = doc.select(
					"meta[property=og:image],meta[name=twitter:image:src],meta[name=twitter:image],img[id=mainImage]");
			boolean mImageFound = false;

			print("\nMedia: (%d) :", metalinks.size());
			for (Element src : metalinks) {
				mImgUrl = src.attr("content");
				if (mImgUrl != null) {
					mModel.setImageUrl(mImgUrl);
					mImageFound = true;
					System.out.println("@@@@@ACTEST IMAGE FOUND:" + mImgUrl);
					break;
				}
			}

			if (!mImageFound && aSlickdealUrl != null) {
				// Lets pull it from Slickdeals page
				doc = Jsoup.connect(aSlickdealUrl).ignoreContentType(true).timeout(5000).userAgent("Mozilla")
						.ignoreHttpErrors(true).maxBodySize(10 * 1000 * 1000 * 1000).get();
				// Document doc =
				// Jsoup.connect("http://www.target.com/p/milk-bone-dog-snacks-large-160-oz/-/A-14721838?clkid=340bfe0eNe725a30bf3241f5680f7311d&lnm=79373&afid=Slickdeals+LLC&ref=tgt_adv_xasd0002").get();
				metalinks = doc.select(
						"meta[property=og:image],meta[name=twitter:image:src],meta[name=twitter:image],img[id=mainImage]");
				mImageFound = false;

				print("\nMedia: (%d) :", metalinks.size());
				for (Element src : metalinks) {
					mImgUrl = src.attr("content");
					if (mImgUrl != null) {
						mModel.setImageUrl(mImgUrl);
						mImageFound = true;
						System.out.println("@@@@@ACTEST IMAGE FOUND from slickdeals link:" + mImgUrl);
						break;
					}
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("warning:" + e.getMessage());
		}

	}

	private static boolean openVendorAndTryToFindProductImage(SDModel mModel) {
		boolean mImageFound = false;
		if (mModel == null || mModel.getUrl() == null) {
			return mImageFound;
		}
		try {
			Document doc = Jsoup.connect(mModel.getUrl()).ignoreContentType(true).timeout(5000).userAgent("Mozilla")
					.ignoreHttpErrors(true).maxBodySize(10 * 1000 * 1000 * 1000).get();
			Elements media = doc.select("[src]");

			print("\nMedia: (%d)", media.size());
			int mCounter = 0;
			Dimension mMaxDim = null;
			String mBigImage = null;
			for (Element src : media) {
				if (src.tagName().equals("img")) {
					print(" * %s: <%s> %sx%s (%s)", src.tagName(), src.attr("abs:src"), src.attr("width"),
							src.attr("height"), trim(src.attr("alt"), 20));

					Dimension mDim = getImageSize(src.attr("abs:src"));

					if (mDim.getWidth() == 0 && mDim.getHeight() == 0) {
						continue;
					}

					mBigImage = src.attr("abs:src");
					if (mBigImage != null && mBigImage.contains("http")
							&& iListOfImageUrlsToIgnore.contains(mBigImage)) {
						// Ignore big images which are from whitelist.
						continue;

					}

					if (mMaxDim != null) {
						if (mDim.getHeight() > mMaxDim.getHeight() && mDim.getWidth() > mMaxDim.getWidth()) {
							mMaxDim = mDim;
							mBigImage = src.attr("abs:src");
							if (mBigImage != null && mBigImage.contains("http")) {
								mModel.setImageUrl(mBigImage);
							}
						}
					} else {
						if (mDim != null) {
							mMaxDim = mDim;
							mBigImage = src.attr("abs:src");
							if (mBigImage != null && mBigImage.contains("http")) {
								mModel.setImageUrl(mBigImage);
							}
						}

					}

					System.out.println("@@@@@@ACTEST mBigImage:" + mBigImage);

					if (mBigImage != null) {
						mImageFound = true;
					}

					mCounter++;
					// Originally this breakpoint was set at 150 - this is too high- lets try 25
					if (mCounter > 25) {

						//
						break;
					}
				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mImageFound;

	}

	private static Dimension getImageSize(String resourceFile) {
		try {
			URL url = new URL(resourceFile);
			BufferedImage image = ImageIO.read(url);

			if (image != null) {
				int width = image.getWidth();
				int height = image.getHeight();

				return new Dimension(width, height);
			}

			/*
			 * 
			 * 
			 * try(ImageInputStream in = ImageIO.createImageInputStream(resourceFile)){
			 * final Iterator<ImageReader> readers = ImageIO.getImageReaders(in); if
			 * (readers.hasNext()) { ImageReader reader = readers.next(); try {
			 * reader.setInput(in); return new Dimension(reader.getWidth(0),
			 * reader.getHeight(0)); } finally { reader.dispose(); } } }
			 */
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.out.println("error fetching image:" + e.getMessage());
		}
		return new Dimension(0, 0);

	}

	private static void openLinkAndExtractData(String url, SDModel iSDModel) {
		long aTime1 = Calendar.getInstance().getTimeInMillis();
		try {
			// print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@Fetching %s...", url);

			Document doc = Jsoup.connect(url).ignoreContentType(true).timeout(5000).userAgent("Mozilla")
					.ignoreHttpErrors(true).maxBodySize(10 * 1000 * 1000 * 1000).get();
			Elements links = doc.select("a[href]");
			// Elements media = doc.select("[src]");
			// Elements imports = doc.select("link[href]");
			// print("\nLinks: (%d)", links.size());
			Map mMap = new HashMap();
			String mActualUrl;// slickdeals has their own urls redirecting to vendor websites.
			for (Element link : links) {
				String mLink = link.attr("abs:href");
				if (mLink == null || mMap.containsKey(mLink) || link == null) {
					continue;
				}

				// System.out.println("@@@@ACTEST link.text():"+link.text());
				if (link.text().toLowerCase().contains("see deals")) {
					continue;
				}

				if (!link.text().toLowerCase().contains("see deal")) {
					continue;
				}
				mActualUrl = getTrueUrl(mLink);
				// System.out.println("@@@@@@ mLink=="+mLink+"] mActualUrl:["+mActualUrl+"]");

				if (mActualUrl != null && mActualUrl.contains("amazon.com")) {
					mActualUrl = mActualUrl.replaceAll("(?<=[?&;])tag=.*?($|[&;])", "");
					mActualUrl = mActualUrl + "/?tag=purvice-20";
				}

				iSDModel.setUrl(mActualUrl);
				mMap.put(mLink, link.text());
				// print(" * a: <%s> (%s)", link.attr("abs:href"), trim(link.text(), 35));
				// break;
			}
		} catch (Exception mExcep) {
			System.out.println("@@@@@@@@@ACTEST time it took to fail:["
					+ (Calendar.getInstance().getTimeInMillis() - aTime1) / 1000 + "] Seconds");
			mExcep.printStackTrace();
		}
	}

	private static String digDeepToFigureOutUrl(String aUrl) {
		// System.out.println("@@@@@@ACTEST came to get digDeepToFigureOutUrl for
		// aUrl:"+aUrl);
		String mTrueUrl = null;
		try {
			System.setProperty("http.agent", "Chrome");
			URLConnection con = new URL(aUrl).openConnection();
			con.addRequestProperty("User-Agent", "Mozilla/4.76");
			// con.setRequestProperty("User-Agent", "Mozilla/5.0");
			con.setConnectTimeout(5000);
			con.setReadTimeout(5000);
			con.connect();
			InputStream is = con.getInputStream();
			mTrueUrl = con.getURL().toString();
			if (mTrueUrl.equals(aUrl)) {
				// wrap the urlconnection in a bufferedreader
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
				String line;
				// read from the urlconnection via the bufferedreader
				while ((line = bufferedReader.readLine()) != null) {
					if (line.contains("merch_url")) {
						mTrueUrl = line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\""));
						break;
					}
					// System.out.println("############################### ACTEST line:"+line );
				}
				bufferedReader.close();

			}

			is.close();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("error:" + e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("error:" + e.getMessage());
		}
		// System.out.println("@@@@@@ACTEST came to get digDeepToFigureOutUrl for
		// mTrueUrl and got:"+mTrueUrl);
		return mTrueUrl;
	}

	private static String getTrueUrl(String mLink) {
		// System.out.println("@@@@@@ACTEST came to get true url for mLink:"+mLink);
		if (mLink != null && mLink.contains("amazon.com")) {
			mLink = mLink.replaceAll("(?<=[?&;])tag=.*?($|[&;])", "");
		}
		String mTrueUrl = mLink;

		Response response;

		try {
			response = Jsoup.connect(mTrueUrl).ignoreContentType(true).timeout(5000).userAgent("Mozilla")
					.ignoreHttpErrors(true).maxBodySize(10 * 1000 * 1000 * 1000).followRedirects(true).execute();
			mTrueUrl = response.url().toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("@@@ACTEST unable to fetch getTrueUrl:"+e.getMessage());
		}

		// System.out.println("@@@@@@ACTEST came to get true url for
		// mTrueUrl:"+mTrueUrl);
		if (mTrueUrl != null && !mTrueUrl.contains("slickdeals.net")) {
			return mTrueUrl;
		}
		return digDeepToFigureOutUrl(mLink);
	}

	private static void print(String msg, Object... args) {
		System.out.println(String.format(msg, args));
	}

	private static String trim(String s, int width) {
		if (s.length() > width)
			return s.substring(0, width - 1) + ".";
		else
			return s;
	}

	public List<SDModel> getMapOfData(HttpServletRequest request) {
		String aItem = request.getParameter("frmSearchParam");
		String aType = "c";
		if (aItem == null) {
			if(iListOutput == null) {
				// startup population is going on.
				return new ArrayList<>();
			}
			return iListOutput;
		}
		return getFilteredList(aItem, aType, request);

	}
	
	public List<SDModel> getMapOfData(String aItem, String aType) {
		System.out.println("size:" + (iListOutput !=null?iListOutput.size():null));
		if (aItem == null) {
			if(iListOutput == null) {
				// startup population is going on.
				return new ArrayList<>();
			}
			return iListOutput;
		}
		return getFilteredList(aItem, aType);

	}

	public List<SDModel> getTopDeals() {
		System.out.println("size:" + iListOutput.size());

		if (iListOutput != null && iListOutput.size() >= 11) {
			return iListOutput.subList(0, 10);
		}

		return new ArrayList<SDModel>();

	}

	private List<SDModel> getFilteredList(String aItem, String aType, HttpServletRequest request) {
		List<SDModel> mNewList = new ArrayList<SDModel>();
		
		if (iListOutput == null) {
			mNewList = dealsServiceImpl.findKeyWordInDeal(request);
		}else{
			for (SDModel m : iListOutput) {
				if (aItem == null) {
					continue;
				}
				
				String arr[] = aItem.split(",");
				for (String mItem : arr) {

					if (m.getText() != null && (m.getText().contains(mItem) || m.getText().toLowerCase().contains(mItem.toLowerCase()))) {
						mNewList.add(m);
					} else if (m.getCategory() != null && m.getCategory().contains(mItem)) {
						mNewList.add(m);
					} else if (m.getSource() != null && m.getSource().contains(mItem)) {
						mNewList.add(m);
					}
				}
			}
			
		}

		/*if (aType != null && aType.equals("c") && Integer.valueOf(request.getParameter("pageNumber").toString()) == 1) {
			List<ProductModel> mListOfProducts = AWSHelper.getAWSSearchData(aItem);
			if (mListOfProducts != null) {
				for (ProductModel pModel : mListOfProducts) {
					if (pModel == null) {
						continue;
					}
					mNewList.add(new SDModel(pModel));
				}
			}
		}
*/
		return mNewList;
	}
	
	
	private List<SDModel> getFilteredList(String aItem, String aType) {
		List<SDModel> mNewList = new ArrayList<SDModel>();
		
		if (iListOutput == null) {
			mNewList = dealsServiceImpl.findKeyWordInDeal(aItem);
		}else{
			for (SDModel m : iListOutput) {
				if (aItem == null) {
					continue;
				}
				
				String arr[] = aItem.split(",");
				for (String mItem : arr) {

					if (m.getText() != null && (m.getText().contains(mItem) || m.getText().toLowerCase().contains(mItem.toLowerCase()))) {
						mNewList.add(m);
					} else if (m.getCategory() != null && m.getCategory().contains(mItem)) {
						mNewList.add(m);
					} else if (m.getSource() != null && m.getSource().contains(mItem)) {
						mNewList.add(m);
					}
				}
			}
			
		}

		if (aType != null && aType.equals("c")) {
			List<ProductModel> mListOfProducts = AWSHelper.getAWSSearchData(aItem);
			if (mListOfProducts != null) {
				for (ProductModel pModel : mListOfProducts) {
					if (pModel == null) {
						continue;
					}
					mNewList.add(new SDModel(pModel));
				}
			}
		}

		return mNewList;
	}

	public void setMapOfData(List<SDModel> mapOfData) {
		this.iListOutput = mapOfData;
	}

	public void findMostCurrentDeals(List<SDModel> aOutputList) {
		try {
			List<SDModel> aOutputListNew = new ArrayList<SDModel>();

			if (iListOutput != null && aOutputList != null) {
				Map<String, SDModel> mMapOriginal = new HashMap<String, SDModel>();
				Map<String, SDModel> mMapNew = new HashMap<String, SDModel>();

				for (SDModel i : iListOutput)
					mMapOriginal.put(i.getText(), i);
				for (SDModel i : aOutputList)
					mMapNew.put(i.getText(), i);

				for (String m : mMapNew.keySet()) {
					if (m == null) {
						continue;
					}
					if (mMapOriginal.containsKey(m)) {
						mMapNew.get(m).setFirstSeen(mMapOriginal.get(m).getFirstSeen());
					}
				}
			}

			for (SDModel m : aOutputList) {
				if (m == null || m.getImageUrl() == null || m.isSdDeal()) {
					continue;
				}
				aOutputListNew.add(m);
			}

			if (aOutputListNew != null) {
				Collections.sort(aOutputListNew, new DateSorter());
				if (aOutputListNew.size() >= 9) {
					iListOutputLatest = aOutputListNew.subList(0, 9);
				} else {
					iListOutputLatest = aOutputListNew;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<SDModel> getListOutputLatest() {
		return iListOutputLatest;
	}

	public static boolean isCheckboxChecked(String aChkVal, String aListOfItems) {
		boolean aFlag = false;

		if (aChkVal == null || aListOfItems == null || aChkVal.equals("") || aListOfItems.equals("")) {
			return aFlag;
		}

		try {
			List<String> mList = Arrays.asList(aListOfItems.split(","));

			for (String mItem : mList) {

				if (aChkVal.indexOf(",") != -1) {
					// we need to see if atleast one item is present
					List<String> mListOfCheckValues = Arrays.asList(aChkVal.split(","));
					for (String mChkValItem : mListOfCheckValues) {
						if (mItem.equalsIgnoreCase(mChkValItem)) {
							aFlag = true;
							break;
						}
					}
				} else {
					if (mItem.equalsIgnoreCase(aChkVal)) {
						aFlag = true;
						break;
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return aFlag;

	}

 public void saveDealsData(List<SDModel> deals,DealCronJob dealCronJob){
	   List<Deal> tempdeals = convertSDdealModelToDeal(deals);
       dealsServiceImpl.saveDealsData(tempdeals, dealCronJob);
	}
 
 public List<Deal> convertSDdealModelToDeal(List<SDModel> deals){
	 List<Deal> tempdeals = new ArrayList();
	 List<Deal> dealsData = new ArrayList();
	 Map<String, Deal> duplicateDealsInScrap = new HashMap<String, Deal>();

	 for(SDModel deal :deals){
		 Deal tempDeal =  new Deal();
		 tempDeal.setUniqueId(deal.getUniqueId());
		 tempDeal.setUrl(deal.getUrl());
		 tempDeal.setText(deal.getText());
		 tempDeal.setImageUrl(deal.getImageUrl());
		 tempDeal.setCategory(deal.getCategory());
		 tempDeal.setSource(deal.getSource());
		 tempDeal.setRandom(deal.getRandom());
		 tempDeal.setOriginalPrice(deal.getOriginalPrice());
		 tempDeal.setSalePrice(deal.getSalePrice());
		 tempDeal.setFirstSeen(deal.getFirstSeen());
		 tempDeal.setSdDeal(deal.isSdDeal());
		 tempDeal.setAffliateLink(deal.getAffliateLink());
		 tempDeal.setCustomerReviewAverage(deal.getCustomerReviewAverage());
		 tempDeal.setCustomerReviewCount(deal.getCustomerReviewCount());
		 tempDeal.setOfferType(deal.getOfferType());
		 tempdeals.add(tempDeal);
		 tempDeal.setCategoryId(getCategoryIdForDeal(deal.getCategory())); 
		 tempDeal.setAddToCartUrl(deal.getAddToCartUrl());
		 duplicateDealsInScrap.put(tempDeal.getUniqueId(), tempDeal);
	 }
	 
	 for(Entry<String, Deal> entry: duplicateDealsInScrap.entrySet()) {
		 dealsData.add(entry.getValue());
	  }
	return dealsData;
	 
 }
 
 @Override
 public Long getCategoryIdForDeal(String categoryText){
	 	Long dealCategoryId = CategoryDealCacheUtility.get(categoryText);
	 	
		if (dealCategoryId == null) {
			DealCategory dealCategory = searchByCategoryByTextLevelOne(categoryText);
			if(dealCategory != null)
				dealCategoryId = dealCategory.getId();
		}

		/*if (dealCategoryId == null) {
			DealCategory dealCategory = searchByCategoryByTextLevelTwo(categoryText);
			if(dealCategory != null)
				dealCategoryId = dealCategory.getId();
		}*/
		
		if (dealCategoryId == null) {
			DealCategory dealCategory = searchByCategoryByTextLevelThree(categoryText);
			if(dealCategory != null)
				dealCategoryId = dealCategory.getId();
		}
		
		if(dealCategoryId == null){
			DealCategory dealCategory = searchByCategoryByTextLevelFive(categoryText);
			if(dealCategory != null)
				dealCategoryId = dealCategory.getId();
		}
		
		CategoryDealCacheUtility.put(categoryText, dealCategoryId);
		
		return dealCategoryId;
 }
 
 
 /* Input: Home,Decore & furniture|Home/Decore & furniture
  * Search: divide by pipe and then search for
  * 	- Home,Decore & furniture
  * 	- Home/Decore & furniture
  * */
 private DealCategory searchByCategoryByTextLevelOne(String categoryText){
	 DealCategory dealCatgory = null;
	 String [] categoryList = categoryText.split("\\|");
	 for (String categoryName : categoryList) {
		 List<DealCategory> dealCategoryList = dealCategoryService.findOneByDealCategoryname(categoryName);
		 if(dealCategoryList.size() > 0){
			dealCatgory= dealCategoryList.iterator().next();
			System.out.println("search technique :: searchByCategoryByTextLevelOne and categoryText :: " + categoryText + " and category :: " + categoryName
					+ " and match deal category :: " + dealCatgory);
		 }
	 }
	 return dealCatgory;
 }
 
 private DealCategory searchByCategoryByTextLevelTwo(String categoryText){
	 DealCategory dealCatgory = null;
	 String [] categoryList =  categoryText.split("\\|");
	 for (String categoryName : categoryList) {
		 String[] keywordList = categoryName.split("[\\s/&,]+");
		 String keywordText = "";
		 for (String keyword : keywordList) {
			 keywordText += "+" + keyword;
		}
		 List<DealCategory> dealCategoryList = (List<DealCategory>) dealCategoryService.fetchByKeywords(keywordText);
		 if(dealCategoryList.size() > 0){
			dealCatgory= dealCategoryList.iterator().next();
			System.out.println("search technique :: searchByCategoryByTextLevelTwo and categoryText :: " + categoryText + " and category :: " + categoryName
					+ " and match deal category :: " + dealCatgory);
			
		 }
	 }
	 return dealCatgory;
 }
 
 private DealCategory searchByCategoryByTextLevelThree(String categoryText){
	 DealCategory dealCatgory = null;
	 String [] categoryList =  categoryText.split("\\|");
	 for (String categoryName : categoryList) {
		 String[] categoryNameList = categoryName.split("[\\/,]+");
		 for (String keyword : categoryNameList) {
			 List<DealCategory> dealCategoryList = dealCategoryService.findOneByDealCategoryname(keyword);
			 if(dealCategoryList.size() > 0){
					dealCatgory= dealCategoryList.iterator().next();
					System.out.println("search technique :: searchByCategoryByTextLevelThree and categoryText :: " + categoryText + " and category :: " + categoryName
							+ " and match deal category :: " + dealCatgory);
				 }
		}
	 }
	 return dealCatgory;
 }
 
/* private DealCategory searchByCategoryByTextLevelFour(String categoryText){
	 DealCategory dealCatgory = null;
	 String [] categoryList =  categoryText.split("\\|");
	 for (String categoryName : categoryList) {
		 String[] categoryNameList = categoryName.split("[\\/,]+");
		 for (String keyword : categoryNameList) {
			 String[] keywordList = categoryName.split("[\\s/&,]+");
			 String keywordText = "";
			 for (String subkeyWord : keywordList) {
				 keywordText += "+" + subkeyWord;
			 }
			 List<DealCategory> dealCategoryList = (List<DealCategory>) dealCategoryService.fetchByKeywords(keywordText);
			 if(dealCategoryList.size() > 0){
				dealCatgory= dealCategoryList.iterator().next();
				System.out.println("search technique :: searchByCategoryByTextLevelFour and categoryText :: " + categoryText + " and category :: " + categoryName
						+ " and match deal category :: " + dealCatgory);
				
			}
		}
	 }
	 return dealCatgory;
 }
 */
	private DealCategory searchByCategoryByTextLevelFive(String categoryText) {
		DealCategory dealCatgory = null;
		String[] categoryList = categoryText.split("[\\|,/&]");
		for (String categoryName : categoryList) {
			List<DealCategory> dealCategoryList = dealCategoryService.findOneByDealCategoryname(categoryName.trim());
			if (dealCategoryList.size() > 0) {
				dealCatgory = dealCategoryList.iterator().next();
				System.out.println(
						"search technique :: searchByCategoryByTextLevelFive and categoryText :: " + categoryText
								+ " and category :: " + categoryName + " and match deal category :: " + dealCatgory);
			}
		}
		return dealCatgory;
	}

	public List<SDModel> getMapOfDataByScrapper(HttpServletRequest request) {
		List<SDModel> mNewList = new ArrayList<SDModel>();
		String aItem = request.getParameter("frmSearchParam");

			List<ProductModel> mListOfProducts = AWSHelper.getAWSSearchData(aItem);
			if (mListOfProducts != null) {
				for (ProductModel pModel : mListOfProducts) {
					if (pModel == null) {
						continue;
					}
					mNewList.add(new SDModel(pModel));
				}
			}
		return mNewList;
	}
 
/* private DealCategory searchByAllContaingWord(String categoryName){
	 DealCategory dealCatgory = null;
	 String[] keywordList = categoryName.split("[\\s/&,]+");
	 String keywordText = "";
	 for (String keyword : keywordList) {
		 keywordText += "+" + keyword;
	}
	dealCategoryService.fetchByKeywords(keywordText);
	return dealCatgory;
 }
 
 private DealCategory searchByAllContaingWordWithSplitting(String categoryName){
	 DealCategory dealCatgory = null;
	 String[] keywordList = categoryName.split("[\\s/&,]+");
	 String keywordText = "";
	 for (String keyword : keywordList) {
		 keywordText += "+" + keyword;
	}
	dealCategoryService.fetchByKeywords(keywordText);
	return dealCatgory;
 }*/
	
}

class PriceSorter implements Comparator<SDModel> {

	@Override
	public int compare(SDModel o1, SDModel o2) {
		// TODO Auto-generated method stub

		if (o1 != null && o2 != null && o1.getOriginalPrice() > 0 && o1.getSalePrice() > 0 && o2.getOriginalPrice() > 0
				&& o2.getSalePrice() > 0) {

			if (o1.getOriginalPrice() == 0 || o1.getSalePrice() == 0) {
				return -1;
			} else if (o2.getOriginalPrice() == 0 || o2.getSalePrice() == 0) {
				return 1;
			} else {
				double salePctO1 = (o1.getOriginalPrice() - o1.getSalePrice()) * 100 / (o1.getOriginalPrice());
				double salePctO2 = (o2.getOriginalPrice() - o2.getSalePrice()) * 100 / (o2.getOriginalPrice());
				return new Double(salePctO1).compareTo(new Double(salePctO2));
			}
		}
		return 0;
	}
}

class DateSorter implements Comparator<SDModel> {

	@Override
	public int compare(SDModel a1, SDModel a2) {

		// reverse sorting
		return a2.getFirstSeen().compareTo(a1.getFirstSeen());
	}
}

class CustomSorter implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		if (ScrapperServiceImpl.ORDERED_ENTRIES.contains(o1) && ScrapperServiceImpl.ORDERED_ENTRIES.contains(o2)) {
			// Both objects are in our ordered list. Compare them by
			// their position in the list
			return ScrapperServiceImpl.ORDERED_ENTRIES.indexOf(o1) - ScrapperServiceImpl.ORDERED_ENTRIES.indexOf(o2);
		}

		if (ScrapperServiceImpl.ORDERED_ENTRIES.contains(o1)) {
			// o1 is in the ordered list, but o2 isn't. o1 is smaller (i.e. first)
			return -1;
		}

		if (ScrapperServiceImpl.ORDERED_ENTRIES.contains(o2)) {
			// o2 is in the ordered list, but o1 isn't. o2 is smaller (i.e. first)
			return 1;
		}

		if (o1 == null || o2 == null) {
			return 0;
		}

		return o1.toString().compareTo(o2.toString());
	}

}
