package com.giftcard.scrapper.job;


public interface ScrapperService {
	void populateData();
	Long getCategoryIdForDeal(String categoryText);
}
