package com.giftcard.filter.specification;

import java.time.chrono.ChronoLocalDate;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.giftcard.filter.Converters;
import com.giftcard.filter.FilterCriteria;
import com.giftcard.filter.FilterSpecifications;
import com.giftcard.model.Deal;



/**
 * 
 * Specification for Employee Entity
 * 
 *
 */
@Service
public class DealFilterSpecification {
	
	
	/**
	 * {@link FilterSpecifications} for Entity {@link Employee} and Field type {@link ChronoLocalDate} (LocalDate)
	 */
	@Autowired
	private FilterSpecifications<Deal, ChronoLocalDate> dateTypeSpecifications;

	/**
	 * {@link FilterSpecifications} for Entity {@link Employee} and Field type {@link String}
	 */
	@Autowired
	private FilterSpecifications<Deal, String> stringTypeSpecifications;

	/**
	 * {@link FilterSpecifications} for Entity {@link Employee} and Field type {@link Integer}
	 * 
	 */
	@Autowired
	private FilterSpecifications<Deal, Integer> integerTypeSpecifications;

	/**
	 * {@link FilterSpecifications} for Entity {@link Employee} and Field type {@link Long}
	 */
	@Autowired
	private FilterSpecifications<Deal, Long> longTypeSpecifications;
	
	
	/**
	 * Converter Functions
	 */
	@Autowired
	private Converters converters;
	
	
	/**
	 * Returns the Specification for Entity {@link Employee} for the given fieldName and filterValue for the field type Date
	 * 
	 * @param fieldName
	 * @param filterValue
	 * @return
	 */
	public Specification<Deal> getDateTypeSpecification(String fieldName, String filterValue) {
		return getSpecification(fieldName, filterValue, converters.getFunction(ChronoLocalDate.class), dateTypeSpecifications);
	}

	/**
	 * Returns the Specification for Entity {@link Employee} for the given fieldName and filterValue for the field type String
	 * @param fieldName
	 * @param filterValue
	 * @return
	 */
	public Specification<Deal> getStringTypeSpecification(String fieldName, String filterValue) {
		return getSpecification(fieldName, filterValue, converters.getFunction(String.class), stringTypeSpecifications);
	}
	
	/**
	 * Returns the Specification for Entity {@link Employee} for the given fieldName and filterValue for the field type Long
	 * 
	 * @param fieldName
	 * @param filterValue
	 * @return
	 */
	public Specification<Deal> getLongTypeSpecification(String fieldName, String filterValue) {
		return getSpecification(fieldName, filterValue, converters.getFunction(Long.class), longTypeSpecifications);
	}
	
	
	/**
	 * Returns the Specification for Entity {@link Employee} for the given fieldName and filterValue for the field type Integer
	 * 
	 * @param fieldName
	 * @param filterValue
	 * @return
	 */
	public Specification<Deal> getIntegerTypeSpecification(String fieldName, String filterValue) {
		return getSpecification(fieldName, filterValue, converters.getFunction(Integer.class), integerTypeSpecifications);
	}

	/**
	 * Generic method to return {@link Specification} for Entity {@link Employee}
	 * 
	 * @param fieldName
	 * @param filterValue
	 * @param converter
	 * @param specifications
	 * @return
	 */
	private <T extends Comparable<T>> Specification<Deal> getSpecification(String fieldName,
			String filterValue, Function<String, T> converter, FilterSpecifications<Deal, T> specifications) {

		if (StringUtils.isNotBlank(filterValue)) {
			
			//Form the filter Criteria
			FilterCriteria<T> criteria = new FilterCriteria<>(fieldName, filterValue, converter);
			return specifications.getSpecification(criteria.getOperation()).apply(criteria);
		}

		return null;

	}
	
}