package com.giftcard.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("application.properties")
public class AppProperties {
 
	@Value(value = "${mail.email}")
    public String email;
	
	@Value(value = "${mail.password}")
	public String password;
	
	@Value(value = "${mail.feedbackemail}")
    public String feedbackemail;
	
	@Value(value = "${mail.feedbackpassword}")
	public String feedbackpassword;
	
	@Value(value = "${mail.smtp.host}")
	public String host;
	
	@Value(value = "${mail.smtp.port}")
	public String port;
	
	@Value(value = "${mail.smtp.auth}")
	public String auth;
	
	@Value(value = "${mail.smtp.starttls.enable}")
	public String enable;

	@Value(value = "${mail.template.verifyemail}")
	public String templateVerifyemail;
	
	@Value(value = "${server.url}")
	public String serverUrl;
	
	@Value(value = "${mail.template.verifyregistration}")
	public String verifyregistration;
	
	@Value(value = "${mail.template.forgetpassword}")
	public String forgetPassword;
	
	@Value(value = "${mail.template.resetPassword}")
	public String resetPassword;
	
	@Value(value = "${server.ui.resetpassword.url}")
	public String resetPasswordUIURL;
	
	@Value(value = "${server.port}")
	public String serverPort;
	
	
	@Value(value = "${mail.template.adminregistration}")
	public String adminRegistration;
	
	@Value(value = "${globalOnePayURL}")
	public String globalOnePayURL;
	
	@Value(value = "${globalOnePaySecret}")
	public String globalOnePaySecret;
	
	@Value(value = "${globalOnePayTerminalId}")
	public String globalOnePayTerminalId;

	public String getGlobalOnePayURL() {
		return globalOnePayURL;
	}

	public void setGlobalOnePayURL(String globalOnePayURL) {
		this.globalOnePayURL = globalOnePayURL;
	}

	public String getGlobalOnePaySecret() {
		return globalOnePaySecret;
	}

	public void setGlobalOnePaySecret(String globalOnePaySecret) {
		this.globalOnePaySecret = globalOnePaySecret;
	}

	public String getGlobalOnePayTerminalId() {
		return globalOnePayTerminalId;
	}

	public void setGlobalOnePayTerminalId(String globalOnePayTerminalId) {
		this.globalOnePayTerminalId = globalOnePayTerminalId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	public String getTemplateVerifyemail() {
		return templateVerifyemail;
	}

	public void setTemplateVerifyemail(String templateVerifyemail) {
		this.templateVerifyemail = templateVerifyemail;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public String getVerifyregistration() {
		return verifyregistration;
	}

	public void setVerifyregistration(String verifyregistration) {
		this.verifyregistration = verifyregistration;
	}

	public String getForgetPassword() {
		return forgetPassword;
	}

	public void setForgetPassword(String forgetPassword) {
		this.forgetPassword = forgetPassword;
	}

	public String getResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}

	public String getResetPasswordUIURL() {
		return resetPasswordUIURL;
	}

	public void setResetPasswordUIURL(String resetPasswordUIURL) {
		this.resetPasswordUIURL = resetPasswordUIURL;
	}

	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}

	public String getAdminRegistration() {
		return adminRegistration;
	}

	public void setAdminRegistration(String adminRegistration) {
		this.adminRegistration = adminRegistration;
	}

	public String getFeedbackemail() {
		return feedbackemail;
	}

	public void setFeedbackemail(String feedbackemail) {
		this.feedbackemail = feedbackemail;
	}

	public String getFeedbackpassword() {
		return feedbackpassword;
	}

	public void setFeedbackpassword(String feedbackpassword) {
		this.feedbackpassword = feedbackpassword;
	}

	
	
}