package com.giftcard;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;

import com.braintreegateway.BraintreeGateway;
import com.giftcard.factory.BraintreeGatewayFactory;
import com.giftcard.security.CorsFilter;


@SpringBootApplication
@EnableAutoConfiguration
@EnableCaching
public class Application {
	
	public static String DEFAULT_CONFIG_FILENAME = "config.properties";
	public static BraintreeGateway gateway;
	public static Date Time = new Date();
	public static Date getTime() {
		return Time;
	}
	public static void setTime(Date time) {
		Time = time;
	}

	public static void main(String[] args) throws IOException {
		
		InputStream configFile = Application.class.getClassLoader().getResourceAsStream(DEFAULT_CONFIG_FILENAME);
		
		try {
			if (configFile.available() > 0) {
				gateway = BraintreeGatewayFactory.fromConfigFile(configFile);
			} else {
				gateway = BraintreeGatewayFactory.fromConfigMapping(System.getenv());
			}
		} catch (NullPointerException e) {
			System.err.println("Could not load Braintree configuration from config file or system environment.");
			System.exit(1);
		}

		SpringApplication.run(Application.class, args);
	}
	
	

	@Bean
	public CorsFilter someFilterRegistration() {

		CorsFilter registration = new CorsFilter();

		return registration;
	}

	public void loadBraingreeConfig() {

	}

}
