package com.giftcard.service;

import java.io.File;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.giftcard.model.DealCategory;

public interface DealCategoryService {

	public List<DealCategory> findOneByDealCategoryname(String categoryName);

	public DealCategory save(DealCategory dealCategory);

	public Collection<DealCategory> fetchMainCategoryForSet(HttpServletRequest request);

	public Collection<DealCategory> fetchDealCategoryByParentId(Long parentId);

	public DealCategory findById(Long dealCategoryId);

	public void delete(Long dealCategoryId);

	public DealCategory updateDealCategory(DealCategory dealCategory, DealCategory tempdealCategory);

	public Long searchCount(HttpServletRequest request);

	public void addCSVDataToDB(File file);

	public Collection<DealCategory> getDealByIds(Collection<DealCategory> dealCategory);

	public Collection<DealCategory> findAllDealCategoryByParentId();

	public DealCategory getDealById(Long id);

	public Collection<DealCategory> findAllDealCategory();

	//public Map<Integer, List<DealCategory>> searchDealCategoryByKeyword(String keyWord);

	public Collection<DealCategory> findOneByDealCategory(String keyWord);

	public Collection<DealCategory> fetchAllTreeHierarchy();

	Collection<DealCategory> fetchByKeywords(String keywordText);

	public Collection<DealCategory> fetchDealCategoryByPriority();

	public Collection<DealCategory> fetchAllDealCategoryTree(HttpServletRequest request);

	public Long getAllDealCategoryTreeCount(HttpServletRequest request);


}
