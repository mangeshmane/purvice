package com.giftcard.service;

import java.util.List;

import com.giftcard.model.Orders;
import com.giftcard.model.User;
import com.tangocard.client.model.CreateOrderCriteria;
import com.tangocard.client.model.OrderViewSummary;
import com.tangocard.client.model.ResendView;


public interface OrdersService {

	public List<Orders> findAll();
	
	public List<Orders> findByIsscheduled(boolean b);
	
	public Orders emailOrder(User user, Orders order);
	
	public OrderViewSummary sendOrder(CreateOrderCriteria createOrderCriteria);
	
	public ResendView resendOrder(String referenceOrderID);
	
	public List<Orders> findByUser(long id);
	
	public void save(Orders order);
	
	public CreateOrderCriteria populateTangoOrder(Orders order);
	
	public Orders findById(Long id);
	
	public String createString(OrderViewSummary orderViewSummary);
	
	public void sendSMS (Orders orders);
	
	public void sendScheduledOrder(Orders order);
	
	public void delete(long id);
	
	public Orders updateOrderData(Orders dbOrder, Orders order);
	
	public void updateOrderStatus();
	
	public List<Orders> findByTransaction(String transactionStatus);
	
	public Long countByUser_idAndTangoOrderStatusNotLike(long id, String status);
}
