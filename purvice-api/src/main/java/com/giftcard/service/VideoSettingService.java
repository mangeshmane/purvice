package com.giftcard.service;

import com.giftcard.model.VideoSetting;

public interface VideoSettingService {

	public VideoSetting findOne(long id);
	
	public VideoSetting save (VideoSetting videoSetting );
}
