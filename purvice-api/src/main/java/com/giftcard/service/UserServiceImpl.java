package com.giftcard.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.giftcard.model.User;
import com.giftcard.properties.AppProperties;
import com.giftcard.repository.UserRepository;
import com.giftcard.util.ChangePasswordUtil;
import com.giftcard.util.EmailUtil;


@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AppProperties appProperties;
	
	@Value(value = "${server.verifyEmail.url}")
	private String verify_URL;
	@Override
	public User findByUsernameAndPassword(String username, String password) {
		User user = userRepository.findByUsername(username);
		if (user != null) {
			if (!this.passwordMatcher(username, password)) {
				user = null;
			}
		} else {
			user = null;
		}
		return user;
	}

	

	@Override
	public User createUser(User user) {
		List<String> roles = new ArrayList<>();
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hasPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(hasPassword);
		roles.add("USER");
		user.setRoles(roles);
		user = userRepository.save(user);
		return user;
	}
	
	public User createAdmin(User user) {
		List<String> roles = new ArrayList<>();
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hasPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(hasPassword);
		roles.clear();
		roles.add("ADMIN");
		user.setRoles(roles);
		return user;
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public List<User> findAll() {

		return userRepository.findAll();
	}

	@Override
	public User findOne(long id) {

		return userRepository.findOne(id);
	}

	@Override
	public void delete(long id) {

		userRepository.delete(id);

	}

	public User setPassword(User user) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hasPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(hasPassword);
		return user;
	}

	@Override
	public User save(User user) {

		return userRepository.save(user);
	}

	@Override
	public void sendEmail(String toEmail, String toSubject, String toBody) {
		final String fromEmail = appProperties.getEmail(); // requires valid
															// gmail id
		final String password = appProperties.getPassword(); // correct password
																// for gmail id

		Properties props = new Properties();
		props.put("mail.smtp.host", appProperties.getHost()); // SMTP Host
		props.put("mail.smtp.port", appProperties.getPort()); // TLS Port
		props.put("mail.smtp.auth", appProperties.getAuth()); // enable
																// authentication
		props.put("mail.smtp.starttls.enable", appProperties.getEnable()); // enable
																			// STARTTLS
		props.put("mail.smtp.ssl.trust", "*");

		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		Session session = Session.getInstance(props, auth);

		EmailUtil.sendEmail(session, toEmail, toSubject, toBody);

	}
	
	
	@Override
	public void sendFeedbackEmail(String toEmail, String toSubject, String toBody) {
		final String fromEmail = appProperties.getEmail(); // requires valid
																// gmail id
		final String password = appProperties.getPassword(); // correct password
        																// for gmail id

		String sendEmail = appProperties.getFeedbackemail();
		
		Properties props = new Properties();
		props.put("mail.smtp.host", appProperties.getHost()); // SMTP Host
		props.put("mail.smtp.port", appProperties.getPort()); // TLS Port
		props.put("mail.smtp.auth", appProperties.getAuth()); // enable
																// authentication
		props.put("mail.smtp.starttls.enable", appProperties.getEnable()); // enable
																			// STARTTLS
		props.put("mail.smtp.ssl.trust", "*");

		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		Session session = Session.getInstance(props, auth);

		EmailUtil.sendFeedbackEmail(session, sendEmail, toSubject, toBody, toEmail);

	}
	
	@Override
	public User resetPassword(User user) {

		User dbUser = this.findByUsername(user.getUsername().toLowerCase());

		if (dbUser != null) {
			dbUser.setPasswordResetToken(UUID.randomUUID().toString()+ ":" + System.currentTimeMillis());
			this.save(dbUser);
		}

		String toSubject = appProperties.getForgetPassword();
		String toBody = appProperties.getResetPassword();
		String urlResetPassword = appProperties.getResetPasswordUIURL().replace("PASSWORD_RESET_TOKEN",
				dbUser.getPasswordResetToken());
		// String urlResetPassword =
		// appProperties.getServerUrl()+"/"+"resetpassword.html";
		toBody = toBody.replace("RESETPASSWORD_URL", urlResetPassword);

		this.sendEmail(dbUser.getUsername(), toSubject, toBody);

		return dbUser;
	}

	public User createUserInfo(User registration) {
		String emailID = registration.getUsername().toLowerCase();
		registration.setUsername(emailID);
		registration.setEmailId(emailID);
		registration.setCreatedDate(new Date());
		if(!registration.isVerify()){
		registration.setToken(UUID.randomUUID().toString()+ ":" + System.currentTimeMillis());
		}
		this.createUser(registration);
		if(!registration.isVerify()){
		String toSubject = appProperties.getVerifyregistration();
		String toBody = appProperties.getTemplateVerifyemail();
		String urlVerification = verify_URL+ registration.getToken();

		toBody = toBody.replace("EMAIL_ID", registration.getUsername()).replace("VERIFY_URL", urlVerification);
		this.sendEmail(registration.getUsername(), toSubject, toBody);
		}
		return registration;
	}
	
	@Override
	public User createAdminInfo(User registration, String adminName) {
		String emailID = registration.getUsername().toLowerCase();
		registration.setUsername(emailID);
		registration.setEmailId(emailID);
		registration.setCreatedDate(new Date());
		registration.setPasswordResetToken(UUID.randomUUID().toString()+ ":" + System.currentTimeMillis());
		User admin = this.createAdmin(registration);
		String toSubject = appProperties.getVerifyregistration();
		
		String toBody = appProperties.getAdminRegistration();
		String urlResetPassword = appProperties.getResetPasswordUIURL().replace("PASSWORD_RESET_TOKEN",
				admin.getPasswordResetToken());
		toBody = toBody.replace("RESETPASSWORD_URL", urlResetPassword);
		toBody = toBody.replace("XYZ", admin.getFirstName());
		toBody = toBody.replace("ABC", adminName);
		this.sendEmail(admin.getUsername(), toSubject, toBody);

		return admin;
	}

	@Override
	public User findByToken(String token) {

		return userRepository.findByToken(token);
	}

	@Override
	public User findByPasswordResetToken(String token) {

		return userRepository.findByPasswordResetToken(token);

	}

	@Override
	public User changePassword(ChangePasswordUtil userDetails) {

		User user = null;
		user = this.findByUsername(userDetails.getUsername());
		boolean ismatched = this.passwordMatcher(userDetails.getUsername(), userDetails.getOldPassword());
		if (ismatched) {
			user.setPassword(userDetails.getNewPassword());
			User dbUser = this.setPassword(user);
			user = this.save(user);
			this.sendEmail(user.getUsername(), "Reset Password",
					"Hi " + user.getFirstName() + ", \nYour Password changed successfully.");
		} else {
			user = null;
		}
		return user;
	}

	public Boolean passwordMatcher(String username, String password) {
		boolean isMatched = false;
		User user = this.findByUsername(username);
		if (user != null) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			if (passwordEncoder.matches(password, user.getPassword())) {
				isMatched = true;
			} else {
				isMatched = false;
			}
		} else {
			isMatched = false;
		}
		return isMatched;
	}



	@Override
	public User findByEmailId(String emailId) {
		return userRepository.findByEmailId(emailId);
	}
}
