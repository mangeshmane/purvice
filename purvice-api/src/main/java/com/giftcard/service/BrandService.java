
package com.giftcard.service;

import java.util.List;

import com.giftcard.model.Brand;

public interface BrandService {

	public Brand findById(long id);
	
	public void deleteById(long id);
	
	public Brand findByBrandName(String brandName);
	
	public List<Brand> findAll();
	
	public Brand save(Brand brand);
	
	public Brand update(Brand brand);
	
	public Brand findByBrandKey(String BrandKey);
	
	//public List<Brand> findByBrandNameLike(String brandName);
}
