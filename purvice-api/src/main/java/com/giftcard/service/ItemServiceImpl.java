package com.giftcard.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.giftcard.model.Item;
import com.giftcard.model.ItemRowMapper;
import com.giftcard.repository.ItemRepository;



@Service
public class ItemServiceImpl implements ItemService{

	@Autowired
	private ItemRepository itemRepository;
	
	 @Autowired
	 @Qualifier("jdbcTemplate")
	 private JdbcTemplate jdbcTemplate;
	 
	@Override
	public Item findById(long id) {
		return itemRepository.findById(id);
	}

	@Override
	public Item deleteById(long id) {
		return itemRepository.deleteById(id);
	}

	@Override
	public Item save(Item item) {
		return itemRepository.save(item);
	}
	
	@Override
	public List<Item> search(Long categroyId, Long brandId, String searchQuery) {
		String select_Part = null;
		StringBuilder where_Part = new StringBuilder();
		StringBuilder queryBuilder = new StringBuilder();
		List<Item> items = null;
		
		select_Part="select i.*  from Item i "+
		      " inner join Brand b on i.brand_id=b.id ";
		queryBuilder.append(select_Part);
		
		          if(categroyId != null || brandId != null || searchQuery != null){
		        	  where_Part.append(" where ");
		           }
	               if(categroyId != null){
	               where_Part.append(" c.category_id = "+ categroyId);
	               queryBuilder.append(" inner join category_item c on i.id = c.Item_id ");
	               
	               }
	               if(brandId != null){
	            	   if(where_Part.length()>7){
	            		   where_Part.append(" and b.id = "+ brandId);
	            	   }else{
	            		   where_Part.append(" b.id="+ brandId);
	            	   }
	               }
	               if(searchQuery != null){
	            	   if(where_Part.length()>7){
	            		   where_Part.append(" and b.brandName like  "+"'%"+searchQuery+"%'");
	            	   }else{
	            		   where_Part.append(" b.brandName like "+"'%"+searchQuery+"%'");
	            	   }
	               }
	               if(where_Part != null){
	            	   queryBuilder.append(where_Part);
	               }
	               
	                 
		try{
			items = jdbcTemplate.query(queryBuilder.toString(), new ItemRowMapper());
		
		}catch(Exception e){
			
		}
		
        return items;
	}

	@Override
	public List<Item> findByCategory_id(long id) {
		
		return itemRepository.findByCategory_id(id);
	}

	@Override
	public Item findByUtid(String utid) {
		
		return itemRepository.findByUtid(utid);
	}

}
