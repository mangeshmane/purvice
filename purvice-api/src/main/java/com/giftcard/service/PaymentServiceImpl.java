package com.giftcard.service;

import org.eclipse.jdt.internal.compiler.codegen.IntegerCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giftcard.model.PaymentDetails;
import com.giftcard.properties.AppProperties;

@Service
public class PaymentServiceImpl implements PaymentService {

	
	@Autowired
	private AppProperties appProperties;
	
	@Override
	public PaymentDetails getAll() {
		// TODO Auto-generated method stub
		
		PaymentDetails paymentDetails = new PaymentDetails();
		
		paymentDetails.setGlobalOnePaySecret(appProperties.getGlobalOnePaySecret());
		paymentDetails.setGlobalOnePayURL(appProperties.getGlobalOnePayURL());
		paymentDetails.setGlobalOnePayTerminalId(Integer.parseInt(appProperties.getGlobalOnePayTerminalId()));
		return paymentDetails;
	}

}
