
package com.giftcard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.giftcard.model.Brand;
import com.giftcard.model.BrandImage;
import com.giftcard.model.Item;
import com.giftcard.model.ItemCountry;
import com.giftcard.repository.BrandImageRespository;
import com.giftcard.repository.BrandRepository;
import com.giftcard.repository.ItemCountryRepository;
import com.giftcard.repository.ItemRepository;

@Service
@Transactional
public class BrandServiceImpl implements BrandService {

	@Autowired
	BrandRepository brandRepository;

	@Autowired
	BrandImageRespository brandImageRespository;

	@Autowired
	ItemRepository itemRepository;

	@Autowired
	ItemCountryRepository itemCountryRepository;

	@Override
	public Brand findByBrandName(String brandName) {
		return brandRepository.findByBrandName(brandName);
	}

	@Override
	@Transactional
	public Brand save(Brand brand) {
		Brand newBrand = brandRepository.save(brand);
		if (newBrand != null) {
			return this.saveItemAndImages(newBrand);
		}
		return null;
	}

	@Transactional
	public Brand saveItemAndImages(Brand brand) {

		if (brand.getImageUrls() != null && !brand.getImageUrls().isEmpty()) {
			List<BrandImage> imageUrls = brand.getImageUrls();
			for (BrandImage brandImage : imageUrls) {
				brandImage.setBrand(brand);
				brandImageRespository.save(brandImage);
			}
			brand.setImageUrls(imageUrls);
		}

		if (brand.getItems() != null && !brand.getItems().isEmpty()) {
			List<Item> items = brand.getItems();
			for (Item item : items) {
				item.setBrand(brand);
				List<ItemCountry> countries = item.getCountries();
				itemRepository.save(item);
				for (ItemCountry itemCountry : countries) {
					itemCountry.setItem(item);
					itemCountryRepository.save(itemCountry);
				}

			}
			brand.setItems(items);
		}

		return brand;
	}

	@Override
	public Brand findById(long id) {
		return brandRepository.findById(id);
	}

	@Override
	public void deleteById(long id) {
		brandRepository.delete(id);
	}

	@Override
	public List<Brand> findAll() {
		return brandRepository.findAll();
	}

	@Override
	public Brand findByBrandKey(String brandKey) {
		return brandRepository.findByBrandKey(brandKey);
	}

	@Override
	public Brand update(Brand brand) {
		return brandRepository.save(brand);
	}

	/*
	 * @Override public List<Brand> findByBrandNameLike(String brandName) {
	 * 
	 * return brandRepository.findByBrandNameLike(brandName); }
	 */
}
