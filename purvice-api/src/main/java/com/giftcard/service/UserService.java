package com.giftcard.service;

import java.util.List;

import com.giftcard.model.User;
import com.giftcard.util.ChangePasswordUtil;

public interface UserService {

	public User findByUsernameAndPassword(String email, String Passwrd);
	
	public User createUser(User user);
	
	public User findByUsername(String username);
	
	public User findByEmailId(String emailId);
	
	public List<User> findAll();
	
	public User findOne(long id);
	
	public void delete(long id);
	
	public User save(User user);
	
	public User setPassword(User appUser);
	
	public void sendEmail(String toEmail, String toSubject, String toBody);
	
	public User createUserInfo(User registration);
	
	public User findByToken(String token);
	
	public User resetPassword(User user);
	
	public User findByPasswordResetToken(String token);
	
	public User changePassword(ChangePasswordUtil userDetails);
	
	public User createAdminInfo(User registration, String adminName);
	
	public void sendFeedbackEmail(String toEmail, String toSubject, String toBody);
	

}
