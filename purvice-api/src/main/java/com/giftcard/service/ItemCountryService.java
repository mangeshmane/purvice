package com.giftcard.service;

import java.util.List;

import com.giftcard.model.ItemCountry;

public interface ItemCountryService {

	public List<ItemCountry> findByItem_id(long id);
	
}
