package com.giftcard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giftcard.model.ItemCountry;
import com.giftcard.repository.ItemCountryRepository;

@Service
public class ItemCountryServiceImpl implements ItemCountryService {

	@Autowired
	ItemCountryRepository itemCountryRepository;
	@Override
	public List<ItemCountry> findByItem_id(long id) {
		
		return itemCountryRepository.findByItem_id(id);
	}

}
