package com.giftcard.service;

import java.util.List;

import com.giftcard.model.BillingAddress;
import com.giftcard.model.States;

public interface BillingAddressService {

	public List<States> findAllStates();
	
	public BillingAddress save(BillingAddress billingAddress);
	
	public BillingAddress findByUser_id(long id);
	
}
