package com.giftcard.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giftcard.model.ItemDenomination;
import com.giftcard.repository.ItemDenominationRepository;

@Service
public class ItemDenominationServiceImpl implements ItemDenominationService {

	@Autowired
	public ItemDenominationRepository itemDenominationRepository;
	
	@Override
	public ItemDenomination findByItemId(long id) {
		
		return itemDenominationRepository.findByItemId(id);
	}
	@Override
	public void save(ItemDenomination itemDenomination) {
		itemDenominationRepository.save(itemDenomination);
		
	}

}
