package com.giftcard.service;

import com.giftcard.model.PaymentDetails;

public interface PaymentService {

	public PaymentDetails getAll();
}
