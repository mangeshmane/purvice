package com.giftcard.service;

import com.giftcard.model.ItemDenomination;

public interface ItemDenominationService {

	public ItemDenomination findByItemId(long id);
	
	public void save(ItemDenomination itemDenomination);
}
