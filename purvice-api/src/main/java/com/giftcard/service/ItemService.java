package com.giftcard.service;

import java.util.List;

import com.giftcard.model.Item;

public interface ItemService {

	public Item findById(long id);
	public Item deleteById(long id);	
	public Item save(Item item);
	public List<Item> search(Long categoryId, Long brandId, String searchQuery);
	public List<Item> findByCategory_id(long id);
	public Item findByUtid(String utid);
}
