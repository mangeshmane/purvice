package com.giftcard.service;

import java.util.List;

import com.giftcard.model.Category;

public interface CategoryService {

	public Category findOneByCategoryname(String categoryName);
	
	public Category save(Category category);
	
	public void delete(long id);
	
	public List<Category> findAll();
	
	public Category findOne(long id);
}
