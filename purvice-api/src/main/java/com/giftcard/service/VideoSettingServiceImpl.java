package com.giftcard.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giftcard.model.VideoSetting;
import com.giftcard.repository.VideoSettingRepository;

@Service
public class VideoSettingServiceImpl implements VideoSettingService {

	@Autowired
	private VideoSettingRepository videoSettingRepository;

	
	
	@Override
	public VideoSetting findOne(long id) {
		
		return videoSettingRepository.findOne(id);
	}

	@Override
	public VideoSetting save(VideoSetting videoSetting) {
	
		return videoSettingRepository.save(videoSetting);
		
	}

}
