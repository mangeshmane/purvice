package com.giftcard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.giftcard.model.Category;
import com.giftcard.repository.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	CategoryRepository categoryRepository;

	@Override
	public Category findOneByCategoryname(String categoryname) {
				
		return categoryRepository.findOneByCategoryname(categoryname);
	}

	@Value(value = "${server.file.folder}")
	private String UPLOADED_FOLDER;

	@Override
	public List<Category> findAll() {
		
		return categoryRepository.findAll();
	}

	@Override
	public Category findOne(long id) {
		
		return categoryRepository.findOne(id);
	}



	@Override
	public Category save(Category category) {
	Category newCategory = categoryRepository.save(category);
		if(newCategory != null){
			return newCategory;
		}
		return null;
	}



	@Override
	public void delete(long id) {
		
		
		 categoryRepository.delete(id);
	}
	
	

}
