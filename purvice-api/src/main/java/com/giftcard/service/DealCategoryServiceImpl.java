package com.giftcard.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.giftcard.deal.service.DealCategoryNode;
import com.giftcard.deal.service.DeleteDealCategoryPermanatlyVisitor;
import com.giftcard.model.DealCategory;
import com.giftcard.repository.DealCategoryRepository;
import com.giftcard.tree.Direction;
import com.giftcard.tree.TreeModel;
import com.giftcard.tree.TreeWalker;

@Service
public class DealCategoryServiceImpl implements DealCategoryService {

	@Autowired
	@Qualifier("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	Map<String, String> dealCategorySetQueryMap;

	@Autowired
	DealCategoryRepository dealCategoryRepository;

	@Autowired
	private DeleteDealCategoryPermanatlyVisitor deleteDealCategoryPermanatlyVisitor;

	public DealCategoryServiceImpl() {
		initializeSqls();
	}

	private void initializeSqls() {
		Map<String, String> sqlMap = new HashMap<String, String>();

		String seardealCategorySQL = "select * from DealCategory as dealCategory " + " {1} " + " WHERE {0} ";
		sqlMap.put("seardealCategorySQL", seardealCategorySQL);

		String countdealCategorySQL = "select count(*) as totalCount from DealCategory as dealCategory " + " {1} "
				+ " WHERE {0} ";
		sqlMap.put("countdealCategorySQL", countdealCategorySQL);

		dealCategorySetQueryMap = Collections.unmodifiableMap(sqlMap);
	}

	@Override
	public List<DealCategory> findOneByDealCategoryname(String categoryName) {
		String select_Part = null;
		List<DealCategory> dealCategory = null;
		select_Part = "select dc.*  from DealCategory dc where dc.category_name LIKE " + '"'+categoryName+'"';
		dealCategory = jdbcTemplate.query(select_Part, dealCategoryMapper);
		return dealCategory;
	}

	@Override
	public List<DealCategory> findOneByDealCategory(String categoryName) {
		String select_Part = null;
		List<DealCategory> dealCategory = null;
		select_Part = "select dc.*  from DealCategory dc where dc.category_name LIKE " + "'%"+categoryName+"%'";
		dealCategory = jdbcTemplate.query(select_Part, dealCategoryMapper);
		return dealCategory;
	}

	@Override
	public DealCategory save(DealCategory dealCategory) {
		DealCategory newDealCategory = dealCategoryRepository.save(dealCategory);
		if(newDealCategory != null){
			return newDealCategory;
		}
		return null;
	}

	private static RowMapper<DealCategory> dealCategoryMapper = new RowMapper<DealCategory>() {
		@Override
		public DealCategory mapRow(ResultSet rs, int rowNum) throws SQLException {
			Map<Long, DealCategory> map = new HashMap<Long, DealCategory>();
			DealCategory dealCategory = new DealCategory();
			dealCategory.setId(rs.getLong("id"));
			dealCategory.setCategoryName(rs.getString("category_name"));
			dealCategory.setDescription(rs.getString("description"));
			dealCategory.setCategoryImage(rs.getString("category_image"));
			dealCategory.setParentId(rs.getLong("parent_id"));
			dealCategory.setPriority(rs.getLong("priority"));
			return dealCategory;
		}
	};

	private static RowMapper<DealCategory> dealCategoryTreeMapper = new RowMapper<DealCategory>() {
		@Override
		public DealCategory mapRow(ResultSet rs, int rowNum) throws SQLException {
			Map<Long, DealCategory> map = new HashMap<Long, DealCategory>();
			DealCategory dealCategory = new DealCategory();
			dealCategory.setId(rs.getLong("id"));
			dealCategory.setCategoryName(rs.getString("category_name"));
			dealCategory.setDescription(rs.getString("description"));
			dealCategory.setCategoryImage(rs.getString("category_image"));
			dealCategory.setParentId(rs.getLong("parent_id"));
			dealCategory.setTree_path(rs.getString("tree_path"));
			return dealCategory;
		}
	};

	@Override
	public Collection<DealCategory> fetchMainCategoryForSet(HttpServletRequest request) {
		Long pageSize = 0L;
		Long pageNumber = 0L;
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder("");
		StringBuilder where = new StringBuilder("");
		where.append(" 1=1 ");
		createWhereClause(request, params, where, sql);

		String finalSQL = new String();
		finalSQL = dealCategorySetQueryMap.get("seardealCategorySQL").replace("{0}", where);
		finalSQL = finalSQL.replace("{1}", sql);
		finalSQL += " ORDER BY dealCategory.category_name";

		if (request.getParameter("pageNumber") != null && request.getParameter("pageSize") != null) {
			pageNumber = Long.valueOf(request.getParameter("pageNumber").toString());
			pageSize = Long.valueOf(request.getParameter("pageSize").toString());
			if (pageNumber != 0 && pageSize != 0) {
				finalSQL = jdbcPagination(pageNumber, pageSize, finalSQL);
			}
		}
		Collection<DealCategory> queryResult = jdbcTemplate.query(finalSQL, dealCategoryMapper);
		return queryResult;
	}

	private void createWhereClause(HttpServletRequest request, Map<String, Object> params, StringBuilder where,
			StringBuilder sql) {
		if (request.getParameterValues("parentId") != null) {
			where.append(" and dealCategory.parent_id = 0 ");
		}
		if(request.getParameterValues("keyword") != null){
			where.append(" and dealCategory.category_name like " + "'%"+request.getParameter("keyword").toString()+"%'");
		}

	}


	@Override
	public Collection<DealCategory> fetchDealCategoryByParentId(Long parentId) {
		String select_Part = null;
		List<DealCategory> dealCategory = null;
		select_Part="select dc.*  from DealCategory dc where parent_id = " + parentId;
		dealCategory = jdbcTemplate.query(select_Part, dealCategoryMapper);
		return dealCategory;
	}

	@Override
	public DealCategory findById(Long dealCategoryId) {
		return dealCategoryRepository.findOne(dealCategoryId);
	}

	@Override
	public void delete(Long dealCategoryId) {
		DealCategory dealCategoryData = dealCategoryRepository.findOne(dealCategoryId);
		TreeModel<DealCategoryNode> model = initialize(dealCategoryData, null);
		TreeWalker treeWalker = new TreeWalker();
		treeWalker.walk(model, deleteDealCategoryPermanatlyVisitor, Direction.BOTTOM_UP);
	}

	private TreeModel<DealCategoryNode> initialize(DealCategory entity, TreeModel<DealCategoryNode> model) {
		if (model == null) {
			model = new TreeModel<DealCategoryNode>();
			DealCategoryNode rootNode = new DealCategoryNode(model, entity);
			Collection<DealCategory> childCategories = null;
			childCategories = this.fetchDealCategoryByParentId(entity.getId());
			if (childCategories != null) {
				for (DealCategory childCategory : childCategories) {
					DealCategoryNode newNode = new DealCategoryNode(model, childCategory);
					rootNode.append(newNode);
					initialize(newNode);
				}
			}
			model.setRoot(rootNode);
		}
		return model;
	}

	private void initialize(DealCategoryNode node) {
		Collection<DealCategory> childCategory = null;
		childCategory = this.fetchDealCategoryByParentId(node.getContents().getId());
		if (childCategory.size() > 0 && !childCategory.isEmpty()) {
			for (DealCategory childcategory : childCategory) {
				DealCategoryNode newNode = new DealCategoryNode(node.getModel(), childcategory);
				node.append(newNode);
				initialize(newNode);
			}
		}
	}

	@Override
	public DealCategory updateDealCategory(DealCategory dealCategory, DealCategory tempdealCategory) {
		tempdealCategory.setCategoryImage(dealCategory.getCategoryImage());
		tempdealCategory.setCategoryName(dealCategory.getCategoryName());
		tempdealCategory.setDescription(dealCategory.getDescription());
		tempdealCategory.setParentId(dealCategory.getParentId());
		dealCategoryRepository.save(tempdealCategory);
		return tempdealCategory;
	}

	protected String jdbcPagination(Long pageNumber, Long pageSize, String finalSQL) {
		Long offset = (pageNumber * pageSize) - pageSize;
		Long limit = pageSize;
		finalSQL += " LIMIT " + limit + " OFFSET " + offset + " ";
		return finalSQL;
	}

	@Override
	public Long searchCount(HttpServletRequest request) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder("");
		StringBuilder where = new StringBuilder("");
		where.append(" 1=1 ");

		createWhereClause(request, params, where, sql);

		String finalSQL = new String();
		finalSQL = dealCategorySetQueryMap.get("countdealCategorySQL").replace("{0}", where);
		finalSQL = finalSQL.replace("{1}", sql);
		Integer totalCount = jdbcTemplate.queryForObject(finalSQL, Integer.class);
		return totalCount.longValue();
	}

	@Override
	public void addCSVDataToDB(File file) {
		String csvFile = file.toString();
		String line = "";
		String cvsSplitBy = ",";

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

			while ((line = br.readLine()) != null) {
				long parentId = 0;
				String[] categories = line.split(cvsSplitBy);

				for (String category : categories) {
					if(isExist(category)){
						DealCategory dealCategory = new DealCategory();
						dealCategory.setCategoryName(category);
						dealCategory.setParentId(parentId);
						DealCategory tempDealCategory = dealCategoryRepository.save(dealCategory);
						parentId = tempDealCategory.getId();
					}else{
						List<DealCategory> newDealCategory = findOneByDealCategoryname(category);
						for (DealCategory dealCategory : newDealCategory) {
							parentId = dealCategory.getId();
						}
					}
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean isExist(String category) {
		List<DealCategory> dealCategory = null;
		dealCategory = findOneByDealCategoryname(category);
		if(dealCategory.size() != 0){
			return false;
		}else{
			return true;
		}
	}

	private void manageChiledData(DealCategory childCategories, DealCategory dealCategory){
		dealCategory.setCategoryName(childCategories.getCategoryName()+" >"+ dealCategory.getCategoryName());
	}

	private void initializeById(DealCategory entity, Collection<DealCategory> dealCategory) {
		DealCategory childCategories = new DealCategory();
		if(entity.getParentId() != 0){
			childCategories = dealCategoryRepository.findOne(entity.getParentId());
			for (DealCategory dealCategory2 : dealCategory) {
				if(dealCategory2.getParentId() == childCategories.getId()){
					dealCategory2.setCategoryName(childCategories.getCategoryName()+" >"+ dealCategory2.getCategoryName());
					dealCategory2.setTempId(childCategories.getParentId());
					break;
				}
			}
			initializeByIds(childCategories,dealCategory);
		}
	}

	private void initializeByIds(DealCategory node,Collection<DealCategory> model) {
		DealCategory childCategory = new DealCategory();
		if(node.getParentId() != 0){
			childCategory = dealCategoryRepository.findOne(node.getParentId());
			for (DealCategory dealCategory : model) {
				if(dealCategory.getTempId() == childCategory.getId()){
					dealCategory.setCategoryName(childCategory.getCategoryName()+" >"+ dealCategory.getCategoryName());
					dealCategory.setTempId(childCategory.getParentId());
					break;
				}
			}

			initializeByIds(childCategory, model);
		}

	}

	@Override
	public Collection<DealCategory> getDealByIds(Collection<DealCategory> dealCategory) {
		for (DealCategory deal : dealCategory) {
			if(deal.getParentId() != 0)
				initializeById(deal, dealCategory);
			//deal.setTempId(deal.getParentId());
		}
		return dealCategory;
	}

	@Override
	public Collection<DealCategory> findAllDealCategoryByParentId() {
		StringBuilder sql = new StringBuilder("");
		StringBuilder where = new StringBuilder("");
		where.append(" 1=1 ");
		where.append(" and parent_id = 0");

		String finalSQL = new String();
		finalSQL = dealCategorySetQueryMap.get("seardealCategorySQL").replace("{0}", where);
		finalSQL = finalSQL.replace("{1}", sql);
		finalSQL += " ORDER BY dealCategory.category_name";

		Collection<DealCategory> queryResult = jdbcTemplate.query(finalSQL, dealCategoryMapper);
		return queryResult;
	}

	private void initializeByIdCats(DealCategory entity, DealCategory dealCategory) {
		DealCategory childCategories = new DealCategory();
		if(entity.getParentId() != 0){
			childCategories = dealCategoryRepository.findOne(entity.getParentId());
			manageChiledData(childCategories,dealCategory);
			initializeByIdCats(childCategories,dealCategory);
		}
	}

	private void initializeByIdCat(DealCategory node, DealCategory model) {
		DealCategory childCategory = new DealCategory();
		if(node.getParentId() != 0){
			childCategory = dealCategoryRepository.findOne(node.getParentId());
			manageChiledData(childCategory, model);
			initializeByIdCats(childCategory, model);
		}

	}

	@Override
	public DealCategory getDealById(Long id) {
		DealCategory dealCategory = dealCategoryRepository.findOne(id);
		if(dealCategory.getParentId() != 0)
			initializeByIdCat(dealCategory, dealCategory);
		return dealCategory;
	}

	@Override
	public Collection<DealCategory> findAllDealCategory() {
		return dealCategoryRepository.findAll();
	}

	/*@Override
public Map<Integer, List<DealCategory>> searchDealCategoryByKeyword(String keyWord) {
	 List<DealCategory> dealCategorys = findOneByDealCategory(keyWord);
	 Map<Integer, List<DealCategory>> dealCategoryList = new HashMap<Integer, List<DealCategory>>();
	 for (DealCategory dealCategory : dealCategorys) {
		if(dealCategory.getParentId() == 0){
			dealCategoryList.put(this.bindCat, null);
			this.bindCat++;
		}else{
			initDealCategoryChield(dealCategory,dealCategoryList);
		}
	}
	return dealCategoryList;
}

private void initDealCategoryChield(DealCategory node, Map<Integer, List<DealCategory>> dealCategoryList) {
	DealCategory chieldCategory = new DealCategory();
	List<DealCategory> tempCat = new ArrayList<DealCategory>();
	tempCat.add(node);
	chieldCategory = dealCategoryRepository.findOne(node.getParentId());
	tempCat.add(chieldCategory);
	initDealCategoryChields(chieldCategory, tempCat, dealCategoryList);
}

private void initDealCategoryChields(DealCategory entity, List<DealCategory> tempCat, Map<Integer, List<DealCategory>> dealCategoryList) {
	DealCategory childCategories = new DealCategory();
	if(entity.getParentId() != 0){
		childCategories = dealCategoryRepository.findOne(entity.getParentId());
		tempCat.add(childCategories);
		initDealCategoryChields(childCategories,tempCat,dealCategoryList);
	}else{
		dealCategoryList.put(this.bindCat, tempCat);
		this.bindCat++;
	}
}*/

	@Override
	public Long getAllDealCategoryTreeCount(HttpServletRequest request) {

		String finalSQL = new String();
		finalSQL =  " with recursive cte (id, tree_path, category_name, category_image, parent_id, description) as ( "
				+ "   select     id, "
				+ " 			 category_name as tree_path, "
				+ "              category_name, "
				+ "              category_image, "
				+ "              parent_id, "
				+ "				description "	
				+ "   from       DealCategory "
				+ "   where      parent_id = 0 "
				+ "   union all "
				+ "   select     p.id, "
				+ " 			 CONCAT(cte.tree_path, '>', p.category_name) as tree_path, "
				+ "              p.category_name, "
				+ "              p.category_image, "
				+ "              p.parent_id, "
				+"				 p.description	"		
				+ "   from       DealCategory p "
				+ "   inner join cte "
				+ "           on p.parent_id = cte.id "
				+ " ) "
				+ " select * from cte "
				+ " where lower(tree_path) like '%"+ request.getParameter("searchText") +"%'"; 

		Integer totalCount = jdbcTemplate.queryForObject(finalSQL, Integer.class);
		return totalCount.longValue();
	}

	@Override
	public Collection<DealCategory> fetchAllDealCategoryTree(HttpServletRequest request) {

		List<DealCategory> dealCategory = null;
		Long pageSize = 0L;
		Long pageNumber = 0L;

		String finalSQL = new String();
		finalSQL =  " with recursive cte (id, tree_path, category_name, category_image, parent_id, description) as ( "
				+ "   select     id, "
				+ " 			 category_name as tree_path, "
				+ "              category_name, "
				+ "              category_image, "
				+ "              parent_id, "
				+ "				description "	
				+ "   from       DealCategory "
				+ "   where      parent_id = 0 "
				+ "   union all "
				+ "   select     p.id, "
				+ " 			 CONCAT(cte.tree_path, '>', p.category_name) as tree_path, "
				+ "              p.category_name, "
				+ "              p.category_image, "
				+ "              p.parent_id, "
				+"				 p.description	"		
				+ "   from       DealCategory p "
				+ "   inner join cte "
				+ "           on p.parent_id = cte.id "
				+ " ) "
				+ " select * from cte "
				+ " where lower(tree_path) like '%"+ request.getParameter("searchText") +"%'"; 


		if (request.getParameter("pageNumber") != null && request.getParameter("pageSize") != null) {
			pageNumber = Long.valueOf(request.getParameter("pageNumber").toString());
			pageSize = Long.valueOf(request.getParameter("pageSize").toString());
			if (pageNumber != 0 && pageSize != 0) {
				finalSQL = jdbcPagination(pageNumber, pageSize, finalSQL);
			}
		}
		dealCategory = jdbcTemplate.query(finalSQL, dealCategoryTreeMapper);
		return dealCategory;




		/*String select_Part = null;
	StringBuilder where_Part = new StringBuilder();
	StringBuilder queryBuilder = new StringBuilder();
	List<DealCategory> dealCategory = null;


	select_Part= " with recursive cte (id, tree_path, category_name, category_image, parent_id, description) as ( "
			+ "   select     id, "
			+ " 			 category_name as tree_path, "
			+ "              category_name, "
			+ "              category_image, "
			+ "              parent_id, "
			+ "				description "	
			+ "   from       DealCategory "
			+ "   where      parent_id = 0 "
			+ "   union all "
			+ "   select     p.id, "
			+ " 			 CONCAT(cte.tree_path, '>', p.category_name) as tree_path, "
			+ "              p.category_name, "
			+ "              p.category_image, "
			+ "              p.parent_id, "
			+"				 p.description	"		
			+ "   from       DealCategory p "
			+ "   inner join cte "
			+ "           on p.parent_id = cte.id "
			+ " ) "
			+ " select * from cte "; 


	//where_Part.append(" where lower(tree_path) like '%"+ key +"%'");

	queryBuilder.append(select_Part);    
	//queryBuilder.append(where_Part);

	dealCategory = jdbcTemplate.query(queryBuilder.toString(), dealCategoryTreeMapper);

    return dealCategory;*/
	}

	private static RowMapper<DealCategory> dealCategoryTreeHierarchyMapper = new RowMapper<DealCategory>() {
		@Override
		public DealCategory mapRow(ResultSet rs, int rowNum) throws SQLException {
			Map<Long, DealCategory> map = new HashMap<Long, DealCategory>();
			DealCategory dealCategory = new DealCategory();
			dealCategory.setId(rs.getLong("id"));
			dealCategory.setCategoryName(rs.getString("category_name"));
			dealCategory.setTree_path(rs.getString("tree_path"));
			return dealCategory;
		}
	};

	@Override
	public Collection<DealCategory> fetchAllTreeHierarchy() {
		String select_Part = null;
		StringBuilder where_Part = new StringBuilder();
		StringBuilder queryBuilder = new StringBuilder();
		List<DealCategory> dealCategory = null;


		select_Part= "WITH RECURSIVE category_path (id, category_name, tree_path) AS"
				+"("
				+"  SELECT id, category_name, category_name as tree_path"
				+"    FROM DealCategory"
				+"    WHERE parent_id = 0"
				+"  UNION ALL"
				+"  SELECT c.id, c.category_name, CONCAT(cp.tree_path, ' > ', c.category_name)"
				+"    FROM category_path AS cp JOIN DealCategory AS c"
				+"      ON cp.id = c.parent_id"
				+")"
				+"SELECT * FROM category_path"
				+"	ORDER BY tree_path";


		//where_Part.append(" where lower(tree_path) like '%"+ key +"%'");

		queryBuilder.append(select_Part);    
		//queryBuilder.append(where_Part);

		dealCategory = jdbcTemplate.query(queryBuilder.toString(), dealCategoryTreeHierarchyMapper);

		return dealCategory;
	}

	@Override
	public Collection<DealCategory> fetchByKeywords(String keywordText) {

		StringBuilder queryBuilder = new StringBuilder();
		List<DealCategory> dealCategory = null;

		String select_Part= "SELECT * "
				+" FROM  DealCategory "
				+" WHERE  Match (category_name) against('" + keywordText + "' IN boolean mode) limit 1 ";


		//where_Part.append(" where lower(tree_path) like '%"+ key +"%'");

		queryBuilder.append(select_Part);    
		//queryBuilder.append(where_Part);

		dealCategory = jdbcTemplate.query(queryBuilder.toString(), dealCategoryMapper);

		return dealCategory;
	}

	@Override
	public Collection<DealCategory> fetchDealCategoryByPriority() {
		String select_Part = null;
		List<DealCategory> dealCategory = null;
		select_Part = "SELECT * FROM DealCategory where priority > 0 and priority <= 8";
		dealCategory = jdbcTemplate.query(select_Part, dealCategoryMapper);
		return dealCategory;
	}

}
