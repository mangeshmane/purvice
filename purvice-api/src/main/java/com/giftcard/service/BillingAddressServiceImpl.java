package com.giftcard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giftcard.model.BillingAddress;
import com.giftcard.model.States;
import com.giftcard.repository.BillingAddressRepository;

import com.giftcard.repository.StatesRepository;

@Service
public class BillingAddressServiceImpl implements BillingAddressService{

	@Autowired
	public StatesRepository statesRepository;
	
	@Autowired
	public BillingAddressRepository billingAddressRepository;

	@Override
	public List<States> findAllStates() {
		
		return statesRepository.findAll();
	}

	@Override
	public BillingAddress save(BillingAddress billingAddress) {
		
		return billingAddressRepository.save(billingAddress);
	}

	@Override
	public BillingAddress findByUser_id(long id) {
		
		return billingAddressRepository.findByUser_id(id);
	}

}
