package com.giftcard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giftcard.model.BrandImage;
import com.giftcard.repository.BrandImageRespository;

@Service
public class BrandImageServiceImpl implements BrandImageService{

	@Autowired
	private BrandImageRespository brandImageRespository;
	
	@Override
	public BrandImage findById(long id) {
		return brandImageRespository.findById(id);
	}

	@Override
	public BrandImage deleteById(long id) {
		return brandImageRespository.deleteById(id);
	}

	@Override
	public BrandImage save(BrandImage brandImage) {
		return brandImageRespository.save(brandImage);
	}

	@Override
	public List<BrandImage> findByBrand_id(long id) {
		
		return brandImageRespository.findByBrand_id(id);
	}

}
