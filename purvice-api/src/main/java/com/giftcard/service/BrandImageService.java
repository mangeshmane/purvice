package com.giftcard.service;

import java.util.List;

import com.giftcard.model.BrandImage;

public interface BrandImageService {

	public BrandImage findById(long id);
	public BrandImage deleteById(long id);
	
	public BrandImage save(BrandImage brandImage);
	
	public List<BrandImage> findByBrand_id(long id);
}
