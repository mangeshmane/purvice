package com.giftcard.service;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Transaction;
import com.braintreegateway.Transaction.Status;
import com.giftcard.Application;
import com.giftcard.model.Orders;
import com.giftcard.model.User;
import com.giftcard.repository.OrdersRepository;
import com.giftcard.tango.jobs.TangoService;
import com.giftcard.util.SMSUtil;
import com.tangocard.client.model.CreateOrderCriteria;
import com.tangocard.client.model.CredentialView;
import com.tangocard.client.model.OrderViewSummary;
import com.tangocard.client.model.RecipientInfoCriteria;
import com.tangocard.client.model.ResendView;
import com.tangocard.client.model.SenderInfoCriteria;

@Service
public class OrdersServiceImpl implements OrdersService {

	@Autowired
	public OrdersRepository ordersRepository;

	@Autowired
	public UserService userService;

	@Autowired
	private TangoService tangoService;

	@Autowired
	private OrdersService ordersService;

	private Status settlementStatus = Transaction.Status.SETTLED;
	private Status settlementStatus1 = Transaction.Status.SUBMITTED_FOR_SETTLEMENT;

	private BraintreeGateway gateway = Application.gateway;
	
	private static final Logger logger = LoggerFactory.getLogger(OrdersServiceImpl.class);

	@Override
	public List<Orders> findAll() {

		return ordersRepository.findAll();
	}

	@Override
	public List<Orders> findByIsscheduled(boolean b) {

		return ordersRepository.findByIsscheduled(b);
	}

	@Override
	public Orders emailOrder(User user, Orders order) {

		if (order.getOrderType().equals("Self")) {
			String emailID = user.getUsername().toLowerCase();
			String toSubject = "Purchase details";
			String toBody = "purchase details " + "transaction id= " + order.getPaymentTransactionId()
					+ "Purchase Amount= " + order.getAmount();
			userService.sendEmail(emailID, toSubject, toBody);
			order.setStatus(Orders.Status.INPROGRESS);

		} else {
			if (order.isIsscheduled() == false || order.getScheduledDate() == null) {
				String emailID = order.getFriendEmailId().toLowerCase();
				String toSubject = "Gift for you";
				String toBody = user.getFirstName() + " " + user.getLastName() + " sends you a gift. "
						+ order.getFriendMessage();
				userService.sendEmail(emailID, toSubject, toBody);
				if (order.getFriendContact().equals(null)) {
					String to = "+91" + order.getFriendContact();
					String body = user.getFirstName() + " " + user.getLastName() + " sends you a gift. "
							+ order.getFriendMessage();
					SMSUtil.sendMessage(to, body);
				}
				order.setStatus(Orders.Status.COMPLETE);
				order.setEmailAttempt("successsful");
				order.setEmailMessage("Gift sent successfully");
			}

		}
		List<Orders> orderList = user.getOrder();
		orderList.add(order);
		return order;
	}

	@Override
	public OrderViewSummary sendOrder(CreateOrderCriteria createOrderCriteria) {

		return tangoService.placeOrder(createOrderCriteria);
	}

	@Override
	public ResendView resendOrder(String referenceOrderID) {

		return tangoService.resendOrder(referenceOrderID);
	}

	@Override
	public List<Orders> findByUser(long id) {

		return ordersRepository.findByUser_id(id);
	}

	@Override
	public void save(Orders order) {
		ordersRepository.save(order);

	}

	@Override
	public CreateOrderCriteria populateTangoOrder(Orders order) {
		CreateOrderCriteria createOrderCriteria = new CreateOrderCriteria();
		RecipientInfoCriteria recipientInfoCriteria = new RecipientInfoCriteria();
		SenderInfoCriteria senderInfoCriteria = new SenderInfoCriteria();
		String extrlnlRefId = UUID.randomUUID().toString()+ "" + System.currentTimeMillis();
		createOrderCriteria.setExternalRefID(extrlnlRefId);
		if (order.getOrderType().equals("Self")) {
			recipientInfoCriteria.setEmail(order.getRecipient());
			recipientInfoCriteria.setFirstName(order.getUser().getFirstName());
			recipientInfoCriteria.setLastName(order.getUser().getLastName());
			createOrderCriteria.setRecipient(recipientInfoCriteria);

		}
		if (order.getOrderType().equals("Friend")) {
			recipientInfoCriteria.setEmail(order.getFriendEmailId());
			recipientInfoCriteria.setFirstName(order.getFriendName());
			/*senderInfoCriteria.setEmail(order.getUser().getUsername());
			senderInfoCriteria.setFirstName(order.getUser().getFirstName());
			senderInfoCriteria.setLastName(order.getUser().getLastName());*/
			createOrderCriteria.setMessage(order.getFriendMessage());
			createOrderCriteria.setRecipient(recipientInfoCriteria);
			createOrderCriteria.setSender(senderInfoCriteria);
		}
		createOrderCriteria.setAccountIdentifier("purvice");
		createOrderCriteria.setAmount(order.getAmount());
		createOrderCriteria.setCustomerIdentifier("purvice");
		createOrderCriteria.setEmailSubject(order.getEmailSubject());
		createOrderCriteria.setSendEmail(true);
		createOrderCriteria.setUtid(order.getUtid());

		return createOrderCriteria;
	}

	@Override
	public Orders findById(Long id) {

		return ordersRepository.findOne(id);
	}

	@Override
	public String createString(OrderViewSummary summary) {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = null;
		try {
			json = ow.writeValueAsString(summary);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return json;
	}

	@Override
	public void sendSMS(Orders order) {
		String to = order.getFriendContact();
		String body = order.getUser().getFirstName() + " " + order.getUser().getLastName() + " sends you a gift. "
				+ order.getFriendMessage() + " To see this gift, check your email " + order.getFriendEmailId();
		SMSUtil.sendMessage(to, body);
	}

	@Override
	public void sendScheduledOrder(Orders order) {
		CreateOrderCriteria createOrderCriteria = null;
		Transaction transaction=null;
		try{
			if(order.getPaymentTransactionId()!=null || !order.getPaymentTransactionId().equals("")){
				transaction = gateway.transaction().find(order.getPaymentTransactionId());
				if (this.validate(transaction, order)) {
					createOrderCriteria = ordersService.populateTangoOrder(order);
				}
			}
			
			if (createOrderCriteria != null) {
				OrderViewSummary orderViewSummary = ordersService.sendOrder(createOrderCriteria);
				if (orderViewSummary != null) {
					order.setIsscheduled(false);
					order.setTangoOrderStatus("Completed");
					order.setStatus(Orders.Status.COMPLETE);
					order.setExternalRefID(orderViewSummary.getReferenceOrderID());
					order.setEmailAttempt("successful");
					order.setEmailMessage("Gift sent successfully");
					String response = ordersService.createString(orderViewSummary);
					order.setTangoResponse(response);
					List<CredentialView> reward = orderViewSummary.getReward().getCredentialList();
					String code="";
					for (CredentialView credentialView : reward) {
						code =code+"    "+ credentialView.getLabel() + " : " + credentialView.getValue()+"    ";
					}
					
					order.setClaimCode(code);
					/*if (order.getOrderType().equals("Friend")) {
						ordersService.sendSMS(order);
					}*/
					ordersService.save(order);

				} else {
					order.setStatus(Orders.Status.DELIVERED_FAILED);
					order.setTangoOrderStatus("Delivered_failed");
					order.setEmailAttempt("Failed");
					order.setEmailMessage("Gift sending failed");
					ordersService.save(order);
				}
			}
		}catch(Exception ex){
			logger.error("error in sending mail to order id=  "+order.getId(), ex);
		}
		
	}

	@Override
	public void delete(long id) {

		ordersRepository.delete(id);

	}

	public boolean validate(Transaction transaction, Orders order) {
		boolean flag = false;
		int code;
		if (transaction.getProcessorResponseCode().equals("") || transaction.getProcessorResponseCode() == null) {
			code = 0;
		} else {
			code = Integer.parseInt(transaction.getProcessorResponseCode());
		}
		if (code >= 1000 && code < 2000 && (transaction.getStatus().equals(settlementStatus) || transaction.getStatus().equals(settlementStatus1))
				&& order.getStatus() == Orders.Status.SCHEDULED) {
			flag = true;
		}
		return flag;
	}

	@Override
	public Orders updateOrderData(Orders tempOrder, Orders order) {

		if (order.isIsscheduled()) {
			tempOrder.setIsscheduled(true);
			tempOrder.setStatus(Orders.Status.SCHEDULED);
			if (order.getScheduledDate() != null) {
				tempOrder.setScheduledDate(order.getScheduledDate());
			}
		} else {
			tempOrder.setIsscheduled(false);
			tempOrder.setStatus(Orders.Status.INPROGRESS);
			tempOrder.setScheduledDate(null);
		}

		if (order.getFriendName() != null) {
			tempOrder.setFriendName(order.getFriendName());
		}
		if (order.getFriendEmailId() != null) {
			tempOrder.setFriendEmailId(order.getFriendEmailId());
		}
		if (order.getFriendMessage() != null) {
			tempOrder.setFriendMessage(order.getFriendMessage());
		}
		if (order.getFriendContact() != null) {
			tempOrder.setFriendContact(order.getFriendContact());
		}

		return tempOrder;
	}

	@Override
	public void updateOrderStatus() {
		List<Orders> orders =ordersService.findByTransaction("SETTLED");
		Iterator<Orders> itr = orders.iterator();
		while (itr.hasNext()) {
			Orders order = itr.next();
			if (!order.getPaymentTransactionId().equals(null)) {
				Transaction transaction = gateway.transaction().find(order.getPaymentTransactionId());
				order.setTransaction(transaction.getStatus().toString());
				ordersService.save(order);
			}
		}
	}

	@Override
	public List<Orders> findByTransaction(String transactionStatus) {

		return ordersRepository.findByTransactionNotLike(transactionStatus);
	}

	@Override
	public Long countByUser_idAndTangoOrderStatusNotLike(long id, String status) {
		
		return ordersRepository.countByUser_idAndTangoOrderStatusNotLike(id, status);
	}
}
