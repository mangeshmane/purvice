package com.giftcard.factory;


import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.braintreegateway.BraintreeGateway;

public class BraintreeGatewayFactory {
	
	private static final Logger logger = LoggerFactory.getLogger(BraintreeGatewayFactory.class);
	
    public static BraintreeGateway fromConfigMapping(Map<String, String> mapping) {
        return new BraintreeGateway(
            mapping.get("BT_ENVIRONMENT"),
            mapping.get("BT_MERCHANT_ID"),
            mapping.get("BT_PUBLIC_KEY"),
            mapping.get("BT_PRIVATE_KEY")
        );
    }

    public static BraintreeGateway fromConfigFile(InputStream inputStream) {
        //InputStream inputStream = null;
        Properties properties = new Properties();

        try {
           // inputStream = new FileInputStream(inputStream);
            properties.load(inputStream);
        } catch (Exception e) {
        	logger.error("Exception: " + e);
        } finally {
            try { inputStream.close(); }
            catch (IOException e) { logger.error("Exception: " + e); }
        }

        return new BraintreeGateway(
            properties.getProperty("BT_ENVIRONMENT"),
            properties.getProperty("BT_MERCHANT_ID"),
            properties.getProperty("BT_PUBLIC_KEY"),
            properties.getProperty("BT_PRIVATE_KEY")
        );
    }
}
