package com.giftcard.scapper.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.giftcard.deals.model.ProductModel;



public class SearchDataHelper {
	private static Map<String,ProductModel> iMap = null;
	
	public static Map<String,ProductModel> getData(){
		if(iMap == null){
			iMap = new HashMap();
					
			List<ProductModel> mList= PullDataHelper.pullCurrentProducts();
			
			for(ProductModel m: mList){
				iMap.put(m.getProductName(), m);
			}
			
		}
		return iMap;
	}
	
	
}
