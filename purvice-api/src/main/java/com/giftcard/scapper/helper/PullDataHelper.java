package com.giftcard.scapper.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProviderChain;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.giftcard.deals.model.ProductModel;


public class PullDataHelper {
    
    static DynamoDB dynamoDB ;



    private static void init() throws Exception {
		/*
		 * The ProfileCredentialsProvider will return your [default] credential
		 * profile by reading from the credentials file located at
		 * (/Users/poorva/.aws/credentials).
		 */
		AWSCredentials credentials = null;
		System.setProperty("AWS_CREDENTIAL_FILE", "dynamo.properties");
    	System.setProperty("aws.accessKeyId", "AKIAJ735GQD6NAPQQMUA");
    	System.setProperty("aws.secretKey", "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");

//		try {
//			credentials = new ProfileCredentialsProvider("default").getCredentials();
//		} catch (Exception e) {
//			throw new AmazonClientException("Cannot load the credentials from the credential profiles file. " + "Please make sure that your credentials file is at the correct " + "location (/Users/poorva/.aws/credentials), and is in valid format.", e);
//		}
		AmazonDynamoDBClient  dynamoDB2 = new AmazonDynamoDBClient();
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		dynamoDB2.setRegion(usWest2);
		dynamoDB = new DynamoDB(dynamoDB2);
		

	}
    
    public static void main(String[] args) throws IOException {
    	List<ProductModel> mList =   pullProducts("test");
    	for(ProductModel mProductModel: mList){
    		System.out.println(mProductModel.getProductName()+","+mProductModel.getPrice()+","+mProductModel.getUrl());
    	}

    }

	public static List<ProductModel> pullCurrentProducts() {
		
		List<ProductModel> mList = new ArrayList<ProductModel>();

		
		
		// Scan items for movies with a year attribute greater than 1985
		if(dynamoDB == null){
			try {
				init();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String tableName = "ac_products_1";
		
		Table table = dynamoDB.getTable(tableName);
		int mCount=0;
        ItemCollection items =  table.scan(
                null,
               null, //ProjectionExpression
                null, //ExpressionAttributeNames - not used in this example 
                null);
            
            //System.out.println("Scan of " + tableName + " for items with a price less than 100.");
            Iterator<Item> iterator = items.iterator();
            
            while (iterator.hasNext()) {
            	Item item = iterator.next();
            	mList.add(new ProductModel(item));
                //System.out.println(item.toJSONPretty());	
                
                mCount++;
                if(mCount > 1000){
                	break;
                }
            }   
            //System.out.println("@@ACTEST count="+mCount);
		
		return mList;
		
	}
   
    
	public static List<ProductModel> pullProducts(String aParam) {
		
		List<ProductModel> mList = new ArrayList<ProductModel>();
		System.out.println("@@@@@@@@@@ACTEST===>>>"+aParam);

		
		
		// Scan items for movies with a year attribute greater than 1985
		if(dynamoDB == null){
			try {
				init();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String tableName = "ac_products_1";
		
		Table table = dynamoDB.getTable(tableName);
		int mCount=0;
        Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
        if(aParam == null || aParam.trim().equals("")){
        	aParam = "s";
        }
        expressionAttributeValues.put(":pr", aParam);
        ItemCollection items =  table.scan(
        		"contains (product, :pr)",
               null, //ProjectionExpression
                null, //ExpressionAttributeNames - not used in this example 
                expressionAttributeValues);
            
            //System.out.println("Scan of " + tableName + " for items with a price less than 100.");
            Iterator<Item> iterator = items.iterator();
            
            while (iterator.hasNext()) {
            	Item item = iterator.next();
            	mList.add(new ProductModel(item));
                //System.out.println(item.toJSONPretty());	
                
                mCount++;
                if(mCount > 1000){
                	break;
                }
            }   
            //System.out.println("@@ACTEST count="+mCount);
		
		return mList;
		
	}
    
	

    
    
}
