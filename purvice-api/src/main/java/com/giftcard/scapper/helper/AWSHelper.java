package com.giftcard.scapper.helper;

import com.giftcard.deals.model.ProductModel;
import com.giftcard.scrappers.AmazonSearchScrapper;
import com.giftcard.scrappers.GenericScrapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;


public class AWSHelper {
	
	public static final int MAX_PAGES = 1;
	public static void main(String args[]) {
		AWSHelper.getAWSSearchData("shirt ");
	}
	
	public static List<ProductModel> getAWSSearchData(String aKeyword){
		List<ProductModel> mList = null;
		
		
		/* Commenting the amazon api search on 03/25/2019
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(true);
		factory.setIgnoringElementContentWhitespace(true);

		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			SignedRequestsHelper mHelper = new SignedRequestsHelper();
			Document doc = null;

			// doc.getDocumentElement().normalize();

			// New Logic Start
			int mNumOfPages = 1;

			Item item = null;
			ProductModel mProductModel = null;
			for (int i = 1; i <= mNumOfPages; i++) {

				try {
					mList = new ArrayList<>();
					// Get all the pages 1 by 1. Amazon gives only 10 records in one
					// call. we need to wait 1 second before making the second call.
					builder = factory.newDocumentBuilder();
					String mUrl = mHelper.sign(getParamMap(String.valueOf(i),aKeyword));
					System.out.println("@@@@@AWS url:"+mUrl);
					doc = builder.parse(mUrl);
					System.out.println("@@@@@@@ACTEST "+doc.toString());
					if (i == 1) {
						mNumOfPages = Integer.parseInt(doc.getElementsByTagName("TotalPages").item(0).getTextContent());
						System.out.println("Number of Pages:" + mNumOfPages);
					}

					NodeList labTestList = doc.getElementsByTagName("Item");
					for (int j = 0; j < labTestList.getLength(); ++j) {
						try {
							Element labTest = (Element) labTestList.item(j);
							item = new Item();
							item.withPrimaryKey("product", labTest.getElementsByTagName("Title").item(0).getTextContent());
							item.withString("ProductGroup",labTest.getElementsByTagName("ProductGroup").item(0).getTextContent());
							item.withString("Title", labTest.getElementsByTagName("Title").item(0).getTextContent());
							//item.withString("Author", labTest.getElementsByTagName("Author").item(0).getTextContent());
							item.withString("FormattedPrice", labTest.getElementsByTagName("FormattedPrice").item(0).getTextContent());
							item.withString("ASIN", labTest.getElementsByTagName("ASIN").item(0).getTextContent());
							item.withString("url", ((labTest.getElementsByTagName("LargeImage").item(0)).getFirstChild().getTextContent()));
							item.withString("desturl", (labTest.getElementsByTagName("DetailPageURL").item(0).getTextContent()));
							String mSalePrice= null;
							Element mElementLowestNewPrice= ((Element)labTest.getElementsByTagName("Offers").item(0));
							if(mElementLowestNewPrice != null){
								Element mElementOfferSummary = ((Element)mElementLowestNewPrice.getElementsByTagName("Offer").item(0));
								if(mElementOfferSummary != null){
									mSalePrice= mElementOfferSummary.getElementsByTagName("FormattedPrice").item(0).getTextContent();
									System.out.println("Sale Price:"+mSalePrice);
									System.out.println("regular Price:"+labTest.getElementsByTagName("FormattedPrice").item(0).getTextContent());
									
									
								}
							}
							item.withString("salePrice",mSalePrice);
							

							
							
							mProductModel = new ProductModel(item);
							System.out.println("@@@ACTEST===>>"+mProductModel.toString());

							mList.add(mProductModel);
						} catch (Exception mE) {
							//mE.printStackTrace();
							System.out.println("error:"+mE.getMessage());
						}

					}
					System.out.println("@@@@ACTEST going to add data for page:"+i);
					if (i >= MAX_PAGES) {
						break;
					}
					Thread.sleep(1000);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			// New Logic End

	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		*/
		
		
		System.out.println("@@@@ACTEST going to search api's...");

		if(mList == null) {
			mList = new ArrayList<>();
		}

		long mStartTime = Calendar.getInstance().getTimeInMillis();

		CompletableFuture<List<ProductModel>> cfa = CompletableFuture.supplyAsync(() -> AmazonSearchScrapper.productSearchUsingScraping(aKeyword));
		CompletableFuture<List<ProductModel>> cfb = CompletableFuture.supplyAsync(() -> BestBuyHelper.getBestBuyItems(aKeyword));
		CompletableFuture<List<ProductModel>> cfc = CompletableFuture.supplyAsync(() -> WalmartHelper.getWalmartSearchItems(aKeyword));
		CompletableFuture<List<ProductModel>> cfd = CompletableFuture.supplyAsync(() -> GenericScrapper.searchByScrapping(null,aKeyword));

		try {
			mList.addAll(cfa.get());
		}
		catch(Exception mExcep){
			System.out.println("@@@ACTEST search error:"+mExcep.getMessage());
		}
		try {
			mList.addAll(cfb.get());
		}
		catch(Exception mExcep){
			System.out.println("@@@ACTEST search error:"+mExcep.getMessage());
		}

		try {
			mList.addAll(cfc.get());
		}
		catch(Exception mExcep){
			System.out.println("@@@ACTEST search error:"+mExcep.getMessage());
		}

		try {
			mList.addAll(cfd.get());
		}
		catch(Exception mExcep){
			System.out.println("@@@ACTEST search error:"+mExcep.getMessage());
		}





		// Scrap Amazon products
		// Now let's call Best Buy webservice
//		List<ProductModel> mAmazonScrappingSearchList  = AmazonSearchScrapper.productSearchUsingScraping(aKeyword);
//
//		if(mAmazonScrappingSearchList != null && !mAmazonScrappingSearchList.isEmpty()) {
//			if(mList == null) {
//				mList = new ArrayList<>();
//			}
//			mList.addAll(mAmazonScrappingSearchList);
//		}
//
//
//
//		// Now let's call Best Buy webservice
//		List<ProductModel> mBestBuyList = BestBuyHelper.getBestBuyItems(aKeyword);
//
//		if(mBestBuyList != null && !mBestBuyList.isEmpty()) {
//			if(mList == null) {
//				mList = new ArrayList<>();
//			}
//			mList.addAll(mBestBuyList);
//		}
//
//
//		// Now let's call Walmart webservice
//		List<ProductModel> mWalmartList = WalmartHelper.getWalmartSearchItems(aKeyword);
//
//		if(mWalmartList != null && !mWalmartList.isEmpty()) {
//			if(mList == null) {
//				mList = new ArrayList<>();
//			}
//			mList.addAll(mWalmartList);
//		}
//
//
//		// Now seacrh from generic scrapper
//		List<ProductModel> mGenericScrapperList = GenericScrapper.searchByScrapping(null,aKeyword);
//
//		if(mGenericScrapperList != null && !mGenericScrapperList.isEmpty()) {
//			if(mList == null) {
//				mList = new ArrayList<>();
//			}
//			mList.addAll(mGenericScrapperList);
//		}

		System.out.println("Time for search query:"+(Calendar.getInstance().getTimeInMillis() - mStartTime)/(1000)+" s");

		return mList;
	}


	
/*
 * http://webservices.amazon.com/onca/xml?
  Service=AWSECommerceService
  &Operation=ItemSearch
  &ResponseGroup=Small
  &SearchIndex=All
  &Keywords=harry_potter
  &AWSAccessKeyId=[Your_AWSAccessKeyID]
  &AssociateTag=[Your_AssociateTag]
  &Timestamp=[YYYY-MM-DDThh:mm:ssZ]
  &Signature=[Request_Signature]
 */
	public static HashMap<String, String> getParamMap(String mItemPageNum,String aKeyword) {
		HashMap<String, String> mParamMap = new HashMap<String, String>();
		//mParamMap.put("BrowseNode", aBrowseNodeId);
		mParamMap.put("Service", "AWSECommerceService");
		mParamMap.put("SearchIndex", "All");
		mParamMap.put("AssociateTag", "purvice-20");
		mParamMap.put("Keywords", aKeyword);
		mParamMap.put("ItemPage", mItemPageNum);
		mParamMap.put("Operation", "ItemSearch");
		mParamMap.put("ResponseGroup", "Large");
		mParamMap.put("Timestamp", getTimestamp());
		mParamMap.put("MerchantId", "Amazon");
		//mParamMap.put("Version", "2011-08-01");

		return mParamMap;
	}

	private static String getTimestamp() {
		Calendar c = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ssZ");
		return df.format(c.getTime());
	}

}
