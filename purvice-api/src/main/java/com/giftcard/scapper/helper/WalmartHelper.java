package com.giftcard.scapper.helper;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.giftcard.deals.model.Product;
import com.giftcard.deals.model.ProductModel;
import com.giftcard.deals.model.SDModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class WalmartHelper {
	private static final String WALMART_API_KEY= "f3eq7jyxtqxg9agmppxetpsu";
	private static final String WALMART_LINKSHARE_KEY = "2dK3xt1T774";

	public static void main(String args[]) {
		List<ProductModel> mList = getWalmartSearchItems("ipad");
		for(ProductModel m: mList) {
			System.out.println("@@@ACTEST "+m.toString());
		}
		
		
		Map<String,SDModel> mMap = new HashMap<>();
		populateTrendingDeals(mMap);
		
	}
	
	
	public static List<ProductModel> getWalmartSearchItems(String aSearchItem){
		List<ProductModel> mList = new ArrayList<>();
		if(aSearchItem != null) {
			aSearchItem = aSearchItem.replaceAll(" ", "%20");
		}
		String mUrl = "http://api.walmartlabs.com/v1/search?apiKey="+WALMART_API_KEY+"&query="+aSearchItem+"&lsPublisherId="+WALMART_LINKSHARE_KEY;
		System.out.println("@@ACTEST:"+mUrl);
		CloseableHttpClient httpclient = HttpClients.createDefault();
		
		CloseableHttpResponse response = null;
		try {
			HttpGet httpget = new HttpGet(mUrl);
			response = httpclient.execute(httpget);
			
			HttpEntity entity = response.getEntity();
			Gson gson = new GsonBuilder().create();
	        ContentType contentType = ContentType.getOrDefault(entity);
	        Charset charset = contentType.getCharset();
	        Reader reader = new InputStreamReader(entity.getContent(), charset);

	        JsonParser jp = new JsonParser(); //from gson
	        JsonElement root = jp.parse(reader);
	        JsonObject rootobj = root.getAsJsonObject();
	        JsonArray mJsonArr = rootobj.getAsJsonArray("items");
	        
	        for(int i = 0; i < mJsonArr.size(); i++) {
	        		//String aPrice,String aProductName,String aUrl, String aDestUrl, String aSalePrice
	        		try {
						JsonObject mJsonObj = mJsonArr.get(i).getAsJsonObject();
						if(mJsonObj == null) {
							continue;
						}
						String mPrice = mJsonObj.get("msrp").getAsString();
						String mProductName = mJsonObj.get("name").getAsString();
						String mUrlImg = mJsonObj.get("largeImage").getAsString();;
						String mDestUrl = mJsonObj.get("productTrackingUrl").getAsString();
						String aSalePrice = mJsonObj.get("salePrice").getAsString();
						
						String mCustomerReviewAverage = mJsonObj.get("customerRating").getAsString();
						String mCustomerReviewCount = mJsonObj.get("numReviews").getAsString();
						String categoryName = mJsonObj.get("categoryPath").getAsString();
						String mAddToCartUrl = null;
						
						if(mJsonObj.has("addToCartUrl")) {
							mAddToCartUrl = mJsonObj.get("addToCartUrl").getAsString();
							
						}
						
						System.out.println("@@@ACTEST mCustomerReviewAverage:"+mCustomerReviewAverage);
						
						ProductModel mModel = new ProductModel(mPrice,mProductName,mUrlImg,mDestUrl,aSalePrice,"walmart.com",mCustomerReviewAverage,mCustomerReviewCount,categoryName, mAddToCartUrl);
						mList.add(mModel);
					} catch (Exception e) {
						// Ignore and move on	
						
					}
	        }
			
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		try {

		} finally {
			try {
				response.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		
		return mList;
		
	}
	
	// This method pulls trending deals from Best Buy API's
	public static void populateTrendingDeals(Map<String,SDModel> mMap) {
		String mUrl = null;

		
		//List<String> mCategories = getListOfCategories();
		//for(String mCategoryId:mCategories) {
			mUrl = "https://api.walmartlabs.com/v1/trends?apikey="+WALMART_API_KEY+"&lsPublisherId="+WALMART_LINKSHARE_KEY;
			
			CloseableHttpClient httpclient = HttpClients.createDefault();
			CloseableHttpResponse response = null;
			try {
				HttpGet httpget = new HttpGet(mUrl);
				response = httpclient.execute(httpget);
				
				HttpEntity entity = response.getEntity();
				Gson gson = new GsonBuilder().create();
		        ContentType contentType = ContentType.getOrDefault(entity);
		        Charset charset = contentType.getCharset();
		        Reader reader = new InputStreamReader(entity.getContent());

		        JsonParser jp = new JsonParser(); //from gson
		        JsonElement root = jp.parse(reader);
		        JsonObject rootobj = root.getAsJsonObject();
		        JsonArray mJsonArr = rootobj.getAsJsonArray("items");
		        
		        for(int i = 0; i < mJsonArr.size(); i++) {
		        		//String aPrice,String aProductName,String aUrl, String aDestUrl, String aSalePrice
		        		try {
							JsonObject mJsonObj = mJsonArr.get(i).getAsJsonObject();
							if(mJsonObj == null) {
								continue;
							}
							
							
							String mPrice = mJsonObj.get("msrp").getAsString();
							String mProductName = mJsonObj.get("name").getAsString();
							String mUrlImg = mJsonObj.get("largeImage").getAsString();;
							String mDestUrl = mJsonObj.get("productUrl").getAsString();
							String mSalePrice = mJsonObj.get("salePrice").getAsString();
							String mCustomerReviewAverage = mJsonObj.get("customerRating").getAsString();
							String mCustomerReviewCount = mJsonObj.get("numReviews").getAsString();
							

							//ProductModel mModel = new ProductModel(mPrice,mProductName,mUrlImg,mDestUrl,mSalePrice,"bestbuy.com");
							
							String categoryName = mJsonObj.get("categoryPath").getAsString();
							
							String mAddToCartUrl = null;
							
							if(mJsonObj.has("addToCartUrl")) {
								mAddToCartUrl = mJsonObj.get("addToCartUrl").getAsString();
								
							}
							
							
							Product mProdModel = new Product(categoryName, mDestUrl, mProductName, mUrlImg, Double.parseDouble(mPrice), Double.parseDouble(mSalePrice),mCustomerReviewAverage,mCustomerReviewCount, mAddToCartUrl);
							
							
							
							System.out.println("@@@ACTEST"+mProdModel.toString());
							SDModel mSDModel = new SDModel(mProdModel);
							mMap.put(mDestUrl, mSDModel);
						} catch (Exception e) {
							// Ignore and move on	
							
						}
		        }
				
			} 
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 

			try {

			} finally {
				try {
					response.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}				
		//}
	}
	
	
	
	public static List<String> getListOfCategories() {
		String mUrl = "https://api.bestbuy.com/v1/categories?format=json&show=id&apiKey="+WALMART_API_KEY;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		List<String> mListOfCategories = new ArrayList<>();
		
		CloseableHttpResponse response = null;
		try {
			HttpGet httpget = new HttpGet(mUrl);
			response = httpclient.execute(httpget);
			
			HttpEntity entity = response.getEntity();
			Gson gson = new GsonBuilder().create();
	        ContentType contentType = ContentType.getOrDefault(entity);
	        Charset charset = contentType.getCharset();
	        Reader reader = new InputStreamReader(entity.getContent(), charset);

	        JsonParser jp = new JsonParser(); //from gson
	        JsonElement root = jp.parse(reader);
	        JsonObject rootobj = root.getAsJsonObject();
	        JsonArray mJsonArr = rootobj.getAsJsonArray("categories");
	        
	        for(int i = 0; i < mJsonArr.size(); i++) {
	        		//String aPrice,String aProductName,String aUrl, String aDestUrl, String aSalePrice
	        		try {
						String mId = mJsonArr.get(i).getAsJsonObject().get("id").getAsString();
						mListOfCategories.add(mId);

					} catch (Exception e) {
						// Ignore and move on	
					}
	        }
			
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		try {

		} finally {
			try {
				response.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return mListOfCategories;
	}
}
