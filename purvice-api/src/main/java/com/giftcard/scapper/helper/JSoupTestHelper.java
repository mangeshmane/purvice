package com.giftcard.scapper.helper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.giftcard.deals.model.SDModel;


public class JSoupTestHelper {

	
	private JSoupTestHelper(){
		//populateData();
	}
	private JSoupTestHelper(SDModel m){
		
	}
	private static final JSoupTestHelper iInstance = new JSoupTestHelper();
	
	   public static JSoupTestHelper getInstance() {
	        return iInstance; 
	    }
	
	public static void main(String[] args) {
		//JSoupTestHelper.getInstance().populateData();
		
		String mLink="http://www.amazon.com/gp/coupon/c/A1G2EDQZZ6GB20?ascsubtag=22e980b6730811e5bed02a47dc170cad0INT&tag=sliccoupons2-20&redirectASIN=B00UASJM8S&tag=purvice-20";
		if(mLink != null && mLink.contains("amazon.com")){
			mLink= mLink.replaceAll("(?<=[?&;])tag=.*?($|[&;])", "");
			
			
		}	
		System.out.println("@@@ACTEST==>>"+mLink);
	}

	public  Map<String,SDModel>  populateData() {
		System.out.println("@@@@@@@@ACTEST...came to populate Data....");
		Map<String,SDModel> tmpMapOfData = new HashMap();
		try {

			// Validate.isTrue(args.length == 1, "usage: supply url to fetch");
			String url = "http://www.target.com/sb/-/N-5tdv0Z55e69Z55e6tZ55e6uZ55dw7#?lnk=ct_menu_11_11&intc=1865098|null";
			//getPageContent(url);
			print("Fetching %s...", url);

			Document doc = Jsoup.connect(url).get();
			Elements links = doc.select("a[href]");
			Elements media = doc.select("[src]");
			Elements imports = doc.select("link[href]");

			 print("\nMedia: (%d)", media.size());
			 for (Element src : media) {
			 if (src.tagName().equals("img"))
			 print(" * %s: <%s> %sx%s (%s)", src.tagName(),
			 src.attr("abs:src"), src.attr("width"), src.attr("height"),
			 trim(src.attr("alt"), 20));
			 else
			 print(" * %s: <%s>", src.tagName(), src.attr("abs:src"));
			 }
			
			 print("\nImports: (%d)", imports.size());
			 for (Element link : imports) {
			 print(" * %s <%s> (%s)", link.tagName(), link.attr("abs:href"),
			 link.attr("rel"));
			 }


		} catch (Exception mExcep) {
			mExcep.printStackTrace();
		}
		return tmpMapOfData;
	}

	private static String getPageContent(String aUrl){
		StringBuilder sb = new StringBuilder();
		try {
			URLConnection con = new URL(aUrl).openConnection();
			con.connect();
			InputStream is = con.getInputStream();

				// wrap the urlconnection in a bufferedreader
			      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
			      String line;
			      // read from the urlconnection via the bufferedreader
			      while ((line = bufferedReader.readLine()) != null)
			      {
			    	  sb.append(line);

			    	  System.out.println(line );
			      }
			      bufferedReader.close();			
				
			
			is.close();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.out.println("error:"+e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("error:"+e.getMessage());
		}
		return sb.toString();		
	}

	private static void print(String msg, Object... args) {
		System.out.println(String.format(msg, args));
	}

	private static String trim(String s, int width) {
		if (s.length() > width)
			return s.substring(0, width - 1) + ".";
		else
			return s;
	}

}
