package com.giftcard.scapper.helper;

import com.giftcard.deals.model.Product;
import com.giftcard.deals.model.ProductModel;
import com.giftcard.deals.model.SDModel;
import com.google.gson.*;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class BestBuyHelper {
	private static final String BESTBUY_API_KEY= "r6i00zWGw7KXqg9xziTGgmYU";
	private static final String[] mListOfCategoriesNamesToPull = {"Cameras","Headphones","Wii","xbox","Nintendo","Playstation","Health%20Fitness%20Beauty","Appliances","TVs","Home%20Automation","Fitness%20Watch","Computers%20Tablets","Laptops","Apple%20Watch","Cell%20Phones"};


	public static void main(String args[]) {
//		List<ProductModel> mList = getBestBuyItems("Samsung Galaxy watch");
//		for(ProductModel m: mList) {
//			System.out.println("@@@ACTEST "+m.toString());
//		}
		Map<String,SDModel> mMap = new HashMap<>();
		//populateTrendingDeals(mMap);
		populateDealOfDayAndDigitalInserts(mMap);
	}
	
	
	public static List<ProductModel> getBestBuyItems(String aSearchItem){
		List<ProductModel> mList = new ArrayList<>();
		String mSearchParam = "";
		if(aSearchItem != null) {
			//aSearchItem = aSearchItem.replaceAll(" ", "%20");
			
			String mArr[] = aSearchItem.split(" ");
			
			int mCounter = 0;
			for(String mSrchTerm:mArr ) {
				mCounter++;
				if(mCounter > 1) {
					mSearchParam += "&";
				}
				mSearchParam += "search="+mSrchTerm;
			}
		}
		
		String mNameFilter = "&name="+aSearchItem.replaceAll(" ", "%20")+"*";
		
		String mUrl = "https://api.bestbuy.com/v1/products(("+mSearchParam+")"+mNameFilter+"&customerReviewAverage%3E=4&type=hardgood)?format=json&show=categoryPath.name,sku,name,regularPrice,salePrice,image,url,customerReviewAverage,customerReviewCount&sort=salePrice.desc,bestSellingRank.asc,salesRankMediumTerm.asc&pageSize=100&apiKey="+BESTBUY_API_KEY;

		CloseableHttpClient httpclient = HttpClients.createDefault();
		System.out.println("@@@ACTEST BestBuy Search URL:"+mUrl);
		
		CloseableHttpResponse response = null;
		try {
			HttpGet httpget = new HttpGet(mUrl);
			response = httpclient.execute(httpget);
			
			HttpEntity entity = response.getEntity();
			Gson gson = new GsonBuilder().create();
	        ContentType contentType = ContentType.getOrDefault(entity);
	        Charset charset = contentType.getCharset();
	        Reader reader = new InputStreamReader(entity.getContent(), charset);

	        JsonParser jp = new JsonParser(); //from gson
	        JsonElement root = jp.parse(reader);
	        JsonObject rootobj = root.getAsJsonObject();
	        JsonArray mJsonArr = rootobj.getAsJsonArray("products");
	        
	        for(int i = 0; i < mJsonArr.size(); i++) {
	        		//String aPrice,String aProductName,String aUrl, String aDestUrl, String aSalePrice
	        		try {
						JsonObject mJsonObj = mJsonArr.get(i).getAsJsonObject();
						if(mJsonObj == null) {
							continue;
						}
						String mPrice = mJsonObj.get("regularPrice").getAsString();
						String mProductName = mJsonObj.get("name").getAsString();
						String mUrlImg = mJsonObj.get("image").getAsString();;
						String mDestUrl = mJsonObj.get("url").getAsString();
						String aSalePrice = mJsonObj.get("salePrice").getAsString();
						String mCustomerReviewAverage = mJsonObj.get("customerReviewAverage").getAsString();
						String mCustomerReviewCount = mJsonObj.get("customerReviewCount").getAsString();
						JsonArray mJsonArrForCategoryPath = rootobj.getAsJsonArray("categoryPath");
//						String mCategoryName = null;
//						if(mJsonArrForCategoryPath != null && mJsonArrForCategoryPath.size() >= 2) {
//							JsonObject mCategoryPathItemJsonObj = mJsonArrForCategoryPath.get(1).getAsJsonObject();// first item is Best Buy. Lets pick second element.
//							if(mCategoryPathItemJsonObj != null) {
//								mCategoryName = mCategoryPathItemJsonObj.get("name").getAsString();
//							}
//						}

						String categoryName = "";
						
						JsonArray jsonArray = mJsonObj.getAsJsonArray("categoryPath");
						for (JsonElement jsonElement : jsonArray) {
							if(categoryName != ""){
								categoryName += "|";
							}
							categoryName += jsonElement.getAsJsonObject().get("name").getAsString();
						}

						ProductModel mModel = new ProductModel(mPrice,mProductName,mUrlImg,mDestUrl,aSalePrice,"bestbuy.com",mCustomerReviewAverage,mCustomerReviewCount,categoryName, null);
						mList.add(mModel);
					} catch (Exception e) {
						// Ignore and move on	
						
					}
	        }
			
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		try {

		} finally {
			try {
				response.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		
		return mList;
		
	}
	
	// This method pulls trending deals from Best Buy API's
	public static void populateTrendingDeals(Map<String,SDModel> mMap) {
		String mUrl = null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		
		List<String> mCategories = getListOfCategories();
		for(String mCategoryId:mCategories) {
			mUrl = "https://api.bestbuy.com/beta/products/trendingViewed(categoryId="+mCategoryId+")?apiKey="+BESTBUY_API_KEY;
			System.out.println("@@@ACTEST mUrl:"+mUrl);;
			CloseableHttpResponse response = null;
			try {
				HttpGet httpget = new HttpGet(mUrl);
				response = httpclient.execute(httpget);
				
				HttpEntity entity = response.getEntity();
				Gson gson = new GsonBuilder().create();
		        ContentType contentType = ContentType.getOrDefault(entity);
		        Charset charset = contentType.getCharset();
		        Reader reader = new InputStreamReader(entity.getContent(), charset);

		        JsonParser jp = new JsonParser(); //from gson
		        JsonElement root = jp.parse(reader);
		        JsonObject rootobj = root.getAsJsonObject();
		        JsonArray mJsonArr = rootobj.getAsJsonArray("results");
		        
		        int mCounter = 0;
		        for(int i = 0; i < mJsonArr.size(); i++) {
		        		//String aPrice,String aProductName,String aUrl, String aDestUrl, String aSalePrice
		        		try {
							JsonObject mJsonObj = mJsonArr.get(i).getAsJsonObject();
							if(mJsonObj == null) {
								continue;
							}
							mCounter++;
							if(mCounter > 10) {
								continue;
							}
							String mPrice = mJsonObj.getAsJsonObject("prices").get("regular").getAsString();
							String mProductName = mJsonObj.getAsJsonObject("names").get("title").getAsString();
							String mUrlImg = mJsonObj.getAsJsonObject("images").get("standard").getAsString();;
							String mDestUrl = mJsonObj.getAsJsonObject("links").get("web").getAsString();
							String mSalePrice = mJsonObj.getAsJsonObject("prices").get("current").getAsString();
							
							String mCustomerReviewAverage = mJsonObj.getAsJsonObject("customerReviews").get("averageScore").getAsString();
							String mCustomerReviewCount = mJsonObj.getAsJsonObject("customerReviews").get("count").getAsString();;

							String categoryName = "";
							
							JsonArray jsonArray = mJsonObj.getAsJsonArray("categoryPath");
							for (JsonElement jsonElement : jsonArray) {
								if(categoryName != ""){
									categoryName += "|";
								}
								categoryName += jsonElement.getAsJsonObject().get("name").getAsString();
							}
							//ProductModel mModel = new ProductModel(mPrice,mProductName,mUrlImg,mDestUrl,mSalePrice,"bestbuy.com");
							
							Product mProdModel = new Product(categoryName, mDestUrl, mProductName, mUrlImg, Double.parseDouble(mPrice), Double.parseDouble(mSalePrice),mCustomerReviewAverage,mCustomerReviewCount, null);
							
							
							
							System.out.println("@@@ACTEST"+mProdModel.toString());
							SDModel mSDModel = new SDModel(mProdModel);
							mMap.put(mDestUrl, mSDModel);
						} catch (Exception e) {
							// Ignore and move on	
							
						}
		        }
		        
		        
				
			} 
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 

			try {

			} finally {
				try {
					response.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}				
		}
		System.out.println("@@@ACTEST best buy product size:"+mMap.size());
	}
	
	
	
	public static void populateDealOfDayAndDigitalInserts(Map<String,SDModel> mMap) {
		int mPageCounter = 0;
		int mTotalPages = 1;// default
		while(true) {
			mPageCounter++;
//			if (mPageCounter >= 10 || mPageCounter > mTotalPages) {
//				break;
//			}

			if(mPageCounter > 1){
				// Best buy is creating too many records - reducing the size to 1 page - 06/14/2019
				break;
			}

			String mUrl = "https://api.bestbuy.com/v1/products(offers.type%20in%20(deal_of_the_day,digital_insert))?format=json&pageSize=100&page="+mPageCounter+"&show=categoryPath.name,offers.type,sku,name,regularPrice,salePrice,image,url,customerReviewAverage,customerReviewCount&apiKey="+BESTBUY_API_KEY;
			
			CloseableHttpClient httpclient = HttpClients.createDefault();
			System.out.println("@@@ACTEST BestBuy Search URL:"+mUrl);
			
			CloseableHttpResponse response = null;
			try {
				HttpGet httpget = new HttpGet(mUrl);
				response = httpclient.execute(httpget);
				
				HttpEntity entity = response.getEntity();
				Gson gson = new GsonBuilder().create();
		        ContentType contentType = ContentType.getOrDefault(entity);
		        Charset charset = contentType.getCharset();
		        Reader reader = new InputStreamReader(entity.getContent(), charset);

		        JsonParser jp = new JsonParser(); //from gson
		        JsonElement root = jp.parse(reader);
		        JsonObject rootobj = root.getAsJsonObject();
		        JsonArray mJsonArr = rootobj.getAsJsonArray("products");
		        
		        mTotalPages = rootobj.get("totalPages").getAsInt();
		        System.out.println("@@@ACTEST "+rootobj.get("totalPages").getAsInt());
		        
		        for(int i = 0; i < mJsonArr.size(); i++) {
		        		//String aPrice,String aProductName,String aUrl, String aDestUrl, String aSalePrice
		        		try {
							JsonObject mJsonObj = mJsonArr.get(i).getAsJsonObject();
							if(mJsonObj == null) {
								continue;
							}
							String mPrice = mJsonObj.get("regularPrice").getAsString();
							String mProductName = mJsonObj.get("name").getAsString();
							String mUrlImg = mJsonObj.get("image").getAsString();;
							String mDestUrl = mJsonObj.get("url").getAsString();
							String aSalePrice = mJsonObj.get("salePrice").getAsString();
							String mCustomerReviewAverage = mJsonObj.get("customerReviewAverage").getAsString();
							String mCustomerReviewCount = mJsonObj.get("customerReviewCount").getAsString();
//							JsonArray mJsonArrForCategoryPath = mJsonObj.getAsJsonArray("categoryPath");
//							String mCategoryName = null;
//							if(mJsonArrForCategoryPath != null && mJsonArrForCategoryPath.size() >= 2) {
//								JsonObject mCategoryPathItemJsonObj = mJsonArrForCategoryPath.get(1).getAsJsonObject();// first item is Best Buy. Lets pick second element.
//								if(mCategoryPathItemJsonObj != null) {
//									mCategoryName = mCategoryPathItemJsonObj.get("name").getAsString();
//									
//								}
//							}
							
							String categoryName = "";
							JsonArray jsonArray = mJsonObj.getAsJsonArray("categoryPath");
							for (JsonElement jsonElement : jsonArray) {
								if(categoryName != ""){
									categoryName += "|";
								}
								categoryName += jsonElement.getAsJsonObject().get("name").getAsString();
							}
							
							String mOfferType = null;
							JsonArray mJsonArrForOffersType = mJsonObj.getAsJsonArray("offers");
							if(mJsonArrForOffersType != null && mJsonArrForOffersType.size() >= 1) {
								JsonObject mOffersTypeItemJsonObj = mJsonArrForOffersType.get(0).getAsJsonObject();
								if(mOffersTypeItemJsonObj != null) {
									mOfferType = mOffersTypeItemJsonObj.get("type").getAsString();
								}
							}
							
							
							ProductModel mModel = new ProductModel(mPrice,mProductName,mUrlImg,mDestUrl,aSalePrice,"bestbuy.com",mCustomerReviewAverage,mCustomerReviewCount,categoryName, null);
							SDModel mSDModel = new SDModel(mModel);
							//mSDModel.setOfferType(mOfferType); // removing the offer type
							//System.out.println("@@@ACTEST mOfferType:"+mOfferType);;
							
							if(mDestUrl != null && !mMap.containsKey(mDestUrl)) {
								mMap.put(mDestUrl, mSDModel);
							}
						} catch (Exception e) {
							// Ignore and move on	
							//e.printStackTrace();
						}
		        }
			} 
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			try {

			} finally {
				try {
					response.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}				
						
			
			
		}
		

		System.out.println("@@@ACTEST product size after adding digital inserts and deal of the day:"+mMap.size());
	}
	
	
	
	public static List<String> getListOfCategories() {
		//String mUrl = "https://api.bestbuy.com/v1/categories(id=abcat*)?&pageSize=100&show=id,name&format=json&apiKey="+BESTBUY_API_KEY;
		String mUrl = null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		List<String> mListOfCategories = new ArrayList<>();		
		
		for(String mCategory: mListOfCategoriesNamesToPull) {
			mUrl = "https://api.bestbuy.com/v1/categories(name="+mCategory+"*)?show=name,id&format=json&apiKey="+BESTBUY_API_KEY;
			CloseableHttpResponse response = null;
			try {
				HttpGet httpget = new HttpGet(mUrl);
				response = httpclient.execute(httpget);
				
				HttpEntity entity = response.getEntity();
				Gson gson = new GsonBuilder().create();
		        ContentType contentType = ContentType.getOrDefault(entity);
		        Charset charset = contentType.getCharset();
		        Reader reader = new InputStreamReader(entity.getContent(), charset);

		        JsonParser jp = new JsonParser(); //from gson
		        JsonElement root = jp.parse(reader);
		        JsonObject rootobj = root.getAsJsonObject();
		        JsonArray mJsonArr = rootobj.getAsJsonArray("categories");
		        
		        for(int i = 0; i < mJsonArr.size(); i++) {
		        		//String aPrice,String aProductName,String aUrl, String aDestUrl, String aSalePrice
		        		try {
							String mId = mJsonArr.get(i).getAsJsonObject().get("id").getAsString();
							System.out.println("@@@@@@ACTEST NAME:"+mJsonArr.get(i).getAsJsonObject().get("name").getAsString());
							String mName = mJsonArr.get(i).getAsJsonObject().get("name").getAsString();
							if(mName != null && mName.toLowerCase().contains("dvd")) {
								continue;
							}
							
							if(!mListOfCategories.contains(mId)) {
								mListOfCategories.add(mId);
							}
						} catch (Exception e) {
							// Ignore and move on	
						}
		        }
				
			} 
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			finally {
				try {
					response.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		

		return mListOfCategories;
	}
}
