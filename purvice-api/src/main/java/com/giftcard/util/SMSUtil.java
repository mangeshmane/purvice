package com.giftcard.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SMSUtil {

	private static final String ACCOUNT_SID = "AC989eba320268755d7761b75ec725822b";
	private static final String AUTH_TOKEN = "b28d4b4d406905160d777c08217e2038";
	private static final String FROM = "+13239224601";
	
	private static final Logger logger = LoggerFactory.getLogger(SMSUtil.class);
	
	public static void sendMessage(String to,String body){
		
		try{
			    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

			    Message message = Message.creator(new PhoneNumber(to),
			        new PhoneNumber(FROM), body).create();
			  
	    } catch(Exception ex) {
	    	logger.error("Error while sending message", ex);

	    }
	}
}
