package com.giftcard.util;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
public class BrandUtil {
	private static final Logger logger = LoggerFactory.getLogger(BrandUtil.class);
	 
	 public static String getCountryCode(String latitude,String longitude){
	   final String uri = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+","+longitude+"&sensor=false";
	    String countryCode = null;
	      RestTemplate restTemplate = new RestTemplate();
	      String output = restTemplate.getForObject(uri, String.class);
	      logger.debug("API Result:===="+output);
	      JSONObject json = JSONObject.fromObject(output);
	               
	      JSONArray jsonArray = json.getJSONArray("results");
	      
	      List<Object> list = (List<Object>) jsonArray.getJSONObject(0).get("address_components");
	      Object[] objArray = list.toArray();
	      for (int i = 0; i < objArray.length; i++) {
	       String values = (String) objArray[i].toString();
	       if(values.contains("country")){
	        String[] locationDetails = values.split(",");
	        String[] countryDetails = locationDetails[1].split(":");
	        countryCode = countryDetails[1].replace('"', ' ').trim().toLowerCase();
	        logger.debug("countryCode:===="+countryCode);
	        return countryCode;
	       }
	   }
	      return countryCode;
	 }
	 /*
	    // test client
	    public static void main(String[] args) {
	     //India latlong
	     //String latitude = "15.8497";
	     //String longitude = "74.4977";
	     
	     //USA latlong
	     //String latitude = "37.773972";
	     //String longitude = "-122.431297"; 
	     
	     //Canada latlong
	     String latitude = "43.653908";
	        String longitude = "-79.384293";
	     getCountryCode(latitude,longitude);
	    }
	*/
}
