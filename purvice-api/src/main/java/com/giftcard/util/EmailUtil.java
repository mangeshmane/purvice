package com.giftcard.util;

import java.util.Date;

import javax.mail.Message;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class EmailUtil {

	/**
	 * Utility method to send simple HTML email
	 * @param session
	 * @param toEmail
	 * @param subject
	 * @param body
	 */

	private static final Logger logger = LoggerFactory.getLogger(EmailUtil.class);
	
	public static void sendEmail(Session session, String toEmail, String subject, String body){
		try
	    {
			
	      MimeMessage msg = new MimeMessage(session);
			
	      //set message headers
	      
	     msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
	     msg.addHeader("format", "flowed");
	     msg.addHeader("Content-Transfer-Encoding", "8bit");
	      msg.setFrom(new InternetAddress("purvice.test@gmail.com", "PURVICE"));

	      msg.setReplyTo(InternetAddress.parse("purvice.test@gmail.com", false));
	      
	     
	      msg.setSubject(subject, "UTF-8");
	      
	      msg.setContent(body, "text/html; charset=utf-8");
	     
	      msg.setSentDate(new Date());

	      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
	      
    	  Transport.send(msg);  
	      
	    }
		catch(SendFailedException s){
			logger.error("Error while sending mail",s);
		}
	    catch (Exception e) {
	    	logger.error("Error while sending mail",e);
	    }
	}
	
	
	public static void sendFeedbackEmail(Session session, String toEmail, String subject, String body, String fromEmail){
		try
	    {
			
	      MimeMessage msg = new MimeMessage(session);
			
	      //set message headers
	      
	     msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
	     msg.addHeader("format", "flowed");
	     msg.addHeader("Content-Transfer-Encoding", "8bit");
	      msg.setFrom(new InternetAddress(fromEmail, "PURVICE"));

	      msg.setReplyTo(InternetAddress.parse(fromEmail, false));
	      
	     
	      msg.setSubject(subject, "UTF-8");
	      
	      msg.setContent(body, "text/html; charset=utf-8");
	     
	      msg.setSentDate(new Date());

	      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
	      
    	  Transport.send(msg);  
	      
	    }
		catch(SendFailedException s){
			logger.error("Error while sending mail",s);
		}
	    catch (Exception e) {
	    	logger.error("Error while sending mail",e);
	    }
	}
}
