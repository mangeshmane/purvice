package com.giftcard.deals.model;
import java.net.URL;

import com.amazonaws.services.dynamodbv2.document.Item;

public class ProductModel {
	private String iUrl;
	private String iPrice;
	private String iProductName;
	private String destUrl;
	private String salePrice;
	private String source;
	private String customerReviewAverage;
	private String customerReviewCount;
	private String category;
	private String addToCartUrl;
	
	public ProductModel(Item aItem){
		if(aItem == null){
			return;
		}
		this.setPrice(aItem.getString("FormattedPrice"));
		this.setProductName(aItem.getString("product"));
		this.setUrl(aItem.getString("url"));
		this.setDestUrl(aItem.getString("desturl"));
		this.setSalePrice(aItem.getString("salePrice"));
		this.setSource("amazon.com");
	}
	
	public ProductModel(String aPrice,String aProductName,String aUrl, String aDestUrl, String aSalePrice,String aSource,String aCustomerReviewAverage,String aCustomerReviewCount,String aCategory, String aAddToCartUrl){
		this.setPrice(aPrice);
		this.setProductName(aProductName);
		this.setUrl(aUrl);
		this.setDestUrl(aDestUrl);
		this.setSalePrice(aSalePrice);
		this.setSource(aSource);
		this.setCustomerReviewAverage(aCustomerReviewAverage);
		this.setCustomerReviewCount(aCustomerReviewCount);
		this.setCategory(aCategory);
		this.setAddToCartUrl(aAddToCartUrl);
	}
	
	
	public String getUrl() {
		return iUrl;
	}
	public void setUrl(String url) {
		this.iUrl = url;
	}
	public String getPrice() {
		return iPrice;
	}
	public void setPrice(String price) {
		this.iPrice = price;
	}
	public String getProductName() {
		return iProductName;
	}
	public void setProductName(String productName) {
		this.iProductName = productName;
	}


	public String getDestUrl() {
		return destUrl;
	}


	public void setDestUrl(String destUrl) {
		this.destUrl = destUrl;
	}
	
	public String toString(){
		//return "url:"+iUrl+",price:"+iPrice+",productname:"+iProductName+",destUrl:"+destUrl;
		return "["+iProductName+"]["+iPrice+"]["+salePrice+"]["+destUrl+"]["+iUrl+"]["+source+"]["+category+"]["+addToCartUrl+"]";
	}




	public String getSalePrice() {
		return salePrice;
	}




	public void setSalePrice(String salePrice) {
		this.salePrice = salePrice;
	}
	
	public String getImgAlt(){
		String mHost = "Deal Logo";
		try {
			if(getDestUrl() != null && !getDestUrl().equals("")){
				URL mUrl = new URL(getDestUrl());
				mHost = mUrl.getHost();
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mHost;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the customerReviewAverage
	 */
	public String getCustomerReviewAverage() {
		return customerReviewAverage;
	}

	/**
	 * @param customerReviewAverage the customerReviewAverage to set
	 */
	public void setCustomerReviewAverage(String customerReviewAverage) {
		this.customerReviewAverage = customerReviewAverage;
	}

	/**
	 * @return the customerReviewCount
	 */
	public String getCustomerReviewCount() {
		return customerReviewCount;
	}

	/**
	 * @param customerReviewCount the customerReviewCount to set
	 */
	public void setCustomerReviewCount(String customerReviewCount) {
		this.customerReviewCount = customerReviewCount;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getAddToCartUrl() {
		return addToCartUrl;
	}

	public void setAddToCartUrl(String addToCartUrl) {
		this.addToCartUrl = addToCartUrl;
	}
}
