package com.giftcard.deals.model;

import org.apache.commons.lang3.StringUtils;

public class ForumProduct extends Product {
	
	private String description;
	
	public ForumProduct(String description, String imageUrl, String productUrl) {
		this.setImageUrl(imageUrl);
		this.setProductUrl(productUrl);
		this.description = description;
		
		this.setProductName(description);
		this.setCategory(getCategory());
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString() {
		return StringUtils.join(new Object[]{description, getCategory(), getImageUrl(), getProductUrl()}, "\t");
	}

}
