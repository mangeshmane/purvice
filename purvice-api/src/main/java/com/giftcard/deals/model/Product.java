package com.giftcard.deals.model;

import org.apache.commons.lang3.StringUtils;

public class Product {
	private String productName;
	private String category;
	private String productUrl;
	private String imageUrl;
	private Double basePrice;
	private Double salePrice;
	private String customerReviewAverage;
	private String customerReviewCount;
	private String addToCartUrl;
	
	public Product() {
		super();
	}
	
	public Product(String category, String productUrl, String productName, String imageUrl, Double basePrice,Double salePrice) {
		new Product(category,  productUrl,  productName,  imageUrl,  basePrice, salePrice, null, null, null);
	}
	
	public Product(String category, String productUrl, String productName, String imageUrl, Double basePrice,
			Double salePrice, String aCustomerReviewAverage, String aCustomerReviewCount, String aAddToCartUrl) {
		this.category = category;
		this.productUrl = productUrl;
		this.productName = productName;
		this.imageUrl = imageUrl;
		this.basePrice = basePrice;
		this.salePrice = salePrice;
		this.customerReviewAverage = aCustomerReviewAverage;
		this.customerReviewCount = aCustomerReviewCount;
		this.addToCartUrl = aAddToCartUrl;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @return the basePrice
	 */
	public Double getBasePrice() {
		return basePrice;
	}

	/**
	 * @return the salePrice
	 */
	public Double getSalePrice() {
		return salePrice;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @param basePrice the basePrice to set
	 */
	public void setBasePrice(Double basePrice) {
		this.basePrice = basePrice;
	}

	/**
	 * @param salePrice the salePrice to set
	 */
	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

	/**
	 * @return the productUrl
	 */
	public String getProductUrl() {
		return productUrl;
	}

	/**
	 * @param productUrl the productUrl to set
	 */
	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}

	public String toString() {
		return StringUtils.join(new Object[]{productName, category, salePrice, basePrice, imageUrl, productUrl,customerReviewAverage,customerReviewCount}, "\t");
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the customerReviewAverage
	 */
	public String getCustomerReviewAverage() {
		return customerReviewAverage;
	}

	/**
	 * @param customerReviewAverage the customerReviewAverage to set
	 */
	public void setCustomerReviewAverage(String customerReviewAverage) {
		this.customerReviewAverage = customerReviewAverage;
	}

	/**
	 * @return the customerReviewCount
	 */
	public String getCustomerReviewCount() {
		return customerReviewCount;
	}

	/**
	 * @param customerReviewCount the customerReviewCount to set
	 */
	public void setCustomerReviewCount(String customerReviewCount) {
		this.customerReviewCount = customerReviewCount;
	}

	public String getAddToCartUrl() {
		return addToCartUrl;
	}

	public void setAddToCartUrl(String addToCartUrl) {
		this.addToCartUrl = addToCartUrl;
	}
}

