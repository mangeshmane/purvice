package com.giftcard.deals.model;

public class AmazonMetadata {
	
	private String marketplaceID;
	private String clientID;
	
	public AmazonMetadata() {
		super();
	}
	
	public AmazonMetadata(String marketplaceID, String clientID) {
		this.marketplaceID = marketplaceID;
		this.clientID = clientID;
	}
	
	/**
	 * @return the marketplaceID
	 */
	public String getMarketplaceID() {
		return marketplaceID;
	}
	/**
	 * @return the clientID
	 */
	public String getClientID() {
		return clientID;
	}
	/**
	 * @param marketplaceID the marketplaceID to set
	 */
	public void setMarketplaceID(String marketplaceID) {
		this.marketplaceID = marketplaceID;
	}
	/**
	 * @param clientID the clientID to set
	 */
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	
	

}
