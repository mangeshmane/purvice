package com.giftcard.deals.model;

import java.util.ArrayList;
import java.util.List;

public class AmazonPayload {
	private AmazonMetadata requestMetadata;
	private List<AmazonDeal> dealTargets;
	
	public AmazonPayload() {
		super();
	}
	
	public AmazonPayload(AmazonMetadata requestMetadata,
			List<AmazonDeal> dealTargets) {
		this.requestMetadata = requestMetadata;
		this.dealTargets = dealTargets;
	}
	
	/**
	 * @return the requestMetadata
	 */
	public AmazonMetadata getRequestMetadata() {
		return requestMetadata;
	}
	/**
	 * @return the dealTargets
	 */
	public List<AmazonDeal> getDealTargets() {
		return dealTargets;
	}
	/**
	 * @param requestMetadata the requestMetadata to set
	 */
	public void setRequestMetadata(AmazonMetadata requestMetadata) {
		this.requestMetadata = requestMetadata;
	}
	/**
	 * @param dealTargets the dealTargets to set
	 */
	public void setDealTargets(List<AmazonDeal> dealTargets) {
		this.dealTargets = dealTargets;
	}
	
	

}