package com.giftcard.deals.model;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class JsoupUtil {
	
	public static Document getDocument(String url) {
		Document doc = null;
		try {
			doc = Jsoup.connect(url).timeout(50000).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return doc;
	}
	
	public static Document getDocument(String url, String cookie) {
		Document doc = null;
		try {
			doc = Jsoup.connect(url).header("Cookie", cookie).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return doc;
	}
	
	public static Element getElement(Element parentNode, String cssQuery) {
		Element res = null;
		if(parentNode != null) {
			res = parentNode.select(cssQuery).first();
		}
		return res;
	}

	public static Double parsePrice(String text) {
		
		Double price = 0.0;
		String priceString = null;
		try {
			Pattern regex = Pattern.compile("[-+]?\\b[0-9]+(\\.[0-9]+)?\\b");
			Matcher regexMatcher = regex.matcher(text.replaceAll(",", ""));
			if (regexMatcher.find()) {
				priceString = regexMatcher.group();
				price = Double.valueOf(priceString);
			} 
		} catch (PatternSyntaxException ex) {
			ex.printStackTrace();
		}
		return price;
	}
}
