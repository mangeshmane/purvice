package com.giftcard.deals.model;

public class AmazonDeal {
	
	private String dealID;
	
	public AmazonDeal() {}

	public AmazonDeal(String dealID) {
		this.dealID = dealID;
	}

	/**
	 * @return the dealID
	 */
	public String getDealID() {
		return dealID;
	}

	/**
	 * @param dealID the dealID to set
	 */
	public void setDealID(String dealID) {
		this.dealID = dealID;
	}
}
