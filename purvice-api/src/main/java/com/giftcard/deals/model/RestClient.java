package com.giftcard.deals.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * @author xtutran
 * 
 */

public class RestClient {

	public static final ObjectMapper JSONNER = new ObjectMapper();
	public static final String[] HEADERS = new String[] {"Product Name", "Product Category", "Price", "Sale Price", "Image URL", "Product URL"};

	private static HttpClient httpClient = HttpClientBuilder.create().build();

	public static String getResponse(String url) {
		
		String responseBody = "";
		BufferedReader reader = null;
		InputStream ins = null;
		try {
			// Create new getRequest with below mentioned URL
			HttpGet getRequest = new HttpGet(url);

			// Add additional header to getRequest which accepts application/xml data
			getRequest.addHeader("accept", "application/json");

			//getRequest.addHeader("x-macys-webservice-client-id", "yhvvqfyuymxvjpvzuxruzxyb");

			// Execute your request and catch response
			HttpResponse response = httpClient.execute(getRequest);

			// Check for HTTP response code: 200 = success
			if (response.getStatusLine().getStatusCode() != 200) {
				//throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
				return null;
			}

			ins = response.getEntity().getContent();

			if(ins != null ) {
				String line = "";
				reader = new BufferedReader(new InputStreamReader(ins));
				while((line = reader.readLine()) != null) {
					responseBody += line;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
			//throw new RuntimeException(e.getCause());
		} finally {
			if(ins != null) {
				try {
					ins.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return responseBody;
	}

	public static String post(String restAPI, String payload) throws IOException {
		HttpPost post = new HttpPost(restAPI);
		
		StringEntity entity = new StringEntity(payload);
		post.addHeader("content-type", "application/json");
		post.setEntity(entity);
		
		HttpResponse response = httpClient.execute(post);
		
		// Check for HTTP response code: 200 = success
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
		}

		InputStream ins = response.getEntity().getContent();
		String responseBody = "";
		if(ins != null ) {
			String line = "";
			BufferedReader reader = new BufferedReader(new InputStreamReader(ins));
			while((line = reader.readLine()) != null) {
				responseBody += line;
			}
			reader.close();
		}
		ins.close();
		
		return responseBody;
	}
	
	public static void writeCsv(BufferedWriter writer, Object[] products) throws IOException {
		if(writer != null && products.length > 0) {
			for(Object info : products) {
				writer.write(info.toString());
			}
		}
	}
}
