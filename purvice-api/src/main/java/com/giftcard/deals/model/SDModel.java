package com.giftcard.deals.model;

import java.net.URL;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;




public class SDModel implements Comparable<SDModel>{
	private String url;
	private String text;
	private String imageUrl;
	private String category;
	private String source;
	private int random;
	private double originalPrice;
	private double salePrice;
	private Date firstSeen;
	private boolean sdDeal;
	private String affliateLink;
	private static List<String> iListOfFavIcons = Arrays.asList("amazon","bestbuy","walmart","bestbuy","samsung","costco","newegg","target","costco","ebay","kmart","forever21","zara","zappos","nordstrom","hpb","barnesandnoble","kohls","homedepot","lowes");
	private String customerReviewAverage;
	private String customerReviewCount;
	private String uniqueId;
	private String offerType;
	private Long likes = (long) 0;
	private Long dislikes = (long) 0;
	private String addToCartUrl;
	
	
	public SDModel(Product p) {
		String mActualUrl = p.getProductUrl();
		if(mActualUrl != null && mActualUrl.contains("amazon.com")){
			mActualUrl = mActualUrl+"/?tag=purvice-20";
		}
		else if(mActualUrl != null && mActualUrl.contains("target.com")){
			mActualUrl = mActualUrl+"/?lnm=81938&afid=Purvice&ref=tgt_adv_xasd0002";
		}
		
		
		
		this.setUrl(mActualUrl);
		this.setImageUrl(p.getImageUrl());
		
		String mText = p.getProductName();
		
		if(mText == null){
			mText = "Great deal at ";
			if(p.getProductUrl() != null && p.getProductUrl().contains(".com")){
				mText = mText + p.getProductUrl().substring(0, p.getProductUrl().indexOf(".com")+4);
			}
			else if(p.getProductUrl() != null && p.getProductUrl().contains(".net")){
				mText = mText + p.getProductUrl().substring(0, p.getProductUrl().indexOf(".net")+4);
			}
		}
		
		if(p.getSalePrice() != null && p.getSalePrice()>=0){
			String mPrice = getFormattedCurrency(p.getSalePrice());
			if(p.getSalePrice() == null || p.getSalePrice() == 0.0){
				mPrice = "Too Low to Display";
			}
			mText = mText + " <font color='red'><b>SALE PRICE: ["+mPrice+"]</b></font>";
			this.setSalePrice(p.getSalePrice());
		}		
		if(p.getBasePrice() != null && p.getBasePrice() > 0){
			if(p.getSalePrice() != null && p.getBasePrice() != null && p.getSalePrice()  < p.getBasePrice()) {
				mText = mText + "&nbsp;Original price:[<s>"+getFormattedCurrency(p.getBasePrice())+"</s>]";
			}
			this.setOriginalPrice(p.getBasePrice());
		}
		
		this.setText(mText);
		
		if(p.getCategory() != null && !p.getCategory().trim().equals("")){
			this.setCategory(p.getCategory());
		}	
		else{
			this.setCategory("misc");
		}
		
		this.setRandom((int)(Math.random() * ((1000 - 0) + 1)));
		this.setFirstSeen(getCurrentTime());
		this.setCustomerReviewAverage(p.getCustomerReviewAverage());
		this.setCustomerReviewCount(p.getCustomerReviewCount());
		this.setAddToCartUrl(p.getAddToCartUrl());
		
	}
	
	private static Date getCurrentTime(){
		return Calendar.getInstance().getTime();
		/*
		Calendar cal = Calendar.getInstance(); // locale-specific
		cal.setTime(Calendar.getInstance().getTime());
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
		*/
	}
	
	public SDModel(ProductModel p) {
		String mActualUrl = p.getDestUrl();
		
		if(mActualUrl != null && mActualUrl.contains("amazon.com") &&  !mActualUrl.contains("purvice-20")){
			mActualUrl = mActualUrl+"/?tag=purvice-20";
		}
		else if(mActualUrl != null && mActualUrl.contains("target.com")){
			mActualUrl = mActualUrl+"/?clkid=43aaf581N2b2ec4403b48c28989e3eb44&lnm=81938&afid=Purvice&ref=tgt_adv_xasd0002";
		}
		
		this.setUrl(mActualUrl);
		this.setImageUrl(p.getUrl());
		
		String mText = p.getProductName();
		
		if(mText == null){
			mText = "Great deal at ";
			if(p.getDestUrl() != null && p.getDestUrl().contains(".com")){
				mText = mText + p.getDestUrl().substring(0, p.getDestUrl().indexOf(".com")+4);
			}
			else if(p.getDestUrl() != null && p.getDestUrl().contains(".net")){
				mText = mText + p.getDestUrl().substring(0, p.getDestUrl().indexOf(".net")+4);
			}
		}
		
		if(p.getSalePrice() != null ){
			mText = mText + " <font color='red'><b>SALE PRICE: ["+p.getSalePrice()+"]</b></font>";
			try {
				this.setSalePrice(Double.parseDouble(p.getSalePrice()));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				this.setSalePrice(0);
			}
		}		
		if(p.getPrice() != null){
			

			try {
				
			if(p.getSalePrice() != null && p.getPrice() != null && Double.parseDouble(p.getSalePrice())  < Double.parseDouble(p.getPrice())) {
				mText = mText + "&nbsp;Original price:[<s>"+p.getPrice()+"</s>]";
			}				
				
			this.setOriginalPrice(Double.parseDouble(p.getPrice()));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				this.setOriginalPrice(0);
			}
		}
		
		this.setText(mText);
		
		if(p.getCategory() == null || p.getCategory().equals("")) {
			this.setCategory("misc");
		}else{
			this.setCategory(p.getCategory());
		}
		
		
		this.setRandom((int)(Math.random() * ((1000 - 0) + 1)));
		this.setFirstSeen(getCurrentTime());
		
		this.setCustomerReviewAverage(p.getCustomerReviewAverage());
		this.setCustomerReviewCount(p.getCustomerReviewCount());
		this.setAddToCartUrl(p.getAddToCartUrl());
		
	}
	
	private void setHostName(String mUrl){
		try {
			URL m = new URL(mUrl);
			this.setSource(m.getHost());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
	
	public SDModel() {
		// TODO Auto-generated constructor stub
		this.setCategory("misc");
		this.setFirstSeen(Calendar.getInstance().getTime());
		this.setSdDeal(true);
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
		this.setHostName(url);
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getImageUrl() {
		if(imageUrl == null || imageUrl.equals("")){
			imageUrl = "img/purvice_deals.jpg";
		}
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public boolean isImageUrlFound(){
		if(imageUrl == null || imageUrl.equals("") || imageUrl.equals("img/purvice_deals.jpg")){
			return false;
		}
		return true;
	}
	
	public String getImgAlt(){
		String mHost = "Deal Logo";
		try {
			if(getUrl() != null && !getUrl().equals("")){
				URL mUrl = new URL(getUrl());
				mHost = mUrl.getHost();
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mHost;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getRandom() {
		return random;
	}

	public void setRandom(int random) {
		this.random = random;
	}
	
	private static String getFormattedCurrency(double amt){
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		return formatter.format(amt);
	}

	public double getOriginalPrice() {
		return originalPrice;
	}

	public void setOriginalPrice(double originalPrice) {
		this.originalPrice = originalPrice;
	}

	public double getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(double salePrice) {
		this.salePrice = salePrice;
	}
	
	public Long getLikes() {
		return likes;
	}

	public void setLikes(Long likes) {
		this.likes = likes;
	}

	public Long getDislikes() {
		return dislikes;
	}

	public void setDislikes(Long dislikes) {
		this.dislikes = dislikes;
	}

	public  int getDiscountPercent() {
		int mDiscountPct = (int)Math.round((this.getOriginalPrice() - this.getSalePrice()) * 100 / this.getOriginalPrice());
		
		if(mDiscountPct > 0) {
			return mDiscountPct;
		}
		return 0;
		
	}
	

	@Override
	public int compareTo(SDModel o2) {
		if(this.getOriginalPrice() == 0 || this.getSalePrice() == 0){
			return -1;
		}
		else if(o2.getOriginalPrice() == 0 || o2.getSalePrice() == 0){
			return 1;
		}
		else{
			double salePctO1 = (this.getOriginalPrice()-this.getSalePrice())*100/(this.getOriginalPrice());
			double salePctO2 = (o2.getOriginalPrice()-o2.getSalePrice())*100/(o2.getOriginalPrice());
			return new Double(salePctO1).compareTo(new Double(salePctO2));
		}	

	}

	public Date getFirstSeen() {
		return firstSeen;
	}

	public void setFirstSeen(Date firstSeen) {
		this.firstSeen = firstSeen;
	}

	public boolean isSdDeal() {
		return sdDeal;
	}

	public void setSdDeal(boolean sdDeal) {
		this.sdDeal = sdDeal;
	}
	
	public String getSourceFavIco() {
		String mSourceFavIco = null;
		if(getSource() != null && getSource().contains(".com")) {
			for(String mItem: iListOfFavIcons) {
				if(getSource().contains(mItem)) {
					mSourceFavIco = "img/"+mItem + "_favicon.ico";
					break;
				}
			}
		}
		return mSourceFavIco;
	}

	/**
	 * @return the affliateLink
	 */
	public String getAffliateLink() {
		return affliateLink;
	}

	/**
	 * @param affliateLink the affliateLink to set
	 */
	public void setAffliateLink(String affliateLink) {
		this.affliateLink = affliateLink;
	}

	/**
	 * @return the customerReviewAverage
	 */
	public String getCustomerReviewAverage() {
		return customerReviewAverage;
	}

	/**
	 * @param customerReviewAverage the customerReviewAverage to set
	 */
	public void setCustomerReviewAverage(String customerReviewAverage) {
		this.customerReviewAverage = customerReviewAverage;
	}

	/**
	 * @return the customerReviewCount
	 */
	public String getCustomerReviewCount() {
		return customerReviewCount;
	}

	/**
	 * @param customerReviewCount the customerReviewCount to set
	 */
	public void setCustomerReviewCount(String customerReviewCount) {
		this.customerReviewCount = customerReviewCount;
	}

	/**
	 * @return the uniqueId
	 */
	public String getUniqueId() {
		String mUniqueId = "";
		if(this.getSource() != null) {
			mUniqueId = mUniqueId + this.getSource();
		}
		
		if(this.getText() != null) {
			
//			if(this.getText().length() > 30) {
//				mUniqueId = mUniqueId +this.getText().substring(0, 30);
//			}
//			else {
				mUniqueId = mUniqueId +this.getText();
			//}
		}
		mUniqueId = mUniqueId.replaceAll("[^a-zA-Z0-9]", "");    
		return mUniqueId;
	}

	/**
	 * @return the offerType
	 */
	public String getOfferType() {
		return offerType;
	}

	/**
	 * @param offerType the offerType to set
	 */
	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public String getAddToCartUrl() {
		return addToCartUrl;
	}

	public void setAddToCartUrl(String addToCartUrl) {
		this.addToCartUrl = addToCartUrl;
	}

}
