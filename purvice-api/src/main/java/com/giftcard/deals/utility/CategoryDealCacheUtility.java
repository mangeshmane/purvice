package com.giftcard.deals.utility;

import java.util.HashMap;

public class CategoryDealCacheUtility {

	private static HashMap<String, Long> cacheMap = new HashMap<>();;

	// PUT method
	public static void put(String key, Long value) {
		synchronized (cacheMap) {
			cacheMap.put(key, value);
		}
	}

	// GET method
	public static Long get(String key) {
		synchronized (cacheMap) {
			return cacheMap.get(key);
		}
	}
}
