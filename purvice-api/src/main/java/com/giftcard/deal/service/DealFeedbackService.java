package com.giftcard.deal.service;


import com.giftcard.model.Deal;
import com.giftcard.model.DealFeedback;
import com.giftcard.model.User;

import java.util.List;

public interface DealFeedbackService {

	public DealFeedback save (Deal deal, String feedbackType, long userId);
	
	public List<DealFeedback> findByUser (User user);
}
