package com.giftcard.deal.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.giftcard.model.Deal;
import com.giftcard.model.DealAlert;
import com.giftcard.model.User;
import com.giftcard.repository.DealAlertRepository;
import com.giftcard.service.UserService;

@Service
public class DealAlertServiceImpl implements DealAlertService {

	@Autowired
	@Qualifier("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Autowired
	DealAlertRepository dealAlertRepository;

	@Autowired
	public UserService userService;


	Map<String, String> dealAlertQueryMap;
	
	@Value( "${email.logo}" )
	private String EMAIL_LOGO;

	public DealAlertServiceImpl() {
		initializeSqls();
	}

	private void initializeSqls() {
		Map<String, String> sqlMap = new HashMap<String, String>();

		String dealAlertSQL = "select dealAlert.* from DealAlert dealAlert " + " WHERE {0} ";
		sqlMap.put("dealAlertSQL", dealAlertSQL);

		dealAlertQueryMap = Collections.unmodifiableMap(sqlMap);
	}

	@Override
	public List<DealAlert> saveDealAlertData(List<DealAlert> dealAlert) {
		Date date = new Date();
		List<DealAlert> DealAlertList= new ArrayList<DealAlert> ();
		for (DealAlert alert : dealAlert) {
			alert.setIsActive("0");
			alert.setIsDeleted("0");
			alert.setCreatedDate(date);
			alert.setEmailSendFlag("0");
			DealAlert newAlert = dealAlertRepository.save(alert);
			DealAlertList.add(newAlert);
		}
		return DealAlertList;
	}

	@Override
	public List<DealAlert> findByIsActive(String isActive,String frequency) {
		String select_Part = null;
		StringBuilder where_Part = new StringBuilder();
		StringBuilder queryBuilder = new StringBuilder();
		List<DealAlert> dealAlert = null;

		select_Part="SELECT * FROM DealAlert da "+
				" INNER JOIN Deal d ON da.category_id = d.category_id ";

		where_Part.append(" WHERE da.active = " + isActive );
		where_Part.append(" AND da.deleted = 0");
		where_Part.append(" AND da.frequencies = " + "'" + frequency + "'");
		where_Part.append(" OR ( da.frequencies = 'Daily,Instantly'");
		where_Part.append(" OR da.frequencies = 'Instantly,Daily' )");
		queryBuilder.append(select_Part);    
		queryBuilder.append(where_Part);

		try{
			dealAlert = jdbcTemplate.query(queryBuilder.toString(), dealAlertMapper);

		}catch(Exception e){

		}

		return dealAlert;
	}

	private static RowMapper<DealAlert> dealAlertMapper = new RowMapper<DealAlert>() {
		@Override
		public DealAlert mapRow(ResultSet rs, int rowNum) throws SQLException {
			DealAlert dealAlert = new DealAlert();
			//CreateDealObject(rs,dealAlert);
			dealAlert.setId(rs.getLong("id"));
			dealAlert.setAlertTitle(rs.getString("alert_title"));
			dealAlert.setName(rs.getString("name"));
			dealAlert.setNotificationMethod(rs.getString("notification_method"));
			dealAlert.setFrequencies(rs.getString("frequencies"));
			dealAlert.setRating(rs.getString("rating"));
			dealAlert.setShareURL(rs.getString("share_url"));
			dealAlert.setCreatedDate(rs.getDate("created_date"));
			dealAlert.setIsActive(rs.getString("active"));
			dealAlert.setIsDeleted(rs.getString("deleted"));
			dealAlert.setUserId(rs.getLong("user_id"));
			dealAlert.setUrl(rs.getString("url"));
			dealAlert.setImageUrl(rs.getString("image_url"));
			dealAlert.setKeyword(rs.getString("keyword"));
			dealAlert.setCategoryName(rs.getString("category_name"));
			dealAlert.setCategoryId(rs.getLong("category_id"));
			dealAlert.setCreatedBy(rs.getString("created"));
			dealAlert.setEmailSendFlag(rs.getString("email_send_flag"));
			dealAlert.setUserName(rs.getString("user_name"));
			return dealAlert;
		}
	};

	@Override
	public void save(DealAlert dealAlert) {
		dealAlertRepository.save(dealAlert);
	}

	@Override
	public DealAlert findById(Long dealAlertId) {
		return dealAlertRepository.findOne(dealAlertId);
	}

	@Override
	public DealAlert updateDealAlert(DealAlert dealAlert, DealAlert tempDealAlert) {
		dealAlert.setIsActive(tempDealAlert.getIsActive());
		dealAlert.setUrl(tempDealAlert.getUrl());
		dealAlert.setIsDeleted(tempDealAlert.getIsDeleted());
		dealAlert.setImageUrl(tempDealAlert.getImageUrl());
		dealAlert.setCreatedBy(tempDealAlert.getCreatedBy());
		dealAlert.setUserId(tempDealAlert.getUserId());
		dealAlert.setCreatedDate(new Date());
		dealAlert.setEmailSendFlag("0");
		dealAlertRepository.save(dealAlert);
		return dealAlert;
	}

	@Override
	public List<DealAlert> getAll() {
		return dealAlertRepository.findAll();
	}

	@Override
	public List<DealAlert> getUserDealAlert(Long userId) {
		List<DealAlert> dealAlert = null;
		StringBuilder where = new StringBuilder("");
		where.append(" dealAlert.user_id = " + userId + " and dealAlert.deleted = false order by id desc");
		String finalSQL = new String();
		finalSQL = dealAlertQueryMap.get("dealAlertSQL").replace("{0}", where);
		dealAlert = jdbcTemplate.query(finalSQL, dealAlertMapper);
		return dealAlert;
	}

	@Override
	public void sendDealAlert(DealAlert tempDealAlert, Deal deal) {
		User user = userService.findOne(tempDealAlert.getUserId());
		String emailID = user.getEmailId().toLowerCase();
		String toSubject = "Deal Alert";
		String[] str = deal.getText().split("<font color='red'><b>");
		String[] str1 = str[1].split("</b></font>&nbsp;");
		int var = 0;
		if(str1.length > 1){
			var = 1;
		}
		String toBody = "<!DOCTYPE html>"
				+ "<html>"
				+"<head>"
				+"<meta charset='UTF-8'>"
				+"<meta name='viewport' content='width=device-width, initial-scale=1'>"
				+"<title></title>"
				+"</head>"

				  +"<body style='background-color:#eee; padding:2rem 0'>"
				  +"<table style='margin-left: auto; margin-right: auto; background-color: #20a8d8; width: 650px; padding:10px 20px;'>"
				  +"<tr>"
				  +"<td colspan='2'>"
				  +"<img href='"+ EMAIL_LOGO +"' style='width:30%; height:auto;'>"
				  +"</td>"
				  +"</tr>"
				  +"</table>"
				  +"<br>"
				  +"<table style='margin-left: auto; margin-right: auto; color: #555559; background-color: white; width: 650px; border-top: 3px solid #ef6400; border-radius:3px'>"
				  +"<tr>"
				  +"<td>"
				  +"<table style='padding: 0 20px 20px 20px;'>"
				  +"<tr>"
				  +"<td>"
				  +"<h3>Hi,"+ user.getFirstName()+"</h3>"
				  +"<p>We found a deal that matched your alert.</p>"
				  +"</td>"    
				  +"<td style='text-align:right'>"
				  +"<h4 style='display:inline-block; background-color: #ef6400; color:#fff; padding: 20px 10px; text-align: center; position: relative; bottom: 27px;'>"
				  +"<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' style='fill:#fff'>"
				  +"<path d='M15.137 3.945c-.644-.374-1.042-1.07-1.041-1.82v-.003c.001-1.172-.938-2.122-2.096-2.122s-2.097.95-2.097 2.122v.003c.001.751-.396 1.446-1.041 1.82-4.667 2.712-1.985 11.715-6.862 13.306v1.749h20v-1.749c-4.877-1.591-2.195-10.594-6.863-13.306zm-3.137-2.945c.552 0 1 .449 1 1 0 .552-.448 1-1 1s-1-.448-1-1c0-.551.448-1 1-1zm3 20c0 1.598-1.392 3-2.971 3s-3.029-1.402-3.029-3h6zm5.778-11.679c.18.721.05 1.446-.304 2.035l.97.584c.504-.838.688-1.869.433-2.892-.255-1.024-.9-1.848-1.739-2.351l-.582.97c.589.355 1.043.934 1.222 1.654zm.396-4.346l-.597.995c1.023.616 1.812 1.623 2.125 2.874.311 1.251.085 2.51-.53 3.534l.994.598c.536-.892.835-1.926.835-3-.001-1.98-1.01-3.909-2.827-5.001zm-16.73 2.692l-.582-.97c-.839.504-1.484 1.327-1.739 2.351-.255 1.023-.071 2.053.433 2.892l.97-.584c-.354-.588-.484-1.314-.304-2.035.179-.72.633-1.299 1.222-1.654zm-4.444 2.308c0 1.074.299 2.108.835 3l.994-.598c-.615-1.024-.841-2.283-.53-3.534.312-1.251 1.101-2.258 2.124-2.873l-.597-.995c-1.817 1.092-2.826 3.021-2.826 5z' />"
				  +"</svg>"
				  +"<br> Deal"
				  +"<br> Alert"
				  +"</h4>"
				  +"</td>"
				  +"</tr>"
				  +"<tr>"
				  +"<td colspan='2'>"
				  +"<hr size='1' color='#eeeff0'>"
				  +"</td>"
				  +"</tr>"
				  +"<tr>"
				  +"<td style='width:50%; height: auto;'><img src='"+deal.getImageUrl()+"' style='width:100%; height: auto;'></td>"
				  +"<td>"
				  +"<p>HURRY, BEFORE YOUR DEAL EXPIRES!</p>"
				  +"<h4>" + str[0] + "</h4>"
				  + "<font color='red'><b>"+str1[0]+"</b></font>"
				  + "<br/>"
				  + str1[var]

						  +"<p style='font-size:21px;'></p>"

				                            +"<a style='color:#ffffff; background-color: #ef6400;  padding: 10px 20px; border-radius: 3px; text-decoration:none;' href='"+deal.getUrl()+"'>See Deal</a>"
				                            +"</td>"
				                            +"</tr>"
				                            +"</table>"
				                            +"</td>"
				                            +"</tr>"
				                            +"</table>"
				                            +"</body>"
				                            +"</html>";



		userService.sendEmail(emailID, toSubject, toBody);	
	}

	@Override
	public List<DealAlert> findByDealAlert(String isActive, String frequency) {
		String select_Part = null;
		StringBuilder where_Part = new StringBuilder();
		StringBuilder queryBuilder = new StringBuilder();
		List<DealAlert> dealAlert = null;

		select_Part="SELECT * FROM DealAlert da WHERE";

		//where_Part.append(" da.email_send_flag = '0'");
		where_Part.append(" da.active = " + isActive );
		where_Part.append(" AND da.deleted = 0");
		where_Part.append(" AND da.frequencies = " + "'" + frequency + "'");
		where_Part.append(" OR ( da.frequencies = 'Daily,Instantly'");
		where_Part.append(" OR da.frequencies = 'Instantly,Daily' )");
		queryBuilder.append(select_Part);    
		queryBuilder.append(where_Part);

		try{
			dealAlert = jdbcTemplate.query(queryBuilder.toString(), dealAlertMapper);

		}catch(Exception e){

		}

		return dealAlert;
	}

	@Override
	public void setParameterAfterMail(DealAlert tempDealAlert) {
		tempDealAlert.setEmailSendFlag(new String("1"));
		dealAlertRepository.save(tempDealAlert);
	}

	@Override
	public List<DealAlert> findEmailFlagDealAlert() {
		List<DealAlert> dealAlert = null;
		StringBuilder where = new StringBuilder("");
		where.append(" dealAlert.email_send_flag = '1'");
		String finalSQL = new String();
		finalSQL = dealAlertQueryMap.get("dealAlertSQL").replace("{0}", where);
		dealAlert = jdbcTemplate.query(finalSQL, dealAlertMapper);
		return dealAlert;
	}

	@Override
	public void sendDealAlertByDeals(DealAlert tempDealAlert, List<Deal> deals) {
		User user = userService.findOne(tempDealAlert.getUserId());
		String emailID = user.getEmailId().toLowerCase();
		String toSubject = "Deal Alert";
		StringBuilder toBody = new StringBuilder();

		String head = "<!DOCTYPE html>"
				+ "<html>"
				+"<head>"
				+"<meta charset='UTF-8'>"
				+"<meta name='viewport' content='width=device-width, initial-scale=1'>"
				+"<title></title>"
				+"</head>"
				+ "<body style='background-color:#eee; padding:2rem 0'>"
				+"<table style='margin-left: auto; margin-right: auto; background-color: #20a8d8; width: 650px; padding:10px 20px;'>"
				+"<tr>"
				+"<td colspan='2'>"
				+"<img href='"+ EMAIL_LOGO +"' style='width:30%; height:auto;'>"
				+"</td>"
				+"</tr>"
				+"</table>"
				+"<br>"

		+"<table style='margin-left: auto; margin-right: auto; color: #555559; background-color: white; width: 650px; border-top: 3px solid #ef6400; border-radius:3px'>"
		+"<tr>"
		+"<td>"
		+"<table style='padding: 0 20px 20px 20px;'>"
		+"<tr>"
		+"<td>"
		+"<h3>Hi,"+ user.getFirstName()+"</h3>"
		+"<p>We found a deal that matched your alert.</p>"
		+"</td>"    
		+"<td style='text-align:right'>"
		+"<h4 style='display:inline-block; background-color: #ef6400; color:#fff; padding: 20px 10px; text-align: center; position: relative; bottom: 27px;'>"
		+"<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' style='fill:#fff'>"
		+"<path d='M15.137 3.945c-.644-.374-1.042-1.07-1.041-1.82v-.003c.001-1.172-.938-2.122-2.096-2.122s-2.097.95-2.097 2.122v.003c.001.751-.396 1.446-1.041 1.82-4.667 2.712-1.985 11.715-6.862 13.306v1.749h20v-1.749c-4.877-1.591-2.195-10.594-6.863-13.306zm-3.137-2.945c.552 0 1 .449 1 1 0 .552-.448 1-1 1s-1-.448-1-1c0-.551.448-1 1-1zm3 20c0 1.598-1.392 3-2.971 3s-3.029-1.402-3.029-3h6zm5.778-11.679c.18.721.05 1.446-.304 2.035l.97.584c.504-.838.688-1.869.433-2.892-.255-1.024-.9-1.848-1.739-2.351l-.582.97c.589.355 1.043.934 1.222 1.654zm.396-4.346l-.597.995c1.023.616 1.812 1.623 2.125 2.874.311 1.251.085 2.51-.53 3.534l.994.598c.536-.892.835-1.926.835-3-.001-1.98-1.01-3.909-2.827-5.001zm-16.73 2.692l-.582-.97c-.839.504-1.484 1.327-1.739 2.351-.255 1.023-.071 2.053.433 2.892l.97-.584c-.354-.588-.484-1.314-.304-2.035.179-.72.633-1.299 1.222-1.654zm-4.444 2.308c0 1.074.299 2.108.835 3l.994-.598c-.615-1.024-.841-2.283-.53-3.534.312-1.251 1.101-2.258 2.124-2.873l-.597-.995c-1.817 1.092-2.826 3.021-2.826 5z' />"
		+"</svg>"
		+"<br> Deal"
		+"<br> Alert"
		+"</h4>"
		+"</td>"
		+"</tr>";
		toBody.append(head);

		for (Deal deal : deals) {

			String[] str = deal.getText().split("<font color='red'><b>");
			String[] str1 = str[1].split("</b></font>&nbsp;");
			int var = 0;
			if(str1.length > 1){
				var = 1;
			}
			String body = "<tr style='padding-bottom: 20px'>"
					+"<td colspan='2'>"
					+"<hr size='1' color='#ef6400'>"
					+"</td>"
					+"</tr>"
					+"<tr style='padding-bottom: 20px'>"
					+"<td style='width:50%; height: auto;'><img src='"+deal.getImageUrl()+"' style='width:100%; height: auto;'></td>"
					+"<td>"
					+"<p>HURRY, BEFORE YOUR DEAL EXPIRES!</p>"
					+"<h4>" + str[0] + "</h4>"
					+ "<font color='red'><b>"+str1[0]+"</b></font>"
					+ "<br/>"
					+ str1[var]

							+"<p style='font-size:21px;'></p>"

					                            +"<a style='color:#ffffff; background-color: #ef6400;  padding: 10px 20px; border-radius: 3px; text-decoration:none;' href='"+deal.getUrl()+"'>See Deal</a>"
					                            +"</td>"
					                            +"</tr>";


			toBody.append(body);
		}

		String footer = "</body>"
				+"</html>"
				+"</table>"
				+"</td>"
				+"</tr>"
				+"</table>";
		toBody.append(footer);
		userService.sendEmail(emailID, toSubject, toBody.toString());	

	}

	@Override
	public DealAlert saveDealAlertDataByShared(DealAlert dealAlert) {
		DealAlert newAlert = new DealAlert();
		List<DealAlert> tempDealAlert = findDealAlert(dealAlert);
		if(tempDealAlert.isEmpty()){
			newAlert = dealAlertRepository.save(dealAlert);
		}

		return newAlert;
	}

	private List<DealAlert> findDealAlert(DealAlert dealAlert) {
		List<DealAlert> dealAlertData = null;
		StringBuilder where = new StringBuilder("");
		where.append(" dealAlert.user_id = " + dealAlert.getUserId() + " and dealAlert.alert_title = " + "'"+ dealAlert.getAlertTitle() +"'");
		String finalSQL = new String();
		finalSQL = dealAlertQueryMap.get("dealAlertSQL").replace("{0}", where);
		dealAlertData = jdbcTemplate.query(finalSQL, dealAlertMapper);
		return dealAlertData;
	}

}
