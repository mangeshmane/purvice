package com.giftcard.deal.service;

import com.giftcard.model.DealCategory;
import com.giftcard.tree.TreeModel;
import com.giftcard.tree.TreeNode;

public class DealCategoryNode extends TreeNode<DealCategory> {

	public DealCategoryNode(TreeModel<?> model, DealCategory contents) {
		super(model, contents);
	}
}