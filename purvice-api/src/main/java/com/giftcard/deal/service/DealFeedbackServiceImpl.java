package com.giftcard.deal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giftcard.model.Deal;
import com.giftcard.model.DealFeedback;
import com.giftcard.model.User;
import com.giftcard.repository.DealFeedbackRepository;

@Service
public class DealFeedbackServiceImpl implements DealFeedbackService {

	@Autowired
	private DealService dealService;

	@Autowired
	public DealFeedbackRepository dealFeedbackRepository;

	@Override
	public DealFeedback save(Deal deal, String feedbackType, long userId) {

		DealFeedback dealFeedback = new DealFeedback();
		User user =  new User();

		user.setId(userId);
		dealFeedback.setUser(user);
		if(deal.getId() == null){
			deal = dealService.findByUniqueId(deal.getUniqueId()); 
		}
		dealFeedback.setDeal(deal);
		DealFeedback dbDealFeedback = dealFeedbackRepository.findByDealAndUser(deal, user);
		if(dbDealFeedback!= null){
			if(feedbackType.equals("like")){
				dbDealFeedback.setLikes(true);
				dbDealFeedback.setDisLikes(false);
			}
			if(feedbackType.equals("disLike")){
				dbDealFeedback.setDisLikes(true);
				dbDealFeedback.setLikes(false);
			}
			return dealFeedbackRepository.save(dbDealFeedback);
		}else{
			if(feedbackType.equals("like")){
				dealFeedback.setLikes(true);
			}
			if(feedbackType.equals("disLike")){
				dealFeedback.setDisLikes(true);
			}
			return dealFeedbackRepository.save(dealFeedback);
		}


	}

	@Override
	public List<DealFeedback> findByUser(User user) {
		return dealFeedbackRepository.findByUser(user);
	}

}
