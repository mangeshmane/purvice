package com.giftcard.deal.service;

import java.util.List;

import com.giftcard.model.Deal;
import com.giftcard.model.DealAlert;
import com.giftcard.model.DealEmailRelation;

public interface DealEmailRelationService {
	
	public DealEmailRelation save(DealEmailRelation dealEmailRelation);

	public void setParameterAfterMailByDeal(List<Deal> deals, DealAlert tempDealAlert);

	public List<DealEmailRelation> findAll();

	public List<DealEmailRelation> findByUserId(Long userId);

	public List<DealEmailRelation> findByUserIdAndDaily(Long userId);

	public void removeAllExpireRelation(List<Deal> deal);

}
