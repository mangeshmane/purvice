package com.giftcard.deal.service;

import java.util.List;

import com.giftcard.model.Banner;

public interface BannerService {

	public Banner save(Banner bannerData);

	public List<Banner> getAll();

	public List<Banner> fetchByEnable();

	public void deleteById(Long bannerId);

}
