package com.giftcard.deal.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.giftcard.model.Deal;
import com.giftcard.model.DealAlert;
import com.giftcard.model.DealEmailRelation;
import com.giftcard.repository.DealEmailRelationRepository;

@Service
public class DealEmailRelationServiceImpl implements DealEmailRelationService {

	@Autowired
	@Qualifier("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Autowired
	DealEmailRelationRepository dealEmailRelationRepository;

	Map<String, String> dealEmailQueryMap;

	public DealEmailRelationServiceImpl() {
		initializeSqls();
	}

	private void initializeSqls() {
		Map<String, String> sqlMap = new HashMap<String, String>();

		String dealEmailRelationSQL = "SELECT dealEmail.* from DealEmailRelation dealEmail " + " WHERE {0} ";
		sqlMap.put("dealEmailRelationSQL", dealEmailRelationSQL);

		dealEmailQueryMap = Collections.unmodifiableMap(sqlMap);
	}

	@Override
	public DealEmailRelation save(DealEmailRelation dealEmailRelation) {
		return dealEmailRelationRepository.save(dealEmailRelation);
	}

	@Override
	public void setParameterAfterMailByDeal(List<Deal> deals, DealAlert tempDealAlert) {
		for (Deal deal : deals) {
			DealEmailRelation dealEmailRelation = new DealEmailRelation();
			dealEmailRelation.setDealUniqueId(deal.getUniqueId());
			if(tempDealAlert.getFrequencies().equalsIgnoreCase("Instantly"))
				dealEmailRelation.setDaily(new String("1"));
			else
				dealEmailRelation.setDaily(new String("0"));
			dealEmailRelation.setEmailSendFlag(new String("1"));
			dealEmailRelation.setUserId(tempDealAlert.getUserId());
			dealEmailRelationRepository.save(dealEmailRelation);
		}
	}

	@Override
	public List<DealEmailRelation> findAll() {
		// TODO Auto-generated method stub
		return dealEmailRelationRepository.findAll();
	}

	@Override
	public List<DealEmailRelation> findByUserId(Long userId) {
		List<DealEmailRelation> dealEmailRelation = null;
		StringBuilder where = new StringBuilder("");
		where.append(" dealEmail.user_id = " + userId );
		String finalSQL = new String();
		finalSQL = dealEmailQueryMap.get("dealEmailRelationSQL").replace("{0}", where);
		dealEmailRelation = jdbcTemplate.query(finalSQL, dealEmailRelationMapper);
		return dealEmailRelation;
	}

	private static RowMapper<DealEmailRelation> dealEmailRelationMapper = new RowMapper<DealEmailRelation>() {
		@Override
		public DealEmailRelation mapRow(ResultSet rs, int rowNum) throws SQLException {
			DealEmailRelation dealEmailRelation = new DealEmailRelation();
			dealEmailRelation.setId(rs.getLong("id"));
			dealEmailRelation.setDaily(rs.getString("daily"));
			dealEmailRelation.setDealUniqueId(rs.getString("deal_unique_id"));
			dealEmailRelation.setEmailSendFlag(rs.getString("email_send_flag"));
			dealEmailRelation.setUserId(rs.getLong("user_id"));
			return dealEmailRelation;
		}
	};

	@Override
	public List<DealEmailRelation> findByUserIdAndDaily(Long userId) {
		List<DealEmailRelation> dealEmailRelation = null;
		StringBuilder where = new StringBuilder("");
		where.append(" dealEmail.user_id = " + userId )
		.append(" AND daily = '0'");
		String finalSQL = new String();
		finalSQL = dealEmailQueryMap.get("dealEmailRelationSQL").replace("{0}", where);
		dealEmailRelation = jdbcTemplate.query(finalSQL, dealEmailRelationMapper);
		return dealEmailRelation;
	}

	@Override
	public void removeAllExpireRelation(List<Deal> deal) {
		List<String> dealUniqueId = new ArrayList<String>();
		String dealUniqueIds = new String();
		String select_Part = null;
		StringBuilder queryBuilder = new StringBuilder();
		
		for (Deal tempDeal : deal) {
			dealUniqueId.add(tempDeal.getUniqueId());
		}
		
		dealUniqueIds = dealUniqueId.toString().replace("[", "").replace("]", "");
		
		  StringBuilder uniqueIds = new StringBuilder();
		  String[] strData = dealUniqueIds.split(",");
		  for (String str : strData) {
			  uniqueIds.append("'"+str+"'"+",");
		   }
		  uniqueIds.deleteCharAt(uniqueIds.length() - 1);
		
		select_Part=" DELETE FROM Dealemailrelation WHERE deal_unique_id IN("+uniqueIds+")";
		queryBuilder.append(select_Part);
		jdbcTemplate.update(select_Part);
	}
}
