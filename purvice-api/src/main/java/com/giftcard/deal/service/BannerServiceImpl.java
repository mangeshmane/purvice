package com.giftcard.deal.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.giftcard.model.Banner;
import com.giftcard.model.DealAlert;
import com.giftcard.repository.BannerRepository;

@Service
public class BannerServiceImpl implements BannerService{
	
	@Autowired
	@Qualifier("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Autowired
	BannerRepository bannerRepository;
	
	Map<String, String> bannerQueryMap;
	
	public BannerServiceImpl() {
		initializeSqls();
	}

	private void initializeSqls() {
		Map<String, String> sqlMap = new HashMap<String, String>();

		String bannerSQL = "select banner.* from Banner banner " + " WHERE {0} ";
		sqlMap.put("bannerSQL", bannerSQL);

		bannerQueryMap = Collections.unmodifiableMap(sqlMap);
	}

	private static RowMapper<Banner> bannerMapper = new RowMapper<Banner>() {
		@Override
		public Banner mapRow(ResultSet rs, int rowNum) throws SQLException {
			Banner banner = new Banner();
			banner.setId(rs.getLong("id"));
			banner.setImageName(rs.getString("image_name"));
			banner.setIsEnable(rs.getString("enable"));
			banner.setType(rs.getString("type"));
			banner.setImageUrl(rs.getString("image_url"));
			return banner;
		}
	};


	@Override
	public Banner save(Banner bannerData) {
		Banner banner = bannerRepository.save(bannerData);
		return banner;
	}


	@Override
	public List<Banner> getAll() {
		return bannerRepository.findAll();
	}


	@Override
	public List<Banner> fetchByEnable() {
		List<Banner> banner = new ArrayList<Banner>();
		StringBuilder where = new StringBuilder("");
		where.append(" banner.enable = 1");
		String finalSQL = new String();
		finalSQL = bannerQueryMap.get("bannerSQL").replace("{0}", where);
		banner = jdbcTemplate.query(finalSQL, bannerMapper);
		return banner;
	}

	@Override
	public void deleteById(Long bannerId) {
		bannerRepository.delete(bannerId);
	}
}
