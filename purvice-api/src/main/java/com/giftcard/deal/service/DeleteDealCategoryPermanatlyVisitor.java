package com.giftcard.deal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.giftcard.model.DealCategory;
import com.giftcard.repository.DealCategoryRepository;
import com.giftcard.tree.Visitor;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DeleteDealCategoryPermanatlyVisitor implements Visitor<DealCategoryNode> {

	@Autowired
	DealCategoryRepository dealCategoryRepository;

	@Override
	public void visit(DealCategoryNode dealCategoryNode) {
		DealCategory dealCategory = dealCategoryNode.getContents();
		dealCategoryRepository.delete(dealCategory);
	}

	@Override
	public void startVisit(DealCategoryNode node) {
		// TODO Auto-generated method stub

	}

	@Override
	public void endVisit(DealCategoryNode node) {
		// TODO Auto-generated method stub

	}

}
