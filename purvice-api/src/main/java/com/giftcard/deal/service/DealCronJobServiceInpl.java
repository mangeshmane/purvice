package com.giftcard.deal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.giftcard.model.DealCronJob;
import com.giftcard.repository.DealCronJobRepository;

@Service
public class DealCronJobServiceInpl implements DealCronJobService {

	@Autowired
	@Qualifier("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Autowired
	DealCronJobRepository dealCronJobRepository;

	public DealCronJobServiceInpl() {
		//initializeSqls();
	}

	@Override
	public DealCronJob save(DealCronJob dealCronJob) {
		return dealCronJobRepository.save(dealCronJob);
	}

}
