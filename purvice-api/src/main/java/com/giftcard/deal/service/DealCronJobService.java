package com.giftcard.deal.service;

import com.giftcard.model.DealCronJob;

public interface DealCronJobService {

	DealCronJob save(DealCronJob dealCronJob);

}
