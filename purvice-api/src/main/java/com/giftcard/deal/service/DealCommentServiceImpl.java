package com.giftcard.deal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.giftcard.model.DealComment;
import com.giftcard.repository.DealCommentRepository;
import com.giftcard.service.UserService;

@Service
public class DealCommentServiceImpl implements DealCommentService{

	@Autowired
	@Qualifier("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Autowired
	DealCommentRepository dealCommentRepository;

	@Autowired
	public UserService userService;

	@Override
	public DealComment saveDealFeedbachByUser(DealComment dealComment) {
		userService.sendFeedbackEmail(dealComment.getEmail(), "Feedback", dealComment.getName()+" "+dealComment.getComment());
		userService.sendEmail(dealComment.getEmail(), "Feedback", "Thanks for sharing your valuable feedback.");
		return dealCommentRepository.save(dealComment);
	}
}
