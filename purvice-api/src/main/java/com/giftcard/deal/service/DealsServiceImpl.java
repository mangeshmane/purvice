package com.giftcard.deal.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.giftcard.controller.DealController;
import com.giftcard.controller.OrdersController;
import com.giftcard.deals.model.SDModel;
import com.giftcard.filter.specification.DealFilterSpecification;
import com.giftcard.model.Deal;
import com.giftcard.model.DealAlert;
import com.giftcard.model.DealCategory;
import com.giftcard.model.DealCronJob;
import com.giftcard.model.DealFeedback;
import com.giftcard.model.User;
import com.giftcard.page.PageRequestBuilder;
import com.giftcard.repository.DealCategoryRepository;
import com.giftcard.repository.DealFeedbackRepository;
import com.giftcard.repository.DealRepository;
import com.giftcard.response.ResponseWrapper;

@Service
@Component("dealserviceimpl")
public class DealsServiceImpl implements DealService {

	@Autowired
	@Qualifier("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Autowired
	DealRepository dealRepository;

	@Autowired
	public DealCronJobService dealCronJobService;

	@Autowired
	public DealAlertService dealAlertService;

	@Autowired
	DealCategoryRepository dealCategoryRepository;

	@Autowired
	private  DealFilterSpecification dealFilterSpecification;

	@Autowired
	private  DealFeedbackRepository dealFeedbackRepository;

	@Autowired
	private DealController dealController;
	
	@Autowired
	private DealEmailRelationService dealEmailRelationService;

	Map<String, String> dealQueryMap;

	public DealsServiceImpl() {
		initializeSqls();
	}

	private List<DealCategory> dealCategoryList;

	private void initializeSqls() {
		Map<String, String> sqlMap = new HashMap<String, String>();

		String dealSQL = "select deal.*  from Deal deal " + " WHERE {0} ";
		sqlMap.put("dealSQL", dealSQL);

		String deal_count_SQL = "select count(*) from Deal deal " + " WHERE {0} ";
		sqlMap.put("deal_count_SQL", deal_count_SQL);

		String dealEmailSQL = "SELECT deal.*  from Deal deal " 
				+" INNER JOIN DealEmailRelation dealEmail "
				+" ON deal.uniqueId != dealEmail.deal_unique_id"
				+ " WHERE {0} ";
		sqlMap.put("dealEmailSQL", dealEmailSQL);

		String deal_Category_sql = "select deal.*,dealCategory.category_name  from Deal deal "
				+ " LEFT JOIN DealCategory dealCategory "
				+ " ON deal.category_id = dealCategory.id " + " WHERE {0} ";

		String deal_Category_sql_count = "select count(*) from Deal deal "
				+ " LEFT JOIN DealCategory dealCategory "
				+ " ON deal.category_id = dealCategory.id " + " WHERE {0} ";

		String deal_dealfeedback_sql = "select deal.* from Deal deal "
				+ " WHERE {0} ";

		String deal_dealfeedback_count_sql = "select count(*) from Deal deal "
				+ " WHERE {0} ";

		sqlMap.put("deal_Category_sql", deal_Category_sql);
		sqlMap.put("deal_Category_sql_count", deal_Category_sql_count);
		sqlMap.put("deal_dealfeedback_sql", deal_dealfeedback_sql);
		sqlMap.put("deal_dealfeedback_count_sql", deal_dealfeedback_count_sql);
		dealQueryMap = Collections.unmodifiableMap(sqlMap);
	}


	private static final DealsServiceImpl iInstance = new DealsServiceImpl();

	public static DealsServiceImpl getInstance() {
		return iInstance;
	}

	@Override
	public void saveDealsData(List<Deal> deals) {

		List<Deal> dbDeals = dealRepository.findAll();
		// Create cache of deals from database 
		Map<String, Deal> dealsInDatabase = new HashMap<String, Deal>();
		/*		for (Deal deal : dbDeals) {
			dealsInDatabase.put(deal.getUniqueId(), deal);
		}*/
		this.compareDealExpire(deals, dbDeals, dealsInDatabase);
		for (Deal deal : deals) {
			boolean isNotExist = dealsInDatabase.containsKey(deal.getUniqueId());
			if (!isNotExist) {
				deal.setIsExpire(new String("0"));
				dealRepository.save(deal);
				dealsInDatabase.put(deal.getUniqueId(), deal);
			} else {
				System.out.println("Deal Already Exist" + deal.toString());
			}
		}
	}

	private void compareDealExpire(List<Deal> scrapDeals, List<Deal> dbDeals, Map<String, Deal> dealsInDatabase) {
		boolean setFlag = false;
		if(!dbDeals.isEmpty()){
			for (Deal dbDeal : dbDeals) {
				for (Deal scrapDeal : scrapDeals) {
					if (dbDeal.getUniqueId().equals(scrapDeal.getUniqueId())){
						dbDeal.setIsExpire(new String("0"));
						setFlag = true;
						break;
					}
				}
				if(!setFlag)
					dbDeal.setIsExpire(new String("1"));
				//dealRepository.save(dbDeal);
				setFlag = false;
				dealsInDatabase.put(dbDeal.getUniqueId(), dbDeal);
			}
			dealRepository.save(dbDeals);
		}
	}

	private boolean findByDealUniqueId(String uniqueId) {
		List<Deal> deal = null;
		StringBuilder where = new StringBuilder("");
		where.append(" deal.uniqueId = " + "'" + uniqueId + "'" ).append("AND ")
		.append(" deal.isExpire = '0'");
		String finalSQL = new String();
		finalSQL = dealQueryMap.get("dealSQL").replace("{0}", where);
		deal = jdbcTemplate.query(finalSQL, dealMapper);
		return deal.isEmpty();
	}

	@Override
	public List<Deal> getAllDeal() {
		return dealRepository.findAll();

	}

	@Override
	public List<Deal> getDealUsingKeyword(String searchKey) {
		return dealRepository.getDealUsingKeyword(searchKey);

	}

	private static RowMapper<Deal> dealMapper = new RowMapper<Deal>() {
		@Override
		public Deal mapRow(ResultSet rs, int rowNum) throws SQLException {
			Deal deal = new Deal();
			deal.setId(rs.getLong("id"));
			deal.setUniqueId(rs.getString("uniqueId"));
			deal.setUrl(rs.getString("url"));
			deal.setText(rs.getString("text"));
			deal.setImageUrl(rs.getString("imageUrl"));
			deal.setCategory(rs.getString("category"));
			deal.setSource(rs.getString("source"));
			deal.setRandom(rs.getInt("random"));
			deal.setOriginalPrice(rs.getDouble("originalPrice"));
			deal.setSalePrice(rs.getDouble("salePrice"));
			deal.setFirstSeen(rs.getDate("firstSeen"));
			deal.setSdDeal(rs.getBoolean("sdDeal"));
			deal.setAffliateLink(rs.getString("affliateLink"));
			deal.setCustomerReviewAverage(rs.getString("customerReviewAverage"));
			deal.setCustomerReviewCount(rs.getString("customerReviewCount"));
			deal.setOfferType(rs.getString("offerType"));
			deal.setCategoryId(rs.getLong("category_id"));
			deal.setIsExpire(rs.getString("isExpire"));
			deal.setLikes(rs.getLong("likes"));
			deal.setDislikes(rs.getLong("dislikes"));
			deal.setAddToCartUrl(rs.getString("addToCartUrl"));
			return deal;
		}
	};
	
	private static RowMapper<Deal> dealSearchMapper = new RowMapper<Deal>() {
		@Override
		public Deal mapRow(ResultSet rs, int rowNum) throws SQLException {
			Deal deal = new Deal();
			deal.setId(rs.getLong("id"));
			deal.setUniqueId(rs.getString("uniqueId"));
			deal.setUrl(rs.getString("url"));
			deal.setText(rs.getString("text"));
			deal.setImageUrl(rs.getString("imageUrl"));
			deal.setCategory(rs.getString("category"));
			deal.setSource(rs.getString("source"));
			deal.setRandom(rs.getInt("random"));
			deal.setOriginalPrice(rs.getDouble("originalPrice"));
			deal.setSalePrice(rs.getDouble("salePrice"));
			deal.setFirstSeen(rs.getDate("firstSeen"));
			deal.setSdDeal(rs.getBoolean("sdDeal"));
			deal.setAffliateLink(rs.getString("affliateLink"));
			deal.setCustomerReviewAverage(rs.getString("customerReviewAverage"));
			deal.setCustomerReviewCount(rs.getString("customerReviewCount"));
			deal.setOfferType(rs.getString("offerType"));
			deal.setCategoryId(rs.getLong("category_id"));
			deal.setIsExpire(rs.getString("isExpire"));
			deal.setLikes(rs.getLong("likes"));
			deal.setDislikes(rs.getLong("dislikes"));
			deal.setLocation(rs.getInt("location"));
			deal.setAddToCartUrl(rs.getString("addToCartUrl"));
			return deal;
		}
	};

	@Override
	public List<Deal> findByCategoryId(DealAlert tempDealAlert) {
		List<Deal> deal = null;
		StringBuilder where = new StringBuilder("");

		if(tempDealAlert.getCategoryId() == 0 || tempDealAlert.getCategoryId() == null){
			where.append(" deal.text like  "+"'%"+tempDealAlert.getCategoryName()+"%'"  + " AND deal.customerReviewAverage >= " + tempDealAlert.getRating())
			.append(" AND deal.isExpire = '0'");
		}else{
			where.append(" deal.category_id = " + tempDealAlert.getCategoryId() + " AND deal.customerReviewAverage >= " + tempDealAlert.getRating())
			.append(" AND deal.isExpire = '0'");
		}
		String finalSQL = new String();
		finalSQL = dealQueryMap.get("dealSQL").replace("{0}", where);
		deal = jdbcTemplate.query(finalSQL, dealMapper);
		return deal;
	}

	@Override
	public List<Deal> getDealByIds(List<Long> dealIds) {
		String ids = dealIds.toString();
		ids = dealIds.toString().replace("[", "").replace("]", "");
		List<Deal> deal = null;
		StringBuilder where = new StringBuilder("");
		where.append(" deal.id IN (" + ids + ")" )
		.append(" AND deal.isExpire = '0'");
		String finalSQL = new String();
		finalSQL = dealQueryMap.get("dealSQL").replace("{0}", where);
		deal = jdbcTemplate.query(finalSQL, dealMapper);
		return deal;
	}

	@Override
	public ResponseWrapper<List<Deal>> getDealWithPaginationAndFilter(HttpServletRequest request) {

		Specification<Deal> specs = Specifications
				.where(dealFilterSpecification.getStringTypeSpecification("text", request.getParameter("frmSearchParam")))
				.and(dealFilterSpecification.getStringTypeSpecification("customerReviewAverage", request.getParameter("customerReviewAverage")))
				.and(dealFilterSpecification.getStringTypeSpecification("source", request.getParameter("source")))
				.and(dealFilterSpecification.getLongTypeSpecification("id",  request.getParameter("id")))
				.and(dealFilterSpecification.getLongTypeSpecification("categoryId",  request.getParameter("categoryId")))
				.and(dealFilterSpecification.getLongTypeSpecification("categoryId",  request.getParameter("dealCategoryId")))
				.and(dealFilterSpecification.getStringTypeSpecification("isExpire", "eq:0"))
				.and(dealFilterSpecification.getStringTypeSpecification("source", "neq:www.rakuten.com"));
				new Sort(Sort.Direction.DESC, "firstSeen");
				
		ResponseWrapper<List<Deal>> pagingResponse = new ResponseWrapper<>();

		// This represents the Page config with sorting
		PageRequest pageRequest = PageRequestBuilder.getPageRequest(request);

		// Get Page info from CountryRepository
		Page<Deal> contriesPage = dealRepository.findAll(specs, pageRequest);

		pagingResponse.setData(contriesPage.getContent());

		// Set the flag to indicate next page exists
		pagingResponse.setHasNextPage(contriesPage.hasNext());

		// Set the flag to indicate previous page exists
		pagingResponse.setHasPreviousPage(contriesPage.hasPrevious());

		// Set the total number of records for the given Filter Specification
		pagingResponse.setTotalNumberOfRecords(contriesPage.getTotalElements());

		// Set the total number of pages for the given filter specification and
		// pagerequests
		pagingResponse.setTotalNumberOfPages(contriesPage.getTotalPages());

		// Page numbers are indexed from 0 but to the consume we follow start
		// index as 1
		pagingResponse.setPageNumber(pageRequest.getPageNumber() + 1);

		// Number of records per page
		pagingResponse.setPageSize(pageRequest.getPageSize());

		return pagingResponse;
	}

	@Override
	public ResponseWrapper<List<Deal>> getDealWithPaginationAndFilterPost(HttpServletRequest request, Deal deal) {

		Specification<Deal> specs = Specifications
				.where(dealFilterSpecification.getStringTypeSpecification("text", request.getParameter("frmSearchParam")))
				.and(dealFilterSpecification.getStringTypeSpecification("customerReviewAverage", request.getParameter("customerReviewAverage")))
				.and(dealFilterSpecification.getStringTypeSpecification("source", request.getParameter("source")))
				.and(dealFilterSpecification.getLongTypeSpecification("id",  request.getParameter("id")))
				.and(dealFilterSpecification.getLongTypeSpecification("categoryId",  deal.getCategory()))
				.and(dealFilterSpecification.getStringTypeSpecification("isExpire", "eq:0"))
				.and(dealFilterSpecification.getStringTypeSpecification("isExpire", "neq:www.rakuten.com"));
				new Sort(Sort.Direction.DESC, "firstSeen");

		ResponseWrapper<List<Deal>> pagingResponse = new ResponseWrapper<>();

		// This represents the Page config with sorting
		PageRequest pageRequest = PageRequestBuilder.getPageRequest(request);

		// Get Page info from CountryRepository
		Page<Deal> contriesPage = dealRepository.findAll(specs, pageRequest);

		pagingResponse.setData(contriesPage.getContent());

		// Set the flag to indicate next page exists
		pagingResponse.setHasNextPage(contriesPage.hasNext());

		// Set the flag to indicate previous page exists
		pagingResponse.setHasPreviousPage(contriesPage.hasPrevious());

		// Set the total number of records for the given Filter Specification
		pagingResponse.setTotalNumberOfRecords(contriesPage.getTotalElements());

		// Set the total number of pages for the given filter specification and
		// pagerequests
		pagingResponse.setTotalNumberOfPages(contriesPage.getTotalPages());

		// Page numbers are indexed from 0 but to the consume we follow start
		// index as 1
		pagingResponse.setPageNumber(pageRequest.getPageNumber() + 1);

		// Number of records per page
		pagingResponse.setPageSize(pageRequest.getPageSize());

		return pagingResponse;
	}
	private void getCategoryIdsRecursively(Long id,StringBuilder categoryIds){

		for (DealCategory dealCategory : dealCategoryList) {
			if(dealCategory.getParentId() == id){
				if(categoryIds.length() > 0){
					categoryIds.append(",");
				}
				categoryIds.append(String.valueOf(dealCategory.getId()));
				getCategoryIdsRecursively(dealCategory.getId(),categoryIds);
			}
		}
	}

	@Override
	public void removeAllExpireDeal() {
		String select_Part = null;
		StringBuilder queryBuilder = new StringBuilder();
		List<Long> dealId = new ArrayList<Long>();
		String dealIdsData;
		List<Deal> deal = null;
		StringBuilder where = new StringBuilder("");    
		where.append(" deal.firstSeen <= NOW() - INTERVAL 3 DAY");  
		String finalSQL = new String();
		finalSQL = dealQueryMap.get("dealSQL").replace("{0}", where);
		deal = jdbcTemplate.query(finalSQL, dealMapper);

		for (Deal dealData : deal) {
			dealId.add(dealData.getId());
		}
		if(!dealId.isEmpty()){
			dealIdsData = dealId.toString().replace("[", "").replace("]", "");
			select_Part=" DELETE FROM DealFeedback WHERE deal IN("+dealIdsData+")";
			queryBuilder.append(select_Part);
			jdbcTemplate.update(select_Part);
		}
		
		dealEmailRelationService.removeAllExpireRelation(deal);
		
		select_Part=" DELETE FROM Deal WHERE "          
				+" firstSeen <= NOW() - INTERVAL 3 DAY";
		queryBuilder.append(select_Part);
		jdbcTemplate.update(select_Part);
	}

	@Override
	public List<SDModel> findKeyWordInDeal(String keyword) {
		List<SDModel> deal = new ArrayList<>();
		StringBuilder where = new StringBuilder("");
		where.append(" deal.url like " + "'%" + keyword + "%'" ).append(" OR ")
		.append(" deal.text like " + "'%" + keyword + "%' ").append(" OR ")
		.append(" dealCategory.category_name like " + "'%" + keyword + "%' ").append(" AND ")
		.append(" deal.isExpire = '0'");
		String finalSQL = new String();
		finalSQL = dealQueryMap.get("deal_Category_sql").replace("{0}", where);
		deal = jdbcTemplate.query(finalSQL, sdModelMapper);
		return deal;
	}

	private void createWhereClause(HttpServletRequest request, Map<String, Object> params, StringBuilder where,
			StringBuilder sql) {
		String keyword = request.getParameter("frmSearchParam").replaceAll("\'","");
		if (keyword != null) {
			if (request.getParameter("source") != null) {
				where.append(" ( deal.url like " + "'%" + keyword + "%'" ).append(" OR ")
				.append(" deal.text like " + "'%" + keyword + "%' ").append(" OR ")
				.append(" dealCategory.category_name like " + "'%" + keyword + "%') ")
				.append(" AND deal.isExpire = '0'");
			}else {
				where.append(" deal.url like " + "'%" + keyword + "%'" ).append(" OR ")
				.append(" deal.text like " + "'%" + keyword + "%' ").append(" OR ")
				.append(" dealCategory.category_name like " + "'%" + keyword + "%' ")
				.append(" AND deal.isExpire = '0'");
			}
		} if (request.getParameter("source") != null) {
			where.append(" AND deal.source IN (" + ConvertString(request.getParameter("source")) +")" );
		}
	}

	private String ConvertString(String parameter) {
	  StringBuilder returnData = new StringBuilder();
	  String[] values = parameter.split(":");
	  String[] strData = values[1].split(",");
	  for (String str : strData) {
		  returnData.append('"'+str+'"'+",");
	   }
	    returnData.deleteCharAt(returnData.length() - 1);
		return returnData.toString();
	}

	@Override
	public List<SDModel> findKeyWordInDeal(HttpServletRequest request) {
		List<SDModel> deal = new ArrayList<>();
		Long pageSize = 0L;
		Long pageNumber = 0L;
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder("");
		StringBuilder where = new StringBuilder("");
		//where.append(" 1=1 ");
		createWhereClause(request, params, where, sql);

		String finalSQL = new String();
		finalSQL = dealQueryMap.get("deal_Category_sql").replace("{0}", where);
		//finalSQL = finalSQL.replace("{1}", sql);

		if (request.getParameter("pageNumber") != null && request.getParameter("pageSize") != null) {
			pageNumber = Long.valueOf(request.getParameter("pageNumber").toString());
			pageSize = Long.valueOf(request.getParameter("pageSize").toString());
			if (pageNumber != 0 && pageSize != 0) {
				finalSQL = jdbcPagination(pageNumber, pageSize, finalSQL);
			}
		}
		deal = jdbcTemplate.query(finalSQL, sdModelMapper);
		return deal;

	}

	private static RowMapper<SDModel> sdModelMapper = new RowMapper<SDModel>() {
		@Override
		public SDModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			SDModel deal = new SDModel();
			deal.setUrl(rs.getString("url"));
			deal.setText(rs.getString("text"));
			deal.setImageUrl(rs.getString("imageUrl"));
			deal.setCategory(rs.getString("category"));
			deal.setSource(rs.getString("source"));
			deal.setRandom(rs.getInt("random"));
			deal.setOriginalPrice(rs.getDouble("originalPrice"));
			deal.setSalePrice(rs.getDouble("salePrice"));
			deal.setFirstSeen(rs.getDate("firstSeen"));
			deal.setSdDeal(rs.getBoolean("sdDeal"));
			deal.setAffliateLink(rs.getString("affliateLink"));
			deal.setCustomerReviewAverage(rs.getString("customerReviewAverage"));
			deal.setCustomerReviewCount(rs.getString("customerReviewCount"));
			deal.setOfferType(rs.getString("offerType"));
			deal.setCategory(rs.getString("category_name"));
			deal.setLikes(rs.getLong("likes"));
			deal.setDislikes(rs.getLong("dislikes"));
			deal.setAddToCartUrl(rs.getString("addToCartUrl"));
			return deal;
		}
	};

	@Override
	public Deal findById(Long id) {

		return dealRepository.findById(id);
	}

	@Override
	public Deal updateDeal(Deal deal, Deal dbDeal, String feedbackType, boolean updatefeedbackFlag, long userId) {
		User user =  new User();
		user.setId(userId);
		if(deal.getId() == null){
			deal = findByUniqueId(deal.getUniqueId()); 
		}
		DealFeedback dbDealFeedback = dealFeedbackRepository.findByDealAndUser(deal, user);
		boolean flag = true;
		if(dbDealFeedback!= null){
			if(dbDealFeedback.isLikes() && feedbackType.equals("like")){
				flag = false;
			}
			if(!dbDealFeedback.isLikes() && feedbackType.equals("disLike")){
				flag = false;
			}
		}
		long disLike = dbDeal.getDislikes();
		long like = dbDeal.getLikes();
		if(feedbackType.equals("like") && flag){
			++like;
			if(updatefeedbackFlag){
				--disLike;
				dbDeal.setDislikes(disLike);
			}		
			dbDeal.setLikes(like);
		}
		if(feedbackType.equals("disLike") && flag){
			++disLike;
			if(updatefeedbackFlag){
				--like;
				dbDeal.setLikes(like);
			}
			dbDeal.setDislikes(disLike);
		}

		return dealRepository.save(dbDeal);
	}

	@Override
	public Deal findByUniqueId(String uniqueId) {
		List<Deal> deal = new ArrayList<Deal>();
		StringBuilder where = new StringBuilder("");
		where.append(" deal.uniqueId = " + "'" + uniqueId + "'" )
		.append(" AND deal.isExpire = '0'");
		String finalSQL = new String();
		finalSQL = dealQueryMap.get("dealSQL").replace("{0}", where);
		deal = jdbcTemplate.query(finalSQL, dealMapper);
		if(deal.size() > 0)
			return deal.get(0);
		else
			return null;
	}

	@Override
	public Deal saveDeal(Deal deal) {
		// TODO Auto-generated method stub
		return dealRepository.save(deal);
	}

	protected String jdbcPagination(Long pageNumber, Long pageSize, String finalSQL) {
		//Long offset = (pageNumber * pageSize) - pageSize;
		//Long limit = pageSize;
		finalSQL += " LIMIT " + pageSize + " OFFSET " + pageNumber + " ";
		return finalSQL;
	}

	@Override
	public Long getTotalCount(HttpServletRequest request) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder("");
		StringBuilder where = new StringBuilder("");
		//where.append(" 1=1 ");
		createWhereClause(request, params, where, sql);

		String finalSQL = new String();
		finalSQL = dealQueryMap.get("deal_Category_sql_count").replace("{0}", where);

		Integer totalCount = jdbcTemplate.queryForObject(finalSQL, Integer.class);
		return totalCount.longValue();
	}

	public void saveDealsData(List<Deal> tempdeals, DealCronJob dealCronJob) {

		dealCronJob.setCronDealCount((long) tempdeals.size());
		Long dealCronCount = 0L;
		Long updateDealCronCount = 0L;

		if(tempdeals.size() > 0 && !tempdeals.isEmpty()){
			StringBuilder where = new StringBuilder("");
			where.append(" 1=1");
			String finalSQL = new String();
			finalSQL = dealQueryMap.get("dealSQL").replace("{0}", where);
			List<Deal> dbDeals = jdbcTemplate.query(finalSQL, dealMapper);

			//List<Deal> dbDeals = dealRepository.findAll(); dsfvsdv
			// Create cache of deals from database 
			Map<String, Deal> dealsInDatabase = new HashMap<String, Deal>();
			for (Deal deal : dbDeals) {
				dealsInDatabase.put(deal.getUniqueId(), deal);
			}

			//this.compareDealExpire(tempdeals,dbDeals, dealsInDatabase);
			if(dbDeals.size() > 0 && !dbDeals.isEmpty()){
				for (Deal deal : tempdeals) {
					boolean isNotExist = dealsInDatabase.containsKey(deal.getUniqueId());
					if (!isNotExist) {
						dealCronCount += 1;
							deal.setIsExpire(new String("0"));
							dealRepository.save(deal);
						dealsInDatabase.put(deal.getUniqueId(), deal);
					} else {
						Deal tempDeal = findByUniqueId(deal.getUniqueId());
						
						if(tempDeal != null){
							updateDealCronCount += 1;
							boolean olderThan3 = findGreaterThanThreeDayDeal(tempDeal.getFirstSeen());

							if(olderThan3){
								tempDeal.setIsExpire(new String("1"));
							} else { 
								tempDeal.setAffliateLink(deal.getAffliateLink());
								tempDeal.setCategory(deal.getCategory());
								tempDeal.setCategoryId(deal.getCategoryId());
								tempDeal.setCustomerReviewAverage(deal.getCustomerReviewAverage());
								tempDeal.setCustomerReviewCount(deal.getCustomerReviewCount());
								tempDeal.setFirstSeen(deal.getFirstSeen());
								tempDeal.setUniqueId(deal.getUniqueId());
								tempDeal.setUrl(deal.getUrl());
								tempDeal.setText(deal.getText());
								tempDeal.setImageUrl(deal.getImageUrl());
								tempDeal.setSource(deal.getSource());
								tempDeal.setRandom(deal.getRandom());
								tempDeal.setOriginalPrice(deal.getOriginalPrice());
								tempDeal.setSalePrice(deal.getSalePrice());
								tempDeal.setFirstSeen(deal.getFirstSeen());
								tempDeal.setIsExpire(new String("0"));
							}
							dealRepository.save(tempDeal);
						}
							 
					}
				}
			}else{
				for (Deal deal : tempdeals) {
					dealCronCount += 1;
					deal.setIsExpire(new String("0"));
					dealRepository.save(deal);
				}
			}

		}else{
			System.out.println("Scrapper "+ dealCronJob.getAuthor() +" not fetch deal...");
		}
		dealCronJob.setUpdateDealCount(updateDealCronCount);
		dealCronJob.setSaveDealCount(dealCronCount);
		dealCronJobService.save(dealCronJob);
		dealController.createCache();
	}

	private boolean findGreaterThanThreeDayDeal(Date date) {
	
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String dateStart = simpleDateFormat.format(new Date());
		
		String dateStop = date.toString();

		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date d1 = null;
		Date d2 = null;
		try {
			d2 = format.parse(dateStart);
			d1 = format.parse(dateStop);

			long diff = d2.getTime() - d1.getTime();
			long diffDays = diff / (24 * 60 * 60 * 1000);

			if(diffDays >= 72)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	private void createOrderClause(HttpServletRequest request, Map<String, Object> params, StringBuilder where,
			StringBuilder sql) {
		String keyword = request.getParameter("ratingFilterData");
		if (keyword.equalsIgnoreCase("Dislikes - High to Low")) {
			where.append("deal.dislikes >= 1").append(" AND deal.isExpire = '0'")
			.append(" ORDER BY deal.dislikes DESC");
		} if (keyword.equalsIgnoreCase("Likes - High to Low")) {
			where.append("deal.likes >= 1").append(" AND deal.isExpire = '0'")
			.append(" ORDER BY deal.likes DESC");	
		} if (keyword.equalsIgnoreCase("Likes - Low to High")) {
			where.append("deal.likes >= 1").append(" AND deal.isExpire = '0'")
			.append(" ORDER BY deal.likes ASC");
		} if (keyword.equalsIgnoreCase("Dislikes - Low to High")) {
			where.append("deal.dislikes >= 1").append(" AND deal.isExpire = '0'")
			.append(" ORDER BY deal.dislikes ASC");
		} if (keyword.equalsIgnoreCase("Popularity")) {
			where.append(" deal.isExpire = '0'").append(" ORDER BY deal.dislikes DESC");
		}
	}

	@Override
	public List<Deal> getDealSortedData(HttpServletRequest request) {
		List<Deal> deal = new ArrayList<>();
		Long pageSize = 0L;
		Long pageNumber = 0L;
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder("");
		StringBuilder where = new StringBuilder("");
		createOrderClause(request, params, where, sql);

		String finalSQL = new String();
		finalSQL = dealQueryMap.get("deal_dealfeedback_sql").replace("{0}", where);

		if (request.getParameter("pageNumber") != null && request.getParameter("pageSize") != null) {
			pageNumber = Long.valueOf(request.getParameter("pageNumber").toString());
			pageSize = Long.valueOf(request.getParameter("pageSize").toString());
			if (pageNumber != 0 && pageSize != 0) {
				finalSQL = jdbcPagination(pageNumber, pageSize, finalSQL);
			}
		}
		deal = jdbcTemplate.query(finalSQL, dealMapper);
		return deal;
	}

	@Override
	public Long getTotalDealCount(HttpServletRequest request) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder("");
		StringBuilder order = new StringBuilder("");
		createOrderClause(request, params, order, sql);

		String finalSQL = new String();
		finalSQL = dealQueryMap.get("deal_dealfeedback_count_sql").replace("{0}", order);

		Integer totalCount = jdbcTemplate.queryForObject(finalSQL, Integer.class);
		return totalCount.longValue();
	}

	@Override
	public List<Deal> getDealByText(HttpServletRequest request) {  
		List<Deal> deal = new ArrayList<>();
		Long pageSize = 0L;
		Long pageNumber = 0L;
		//StringBuilder where = new StringBuilder("");
		//where.append(" deal.text REGEXP '"+ request.getParameter("searchText") +".'")
		//.append(" and deal.isExpire = '0'").append(" ORDER by deal.text");

		String finalSQL = new String();
		finalSQL = " SELECT *,LOCATE('"+request.getParameter("searchText")+"',text) as location" 
		+" FROM Deal " 
		+" WHERE locate('"+request.getParameter("searchText")+"',text)>0 and isExpire = '0'"; 
		
		
		//finalSQL = dealQueryMap.get("dealSQL").replace("{0}", where);

		/*
		 * if (request.getParameter("pageNumber") != null &&
		 * request.getParameter("pageSize") != null) { pageNumber =
		 * Long.valueOf(request.getParameter("pageNumber").toString()); pageSize =
		 * Long.valueOf(request.getParameter("pageSize").toString()); if (pageNumber !=
		 * 0 && pageSize != 0) { finalSQL = jdbcPagination(pageNumber, pageSize,
		 * finalSQL); } }
		 */
		deal = jdbcTemplate.query(finalSQL, dealSearchMapper);
		return deal;
	}

	@Override
	public Long getDealByTextCount(HttpServletRequest request) {
		/*
		 * StringBuilder where = new StringBuilder("");
		 * where.append(" deal.text like '%"+ request.getParameter("searchText") +"%'")
		 * .append(" and deal.isExpire = '0'");
		 * 
		 * String finalSQL = new String(); finalSQL =
		 * dealQueryMap.get("deal_count_SQL").replace("{0}", where);
		 */
		String finalSQL = new String();
		finalSQL = " SELECT count(*)" 
		+" FROM Deal " 
		+" WHERE locate('at',text)>0 and isExpire = '0'"; 
		
		Integer totalCount = jdbcTemplate.queryForObject(finalSQL, Integer.class);
		return totalCount.longValue();
	}

	//@Cacheable(value = "test")
	@Override
	public Deal getAllHotDealTemp() {
		Deal d  = new Deal();
		d.setId(11111111111l);
		d.setCategory("===========");
		return d;
	}


	@Override
	public List<Deal> getAllHotDeal() {
		List<Deal> deal = null;
		StringBuilder where = new StringBuilder("");
		where.append(" deal.isExpire = '0' or isExpire is null " ).append(" order by firstSeen desc" );
		String finalSQL = new String();
		finalSQL = dealQueryMap.get("dealSQL").replace("{0}", where);
		deal = jdbcTemplate.query(finalSQL, dealMapper);
		return deal;
	}

	@Override
	public Page<Deal> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return dealRepository.findAll(pageable);
	}

	@Override
	public List<Deal> getAllHotDealWithtext() {
		List<Deal> deal = new ArrayList<Deal>();
		String finalSQL = new String();
		finalSQL = " SELECT source, text FROM Deal" 
		+" WHERE isExpire = '0' or isExpire is null ORDER BY text"; 
		deal = jdbcTemplate.query(finalSQL, dealAllHotDealWithTextMapper);
		return deal;
	}
	
	private static RowMapper<Deal> dealAllHotDealWithTextMapper = new RowMapper<Deal>() {
		@Override
		public Deal mapRow(ResultSet rs, int rowNum) throws SQLException {
			Deal deal = new Deal();
			deal.setText(rs.getString("text"));
			deal.setSource(rs.getString("source"));
			return deal;
		}
	};

	@Override
	public List<Deal> getAllSearchKeyword() {
		List<Deal> deals = new ArrayList<Deal>();
		List<DealCategory> dealCategorys = new ArrayList<DealCategory>();
		dealCategorys = dealCategoryRepository.findAll();
		for (DealCategory dealCategory : dealCategorys) {
			Deal deal = new Deal();
			deal.setCategory(dealCategory.getCategoryName());
			deals.add(deal);
		}
		List<String> iListOfStores = Deal.getiListOfFavIcons();
		for (String store : iListOfStores) {
			Deal deal = new Deal();
			deal.setCategory(store);
			deals.add(deal);
		}
		return deals;
	}

	@Override
	public void applyIsExpire() {
		List<Deal> dbDeal =  new ArrayList<Deal>();
		StringBuilder where = new StringBuilder("");    
		where.append(" deal.firstSeen <= NOW() - INTERVAL 3 DAY");  
		String finalSQL = new String();
		finalSQL = dealQueryMap.get("dealSQL").replace("{0}", where);
		dbDeal = jdbcTemplate.query(finalSQL, dealMapper);

		if(dbDeal.size() > 0) {
			for (Deal deal : dbDeal) {
			   deal.setIsExpire(new String("1"));
			}
			dealRepository.save(dbDeal);
		}
	}
}
