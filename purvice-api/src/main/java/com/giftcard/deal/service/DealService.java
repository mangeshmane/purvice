package com.giftcard.deal.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.cache.annotation.CachePut;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.giftcard.deals.model.SDModel;
import com.giftcard.model.Deal;
import com.giftcard.model.DealAlert;
import com.giftcard.response.ResponseWrapper;

public interface DealService {
	public void saveDealsData(List<Deal> deals);
	public List<Deal> getAllDeal();
	public List<Deal> findByCategoryId(DealAlert tempDealAlert);
	List<Deal> getDealUsingKeyword(String searchKey);
	public List<Deal> getDealByIds(List<Long> dealId);
	public ResponseWrapper<List<Deal>> getDealWithPaginationAndFilter(HttpServletRequest request);
	public ResponseWrapper<List<Deal>> getDealWithPaginationAndFilterPost(HttpServletRequest request, Deal deal);
	public void removeAllExpireDeal();
	public List<SDModel> findKeyWordInDeal(String uniqueId);
	public Deal findById(Long id);
	public Deal updateDeal(Deal deal, Deal tempDeal, String feedbackType, boolean updatefeedbackFlag, long userId);
	public Deal findByUniqueId(String uniqueId);
	public Deal saveDeal(Deal deal);
	public List<SDModel> findKeyWordInDeal(HttpServletRequest request);
	public Long getTotalCount(HttpServletRequest request);
	public List<Deal> getDealSortedData(HttpServletRequest request);
	public Long getTotalDealCount(HttpServletRequest request);
	public List<Deal> getDealByText(HttpServletRequest request);
	public Long getDealByTextCount(HttpServletRequest request);
	
	
	public List<Deal> getAllHotDeal();
	//@CachePut(value = "deals", key = "#deal.id")
	Deal getAllHotDealTemp();
	
	public Page<Deal> findAll(Pageable pageable);
	public List<Deal> getAllHotDealWithtext();
	public List<Deal> getAllSearchKeyword();
	public void applyIsExpire();

}
