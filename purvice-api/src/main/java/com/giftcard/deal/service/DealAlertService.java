package com.giftcard.deal.service;

import java.util.List;

import com.giftcard.model.Deal;
import com.giftcard.model.DealAlert;

public interface DealAlertService {

	public List<DealAlert> saveDealAlertData(List<DealAlert> dealAlert);

	public List<DealAlert> findByIsActive(String isActive, String frequency);

	public void sendDealAlert(DealAlert tempDealAlert, Deal deal);

	public void save(DealAlert dealAlert);

	public DealAlert findById(Long dealAlertId);

	public DealAlert updateDealAlert(DealAlert dealAlert, DealAlert tempDealAlert);

	public List<DealAlert> getAll();

	public List<DealAlert> getUserDealAlert(Long userId);

	public List<DealAlert> findByDealAlert(String b, String string);

	public void setParameterAfterMail(DealAlert tempDealAlert);

	public List<DealAlert> findEmailFlagDealAlert();

	public void sendDealAlertByDeals(DealAlert tempDealAlert, List<Deal> deals);

	public DealAlert saveDealAlertDataByShared(DealAlert dealAlert);
	
}
