package com.giftcard.tree;

public interface Visitor<T> {
	public void visit (T node);

	public void startVisit(T node);

	public void endVisit(T node);
}
