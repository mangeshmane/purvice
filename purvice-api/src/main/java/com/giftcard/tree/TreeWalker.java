package com.giftcard.tree;

import org.springframework.stereotype.Component;

@Component
public class TreeWalker {

	@SuppressWarnings("rawtypes")
	public void walk(TreeModel model, Visitor visitor, Direction direction) {
		visitNode(model.getRoot(), visitor, direction);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void visitNode(TreeNode node, Visitor visitor, Direction direction) {
		if (node == null) return; 
		
		try {
			visitor.startVisit(node);
		switch (direction) {
		case TOP_DOWN: {
			visitor.visit(node);
			visitNode(node.getFirstChild(), visitor, direction);
			visitNode(node.getSibling(), visitor, direction);
			break;
		}
		case BOTTOM_UP: {
			visitNode(node.getFirstChild(), visitor, direction);
			visitNode(node.getSibling(), visitor, direction);
			visitor.visit(node);
			break;
		}
		}
		}
		finally {
			visitor.endVisit(node);
		}
	}

}
