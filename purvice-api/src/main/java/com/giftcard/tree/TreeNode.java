package com.giftcard.tree;

public abstract class TreeNode<NodeType extends Object> {

	private TreeModel<?> model;
	private TreeNode<NodeType> sibling;
	private TreeNode<NodeType> firstChild;
	
	private NodeType contents;
	
	public TreeNode ()
	{
	}
	
	public TreeNode (TreeModel<?> model, NodeType contents)
	{
		this.model = model;
		this.contents = contents;
	}

	public TreeNode<NodeType> getSibling() {
		return sibling;
	}

	public void setSibling(TreeNode<NodeType> sibling) {
		this.sibling = sibling;
	}

	public TreeNode<NodeType> getFirstChild() {
		return firstChild;
	}

	public void setFirstChild(TreeNode<NodeType> firstChild) {
		this.firstChild = firstChild;
	}

	public NodeType getContents() {
		return contents;
	}

	public void setContents(NodeType contents) {
		this.contents = contents;
	}

	public TreeModel<?> getModel() {
		return model;
	}

	public void append(TreeNode<NodeType> newNode) {
		TreeNode<NodeType> nextNode = getFirstChild(); 
		if (nextNode == null)
		{
			setFirstChild(newNode);
		}
		else {
			while (nextNode.getSibling() != null)
				nextNode = nextNode.getSibling();
			
			nextNode.setSibling(newNode);
		}
	}
}
