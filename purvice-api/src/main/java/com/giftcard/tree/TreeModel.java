package com.giftcard.tree;

import java.io.Serializable;

public class TreeModel <T extends TreeNode<?>> implements Serializable{

	private static final long serialVersionUID = -406447598317050780L;

	private T root;

	public T getRoot() {
		return root;
	}

	public void setRoot(T root) {
		this.root = root;
	}
	
}

