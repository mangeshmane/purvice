package com.giftcard.tango.jobs;

import com.tangocard.client.model.CreateOrderCriteria;
import com.tangocard.client.model.OrderViewSummary;
import com.tangocard.client.model.ResendView;

public interface TangoService {

	void getCatalog();
	public OrderViewSummary placeOrder(CreateOrderCriteria createOrderCriteria);
	
	public ResendView resendOrder(String referenceOrderID);
}
