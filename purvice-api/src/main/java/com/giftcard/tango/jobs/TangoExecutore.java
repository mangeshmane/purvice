package com.giftcard.tango.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@EnableScheduling
public class TangoExecutore {
	@Autowired
	private TangoService tangoService;
	//every 1 minute
	@Scheduled(cron = "0 0 0 * * ?")
	@Transactional
	 public void cronTaskForGetCatalog(){
	 tangoService.getCatalog();
	 }
}
