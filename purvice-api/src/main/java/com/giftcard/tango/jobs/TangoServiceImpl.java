package com.giftcard.tango.jobs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.giftcard.model.Brand;
import com.giftcard.model.BrandImage;
import com.giftcard.model.ItemCountry;
import com.giftcard.service.BrandService;
import com.google.gson.Gson;
import com.tangocard.client.ApiClient;
import com.tangocard.client.ApiException;
import com.tangocard.client.Configuration;
import com.tangocard.client.api.OrdersApi;
import com.tangocard.client.auth.HttpBasicAuth;
import com.tangocard.client.model.CreateOrderCriteria;
import com.tangocard.client.model.OrderViewSummary;
import com.tangocard.client.model.ResendView;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Component
@PropertySource("classpath:application.properties")
public class TangoServiceImpl implements TangoService {
	@Value(value = "${tango.url}")
	private String URL;

	@Value(value = "${tango.token}")
	private String token;

	@Value(value = "${tango.userName}")
	private String userName;

	@Value(value = "${tango.password}")
	private String password;
	
	@Value(value = "${tango.basePath}")
	private String basePath;	

	@Autowired
	private BrandService brandService;

	private static final Logger logger = LoggerFactory.getLogger(TangoServiceImpl.class);

	int count =2;
	
	@Override
	@Transactional
	public void getCatalog() {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Authorization", token);
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> result = restTemplate.exchange(URL, HttpMethod.GET, entity, String.class);
		String catalog = result.getBody();
		Gson gson = new Gson();
		if (catalog != null) {
			List<Brand> brandList = formatRowData(catalog);
			this.saveBrands(brandList);
		}

	}

	public List<Brand> formatRowData(String result) {
		JSONObject json = JSONObject.fromObject(result);

		JSONArray jsonArray = json.getJSONArray("brands");

		List<Brand> brands = new ArrayList<Brand>();
		List<JSONObject> list = (List<JSONObject>) jsonArray;

		for (JSONObject object : list) {

			Gson gson = new Gson();
			JSONObject brandImageJson = (JSONObject) object.get("imageUrls");
			List<JSONObject> itemList = object.getJSONArray("items");

			for (JSONObject item : itemList) {

				JSONArray j = item.getJSONArray("countries");
				List<ItemCountry> countryList = new ArrayList<ItemCountry>();
				for (int i = 0; i < j.size(); i++) {
					ItemCountry itemCountry = new ItemCountry();
					itemCountry.setName(j.getString(i));
					countryList.add(itemCountry);
				}
				item.put("countries", countryList);
			}
			Iterator<?> keys = brandImageJson.keys();
			List<BrandImage> brandImageList = new ArrayList<BrandImage>();
			List<ItemCountry> itemCountryList = new ArrayList<ItemCountry>();

			while (keys.hasNext()) {
				String key = (String) keys.next();
				BrandImage brandImage = new BrandImage();
				if (brandImageJson.get(key).toString() != null) {
					brandImage.setSize(key);
					brandImage.setImageURL(brandImageJson.get(key).toString());
					brandImageList.add(brandImage);
				}
			}
			object.put("imageUrls", brandImageList);

			Brand brand = gson.fromJson(object.toString(), Brand.class);

			brands.add(brand);
		}
		return brands;
	}

	public void saveBrands(List<Brand> brandList) {
		try {
			for (Brand brand : brandList) {
				Brand dbBrand = brandService.findByBrandKey(brand.getBrandKey());
				if (dbBrand != null) {
					brandService.update(dbBrand);
				} else {
					brandService.save(brand);
				}

			}

		} catch (Exception exe) {
			logger.debug("BrandData:========" + exe);
		}

	}

	@Override
	public OrderViewSummary placeOrder(CreateOrderCriteria createOrderCriteria) {
		OrderViewSummary result = null;
		try {
			ApiClient defaultClient = null;
			defaultClient = Configuration.getDefaultApiClient();
			System.out.println("@@@@@ACTEST old base path:"+defaultClient.getBasePath());
			defaultClient.setBasePath(basePath);

			// Configure HTTP basic authorization: basicAuth
			HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
			basicAuth.setUsername(userName);
			basicAuth.setPassword(password);
			OrdersApi apiInstance = new OrdersApi();
			System.out.println("@@@@@ACTEST new base path 1:"+defaultClient.getBasePath());
			System.out.println("@@@@@ACTEST new base path 2:"+apiInstance.getApiClient().getBasePath());
			
			result = apiInstance.createOrder(createOrderCriteria);
		} catch (ApiException e) {
			logger.error("Exception when calling OrdersApi#createOrder.== Responsecode : "+e.getCode() , e);
			while(count>0){
				count--;
				placeOrder(createOrderCriteria);
				
			}
		}
		return result;
	}

	@Override
	public ResendView resendOrder(String referenceOrderID) {
		ApiClient defaultClient = Configuration.getDefaultApiClient();
		defaultClient.setBasePath(basePath);

		// Configure HTTP basic authorization: basicAuth
		HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
		basicAuth.setUsername(userName);
		basicAuth.setPassword(password);

		OrdersApi apiInstance = new OrdersApi();
		ResendView result = null;
		try {
			result = apiInstance.getOrderResends(referenceOrderID);
			System.out.println(result);
		} catch (ApiException e) {
			logger.error("Exception when calling OrdersApi#getOrderResends. Responsecode : "+e.getCode(),e);
		}

		return result;
	}

}