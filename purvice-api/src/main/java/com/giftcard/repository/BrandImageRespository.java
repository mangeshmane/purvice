package com.giftcard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giftcard.model.BrandImage;

public interface BrandImageRespository extends JpaRepository<BrandImage, Long> {

	public BrandImage findById(long id);
	
	public BrandImage deleteById(long id);
	
	public List<BrandImage> findByBrand_id(long id);
	
}

