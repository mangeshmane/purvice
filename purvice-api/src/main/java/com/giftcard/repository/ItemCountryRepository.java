package com.giftcard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giftcard.model.ItemCountry;

public interface ItemCountryRepository extends JpaRepository<ItemCountry, Long> {

	public List<ItemCountry> findByItem_id(long id);
}
