package com.giftcard.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.giftcard.model.Deal;

@Repository
public interface DealRepository extends JpaRepository<Deal, Long>, JpaSpecificationExecutor<Deal>, PagingAndSortingRepository<Deal, Long> {

	@Query("select deal from Deal deal where lower(deal.text) like CONCAT('%', :searchKey, '%') or lower(deal.url) like CONCAT('%', :searchKey, '%')")
	List<Deal> getDealUsingKeyword(@Param("searchKey") String searchKey);
	public Deal findById(long id);
	public Deal save (Deal deal);
	
	Page<Deal> findAll(Pageable pageable);
}
