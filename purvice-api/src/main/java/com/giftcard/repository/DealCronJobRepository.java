package com.giftcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giftcard.model.DealCronJob;

public interface DealCronJobRepository extends JpaRepository<DealCronJob, Long> {

}
