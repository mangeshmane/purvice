package com.giftcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.giftcard.model.Banner;

@Repository
public interface BannerRepository extends JpaRepository<Banner, Long>  {

}
