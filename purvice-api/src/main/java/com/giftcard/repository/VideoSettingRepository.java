package com.giftcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giftcard.model.VideoSetting;

public interface VideoSettingRepository extends JpaRepository<VideoSetting, Long> {

	public VideoSetting findOne(long id);
}
