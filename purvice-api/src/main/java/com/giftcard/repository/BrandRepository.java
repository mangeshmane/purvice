
package com.giftcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.giftcard.model.Brand;

@Repository
public interface BrandRepository extends JpaRepository<Brand, Long> {

	
	public Brand findById(long id);
	
	public Brand deleteById(long id);
	
	public Brand findByBrandName(String brandName);
	
	public Brand findByBrandKey(String brandKey);
}
