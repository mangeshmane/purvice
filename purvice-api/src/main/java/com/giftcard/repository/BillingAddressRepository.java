package com.giftcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giftcard.model.BillingAddress;

public interface BillingAddressRepository extends JpaRepository<BillingAddress, Long>{

	public BillingAddress findByUser_id(long id);
}
