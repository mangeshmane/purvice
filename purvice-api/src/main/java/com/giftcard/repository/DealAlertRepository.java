package com.giftcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.giftcard.model.DealAlert;

@Repository
public interface DealAlertRepository  extends JpaRepository<DealAlert, Long> {

}
