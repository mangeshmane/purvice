package com.giftcard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.giftcard.model.Category;
public interface CategoryRepository extends JpaRepository<Category, Long>{

	public Category findOneByCategoryname(String categoryname);
	public Category getOne(long id);
	public List<Category> findAll();
	public Category findOne(long id);
	
}
