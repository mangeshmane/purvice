package com.giftcard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giftcard.model.Orders;

public interface OrdersRepository extends JpaRepository<Orders, Long> {

	public List<Orders> findAll();

	public List<Orders> findByIsscheduled(boolean b);

	public List<Orders> findByUser_id(long id);

	public void delete(long id);

	public List<Orders> findByTransactionNotLike(String transactionStatus);

	public Long countByUser_idAndTangoOrderStatusNotLike(long id, String status);
}
