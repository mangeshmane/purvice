package com.giftcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giftcard.model.DealEmailRelation;

public interface DealEmailRelationRepository extends JpaRepository<DealEmailRelation, Long>{

	public DealEmailRelation save(DealEmailRelation dealEmailRelation);
}
