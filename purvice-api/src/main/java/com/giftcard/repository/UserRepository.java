package com.giftcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giftcard.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	public User findByUsername(String username);
	
	public User findByToken(String token);
	
	public User findByPasswordResetToken(String token);

	public User findByEmailId(String emailId);
	
}
