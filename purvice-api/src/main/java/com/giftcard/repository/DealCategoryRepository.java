package com.giftcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.giftcard.model.DealCategory;

@Repository
public interface DealCategoryRepository extends JpaRepository<DealCategory, Long>{


}
