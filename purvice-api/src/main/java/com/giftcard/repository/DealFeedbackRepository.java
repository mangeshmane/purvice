package com.giftcard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giftcard.model.Deal;
import com.giftcard.model.DealFeedback;
import com.giftcard.model.User;

public interface DealFeedbackRepository extends JpaRepository<DealFeedback, Long>{

	public DealFeedback save(DealFeedback dealFeedback);
	
	public DealFeedback findByDealAndUser (Deal id, User user_id);
	
	public List<DealFeedback> findByUser (User user);
}

