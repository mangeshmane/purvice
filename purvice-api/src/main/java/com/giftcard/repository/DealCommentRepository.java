package com.giftcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giftcard.model.DealComment;

public interface DealCommentRepository extends JpaRepository<DealComment, Long>{

	public DealComment findById(long id);
	@SuppressWarnings("unchecked")
	public DealComment save (DealComment dealComment);
}
