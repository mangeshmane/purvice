package com.giftcard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.giftcard.model.Brand;
import com.giftcard.model.Item;

public interface ItemRepository extends JpaRepository<Item, Long>{
	
	public Item findById(long id);
	
	public Item deleteById(long id);
	
	public List<Item> findByCategory_id(long id);
	
	public List<Item> findByBrand_id(long id);
	
	public Brand findByBrand_id(Brand brand);
	
	public List<Item> findByBrand_idOrCategory_id(@Param("brandId")long brandId, @Param("brandId")long CategoryId);
	
	public Item findByUtid(String utid);
}
