package com.giftcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giftcard.model.ItemDenomination;

public interface ItemDenominationRepository extends JpaRepository<ItemDenomination, Long>{

	public ItemDenomination findByItemId(long id);
}
