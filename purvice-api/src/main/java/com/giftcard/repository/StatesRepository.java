package com.giftcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.giftcard.model.States;

public interface StatesRepository extends JpaRepository<States, Long> {

	
}
