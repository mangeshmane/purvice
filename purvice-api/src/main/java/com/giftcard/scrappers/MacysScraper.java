package com.giftcard.scrappers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.giftcard.deals.model.JsoupUtil;
import com.giftcard.deals.model.Product;


public class MacysScraper extends AbstractScraper {
	
	public MacysScraper() {
	}

	public static final String BASE_URL = "http://www.macys.com/";
	public static final String MACYS_PAGINATION = "/Pageindex,Productsperpage/%d,40?";
	public static final int MAX_PAGE = 1;
	
	protected String nextUrl = BASE_URL;

	public List<Product> pageScraping(String nextPage) {
		List<Product> products = new ArrayList<Product>();

		Document doc = null;

		Product product;
		String productName, imageUrl, productUrl;
		Double saleprice, baseprice;
		try {
			doc = Jsoup.connect(nextPage).timeout(50000).get();
			
			//System.out.println(doc);
			
			Elements entries = doc.select("li[class=productThumbnail borderless]");
			System.out.println(entries.size());
			for(Element entry : entries) {
				
				Element name = entry.select("div.shortDescription").first();
				Element prodUrl = entry.getElementsByTag("a").first();
				Element image = prodUrl.getElementsByAttributeValue("name", "CATimage").first();
				Element basePrice = entry.select("div.prices span").first();
				Element salePrice = entry.select("div.prices span.priceSale").first();
				
				productName = name.text();
				productUrl = prodUrl.absUrl("href");
				imageUrl = image.absUrl("data-src");
				baseprice = JsoupUtil.parsePrice(basePrice.text());
				saleprice = (salePrice != null) ? JsoupUtil.parsePrice(salePrice.text()) : baseprice;
				
				product = new Product("",productUrl, productName, imageUrl, baseprice, saleprice);
				products.add(product);
				System.out.println(product.toString());
				//System.out.println("#####################");
			}
			
			//nextUrl = String.format(REDDIT_URL_PAGINATION, entries.last().parent().attr("data-fullname"));
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return products;
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		AbstractScraper scp = new MacysScraper();
		List<Product> products = scp.productScraping();
		scp.writeOutput(products, "macys_output.txt");
	}

	@Override
	public List<Product> productScraping() {
		List<Product> products = new ArrayList<Product>();
		Document doc;
		try {
			//doc = Jsoup.connect(BASE_URL).timeout(50000).get();
			//Element special = doc.select("area[alt=shop all]").first();
			//nextUrl = special.absUrl("href");
			
			//nextUrl = "http://www1.macys.com/shop/sale/clearance-closeout?id=54698&edge=hybrid";
			nextUrl = "http://www1.macys.com/shop/sale/clearance-closeout/Pageindex,Productsperpage/2,40?id=54698&edge=hybrid";
			Response response = Jsoup.connect(nextUrl).followRedirects(true).execute();
			nextUrl = response.url().toString();
			
			System.out.println(nextUrl);
			if(nextUrl != null) {
				
				int count = 1;
				while(true) {
					if(count > MAX_PAGE) {
						break;
					}
					
					products.addAll(pageScraping(nextUrl));
					count++;
				}
			}

			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return products;
	}

}
