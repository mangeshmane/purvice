package com.giftcard.scrappers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.giftcard.deals.model.JsoupUtil;
import com.giftcard.deals.model.Product;


public class NordstromScraper extends AbstractScraper {
	public static final String BASE_URL = "https://shop.nordstrom.com/c/trend-sale?sort=Featured&page=%d";
	public static final int MAX_PAGE = 3;

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		AbstractScraper scp = new NordstromScraper();
		List<Product> products = scp.productScraping();
		scp.writeOutput(products, "nordstrom_output.txt");
	}
	
	
	
	
	
	
	
	
	
	
	
	public List<Product> pageScraping(String nextPage) {
		List<Product> products = new ArrayList<Product>();

		Document doc = null;

		Product product;
		String productName, imageUrl, productUrl;
		Double saleprice=0.0, baseprice;
		try {
			doc = Jsoup.connect(nextPage).get();
			
			//System.out.println("@@ACTEST==>>"+doc.html());
			
			//Elements rows = doc.select("div[class=row clearfix standard-row]");
			//System.out.println("@@ACTEST==>>"+doc.html());
			
			//System.out.println("@@ACTEST1==>>"+doc.select("div[id=\"product-results-view\"] section article").size());
//			boolean test  = true;
//			if(test) {
//				return null;
//			}
			
			Elements rows = doc.select("div[id=\"product-results-view\"] section article");
			
			//System.out.println("@@ACTEST==>>"+rows.html());
			
			
			
			
			System.out.println("@@ACTEST==>>"+rows.size());
			for(Element row : rows) {
				//System.out.println("@@ACTEST==>>"+row.html());
				productName = null;
				productUrl = null;
				imageUrl = null;
				baseprice = 0.0;

				
				
				try {
					productName = row.select("[name=\\\"product-module-image\\\"]").text();
					//System.out.println("@@@@ACTEST productName:"+productName);
				} catch (Exception e) {
				}
				
				
				try {
					productUrl = row.selectFirst("[data-element=touch-target product-module-media-link]").absUrl("href");
				} catch (Exception e) {
				}
				
				try {
					imageUrl = row.selectFirst("[name=product-module-image]").absUrl("src");
				} catch (Exception e) {
				}
				
				try {
					saleprice = JsoupUtil.parsePrice(row.selectFirst("[data-element=product-module-price-line product-module-price-line-sale]").selectFirst("[data-element=product-module-price-line-price]").text());
				} catch (Exception e) {
				}
				
				try {
					baseprice = JsoupUtil.parsePrice(row.selectFirst("[data-element=product-module-price-line product-module-price-line-original]").selectFirst("[data-element=product-module-price-line-price]").text());
				} catch (Exception e) {
				}
				
				product = new Product("",productUrl, productName, imageUrl, baseprice, saleprice,null,null, null);
				
				
				

				products.add(product);
				System.out.println(product.toString());
				
				//System.out.println("@@ACTEST==>>\nproductName:["+productName+"] \nproductUrl:["+productUrl+"] \nimageUrl:["+imageUrl+"] \nsaleprice:["+saleprice+"] \nbaseprice:["+baseprice+"]");
				
				
				/*
				for(Element column : row.children()) {
					Element name = column.select("a[class=title]").first();
					Element image = column.select("img").first();
					Element basePrice = column.select("span[class=price regular]").first();
					Element salePrice = column.select("span[class=price sale]").first();
					
					productName = name.text();
					productUrl = name.absUrl("href");
					imageUrl = image.absUrl("data-original");
					saleprice = (salePrice != null) ? JsoupUtil.parsePrice(salePrice.text()) : 0.0;
					baseprice = (basePrice != null) ? JsoupUtil.parsePrice(basePrice.text()) : 0.0;
					
					product = new Product("",productUrl, productName, imageUrl, baseprice, saleprice);
					products.add(product);
					System.out.println(product.toString());
				}
				*/
			}
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return products;
	}

	@Override
	public List<Product> productScraping() {
		List<Product> products = new ArrayList<Product>();

		int count = 1;
		
		while(true) {
			if(count >= MAX_PAGE) {
				break;
			}
			String nextUrl = String.format(BASE_URL, count);
			products.addAll(pageScraping(nextUrl));
			count++;
		}
		return products;
	}

}
