package com.giftcard.scrappers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.giftcard.deals.model.Product;




public abstract class AbstractScraper {
	
	public abstract List<Product> productScraping();

	public void writeOutput(List<Product> products, String outputPath) throws IOException {
		Writer writer = new BufferedWriter(new FileWriter(outputPath));
		IOUtils.writeLines(products, null, writer);
		IOUtils.closeQuietly(writer);
	}
	
}
