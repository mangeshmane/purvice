package com.giftcard.scrappers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.giftcard.deals.model.JsoupUtil;
import com.giftcard.deals.model.Product;
import com.giftcard.deals.model.RestClient;



public class TargetScraper extends AbstractScraper {

	public static final String BASE_URL = "http://www.target.com/";
	public static final String REST_API = "http://tws.target.com/searchservice/item/search_results/v2/by_keyword?callback=getPlpResponse&sort_by=bestselling&pageCount=60&zone=PLP&facets=d_deals%3AWeekly+Ad&category=&view_type=medium&stateData=&response_group=Items%2CVariationSummary&isLeaf=";
	public static final String PAGINATION_FORMAT = "&page=%d&offset=%d";
	public static final int MAX_PAGE = 3;

	public List<Product> pageScraping(String nextUrl) {
		List<Product> products = new ArrayList<Product>();

		Product product;
		String productName, productUrl, imageUrl, category;
		Double saleprice, baseprice;

		String bodyText = RestClient.getResponse(nextUrl);

		int first = bodyText.indexOf(":");
		int last = bodyText.lastIndexOf("}");
		String json = bodyText.substring(first + 1, last);
		try {
			JsonNode root = new ObjectMapper().readTree(json);
			Iterator<JsonNode> iterator = root.findValue("Item").iterator();
			JsonNode productNode;
			JsonNode priceNode;

			String textPrice;
			while(iterator.hasNext()) {
				productNode = iterator.next();
				priceNode = productNode.get("priceSummary");


				category = productNode.get("itemType").asText();
				productName = productNode.get("title").asText();
				imageUrl = productNode.get("images").get("image").get(0).get("url").asText();
				productUrl = BASE_URL + productNode.get("productDetailPageURL").asText();

				textPrice = priceNode.get("offerPrice").get("amount").asText();
				saleprice = (textPrice.isEmpty()) ? 0.0 : JsoupUtil.parsePrice(textPrice);

				textPrice = priceNode.get("listPrice").get("amount").asText();
				baseprice = (textPrice.isEmpty()) ? 0.0 : JsoupUtil.parsePrice(textPrice);

				product = new Product(category, productUrl, productName, imageUrl, baseprice, saleprice);
				products.add(product);
			}
		} catch(IOException e) {
			e.printStackTrace();
		}

		return products;
	}

	@Override
	public List<Product> productScraping() {

		List<Product> products = new ArrayList<Product>();

		int count = 1;

		while(true) {
			if(count >= MAX_PAGE) {
				break;
			}

			String nextUrl = REST_API + String.format(PAGINATION_FORMAT, count, (count - 1) * 60);

			products.addAll(pageScraping(nextUrl));
			count++;
		}
		return products;
	}

	public static void main(String[] args) throws IOException {
		AbstractScraper scp = new TargetScraper();
		List<Product> products = scp.productScraping();
		scp.writeOutput(products, "target_output.txt");
	}

}
