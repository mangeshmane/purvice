package com.giftcard.scrappers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.giftcard.deals.model.JsoupUtil;
import com.giftcard.deals.model.Product;





public class BestbuyScraper extends AbstractScraper {

	public static final String BASE_URL = "http://www.bestbuy.com";
	public static final String COOKIE = "intl_splash=false";

	@Override
	public List<Product> productScraping() {
		List<Product> products = new ArrayList<Product>();

		Product product;
		String productName, imageUrl, productUrl, category;
		Double saleprice, baseprice;

		Document doc = JsoupUtil.getDocument(BASE_URL, COOKIE);
		if(doc != null) {
			Element hotDeal = JsoupUtil.getElement(doc, "ul.primary-nav li li:contains(Hottest)");

			if(hotDeal != null) {
				String hotdealUrl = hotDeal.child(0).absUrl("href");
				// HARDCODED URL;
				//hotdealUrl = "http://www.bestbuy.com/site/misc/black-friday/pcmcat225600050002.c?id=pcmcat225600050002";
				doc = JsoupUtil.getDocument(hotdealUrl);

				if(doc != null) {
					Elements contentElm = doc.select("div#site-control-content div.container div[class=feature-header bleed-margin]");

					for(Element header : contentElm) {
						//System.out.println(header.text());

						category = header.select("h2").first().text();
						Element content = header.nextElementSibling();

						for(Element node : content.children()) {
							Element image = node.select("img").first();
							Element cart = node.select("div.cart-button").first();
							Element name = node.select("h3.feature-ellipsis").first();
							Element salePrice = node.select("div.medium-item-price").first();
							Element basePrice = node.select("span.regular-price").first();
							if(cart != null) {

								productName = name.text();

								productUrl = name.child(0).absUrl("href");
								imageUrl = image.attr("src");
								
								if(salePrice == null && basePrice == null) {
									continue;
								}
								
								saleprice = JsoupUtil.parsePrice(salePrice.text());
								baseprice = (basePrice != null) ? JsoupUtil.parsePrice(basePrice.text()) : saleprice;
								
								// TODO: need to check
								List<String> categoryList = new ArrayList<>();
								categoryList.add(category);
								product = new Product(category, productUrl, productName, imageUrl, baseprice, saleprice);
								System.out.println(product);
								products.add(product);
							}
						}

					}
				}
			}
		}

		return products;
	}

	public static void main(String[] args) throws IOException {
		AbstractScraper scp = new BestbuyScraper();
		List<Product> products = scp.productScraping();
		scp.writeOutput(products, "bestbuy_output.txt");
	}

}
