package com.giftcard.scrappers;


import com.giftcard.deals.model.ProductModel;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class GenericScrapper {

	private static final int NUM_OF_ATTEMPTS = 1;
	//public static final String PROXY_SERVER  = "54.153.69.34";
	public static final String PROXY_SERVER  = "purvise.com";
	public  static final int PROXY_PORT = 8888;
	
	public static void main(String args[]) {
		//parseAndPopulate("amazon-search", "kindle"); // amazon search
		//parseAndPopulate(null,null);// populate all
		//parseAndPopulate("macys", null);
		//parseAndPopulate("nordstrom", null);
		//parseAndPopulate("costco", null);
		//parseAndPopulate("lululemon", null);
		//parseAndPopulate("forever21", null);
		//parseAndPopulate("zara", null);
		//parseAndPopulate("zappos", null);
		//parseAndPopulate("bn", null);
		//parseAndPopulate("hpb", null);
		//parseAndPopulate("kohls", null);
		//parseAndPopulate("homedepot",null);
		//parseAndPopulate("lowes",null);
		//parseAndPopulate("nike",null);
		//parseAndPopulate("urban",null);
		//parseAndPopulate("prettylittlething",null);
		//parseAndPopulate("topshop",null);
		//parseAndPopulate("gymshark",null);
		//parseAndPopulate("revolve",null);
		//parseAndPopulate("revolve-men",null);
		//parseAndPopulate("charmingcharlie",null);
		//parseAndPopulate("bathandbodyworks",null);
		//parseAndPopulate("newegg",null);
		//parseAndPopulate("agjeans",null);
		//parseAndPopulate("allsaints",null);
		//parseAndPopulate("dockers",null);
		parseAndPopulate("levi",null);
		//parseAndPopulate("laura",null);
		//parseAndPopulate("princesspolly",null);


		//searchByScrapping(null, "t shirts");
		//parseAndPopulate("urban-search","t shirts");
		//parseAndPopulate("prettylittlething-search","t shirts");
		//parseAndPopulate("gymshark-search","t shirts");
		//parseAndPopulate("revolve-search","t shirts");
		//parseAndPopulate("newegg-search","samsung tv");
		//parseAndPopulate("zappos-search","steel cut boots");
		//parseAndPopulate("nordstrom-search","t shirts"); // Didn't work
		//parseAndPopulate("bn-search","Harry Potter");
		//parseAndPopulate("hpb-search","Harry Potter"); // Didn't work
		//parseAndPopulate("topshop-search","t shirts");

		// failed brands
		// Adidas, Costco, Macys, Lululemon, Hollister
		// brands loading via javascript
		// wayfair, oldnavy, gap, garage, bhphotovideo

	}
	
	
	private static List<ProductModel> performMultipleAttempts(JSONObject mVendorObj, String aAppendToUrl){
		List<ProductModel> mVendorList = null;
		for(int i = 0; i < NUM_OF_ATTEMPTS; i++) {
			
			mVendorList = scrapIt(mVendorObj, aAppendToUrl);
			if(mVendorList != null && mVendorList.size() == 0) {
				// try one more time 
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
				}
				System.out.println("@@@@@@ACTEST going to make "+(i+1)+" attempt to scrap the page...."+mVendorObj.getString("vendor-name"));
			}
			else {
				break;
			}
			
			
		}
		return mVendorList;
	}
	

	public static List<ProductModel> parseAndPopulate(String aVendorKey, String aAppendToUrl) {
		List<ProductModel> mList = new ArrayList<>();
		String mInputFileName = "/scrapper-inputs.json";
		

		
		
		//JSONObject mObj = new JSONObject(readDataFromFile("src/main/resources/scrapper-inputs.json"));
		JSONObject mObj = new JSONObject(readDataFromFile(mInputFileName));
		JSONArray mArr = mObj.getJSONArray("vendors");
		
		List<JSONObject> mVendorConfigList = new ArrayList<>();
		for (int i = 0; i < mArr.length(); i++) {
			JSONObject mVendorObj = mArr.getJSONObject(i);

			if (aVendorKey != null && !aVendorKey.equalsIgnoreCase(mVendorObj.getString("vendor-name"))) {
				continue;
			}
			String mIgnore = null;
			if(mVendorObj.has("ignore-for-data-collection")) {
				mIgnore = mVendorObj.getString("ignore-for-data-collection");
			}

			if(aVendorKey == null && mIgnore != null && mIgnore.equals("true")) {
				continue;
			}
			mVendorConfigList.add(mVendorObj);

		}		
		
		
		mVendorConfigList.parallelStream().forEach((mVendorObj) -> {
			// System.out.println("@@@@ACTEST ==>"+mVendorObj.toString());
			List<ProductModel> mVendorList = performMultipleAttempts(mVendorObj, aAppendToUrl);
			mList.addAll(mVendorList);			
		});
		
		System.out.println("@@@@ACTEST mList.size():"+mList.size());
		
		
		
//		for (int i = 0; i < mArr.length(); i++) {
//			JSONObject mVendorObj = mArr.getJSONObject(i);
//
//			if (aVendorKey != null && !aVendorKey.equalsIgnoreCase(mVendorObj.getString("vendor-name"))) {
//				continue;
//			}
//			String mIgnore = null;
//			if(mVendorObj.has("ignore-for-data-collection")) {
//				mIgnore = mVendorObj.getString("ignore-for-data-collection");
//			}
//
//			if(aVendorKey == null && mIgnore != null && mIgnore.equals("true")) {
//				continue;
//			}
//			
//
//			// System.out.println("@@@@ACTEST ==>"+mVendorObj.toString());
//			List<ProductModel> mVendorList = scrapIt(mVendorObj, aAppendToUrl);
//			mList.addAll(mVendorList);
//		}
		
		
		
		return mList;

	}



	public static List<ProductModel> searchByScrapping(String aVendorKey, String aAppendToUrl) {
		List<ProductModel> mList = new ArrayList<>();
		String mInputFileName = "/scrapper-inputs.json";

		//JSONObject mObj = new JSONObject(readDataFromFile("src/main/resources/scrapper-inputs.json"));
		JSONObject mObj = new JSONObject(readDataFromFile(mInputFileName));
		JSONArray mArr = mObj.getJSONArray("vendors");

		List<JSONObject> mVendorConfigList = new ArrayList<>();
		for (int i = 0; i < mArr.length(); i++) {
			JSONObject mVendorObj = mArr.getJSONObject(i);

			if (aVendorKey != null && !aVendorKey.equalsIgnoreCase(mVendorObj.getString("vendor-name"))) {
				continue;
			}


			String mSearchFlag = null;
			if(mVendorObj.has("search-flag")) {
				mSearchFlag = mVendorObj.getString("search-flag");
			}

			if(aVendorKey == null && mSearchFlag != null && mSearchFlag.equals("true")) {
				mVendorConfigList.add(mVendorObj);
			}
		}
		int NUM_OF_SEARCH_ITEMS_LIMIT = 10;

		mVendorConfigList.parallelStream().forEach((mVendorObj) -> {
			// System.out.println("@@@@ACTEST ==>"+mVendorObj.toString());
			List<ProductModel> mVendorList = performMultipleAttempts(mVendorObj, aAppendToUrl);

			if(mVendorList != null && mVendorList.size() > NUM_OF_SEARCH_ITEMS_LIMIT){
				mList.addAll(mVendorList.subList(0,NUM_OF_SEARCH_ITEMS_LIMIT));
			}
			else{
				mList.addAll(mVendorList);
			}


		});

		System.out.println("@@@@ACTEST search from generic scrapper - mList.size():"+mList.size());



//		for (int i = 0; i < mArr.length(); i++) {
//			JSONObject mVendorObj = mArr.getJSONObject(i);
//
//			if (aVendorKey != null && !aVendorKey.equalsIgnoreCase(mVendorObj.getString("vendor-name"))) {
//				continue;
//			}
//			String mIgnore = null;
//			if(mVendorObj.has("ignore-for-data-collection")) {
//				mIgnore = mVendorObj.getString("ignore-for-data-collection");
//			}
//
//			if(aVendorKey == null && mIgnore != null && mIgnore.equals("true")) {
//				continue;
//			}
//
//
//			// System.out.println("@@@@ACTEST ==>"+mVendorObj.toString());
//			List<ProductModel> mVendorList = scrapIt(mVendorObj, aAppendToUrl);
//			mList.addAll(mVendorList);
//		}



		return mList;

	}



	private static String readDataFromFile(String aCsvFile) {
		StringBuilder sb = new StringBuilder();
		System.out.println("@@ACTEST aCsvFile:"+aCsvFile);

		try {
			BufferedReader mReader = new BufferedReader(new InputStreamReader(GenericScrapper.class.getResourceAsStream(aCsvFile)));

			//BufferedReader mReader = new BufferedReader(new FileReader(aCsvFile));
			String mLine = null;
			while ((mLine = mReader.readLine()) != null) {
				sb.append(mLine);
			}
			mReader.close();
		} catch (Exception mExcep) {
			mExcep.printStackTrace();
		}

		return sb.toString();
	}

	private static Object getDataObject(String aKey, Object aDoc, JSONObject aObj) {
		// First read from JSON regarding what needs to be done.
		JSONArray mArr = aObj.getJSONArray(aKey);
		Object mObjToInvokeMethodOn = aDoc;

		for (int i = 0; i < mArr.length(); i++) {
			JSONObject mObj = (JSONObject) mArr.get(i);
			String mMethodName = mObj.getString("method-name");
			String mArgumentVal = mObj.getString("argument-val");
			String mReturnObjectType = mObj.getString("return-object-type");
			mObjToInvokeMethodOn = invokeMethod(mObjToInvokeMethodOn, mMethodName, mArgumentVal, mReturnObjectType);
		}
		return mObjToInvokeMethodOn;
	}

	private static Object invokeMethod(Object aObj, String aMethodName, String aArgumentVal, String aReturnObjectType) {
		Object mReturnObj = null;
		// System.out.println("aObj:["+aObj.getClass().getName()+"],aMethodName:["+aMethodName+"],
		// aArgumentVal:["+aArgumentVal+"], aReturnObjectType:["+aReturnObjectType+"]");

		if (aObj instanceof Document) {
			if (aMethodName.equalsIgnoreCase("select")) {
				mReturnObj = ((Document) aObj).select(aArgumentVal);
			}
			else if (aMethodName.equalsIgnoreCase("selectFirst")) {
				mReturnObj = ((Element) aObj).selectFirst(aArgumentVal);
			}
		} else if (aObj instanceof Elements) {
			if (aMethodName.equalsIgnoreCase("select")) {
				mReturnObj = ((Elements) aObj).select(aArgumentVal);
			} else if (aMethodName.equalsIgnoreCase("attr")) {
				mReturnObj = ((Elements) aObj).attr(aArgumentVal);
			}
		} else if (aObj instanceof Element) {
			if (aMethodName.equalsIgnoreCase("selectFirst")) {
				mReturnObj = ((Element) aObj).selectFirst(aArgumentVal);
			} else if (aMethodName.equalsIgnoreCase("text")) {
				mReturnObj = ((Element) aObj).text();
			} else if (aMethodName.equalsIgnoreCase("absUrl")) {
				mReturnObj = ((Element) aObj).absUrl(aArgumentVal);
			} else if (aMethodName.equalsIgnoreCase("attr")) {
				mReturnObj = ((Element) aObj).attr(aArgumentVal);
			} else if (aMethodName.equalsIgnoreCase("select")) {
				mReturnObj = ((Element) aObj).select(aArgumentVal);
			}
		}

		switch (aReturnObjectType) {
		case "Elements":
			return (Elements) mReturnObj;
		case "Element":
			return (Element) mReturnObj;
		case "String":
			return (String) mReturnObj;

		default:
			return mReturnObj;
		}
	}

	public static List<ProductModel> scrapIt(JSONObject aObj, String aAppendToUrl) {
		// first load all the information needed to scrap a page.
		if (aAppendToUrl == null) {
			aAppendToUrl = "";
		}
		else{
			aAppendToUrl = aAppendToUrl.replaceAll(" ","%20");
		}

		List<ProductModel> mListOfProducts = new ArrayList<ProductModel>();
		int mTimeoutInSeconds = 50;
		
		if(aObj.has("timeout")) {
			try {
				mTimeoutInSeconds = Integer.parseInt(aObj.getString("timeout"));
			} 
			catch (Exception e) {
			} 
		}

		boolean mIsDetailLevelNeeded = false;
		if(aObj.has("dig-detail")){
			if(aObj.getString("dig-detail").equals("true")){
				mIsDetailLevelNeeded = true;
			}
		}


		//System.out.println("@@@ACTEST mTimeoutInSeconds:"+mTimeoutInSeconds);
		String mUrl = null;

		try {


			Document doc = null;
			boolean mIsProxyFailed = false;

			mUrl = aObj.getString("url");

			if(mUrl != null && mUrl.contains("XXXREPLACEMEXXX")){
				mUrl = mUrl.replaceAll("XXXREPLACEMEXXX", aAppendToUrl);
				aAppendToUrl = "";
			}

			System.out.println("@@@@@@@Going to scrap:" + mUrl + aAppendToUrl);

			// first try with proxy

			try{
				doc = Jsoup.connect(mUrl + aAppendToUrl)
						.proxy(PROXY_SERVER, PROXY_PORT) // sets a HTTP proxy
						.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36")
						.header("Accept-Encoding", "gzip, deflate")
						.referrer(aObj.getString("referrer"))
						.maxBodySize(0)
						//.cookie("i18n-prefs", "USD")
						.ignoreHttpErrors(true)
						.followRedirects(true)
						.timeout(mTimeoutInSeconds*1000)
						.method(Connection.Method.GET).get();

			}
			catch(Exception mExcep){
				mIsProxyFailed = true;
				System.out.println("Proxy exceptio:"+mExcep.getMessage());
			}

			if(doc == null){
				mIsProxyFailed = true;
			}

			if(mIsProxyFailed){
				System.out.println("@@@@@ACTEST PROXY FAILED - going to pull data directly...");
			}
			else{
				System.out.println("@@@@@ACTEST PROXY SUCCESS...");
			}


			if(mIsProxyFailed){
				doc = Jsoup.connect(mUrl + aAppendToUrl)
						.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36")
						.header("Accept-Encoding", "gzip, deflate")
						.referrer(aObj.getString("referrer"))
						.maxBodySize(0)
						//.cookie("i18n-prefs", "USD")
						.ignoreHttpErrors(true)
						.followRedirects(true)
						.timeout(mTimeoutInSeconds*1000)
						.method(Connection.Method.GET).get();
			}







			//System.out.println("@@@ACTEST doc.html===>> " + doc.html());


			Elements products = (Elements) getDataObject("list", doc, aObj);
			System.out.println("@@@ACTEST number of products found===>> " + products.size());
			//System.out.println("@@@ACTEST number of products found===>> " + products.toString());
			boolean mTest = false;;

			for (Element mProduct : products) {
				if(mTest) {
					System.out.println("@@@ACTEST mProduct===>> " + mProduct.toString());
				}
				

				
				String mProductName = null;
				String mProductPrice = null;
				String mProductUrl = null;
				String mProductImageUrl = null;
				String mReviewAvg = null;
				String mReviewCount = null;
				String mProductSalePrice = null;

				try {
					// mProductName = mProduct.selectFirst(".a-text-normal").text();
					// mProductName = ((Element)constructAndInvokeMethodObj(mProduct, "selectFirst",
					// ".a-text-normal")).text();
					// mProductUrl =
					// mProduct.selectFirst("[data-component-type=\"s-product-image\"]").selectFirst(".a-link-normal").absUrl("href");
					// mProductImageUrl =
					// mProduct.selectFirst("[data-component-type=\"s-product-image\"]").select("img").attr("src");
					try {
						mProductName = (String) getDataObject("product-name", mProduct, aObj);
					} catch (Exception e) {
						e.printStackTrace();
					}

					try {
						mProductPrice = (String) getDataObject("product-price", mProduct, aObj);
					} catch (Exception e) {
					}

					try {
						mProductSalePrice = (String) getDataObject("product-sale-price", mProduct, aObj);
					} catch (Exception e) {
					}

					if (mProductSalePrice == null) {
						// mark it same as product price
						mProductSalePrice = mProductPrice;
					}

					try {
						mProductUrl = (String) getDataObject("product-url", mProduct, aObj);
					} catch (Exception e) {
					}

					try {
						mProductImageUrl = (String) getDataObject("product-image-url", mProduct, aObj);
						//System.out.println("@@@@@ACTEST mProductImageUrl:"+mProductImageUrl);
					} catch (Exception e) {
						e.printStackTrace();
					}
					// mReviewAvg = getReviewAverage(mProduct);
					// mReviewCount = getReviewCount(mProduct);

					// String mPrice = mProductPrice.replaceAll("\\$","");
					ProductModel mProductObj = new ProductModel(mProductPrice, mProductName, mProductImageUrl,
							mProductUrl, mProductSalePrice, aObj.getString("source"), mReviewAvg, mReviewCount,
							aObj.getString("category"), null);
					mListOfProducts.add(mProductObj);
					System.out.println(mProductObj.toString());
					
					
					if(mTest) {
						break;
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}

				// System.out.println("["+mProductName+"] ["+mProductPrice+"] ["+mProductUrl+"]
				// ["+mProductImageUrl+"]");

			}


		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("@@@@@@@ Error while scrapping:"+ (aObj.getString("url"))+"] message:"+e.getMessage());
		}

		if(mIsDetailLevelNeeded){
			scrapDetail(aObj,mListOfProducts);
		}

		System.out.println("@@@@@@@completed scrapping:" + (mUrl) + aAppendToUrl+"] - Found following number of items:["+mListOfProducts.size()+"]");

		return mListOfProducts;

	}

	private static void  scrapDetail(JSONObject aObj, List<ProductModel> aList){
		int mMaxCount = 100;
		int mCount = 0;

		int mTimeoutInSeconds = 3;

//		if(aObj.has("timeout")) {
//			try {
//				mTimeoutInSeconds = Integer.parseInt(aObj.getString("timeout"));
//			}
//			catch (Exception e) {
//			}
//		}


		for(ProductModel mModel:aList){
			mCount++;
			if(mCount > mMaxCount){
				break;
			}
			try {
				String mDetailUrl = mModel.getDestUrl();
				Thread.sleep(1000*3);


				Document doc = null;
				boolean mIsProxyFailed = false;



				System.out.println("@@@@@@@ACTEST Going to scrap detail:" + mDetailUrl);

				// first try with proxy

				try{
					doc = Jsoup.connect(mDetailUrl)
							.proxy(PROXY_SERVER, PROXY_PORT) // sets a HTTP proxy
							.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36")
							.header("Accept-Encoding", "gzip, deflate")
							.referrer(aObj.getString("referrer"))
							.maxBodySize(0)
							//.cookie("i18n-prefs", "USD")
							.ignoreHttpErrors(true)
							.followRedirects(true)
							.timeout(mTimeoutInSeconds*1000)
							.method(Connection.Method.GET).get();

				}
				catch(Exception mExcep){
					mIsProxyFailed = true;
					System.out.println("Proxy exceptio:"+mExcep.getMessage());
				}

				if(doc == null){
					mIsProxyFailed = true;
				}

				if(mIsProxyFailed){
					System.out.println("@@@@@ACTEST PROXY FAILED - going to pull data directly...");
				}
				else{
					System.out.println("@@@@@ACTEST PROXY SUCCESS...");
				}


				if(mIsProxyFailed){
					doc = Jsoup.connect(mDetailUrl)
							.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36")
							.header("Accept-Encoding", "gzip, deflate")
							.referrer(aObj.getString("referrer"))
							.maxBodySize(0)
							//.cookie("i18n-prefs", "USD")
							.ignoreHttpErrors(true)
							.followRedirects(true)
							.timeout(mTimeoutInSeconds*1000)
							.method(Connection.Method.GET).get();
				}


				String mProductImageUrl = null;

				//System.out.println("detail doc:"+doc.html());


				try {
					mProductImageUrl = (String) getDataObject("detail-product-image-url", doc, aObj);
					System.out.println("@@@@@ACTEST mProductImageUrl:"+mProductImageUrl);
				} catch (Exception e) {
					//e.printStackTrace();
				}

				if(mProductImageUrl != null){
					mModel.setUrl(mProductImageUrl);
				}
				System.out.println(mModel.toString());


			}
			catch (Exception e) {
				System.out.println("error occurred while doing detail scrapping:"+e.getMessage());
			}


		}

	}


}
