package com.giftcard.scrappers;

import com.giftcard.deals.model.*;
import com.google.common.collect.Lists;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


public class AmazonScraper extends AbstractScraper {
	public static final String BASE_URL = "https://www.amazon.com/gp/goldbox#best";
	public static final String REST_API = "https://www.amazon.com/xa/dealcontent/v2/GetDeals";
	private static  final String AWS_ADD_TO_CART_URL = "https://www.amazon.com/gp/aws/cart/add.html?";
	private static final String PURVICE_TAG = "purvice-20"; // This is hardcoded elsewhere - fix it
	
	public static final int NUM_PER_PAGE = 10;
	public static final int MAX_PAGE = 10;
	
	public static final ObjectMapper OBJ_MAPPER = new ObjectMapper();

	public String regexCapture(String text, String regexStr, int group) {
		String ResultString = null;
		try {
			Pattern regex = Pattern.compile(regexStr);
			Matcher regexMatcher = regex.matcher(text);
			if (regexMatcher.find()) {
				if(group == 0)  {
					ResultString = regexMatcher.group();
				} else
					ResultString = regexMatcher.group(group);
			} 
		} catch (PatternSyntaxException ex) {
			// Syntax error in the regular expression
			ex.printStackTrace();
		}
		return ResultString;
	}
	
	public List<Product> pageScraping(String responseBody) throws IOException	{
		
		List<Product> products = new ArrayList<Product>();

		Product product;
		String productName, productUrl, imageUrl, category, customerReviewAverage, customerReviewCount = "";
		Double saleprice, baseprice;

		try {
			JsonNode root = new ObjectMapper().readTree(responseBody);
			
			Iterator<JsonNode> iterator = root.findValue("dealDetails").iterator();
			JsonNode productNode;
			String textPrice;
			while(iterator.hasNext()) {
				productNode = iterator.next();
				//priceNode = productNode.get("priceSummary");

				//System.out.println("@@@ACTEST productNode:"+productNode.toString());

				//category = productNode.get("itemType").asText();
				productName = productNode.get("title").asText();
				imageUrl = productNode.get("primaryImage").asText();
				productUrl = productNode.get("egressUrl").asText();

				textPrice = productNode.get("minDealPrice").asText();
				saleprice = (textPrice.isEmpty()) ? 0.0 : JsoupUtil.parsePrice(textPrice);

				textPrice = productNode.get("minListPrice").asText();
				baseprice = (textPrice.isEmpty()) ? 0.0 : JsoupUtil.parsePrice(textPrice);
				
				String str = productNode.get("glProductGroup").asText();
				category = str.replaceFirst("gl_", "");   
				customerReviewAverage = productNode.get("reviewRating").asText();
				customerReviewCount = productNode.get("totalReviews").asText();


				String mAddToCartUrl = null;

				if(productNode.has("impressionAsin")){
					String mAsin = productNode.get("impressionAsin").asText();

					if(mAsin != null && !mAsin.equals("")){
						mAddToCartUrl = AWS_ADD_TO_CART_URL+ "&ASIN.1="+mAsin+"&Quantity.1=1&AssociateTag="+PURVICE_TAG;
					}

				}

				//System.out.println("@@@ACTEST mAddToCartUrl:"+mAddToCartUrl);

				product = new Product(category, productUrl, productName, imageUrl, baseprice, saleprice,customerReviewAverage,customerReviewCount, mAddToCartUrl);

				System.out.println(product);
				products.add(product);
			}
		} catch(IOException e) {
			e.printStackTrace();
		}

		return products;
		
	}
	
	@Override
	public List<Product> productScraping() {
		List<Product> products = new ArrayList<Product>();
		Document doc = JsoupUtil.getDocument(BASE_URL);
		if(doc != null) {
			String marketplaceID = regexCapture(doc.html(), "\"marketplaceId\" : \"(.*?)\"", 1);
			
			AmazonMetadata metadata = new AmazonMetadata(marketplaceID, "goldbox_mobile_pc");
			
			String json = regexCapture(doc.html().replaceAll("\n", " "), "\\\"sortedDealIDs\\\"\\s*:\\s*(.*)?]", 1);
			json = json.replaceAll("\\s+", " ") + "]";
			
			try {
				String[] dealIDs = OBJ_MAPPER.readValue(json, String[].class);
				List<AmazonDeal> deals = new ArrayList<AmazonDeal>();
				for(String dealID : dealIDs) {
					deals.add(new AmazonDeal(dealID));
				}
				
				int urlPerThreads = Math.round((float)deals.size() / NUM_PER_PAGE);

				List<List<AmazonDeal>> smallerLists = Lists.partition(deals, urlPerThreads);
				
				int count = 0;
				AmazonPayload payload = new AmazonPayload();
				payload.setRequestMetadata(metadata);
				String responseBody = "";
				while(true) {
					if(count >= MAX_PAGE) {
						break;
					}
					
					try {
						payload.setDealTargets(smallerLists.get(count));
						
						responseBody = RestClient.post(REST_API, OBJ_MAPPER.writeValueAsString(payload));
						products.addAll(pageScraping(responseBody));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					count++;
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return products;
	}


	public static void main(String[] args) throws IOException {
		AbstractScraper sp = new AmazonScraper();
		List<Product> products = sp.productScraping();
		sp.writeOutput(products, "amazon_output.txt");
	}
}

