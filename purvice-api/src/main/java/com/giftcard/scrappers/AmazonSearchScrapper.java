package com.giftcard.scrappers;

import com.giftcard.deals.model.ProductModel;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AmazonSearchScrapper {
    //public static final String BASE_URL = "https://www.amazon.com/s?ref=nb_sb_noss&k=";
	public static final String BASE_URL = "https://www.amazon.com/s?ref=sr_nr_p_6_3&k=";

    public static List<ProductModel> productSearchUsingScraping(String aKeyword) {
       List<ProductModel> mListOfProducts = new ArrayList<ProductModel>();

        try {

			Document doc = null;
			boolean mIsProxyFailed = false;

			// first try with proxy
			try{
				doc = Jsoup.connect(BASE_URL+aKeyword)
						.proxy(GenericScrapper.PROXY_SERVER, GenericScrapper.PROXY_PORT) // sets a HTTP proxy
						.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36")
						.referrer("")
						.maxBodySize(0)
						.cookie("i18n-prefs", "USD")
						.referrer("https://www.amazon.com")
						.timeout(50000)
						.method(Method.GET)
						.get();


			}
			catch(Exception mExcep){
				mIsProxyFailed = true;
				System.out.println("Proxy exceptio:"+mExcep.getMessage());
			}

			if(doc == null){
				mIsProxyFailed = true;
			}

			if(mIsProxyFailed){
				System.out.println("@@@@@ACTEST PROXY FAILED - going to pull data directly...");
			}
			else{
				System.out.println("@@@@@ACTEST PROXY SUCCESS...");
			}


			if(mIsProxyFailed){
				doc = Jsoup.connect(BASE_URL+aKeyword)
						.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36")
						.referrer("")
						.maxBodySize(0)
						.cookie("i18n-prefs", "USD")
						.referrer("https://www.amazon.com")
						.timeout(50000)
						.method(Method.GET)
						.get();
			}




          
            Elements products = doc.select("[data-component-type=\"s-search-results\"]").select("[data-asin]");
            //Elements products = doc.select(".s-result-list-parent-container > ul > li .s-access-title");

            System.out.println("@@@ACTEST ===>> "+products.size());

            for(Element mProduct: products){
                String mProductName = null;
                String mProductPrice = null;
                String mProductUrl = null;
                String mProductImageUrl = null;
                String mReviewAvg = null;
                String mReviewCount = null;
                String mProductCategoryPath = "";
                try {
					mProductName =  mProduct.selectFirst(".a-text-normal").text();
					mProductPrice = mProduct.selectFirst(".a-price").selectFirst(".a-offscreen").text();
					mProductUrl = mProduct.selectFirst("[data-component-type=\"s-product-image\"]").selectFirst(".a-link-normal").absUrl("href");
					mProductImageUrl = mProduct.selectFirst("[data-component-type=\"s-product-image\"]").select("img").attr("src");
					// multiple calls to amazon might trigger ip blocking - so lets
					//mProductCategoryPath = getCategoryNameFromProductURL(mProductUrl);
					mReviewAvg = getReviewAverage(mProduct);
					mReviewCount = getReviewCount(mProduct);

					//String mPrice = mProductPrice.replaceAll("\\$","");
					ProductModel mProductObj = new ProductModel(mProductPrice,mProductName,mProductImageUrl,mProductUrl,mProductPrice,"amazon.com",mReviewAvg,mReviewCount, mProductCategoryPath, null);
					mListOfProducts.add(mProductObj);
					System.out.println(mProductObj.toString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					
				}
                
                
                //System.out.println("["+mProductName+"]      ["+mProductPrice+"]     ["+mProductUrl+"]       ["+mProductImageUrl+"]");

            }


        } catch (IOException e) {
            e.printStackTrace();
        }

      return mListOfProducts;
    }
    
    private static String getCategoryNameFromProductURL(String url) throws IOException{
    	  Document doc = Jsoup.connect(url)
                  .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36")
                  .referrer("")
                  .maxBodySize(0)
                  .cookie("i18n-prefs", "USD")
                  .referrer("https://www.amazon.com")
                  .timeout(50000)
                  .method(Method.GET)
                  .get();
        
          Elements categories = doc.select("[data-feature-name=\"wayfinding-breadcrumbs\"]").select(".a-unordered-list .a-list-item .a-link-normal");
          
          StringBuilder str = new StringBuilder();
          
          for(Element category: categories){
        	  str.append(category.text());
        	  str.append("|");
          }
          return str.toString();
    	
    }
    
    private static String getReviewAverage(Element mProduct) {
    	String mReviewAvg = null;
		try {
			mReviewAvg = mProduct.selectFirst(".a-icon-star-small").selectFirst(".a-icon-alt").text();
			if(mReviewAvg != null && mReviewAvg.contains(" out of 5 stars")) {
				mReviewAvg = mReviewAvg.replace(" out of 5 stars", "");
			}
		} catch (Exception e1) {

		}
		
    	return mReviewAvg;
    }
    
    private static String getReviewCount(Element mProduct) {
		// we will try to pull total reviews from the complete text line
		// sample line: @@@ACTEST ===>> Yajuhoy iPhone 8 Plus Case/iPhone 7 Plus Case, Liquid Silicone Gel Rubber Case Soft Microfiber Cloth Lining Cushion Compatible with Apple iPhone 8 Plus (2017) / iPhone 7 Plus (2016) - Yellow by Yajuhoy 4.2 out of 5 stars 403 $10.99$10.99 Get it as soon as Tomorrow, Mar 7 FREE Shipping on orders over $25 shipped by Amazon
    	String mReviewCount = null;
    	
		try {
			String mTmpCompleteTxt = mProduct.text();
			if(mTmpCompleteTxt != null && mTmpCompleteTxt.contains(" out of 5 stars ")) {
				String mTmpArr[] = mTmpCompleteTxt.split(" out of 5 stars ");
				if(mTmpArr.length >=  1) {
					String mTmpStr = mTmpArr[1];
					// Now we have the string starting with review count. Lets split on space.
					mReviewCount = mTmpStr.split(" ")[0];
				}
			}
		} catch (Exception e) {
		}
		
		return mReviewCount;
		    	
    }

    public static void main(String[] args) throws IOException {
        AmazonSearchScrapper sp = new AmazonSearchScrapper();
        List<ProductModel> products = sp.productSearchUsingScraping("apple iphone yellow");
    	
    }
}
