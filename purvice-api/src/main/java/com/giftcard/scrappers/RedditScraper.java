package com.giftcard.scrappers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.giftcard.deals.model.ForumProduct;
import com.giftcard.deals.model.Product;



public class RedditScraper extends AbstractScraper {
	public static final String BASE_URL = "https://www.reddit.com/r/deals";
	public static final String REDDIT_URL_PAGINATION = "https://www.reddit.com/r/deals/?count=25&after=%s";
	public static final int MAX_PAGE = 3;
	
	protected String nextUrl = BASE_URL;

	public List<Product> pageScraping(String nextPage) {
		List<Product> products = new ArrayList<Product>();

		Document doc = null;

		Product product;
		String productDescription, imageUrl, productUrl;
		try {
			doc = Jsoup.connect(nextPage).timeout(50000).get();
			
			//Elements media = doc.select("img[src*=b.thumbs.redditmedia.com]");
			Elements entries = doc.select("div[class=entry unvoted]");
			for(Element entry : entries) {
				
				Element img = entry.previousElementSibling();
				Element name = entry.select("p.title a").first();
				
				productUrl = name.absUrl("href");
				imageUrl = img.childNodeSize() == 0 ? null : img.child(0).absUrl("src");
				productDescription = name.text();
				
				product = new ForumProduct(productDescription, imageUrl, productUrl);
				products.add(product);
				//System.out.println(product.toString());
				
				//System.out.println("#####################");
			}
			
			nextUrl = String.format(REDDIT_URL_PAGINATION, entries.last().parent().attr("data-fullname"));
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return products;
	}
	
	@Override
	public List<Product> productScraping() {
		
		List<Product> products = new ArrayList<Product>();

		int count = 1;
		
		while(true) {
			if(count >= MAX_PAGE) {
				break;
			}
			
			products.addAll(pageScraping(nextUrl));
			count++;
		}
		return products;
	}

	public static void main(String[] args) throws IOException {
		RedditScraper scp = new RedditScraper();
		List<Product> products = scp.productScraping();
		scp.writeOutput(products, "reddit_output.txt");
	}
}
