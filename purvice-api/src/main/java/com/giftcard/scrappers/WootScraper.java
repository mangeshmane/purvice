package com.giftcard.scrappers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.giftcard.deals.model.ForumProduct;
import com.giftcard.deals.model.Product;

public class WootScraper extends AbstractScraper {
	
	public static final String BASE_URL = "http://deals.woot.com";
	public static final String WOOT_URL_PAGINATION = "http://deals.woot.com/?page=%d";
	public static final int MAX_PAGE = 2;
	
	protected String nextUrl = BASE_URL;

	public static void main(String[] args) throws IOException {
		AbstractScraper scp = new WootScraper();
		List<Product> products = scp.productScraping();
		scp.writeOutput(products, "woot_output.txt");
	}
	
	private List<Product> pageScraping(String nextPage) {
		List<Product> products = new ArrayList<Product>();

		Document doc = null;

		Product product;
		String productDescription, category, imageUrl, productUrl;
		try {
			doc = Jsoup.connect(nextPage).get();
			
			Elements entries = doc.select("div#deals div[class=postInfo sale]");
			for(Element entry : entries) {
				
				if(entry.parent().className().endsWith("expired")) {
					continue;
				}
				
				Element img = entry.select("img").first();
				Element name = entry.select("h3").first();
				
				productUrl = name.select("a").first().absUrl("href");
				imageUrl = img == null ? null : img.absUrl("src");
				productDescription = name.text();
				
				Elements cate = entry.select("a[class=category]");
				category = cate != null ? cate.text().replaceAll(" ", ",") : "";
				
				product = new ForumProduct(productDescription, imageUrl, productUrl);
				product.setCategory(category);
				products.add(product);
				System.out.println(product.toString());
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return products;
	}

	@Override
	public List<Product> productScraping() {
		List<Product> products = new ArrayList<Product>();

		int count = 1;
		
		while(true) {
			if(count > MAX_PAGE) {
				break;
			}
			nextUrl = String.format(WOOT_URL_PAGINATION, count);
			products.addAll(pageScraping(nextUrl));
			count++;
		}
		return products;
	}

}
