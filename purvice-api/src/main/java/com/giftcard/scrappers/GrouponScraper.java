package com.giftcard.scrappers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.giftcard.deals.model.JsoupUtil;
import com.giftcard.deals.model.Product;



public class GrouponScraper extends AbstractScraper {

	public static final String BASE_URL = "https://www.groupon.com/browse/chicago";
	public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36";

	@Override
	public List<Product> productScraping() {
		List<Product> products = new ArrayList<Product>();

		Document doc = null;

		Product product;
		String productName, imageUrl, productUrl;
		Double saleprice, baseprice;
		try {
			doc = Jsoup
					.connect(BASE_URL)
					//.header("Cookie", "CID=US_DTI_0_0_0_0; division=chicago")
					.userAgent(USER_AGENT)
					.get();

			//System.out.println(doc.html());

			Elements navigate = doc.select("div#pull-cards figure");
			if(navigate != null) {

				for(Element node : navigate) {
				    //System.out.println(node);

					Element image = node.select("div.image-container img").first();
					Element name = node.select("p.deal-title ").first();
					Element link = node.select("div > a").first();
					Element basePrice = node.select("s.original-price").first();
					Element salePrice = node.select("span.discount-price ").first();
					

					//System.out.println(image.absUrl("data-original"));
					//System.out.println(name.text());
					//System.out.println(basePrice.text());
					//System.out.println(salePrice.text());
					/*if(name == null) {
					    productName = link.text();
					} else {
					    productName = name.text();
					}*/
					
					//System.out.println(image);
					//System.out.println(link);
					
					if(name == null) {
					    continue;
					}
					productName = name.text();
					imageUrl = image.absUrl("data-original");
					saleprice = JsoupUtil.parsePrice(salePrice.text());
					baseprice = (basePrice != null) ? JsoupUtil.parsePrice(basePrice.text()) : saleprice;
					productUrl = link.absUrl("href");

					product = new Product("", productUrl, productName, imageUrl, baseprice, saleprice);
					System.out.println(product.toString());
					products.add(product);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return products;
	}

	public static void main(String[] args) throws IOException {
		AbstractScraper scp = new GrouponScraper();
		List<Product> products = scp.productScraping();
		scp.writeOutput(products, "groupon_output.txt");
	}
}
