package com.giftcard.scrappers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.giftcard.deals.model.JsoupUtil;
import com.giftcard.deals.model.Product;

public class WalmartScraper extends AbstractScraper {
	
	public static final String BASE_URL = "http://www.walmart.com/";
	public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36";

	@Override
	public List<Product> productScraping() {
		
		List<Product> products = new ArrayList<Product>();

		Document doc = null;

		Product product;
		String productName, imageUrl, productUrl;
		Double saleprice, baseprice;
		try {
			doc = Jsoup
					.connect(BASE_URL)
					//.header("Cookie", "CID=US_DTI_0_0_0_0; division=chicago")
					.userAgent(USER_AGENT)
					.get();
			
			Elements navigate = doc.getElementsByClass("Tile-content");
			
			//System.out.println(doc.html());
			for(Element node : navigate) {
				Element salePrice = node.select("div div").first();
				
				if(salePrice == null) {
					break;
				}
				
				Elements basePrice = node.select("div.old-price");
				Element name = node.select("a").first();
				Element root = name.parent().parent();
				Element image = root.child(2);
				
				productName = name.text();
				imageUrl = image.absUrl("src");
				productUrl = root.child(0).absUrl("href");
				saleprice = JsoupUtil.parsePrice(salePrice.text());
				baseprice = (basePrice == null || !basePrice.hasText()) ? saleprice : JsoupUtil.parsePrice(basePrice.text());

				product = new Product("", productUrl, productName, imageUrl, baseprice, saleprice);
				System.out.println(product.toString());
				products.add(product);
			}
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		return products;
	}

	public static void main(String[] args) throws IOException {
		AbstractScraper scp = new WalmartScraper();
		List<Product> products = scp.productScraping();
		scp.writeOutput(products, "walmart_output.txt");
	}
}
