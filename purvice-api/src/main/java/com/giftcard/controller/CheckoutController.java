package com.giftcard.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.CreditCard;
import com.braintreegateway.Customer;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.Transaction.Status;
import com.braintreegateway.TransactionRequest;
import com.braintreegateway.ValidationError;
import com.braintreegateway.exceptions.ServerException;
import com.giftcard.Application;
import com.giftcard.properties.AppProperties;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.UserService;

@RestController
@RequestMapping(value = "/braintree")
public class CheckoutController {

	private BraintreeGateway gateway = Application.gateway;

	@Autowired
	private UserService userService;
	
	@Autowired
	private AppProperties appProperties;

	private static final Logger logger = LoggerFactory.getLogger(CheckoutController.class);

	private Status[] TRANSACTION_SUCCESS_STATUSES = new Status[] { Transaction.Status.AUTHORIZED,
			Transaction.Status.AUTHORIZING, Transaction.Status.SETTLED, Transaction.Status.SETTLEMENT_CONFIRMED,
			Transaction.Status.SETTLEMENT_PENDING, Transaction.Status.SETTLING,
			Transaction.Status.SUBMITTED_FOR_SETTLEMENT };

	/*@RequestMapping(value = "/{customer_id}", method = RequestMethod.GET)
	public ResultWrapper<Customer> customer(@PathVariable Long customer_id) {
		ResultWrapper<Customer> result = new ResultWrapper<Customer>();

		Customer customer = null;
		String id = customer_id.toString();
		try {

			CustomerSearchRequest request1 = new CustomerSearchRequest().id().is(id);

			ResourceCollection<Customer> collection = gateway.customer().search(request1);

			for (Customer vaultcustomer : collection) {
				customer = vaultcustomer;
			}
			if (customer != null) {
				result.setResult(customer);
				result.setMessage(com.giftcard.response.Result.SUCCESS);
				result.setExplanation("customer details found.");
			} else {
				User user = userService.findOne(customer_id);
				if (user != null) {
					CustomerRequest request = new CustomerRequest().id(id).firstName(user.getFirstName())
							.lastName(user.getLastName());
					Result<Customer> customerResult = gateway.customer().create(request);

					if (customerResult.isSuccess()) {
						String customerId = customerResult.getTarget().getId();
						customer = gateway.customer().find(customerId);
						result.setResult(customer);
						result.setMessage(com.giftcard.response.Result.SUCCESS);
						result.setExplanation("customer created successfully");
					} else {
						result.setMessage(com.giftcard.response.Result.FAIL);
						result.setExplanation("Customer not created.please try again");
					}

				} else {
					result.setMessage(com.giftcard.response.Result.SUCCESS);
					result.setExplanation("Customer not found in vault");
				}

			}
		} catch (Exception e) {
			result.setMessage(com.giftcard.response.Result.FAIL);
			result.setExplanation("error occured." + e);
			logger.error("Exception: ", e);
		}

		return result;
	}

	@RequestMapping(value = "/billing-address", method = RequestMethod.POST)
	public ResultWrapper<Address> billingAddress(@RequestBody BillingAddress billingAddress) {
		ResultWrapper<Address> result = new ResultWrapper<Address>();
		long id = billingAddress.getUser().getId();
		String customerId = "" + id;
		Customer customer = null;
		try {
			CustomerSearchRequest request1 = new CustomerSearchRequest().id().is(customerId);

			ResourceCollection<Customer> collection = gateway.customer().search(request1);

			for (Customer vaultcustomer : collection) {
				customer = vaultcustomer;
			}
			Address address = null;
			if (!customer.getAddresses().isEmpty()) {
				address = customer.getAddresses().get(0);
			}
			if (address != null) {
				AddressRequest updateRequest = new AddressRequest().firstName(billingAddress.getUser().getFirstName())
						.lastName(billingAddress.getUser().getLastName())
						.streetAddress(billingAddress.getAddressLine1())
						.extendedAddress(billingAddress.getAddressLine2()).locality(billingAddress.getCity())
						.region(billingAddress.getState()).postalCode(billingAddress.getZipCode())
						.countryCodeAlpha2("US");

				gateway.address().update(customerId, billingAddress.getAddressId(), updateRequest);
				result.setMessage(com.giftcard.response.Result.SUCCESS);
				result.setExplanation("address updated successfully");
			} else {
				AddressRequest request = new AddressRequest().firstName(billingAddress.getUser().getFirstName())
						.lastName(billingAddress.getUser().getLastName())
						.streetAddress(billingAddress.getAddressLine1())
						.extendedAddress(billingAddress.getAddressLine2()).locality(billingAddress.getCity())
						.region(billingAddress.getState()).postalCode(billingAddress.getZipCode())
						.countryCodeAlpha2("US");

				Result<Address> addressResult = gateway.address().create(customerId, request);
				Address tempAddress = addressResult.getTarget();
				result.setResult(tempAddress);
				result.setMessage(com.giftcard.response.Result.SUCCESS);
				result.setExplanation("address created successfully");

			}
		} catch (Exception e) {
			result.setMessage(com.giftcard.response.Result.EXCEPTION);
			result.setExplanation("exception occures :" + e);
			logger.error("Exception: ", e);
		}
		return result;
	}*/

	@RequestMapping(value = "/getClientToken", method = RequestMethod.GET)
	public ResultWrapper<String> getToken() {
		ResultWrapper<String> result = new ResultWrapper<String>();
		try {
			String clientToken = gateway.clientToken().generate();
			result.setMessage(com.giftcard.response.Result.SUCCESS);
			result.setResult(clientToken);
			result.setExplanation("client token genrated sucessfully");
		} catch (ServerException e) {
			result.setMessage(com.giftcard.response.Result.FAIL);
			result.setResult("something goes wrong");
		}
		return result;
	}

	@RequestMapping(value = "/checkouts", method = RequestMethod.POST)
	public ResultWrapper<Object> postForm(@RequestParam("amount") String amount,
			@RequestParam("payment_method_nonce") String nonce,@RequestParam("postalcode") String postalcode) {
		BigDecimal decimalAmount;
		ResultWrapper<Object> result = new ResultWrapper<Object>();
		try {
			decimalAmount = new BigDecimal(amount);
		} catch (NumberFormatException e) {
			result.setMessage(com.giftcard.response.Result.FAIL);
			result.setExplanation("Error: 81503: Amount is an invalid format.");
			return result;
		}

		TransactionRequest request = new TransactionRequest()
				.amount(decimalAmount)
				.paymentMethodNonce(nonce)
				.billingAddress()
					.postalCode(postalcode)
					.done()
				.options()
				.submitForSettlement(true)
				.done();

		Result<Transaction> payment = gateway.transaction().sale(request);

		if (payment.isSuccess()) {
			Transaction transaction = payment.getTarget();
			result.setResult(transaction);

		} else if (payment.getTransaction() != null) {
			Transaction transaction = payment.getTransaction();
			result.setResult(transaction);

		} else {
			String errorString = "";
			for (ValidationError error : payment.getErrors().getAllDeepValidationErrors()) {
				errorString += "Error: " + error.getCode() + ": " + error.getMessage() + "\n";
			}
			result.setMessage(com.giftcard.response.Result.FAIL);
			result.setExplanation(errorString);

		}
		return result;
	}

	@RequestMapping(value = "/checkouts/{transactionId}")
	public List<Object> getTransaction(@PathVariable String transactionId) {
		Transaction transaction;
		CreditCard creditCard;
		Customer customer;
		List<Object> result = new ArrayList<Object>();
		try {
			transaction = gateway.transaction().find(transactionId);
			creditCard = transaction.getCreditCard();
			customer = transaction.getCustomer();
			result.add(transaction);
			result.add(creditCard);
			result.add(customer);
		} catch (Exception e) {

			logger.error("Exception: ", e);
			result.add(e);
		}

		return result;
	}
	
	
	@RequestMapping(value = "/checkoutTransaction", method = RequestMethod.POST)
	public ResultWrapper<StringBuffer> createTransaction(@RequestBody String jsonData) throws ParserConfigurationException, SAXException, IOException {
		 
		ResultWrapper<StringBuffer> result = new ResultWrapper<StringBuffer>();
		try {
		     
			
			String httpsUrl = appProperties.getGlobalOnePayURL() ;
			System.out.println("@@@ACTEST jsonData:"+jsonData);

			URL url;
			url = new URL(httpsUrl);
			HttpsURLConnection con = (HttpsURLConnection)url.openConnection();

			con.setRequestMethod("POST");
			con.addRequestProperty("Content-Type", "application/xml;");
			con.setDoOutput(true);
				con.getOutputStream().write(jsonData.getBytes("UTF8"));
			
			
			int responseCode = con.getResponseCode();

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			result.setResult(response);
			System.out.println("@@@ACTEST response:"+response);
			return result;
		   	}
		catch (Exception e) {
			System.out.println(e);
			result.setMessage(com.giftcard.response.Result.FAIL);
			result.setExplanation("Exception: " +e.toString());
			return result;
		}
		   	

		
	}
	
	
	
}
