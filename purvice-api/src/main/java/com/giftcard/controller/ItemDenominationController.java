package com.giftcard.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.giftcard.model.ItemDenomination;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.ItemDenominationService;

@RestController
@RequestMapping("/denomination")
public class ItemDenominationController {
	
	@Autowired
	private ItemDenominationService itemDenominationService;
	private static final Logger logger = LoggerFactory.getLogger(ItemDenominationController.class);
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<ResultWrapper<ItemDenomination>> createDenomination(@RequestBody ItemDenomination itemDenomination) {

		ResultWrapper<ItemDenomination> result = new ResultWrapper<ItemDenomination>();

		try {
			ItemDenomination dbItemDenomination = itemDenominationService.findByItemId(itemDenomination.getItemId());
			if (dbItemDenomination != null) {
				dbItemDenomination.setPrices(itemDenomination.getPrices());
				itemDenominationService.save(dbItemDenomination);
				result.setResult(dbItemDenomination);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Price values updated successfully");
			} else {
				itemDenominationService.save(itemDenomination);
				result.setResult(itemDenomination);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Denomination prices created successfully.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while creating Denomination prices.", e);
		}
		return ResponseEntity.ok(result);
	}
}
