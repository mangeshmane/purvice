package com.giftcard.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.giftcard.model.VideoSetting;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.VideoSettingService;

@RestController
@RequestMapping("/setting")
public class VideoSettingController {

	
	@Autowired
	private VideoSettingService videoSettingService;

	private static final Logger logger = LoggerFactory.getLogger(VideoSettingController.class);

	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<ResultWrapper<VideoSetting>> updateBrand(@RequestBody VideoSetting videoSetting) {

		ResultWrapper<VideoSetting> result = new ResultWrapper<VideoSetting>();

		try {
			VideoSetting dbVideoSetting = videoSettingService.findOne(1);

			if (dbVideoSetting != null) {
				dbVideoSetting.setStatus(videoSetting.isStatus());
				VideoSetting newVideoSetting = videoSettingService.save(dbVideoSetting);
				result.setResult(newVideoSetting);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Video Setting updated successfully.");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("Video Setting does not exists.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while updating VideoSetting.", e);
		}
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping("/search")
	public ResponseEntity<ResultWrapper<VideoSetting>> readCategory() {
		ResultWrapper<VideoSetting> result = new ResultWrapper<VideoSetting>();

		try {
			VideoSetting dbVideoSetting = videoSettingService.findOne(1);
			if (dbVideoSetting != null) {
				result.setResult(dbVideoSetting);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Video Setting details found.");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("No data found.");
			}

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage() + ".");
			logger.error("Error while reading Video Setting details.", e);
		}
		return ResponseEntity.ok(result);
	}
}
