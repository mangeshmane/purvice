package com.giftcard.controller;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Transaction;
import com.braintreegateway.Transaction.Status;
import com.braintreegateway.exceptions.NotFoundException;
import com.braintreegateway.exceptions.UnexpectedException;
import com.giftcard.Application;
import com.giftcard.model.*;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.BrandImageService;
import com.giftcard.service.ItemService;
import com.giftcard.service.OrdersService;
import com.giftcard.service.UserService;
import com.tangocard.client.model.CreateOrderCriteria;
import com.tangocard.client.model.CredentialView;
import com.tangocard.client.model.OrderViewSummary;
import com.tangocard.client.model.ResendView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/order")
public class OrdersController {

	@Autowired
	private UserService userService;

	@Autowired
	private OrdersService ordersService;

	@Autowired
	private BrandImageService brandImageService;

	@Autowired
	private ItemService itemService;

	private BraintreeGateway gateway = Application.gateway;

	private Status submittedForSettlement = Transaction.Status.SUBMITTED_FOR_SETTLEMENT;
	private Status settled = Transaction.Status.SETTLED;
	private Status settlementConfirmed = Transaction.Status.SETTLEMENT_CONFIRMED;
	private Status settlementPending = Transaction.Status.SETTLEMENT_PENDING;
	private Status settling = Transaction.Status.SETTLING;

	private static final Logger logger = LoggerFactory.getLogger(OrdersController.class);

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<ResultWrapper<Orders>> createOrder(@RequestBody Orders orders) {

		ResultWrapper<Orders> result = new ResultWrapper<Orders>();

		try {
			orders.setOrderDate(new Date());
			orders.setStatus(Orders.Status.INPROGRESS);
			orders.setPaymentStatus("Pending");
			orders.setTangoOrderStatus("Inprogress");
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = userService.findByUsername(auth.getName());
			orders.setUser(user);
			if (orders.getOrderType().equals("Self")) {
				orders.setFriendName(null);
				orders.setFriendEmailId(null);
				orders.setFriendMessage(null);
				orders.setFriendContact(null);
				orders.setIsscheduled(false);
				orders.setScheduledDate(null);
				orders.setEmailSubject("purvice gift");
				ordersService.save(orders);
				result.setResult(orders);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Order stored successfully.");
			} else {
				if (orders.isIsscheduled() && orders.getScheduledDate() != null) {
					ordersService.save(orders);
					result.setResult(orders);
					result.setMessage(Result.SUCCESS);
					result.setExplanation("Order stored successfully.");
				} else if (!orders.isIsscheduled()) {
					orders.setScheduledDate(null);
					ordersService.save(orders);
					result.setResult(orders);
					result.setMessage(Result.SUCCESS);
					result.setExplanation("Order stored successfully.");
				} else {
					result.setResult(orders);
					result.setMessage(Result.FAIL);
					result.setExplanation("Scheduled date is compulsory");
				}
			}

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while storing information", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/{referenceOrderID}", method = RequestMethod.POST)
	public ResponseEntity<ResultWrapper<ResendView>> resendOrder(@PathVariable String referenceOrderID) {

		ResultWrapper<ResendView> result = new ResultWrapper<ResendView>();
		ResendView resendView = null;
		try {
			resendView = ordersService.resendOrder(referenceOrderID);
			if (resendView != null) {
				result.setResult(resendView);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Order resend successfully.");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("Order resend failed. please try after sometime");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultWrapper<List<Orders>>> getOrder(@PathVariable Long id) {

		ResultWrapper<List<Orders>> result = new ResultWrapper<List<Orders>>();
		try {
			List<Orders> orders = ordersService.findByUser(id);
			Iterator<Orders> itr = orders.iterator();
			if (orders != null && !orders.isEmpty()) {

				while (itr.hasNext()) {
					Orders dbOrders = itr.next();
					Item item = itemService.findByUtid(dbOrders.getUtid());
					Brand brand = item.getBrand();
					List<BrandImage> brandImages = brandImageService.findByBrand_id(brand.getId());
					brand.setImageUrls(brandImages);
					item.setBrand(brand);
					dbOrders.setItem(item);
				}
				result.setResult(orders);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Orders get successfully.");
			} else {
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Orders not present");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while getting information", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<ResultWrapper<Orders>> updateOrder(@RequestBody Orders order) {
		System.out.println("@@@@@@@@@@@@@@@@@@@ACTEST CAME INSIDE /update @@@@@@@@@@@@@@@@@@@@@@@@@@");

		ResultWrapper<Orders> result = new ResultWrapper<Orders>();
		Orders tempOrder = ordersService.findById(order.getId());
		if (tempOrder != null) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = userService.findByUsername(auth.getName());
			tempOrder.setUser(user);
			CreateOrderCriteria createOrderCriteria = null;
			Transaction transaction = null;
			try {

				if (order.getPaymentTransactionId() != null) {
					transaction = gateway.transaction().find(order.getPaymentTransactionId());
					tempOrder.setPaymentTransactionId(order.getPaymentTransactionId());
					tempOrder.setTransaction(transaction.getStatus().toString());
					int code;
					if (transaction.getProcessorResponseCode().equals("")
							|| transaction.getProcessorResponseCode() == null) {
						code = 0;
					} else {
						code = Integer.parseInt(transaction.getProcessorResponseCode());
						tempOrder.setPaymentProcessorResponseCode(transaction.getProcessorResponseCode());
						tempOrder.setPaymentrocessorResponseText(transaction.getProcessorResponseText());
					}

					if (code >= 1000 && code < 2000 && transactionValidate(transaction)) {
						tempOrder.setPaymentStatus("Completed");
						tempOrder.setPaymentMessage("Your Transaction is Completed");
						if (tempOrder.getOrderType().equals("Friend") && tempOrder.isIsscheduled()) {
							tempOrder.setTangoOrderStatus("Scheduled");
							tempOrder.setStatus(Orders.Status.SCHEDULED);
						}
					} else {
						tempOrder.setPaymentStatus("Failed");
						tempOrder.setPaymentMessage("Your Transaction has failed");
					}

				} else {
					if (tempOrder.getOrderType().equals("Friend")) {
						tempOrder = ordersService.updateOrderData(tempOrder, order);
					}
				}
				ordersService.save(tempOrder);
				Orders dbOrder = ordersService.findById(tempOrder.getId());
				if (orderValidate(transaction, dbOrder)) {
					createOrderCriteria = ordersService.populateTangoOrder(dbOrder);
				}
				if (createOrderCriteria != null) {
					OrderViewSummary orderViewSummary = ordersService.sendOrder(createOrderCriteria);
					if (orderViewSummary != null) {
						dbOrder.setTangoOrderStatus("Completed");
						dbOrder.setStatus(Orders.Status.COMPLETE);
						dbOrder.setExternalRefID(orderViewSummary.getReferenceOrderID());
						String response = ordersService.createString(orderViewSummary);
						dbOrder.setTangoResponse(response);
						dbOrder.setEmailAttempt("Successful");
						dbOrder.setEmailMessage("Gift sent successfully");
						List<CredentialView> reward = orderViewSummary.getReward().getCredentialList();
						String code="";
						for (CredentialView credentialView : reward) {
							code =code+"    "+ credentialView.getLabel() + " : " + credentialView.getValue()+"    ";
						}
						
						dbOrder.setClaimCode(code);
						/*if (dbOrder.getOrderType().equals("Friend")) {
							ordersService.sendSMS(dbOrder);
						}*/
						ordersService.save(dbOrder);
						dbOrder.setClaimCode(null);
						result.setResult(dbOrder);
						result.setMessage(Result.SUCCESS);
						result.setExplanation("Order sent successfully.");
					} else {
						dbOrder.setStatus(Orders.Status.DELIVERED_FAILED);
						dbOrder.setTangoOrderStatus("Delivered_failed");
						dbOrder.setEmailAttempt("Failed");
						dbOrder.setEmailMessage("Gift sending failed");
						ordersService.save(dbOrder);
						result.setResult(dbOrder);
						result.setMessage(Result.SUCCESS);
						result.setExplanation("Order sending failed");
					}
				} else {
					if(transaction != null && !dbOrder.getStatus().equals(Orders.Status.SCHEDULED)){
						dbOrder.setStatus(Orders.Status.DELIVERED_FAILED);
						dbOrder.setTangoOrderStatus("Delivered_failed");
						ordersService.save(dbOrder);
					}
					result.setResult(dbOrder);
					result.setMessage(Result.SUCCESS);
					result.setExplanation("Order updated successfully.");
				}
			} catch (UnexpectedException | NotFoundException nfe) {
				result.setMessage(Result.FAIL);
				result.setExplanation("error = " + "Transaction Id is invalid");
				logger.error("Error", nfe);
			} catch (Exception e) {
				result.setMessage(Result.FAIL);
				result.setExplanation("error = " + e);
				logger.error("Error", e);
			}
		} else {

			result.setMessage(Result.FAIL);
			result.setExplanation("Order not present");
		}
		return ResponseEntity.ok(result);
	}

	private static boolean validateCvvAndAvs(String aCvvCode, String aAvsCode){
		boolean mResult = false;

		if(aCvvCode != null && aAvsCode != null && aCvvCode.equalsIgnoreCase("M") && (aAvsCode.equalsIgnoreCase("Y")  || aAvsCode.equalsIgnoreCase("X"))){
			mResult = true;
		}

		return mResult;
	}

	@RequestMapping(value = "/updateOrder", method = RequestMethod.PUT)
	public ResponseEntity<ResultWrapper<Orders>> updateCreditCardOrder(@RequestBody Orders order, HttpServletRequest request) {

		ResultWrapper<Orders> result = new ResultWrapper<Orders>();
		Orders tempOrder = ordersService.findById(order.getId());
		if (tempOrder != null) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = userService.findByUsername(auth.getName());
			tempOrder.setUser(user);
			CreateOrderCriteria createOrderCriteria = null;
			String transaction = null;
			try {

				if (order.getPaymentTransactionId() != null) {
					System.out.println("@@@@@@@@@@@@@ACTEST order.getAvsResponse():"+order.getAvsResponse());
					System.out.println("@@@@@@@@@@@@@ACTEST order.getCvvResponse():"+order.getCvvResponse());
					System.out.println("@@@@@@@@@@@@@ACTEST request.getRemoteAddr():"+request.getRemoteAddr());
					transaction = order.getPaymentProcessorResponseCode();
					tempOrder.setPaymentTransactionId(order.getPaymentTransactionId());
					tempOrder.setTransaction(transaction);
					tempOrder.setAvsResponse(order.getAvsResponse());
					tempOrder.setCvvResponse(order.getCvvResponse());
					tempOrder.setIpAddress(request.getRemoteAddr());
					String code;
					if (transaction.equals("")	|| transaction == null) {
						code = "invalidTransaction";
					} else {
						code = transaction;
						//tempOrder.setPaymentProcessorResponseCode(transaction.getProcessorResponseCode());
						//tempOrder.setPaymentrocessorResponseText(transaction.getProcessorResponseText());
					}

					if ((code.equals("A") || code.equals("E"))  && validateCvvAndAvs(order.getCvvResponse(),order.getAvsResponse())) {
						tempOrder.setPaymentStatus("Completed");
						tempOrder.setPaymentMessage("Your Transaction is Completed");
						if (tempOrder.getOrderType().equals("Friend") && tempOrder.isIsscheduled()) {
							tempOrder.setTangoOrderStatus("Scheduled");
							tempOrder.setStatus(Orders.Status.SCHEDULED);
						}
					} else {
						tempOrder.setPaymentStatus("Failed");
						String mCvvAvsMessage = "";
						if((code.equals("A") || code.equals("E"))  && !validateCvvAndAvs(order.getCvvResponse(),order.getAvsResponse())){
							mCvvAvsMessage = ". We couldn't validate your CVV/Address.";
						}
						tempOrder.setPaymentMessage("Your Transaction has failed"+mCvvAvsMessage);
					}

				} else {
					if (tempOrder.getOrderType().equals("Friend")) {
						tempOrder = ordersService.updateOrderData(tempOrder, order);
					}
				}
				ordersService.save(tempOrder);
				Orders dbOrder = ordersService.findById(tempOrder.getId());
				if (creditCardorderValidate(transaction, dbOrder)) {
					createOrderCriteria = ordersService.populateTangoOrder(dbOrder);
				}
				if (createOrderCriteria != null) {
					OrderViewSummary orderViewSummary = ordersService.sendOrder(createOrderCriteria);
					if (orderViewSummary != null) {
						dbOrder.setTangoOrderStatus("Completed");
						dbOrder.setStatus(Orders.Status.COMPLETE);
						dbOrder.setExternalRefID(orderViewSummary.getReferenceOrderID());
						String response = ordersService.createString(orderViewSummary);
						dbOrder.setTangoResponse(response);
						dbOrder.setEmailAttempt("Successful");
						dbOrder.setEmailMessage("Gift sent successfully");
						List<CredentialView> reward = orderViewSummary.getReward().getCredentialList();
						String code="";
						for (CredentialView credentialView : reward) {
							code =code+"    "+ credentialView.getLabel() + " : " + credentialView.getValue()+"    ";
						}
						
						dbOrder.setClaimCode(code);
						/*if (dbOrder.getOrderType().equals("Friend")) {
							ordersService.sendSMS(dbOrder);
						}*/
						ordersService.save(dbOrder);
						dbOrder.setClaimCode(null);
						result.setResult(dbOrder);
						result.setMessage(Result.SUCCESS);
						result.setExplanation("Order sent successfully.");
					} else {
						dbOrder.setStatus(Orders.Status.DELIVERED_FAILED);
						dbOrder.setTangoOrderStatus("Delivered_failed");
						dbOrder.setEmailAttempt("Failed");
						dbOrder.setEmailMessage("Gift sending failed");
						ordersService.save(dbOrder);
						result.setResult(dbOrder);
						result.setMessage(Result.SUCCESS);
						result.setExplanation("Order sending failed");
					}
				} else {
					if(transaction != null && !dbOrder.getStatus().equals(Orders.Status.SCHEDULED)){
						dbOrder.setStatus(Orders.Status.DELIVERED_FAILED);
						dbOrder.setTangoOrderStatus("Delivered_failed");
						ordersService.save(dbOrder);
					}
					result.setResult(dbOrder);
					result.setMessage(Result.SUCCESS);
					result.setExplanation("Order updated successfully.");
				}
			} catch (UnexpectedException | NotFoundException nfe) {
				result.setMessage(Result.FAIL);
				result.setExplanation("error = " + "Transaction Id is invalid");
				logger.error("Error", nfe);
			} catch (Exception e) {
				result.setMessage(Result.FAIL);
				result.setExplanation("error = " + e);
				logger.error("Error", e);
			}
		} else {

			result.setMessage(Result.FAIL);
			result.setExplanation("Order not present");
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<ResultWrapper<Orders>> cancelOrder(@PathVariable Long id) {

		ResultWrapper<Orders> result = new ResultWrapper<Orders>();

		try {
			Orders order = ordersService.findById(id);
			if (order != null) {
				order.setTangoOrderStatus("Cancelled");
				order.setStatus(Orders.Status.CANCELED);
				ordersService.save(order);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Order cancelled successfully.");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("Order details not found.");
			}

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while deleting order", e);
		}
		return ResponseEntity.ok(result);
	}

	public boolean orderValidate(Transaction transaction, Orders order) {
		boolean flag = false;
		if (transaction != null) {
			int code;
			Status status = transaction.getStatus();
			if (transaction.getProcessorResponseCode().equals("") || transaction.getProcessorResponseCode() == null) {
				code = 0;
			} else {
				code = Integer.parseInt(transaction.getProcessorResponseCode());
			}
			if (code >= 1000 && code < 2000 && (status.equals(submittedForSettlement) || status.equals(settled)
					|| status.equals(settlementConfirmed) || status.equals(settlementPending)
					|| status.equals(settling))
					&& order.getStatus() == Orders.Status.INPROGRESS) {
				flag = true;
			}
		}
		return flag;
	}

	public boolean creditCardorderValidate(String transaction, Orders order) {
		boolean flag = false;
		if (transaction != null) {
			String code;
			String status = transaction;
			if (transaction.equals("") || transaction == null) {
				code = "invalidTransaction";
			} else {
				code = transaction;
			}

			System.out.println("@@@@@@@ACTEST inside creditCardorderValidate ... order.getCvvResponse():["+order.getCvvResponse()+"] order.getAvsResponse():["+order.getAvsResponse()+"]");

			if ((code.equals("A") || code.equals("E")) && order.getStatus() == Orders.Status.INPROGRESS && validateCvvAndAvs(order.getCvvResponse(),order.getAvsResponse())) {
				flag = true;
			}
		}
		return flag;
	}

	public boolean transactionValidate(Transaction transaction) {
		boolean flag = false;
		if (transaction != null) {
			int code;
			Status status = transaction.getStatus();
			if (transaction.getProcessorResponseCode().equals("") || transaction.getProcessorResponseCode() == null) {
				code = 0;
			} else {
				code = Integer.parseInt(transaction.getProcessorResponseCode());
			}
			if (code >= 1000 && code < 2000
					&& (status.equals(submittedForSettlement) || status.equals(settled)
							|| status.equals(settlementConfirmed) || status.equals(settlementPending)
							|| status.equals(settling))) {
				flag = true;
			}
		}
		return flag;
	}
	
	@RequestMapping(value = "/count/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultWrapper<Long>> countOrder(@PathVariable Long id) {

		ResultWrapper<Long> result = new ResultWrapper<Long>();
		try {
			Long count=(long) 0;
			count = ordersService.countByUser_idAndTangoOrderStatusNotLike(id, "Cancelled");
			
				result.setResult(count);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Orders get successfully.");
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while getting information", e);
		}
		return ResponseEntity.ok(result);
	}
}
