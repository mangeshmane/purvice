package com.giftcard.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giftcard.deal.service.BannerService;
import com.giftcard.model.Banner;
import com.giftcard.model.DealAlert;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;

@RestController
@RequestMapping("/banner")
public class BannerController {

	@Autowired
	private BannerService bannerService;

	private static final Logger logger = LoggerFactory.getLogger(BannerController.class);
	
/*	List<Banner> bannerCacheMap = new ArrayList<Banner>();
	
	@PostConstruct
	public void init(){
		createCache();
	}
	
	private void createCache() {
		bannerCacheMap.clear();
		bannerCacheMap = bannerService.fetchByEnable();
	}  */

		@RequestMapping(value = "/bannersave", method = RequestMethod.POST)
		public ResultWrapper<Banner> createBanner(@RequestBody Banner banner) {
			ResultWrapper<Banner> result = new ResultWrapper<Banner>();
			try {
				Banner tempBanner = bannerService.save(banner);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Banner image uploaded successfully!");
				result.setResult(tempBanner);
				//createCache();
			} catch (Exception e) {
				result.setMessage(Result.FAIL);
				result.setExplanation("The size of image banner should be less than 16MB");
				logger.error(e.toString());
			}
			return result;
		}
		
		@RequestMapping(value = "/all", method = RequestMethod.GET)
		public ResultWrapper<List<Banner>> getAll() {
			ResultWrapper<List<Banner>> result = new ResultWrapper<List<Banner>>();
			List<Banner> bannerData = null;
			try {
				bannerData = bannerService.getAll();		
				result.setResult(bannerData);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Fetch all Images.");
			} catch (Exception e) {
				result.setMessage(Result.FAIL);
				logger.error("Error while fetch all deal alert.", e);
			}
			return result;
		}
		
		@RequestMapping(value = "/fetchByEnable", method = RequestMethod.GET)
		public ResultWrapper<List<Banner>> fetchByEnableData() {
			ResultWrapper<List<Banner>> result = new ResultWrapper<List<Banner>>();
			try {	
				List<Banner> bannerData= bannerService.fetchByEnable();
				result.setResult(bannerData);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Fetch all Images.");
			} catch (Exception e) {
				result.setMessage(Result.FAIL);
				logger.error(e.toString());
			}
			return result;
		}
 
		@RequestMapping(value = "/update", method = RequestMethod.POST)
		public ResultWrapper<Banner> updateBanner(@RequestBody Banner banner) {
			ResultWrapper<Banner> result = new ResultWrapper<Banner>();
			try {
				Banner tempBanner = bannerService.save(banner);
				result.setMessage(Result.SUCCESS);
				result.setResult(tempBanner);
				//createCache();
			} catch (Exception e) {
				result.setMessage(Result.FAIL);
				result.setExplanation(e.getMessage());
				logger.error(e.toString());
			}
			return result;
		}
		
		@RequestMapping(value = "/delete", method = RequestMethod.GET)
		public ResultWrapper<List<Banner>> deleteByBannerId (
				@RequestParam Long bannerId) {
			ResultWrapper<List<Banner>> result = new ResultWrapper<List<Banner>>();
			try {
				bannerService.deleteById(bannerId);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("delete banner");
			} catch (Exception e) {
				result.setMessage(Result.FAIL);
				logger.error("Error while deleting banner.", e);
			}
			return result;
		}
}
