package com.giftcard.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.giftcard.model.Orders;
import com.giftcard.model.PaymentDetails;
import com.giftcard.model.User;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.PaymentService;
import com.giftcard.service.UserService;

@RestController
@RequestMapping("/payment")
public class PaymentController {

	@Autowired
	private PaymentService paymentService;
	
	private static final Logger logger = LoggerFactory.getLogger(OrdersController.class);

	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public ResponseEntity<ResultWrapper<PaymentDetails>> getPaymentDetails() {

		ResultWrapper<PaymentDetails> result = new ResultWrapper<PaymentDetails>();
		
		try {
			PaymentDetails paymentDetails = paymentService.getAll();
			
			if (paymentDetails != null) {
				result.setMessage(Result.SUCCESS);
				result.setResult(paymentDetails);
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("No Details present");
			}
		}catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while getting data: " + e.getMessage() + ".");
			logger.error("Error while getting data.", e);
		}
		return ResponseEntity.ok(result);
	}
}
