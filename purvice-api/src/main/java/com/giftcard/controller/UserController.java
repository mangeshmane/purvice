package com.giftcard.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giftcard.model.User;
import com.giftcard.repository.UserRepository;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.UserService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@CrossOrigin
public class UserController {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserService userService;

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	/**
	 * This method is used for user registration. Note: user registration is not
	 * require any authentication.
	 * 
	 * @param appUser
	 * @return
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<ResultWrapper<User>> createUser(@RequestBody User appUser, HttpServletResponse response) {

		ResultWrapper<User> result = new ResultWrapper<User>();

		try {
			User dbUser = userService.findByUsername(appUser.getUsername());

			if (dbUser != null) {
				result.setMessage(Result.FAIL);
				result.setExplanation("Email address already used.Please use different Email address.");
			} else {
				User user = userService.createUserInfo(appUser);
				result.setResult(user);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Please check your mail and verify your account.");
			}

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while creating User.", e);
		}

		return ResponseEntity.ok(result);
	}

	/**
	 * This method will return the logged user.
	 * 
	 * @param principal
	 * @return Principal java security principal object
	 */
	@RequestMapping("/user")
	public User user(Principal principal) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String loggedUsername = auth.getName();
		return userRepository.findByUsername(loggedUsername);
	}

	/**
	 * @param username
	 * @param password
	 * @param response
	 * @return JSON contains token and user after success authentication.
	 * @throws IOException
	 */
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<ResultWrapper<Map<String, Object>>> login(@RequestBody User user,
			HttpServletResponse response,Device device) throws IOException {

		ResultWrapper<Map<String, Object>> result = new ResultWrapper<Map<String, Object>>();
		String deviceType = "unknown";
		 if (device.isMobile() || device.isTablet()) {
	            deviceType = "mobile or tablet";
	            try {
	    			String token = null;
	    			Map<String, Object> tokenMap = new HashMap<String, Object>();
	    			User appUser = userService.findByUsernameAndPassword(user.getUsername(), user.getPassword());

	    			if (appUser != null) {
	    				if (appUser.isVerify() == true) {
	    					
	    					token = Jwts.builder().setSubject(user.getUsername()).claim("roles", appUser.getRoles())
	    							.setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, "secretkey")
	    							.compact();
	    					tokenMap.put("token", token);
	    					tokenMap.put("user", appUser);
	    					result.setResult(tokenMap);
	    					result.setMessage(Result.SUCCESS);
	    					result.setExplanation("Login Successful");
	    				} else {
	    					result.setMessage(Result.FAIL);
	    					result.setExplanation("Please check your mail and verify your account.");
	    				}
	    			} else {
	    				tokenMap.put("token", null);
	    				result.setResult(tokenMap);
	    				result.setMessage(Result.FAIL);
	    				result.setExplanation("Email or Password is invalid.");
	    			}

	    		} catch (Exception e) {
	    			result.setMessage(Result.FAIL);
	    			result.setExplanation(e.getMessage());
	    			logger.error("Error while user login.", e);
	    		}
	    		System.out.println("Hello " + deviceType + " browser!");
	    		return ResponseEntity.ok(result);
	        } else{
	        	try {
	    			
	    			String token = null;
	    			Map<String, Object> tokenMap = new HashMap<String, Object>();
	    			User appUser = userService.findByUsernameAndPassword(user.getUsername(), user.getPassword());

	    			if (appUser != null) {
	    				if (appUser.isVerify() == true) {
	    					int addMinuteTime = 240;
	    					Date ExpirationTime = new Date(); // now
	    					ExpirationTime = DateUtils.addMinutes(ExpirationTime, addMinuteTime);
	    					token = Jwts.builder().setSubject(user.getUsername()).claim("roles", appUser.getRoles())
	    							.setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, "secretkey")
	    							.setExpiration(ExpirationTime).compact();
	    					tokenMap.put("token", token);
	    					tokenMap.put("user", appUser);
	    					result.setResult(tokenMap);
	    					result.setMessage(Result.SUCCESS);
	    					result.setExplanation("Login Successful");
	    				} else {
	    					result.setMessage(Result.FAIL);
	    					result.setExplanation("Please check your mail and verify your account.");
	    				}
	    			} else {
	    				tokenMap.put("token", null);
	    				result.setResult(tokenMap);
	    				result.setMessage(Result.FAIL);
	    				result.setExplanation("Email or Password is invalid.");
	    			}

	    		} catch (Exception e) {
	    			result.setMessage(Result.FAIL);
	    			result.setExplanation(e.getMessage());
	    			logger.error("Error while user login.", e);
	    		}
	    		System.out.println("Hello " + deviceType + " browser!");
	    		return ResponseEntity.ok(result);
	        }
		
	}

	@RequestMapping(method = RequestMethod.GET, value = "/verifyEmail/{token}")
	public ResponseEntity<ResultWrapper<String>> verifyEmail(@PathVariable String token) {

		ResultWrapper<String> result = new ResultWrapper<String>();
		try {
			User user = userService.findByToken(token);
			if (user != null) {
				String[] dbToken = user.getToken().split(":");
				String dbTime = dbToken[1];
				Long tokenCreateTime = Long.parseLong(dbTime);
				Long currentTime = System.currentTimeMillis();
				if (currentTime - tokenCreateTime < 172800000) { // 172800000
																	// milliseconds
																	// = 48
																	// hours
					user.setVerify(true);
					user.setToken(null);
					userService.save(user);
					result.setMessage(Result.SUCCESS);
					result.setResult("Verification successful.");
					result.setExplanation("User verified successfully.");
				} else {
					user.setToken(null);
					userService.save(user);
					result.setMessage(Result.FAIL);
					result.setResult("Token Expired");
				}
			} else {
				result.setMessage(Result.FAIL);
				result.setResult("Verification Error. Token Expired");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("User Verification Error: " + e.getMessage() + ".");
			logger.error("User Verification Error.", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public ResponseEntity<ResultWrapper<User>> userByUserName(@RequestParam("username") String username) {

		ResultWrapper<User> result = new ResultWrapper<User>();

		try {
			User dbUser = userService.findByUsername(username);
			if (dbUser != null) {
				result.setResult(dbUser);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("User details found.");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("No data found.");
			}

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage() + ".");
			logger.error("Error while reading User details.", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<ResultWrapper<Map<String, Object>>> socialLogin(@RequestBody User user,
			HttpServletResponse response ,Device device) throws IOException {

		ResultWrapper<Map<String, Object>> result = new ResultWrapper<Map<String, Object>>();
		String deviceType = "unknown";
		if (device.isMobile() || device.isTablet()) {
            deviceType = "mobile or tablet";
            
		try {
			String token = null;
			Map<String, Object> tokenMap = new HashMap<String, Object>();
			User dbUser = userService.findByUsername(user.getUsername());
			if (dbUser == null) {
				user.setCreatedDate(new Date());
				BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				String hasPassword = passwordEncoder.encode(user.getPassword());
				user.setPassword(hasPassword);
				List<String> roles = new ArrayList<>();
				roles.add("SOCIAL_USER");
				user.setRoles(roles);
				userRepository.save(user);
			}
			dbUser = userService.findByUsername(user.getUsername());
			token = Jwts.builder().setSubject(user.getUsername()).claim("roles", dbUser.getRoles())
					.setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, "secretkey")
					.compact();
			tokenMap.put("token", token);
			tokenMap.put("user", dbUser);
			result.setResult(tokenMap);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("Login Successful");
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while user login.", e);
		}

		return ResponseEntity.ok(result);
	}else{
		try {
			String token = null;
			Map<String, Object> tokenMap = new HashMap<String, Object>();
			User dbUser = userService.findByUsername(user.getUsername());
			if (dbUser == null) {
				user.setCreatedDate(new Date());
				BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				String hasPassword = passwordEncoder.encode(user.getPassword());
				user.setPassword(hasPassword);
				List<String> roles = new ArrayList<>();
				roles.add("SOCIAL_USER");
				user.setRoles(roles);
				userRepository.save(user);
			}
			dbUser = userService.findByUsername(user.getUsername());
			int addMinuteTime = 240;
			Date ExpirationTime = new Date(); // now
			ExpirationTime = DateUtils.addMinutes(ExpirationTime, addMinuteTime);
			token = Jwts.builder().setSubject(user.getUsername()).claim("roles", dbUser.getRoles())
					.setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, "secretkey")
					.setExpiration(ExpirationTime).compact();
			tokenMap.put("token", token);
			tokenMap.put("user", dbUser);
			result.setResult(tokenMap);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("Login Successful");
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while user login.", e);
		}

		return ResponseEntity.ok(result);
	}
	}
}
