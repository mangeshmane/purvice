package com.giftcard.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;

@Controller
public class UploadController {

	private static final Logger logger = LoggerFactory.getLogger(UploadController.class);

	@Value(value = "${server.file.folder}")
	private String UPLOADED_FOLDER;

	@Value(value = "${server.url}")
	private String SERVER_URL;

	@Value(value = "${server.port}")
	private String SERVER_PORT;
	
	@Value(value="${server.contextPath}")
	private String CONTEXT_PATH;

	@GetMapping("/")
	public String index() {
		return "upload";
	}

	@PostMapping("/upload")
	public ResponseEntity<ResultWrapper<String>> upload(@RequestParam("file") MultipartFile file,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {

		ResultWrapper<String> result = new ResultWrapper<String>();
		String fileUrl = null;
		if (file.isEmpty()) {
			result.setMessage(Result.FAIL);
			result.setExplanation("No file selected.");

			return ResponseEntity.ok().body(result);
		}

		try {
			String folder = java.util.UUID.randomUUID().toString();
			String folderPath = UPLOADED_FOLDER + folder;
			String filePath = folderPath + "/" + file.getOriginalFilename();
			fileUrl = SERVER_URL + ":" + SERVER_PORT + CONTEXT_PATH+ "/file/" + folder + "/" + file.getOriginalFilename();
			File dir = new File(folderPath);
			dir.mkdir();

			byte[] bytes = file.getBytes();
			Path path = Paths.get(filePath);
			Files.write(path, bytes);

			result.setMessage(Result.SUCCESS);
			result.setResult(fileUrl);
			result.setExplanation("File uploaded successfully.");

		} catch (IOException e) {
			logger.error("File uplaod fail", e);
			result.setMessage(Result.FAIL);
			result.setExplanation("File uplaod fail.");
			return ResponseEntity.ok().body(result);
		}
		return ResponseEntity.ok().body(result);
	}


	@RequestMapping(value = "/file/{foldername}/{filename:.+}", method = RequestMethod.GET) // //new annotation since 4.3
	public  ResponseEntity<Resource> download(@PathVariable(value="") String foldername, 
			@PathVariable String filename, HttpServletRequest request, HttpServletResponse response) throws FileNotFoundException {
		String filePath = UPLOADED_FOLDER + foldername + "/" + filename;

		File file = new File(filePath);

		ServletContext context = request.getServletContext();
		String mimeType = context.getMimeType(filePath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}
		response.setContentType(mimeType);

		
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		if (!mimeType.contains("image")) {
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", filename);
			headers.add(headerKey, headerValue);
		}
		return ResponseEntity.ok().headers(headers)
				// .contentType(new MediaType(mimeType))
				.contentLength(file.length()).body(resource);
	}

}
