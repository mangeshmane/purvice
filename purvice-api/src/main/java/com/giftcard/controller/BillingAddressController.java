package com.giftcard.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.giftcard.model.BillingAddress;
import com.giftcard.model.States;
import com.giftcard.model.User;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.BillingAddressService;
import com.giftcard.service.UserService;

@RestController
@RequestMapping("/billing-address")
public class BillingAddressController {

	@Autowired
	private BillingAddressService billingAddressService;
	
	@Autowired
	private UserService userService;
	
	private static final Logger logger = LoggerFactory.getLogger(BillingAddressController.class);
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<ResultWrapper<BillingAddress>> saveAddress(@RequestBody BillingAddress billingAddress) {

		ResultWrapper<BillingAddress> result = new ResultWrapper<BillingAddress>();

		
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = userService.findByUsername(auth.getName());
			
			BillingAddress dbBillingAddress = billingAddressService.findByUser_id(user.getId());
			if (dbBillingAddress != null) {
				dbBillingAddress.setAddressLine1(billingAddress.getAddressLine1());
				dbBillingAddress.setAddressLine2(billingAddress.getAddressLine2());
				dbBillingAddress.setCity(billingAddress.getCity());
				dbBillingAddress.setCountry(billingAddress.getCountry());
				dbBillingAddress.setState(billingAddress.getState());
				dbBillingAddress.setZipCode(billingAddress.getZipCode());
				BillingAddress newBillingAddress = billingAddressService.save(dbBillingAddress);
				result.setResult(newBillingAddress);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Billing Addreess updated");
			} else {
				billingAddress.setUser(user);
				BillingAddress newBillingAddress = billingAddressService.save(billingAddress);
				result.setResult(newBillingAddress);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Billing Addreess stored successfully");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while creating Category.", e);
		}
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<ResultWrapper<BillingAddress>> readAddress(@PathVariable Long id) {

		ResultWrapper<BillingAddress> result = new ResultWrapper<BillingAddress>();

		try {
			BillingAddress dbBillingAddress = billingAddressService.findByUser_id(id);
			if (dbBillingAddress != null) {
				result.setMessage(Result.SUCCESS);
				result.setResult(dbBillingAddress);
				result.setExplanation("BillingAddress gets successfully");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("No Records Present.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while getting data: " + e.getMessage() + ".");
			logger.error("Error while getting data.", e);
		}
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/states")
	public ResponseEntity<ResultWrapper<List<States>>> readStates() {

		ResultWrapper<List<States>> result = new ResultWrapper<List<States>>();

		try {
			List<States> states = billingAddressService.findAllStates();
			if (states != null && !states.isEmpty()) {
				result.setMessage(Result.SUCCESS);
				result.setResult(states);
				result.setExplanation("totalStates: " + states.size() + ".");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("No Records Present.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while getting data: " + e.getMessage() + ".");
			logger.error("Error while getting data.", e);
		}
		return ResponseEntity.ok(result);
	}
}
