package com.giftcard.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giftcard.model.Orders;
import com.giftcard.model.User;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.UserService;

@RestController
@CrossOrigin
public class PasswordResetController {

	@Autowired
	private UserService userService;

	private static final Logger logger = LoggerFactory.getLogger(PasswordResetController.class);
	
	@RequestMapping(method = RequestMethod.POST, value = "/forgot")
	public ResponseEntity<ResultWrapper<User>> forgetPassword(@RequestBody User user) {

		ResultWrapper<User> result = new ResultWrapper<User>();
		try {

			User dbUser = userService.findByUsername(user.getUsername().toLowerCase());

			if (dbUser != null) {
				userService.resetPassword(user);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Please check your mail to reset your new password.");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("This user does not found.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while forget password." + e.getMessage());
			logger.error("Error while forget password.", e.getMessage());
		}

		return ResponseEntity.ok(result);
	}

	@SuppressWarnings("unused")
	@RequestMapping(method = RequestMethod.GET, value = "/resetPassword")
	public ResponseEntity<ResultWrapper<User>> displayResetPasswordPage(@RequestParam("token") String token) {

		User user = userService.findByPasswordResetToken(token);
		ResultWrapper<User> response = new ResultWrapper<User>();
		
		if (user != null) { // Token found in DB
			List<Orders> order = null;
			user.setOrder(order);
			response.setResult(user);
			response.setExplanation("valid token");
		} else { 
			// Token not found in DB
			response.setMessage(Result.FAIL);
			response.setExplanation("This is an invalid password reset link.");
		}

		return ResponseEntity.ok(response);
		
	}

	@RequestMapping(method = RequestMethod.POST, value = "/resetPassword")
	public ResponseEntity<ResultWrapper<User>> setNewPassword(@RequestParam("token") String token, @RequestBody User user) {

		ResultWrapper<User> result = new ResultWrapper<User>();
		try {
			// Find the user associated with the reset token
			User dbUser = userService.findByPasswordResetToken(token);

			// This should always be non-null but we check just in case
			if (user != null && dbUser!=null) {
				//User resetUser = user.get();
				// Set new password
				String [] dbToken = dbUser.getPasswordResetToken().split(":");
				String dbTime = dbToken[1];
				Long tokenCreateTime = Long.parseLong(dbTime);
				Long currentTime = System.currentTimeMillis();
				if(currentTime- tokenCreateTime < 172800000){  // 172800000 milliseconds = 48 hours
				dbUser.setPassword(user.getPassword());
				// encrypt new password
				dbUser = userService.setPassword(dbUser);
				// Set the reset token to null so it cannot be used again
				dbUser.setPasswordResetToken(null);
				dbUser.setVerify(true);
				// Save user
				userService.save(dbUser);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Password Changed Successfully.");
				userService.sendEmail(dbUser.getUsername(), "Reset Password",
						"Hi " + dbUser.getFirstName() + ", \nYour Password Reset successfully.");
				}else{
					dbUser.setPasswordResetToken(null);
					userService.save(dbUser);
					result.setMessage(Result.FAIL);
					result.setExplanation("Password Reset Link Expired");
				}
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("Password Reset Error. Token Expired");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while reset password." + e.getMessage());
			logger.error("Error while reset password.", e.getMessage());
		}
		return ResponseEntity.ok(result);
	}
}
