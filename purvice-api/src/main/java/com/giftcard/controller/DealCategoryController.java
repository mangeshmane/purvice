package com.giftcard.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.giftcard.model.DealCategory;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.DealCategoryService;

@RestController
@RequestMapping("/dealcategory")
public class DealCategoryController {

	private static final String CSV_DEFAULT_PATH = "/giftcard-files/csv_files";

	@Autowired
	private DealCategoryService dealCategoryService;

	private static final Logger logger = LoggerFactory.getLogger(DealCategoryController.class);

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<ResultWrapper<DealCategory>> createDealCategory(@RequestBody DealCategory dealCategory) {

		ResultWrapper<DealCategory> result = new ResultWrapper<DealCategory>();
		try {
			List<DealCategory> dbCategory = dealCategoryService.findOneByDealCategoryname(dealCategory.getCategoryName());
			if (dbCategory != null && dbCategory.size() > 0) {
				result.setMessage(Result.FAIL);
				result.setExplanation("Deal category already exists.");
			} else {
				DealCategory newDealCategory = dealCategoryService.save(dealCategory);
				result.setResult(newDealCategory);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Deal category created successfully.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while creating deal category.", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/createSubCategory", method = RequestMethod.POST)
	public ResponseEntity<ResultWrapper<DealCategory>> createDealSubCategory(@RequestBody DealCategory dealCategory) {
		ResultWrapper<DealCategory> result = new ResultWrapper<DealCategory>();
		try {
			DealCategory newDealCategory = dealCategoryService.save(dealCategory);
			result.setResult(newDealCategory);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("Deal category created successfully.");
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while creating deal category.", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/getAllDealCategory", method = { RequestMethod.GET })
	public ResultWrapper<Collection<DealCategory>> getAllDealCategory(HttpServletRequest request) {
		ResultWrapper<Collection<DealCategory>> result = new ResultWrapper<Collection<DealCategory>>();
		Collection<DealCategory> dealCategory = null;
		try {
			dealCategory = dealCategoryService.fetchMainCategoryForSet(request);

			if (request.getParameter("pageSize") != null) {
				result.setPageSize(Long.valueOf(request.getParameter("pageSize").toString()));
			}

			if (request.getParameter("pageNumber") != null) {
				result.setPageNumber(Long.valueOf(request.getParameter("pageNumber").toString()));
			}

			result.setTolalRecord(dealCategoryService.searchCount(request));
			result.setMessage(Result.SUCCESS);
			result.setResult(dealCategory);

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while fetching deal category.", e);
		}
		return result;
	}

	@RequestMapping(value = "/getAllDealCategorys", method = { RequestMethod.GET })
	public ResultWrapper<Collection<DealCategory>> getAllDealCategorys() {
		ResultWrapper<Collection<DealCategory>> result = new ResultWrapper<Collection<DealCategory>>();
		Collection<DealCategory> dealCategory = null;
		try {
			dealCategory = dealCategoryService.findAllDealCategoryByParentId();
			result.setMessage(Result.SUCCESS);
			result.setResult(dealCategory);

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while fetching deal category.", e);
		}
		return result;
	}

	@RequestMapping(value = "/allDealCategory", method = { RequestMethod.GET })
	public ResultWrapper<Collection<DealCategory>> allDealCategory() {
		ResultWrapper<Collection<DealCategory>> result = new ResultWrapper<Collection<DealCategory>>();
		Collection<DealCategory> dealCategory = null;
		try {
			dealCategory = dealCategoryService.findAllDealCategory();
			result.setMessage(Result.SUCCESS);
			result.setResult(dealCategory);

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while fetching deal category.", e);
		}
		return result;
	}

	@RequestMapping(value = "/getDealCategoryByParentId", method = { RequestMethod.GET })
	public ResultWrapper<Collection<DealCategory>> getDealCategoryByParentId(@RequestParam Long parentId) {
		ResultWrapper<Collection<DealCategory>> result = new ResultWrapper<Collection<DealCategory>>();
		Collection<DealCategory> dealCategory = null;
		try {
			dealCategory = dealCategoryService.fetchDealCategoryByParentId(parentId);
			result.setMessage(Result.SUCCESS);
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while fetching deal category by parent id.", e);
		}
		result.setResult(dealCategory);
		return result;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public ResponseEntity<ResultWrapper<List<DealCategory>>> deleteByDealCategoryId(
			@RequestParam Long dealCategoryId) {
		ResultWrapper<List<DealCategory>> result = new ResultWrapper<List<DealCategory>>();
		DealCategory dealCategory = null;
		try {
			dealCategory = dealCategoryService.findById(dealCategoryId);
			if(dealCategory != null){
				dealCategoryService.delete(dealCategoryId);		
				result.setMessage(Result.SUCCESS);
				result.setExplanation("delete deal category by: " + dealCategory.getCategoryName() + ".");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while deleting deal category: " + e.getMessage() + ".");
			logger.error("Error while deleting deal category.", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/updateDealCategory", method = RequestMethod.PUT)
	public ResponseEntity<ResultWrapper<DealCategory>> updateDealAlert(@RequestBody DealCategory dealCategory, HttpServletRequest request) {

		ResultWrapper<DealCategory> result = new ResultWrapper<DealCategory>();
		DealCategory dealCategoryResult = null; 
		try{
			DealCategory tempdealCategory = dealCategoryService.findById(dealCategory.getId());
			dealCategoryResult = dealCategoryService.updateDealCategory(dealCategory, tempdealCategory);
			result.setResult(dealCategoryResult);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("Deal Category update successfully.");
		}catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("error = " + e);
			logger.error("Error", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/importCSVFile", method = { RequestMethod.POST }, headers = "Accept=*/*")
	public ResultWrapper<String> importCSVFile(HttpServletResponse response, MultipartHttpServletRequest request)
			throws Exception {
		ResultWrapper<String> result = new ResultWrapper<String>();

		File directory = new File(CSV_DEFAULT_PATH);
		if (!directory.exists()) {
			directory.mkdirs();
		}
		Iterator<String> itr = request.getFileNames();
		MultipartFile mpf = request.getFile(itr.next());
		String fileName = mpf.getOriginalFilename();
		boolean status = isFileAvailableInCSVDefaultDir(mpf.getOriginalFilename());

		byte[] bytes = null;
		try {
			if (!status) {
				File file = new File(CSV_DEFAULT_PATH + "/" + fileName);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
				bytes = mpf.getBytes();
				stream.write(bytes);
				stream.close();
				result.setMessage(Result.SUCCESS);
				result.setExplanation("File imported successfully.");

			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("File already exist.");
			}
		} catch (IOException e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("error = " + e);
			logger.error("Error", e);
		}
		return result;
	}

	public boolean isFileAvailableInCSVDefaultDir(String fileName) {
		File[] files = new File(CSV_DEFAULT_PATH).listFiles();
		if(files != null) {
			for (File file : files) {
				if (file.getName().equals(fileName)) {
					return true;
				}
			}
		}
		return false;
	}

	@RequestMapping(value = "/importCSVFileData", method = { RequestMethod.GET })
	public ResultWrapper<String> importCSVFileData(String fileName)
			throws Exception {
		ResultWrapper<String> result = new ResultWrapper<String>();
		try {
			File file = new File(CSV_DEFAULT_PATH + "/" + fileName);
			dealCategoryService.addCSVDataToDB(file);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("File imported successfully.");
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("error = " + e);
			logger.error("Error", e);
		}
		return result;
	}

	@RequestMapping(value = "/search", method = { RequestMethod.GET })
	public ResultWrapper<Collection<DealCategory>> search(HttpServletRequest request) {
		ResultWrapper<Collection<DealCategory>> result = new ResultWrapper<Collection<DealCategory>>();
		Collection<DealCategory> dealCategory;
		Collection<DealCategory> dealCategorytemp;
		try {
			dealCategory = dealCategoryService.fetchMainCategoryForSet(request);
			if(request.getParameter("pageSize") != null) {
				result.setPageSize(Long.valueOf(request.getParameter("pageSize").toString()));
			}

			if(request.getParameter("pageNumber") != null) {
				result.setPageNumber(Long.valueOf(request.getParameter("pageNumber").toString()));
			}
			dealCategorytemp = dealCategoryService.getDealByIds(dealCategory);

			result.setTolalRecord(dealCategoryService.searchCount(request));
			result.setResult(dealCategorytemp);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return result;
	}

	@RequestMapping(value = "/allsearch", method = RequestMethod.GET)
	public ResultWrapper<Collection<DealCategory>> searchAll(HttpServletRequest request) {
		ResultWrapper<Collection<DealCategory>> result = new ResultWrapper<Collection<DealCategory>>();
		Collection<DealCategory> dealData = null;
		try {
			dealData =  dealCategoryService.fetchAllDealCategoryTree(request);
			if(request.getParameter("pageSize") != null) {
				result.setPageSize(Long.valueOf(request.getParameter("pageSize").toString()));
			}

			if(request.getParameter("pageNumber") != null) {
				result.setPageNumber(Long.valueOf(request.getParameter("pageNumber").toString()));
			}

			result.setMessage(Result.SUCCESS);
			result.setResult(dealData);
		}catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while fetch all deal: " + e.getMessage() + ".");
			logger.error("Error while fetch all deal.", e);
		}
		return result;
	}

	@RequestMapping(value = "/dealbyid", method = { RequestMethod.GET })
	public ResultWrapper<DealCategory> getDealById(@RequestParam Long id) {
		ResultWrapper<DealCategory> result = new ResultWrapper<DealCategory>();
		DealCategory dealCategory = null;
		try {
			dealCategory = dealCategoryService.getDealById(id);
			result.setMessage(Result.SUCCESS);
			result.setResult(dealCategory);

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while fetching deal category.", e);
		}
		return result;
	}

	@RequestMapping(value = "/searchDealCategory", method = { RequestMethod.GET })
	public ResultWrapper<Collection<DealCategory>> searchDealCategoryByKeyword(@RequestParam String keyWord) {
		ResultWrapper<Collection<DealCategory>> result = new ResultWrapper<Collection<DealCategory>>();
		Collection<DealCategory> dealCategory = new ArrayList<DealCategory>();
		try {
			dealCategory = dealCategoryService.findOneByDealCategory(keyWord);
			result.setMessage(Result.SUCCESS);
			result.setResult(dealCategory);
			result.setTolalRecord((long) dealCategory.size());

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while fetching deal category.", e);
		}
		return result;
	}

	@RequestMapping(value = "/treeHierarchy", method = { RequestMethod.GET })
	public ResultWrapper<Collection<DealCategory>> fetchAllTreeHierarchy() {
		ResultWrapper<Collection<DealCategory>> result = new ResultWrapper<Collection<DealCategory>>();
		Collection<DealCategory> dealCategory;
		try {
			dealCategory = dealCategoryService.fetchAllTreeHierarchy();
			result.setResult(dealCategory);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return result;
	}

	@RequestMapping(value = "/getDealCategoryByName", method = { RequestMethod.GET })
	public ResultWrapper<Collection<DealCategory>> getDealCategoryByName(@RequestParam String categoryName) {
		ResultWrapper<Collection<DealCategory>> result = new ResultWrapper<Collection<DealCategory>>();
		Collection<DealCategory> dealCategory = null;
		try {
			dealCategory = dealCategoryService.findOneByDealCategoryname(categoryName);
			result.setMessage(Result.SUCCESS);
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while fetching deal category by parent id.", e);
		}
		result.setResult(dealCategory);
		return result;
	}

	@RequestMapping(value = "/getDealCategoryByPriority", method = { RequestMethod.GET })
	public ResultWrapper<Collection<DealCategory>> getDealCategoryByPriority() {
		ResultWrapper<Collection<DealCategory>> result = new ResultWrapper<Collection<DealCategory>>();
		Collection<DealCategory> dealCategory = null;
		try {
			dealCategory = dealCategoryService.fetchDealCategoryByPriority();
			result.setMessage(Result.SUCCESS);
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while fetching deal category ", e);
		}
		result.setResult(dealCategory);
		return result;
	}
}
