package com.giftcard.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.giftcard.deal.service.DealFeedbackService;
import com.giftcard.model.DealFeedback;
import com.giftcard.model.User;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;

@Controller
public class DealFeedbackController {

	private static final Logger logger = LoggerFactory.getLogger(OrdersController.class);

	@Autowired
	public DealFeedbackService dealFeedbackService;

	@RequestMapping("/dealFeedback")
	public ResponseEntity<ResultWrapper<List<DealFeedback>>> dealPage(@RequestParam String userId) {
		ResultWrapper<List<DealFeedback>> result = new ResultWrapper<List<DealFeedback>>();
		try {
			User user = new User();
			user.setId(Long.parseLong(userId));
			List<DealFeedback> dealFeedback = dealFeedbackService.findByUser(user);
			result.setResult(dealFeedback);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("DealFeedback fetched sucessfully");
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
		}
		return ResponseEntity.ok(result);
	}
}
