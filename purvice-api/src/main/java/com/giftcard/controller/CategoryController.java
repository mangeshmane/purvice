package com.giftcard.controller;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.giftcard.model.Category;

import com.giftcard.model.Item;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.CategoryService;
import com.giftcard.service.ItemService;

@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	
	
	@Autowired
	private ItemService itemService;

	private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<ResultWrapper<Category>> createCategory(@RequestBody Category category) {

		ResultWrapper<Category> result = new ResultWrapper<Category>();

		try {
			Category dbCategory = categoryService.findOneByCategoryname(category.getCategoryname());
			if (dbCategory != null) {
				result.setMessage(Result.FAIL);
				result.setExplanation("Category already exists.");
			} else {
				Category newCategory = categoryService.save(category);
				result.setResult(newCategory);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Category created successfully.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while creating Category.", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<ResultWrapper<Category>> updateCategory(@RequestBody Category category) {

		ResultWrapper<Category> result = new ResultWrapper<Category>();

		try {
			Category dbCategory = categoryService.findOne(category.getId());
			if (dbCategory != null) {
				Category newCategory = categoryService.save(category);
				result.setResult(newCategory);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Category updated successfully.");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("Category does not exists.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while updating Category.", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<ResultWrapper<Category>> readCategory(@PathVariable Long id) {
		ResultWrapper<Category> result = new ResultWrapper<Category>();

		try {
			Category dbCategory = categoryService.findOne(id);
			if (dbCategory != null) {
				result.setResult(dbCategory);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Category details found.");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("No data found.");
			}

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage() + ".");
			logger.error("Error while reading Category details.", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping("/all")
	public ResponseEntity<ResultWrapper<List<Category>>> readAllCategories() {

		ResultWrapper<List<Category>> result = new ResultWrapper<List<Category>>();

		try {
			List<Category> category = categoryService.findAll();
			if (category != null && !category.isEmpty()) {
				result.setMessage(Result.SUCCESS);
				result.setResult(category);
				result.setExplanation("totalCategories: " + category.size() + ".");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("No Records Present.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while getting data: " + e.getMessage() + ".");
			logger.error("Error while getting data.", e);
		}
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<ResultWrapper<Category>> delete(@PathVariable Long id) {
		ResultWrapper<Category> result = new ResultWrapper<Category>();

		try{
			Category dbCategory = categoryService.findOne(id);
			if(dbCategory != null){
			List<Item> items = itemService.findByCategory_id(dbCategory.getId());
			Iterator<Item> itemIterator = items.iterator();
			while (itemIterator.hasNext()) {
				Item tempItem = itemIterator.next();
				List<Category> categories = tempItem.getCategory();
				categories.remove(dbCategory);
				tempItem.setCategory(categories);
				itemService.save(tempItem);
			}
			
				categoryService.delete(dbCategory.getId());
				result.setResult(dbCategory);
				result.setMessage(Result.SUCCESS);	
				result.setExplanation("Category deleted sucessfully.");
			}else{
				result.setMessage(Result.FAIL);
				result.setExplanation("Category details not found.");
			}
			
		}catch(Exception e){
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage()+".");
			logger.error("Error while deleting Category.",e);
		}
		return  ResponseEntity.ok(result);
	}

	
}
