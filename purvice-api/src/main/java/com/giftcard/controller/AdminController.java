package com.giftcard.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.giftcard.model.User;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.UserService;
import com.giftcard.util.ChangePasswordUtil;

@RestController
@RequestMapping(value = "/user")
public class AdminController {

	@Autowired
	private UserService userService;

	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

	/**
	 * Web service for getting all the appUsers in the application.
	 * 
	 * @return list of all AppUser
	 */
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<ResultWrapper<List<User>>> users() {

		ResultWrapper<List<User>> result = new ResultWrapper<List<User>>();

		try {
			List<User> users = userService.findAll();
			
			if (users != null && !users.isEmpty()) {
				result.setMessage(Result.SUCCESS);
				result.setResult(users);
				result.setExplanation("totalUsers: " + users.size() + ".");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("No Records Present.");
			}
		}catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while getting data: " + e.getMessage() + ".");
			logger.error("Error while getting data.", e);
		}
		return ResponseEntity.ok(result);
	}

	/**
	 * Web service for getting a user by his ID
	 * 
	 * @param id
	 *            appUser ID
	 * @return appUser
	 */

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping( value="/{id}",method = RequestMethod.GET)
	public ResponseEntity<ResultWrapper<User>> userById(@PathVariable Long id) {

		ResultWrapper<User> result = new ResultWrapper<User>();

		try {

			// long newId= Integer.parseInt(id);

			User dbUser = userService.findOne(id);
			if (dbUser != null) {
				result.setResult(dbUser);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("User details found.");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("No data found.");
			}

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage() + ".");
			logger.error("Error while reading User details.", e);
		}
		return ResponseEntity.ok(result);
	}

	/**
	 * Method for deleting a user by his ID
	 * 
	 * @param id
	 * @return
	 */
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ResultWrapper<User>> deleteUser(@PathVariable Long id) {

		ResultWrapper<User> result = new ResultWrapper<User>();

		try {
			User dbUser = userService.findOne(id);
			if (dbUser != null) {
				userService.delete(dbUser.getId());
				result.setMessage(Result.SUCCESS);
				result.setExplanation("User deleted sucessfully.");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("User details not found.");
			}

		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage() + ".");
			logger.error("Error while deleting User.", e);
		}
		return ResponseEntity.ok(result);
	}

	/**
	 * Method for adding a appUser
	 * 
	 * @param appUser
	 * @return
	 */
	/**
	 * Method for editing an user details
	 * 
	 * @param appUser
	 * @return modified appUser
	 */
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<ResultWrapper<User>> updateUser(@RequestBody User appUser) {

		ResultWrapper<User> result = new ResultWrapper<User>();

		try {

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = userService.findByUsername(auth.getName());
			String userRole=null;
			for(String role : user.getRoles()){
				if(role!=null){
					userRole=role;
				}
			}
			if (userRole.equals("USER") || userRole.equals("SOCIAL_USER")) {
				if (user != null && user.getUsername().equals(appUser.getUsername())) {
					user.setFirstName(appUser.getFirstName());
					user.setLastName(appUser.getLastName());
					user.setUpdateDate(new Date());
					user = userService.save(user);
					result.setResult(user);
					result.setMessage(Result.SUCCESS);
					result.setExplanation("User updated successfully.");
				} else {
					result.setMessage(Result.FAIL);
					result.setExplanation("User does not exists.");
				}
			} else {
				User dbuser = userService.findByUsername(appUser.getUsername());
				if (dbuser != null) {

					if(dbuser.getRoles().contains("ADMIN")){
					dbuser.setFirstName(appUser.getFirstName());
					dbuser.setLastName(appUser.getLastName());
					}
					dbuser.setRoles(appUser.getRoles());
					dbuser.setVerify(true);
					appUser.setUpdateDate(new Date());
					dbuser = userService.save(dbuser);
					result.setResult(dbuser);
					result.setMessage(Result.SUCCESS);
					result.setExplanation("User updated successfully.");
				} else {
					result.setMessage(Result.FAIL);
					result.setExplanation("User does not exists.");
				}
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while updating User.", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/changePassword")
	public ResponseEntity<ResultWrapper<User>> changePassword(@RequestBody ChangePasswordUtil userInfoDTO) {
		ResultWrapper<User> result = new ResultWrapper<User>();
		try {
			User user = userService.changePassword(userInfoDTO);
			if (user != null) {
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Password changed Successfully.");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("Password not matched");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while changing password." + e.getMessage());
			logger.error("Error while changing password.", e.getMessage());
		}
		return ResponseEntity.ok(result);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<ResultWrapper<User>> createAdmin(@RequestBody User appUser) {

		ResultWrapper<User> result = new ResultWrapper<User>();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User admin = userService.findByUsername(auth.getName());
		String adminRole=null;
		for(String role : admin.getRoles()){
			if(role!=null){
				adminRole=role;
			}
		}
		
		if (adminRole.equals("ADMIN")) {
			try {
				User dbUser = userService.findByUsername(appUser.getUsername());

				if (dbUser != null) {
					result.setMessage(Result.FAIL);
					result.setExplanation("Email address already used.Please use different Email address.");
				} else {
					User user = userService.createAdminInfo(appUser, admin.getFirstName());
					userService.save(user);
					result.setResult(user);
					result.setMessage(Result.SUCCESS);
					result.setExplanation("Admin created successfully");
				}

			} catch (Exception e) {
				result.setMessage(Result.FAIL);
				result.setExplanation(e.getMessage());
				logger.error("Error while creating User.", e);
			}
		} else {
			result.setMessage(Result.FAIL);
			result.setExplanation("You dont have authority to create Admin");
		}
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/update-email", method = RequestMethod.PUT)
	public ResponseEntity<ResultWrapper<User>> updateEmail(@RequestBody User appUser) {

		ResultWrapper<User> result = new ResultWrapper<User>();

		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			 User user = userService.findOne(appUser.getId());
				if (user != null && auth.getName().equals(user.getUsername())) {
					user.setEmailId(appUser.getEmailId());
					user = userService.save(user);
					result.setResult(user);
					result.setMessage(Result.SUCCESS);
					result.setExplanation("Email id updated successfully.");
				} else {
					result.setMessage(Result.FAIL);
					result.setExplanation("User id does not exist or U dont have permission to change emailId");
				}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while updating Email.", e);
		}
		return ResponseEntity.ok(result);
	}
}
