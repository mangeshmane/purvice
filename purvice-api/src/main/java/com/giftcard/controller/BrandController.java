
package com.giftcard.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.giftcard.model.Brand;
import com.giftcard.repository.BrandRepository;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.BrandService;

@RestController
@RequestMapping("/brand")
public class BrandController {

	@Autowired
	private BrandService brandService;

	@Autowired
	private BrandRepository brandRepository;

	private static final Logger logger = LoggerFactory.getLogger(BrandController.class);

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<ResultWrapper<Brand>> createBrand(@RequestBody Brand brand) {

		ResultWrapper<Brand> result = new ResultWrapper<Brand>();

		try {

			Brand dbBrand = brandService.findByBrandName(brand.getBrandName());

			if (dbBrand != null) {
				result.setMessage(Result.FAIL);
				result.setExplanation("Brand already exists.");
			} else {
				brandService.save(brand);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Brand created successfully.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while creating Brand.", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<ResultWrapper<Brand>> updateBrand(@RequestBody Brand brand) {

		ResultWrapper<Brand> result = new ResultWrapper<Brand>();

		try {
			Brand dbBrand = brandService.findById(brand.getId());

			if (dbBrand != null) {
				Brand newBrand = brandRepository.save(brand);
				result.setResult(newBrand);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Brand updated successfully.");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("Brand does not exists.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while updating Brand.", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping("/search")
	public ResponseEntity<ResultWrapper<List<Brand>>> readAllBrands() {

		ResultWrapper<List<Brand>> result = new ResultWrapper<List<Brand>>();

		try {
			List<Brand> brands = brandService.findAll();
			if (brands != null && !brands.isEmpty()) {
				result.setMessage(Result.SUCCESS);
				result.setResult(brands);
				result.setExplanation("totalBrands: " + brands.size() + ".");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("No Records Present.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while getting data: " + e.getMessage() + ".");
			logger.error("Error while getting data.", e);
		}
		return ResponseEntity.ok(result);
	}

}
