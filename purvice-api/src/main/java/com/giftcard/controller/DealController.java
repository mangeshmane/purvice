package com.giftcard.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giftcard.Application;
import com.giftcard.deal.service.DealCommentService;
import com.giftcard.deal.service.DealFeedbackService;
import com.giftcard.deal.service.DealService;
import com.giftcard.deals.model.SDModel;
import com.giftcard.model.Deal;
import com.giftcard.model.DealComment;
import com.giftcard.model.DealFeedback;
import com.giftcard.response.ResponseWrapper;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.scrapper.job.ScrapperServiceImpl;

@RestController
public class DealController {

	private static final Logger logger = LoggerFactory.getLogger(OrdersController.class);

	@Autowired
	private DealService dealService;
	
	@Autowired
	private ScrapperServiceImpl  scrapperServiceImpl;

	@Autowired
	public DealFeedbackService dealFeedbackService;

	@Autowired
	public DealCommentService dealCommentService;

	private long time = Application.getTime().getTime();

	Map<String, List<Deal>> cacheMap = new HashMap();
	
	List<Deal> cacheSearchAllDeal = new ArrayList<Deal>();
	
	private int cacheSearchAllDealIndex = 0;

	@Value( "${cache.value}" )
	private Integer CACHE_VALUE;

	@PostConstruct
	public void init(){
		createCache();
	}

	public void createCache() {
		cacheMap.clear();
		//cacheSearchAllDeal.clear();
		List<Deal> dealData = dealService.getAllHotDeal();
		//cacheSearchAllDeal = dealService.getAllHotDealWithtext();
		int count = 0;
		int cnt = 1;
		ArrayList<Deal> tempDealData= null;
		for (Deal deal : dealData) {
			count++;
			if(count == 1) {
				tempDealData = new ArrayList<Deal>();
			}
			tempDealData.add(deal);
			if(count == CACHE_VALUE) {
				cacheMap.put(cnt+CACHE_VALUE.toString(), tempDealData);
				count = 0;
				cnt++;
			}
		}
	}

	@RequestMapping(value = "/deal/create", method = RequestMethod.POST)
	public Deal createDaal(@RequestBody Deal Deal) {
		Deal dealCreated =  dealService.saveDeal(Deal);
		return dealCreated; 
	}

	@RequestMapping("/deal/{dealid}")
	public Deal getDeal(@PathVariable Long dealid) {
		Deal deal =  dealService.findById(dealid);
		return deal; 
	}

	@RequestMapping("/deals")
	public ResponseWrapper<List<Deal>> dealPage(HttpServletRequest request) {
		ResponseWrapper<List<Deal>> result = new ResponseWrapper<List<Deal>>();
		List<Deal> tempDeal = new ArrayList<Deal>();
		try {
			if(request.getParameter("frmSearchParam")== null && request.getParameter("customerReviewAverage")== null && request.getParameter("source")== null
					&& request.getParameter("id")== null  && request.getParameter("categoryId")== null && request.getParameter("dealCategoryId")== null && !cacheMap.isEmpty()) {

				tempDeal = cacheMap.get(request.getParameter("pageNumber")+request.getParameter("pageSize"));

				if (request.getParameter("pageSize") != null) {
					result.setPageSize(Integer.valueOf(request.getParameter("pageSize").toString()));
				}

				if (request.getParameter("pageNumber") != null) {
					result.setPageNumber(Integer.valueOf(request.getParameter("pageNumber").toString()));
				}

				result.setData(tempDeal);
				result.setHasNextPage(true);
				result.setHasPreviousPage(false);

			}else {
				result = dealService.getDealWithPaginationAndFilter(request);
			}
			result.setMessage(Result.SUCCESS.toString());
			result.setDetailedMessage("New deals will arrive in =" + time);
		} catch (Exception e) {
			result.setMessage(Result.FAIL.toString());
			result.setDetailedMessage(e.getMessage());
		}
		return result;
	}

	@RequestMapping("/dealsData")
	public ResponseWrapper<List<Deal>> fetchAllDeal() {
		ResponseWrapper<List<Deal>> result = new ResponseWrapper<List<Deal>>();
		List<Deal> dealData = null;
		try {
			dealData = dealService.getAllHotDeal();
			result.setData(dealData);
			result.setMessage(Result.SUCCESS.toString());
			result.setDetailedMessage("New deals will arrive in =" + time);
		} catch (Exception e) {
			result.setMessage(Result.FAIL.toString());
			result.setDetailedMessage(e.getMessage());
		}
		return result;
	}

	@RequestMapping(value = "deals/create", method = RequestMethod.POST)
	public ResultWrapper<Deal> createDeal(@RequestBody Deal deal) {

		ResultWrapper<Deal> result = new ResultWrapper<Deal>();

		try {


		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while storing information", e);
		}
		return result;
	}

	@RequestMapping(value = "deals/getDealsByCategories", method = RequestMethod.POST)
	public ResponseWrapper<List<Deal>> dealPage(HttpServletRequest request, @RequestBody Deal category) {
		ResponseWrapper<List<Deal>> result = new ResponseWrapper<List<Deal>>();
		try {
			result = dealService.getDealWithPaginationAndFilterPost(request,category);
			result.setMessage(Result.SUCCESS.toString());
			result.setDetailedMessage("Get deal successfully.");
		} catch (Exception e) {
			result.setMessage(Result.FAIL.toString());
			result.setDetailedMessage(e.getMessage());
		}
		return result;
	}

	@RequestMapping(value = "deals/globalsearchbyscrap", method = RequestMethod.GET)
	public ResponseWrapper<List<SDModel>> globalSearchByScrapping(HttpServletRequest request) {
		ResponseWrapper<List<SDModel>> result = new ResponseWrapper<List<SDModel>>();
		List<SDModel> dealAlertData = null;
		try {
			dealAlertData = scrapperServiceImpl.getMapOfDataByScrapper(request);	
			if (request.getParameter("pageSize") != null) {
				result.setPageSize(Integer.valueOf(request.getParameter("pageSize").toString()));
			}

			if (request.getParameter("pageNumber") != null) {
				result.setPageNumber(Integer.valueOf(request.getParameter("pageNumber").toString()));
			}
			//result.setTotalNumberOfRecords(Long(dealAlertData.size()));
			result.setData(dealAlertData);
			result.setHasNextPage(true);
			result.setMessage(Result.SUCCESS.toString());
			result.setDetailedMessage("New deals will arrive in =" + time);
		} catch (Exception e) {
			result.setMessage(Result.FAIL.toString());
			result.setDetailedMessage("Error while fetch all deal alert: " + e.getMessage() + ".");
			logger.error("Error while fetch all deal alert.", e);
		}
		return result;
	}


	@RequestMapping(value = "deals/globalsearch", method = RequestMethod.GET)
	public ResponseWrapper<List<SDModel>> globalSearch(HttpServletRequest request) {
		ResponseWrapper<List<SDModel>> result = new ResponseWrapper<List<SDModel>>();
		List<SDModel> dealAlertData = null;
		Long totalCount = 0L;
		Long totalPage = 0L;
		try {
			dealAlertData = dealService.findKeyWordInDeal(request);	
			totalCount = dealService.getTotalCount(request);
			if (request.getParameter("pageSize") != null) {
				result.setPageSize(Integer.valueOf(request.getParameter("pageSize").toString()));
			}

			if (request.getParameter("pageNumber") != null) {
				result.setPageNumber(Integer.valueOf(request.getParameter("pageNumber").toString()));
			}
			result.setTotalNumberOfRecords(totalCount);
			result.setData(dealAlertData);
			totalPage = totalCount / 50;
			Long remainder = totalCount % 50;
			if(remainder >= 1){
				totalPage = totalPage + 1;
			}
			if(totalPage > Long.valueOf(request.getParameter("pageNumber").toString()))
				result.setHasNextPage(true);
			else
				result.setHasNextPage(false);
			result.setMessage(Result.SUCCESS.toString());
			result.setDetailedMessage("New deals will arrive in =" + time);
		} catch (Exception e) {
			result.setMessage(Result.FAIL.toString());
			result.setDetailedMessage("Error while fetch all deal alert: " + e.getMessage() + ".");
			logger.error("Error while fetch all deal alert.", e);
		}
		return result;
	}

	@RequestMapping(value = "deals/fetchbyid", method = RequestMethod.GET)
	public ResultWrapper<List<Deal>> fetchDealByIds(@RequestParam List<Long> dealId) {
		ResultWrapper<List<Deal>> result = new ResultWrapper<List<Deal>>();
		List<Deal> dealData = null;
		try {
			dealData = dealService.getDealByIds(dealId);		
			result.setResult(dealData);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("Fetch all deal.");
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while fetch all deal: " + e.getMessage() + ".");
			logger.error("Error while fetch all deal.", e);
		}
		return result;
	}

	@RequestMapping(value = "deals/updateDeal", method = RequestMethod.PUT)
	public ResultWrapper<Deal> updateDeal(@RequestBody Deal deal, @RequestParam String feedbackType, @RequestParam String userId, @RequestParam String updatefeedbackFlag) {

		ResultWrapper<Deal> result = new ResultWrapper<Deal>();
		Deal dealResult = null; 
		Deal tempDeal = null;
		try{
			if(deal.getId() != null)
				tempDeal = dealService.findById(deal.getId()); 
			else
				tempDeal = dealService.findByUniqueId(deal.getUniqueId()); 
			if(tempDeal == null){ 
				deal.setCategoryId(scrapperServiceImpl.getCategoryIdForDeal(deal.getCategory()));  
				tempDeal = dealService.saveDeal(deal);
			}
			dealResult = dealService.updateDeal(deal, tempDeal, feedbackType, Boolean.parseBoolean(updatefeedbackFlag), Long.parseLong(userId));
			DealFeedback dealFeedback = dealFeedbackService.save(deal, feedbackType, Long.parseLong(userId));
			result.setResult(dealResult);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("Deal alert changes have been saved.");
		}catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("error = " + e);
			logger.error("Error", e);
		}
		return result;
	}

	@RequestMapping(value = "deals/dealsort", method = RequestMethod.GET)
	public ResponseWrapper<List<Deal>> dealSort(HttpServletRequest request) {
		ResponseWrapper<List<Deal>> result = new ResponseWrapper<List<Deal>>();
		List<Deal> dealAlertData = null;
		Long totalCount = 0L;
		Long totalPage = 0L;
		try {
			dealAlertData = dealService.getDealSortedData(request);	
			totalCount = dealService.getTotalDealCount(request);
			if (request.getParameter("pageSize") != null) {
				result.setPageSize(Integer.valueOf(request.getParameter("pageSize").toString()));
			}

			if (request.getParameter("pageNumber") != null) {
				result.setPageNumber(Integer.valueOf(request.getParameter("pageNumber").toString()));
			}
			result.setTotalNumberOfRecords(totalCount);
			result.setData(dealAlertData);
			totalPage = totalCount / 50;
			Long remainder = totalCount % 50;
			if(remainder >= 1){
				totalPage = totalPage + 1;
			}
			if(totalPage > Long.valueOf(request.getParameter("pageNumber").toString()))
				result.setHasNextPage(true);
			else
				result.setHasNextPage(false);
			result.setMessage(Result.SUCCESS.toString());
			result.setDetailedMessage("New deals will arrive in =" + time);
		} catch (Exception e) {
			result.setMessage(Result.FAIL.toString());
			result.setDetailedMessage("Error while fetch all deal alert: " + e.getMessage() + ".");
			logger.error("Error while fetch all deal alert.", e);
		}
		return result;
	}

	@RequestMapping(value = "deals/fetchbytextsearch", method = RequestMethod.GET)
	public ResultWrapper<List<Deal>> fetchDealBysearchText(HttpServletRequest request) {
		ResultWrapper<List<Deal>> result = new ResultWrapper<List<Deal>>();
		List<Deal> dealData = new ArrayList<Deal>();
		try {
			if(request.getParameter("pageNumber").equals("1")) {
				this.cacheSearchAllDealIndex = 0;
			}
		if(request.getParameter("searchText") != null && request.getParameter("searchText").length() >= 4) {
			for (int i = this.cacheSearchAllDealIndex; i < this.cacheSearchAllDeal.size(); i++) {
				 int isFound = this.cacheSearchAllDeal.get(i).getText().indexOf(request.getParameter("searchText"));
				 if(isFound != -1 && this.cacheSearchAllDealIndex != this.cacheSearchAllDeal.indexOf(this.cacheSearchAllDeal.get(i))) {
					 dealData.add(this.cacheSearchAllDeal.get(i));
					 this.cacheSearchAllDealIndex = this.cacheSearchAllDeal.indexOf(this.cacheSearchAllDeal.get(i));
				 }
				 
				 if(dealData != null && dealData.size() == 5) {
					 break;
				 }
			}
		}
			if(request.getParameter("pageSize") != null) {
				result.setPageSize(Long.valueOf(request.getParameter("pageSize").toString()));
			}

			if(request.getParameter("pageNumber") != null) {
				result.setPageNumber(Long.valueOf(request.getParameter("pageNumber").toString()));
			}
			result.setTolalRecord(Long.valueOf(this.cacheSearchAllDeal.size()));
			result.setMessage(Result.SUCCESS);
			result.setResult(dealData);
		}catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while fetch all deal: " + e.getMessage() + ".");
			logger.error("Error while fetch all deal.", e);
		}
		return result;
	}
	
/*	@RequestMapping(value = "deals/fetchbytextsearch", method = RequestMethod.GET)
	public ResultWrapper<List<Deal>> fetchDealBysearchText(HttpServletRequest request) {
		ResultWrapper<List<Deal>> result = new ResultWrapper<List<Deal>>();
		List<Deal> dealData = new ArrayList<Deal>();
		try {
			if(request.getParameter("pageNumber").equals("1")) {
				this.cacheSearchAllDealIndex = 0;
			}
		if(request.getParameter("searchText") != null && request.getParameter("searchText").length() >= 4) {
			for (int i = this.cacheSearchAllDealIndex; i < this.cacheSearchAllDeal.size(); i++) {
				 int isFound = this.cacheSearchAllDeal.get(i).getText().indexOf(request.getParameter("searchText"));
				 if(isFound != -1) {
					 Deal deal = new Deal();
					 deal.setSource(this.cacheSearchAllDeal.get(i).getSource());
					
						 if(this.cacheSearchAllDeal.get(i).getText().length() <= 40) {
							 deal.setText(this.cacheSearchAllDeal.get(i).getText());
						 }else {
							 String prev = new String();
							 String last = new String();
						   for(int j = isFound; j >= 0; j--){
							 if(Character.isWhitespace(this.cacheSearchAllDeal.get(i).getText().charAt(j)) || j == 0){
								  prev = this.cacheSearchAllDeal.get(i).getText().substring(0, j);
								  last = this.cacheSearchAllDeal.get(i).getText().substring(j);
								  break;
							 }
						   }
						 if(prev.length() >= 40) {
								 String[] currencies = last.split(" ");
								 prev = prev+" "+currencies[1];
								 int startIndex = prev.length() - 40;
								 deal.setText(prev.substring(startIndex, prev.length()));
							 }else if(last.length() >= 40) {
								 deal.setText(last.substring(0,40));
							 }else if (prev.length() < 40 && last.length() < 40) {
								 if(last.length() < 10) {
									 deal.setText(prev+last);
								 }else {
									 deal.setText(last);
								 }
							 }
						 }
					 dealData.add(deal);
					 this.cacheSearchAllDealIndex = this.cacheSearchAllDeal.indexOf(this.cacheSearchAllDeal.get(i));
				 }
				 
				 if(dealData != null && dealData.size() == 5) {
					 break;
				 }
			}
		}
			if(request.getParameter("pageSize") != null) {
				result.setPageSize(Long.valueOf(request.getParameter("pageSize").toString()));
			}

			if(request.getParameter("pageNumber") != null) {
				result.setPageNumber(Long.valueOf(request.getParameter("pageNumber").toString()));
			}
			result.setTolalRecord(Long.valueOf(this.cacheSearchAllDeal.size()));
			result.setMessage(Result.SUCCESS);
			result.setResult(dealData);
		}catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while fetch all deal: " + e.getMessage() + ".");
			logger.error("Error while fetch all deal.", e);
		}
		return result;
	} */
	
	@RequestMapping(value = "deals/saveDealFeedback", method = RequestMethod.POST)
	public ResponseWrapper<List<DealComment>> saveDealFeedbachByUser(@RequestBody DealComment dealComment) {
		ResponseWrapper<List<DealComment>> result = new ResponseWrapper<List<DealComment>>();
		DealComment dealCommentData = new DealComment();
		try {
			dealCommentData = dealCommentService.saveDealFeedbachByUser(dealComment);
			result.setMessage(Result.SUCCESS.toString());
			result.setDetailedMessage("We have received your email and we will get back to you in 24 hours");
		} catch (Exception e) {
			result.setMessage(Result.FAIL.toString());
			result.setDetailedMessage(e.getMessage());
		}
		return result;
	}

	//@CacheEvict(value = "purviceCache", allEntries=true)
	@RequestMapping("/removeRadisCache")
	public ResponseWrapper<List<Deal>> clearCache() {
		ResponseWrapper<List<Deal>> result = new ResponseWrapper<List<Deal>>();
		try {
			result.setMessage(Result.SUCCESS.toString());
			result.setDetailedMessage("Radis cache cleare successfully !");
		} catch (Exception e) {
			result.setMessage(Result.FAIL.toString());
			result.setDetailedMessage(e.getMessage());
		}
		return result;
	}

}
