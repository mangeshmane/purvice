package com.giftcard.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giftcard.deal.service.DealAlertService;
import com.giftcard.model.DealAlert;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;

@RestController
@RequestMapping("/dealalert")
public class DealAlertController {

	@Autowired
	private DealAlertService dealAlertService;

	private static final Logger logger = LoggerFactory.getLogger(DealAlertController.class);

	@RequestMapping(value = "/dealalertsave", method = RequestMethod.POST)
	public ResultWrapper<List<DealAlert>> createDealAlert(@RequestBody List<DealAlert> dealAlert) {
		ResultWrapper<List<DealAlert>> result = new ResultWrapper<List<DealAlert>>();
		try {
			List<DealAlert> dealAlertData = dealAlertService.saveDealAlertData(dealAlert);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("Deal Alert has been created successfully.");
			result.setResult(dealAlertData);
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while creating Deal Alert.", e);
		}
		return result;
	}

	@RequestMapping(value = "/dealalertsavebysharedalert", method = RequestMethod.POST)
	public ResultWrapper<List<DealAlert>> createDealAlertByShared(@RequestBody DealAlert dealAlert) {
		ResultWrapper<List<DealAlert>> result = new ResultWrapper<List<DealAlert>>();
		List<DealAlert> dealAlertResult = new ArrayList<DealAlert>();
		try {
			DealAlert dealAlertData = dealAlertService.saveDealAlertDataByShared(dealAlert);
			dealAlertResult.add(dealAlertData);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("Deal Alert has been created successfully.");
			result.setResult(dealAlertResult);
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation(e.getMessage());
			logger.error("Error while creating Deal Alert.", e);
		}
		return result;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ResultWrapper<List<DealAlert>> deleteByDealAlertId(
			@RequestParam Long dealAlertId) {
		ResultWrapper<List<DealAlert>> result = new ResultWrapper<List<DealAlert>>();
		DealAlert dealAlert = null;
		try {
			dealAlert = dealAlertService.findById(dealAlertId);
			dealAlert.setIsDeleted("1");
			dealAlertService.save(dealAlert);		
			result.setMessage(Result.SUCCESS);
			result.setExplanation("delete deal alert by: " + dealAlert.getAlertTitle() + ".");
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while deleting deal alert: " + e.getMessage() + ".");
			logger.error("Error while deleting deal alert.", e);
		}
		return result;
	}

	@RequestMapping(value = "/pause", method = RequestMethod.GET)
	public ResultWrapper<List<DealAlert>> pauseByDealAlertId(@RequestParam Long dealAlertId) {

		ResultWrapper<List<DealAlert>> result = new ResultWrapper<List<DealAlert>>();
		DealAlert dealAlert = null;
		try {
			dealAlert = dealAlertService.findById(dealAlertId);
			if(dealAlert.getIsActive().equalsIgnoreCase("1")){
				dealAlert.setIsActive("0");
			}else{
				dealAlert.setIsActive("1");
			}
			dealAlertService.save(dealAlert);		
			result.setMessage(Result.SUCCESS);
			result.setExplanation("pause deal alert by: " + dealAlert.getAlertTitle() + ".");
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while pause deal alert: " + e.getMessage() + ".");
			logger.error("Error while pause deal alert.", e);
		}
		return result;
	}

	@RequestMapping(value = "/updateDealAlert", method = RequestMethod.PUT)
	public ResultWrapper<DealAlert> updateDealAlert(@RequestBody DealAlert dealAlert, HttpServletRequest request) {

		ResultWrapper<DealAlert> result = new ResultWrapper<DealAlert>();
		DealAlert dealAlertResult = null; 
		try{
			DealAlert tempDealAlert = dealAlertService.findById(dealAlert.getId());
			dealAlertResult = dealAlertService.updateDealAlert(dealAlert, tempDealAlert);
			result.setResult(dealAlertResult);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("Deal alert changes have been saved.");
		}catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("error = " + e);
			logger.error("Error", e);
		}
		return result;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResultWrapper<List<DealAlert>> getAll() {
		ResultWrapper<List<DealAlert>> result = new ResultWrapper<List<DealAlert>>();
		List<DealAlert> dealAlertData = null;
		try {
			dealAlertData = dealAlertService.getAll();		
			result.setResult(dealAlertData);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("Fetch all deal alert.");
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while fetch all deal alert: " + e.getMessage() + ".");
			logger.error("Error while fetch all deal alert.", e);
		}
		return result;
	}

	@RequestMapping(value = "/userdealalert", method = RequestMethod.GET)
	public ResultWrapper<List<DealAlert>> getUserDealAlert(@RequestParam Long userId) {
		ResultWrapper<List<DealAlert>> result = new ResultWrapper<List<DealAlert>>();
		List<DealAlert> dealAlertData = null;
		try {
			dealAlertData = dealAlertService.getUserDealAlert(userId);		
			result.setResult(dealAlertData);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("Fetch user deal alert.");
			//deserialize(result);
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while fetch user deal alert: " + e.getMessage() + ".");
			logger.error("Error while fetch user deal alert.", e);
		}
		return result;
	}

	@RequestMapping(value = "/fetchbydealalertid", method = RequestMethod.GET)
	public ResultWrapper<List<DealAlert>> fetchDealByIds(@RequestParam Long dealAlertId) {
		ResultWrapper<List<DealAlert>> result = new ResultWrapper<List<DealAlert>>();
		List<DealAlert> dealAlertData = new ArrayList<DealAlert>();
		DealAlert dealAlert = null;
		try {
			dealAlert = dealAlertService.findById(dealAlertId);		
			dealAlertData.add(dealAlert);
			result.setResult(dealAlertData);
			result.setMessage(Result.SUCCESS);
			result.setExplanation("Fetch deal alert.");
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while fetch deal alert: " + e.getMessage() + ".");
			logger.error("Error while fetch deal alert.", e);
		}
		return result;
	}
}
