package com.giftcard.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giftcard.model.Brand;
import com.giftcard.model.BrandImage;
import com.giftcard.model.Category;
import com.giftcard.model.Item;
import com.giftcard.model.ItemCountry;
import com.giftcard.model.ItemDenomination;
import com.giftcard.response.Result;
import com.giftcard.response.ResultWrapper;
import com.giftcard.service.BrandImageService;
import com.giftcard.service.ItemCountryService;
import com.giftcard.service.ItemDenominationService;
import com.giftcard.service.ItemService;

@RestController
@RequestMapping("/item")
public class ItemController {

	@Autowired
	private ItemService itemService;

	@Autowired
	private BrandImageService brandImageService;

	@Autowired
	private ItemCountryService itemCountryService;

	@Autowired
	private ItemDenominationService itemDenominationService;

	private static final Logger logger = LoggerFactory.getLogger(ItemController.class);

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ResponseEntity<ResultWrapper<List<Item>>> getItems(
			@RequestParam(value = "categoryId", required = false) Long categoryId,
			@RequestParam(value = "brandId", required = false) Long brandId,
			@RequestParam(value = "query", required = false) String query) {

		ResultWrapper<List<Item>> result = new ResultWrapper<List<Item>>();

		List<Item> items = null;
		try {
			items = itemService.search(categoryId, brandId, query);

			for (Item item : items) {
				Item tempItem = itemService.findById(item.getId());
				Brand brand = tempItem.getBrand();
				List<BrandImage> brandImages = brandImageService.findByBrand_id(brand.getId());
				List<ItemCountry> itemCountries = itemCountryService.findByItem_id(item.getId());
				ItemDenomination itemDenomination = itemDenominationService.findByItemId(item.getId());
				item.setCountries(itemCountries);
				brand.setImageUrls(brandImages);
				item.setCategory(tempItem.getCategory());
				item.setBrand(brand);
				item.setItemDenomination(itemDenomination);

			}

			if (items != null && !items.isEmpty()) {
				result.setMessage(Result.SUCCESS);
				result.setResult(items);
				result.setExplanation("totalItems: " + items.size() + ".");
			} else {
				result.setMessage(Result.FAIL);
				result.setExplanation("No Records Present.");
			}
		} catch (Exception e) {
			result.setMessage(Result.FAIL);
			result.setExplanation("Error while getting data: " + e.getMessage() + ".");
			logger.error("Error while getting data.", e);
		}
		return ResponseEntity.ok(result);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/assignCategory")
	public ResponseEntity<ResultWrapper<Item>> assignCategory(@RequestParam("itemId") String itemId,
			@RequestBody Category category) {
		ResultWrapper<Item> result = new ResultWrapper<Item>();
		List<Item> items = itemService.findByCategory_id(category.getId());
		Iterator<Item> itemIterator = items.iterator();
		if (itemId.equals("")) {
			while (itemIterator.hasNext()) {
				Item tempItem = itemIterator.next();
				tempItem.setCategory(null);
				itemService.save(tempItem);
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Removed all items assigned to this category");
			}
		} else {
			try {
				List<Item> sourceItemList = new ArrayList<>();

				List<String> source = Arrays.asList(itemId.split(","));
				for (String item : source) {
					sourceItemList.add(itemService.findById(Integer.parseInt(item)));
				}

				List<Item> addList = new ArrayList<Item>(sourceItemList);
				List<Item> deleteList = new ArrayList<Item>(items);

				addList.removeAll(items);
				deleteList.removeAll(sourceItemList);
				for (Item item : deleteList) {
					List<Category> categories = item.getCategory();
					for (Category categoriedb : categories) {
						if (categoriedb.getId() == category.getId()) {
							categories.remove(categoriedb);
							break;
						}
					}
					item.setCategory(categories);
					itemService.save(item);
				}
				for (Item item : addList) {
					List<Category> categories = item.getCategory();
					categories.add(category);
					item.setCategory(categories);
					itemService.save(item);
				}
				result.setMessage(Result.SUCCESS);
				result.setExplanation("Category assingned successfully");
			} catch (Exception e) {
				result.setMessage(Result.FAIL);
				result.setExplanation(e.getMessage() + ".");
				logger.error("Error while assigning category.", e);
			}

		}
		
		return ResponseEntity.ok(result);
	}

}