<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.giftcard.deals.model.*" %> 
<%@ page import="com.giftcard.scapper.helper.*" %>    
<%@ page import="com.giftcard.scrapper.job.*" %>      
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<ProductModel> mListOfProducts = AWSHelper.getAWSSearchData(request.getParameter("frmSearchParam"));
//List<ProductModel> mListOfProducts =   PullDataHelper.pullProducts(request.getParameter("frmSearchParam"));

%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>purvice - Hot Deals, Gift Cards and more!</title>

    <!-- Bootstrap -->
    <link href="bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>

   
   
body { padding-top: 15px; }    
 </style>
 
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
    <script src="jquery-ui-1.11.4/external/jquery/jquery.js"></script>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>    
    
 
 
 
 
 
 
 
 
  </head>
  <body>
    <%@include file="/WEB-INF/jsp/menu.jsp" %>
    <div class="page-container" style="margin:90px 0 40px 0;">
    <div id="log" style="color:red;background-color:yellow;font-weight:bold;"></div>
 <div class="container-fluid">
  <div class="row">
<%
for(ProductModel mProductModel: mListOfProducts){
%>
	<div class="col-sm-12 col-md-4 col-lg-2"  style="overflow: hidden;float:center;min-height:350px;">
	<a href="<%=mProductModel.getDestUrl() %>" target="_blank">
	<img src="<%=mProductModel.getUrl() %>" class="img-thumbnail" alt="<%=mProductModel.getProductName() %>" style="max-height: 200px;max-width:200px;height:auto;width:auto;">
	</a>
	<br/><h6><small><%=mProductModel.getProductName() %></small></h6>
	<br/>
	<%if(mProductModel.getSalePrice() != null && mProductModel.getPrice() != null && !mProductModel.getSalePrice().equals(mProductModel.getPrice())) {%>
	<h4><small>Price:&nbsp;&nbsp;<s><%=mProductModel.getPrice() %></small></s><medium style="color:red">&nbsp;&nbsp;<%=mProductModel.getSalePrice() %><br/><small style='color:brown;align:center;'><%=mProductModel.getSource() %></small></h4>
	<%} else{ %>
	<h4><medium>Price:&nbsp;&nbsp;<%=mProductModel.getPrice() %></medium><br/><small style='color:brown;align:center;'><%=mProductModel.getSource() %></small></h4>
	<%} %>
	</div>
<% 
}
%>
  </div>
    
  </div>

<!-- 
    <div class="container">
   <div class="row">
    <div class="col-sm-4" ><img src="img/test18.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
    
    <div class="col-sm-4" \><img src="img/test16.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>

    <div class="col-sm-4" ><img src="img/test15.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
    <div class="col-sm-4" ><img src="img/test14.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
    <div class="col-sm-4" \><img src="img/test13.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>

    <div class="col-sm-4" ><img src="img/test12.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
    <div class="col-sm-4" ><img src="img/test11.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
    <div class="col-sm-4" \><img src="img/test10.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
    <div class="col-sm-4" ><img src="img/test17.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
  </div>
    
  </div>
   -->
  
  </body>
</html>


