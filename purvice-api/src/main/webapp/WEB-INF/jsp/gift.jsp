<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>purvice Gift Cards</title>

    <!-- Bootstrap -->
    <link href="bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>

   
   
body { padding-top: 15px; }   
 

.btn-color {
    background: #0099cc;
    color: #ffffff;
}

#myAlert {
    display: none;
}

 </style>
 
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
    <script src="jquery-ui-1.11.4/external/jquery/jquery.js"></script>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>    




  </head>
  <body>

   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
    <script src="jquery-ui-1.11.4/external/jquery/jquery.js"></script>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>    
    
    
    
    
    
    <%@include file="/WEB-INF/jsp/menu.jsp" %>
    
   
<div class="page-container">
  <br/>

      
    <div class="container">
      <div class="row row-offcanvas row-offcanvas-left">
        
        <!-- sidebar -->
        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
            <ul class="nav">
              <li class="active"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> GIFT CARDS COMING SOON</li>
              <li><a href="#"><span class="glyphicon glyphicon-th" aria-hidden="true"></span> All Retailers</a></li>
              <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> New</a></li>
              <li><a href="#"><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> Featured Gift Cards</a></li>   
              <li><a href="#"><span class="glyphicon glyphicon-education" aria-hidden="true"></span> Babies & Children</a></li>   
              <li><a href="#"><span class="glyphicon glyphicon-apple" aria-hidden="true"></span> Electronics</a></li>   
              <li><a href="#"><span class="glyphicon glyphicon-record" aria-hidden="true"></span> Entertainment</a></li>   
              <li><a href="#"><span class="glyphicon glyphicon-fire" aria-hidden="true"></span> Fashion</a></li>   
              <li><a href="#"><span class="glyphicon glyphicon-lamp" aria-hidden="true"></span> Home Goods</a></li>  
              <li><a href="#"><span class="glyphicon glyphicon-glass" aria-hidden="true"></span> Restaurants</a></li>   
              <li><a href="#"><span class="glyphicon glyphicon-leaf" aria-hidden="true"></span> Others</a></li>   
            </ul>
        </div>
  	
        <!-- main area -->
        <div class="col-xs-12 col-sm-9">

			  <div class="row">
			    <div class="col-sm-3 col-md-3" >
			    <img src="img/amazon_gc.png" class="img-thumbnail" alt="Purvice" width="150" height="150">
					<br/><h6><medium>Price:&nbsp;&nbsp;$10.00</medium></h6>
					<button type="button" class="btn btn-default navbar-btn btn-color" data-toggle="modal" data-target="#addToCartModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;Add to Cart</button>
			    </div>
			    <div class="col-sm-3 col-md-3" ><img src="img/bestbuy_gc.png" class="img-thumbnail" alt="Purvice" width="150" height="150">
					<br/><h6><medium>Price:&nbsp;&nbsp;$10.00</medium></h6>			    
					<button type="button" class="btn btn-default navbar-btn btn-color" data-toggle="modal" data-target="#addToCartModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;Add to Cart</button>
			    </div>
			    <div class="col-sm-3 col-md-3" ><img src="img/cvs_gc.png" class="img-thumbnail" alt="Purvice" width="150" height="150">
					<br/><h6><medium>Price:&nbsp;&nbsp;$10.00</medium></h6>		
					<button type="button" class="btn btn-default navbar-btn btn-color" data-toggle="modal" data-target="#addToCartModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;Add to Cart</button>	    
			    </div>
			    <div class="col-sm-3 col-md-3" ><img src="img/ebay_gc.png" class="img-thumbnail" alt="Purvice" width="150" height="150">
					<br/><h6><medium>Price:&nbsp;&nbsp;$10.00</medium></h6>			    
					<button type="button" class="btn btn-default navbar-btn btn-color" data-toggle="modal" data-target="#addToCartModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;Add to Cart</button>
			    </div>
			  </div>
			<br/>
			  <div class="row">
			    <div class="col-sm-3 col-md-3" >
			    <img src="img/express_gc.png" class="img-thumbnail" alt="Purvice" width="150" height="150">
					<br/><h6><medium>Price:&nbsp;&nbsp;$10.00</medium></h6>
					<button type="button" class="btn btn-default navbar-btn btn-color" data-toggle="modal" data-target="#addToCartModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;Add to Cart</button>
			    </div>
			    <div class="col-sm-3 col-md-3" ><img src="img/gamestop_gc.png" class="img-thumbnail" alt="Purvice" width="150" height="150">
					<br/><h6><medium>Price:&nbsp;&nbsp;$10.00</medium></h6>			    
					<button type="button" class="btn btn-default navbar-btn btn-color" data-toggle="modal" data-target="#addToCartModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;Add to Cart</button>
			    </div>
			    <div class="col-sm-3 col-md-3" ><img src="img/homedepot_gc.png" class="img-thumbnail" alt="Purvice" width="150" height="150">
					<br/><h6><medium>Price:&nbsp;&nbsp;$10.00</medium></h6>			 
					<button type="button" class="btn btn-default navbar-btn btn-color" data-toggle="modal" data-target="#addToCartModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;Add to Cart</button>   
			    </div>
			    <div class="col-sm-3 col-md-3" ><img src="img/itunes_gc.png" class="img-thumbnail" alt="Purvice" width="150" height="150">
					<br/><h6><medium>Price:&nbsp;&nbsp;$10.00</medium></h6>			
					<button type="button" class="btn btn-default navbar-btn btn-color" data-toggle="modal" data-target="#addToCartModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;Add to Cart</button>    
			    </div>
			  </div>
			  <br/>

			  <div class="row">
			    <div class="col-sm-3 col-md-3" >
			    <img src="img/kohls_gc.png" class="img-thumbnail" alt="Purvice" width="150" height="150">
					<br/><h6><medium>Price:&nbsp;&nbsp;$10.00</medium></h6>
					<button type="button" class="btn btn-default navbar-btn btn-color"  data-toggle="modal" data-target="#addToCartModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;Add to Cart</button>
			    </div>
			    <div class="col-sm-3 col-md-3" ><img src="img/lowes_gc.png" class="img-thumbnail" alt="Purvice" width="150" height="150">
					<br/><h6><medium>Price:&nbsp;&nbsp;$10.00</medium></h6>	
					<button type="button" class="btn btn-default navbar-btn btn-color" data-toggle="modal" data-target="#addToCartModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;Add to Cart</button>		    
			    </div>
			    <div class="col-sm-3 col-md-3" ><img src="img/starbucks_gc.png" class="img-thumbnail" alt="Purvice" width="150" height="150">
					<br/><h6><medium>Price:&nbsp;&nbsp;$10.00</medium></h6>			    
					<button type="button" class="btn btn-default navbar-btn btn-color" data-toggle="modal" data-target="#addToCartModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;Add to Cart</button>
			    </div>
			    <div class="col-sm-3 col-md-3" ><img src="img/target_gc.png" class="img-thumbnail" alt="Purvice" width="150" height="150">
					<br/><h6><medium>Price:&nbsp;&nbsp;$10.00</medium></h6>			   
					<button type="button" class="btn btn-default navbar-btn btn-color" data-toggle="modal" data-target="#addToCartModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;Add to Cart</button> 
			    </div>
			  </div>          
          
          
        </div><!-- /.col-xs-12 main -->
    </div><!--/.row-->
  </div><!--/.container-->
  
  
 <div id="addToCartModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
        <h4 class="modal-title" id="myModalLabel">Please Choose Gift Card Amount:</h4>
      </div>
      <div class="modal-body">
		<div class="btn-group" role="group" aria-label="...">
  <button type="button" class="btn btn-default">$10</button>
  <button type="button" class="btn btn-default">$20</button>
  <button type="button" class="btn btn-default">$30</button>
  <button type="button" class="btn btn-default">$50</button>
  <button type="button" class="btn btn-default">$100</button>
</div>
 </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="javascript:alert('This will take you to checkout page');" data-dismiss="modal">Buy Now</button>


        
      </div>
    </div>
  </div>
</div>
  
</div><!--/.page-container-->   
   
  
  </body>
</html>


