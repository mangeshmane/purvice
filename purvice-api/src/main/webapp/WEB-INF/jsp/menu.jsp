<%@ page import="com.auth0.*" %>


<%!

         // Converts a relative path into a full path
         // Taken from http://stackoverflow.com/posts/5212336/revisions
        public String buildUrl(HttpServletRequest request, String relativePath) {


         String scheme      =    request.getScheme();        // http
         String serverName  =    request.getServerName();    // hostname.com
         int serverPort     =    request.getServerPort();    // 80
         String contextPath =    request.getContextPath();   // /mywebapp

         // Reconstruct original requesting URL
         StringBuffer url =  new StringBuffer();
         url.append(scheme).append("://").append(serverName);

         if ((serverPort != 80) && (serverPort != 443)) {
             url.append(":").append(serverPort);
         }

         url.append(contextPath).append(relativePath);

         return url.toString();

         }
      %>
<%
 final NonceGenerator nonceGenerator = new NonceGenerator();
NonceStorage nonceStorage = new RequestNonceStorage(request);
    String nonce = nonceGenerator.generateNonce();
    nonceStorage.setState(nonce);
    request.setAttribute("state", nonce);


Auth0User user = Auth0User.get(request);
String iName=null;
if(user != null){
	iName = user.getName() ;

}

%>
<html>

<head>



    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">



<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script src="encode.js"></script>
<script src="lazy.js"></script>
 <link rel="stylesheet" type="text/css" href="tooltipster.bundle.min.css" />
  <link rel="stylesheet" type="text/css" href="tooltipster-sideTip-shadow.min.css" />
    <script type="text/javascript" src="tooltipster.bundle.min.js"></script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68503539-1', 'auto');
  ga('send', 'pageview');







</script>
  <script src="https://cdn.auth0.com/js/lock-7.9.min.js"></script>



<script type="text/javascript">

var lock = new Auth0Lock('<%= application.getInitParameter("auth0.client_id") %>', '<%= application.getInitParameter("auth0.domain") %>');

function signin() {
  lock.show({
	  callbackURL: '<%= buildUrl(request, "/gift.jsp") %>'
    , responseType: 'code'
    , authParams: {
        state: '${state}'
      , scope: 'openid name email picture'
      }
  });
}
</script>


<style>
.navbar-default {
  background-color: #010d25;
  border-color: #1a1a52;
}
.navbar-default .navbar-brand {
  color: #ecf0f1;
}
.navbar-default .navbar-brand:hover, .navbar-default .navbar-brand:focus {
  color: #ecdbff;
}
.navbar-default .navbar-text {
  color: #ecf0f1;
}
.navbar-default .navbar-nav > li > a {
  color: #ecf0f1;
}
.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
  color: #ecdbff;
}
.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus {
  color: #ecdbff;
  background-color: #1a1a52;
}
.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
  color: #ecdbff;
  background-color: #1a1a52;
}
.navbar-default .navbar-toggle {
  border-color: #1a1a52;
}
.navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:focus {
  background-color: #1a1a52;
}
.navbar-default .navbar-toggle .icon-bar {
  background-color: #ecf0f1;
}
.navbar-default .navbar-collapse,
.navbar-default .navbar-collapseNew,
.navbar-default .navbar-form {
  border-color: #ecf0f1;
}
.navbar-default .navbar-link {
  color: #ecf0f1;
}
.navbar-default .navbar-link:hover {
  color: #ecdbff;
}

@media (max-width: 767px) {
.filtersContent {
display:none;
}
  .navbar-default .navbar-nav .open .dropdown-menu > li > a {
    color: #ecf0f1;
  }
  .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover, .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
    color: #ecdbff;
  }
  .navbar-default .navbar-nav .open .dropdown-menu > .active > a, .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover, .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
    color: #ecdbff;
    background-color: #1a1a52;
  }
}
#logo-link {
    height: 52px;
    display: block;
    float: left;
    width: 140px;
    margin: 13px 0px 8px 0;
    background-size: 72px 46px;
    font-size: 30px;
    color: white;
}
#logo-link img {
max-width:100%;
}
#share-buttons img {
display: inline;
max-height:10px;
max-width:10px;
}

.headerWrapper{
	position: fixed;
    z-index: 999;
    width: 100%;
    background-color: #20a8d8;
    height: 80px;
    top: 0;
    right: 0;
    box-shadow: 0 6px 6px 0 rgba(22, 38, 59, 0.34);
    height: 60px;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
   
}
.headerWrapper .navbar.navbar-default {
background-color: #20a8d8;
    height: 80px;
    top: 0;
    right: 0;
    box-shadow: 0 6px 6px 0 rgba(22, 38, 59, 0.34);
    height: 60px;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
        border: 0;
   
}
.header-button {
	color:#20a8d8;
	background-color:#fff;
	cursor:pointer;
    float: right;
    margin: 17px 0 0 8px;
    padding: 0 40px;
    text-transform: uppercase;
    font-size: 15px;
    font-weight: 400;
    line-height: 47px!important;
    height: 45px!important;
    border-radius:3px;
}

.headerWrapper .navbar-nav {
	margin-left:35px;
	margin-top: 17px;
}

.headerWrapper .nav-item {
    position: relative;
    min-width: 50px;
    margin: 0 !important;
    text-align: center;
    padding: 0 15px;
}

.headerWrapper .nav-item a {
color: #fff;
font-size: 16px;
font-weight: 600;
padding: 0 !important;
}
.headerWrapper .nav-item a:hover {
color: #16253b !important;
}

.headerWrapper .search-bar {
    margin-right: 60px;
    position: relative;
    width: 200px;
    float: right;
    margin-top: 13px;
}

.headerWrapper .search-bar input {
    width: 200px;
    border-radius: 4px;
    border: 0;
    padding: 4px 10px;
}

.headerWrapper .search-bar .search-icon {
    width: 30px;
    background: #f9a71a;
    height: 28px;
    display: inline-block;
    position: absolute;
    right: -26px;
    top: 0px;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
    cursor: pointer;
}

.headerWrapper .search-bar .search-icon i {
    position: relative;
    left: 8px;
    top: 7px;
    color: #fff;
}
.mobile-search-icon {
display:none;
}
    
    .headerWrapper .can-logout {
    color: #fff;
    padding-right: 20px;
    font-size: 16px;
    height: 29px;
    display: block;
    line-height: 29px;
    float: right;
    margin-top: 13px;
}

.headerWrapper .can-logout:hover {
color: #16253b !important;
}

.headerWrapper .can-logout .circle {
    background: #fff;
    padding: 5px;
    width: 21px;
    height: 21px;
    display: inline-block;
    position: relative;
    top: 5px;
    border-radius: 10px;
    margin-right: 7px;
    float: left;
}
.navbar-collapseNew {
    position: fixed;
    width: 200px;
    height: 100%;
    z-index: 999;
    top: 80px;
        width: 220px;
    margin-left: 0px;
    left: 0;
        top: 60px;
    background: #16253b;
    display:none;
}

.headerWrapper .can-logout .circle i {
    position: relative;
    top: -1px;
    color: #f9a71a;
    font-size: 13px;
}
.fa-sign-in:before {
    content: "\f2f6";
}
.headerWrapper .can-logout .circle.login i {
    left: -2px;
}

.headerWrapper .can-logout:hover, .headerWrapper .can-logout:hover .circle i {
color: #16253b !important;
text-decoration: none;
}
.navbar-default .navbar-toggle {
  border: 0;
}


@media (max-width: 1100px) {

.headerWrapper .navbar-nav {
margin-left:20px;
}
.headerWrapper .nav-item {
padding:0 10px;
}
.headerWrapper .can-logout {
padding-right: 15px;
}
.headerWrapper .search-bar {
margin-right: 40px;
}
}

@media (max-width: 940px) {
.mobile-search-icon {
    display: block;
    font-size: 18px;
    position: absolute;
    right: 80px;
    top: 15px;
    color: #fff;
    cursor: pointer;
}
.headerWrapper .search-bar {
    position: absolute !important;
    display: none;
    left: 0;
    top: 60px;
    padding: 5px 15px 15px;
    margin-right: 0;
    margin-top: 0;
    width: 100%;
    background: #20a8d8;
    box-shadow: 0 6px 6px 0 rgba(22, 38, 59, 0.34);
}
.headerWrapper .search-bar input {
	width: 100%;
}
.headerWrapper .search-bar .search-icon {
	right: 12px;
	top: 5px;
}
.navbar-toggle {
    display: block;
}
ul.nav.navbar-nav, .search-bar, .headerWrapper .can-logout{
display:none;
}
  .navbar-default .navbar-nav .open .dropdown-menu > li > a {
    color: #ecf0f1;
  }
  .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover, .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
    color: #ecdbff;
  }
  .navbar-default .navbar-nav .open .dropdown-menu > .active > a, .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover, .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
    color: #ecdbff;
    background-color: #1a1a52;
  }
}

.navbar-collapseNew {
  position: fixed;
    width: 200px;
    height: 100%;
    z-index: 999;
    top: 80px;
        width: 220px;
    margin-left: 0px;
    left: 0;
        top: 60px;
    background: #16253b;
    display:none;
}
.in {
    margin-left: 0px;
}

.nav .nav-item {
    position: relative;
    margin: 0;
    transition: background .3s ease-in-out;
}
.nav .nav-item:hover, .nav .nav-item.active {
    color: #fff !important;
    background: #09A9F5 !important;
    }
  .nav .nav-item .nav-link {
  display: block;
    padding: 0.75rem 1rem;
    color: #fff;
    text-decoration: none;
    background: transparent;
  } 
        
}
.tooltip_templates {
display: none;}
/* This is how you would create a custom secondary theme on top of tooltipster-noir: */

.tooltipster-sidetip.tooltipster-shadow.tooltipster-shadow-customized .tooltipster-content {
	color: #333;
	padding: 10px;
	font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
	font-size: 13px;
}
.is-active {
    border-bottom: thick solid #f9a71a;
}
</style>

</head>
<!-- <div class="headerWrapper">
    <nav class="navbar navbar-default"> -->
 <!--  <div class="container-fluid" style="display:block;">
    Brand and toggle get grouped for better mobile display
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="../#/home" id="logo-link"><img style="max-width:100%;height:auto;" alt="logo" src="img/purvice_logo_pure_white.png"></a>
      <a class="nav-link" style="color:#fff;cursor:pointer;float:left;margin-top:28px;padding:0;" href="../#/buy-gift-card" >Buy Gift Cards</a>
      <a class="nav-link" style="color:#fff;cursor:pointer;float:left;margin:28px 0 0 20px;padding:0;" href="../purvice1/deals" >Deals</a>
      <a class="nav-link" style="color:#fff;cursor:pointer;float:left;margin:28px 0 0 20px;padding:0;" href="https://travel.purvice.com" >Book Air/Hotels</a>

    </div>
	<a class="button header-button" style="float:right;" href="../#/login">LOGIN</a>
	<a class="button header-button" style="float:right;" href="../#/register">SIGN UP FREE</a>

    Collect the nav links, forms, and other content for toggling
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <form class="navbar-form pull-right small"  role="search" style="margin-top:20px;">
        <div class="form-group">
          <input type="text" id="idFrmSearchParam" name="frmSearchParam"  class="form-control small" placeholder="Search...">
        </div>
      </form>
    </div>



  </div>/.container-fluid -->
<!--   <div class="container-fluid">
    <a id="logo-link" href="http://localhost:4200/#/home"><img class="logo_image" src="img/purvice_logo_pure_white.png"></a>
    


    <ul class="nav navbar-nav d-md-down-none mr-auto">

      <li class="nav-item px-3">

        <a class="nav-link" href="http://localhost:4200/#/buy-gift-card" >Buy Gift Cards</a>

      </li>
      <li class="nav-item px-3">
        <a class="nav-link is-active" href="../purvice/deals">Hot Deals</a>
      </li>

      <li class="nav-item px-3">
        <a class="nav-link" href="https://travel.purvice.com">Travel</a>


      </li>
    </ul>
    
   

    <div appmobilesearchbartoggler="" class="mobile-search-icon" ><i class="fa fa-search"></i></div>
    
<a class="float-right can-logout" data-alt-label="Logout" title="Login" href="http://localhost:4200/#/login">
        <span class="circle login"><i class="fa fa-sign-in"></i></span> <strong>Login</strong> </a>

<a class="float-right can-logout signup" data-alt-label="Logout" href="http://localhost:4200/#/register" title="Register" >
      <span class="circle"><i class="fa fa-user"></i></span> <strong>Sign Up Free</strong> </a>
       <div class="mobile-search-icon" ><i class="fas fa-search"></i></div>
        <button type="button" class="navbar-toggle collapsed">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

         <div class="search-bar">
         <form role="search" id="formSerch">
      <input placeholder="Search" type="text" value="" id="idFrmSearchParam" name="frmSearchParam">
      <span class="search-icon"><i class="fa fa-search"></i></span>
      </form>
    </div>
  </div> -->

<!-- </nav>
</div> -->
 <!-- <div class="navbar-collapseNew">
      <nav class="sidebar-nav">
      <ul class="nav">
        <li class="nav-item">
          <a class="nav-link" href="../#/home">Home</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="../#/buy-gift-card" > Buy Gift Card</a>
        </li>
        <li class="nav-item">
        	<a class="nav-link" href="https://travel.purvice.com">Travel</a>
        </li>
<li class="nav-item">
          <a class="nav-link" href="../#/login"> Login</a>
        </li>
<li class="nav-item">
          <a class="nav-link" href="../#/register">Sign Up</a>
        </li>

        <li class="divider"></li>

      </ul>
    </nav>
    </div> -->
</html>
