<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="org.json.*"%>
<%@ page import="java.util.*" %>
<%@ page import="com.giftcard.deals.model.*" %> 
<%@ page import="com.giftcard.scapper.helper.*" %>    
<%@ page import="com.giftcard.scrapper.job.*" %>         

<%

Map<String,ProductModel> mMap = SearchDataHelper.getData();
	JSONArray ja = new JSONArray();
	String q = request.getParameter("q");
 	
	for(String mKey:mMap.keySet()){
		JSONObject json = new JSONObject();
		if(mKey == null || q == null){
			continue;
		}
		if(mKey.toLowerCase().contains(q.toLowerCase())){
		    json.put("id", mKey);
		    json.put("value", mKey);
		    json.put("price", mMap.get(mKey).getPrice());
		    json.put("destUrl", mMap.get(mKey).getDestUrl());
		    json.put("url", mMap.get(mKey).getUrl());
		    ja.put(json);
		}
    }
    
    
    out.print(ja);
    out.flush();
%>