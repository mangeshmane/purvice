<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="com.giftcard.deals.model.*" %> 
<%@ page import="com.giftcard.scapper.helper.*" %>    
<%@ page import="com.giftcard.scrapper.job.*" %>        
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<SDModel> mListOfData =   ScrapperServiceImpl.getInstance().getMapOfData(null,null);

%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>purvice - best purchase advice, period!</title>

    <!-- Bootstrap -->
    <link href="bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="DataTables-1.10.9/media/css/jquery.dataTables.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <style>

   
   
body { padding-top: 15px; }    
div.container {
        width: 80%;
    }
    

}
 </style>
 
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
    <script src="jquery-ui-1.11.4/external/jquery/jquery.js"></script>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>    
    <script src="DataTables-1.10.9/media/js/jquery.dataTables.min.js"></script>  
    
 <script>
 
 
 $(document).ready(function() {
    $('#couponstable').DataTable({
    	/*"iDisplayLength": 100,
    	"order": [[0, "desc"]]*/
    	"iDisplayLength": 100,
    	"order": []
    	/*
        "columnDefs": [
                       {
                           "targets": [ 4 ],
                           "visible": false,
                           "searchable": false
                       }
                   ]          
               ,"order": [[4, "desc"]]    
    	*/
    });
} );
 
 $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { 
	    console.log(message);
	};

 </script>
 
 
 
 
 
  </head>
  <body>
    <%@include file="/WEB-INF/jsp/menu.jsp" %>
    
    <div class="page-container">

      
    <div class="container">
    

  
  
  <table id="couponstable" class="display" cellspacing="0" width="100%" style="font-size:11px;">
        <thead>
            <tr>
            	<th>&nbsp;</th>
                <th><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>&nbsp;Hot SDModel</th>
                <th><span class="glyphicon glyphicon-send" aria-hidden="true"></span>&nbsp;Source</th>
                <th><span class="glyphicon glyphicon-list" aria-hidden="true"></span>&nbsp;</th>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
            	<th>&nbsp;</th>
               <th><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>&nbsp;Hot SDModel</th>
                <th><span class="glyphicon glyphicon-send" aria-hidden="true"></span>&nbsp;Source</th>
                <th><span class="glyphicon glyphicon-list" aria-hidden="true"></span>&nbsp;</th>
            </tr>
        </tfoot>
        
        
        
 
        <tbody>
			<%
		  			for (SDModel mModel : mListOfData) {
			%>
			<tr style="background-color:white;">
			<td align="center" style="height:120px;width:120px;"><a href="<%=mModel.getUrl()%>" target="_blank">
			<%if(mModel.getImageUrl() != null && !mModel.getImageUrl().contains("purvice_deals.jpg")){ %>
			<img src="<%=mModel.getImageUrl()%>" alt="<%=mModel.getImgAlt()%>" style="max-height: 120px; max-width:120px;  "/>
			<%}else{ %>
				<span class="glyphicon glyphicon-camera" aria-hidden="true"></span>&nbsp;
			<% }%>
			</a></td>
			<td><a href="<%=mModel.getUrl()%>" target="_blank"><%=mModel.getText()%></a></td>
			<td><a href="<%=mModel.getUrl()%>" target="_blank"><%=mModel.getSource()%></a></td>
			<td><a href="<%=mModel.getUrl()%>" target="_blank"><%=mModel.getCategory()%></a></td>
			</tr>
			<%} %>	
        </tbody>
    </table>
  
  </div>
  </div>

  </body>
</html>


