<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="com.giftcard.deals.model.*" %> 
<%@ page import="com.giftcard.model.*" %> 
<%@ page import="com.giftcard.scapper.helper.*" %>    
<%@ page import="com.giftcard.scrapper.job.*" %>      
<%@ page import="com.giftcard.deal.service.*" %>
<%@ page import="com.auth0.*" %> 
<%@ page import="org.springframework.context.ApplicationContext,org.springframework.web.servlet.support.RequestContextUtils"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	ApplicationContext aC = RequestContextUtils.getWebApplicationContext(request);
	DealsServiceImpl dealsServiceImpl = (DealsServiceImpl) aC.getBean("dealserviceimpl");
%>
<%
List<Deal> mListOfData = null;
if(request.getParameter("frmSearchParam") != null && !request.getParameter("frmSearchParam").equals("")){
	//mListOfData =   ScrapperServiceImpl.getInstance().getMapOfData(request.getParameter("frmSearchParam"),"c");
	mListOfData = dealsServiceImpl.getDealUsingKeyword(request.getParameter("frmSearchParam"));
	
}
else{
	/* mListOfData =   ScrapperServiceImpl.getInstance().getMapOfData(request.getParameter("item"),request.getParameter("t")); */
	mListOfData = dealsServiceImpl.getAllDeal();
}

List<SDModel> mListOfLatestDeals =   ScrapperServiceImpl.getInstance().getListOutputLatest();
String mSelectedItem = request.getParameter("item");
System.out.println("mSelectedItem:"+mSelectedItem);

%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="600" >
    <link rel="shortcut icon" href="img/purvice.png">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>purvice - Hot Deals!</title>

    <!-- Bootstrap -->
    <link href="bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="DataTables-1.10.9/media/css/jquery.dataTables.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
body {
font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
}
 .ui-menu { width: 170px; }  
 .ui-widget {
    font-family: "Open Sans", Arial, sans-serif !important;
    font-size: 14px !important;
}
.dropdown-menu{
	position:relative;
	border-radius:3px;
	z-index:1;
}
.ui-widget-content a{
	display:block;
}
.filterCheckboxes{
  background-color:#fff;
  color:#222;
  border-radius:3px;
  margin-bottom: 15px;
}
.filterCheckboxes inputDiv {
	width:100%;
}
.filterCheckboxes input{
	margin:0 10px 0 5px;
}
.filtersCheckboxHeading{
    padding: 10px 0 10px 8px;
    font-size: 14px;
    text-transform: capitalize;
    background: #f9a71a;
    color: #fff;
    cursor: pointer;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
}
.filtersCheckboxHeading i {
padding-right: 5px;
padding-left: 5px;
}
.filtersContent {
padding:10px 0;
}
.filtersContent .checkFilter{
padding:0;
position:relative;
font-size:13px;
font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
}
.filtersContent .checkFilter input {
position: absolute;
top: 8px;
left:5px;
}
.filtersContent .checkFilter label {
display: block;
padding: 5px 0;
margin: 0;
padding-left: 30px;
cursor: pointer;
}
.filtersContent .checkFilter label:hover {
background-color: rgba(32,168,216, 0.5);
}

input[type=checkbox]:checked + label {
  background-color: rgba(32,168,216, 0.5);
} 
body { background-color: #e4e4e4; }     
div.container {
        width: 100%;
    }
 .dropdown-menu>li {
    font-size: 11px;
    font-weight:bold;
    color:#001744;
  }   
   .dropdown-menu>li>ul>li>a {
    font-size: 11px;
    font-weight:bold;
    color:#001744;
  }  
.dataTables_filter input {

    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-top-color: rgb(204, 204, 204);
    border-top-style: solid;
    border-top-width: 1px;
}
.items {
    background-color: #fff;
    margin: 0 0 16px 0;
    vertical-align: top;
    overflow: hidden;
    border-radius: 8px;
    box-sizing: border-box;
    /* height:100%; */
    height:335px;
    background: #efefef;
}
.items .top {
    position: relative;
    padding-bottom: 5px;
    overflow: hidden;
    z-index: 0;
    /* height:65%; */
      height:220px;
      background: #fff;
     
}
.items .middle {
    box-sizing: border-box;
    hegiht:100px;
    padding: 10px;
    text-align:center;
    font-size:11px;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
}

.items .middle a {
color: #333;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
} 
.items .bottom {
    height: 15px;
    padding: 0 5px 5px;
    box-sizing: border-box;
    text-align:center;
    font-weight:bold;
    color:grey;
    position: absolute;
}
.items .top img {
max-height:180px;
max-width:180px;
height:auto;
width:auto;
}
.items .bottom {
position: absolute;
top: 175px;
left: 20px;
z-index: 9}
.items .top img {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
}
.items span.stars {
position: absolute;
top: 195px;
    right: 20px;
    z-index: 10;
}
#main-footer {
    width: 100%;
    padding: 0px 7px 10px;
    text-align: center;
    position: fixed;
    margin-top: 15px;
    bottom: 0;
    z-index: 999;
    background-color:#16253b;
}
#main-footer .copy {
    float: left;
    display: inline-block;
    color: #fff;
    font-size: 13px;
    margin-bottom: 0px;
    margin-top: 10px;
}
#main-footer .footer-link {
    display: inline-block;
    float: right;
    color: #fff;
    font-size: 13px;
    margin-bottom: 0px;
    margin-top: 10px;
}
.main-footer a {
    color: #fff;
    margin-right:5px;
}
.main-footer a:hover {
    color: #f9a71a;
   cursor:pointer;
}

.carousel-caption {
    position: relative;
    left: auto;
    right: auto;
}


span.stars, span.stars span {
    display: block;
    background: url(img/stars.png) 0 -16px repeat-x;
    width: 80px;
    height: 16px;
}

span.stars span {
    background-position: 0 0;
}

.carousel-inner {
background: #fff;
border-radius: 4px;
}
.carousel-control.left, .carousel-control.right {
background: none;
width: 20px;
}
.carousel-control.left i, .carousel-control.right i {
font-size: 40px;
color: #000;
top: 50%;
    position: absolute;
    margin-top: -20px;
    text-shadow: none;
}
.carousel-control.left i:hover, .carousel-control.right i:hover {
color: #16253b;
}
.carousel-control.left i {
left:10px;
}
.carousel-control.right i {
right: 10px;
}
#myCarousel {
margin-bottom: 15px;
    margin-right: 10px;
    border-radius: 4px;
    margin-left: 5px;
    padding: 10px;
    background:#fff;
    border-radius: 4px;
}
.carosel-image {
height:150px;
}
.carousel-caption, .carousel-caption a {
text-shadow: none;
font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
color: #333;
font-size:11px;
}

.carousel-indicators {
bottom:5px;
display: none;
}
.carousel-indicators li {
border: 1px solid #333;
}
.carousel-indicators .active {
background-color: #333;
}

.brandImg img {
max-width: 20px;
max-height: 20px;
}
.brandImg {
    padding: 10px;
    border: 1px solid #ccc;
    border-radius: 20px;
    }
.brandText {
padding: 5px 10px;
    border-radius: 6px;
    color: #fff;
    font-weight: normal;
    background: #f9a71a;
        background: rgba(22,37,59,0.6);
    margin-top: 13px;
    font-size: 11px;
    text-shadow: none;
font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
    
}
.carosel-image {
position: relative;
}
.carosel-image img {
max-height: 150px;
    max-width: 150px;
    height: auto;
    width: auto;
    margin-top: 5px;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin:auto;
}
.brandImgCR {
position: absolute;
padding: 10px;
    border: 1px solid #ccc;
    border-radius: 20px;
    top: 15px;
    left: 10px;
}
   
.brandImgCR img {
max-width: 20px;
max-height: 20px;
}
.carousel-caption {
padding-top:40px;
}

.brandTextCR {
position: absolute;
top: 20px;
left: 10px;
padding: 5px 10px;
    border-radius: 6px;
    color: #fff;
    font-weight: normal;
    background: rgba(22,37,59,0.6);
    margin-top: 13px;
    font-size: 11px;
    text-shadow: none;
font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
 
}
.items {
box-shadow: 0 5px 10px 0 rgba(0,0,0,0.25);
}
.items:hover {
box-shadow: 0 15px 20px 0 rgba(0,0,0,0.7);
}
.favHotDeals {
	    position: absolute;
    top: 14px;
    right: 14px;
    color: #ccc;
    cursor: pointer;
    z-index:10;
    padding: 6px;
    font-size: 16px;
}
.favHotDeals.active {
color:#C00;
}
.favLink {
    margin: 0;
    margin-top: 10px;
    text-align: right;
        float: right;
    position: relative;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
}
.showFav {
    position: absolute;
    right: 10px;
    background: #f9a71a;
    padding: 5px 10px;
    border-radius: 4px;
    color: #fff;
    width: 150px;
    cursor: pointer;
}
.showFav i {
padding-right: 6px;
}

.purvice-banner {
	background-image: url('img/hotdeals-banner.png');
	background-size: contain;
	color: #ffffff;
	font-family: "American Typewriter";
	font-weight: bold;
	font-size: 16px;
	margin: 0 10px;
	padding: 10px 0;
	border-radius: 6px;
	margin-bottom: 5px;
	margin-top: -5px;
}

.purvice-banner a{
	color: #ffffff;
}
.fav-container {
    background: #fff;
    margin: 0 10px;
    padding: 10px 0;
    border-radius: 6px;
    margin-bottom: 30px;
    margin-top: -5px;
    display: none;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif !important;
}
.fav-container h2 {
    text-transform: uppercase;
    font-size: 17px;
    margin-top: 20px;
    border-bottom: 1px solid #333;
    border-top: 1px solid #333;
    padding-top: 10px;
    padding-bottom: 10px;
    width: 100%;
    padding-left: 10px;
}
.fav-container .favItems {
    width: 100%;
    margin: 10px 0;
}
.fav-result {
    text-align: center;
    margin: 0 auto;
}
.fav-items-deals + .fav-result {
    display: none;
}

.fav-items-deals .close-fav-deals {
        position: absolute;
    top: 4px;
    right: 3px;
    z-index: 10;
    line-height: 21px;
    display: block;
    width: 24px;
    height: 24px;
    border-radius: 50%;
    background: #d2d7dd;
    color: #fff;
    text-align: center;
    cursor: pointer;
    transition: background-color .15s ease-out;
}
.fav-items-deals .favHotDeals {
display:none;}

@media (min-width: 1500px) {
.col-lg-20 {
width: 20%;
}
}
@media (min-width: 1500px) {
.col-lg-20 {
width: 20%;
}
}
@media (min-width: 640px) and (max-width: 991px) {
.col-lg-20 {
width: 50%;
}
}

.filter_maincontainer {
        margin: 10px;
    background: #fff;
    padding: 10px;
    border-radius: 6px;
}
.filterText {
    display: inline-block;
    background-color: #16253b;
    color: #fff;
    padding: 8px 13px;
    font-size: 16px;
    text-transform:uppercase;
    margin-left: 10px;
}

.filter_maincontainer .filterCheckboxes {
margin-bottom:0;
position:relative;
display: inline-block;
}
.filter_maincontainer .filtersCheckboxHeading {
display: inline-block;
background-color: #fff;
color: #333;
padding-right:30px;
font-size: 18px;
}
.filter_maincontainer .filtersContent {
position:absolute;
top:30px;
left:15px;
background:#fff;
display:none;
z-index: 20;
width: 185px;
box-shadow: 3px 3px 3px 3px rgba(22, 38, 59, 0.34);
border-radius:4px;
}

@media (max-width: 640px) {
.filter_maincontainer .filterCheckboxes, .filterText {
display: block;
}

.favLink {
    float: none;
    width:100%;
    height:36px;
}
.showFav {
left: 10px;
}
}
.fas.fa-sort-up, .fas.fa-sort-down {
padding-left: 5px;
    font-size: 17px;
    padding-top: 0px;
    position: relative;
    top: 5px;}
    .fas.fa-sort-down {
    top: -2px;}
    .up-fav {
    display:none;}
    .fav-container {
    position:relative;}
    .fav-container .favLink {
        position: absolute;
    right: 10px;
    top: 25px;}
    
    
.onSaleTriangle {
    position: absolute;
    top: 10px;
    left: 10px;
    width: 0;
    height: 0;
    border-bottom: 50px solid transparent;
    border-right: 65px solid transparent;
    border-top: 50px solid #E74C3C;
    z-index: 10;
}    

.salePrcntTxt {
    position: relative;
    bottom: 50px;
    left: 3px;
    color: #fff;
    font-weight: bold;
    font-size: 12px;
    width: 75px;
    display: inline-block;
}
    
 </style>
 
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
    <script src="jquery-ui-1.11.4/external/jquery/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script> 
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>    
    <script src="DataTables-1.10.9/media/js/jquery.dataTables.min.js"></script>  
    
 <script>
 
 $( function() {
	    $( "#menu" ).menu();
	  } );
 $(document).ready(function() {
    $('#couponstable').DataTable({
    	/*"iDisplayLength": 100,
    	"order": [[0, "desc"]]*/
    	"iDisplayLength": 100,
    	"bLengthChange": false,
    	"bSort": false,
    	"order": []
    	,  "columnDefs": [
    	                  { "width": "33%", "targets": 0 }
    	                  ]
    	/*
        "columnDefs": [
                       {
                           "targets": [ 4 ],
                           "visible": false,
                           "searchable": false
                       }
                   ]          
               ,"order": [[4, "desc"]]    
    	*/
    });
} );
 
 
 $(document).ready(function() {
	    $('#latestdealstable').DataTable({

	    	"iDisplayLength": 10,
	    	"bFilter": false,
	    	"bInfo": false,
	    	"bSort": false,
	    	"bLengthChange": false,
	    	"fixedColumns": true,
	    	"bPaginate": false,
	    	"order": []
	    	,  "columnDefs": [
	    	                  { "width": "33%", "targets": 0 }
	    	                  ]

	    });
	} );
 
 $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { 
	    console.log(message);
	};
	
	function hideTable(){
	
	<%if(request.getParameter("item") != null){%>
	 $('#divForLatestTable').hide();
	<%}%>
	}
	

 
 
 $(document).ready(function() {
	 $('.lazy').error(function(){
			$(this).text('img/default.png');
		});
	 
	 Encoder.EncodeType = "entity";
	 $(document).on('click', '.favHotDeals', function(e) {
	        e.stopPropagation();
	        
	        var itemDetails = {};
	        var favObj = {};
	        var getBrandImg, getBrandImage, getBrandAlt, getBrandText, getLink, getImage, getText;
	        var $this = $(this).parent('.items');
	        var getID = $this.attr('id');
	        var getHTML = $this.html();
	        var favDealsObj = Encoder.htmlEncode($(this).parent('.items').html());
	        itemDetails.id = getID;
	        itemDetails.data = favDealsObj;
	        if (localStorage.getItem("myfavDeals") !== null) {
	            favObj = JSON.parse(localStorage.getItem('myfavDeals'));
	        }
	        favObj[getID] = itemDetails;
	        var favData = JSON.stringify(favObj);
	        if($(this).hasClass('active')) {
	        	$(this).removeClass('active');
	            delete favObj[getID];
	            favData = JSON.stringify(favObj);
	            if (document.cookie.indexOf('myFavDealsList') != -1) {
	                var getCookieValue = getCookie("myFavDealsList");
	                var isCheck = getCookieValue.indexOf(getID) !== -1;
	                if(isCheck) {
	                    var removeTitle = getCookieValue.replace(getID, '');
	                    setCookie("myFavDealsList", removeTitle, 365);
	                    localStorage.setItem('myFavDealsList', removeTitle);
	                    $('#fav'+getID).remove();
	                    localStorage.setItem('myfavDeals', favData);
	                }
	                $('#fav-'+ getID).remove();
	            }
	        
	        } else {
	        	 $(this).addClass('active');
		            if (document.cookie.indexOf('myFavDealsList') == -1) {
		                setCookie("myFavDealsList", getID, 365);
		                localStorage.setItem('myFavDealsList', getID);
		                localStorage.setItem('myfavDeals', favData);
		                //setCookie("myfavData", favData, 365);
		            } else {
		                var getCookieValue = getCookie("myFavDealsList");
		                var isCheck = getCookieValue.indexOf(getID) === -1;
		                if(isCheck) {
		                    var addTitle = getCookieValue + ', ' + getID;
		                    setCookie("myFavDealsList", addTitle, 365);
		                    localStorage.setItem('myFavDealsList', addTitle);
		                    localStorage.setItem('myfavDeals', favData);
		                   // setCookie("myfavData", favData, 365);
		                }
		            }
		            //$('#favItems .fav-result').hide();
		            $('#favItems').prepend('<div class="col-sm-12 col-md-3 col-lg-2 fav-items-deals" style="height:300px;border-radius:4px;margin-bottom:20px;padding:10px;" ref-id="' + getID + '" id="fav-' + getID + '"><div class="items" style="height:300px;"><span class="close-fav-deals">x</span>' + getHTML +' </div></div>');
		               
		            $('.middle a').tooltipster({
		         	   animation: 'fade',
		         	   delay: 100,
		         	   theme: ['tooltipster-shadow', 'tooltipster-shadow-customized'],
		         	   trigger: 'hover',
		         	   maxWidth: 300
		         	});
	        }
	        
	       
	    });
	 
	 $(document).on('click', '.navbar-toggle', function(e) { 
		 $('.navbar-collapseNew').slideToggle( "slow" );
	 });
	 
	 $(document).on('click', '.showFav', function(e) {
		    if (localStorage.getItem("myfavDeals") !== null) {
		        $('#favItems').html('<div class="fav-result">Sorry, no Favourite Gift Cards found.</div>');
		        //console.log(getCookie("myfavData"));
		        var getFavObj = JSON.parse(localStorage.getItem('myfavDeals'));
		        $.each(getFavObj, function(k, v) {
		        	var getData = Encoder.htmlDecode(getFavObj[k].data);
		        	$('#favItems').prepend('<div class="col-sm-12 col-md-3 col-lg-2 fav-items-deals" style="height:300px;border-radius:4px;margin-bottom:20px;padding:10px;" ref-id="' + getFavObj[k].id + '" id="fav-' + getFavObj[k].id + '"><div class="items" style="height:300px;"><span class="close-fav-deals">x</span>' + getData +' </div></div>');
		        });

		    }
		    $('.fav-container').slideToggle( "slow" );
		    $('.down-fav, .up-fav').slideToggle( "fast" );
		    $('.middle a').tooltipster({
		    	   animation: 'fade',
		    	   delay: 100,
		    	   theme: ['tooltipster-shadow', 'tooltipster-shadow-customized'],
		    	   trigger: 'hover',
		    	   maxWidth: 300
		    	});
		    
		});
	 
	 $(document).on('click', '.fav-items-deals .close-fav-deals', function(e) {
	        var getTitle = $(this).parents('.fav-items-deals').attr('ref-id');
	        var getCookieValue = getCookie("myFavDealsList");
	                var isCheck = getCookieValue.indexOf(getTitle) !== -1;
	                if(isCheck) {
	                    var removeTitle = getCookieValue.replace(getTitle, '');
	                    setCookie("myFavDealsList", removeTitle, 365);
	                    localStorage.setItem('myFavDealsList', removeTitle);
	                }
	                if (localStorage.getItem("myfavDeals") !== null) {
	                    var favObj = JSON.parse(localStorage.getItem('myfavDeals'));
	                }
	                delete favObj[getTitle];
	                var favData = JSON.stringify(favObj);
	                $('#fav'+getTitle).remove();
	                localStorage.setItem('myfavDeals', favData);
	                //setCookie("myfavData", favData, 365);
	                $('#'+getTitle).find('.favHotDeals').removeClass('active');
	                $(this).parents('.fav-items-deals').remove();
	    });
	 
	 
	 setTimeout(function(){
	        if (localStorage.getItem("myFavDealsList") !== null) {
	          var getFavList = localStorage.getItem('myFavDealsList');
	          getFavList = getFavList.replace(/^\s+|\s+$/gm,'');
	          var getFavItems = getFavList.split(',');
	          console.log(getFavItems.length);
	          for(var kh=0; kh < getFavItems.length; kh++) {
	            if(getFavItems[kh] != '' && getFavItems[kh] != ' ' && getFavItems[kh] != undefined) {
	              var getFavItemsto = getFavItems[kh].replace(/^\s+|\s+$/gm,'');
	              $('#' + getFavItemsto).find('.favHotDeals').addClass('active');
	            }
	          }
	        }
	        
	        
	       }, 1000);
	 
	    //set initial state.
	$(document).on('click', '.mobile-search-icon', function(e) {
		$('.search-bar').slideToggle();
	});
	    
	$(document).on('click', '.filtersCheckboxHeading', function(){
		$(this).parent('.filterCheckboxes').find('.filtersContent').slideToggle();
	})
	    
	 $(".chkFilterStore").click(function(e){
		 var checkedValues = $('.chkFilterStore:checked').map(function() {
			    return this.value;
			}).get();
		 mReqParam = checkedValues;
		 if(e.target.value == "all" && mReqParam != null && mReqParam.indexOf("all") != -1){
			 mReqParam = null;
		 }
		 else{
			 mReqParam = mReqParam.toString().replace("all","");
			 if(mReqParam.indexOf(",") == 0 && mReqParam.length > 1){
				 mReqParam = mReqParam.substring(1);
			 }
		 }
		 
		 if(mReqParam != null && mReqParam != ""){
			 window.location.assign("deals?item="+mReqParam);
		 }
		 else{
			 window.location.assign("deals");
		 }
		 return;
		 
	 });
	    
	 $(".chkFilterCategory").click(function(e){
		 var checkedValues = $('.chkFilterCategory:checked').map(function() {
			    return this.value;
			}).get();
		 mReqParam = checkedValues;
		 if(e.target.value == "all" && mReqParam != null && mReqParam.indexOf("all") != -1){
			 mReqParam = null;
		 }
		 else{
			 mReqParam = mReqParam.toString().replace("all","");
			 if(mReqParam.indexOf(",") == 0 && mReqParam.length > 1){
				 mReqParam = mReqParam.substring(1);
			 }
		 }
		 
		 if(mReqParam != null && mReqParam != ""){
			 window.location.assign("deals?&t=c&item="+mReqParam);
		 }
		 else{
			 window.location.assign("deals");
		 }
		 return;
	 });  
	 
	 
	 $("#idFrmSearchParam").keypress(function(e){
		 
		if(e.which == 13) {
			console.log("@@@@ACTEST 1:"+$("#idFrmSearchParam").val());
			//window.location.replace("hotdeals.jsp?t=search&item="+$("#idFrmSearchParam").val());
			window.location.assign("deals");
			return;
		}
		
		return;
		 
	 });
	 
	 $('.search-icon').click(function(e){
		 if($("#idFrmSearchParam").val() != ''){
			 window.location.assign("deals");
			 $('#formSerch').submit();
		 }
		 
		/*  console.log("@@@@ACTEST 1:"+$("#idFrmSearchParam").val());
			//window.location.replace("hotdeals.jsp?t=search&item="+$("#idFrmSearchParam").val());
			window.location.assign("deals");
			return; */
	 })
	    
	    
 });

$.fn.stars = function() {
	return $(this).each(function() {
	    // Get the value
	    var val = parseFloat($(this).html());
	    val = Math.round(val * 2) / 2; // To round to nearest half 
	    // Make sure that the value is in 0 - 5 range, multiply to get width
	    var size = Math.max(0, (Math.min(5, val))) * 16;
	    // Create stars holder
	    var $span = $('<span />').width(size);
	    // Replace the numerical value with stars
	    $(this).html($span);
	});
} 
 
$(function() {
	    $('span.stars').stars();
});

$(function() {
    $('.lazy').lazy({
        placeholder: "data:image/gif;base64,R0lGODlhEALAPQAPzl5uLr9Nrl8e7..."
    });
    
    $('.middle a').tooltipster({
    	   animation: 'fade',
    	   delay: 100,
    	   theme: ['tooltipster-shadow', 'tooltipster-shadow-customized'],
    	   trigger: 'hover',
    	   maxWidth: 300
    	});
});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function deleteCookie(name) {
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

 
</script>
 
 <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5bda216e8a1fb80011151d18&product=inline-share-buttons"></script>
 
 
  </head>
  <body onload="javascript:hideTable();" >
    

  
  
    <%@include file="/WEB-INF/jsp/menu.jsp" %>
    
    <div class="page-container" style="font-family:Gill Sans,Helvetica;">


    <div class="container">
    
<!-- Single button -->
<!-- <div class="btn-group" >
  <button type="button" id= "amit" class="btn btn-danger dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Filter by Store <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
 		 <li><a href="hotdeals.jsp">All</a></li>
		<li><a href="hotdeals.jsp?item=amazon">Amazon</a></li>
		<li><a href="hotdeals.jsp?item=bestbuy">Best Buy</a></li>
		<li><a href="hotdeals.jsp?item=target">Target</a></li>
		<li><a href="hotdeals.jsp?item=groupon">Groupon</a></li>
		<li><a href="hotdeals.jsp?item=macy">Macys</a></li>
		<li><a href="hotdeals.jsp?item=nordstrom">Nordstrom</a></li>
		<li><a href="hotdeals.jsp?item=walmart">Walmart</a></li>
		<li><a href="hotdeals.jsp?item=woot">Woot</a></li>
  </ul>

</div> -->
<!-- Single button -->
<!-- div class="btn-group">
  <button type="button" class="btn btn-info dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Deals by Category <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <li><a href="hotdeals.jsp">All</a></li>
		

		
		<li><a href="hotdeals.jsp?item=appliances&t=c">Appliances</a></li>
		<li><a href="hotdeals.jsp?item=arts&t=c">Arts</a></li>
		<li><a href="hotdeals.jsp?item=automative&t=c">Automative</a></li>
		<li><a href="hotdeals.jsp?item=baby&t=c">Baby</a></li>
		<li><a href="hotdeals.jsp?item=beauty&t=c">Beauty</a></li>
		<li><a href="hotdeals.jsp?item=books&t=c">Books</a></li>
		<li><a href="hotdeals.jsp?item=macys,nordstrom,accessories,clothing,shoes,jewelry&t=c">Clothing, Shoes & Jewelry</a></li>
		<li><a href="hotdeals.jsp?item=bestbuy,electronics&t=c">Electronics</a></li>
		<li><a href="hotdeals.jsp?item=grocery&t=c">Grocery</a></li>
		<li><a href="hotdeals.jsp?item=health,personal,care&t=c">Health & Personal Care</a></li>
		<li><a href="hotdeals.jsp?item=home,kitchen&t=c">Home & Kitchen</a></li>
		<li><a href="hotdeals.jsp?item=office&t=c">Office Products</a></li>
		<li><a href="hotdeals.jsp?item=pet&t=c">Pet Supplies</a></li>
		<li><a href="hotdeals.jsp?item=toys&t=c">Toys & Games</a></li>

  </ul>

</div> -->
<!-- Latest Deals -->
<%--  <div id='divForLatestTable'>
<table id="latestdealstable" class="display" cellspacing="0" width="100%" style="font-size:11px;">
        <thead>
            <tr>
                <th><span aria-hidden="true" ></span>&nbsp;Latest Deals</th>
                <th><span  aria-hidden="true"></span>&nbsp;</th>
                <th><span aria-hidden="true"></span>&nbsp;</th>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
               <th><span  aria-hidden="true"></span>&nbsp;Latest Deals</th>
                <th><span aria-hidden="true"></span>&nbsp;</th>
                <th><span aria-hidden="true"></span>&nbsp;</th>
            </tr>
        </tfoot>
        
        
            <tbody>
            <tr style="background-color:white;">
			<%
		  			for (int i = 0; i< mListOfLatestDeals.size();i++) {
		  				SDModel mModel = mListOfLatestDeals.get(i);
		  				if(i%3 == 0){
			%>
			
			</tr><tr style="background-color:white;">
			<% }%>
			
			<td align="center" ><a href="<%=mModel.getUrl()%>" target="_blank">
			<table >
			<tr><td align="center">
			<%if(mModel.getImageUrl() != null){ %>
			<img src="<%=mModel.getImageUrl()%>" alt="<%=mModel.getImgAlt()%>" style="max-height: 120px; max-width:120px;  "/>
			<%}else{ %>
				<span class="glyphicon glyphicon-camera" aria-hidden="true"></span>&nbsp;
			<% }%>
			</a>
			</td>
			</tr>
			<tr><td style="vertical-align:bottom;text-align:center;border:none;" >
			<a href="<%=mModel.getUrl()%>" target="_blank"><%=mModel.getText()%></a>
			<br/><small><%=mModel.getSource()%></small>
			</td>
			</tr>
			</table>
			<%} %>	
			</tr>
        </tbody>
        
 
    </table>
</div> --%>

<!-- Hot Deals	 -->

<div class="row">
<%-- <div class="col-sm-2 col-md-2 col-lg-2" style="font-size:11px;">
	<div class="row">
	<!-- 
	<div class="col-sm-12 col-md-12 col-lg-12">
		
		<ul class="dropdown-menu" id="menu" style="width:100%;font-size:11px;">
			<li>Filter by Store
			
				<ul>
					<li><a href="deals">All</a></li>
					<li><a href="deals?item=amazon">Amazon</a></li>
					<li><a href="deals?item=bestbuy">Best Buy</a></li>
					<li><a href="deals?item=target">Target</a></li>
					<li><a href="deals?item=groupon">Groupon</a></li>
					<li><a href="deals?item=macy">Macys</a></li>
					<li><a href="deals?item=nordstrom">Nordstrom</a></li>
					<li><a href="deals?item=walmart">Walmart</a></li>
					<li><a href="deals?item=woot">Woot</a></li>
				</ul>
		 	</li>
		 	<li>Deals by Category 
		 		<ul>
			 		<li><a href="deals">All</a></li>
					<li><a href="deals?item=appliances&t=c">Appliances</a></li>
					<li><a href="deals?item=arts&t=c">Arts</a></li>
					<li><a href="deals?item=automative&t=c">Automative</a></li>
					<li><a href="deals?item=baby&t=c">Baby</a></li>
					<li><a href="deals?item=beauty&t=c">Beauty</a></li>
					<li><a href="deals?item=books&t=c">Books</a></li>
					<li><a href="deals?item=macys,nordstrom,accessories,clothing,shoes,jewelry&t=c">Clothing, Shoes & Jewelry</a></li>
					<li><a href="deals?item=bestbuy,electronics&t=c">Electronics</a></li>
					<li><a href="deals?item=grocery&t=c">Grocery</a></li>
					<li><a href="deals?item=health,personal,care&t=c">Health & Personal Care</a></li>
					<li><a href="deals?item=home,kitchen&t=c">Home & Kitchen</a></li>
					<li><a href="deals?item=office&t=c">Office Products</a></li>
					<li><a href="deals?item=pet&t=c">Pet Supplies</a></li>
					<li><a href="deals?item=toys&t=c">Toys & Games</a></li>
				</ul>
		 	</li>	 
	  	</ul>
	  	
	  </div>
	   -->
	  <div class="col-sm-12 col-md-12 col-lg-12" >
	  		<div class="filterCheckboxes">
		  		<div class="filtersCheckboxHeading"><i class="fas fa-bars"></i> Show results by store</div>
		  		<div class="filtersContent">
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_1" value="all"  <%if(mSelectedItem == null){%>  checked   <%} %> ><label for="chkFilterStore_1">All</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_2" value="amazon" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "amazon")){%>  checked   <%} %> ><label for="chkFilterStore_2">Amazon</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_3" value="bestbuy" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "bestbuy")){%>  checked   <%} %> ><label for="chkFilterStore_3">Best Buy</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_4" value="target" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "target")){%>  checked   <%} %> ><label for="chkFilterStore_4">Target</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_5" value="groupon" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "groupon")){%>  checked   <%} %> ><label for="chkFilterStore_5">Groupon</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_6" value="macy" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "macy")){%>  checked   <%} %> ><label for="chkFilterStore_6">Macys</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_7" value="nordstrom" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "nordstrom")){%>  checked   <%} %> ><label for="chkFilterStore_7">Nordstrom</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_8" value="walmart" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "walmart")){%>  checked   <%} %> ><label for="chkFilterStore_8">Walmart</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_9" value="woot" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "woot")){%>  checked   <%} %> ><label for="chkFilterStore_9">Woot</label></div>
	  			</div>
	  		</div>
	  </div>
	  <div class="col-sm-12 col-md-12 col-lg-12" >
	  		<div class="filterCheckboxes">
		  		<div class="filtersCheckboxHeading"><i class="fas fa-bars"></i> Show results by Category</div>
		  		<div class="filtersContent">
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_1" value="all"  <%if(mSelectedItem == null){%>  checked   <%} %> ><label for="chkFilterCategory_1">All</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_2" value="appliances"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "appliances")){%>  checked   <%} %> ><label for="chkFilterCategory_2">Appliances</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_3" value="arts"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "arts")){%>  checked   <%} %> ><label for="chkFilterCategory_3">Arts</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_4" value="automative"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "automative")){%>  checked   <%} %> ><label for="chkFilterCategory_4">Automative</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_5" value="baby"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "baby")){%>  checked   <%} %> ><label for="chkFilterCategory_5">Baby</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_6" value="beauty"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "beauty")){%>  checked   <%} %> ><label for="chkFilterCategory_6">Beauty</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_7" value="books"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "books")){%>  checked   <%} %> ><label for="chkFilterCategory_7">Books</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_8" value="macys,nordstrom,accessories,clothing,shoes,jewelry"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "macys,nordstrom,accessories,clothing,shoes,jewelry")){%>  checked   <%} %> ><label for="chkFilterCategory_8">Clothing, Shoes & Jewelry</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_9" value="bestbuy,electronics"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "bestbuy,electronics")){%>  checked   <%} %> ><label for="chkFilterCategory_9">Electronics</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_10" value="grocery"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "grocery")){%>  checked   <%} %> ><label for="chkFilterCategory_10">Grocery</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_11" value="health,personal,care"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "health,personal,care")){%>  checked   <%} %> ><label for="chkFilterCategory_11">Health & Personal Care</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_12" value="home,kitchen"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "home,kitchen")){%>  checked   <%} %> ><label for="chkFilterCategory_12">Home & Kitchen</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_13" value="office"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "office")){%>  checked   <%} %> ><label for="chkFilterCategory_13">Office Products</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_14" value="pet"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "pet")){%>  checked   <%} %> ><label for="chkFilterCategory_14">Pet Supplies</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_15" value="toys"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "toys")){%>  checked   <%} %> ><label for="chkFilterCategory_15">Toys & Games</label></div>
		  		</div>
	  		</div>
	  </div>
  </div>
  
</div> --%>



<div class="col-sm-12 col-md-12 col-lg-12">
	<div class="row" id="couponstable">
	
	<!-- Carousel START -->

	<% // Leaving this placeholder incase i need to put carousel code back in place.%>
	<!-- Carousel END -->
	<div class="purvice-banner col-12">
		<div class="row" style="float:center;">
			<div class="col-xs-4 col-sm-2  col-md-2 col-lg-2 text-center">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><a href="/purvice1/deals?item=amazon"><small>Amazon</small></a></div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><a href="/purvice1/deals?item=bestbuy"><small>Best Buy</small></a></div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><a href="/purvice1/deals?item=walmart"><small>Walmart</small></a></div>
				</div>
			</div>
			<div class="col-xs-4 col-sm-8  col-md-8 col-lg-8 text-center">
				<h1>Purvice Deals</h1>
				<h6>New deals every 30 minutes!</h6>
			</div>
			<div class="col-xs-4 col-sm-2  col-md-2 col-lg-2 text-center">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><a href="/purvice1/deals?item=macy"><small>Macys</small></a></div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><a href="/purvice1/deals?item=target"><small>Target</small></a></div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><a href="/purvice1/deals"><small>All Deals</small></a></div>
				</div>

			</div>
		</div>
	</div>

	<div class="fav-container col-12">
				<h2>My Favorites</h2>
				<div class="favLink"><span class="showFav up-fav"><i class="fa fa-heart"></i>My Favorites <i class="fas fa-sort-up"></i></span></div>
							<div class="favItems row" id="favItems"><div class="fav-result">Sorry, you don't have any Saved Deals.</div></div>
			</div>
	<div class="col-12 filter_maincontainer">
	<div class="filter_container ">
	<div class="filterText"><i class="fas fa-filter"></i> Filter By: </div> 
	  		<div class="filterCheckboxes">
		  		<div class="filtersCheckboxHeading"><i class="fas fa-store"></i> Store</div>
		  		<div class="filtersContent">
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_1" value="all"  <%if(mSelectedItem == null){%>  checked   <%} %> ><label for="chkFilterStore_1">All</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_2" value="amazon" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "amazon")){%>  checked   <%} %> ><label for="chkFilterStore_2">Amazon</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_3" value="bestbuy" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "bestbuy")){%>  checked   <%} %> ><label for="chkFilterStore_3">Best Buy</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_4" value="target" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "target")){%>  checked   <%} %> ><label for="chkFilterStore_4">Target</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_5" value="groupon" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "groupon")){%>  checked   <%} %> ><label for="chkFilterStore_5">Groupon</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_6" value="macy" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "macy")){%>  checked   <%} %> ><label for="chkFilterStore_6">Macys</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_7" value="nordstrom" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "nordstrom")){%>  checked   <%} %> ><label for="chkFilterStore_7">Nordstrom</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_8" value="walmart" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "walmart")){%>  checked   <%} %> ><label for="chkFilterStore_8">Walmart</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterStore" id="chkFilterStore_9" value="woot" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "woot")){%>  checked   <%} %> ><label for="chkFilterStore_9">Woot</label></div>
	  			</div>
	  		</div>
	  		<div class="filterCheckboxes">
		  		<div class="filtersCheckboxHeading"><i class="fas fa-list-alt"></i> Category</div>
		  		<div class="filtersContent">
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_1" value="all"  <%if(mSelectedItem == null){%>  checked   <%} %> ><label for="chkFilterCategory_1">All</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_2" value="appliances"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "appliances")){%>  checked   <%} %> ><label for="chkFilterCategory_2">Appliances</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_3" value="arts"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "arts")){%>  checked   <%} %> ><label for="chkFilterCategory_3">Arts</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_4" value="automative"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "automative")){%>  checked   <%} %> ><label for="chkFilterCategory_4">Automative</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_5" value="baby"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "baby")){%>  checked   <%} %> ><label for="chkFilterCategory_5">Baby</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_6" value="beauty"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "beauty")){%>  checked   <%} %> ><label for="chkFilterCategory_6">Beauty</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_7" value="books"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "books")){%>  checked   <%} %> ><label for="chkFilterCategory_7">Books</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_8" value="macys,nordstrom,accessories,clothing,shoes,jewelry"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "macys,nordstrom,accessories,clothing,shoes,jewelry")){%>  checked   <%} %> ><label for="chkFilterCategory_8">Clothing, Shoes & Jewelry</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_9" value="bestbuy,electronics"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "bestbuy,electronics")){%>  checked   <%} %> ><label for="chkFilterCategory_9">Electronics</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_10" value="grocery"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "grocery")){%>  checked   <%} %> ><label for="chkFilterCategory_10">Grocery</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_11" value="health,personal,care"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "health,personal,care")){%>  checked   <%} %> ><label for="chkFilterCategory_11">Health & Personal Care</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_12" value="home,kitchen"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "home,kitchen")){%>  checked   <%} %> ><label for="chkFilterCategory_12">Home & Kitchen</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_13" value="office"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "office")){%>  checked   <%} %> ><label for="chkFilterCategory_13">Office Products</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_14" value="pet"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "pet")){%>  checked   <%} %> ><label for="chkFilterCategory_14">Pet Supplies</label></div>
			  		<div class="checkFilter"><input type="checkbox" class="chkFilterCategory" id="chkFilterCategory_15" value="toys"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "toys")){%>  checked   <%} %> ><label for="chkFilterCategory_15">Toys & Games</label></div>
		  		</div>
	  		</div>
	  		<div class="favLink"><span class="showFav down-fav"><i class="fa fa-heart"></i>My Favorites <i class="fas fa-sort-down"></i></span></div>
	</div>
	</div>
	<% for (int i = 0; i< mListOfData.size();i++) {
  			Deal mModel = mListOfData.get(i);
  	%>
	<div class="col-xs-6 col-sm-6 teee col-md-3 col-lg-2" style="height:300px;border-radius:4px;margin-bottom:20px;padding:10px;">
	
		<div class="items" style="height:300px;" id="favDeals_<%=mModel.getUniqueId()%>">
		
		<% if(mModel.getDiscountPercent() > 0) {%>
			<div class="onSaleTriangle"><span class="salePrcntTxt"><%=mModel.getDiscountPercent()%>% Off</span></div>
		<%} %>
		
										
		<div class="favHotDeals"><i class="fas fa-heart"></i></div>
			<div class="bottom">
				<small>
				<%if(mModel.getSourceFavIco() != null){ %>
				<div class="brandDetail brandImg" >
					<img src="<%=mModel.getSourceFavIco()%>" alt="<%=mModel.getSource()%>" />
					<%if (mModel != null && mModel.getOfferType() != null){%>
						<%=mModel.getOfferType()%>
					<%} %>	
				</div>
				<%} else{ %>
					<div class="brandDetail brandText"><%=mModel.getSource()%></div>
				<%} %>
				</small>
				
			</div>
			<a href="<% if(mModel != null && mModel.getAffliateLink() != null){%><%=mModel.getAffliateLink()%><%}else{%><%=mModel.getUrl()%><%} %>" target="_blank">
				<%if(mModel.getCustomerReviewAverage() != null ){%>
				<span class="stars"><%=mModel.getCustomerReviewAverage()%></span>
				<%} %>
				<div class="top">
					
					
					<%if(mModel.getImageUrl() != null && !mModel.getImageUrl().contains("purvice_deals.jpg")){ %>
						<img src="img/default.png" alt="<%=mModel.getImgAlt()%>" class="lazy" data-src="<%=mModel.getImageUrl()%>" />
					<%}else if(mModel.getImageUrl() != null && mModel.getImageUrl().contains("purvice_deals.jpg")){ %>
						<div class='text-center' style=""><span style='margin-top:10px;font-size:100px;color:orange;' class="glyphicon glyphicon-tags" aria-hidden="true"></span></div>
					<%}else{ %>
						<span class="glyphicon glyphicon-camera" aria-hidden="true"></span>
					<% }%>
				</div>
			</a>
			<div class="middle">
				<a href="<% if(mModel != null && mModel.getAffliateLink() != null){%><%=mModel.getAffliateLink()%><%}else{%><%=mModel.getUrl()%><%} %>" target="_blank" data-tooltip-content="#tooltip_content_<%=i%>"  title="<%=mModel.getText()%>"><%=mModel.getText()%></a>
				<div class="tooltip_templates" style="display:none;">
    <span id="tooltip_content_<%=i%>">
        <%=mModel.getText()%>
    </span>
</div>
			</div>

		</div>
	</div>
	<% }%>
	</div>
</div>
	
</div>
  </div>
  
  </div>
  
<%--   <div class="container-fluid" style="background-color:#cacaca;padding-left:0">
	<div id="main-footer" class="main-footer">
      <div class="">

        <p class="copy">Copyright ©  <%= Calendar.getInstance().get(Calendar.YEAR)  %> purvice LLC. All Rights Reserved.</p>
        <p class="footer-link mx-auto">
        	<a href="../#/home/about-us">About Us</a> 
        	<a href="../#/home/faq">FAQ</a>
        	<a href="../#/home/privacy">Privacy</a>
          	<a href="../#/home/terms-of-service">Terms</a>
          	<a href="../#/home/copyright">Copyright</a>
        </p>
      </div>
    </div>
  </div> --%>
  </body>
</html>


