<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.giftcard.deals.model.*" %> 
<%@ page import="com.giftcard.scapper.helper.*" %>    
<%@ page import="com.giftcard.scrapper.job.*" %>      
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<ProductModel> mListOfProducts =   PullDataHelper.pullCurrentProducts();


%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>purvice - Hot Deals, Gift Cards and more!</title>
    <!-- Bootstrap -->
    <link href="bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>

   
   
body { padding-top: 15px; }    
.carousel-caption {
    position: relative;
    left: auto;
    right: auto;
}

.carousel-inner img {
  margin: auto;
}
 </style>
 
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
    <script src="jquery-ui-1.11.4/external/jquery/jquery.js"></script>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>    
    
 
 
 
 
 
 
 
 
  </head>
  <body>

    <%@include file="/WEB-INF/jsp/menu.jsp" %>
    
<div class="container">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox" style="height:200px;">
    
		<%
		int mCounter=0;
		for(ProductModel mProductModel: mListOfProducts){
			mCounter++;
			if(mCounter < 106){
				continue;
			}
			if(mCounter > 125){
				break;
				
			}
			
			
		%>
		
		<%if(mCounter == 106){ %>
		<div class="item active" >
		<% }else{%>
		<div class="item">
		<%} %>
			<a href="<%=mProductModel.getDestUrl() %>" target="_blank">
        		<img src="<%=mProductModel.getUrl()%>" alt="<%=mProductModel.getProductName() %>" style="height: 170px;  "/>
        		

			           </a>
			           			        <div class="carousel-caption" >
			           <h4><small><%=mProductModel.getProductName() %></small></h4>
			          <br/><h2><small>Price:&nbsp;&nbsp;<%=mProductModel.getPrice() %></small></h2>
		
			           </div>
     	 	</div>

		<% 
		}
		%>  
    


    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<br/>

 <div class="container">
  <div class="row" >
<%
for(ProductModel mProductModel: mListOfProducts){
%>
	<div class="col-sm-4"  style="overflow: hidden;float:center;min-height:350px;">
	<a href="<%=mProductModel.getDestUrl() %>" target="_blank">
	<img src="<%=mProductModel.getUrl() %>" class="img-thumbnail" alt="<%=mProductModel.getProductName() %>"  style="max-height: 200px;max-width:200px;" >
	</a>
	<br/><h6><small><%=mProductModel.getProductName() %></small></h6>
	<br/><h4><medium>Price:&nbsp;&nbsp;<%=mProductModel.getPrice() %></medium></h4>
	</div>
<% 
}
%>
  </div>
    
  </div>

<!-- 
    <div class="container">
   <div class="row">
    <div class="col-sm-4" ><img src="img/test18.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
    
    <div class="col-sm-4" \><img src="img/test16.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>

    <div class="col-sm-4" ><img src="img/test15.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
    <div class="col-sm-4" ><img src="img/test14.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
    <div class="col-sm-4" \><img src="img/test13.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>

    <div class="col-sm-4" ><img src="img/test12.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
    <div class="col-sm-4" ><img src="img/test11.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
    <div class="col-sm-4" \><img src="img/test10.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
    <div class="col-sm-4" ><img src="img/test17.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></div>
  </div>
    
  </div>
   -->
  
  </body>
</html>


