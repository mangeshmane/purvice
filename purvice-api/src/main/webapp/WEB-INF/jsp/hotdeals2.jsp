<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="com.giftcard.deals.model.*" %> 
<%@ page import="com.giftcard.scapper.helper.*" %>    
<%@ page import="com.giftcard.scrapper.job.*" %>      
<%@ page import="com.auth0.*" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<SDModel> mListOfData = null;
if(request.getParameter("frmSearchParam") != null && !request.getParameter("frmSearchParam").equals("")){
	mListOfData =   ScrapperServiceImpl.getInstance().getMapOfData(request.getParameter("frmSearchParam"),"c");
}
else{
	mListOfData =   ScrapperServiceImpl.getInstance().getMapOfData(request.getParameter("item"),request.getParameter("t"));
}

List<SDModel> mListOfLatestDeals =   ScrapperServiceImpl.getInstance().getListOutputLatest();
String mSelectedItem = request.getParameter("item");
System.out.println("mSelectedItem:"+mSelectedItem);

%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="600" >
    
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>purvice - Hot Deals!</title>

    <!-- Bootstrap -->
    
    <link href="DataTables-1.10.9/media/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="css/bootstrap_4.0.0.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9 {
    float: left;

} 
.navbar {
    position: relative;
    min-height: 50px;
    margin-bottom: 20px;
    border: 1px solid transparent;
}

@media (min-width: 1200px){
.container {
    max-width: 100%;
}
}
.row{
	display:block;
}
 .ui-menu { width: 170px; }  
 .ui-widget {
    font-family: "Open Sans", Arial, sans-serif !important;
    font-size: 14px !important;
}
.dropdown-menu{
	position:relative;
	border-radius:3px;
	z-index:1;
}
.ui-widget-content a{
	display:block;
}
.filterCheckboxes{
  border:1px solid #aaaaaa;
  background-color:#fff;
  color:#222;
  border-radius:3px;
  margin-top:10px;
  padding:5px;
}
.filterCheckboxes inputDiv {
	width:100%;
}
.filterCheckboxes input{
	margin:0 10px 0 5px;
}
.filtersCheckboxHeading{
	font-weight:bold;
	border-bottom:1px solid #ddd;
	padding:3px 0 10px 8px;
	margin-bottom:10px;
}
body { background-color: #e4e4e4;font-size:14px;}     
div.container {
        width: 100%;
    }
 .dropdown-menu>li {
    font-size: 11px;
    font-weight:bold;
    color:#001744;
  }   
   .dropdown-menu>li>ul>li>a {
    font-size: 11px;
    font-weight:bold;
    color:#001744;
  }  
.dataTables_filter input {

    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-top-color: rgb(204, 204, 204);
    border-top-style: solid;
    border-top-width: 1px;
}
.items {
    background-color: #fff;
    margin: 0 0 16px 0;
    vertical-align: top;
    overflow: hidden;
    border-radius: 8px;
    box-sizing: border-box;
    /* height:100%; */
    height:335px;
}
.items .top {
    position: relative;
    padding-bottom: 5px;
    overflow: hidden;
    z-index: 0;
    /* height:65%; */
      height:75%;
     
}
.items .middle {
    box-sizing: border-box;
    padding: 0 5px 5px;
    text-align:center;
    font-size:11px;
}
.items .bottom {
    height: 15px;
    padding: 0 5px 5px;
    box-sizing: border-box;
    text-align:center;
    font-weight:bold;
    color:grey;
}
.items .top img {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    width: 100%;
}
#main-footer {
    width: 100%;
    padding: 0px 7px 10px;
    text-align: center;
    position: fixed;
    margin-top: 15px;
    bottom: 0;
    z-index: 999;
    background-color:#cacaca;
}
#main-footer .copy {
    float: left;
    display: inline-block;
    color: #7b7e81;
    font-size: 13px;
    margin-bottom: 0px;
    margin-top: 10px;
}
#main-footer .footer-link {
    display: inline-block;
    float: right;
    color: #7b7e81;
    font-size: 13px;
    margin-bottom: 0px;
    margin-top: 10px;
}
.main-footer a {
    color: #7b7e81;
    margin-right:5px;
}
.main-footer a:hover {
    color: #20a8d8;
   cursor:pointer;
}
.carousel-caption {
    position: relative;
    left: auto;
    right: auto;
}
.carousel-indicators {
    position: absolute;
    bottom: 10px;
    left: 50%;
    z-index: 15;
    width: 60%;
    padding-left: 0;
    margin-left: -30%;
    text-align: center;
    list-style: none;
    display:block;
}
.carousel-indicators li {
    display: inline-block;
    width: 10px;
    height: 10px;
    margin: 1px;
    text-indent: -999px;
    cursor: pointer;
    background-color: #000 \9;
    background-color: rgba(0,0,0,0);
    border: 1px solid #fff;
    border-radius: 10px;
}
.center-block {
    display: block;
    margin-right: auto;
    margin-left: auto;
}
span.stars, span.stars span {
    display: block;
    background: url(img/stars.png) 0 -16px repeat-x;
    width: 80px;
    height: 16px;
}

span.stars span {
    background-position: 0 0;
}

 </style>
 
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
    <script src="jquery-ui-1.11.4/external/jquery/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script> 
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap_4.0.0.min.js"></script>
    <script src="DataTables-1.10.9/media/js/jquery.dataTables.min.js"></script>  
    
 <script>
 
 $( function() {
	    $( "#menu" ).menu();
	  } );
 $(document).ready(function() {
    $('#couponstable').DataTable({
    	/*"iDisplayLength": 100,
    	"order": [[0, "desc"]]*/
    	"iDisplayLength": 100,
    	"bLengthChange": false,
    	"bSort": false,
    	"order": []
    	,  "columnDefs": [
    	                  { "width": "33%", "targets": 0 }
    	                  ]
    	/*
        "columnDefs": [
                       {
                           "targets": [ 4 ],
                           "visible": false,
                           "searchable": false
                       }
                   ]          
               ,"order": [[4, "desc"]]    
    	*/
    });
} );
 
 
 $(document).ready(function() {
	    $('#latestdealstable').DataTable({

	    	"iDisplayLength": 10,
	    	"bFilter": false,
	    	"bInfo": false,
	    	"bSort": false,
	    	"bLengthChange": false,
	    	"fixedColumns": true,
	    	"bPaginate": false,
	    	"order": []
	    	,  "columnDefs": [
	    	                  { "width": "33%", "targets": 0 }
	    	                  ]

	    });
	} );
 
 $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { 
	    console.log(message);
	};
	
	function hideTable(){
	
	<%if(request.getParameter("item") != null){%>
	 $('#divForLatestTable').hide();
	<%}%>
	}
	

 
 
 $(document).ready(function() {
	    //set initial state.
	 $(".chkFilterStore").click(function(e){
		 var checkedValues = $('.chkFilterStore:checked').map(function() {
			    return this.value;
			}).get();
		 mReqParam = checkedValues;
		 if(e.target.value == "all" && mReqParam != null && mReqParam.indexOf("all") != -1){
			 mReqParam = null;
		 }
		 else{
			 mReqParam = mReqParam.toString().replace("all","");
			 if(mReqParam.indexOf(",") == 0 && mReqParam.length > 1){
				 mReqParam = mReqParam.substring(1);
			 }
		 }
		 
		 if(mReqParam != null && mReqParam != ""){
			 window.location.assign("deals?item="+mReqParam);
		 }
		 else{
			 window.location.assign("deals");
		 }
		 return;
		 
	 });
	    
	 $(".chkFilterCategory").click(function(e){
		 var checkedValues = $('.chkFilterCategory:checked').map(function() {
			    return this.value;
			}).get();
		 mReqParam = checkedValues;
		 if(e.target.value == "all" && mReqParam != null && mReqParam.indexOf("all") != -1){
			 mReqParam = null;
		 }
		 else{
			 mReqParam = mReqParam.toString().replace("all","");
			 if(mReqParam.indexOf(",") == 0 && mReqParam.length > 1){
				 mReqParam = mReqParam.substring(1);
			 }
		 }
		 
		 if(mReqParam != null && mReqParam != ""){
			 window.location.assign("deals?&t=c&item="+mReqParam);
		 }
		 else{
			 window.location.assign("deals");
		 }
		 return;
	 });  
	 
	 
	 $("#idFrmSearchParam").keypress(function(e){
		 
		if(e.which == 13) {
			console.log("@@@@ACTEST 1:"+$("#idFrmSearchParam").val());
			//window.location.replace("hotdeals.jsp?t=search&item="+$("#idFrmSearchParam").val());
			window.location.assign("deals");
			return;
		}
		
		return;
		 
	 });
	    
	    
 });

$.fn.stars = function() {
	return $(this).each(function() {
	    // Get the value
	    var val = parseFloat($(this).html());
	    val = Math.round(val * 2) / 2; /* To round to nearest half */
	    // Make sure that the value is in 0 - 5 range, multiply to get width
	    var size = Math.max(0, (Math.min(5, val))) * 16;
	    // Create stars holder
	    var $span = $('<span />').width(size);
	    // Replace the numerical value with stars
	    $(this).html($span);
	});
} 
 
$(function() {
	    $('span.stars').stars();
});
 
</script>
 
 
 
  </head>
  <body onload="javascript:hideTable();" >
    

  
  
   <%@include file="/WEB-INF/jsp/menu.jsp" %> 
    
    <div class="page-container" style="font-family:Gill Sans,Helvetica;">


    <div class="container">
  

<!-- Hot Deals	 -->

<div class="row">
<div class="col-sm-2 col-md-2 col-lg-2" style="font-size:11px;">
	<div class="row">
	
	  <div class="col-sm-12 col-md-12 col-lg-12" >
	  		<div class="filterCheckboxes">
		  		<div class="filtersCheckboxHeading">Show results by store</div>
		  		
		  		<div><input type="checkbox" class="chkFilterStore" value="all"  <%if(mSelectedItem == null){%>  checked   <%} %> >All</div>
		  		<div><input type="checkbox" class="chkFilterStore" value="amazon" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "amazon")){%>  checked   <%} %> >Amazon</div>
		  		<div><input type="checkbox" class="chkFilterStore" value="bestbuy" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "bestbuy")){%>  checked   <%} %> >Best Buy</div>
		  		<div><input type="checkbox" class="chkFilterStore" value="target" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "target")){%>  checked   <%} %> >Target</div>
		  		<div><input type="checkbox" class="chkFilterStore" value="groupon" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "groupon")){%>  checked   <%} %> >Groupon</div>
		  		<div><input type="checkbox" class="chkFilterStore" value="macy" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "macy")){%>  checked   <%} %> >Macys</div>
		  		<div><input type="checkbox" class="chkFilterStore" value="nordstrom" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "nordstrom")){%>  checked   <%} %> >Nordstrom</div>
		  		<div><input type="checkbox" class="chkFilterStore" value="walmart" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "walmart")){%>  checked   <%} %> >Walmart</div>
		  		<div><input type="checkbox" class="chkFilterStore" value="woot" <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "woot")){%>  checked   <%} %> >Woot</div>
	  		</div>
	  </div>
	  <div class="col-sm-12 col-md-12 col-lg-12" >
	  		<div class="filterCheckboxes">
		  		<div class="filtersCheckboxHeading">Show results by Category</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="all"  <%if(mSelectedItem == null){%>  checked   <%} %> >All</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="appliances"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "appliances")){%>  checked   <%} %> >Appliances</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="arts"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "arts")){%>  checked   <%} %> >Arts</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="automative"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "automative")){%>  checked   <%} %> >Automative</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="baby"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "baby")){%>  checked   <%} %> >Baby</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="beauty"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "beauty")){%>  checked   <%} %> >Beauty</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="books"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "books")){%>  checked   <%} %> >Books</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="macys,nordstrom,accessories,clothing,shoes,jewelry"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "macys,nordstrom,accessories,clothing,shoes,jewelry")){%>  checked   <%} %> >Clothing, Shoes & Jewelry</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="bestbuy,electronics"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "bestbuy,electronics")){%>  checked   <%} %> >Electronics</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="grocery"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "grocery")){%>  checked   <%} %> >Grocery</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="health,personal,care"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "health,personal,care")){%>  checked   <%} %> >Health & Personal Care</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="home,kitchen"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "home,kitchen")){%>  checked   <%} %> >Home & Kitchen</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="office"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "office")){%>  checked   <%} %> >Office Products</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="pet"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "pet")){%>  checked   <%} %> >Pet Supplies</div>
		  		<div><input type="checkbox" class="chkFilterCategory" value="toys"  <%if(ScrapperServiceImpl.isCheckboxChecked(mSelectedItem, "toys")){%>  checked   <%} %> >Toys & Games</div>
		  		
	  		</div>
	  </div>
  </div>
  
</div>



<div class="col-sm-10 col-md-10 col-lg-10">
	<div class="row" id="couponstable">
	
	
	<!-- Carousel START -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-bottom:10px;background-color:#888484;width:100%;">
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
	    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	    <li data-target="#myCarousel" data-slide-to="1"></li>
	    <li data-target="#myCarousel" data-slide-to="2"></li>
	    <li data-target="#myCarousel" data-slide-to="3"></li>

	  </ol>
	
	  <!-- Wrapper for slides -->
	  <div class="carousel-inner" style="max-height: 250px !important;">
	  	<% int mCarouselCounter  = 0;%>
	  	<% for (int i = 0; i< mListOfData.size();i=i+4) {
	  		SDModel mModel = null;
  			mCarouselCounter++;
  			if(mCarouselCounter >= 16){
  				break;
  			}
  			
  		%>
  		
	    <div class="carousel-item <%if(mCarouselCounter <= 1) {%>active<%}%>" >
	    
	    	<%for (int j = 0; j < 4;j++) {
	    		if(mListOfData != null && mListOfData.size() <= (i+j)){
	    			continue;
	    		}
	    		
	    		mModel = mListOfData.get(i+j);
	    		%>
	    		<div class="col-sm-12 col-md-4 col-lg-3" >
	    
		    <%if(mModel.getImageUrl() != null && !mModel.getImageUrl().contains("purvice_deals.jpg")){ %>
				<img src="<%=mModel.getImageUrl()%>" alt="<%=mModel.getImgAlt()%>" class="img-responsive center-block" style="max-height:150px;max-width:150px;height:auto;width:auto;margin-top:5px;"/>
			<%}else if(mModel.getImageUrl() != null && mModel.getImageUrl().contains("purvice_deals.jpg")){ %>
				<div class='text-center' style=""><span style='margin-top:10px;font-size:100px;color:orange;' class="glyphicon glyphicon-tags" aria-hidden="true"></span></div>
			<%}else{ %>
				<span class="glyphicon glyphicon-camera" aria-hidden="true"></span>
			<% }%>
	      
	      
	      
	      <div class="carousel-caption">
	        <h3>
	        		<%if(mModel.getSourceFavIco() != null){ %>
				<img src="<%=mModel.getSourceFavIco()%>" alt="<%=mModel.getSource()%>" style="height:auto;width:auto;max-height:10px;max-width:10px;"/>
				<%} else{ %>
					<%=mModel.getSource()%>
				<%} %>
	        </h3>
	        <p ><a href="<% if(mModel != null && mModel.getAffliateLink() != null){%><%=mModel.getAffliateLink()%><%}else{%><%=mModel.getUrl()%><%} %>" target="_blank" style="color:white;font-size:12px;"><%=mModel.getText()%></a></p>
	      </div>
	      
	      </div>
	      
	      <%} %>
	      



	      
	    </div>  		
  		<% }%>
	  </div>
		
		
		
		
	  <!-- Left and right controls -->
	 
	  <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
	    <span class="carousel-control-prev-icon"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#myCarousel" data-slide="next">
	    <span class="carousel-control-next-icon"></span>
	    <span class="sr-only">Next</span>
	  </a>
	</div>

	<!-- Carousel END -->
	
	
	
	<% for (int i = 0; i< mListOfData.size();i++) {
  			SDModel mModel = mListOfData.get(i);
  	%>
  	
	<div class="col-sm-12 col-md-12 col-lg-2" style="border-radius:3px;margin-bottom:5px;padding:0 5px;">
		<div class="items" style="border:1px solid #ccc;height:330px;padding-bottom:10px;">
			<div class="card" style="border-bottom:0;height:100%;">
				<div style="height:75%;margin:0px auto;">
					<a href="<% if(mModel != null && mModel.getAffliateLink() != null){%><%=mModel.getAffliateLink()%><%}else{%><%=mModel.getUrl()%><%} %>" target="_blank">
						
						<%if(mModel.getImageUrl() != null && !mModel.getImageUrl().contains("purvice_deals.jpg")){ %>
					    	<img class="card-img-top" src="<%=mModel.getImageUrl()%>" alt="<%=mModel.getImgAlt()%>" style="max-width:180px;max-height:180px;width:auto;height:auto;">
					    <%}else if(mModel.getImageUrl() != null && mModel.getImageUrl().contains("purvice_deals.jpg")){ %>
					    	<div class='text-center' style=""><span style='margin-top:10px;font-size:100px;color:orange;' class="glyphicon glyphicon-tags" aria-hidden="true"></span></div>
					    	<%}else{ %>
									<span class="glyphicon glyphicon-camera" aria-hidden="true"></span>
								<% }%>
					</a>	
						
				</div>
				<%if(mModel.getCustomerReviewAverage() != null ){%>
				<span class="stars" style="margin:0px auto;"><%=mModel.getCustomerReviewAverage()%></span>
				<%} %>
			    <div class="card-body" style="border-top:1px solid #ccc;font-size:11px;padding:6px 0 0 ;text-align:center;">
			    	<p class="card-text"><a href="<%=mModel.getUrl()%>" target="_blank" title="<%=mModel.getText()%>"><%=mModel.getText()%></a></p>
			    </div>
			    <div style="text-align:center;">
					<small>
					<%if(mModel.getSourceFavIco() != null){ %>
						<img src="<%=mModel.getSourceFavIco()%>" alt="<%=mModel.getSource()%>" style="max-height:20px;max-width:180px;height:auto;width:auto;"/>
					<%} else{ %>
						<h2 class="card-title" style="margin:0px auto;font-size:18px;"><small><%=mModel.getSource()%></small></h2>
					<%} %>
					</small>
				</div>
			     
			</div>
		</div>
	</div>
	<% }%>
	</div>
</div>
	
</div>
  </div>
  
  </div>
  
<%--   <div class="container-fluid" style="background-color:#cacaca;padding-left:0">
	<div id="main-footer" class="main-footer">
      <div class="">

        <p class="copy">Copyright ©  <%= Calendar.getInstance().get(Calendar.YEAR)  %> purvice LLC. All Rights Reserved.</p>
        <p class="footer-link mx-auto">
        	<a href="../#/home/about-us">About Us</a> 
        	<a href="../#/home/privacy">Privacy</a>
          	<a href="../#/home/terms-of-service">Terms</a>
          	<a href="../#/home/copyright">Copyright</a>
        </p>
      </div>
    </div>
  </div> --%>
  </body>
</html>


