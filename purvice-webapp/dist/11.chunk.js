webpackJsonp([11,19],{

/***/ 496:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__verify_routing_module__ = __webpack_require__(684);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__verify_component__ = __webpack_require__(649);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__verify_service__ = __webpack_require__(650);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifyModule", function() { return VerifyModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var VerifyModule = (function () {
    function VerifyModule() {
    }
    return VerifyModule;
}());
VerifyModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_4__verify_routing_module__["a" /* VerifyRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["HttpModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_common__["CommonModule"]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_6__verify_service__["a" /* VerifyService */]],
        declarations: [__WEBPACK_IMPORTED_MODULE_5__verify_component__["a" /* VerifyComponent */]]
    })
], VerifyModule);

//# sourceMappingURL=verify.module.js.map

/***/ }),

/***/ 649:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__verify_service__ = __webpack_require__(650);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerifyComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var VerifyComponent = (function () {
    function VerifyComponent(_router, activatedRoute, verifyService, cdrf) {
        this._router = _router;
        this.activatedRoute = activatedRoute;
        this.verifyService = verifyService;
        this.cdrf = cdrf;
        this.showSuccess = false;
        this.showError = false;
    }
    VerifyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            _this.token = params['token'];
            _this.verifyToken(_this.token).then(function (data) {
                if (data == "SUCCESS") {
                    _this.showSuccess = true;
                }
                else {
                    _this.showError = true;
                }
                _this.cdrf.detectChanges();
            });
        });
    };
    VerifyComponent.prototype.verifyToken = function (token) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.verifyService.verifyToken(token).subscribe(function (data) {
                resolve(data.message);
            });
        });
    };
    VerifyComponent.prototype.goToLoginPage = function (model) {
        this._router.navigate(['login']);
    };
    VerifyComponent.prototype.goToHome = function () {
        this._router.navigate(['home']);
    };
    return VerifyComponent;
}());
VerifyComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__(724)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__verify_service__["a" /* VerifyService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__verify_service__["a" /* VerifyService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]) === "function" && _d || Object])
], VerifyComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=verify.component.js.map

/***/ }),

/***/ 650:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(105);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerifyService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var VerifyService = (function () {
    function VerifyService(http) {
        this.http = http;
    }
    VerifyService.prototype.verifyToken = function (token) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].baseUrl + 'verifyEmail/' + token)
            .map(function (res) {
            return res.json();
        });
    };
    return VerifyService;
}());
VerifyService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === "function" && _a || Object])
], VerifyService);

var _a;
//# sourceMappingURL=verify.service.js.map

/***/ }),

/***/ 684:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__verify_component__ = __webpack_require__(649);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerifyRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__verify_component__["a" /* VerifyComponent */],
        data: {
            title: 'Verify Registeration'
        }
    }
];
var VerifyRoutingModule = (function () {
    function VerifyRoutingModule() {
    }
    return VerifyRoutingModule;
}());
VerifyRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"].forChild(routes)],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["RouterModule"]]
    })
], VerifyRoutingModule);

//# sourceMappingURL=verify-routing.module.js.map

/***/ }),

/***/ 724:
/***/ (function(module, exports) {

module.exports = "<div class=\"app align-items-center\">\r\n    <div class=\"login margin-top-100\">\r\n        <div class=\"container\">\r\n            <div class=\"row justify-content-center\">\r\n\r\n                <div class=\"col-md-5 col-sm-12 col-xs-12 login-section\">\r\n                    <div class=\"mb-0\">\r\n                        <div class=\"card p-4\">\r\n                            <div class=\"card-block \">\r\n                                <h1 class=\"login-title\" (click)=\"goToHome();\"><img class=\"logo_image logo_icon\"   src=\"assets/img/purvice_logo_blue.jpg\"  ></h1>\r\n                                <!--<img class=\"logo_image\"   src=\"assets/img/purvice_1.png\"  >-->\r\n                                <form style=\"text-align: center;margin-top:25px;\">\r\n                                    <h2 *ngIf=\"showSuccess\" class=\"verify-message\">Thanks !!!</h2>\r\n                                    <h2 *ngIf=\"showSuccess\" class=\"verify-message\">Registering With purvice </h2>\r\n                                    <h2 *ngIf=\"showSuccess\" class=\"verify-message\">Please Click Login to Start Buying Cards </h2>\r\n                                    <h2 *ngIf=\"showError\" class=\"verify-message\">This Token is Expire</h2>\r\n                                    <h2 *ngIf=\"showError\" class=\"verify-message\">Please Use Valid One</h2>\r\n                                    <div class=\"row\">\r\n                                        <div *ngIf=\"showSuccess\" class=\" col-12\">\r\n                                            <button type=\"button\" (click)=\"goToLoginPage()\" class=\"btn btn-primary p login-button mt-1 \">Login</button>\r\n                                        </div>\r\n\r\n                                    </div>\r\n\r\n                                </form>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n\r\n    </div>\r\n\r\n    <div id=\"login-footer\">\r\n\r\n        <div class=\"content-width\">\r\n            <div class=\"section-divider\">\r\n                <div class=\"divider-div\"></div>\r\n            </div>\r\n            <p class=\"copy\">Copyright © 2018 purvice,Inc. All Rights Reserved.</p>\r\n            <p class=\"footer-link\"><a [routerLink]=\"['../home/copyright']\" class=\"mr-1 link-hove \">About Us</a>\r\n                <a [routerLinkActive]=\"['active-link']\" [routerLink]=\"['../home/faq']\" class=\"mr-1 link-hove \">FAQ</a>\r\n                <a [routerLink]=\"['../home/privacy']\" class=\"mr-1 link-hove\">Privacy</a>\r\n                <a class=\"mr-1 link-hove\" [routerLink]=\"['../home/terms-of-service']\">Terms</a>\r\n                <a class=\"mr-1 link-hove\" [routerLink]=\"['../home/copyright']\">Copyright</a>\r\n            </p>\r\n        </div>\r\n\r\n\r\n    </div>"

/***/ })

});
//# sourceMappingURL=11.chunk.js.map