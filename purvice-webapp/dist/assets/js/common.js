$(document).ready(function(){
//     setTimeout(function(){
//     if (document.cookie.indexOf('myfavData') != -1) {
//         var getFavObj = JSON.parse(getCookie("myfavData"));
//         $.each(getFavObj, function(k, v) {
//             var generateHTML = '<span class="close-card-details">x</span>';
//             generateHTML += '<img class="giftcard-image" src="' + getFavObj[k].brandSrc + '">';
//             generateHTML += '<h3 class="giftcard-title">' + getFavObj[k].brandName + '</h3>'
//             generateHTML += '<div class="gift-switch-wrapper"><div class="gift-switch"><span class="card-range">' + getFavObj[k].range + ' </span></div></div>';
//             $('#favItems .fav-result').hide();
//             $('#favItems').prepend('<div class="col-md-3 col-sm-6 col-xs-6 ml-2 fav-giftcard giftcard" ref-id="' + getFavObj[k].id + '" id="fav-' + getFavObj[k].id + '">' + generateHTML +' </div>');
//         });

//     }
// }, 3000);

$(document).on('click', '.showFav', function(e) {
    if (localStorage.getItem("myfavData") !== null) {
        $('#favItems').html('<div class="fav-result">Sorry, no Favourite Gift Cards found.</div>');
        //console.log(getCookie("myfavData"));
        var getFavObj = JSON.parse(localStorage.getItem('myfavData'));
        $.each(getFavObj, function(k, v) {
            var generateHTML = '<span class="close-card-details">x</span>';
            generateHTML += '<img class="giftcard-image" src="' + getFavObj[k].brandSrc + '">';
            generateHTML += '<h3 class="giftcard-title">' + getFavObj[k].brandName + '</h3>'
            generateHTML += '<div class="gift-switch-wrapper"><div class="gift-switch"><span class="card-range">' + getFavObj[k].range + ' </span></div></div>';
            //$('#favItems .fav-result').hide();
            $('#favItems').prepend('<div class="col-md-3 col-sm-6 col-xs-6 ml-2 fav-giftcard giftcard" ref-id="' + getFavObj[k].id + '" id="fav-' + getFavObj[k].id + '">' + generateHTML +' </div>');
        });

    }
    $('.fav-container').slideToggle( "slow" );
});

$(document).on('click', '.fav-giftcard .giftcard-image, .fav-giftcard .giftcard-title', function(e) {
    var getURL = window.location.href;
    var getDomain = getURL.split('#');
    var getID = $(this).parent('.fav-giftcard').attr('id');
    getID = getID.split('_');
    var buildURL = getDomain[0] +'#/buy-gift-card/card-details/' +getID[1];
    window.location = buildURL;
});
    
    $(document).on('click', '.gift-switch .fav-icon', function(e) {
        e.stopPropagation();
        var brandDetails = {};
        var favObj = {};
        var getParentID = $(this).parents('.giftcard').attr('parent-id');
        var getBrandID = $(this).parents('.giftcard').attr('brand-id');
        var getTitle = 'subcard_' + getParentID + '_' + getBrandID;
        var getHTML = $(this).parents('.giftcard').html();
        var getSrc = $(this).parents('.giftcard').find('.giftcard-image').attr('src');
        var getBrandName= $(this).parents('.giftcard').find('.giftcard-title').text();
        var getRange = $(this).parents('.giftcard').find('.card-range').text();
        var getObj = $(this).parents('.objData').val();
        brandDetails.id = getTitle;
        brandDetails.brandSrc = getSrc;
        brandDetails.brandName = getBrandName;
        brandDetails.range = getRange;
        if (localStorage.getItem("myfavData") !== null) {
            favObj = JSON.parse(localStorage.getItem('myfavData'));
        }
        // if (document.cookie.indexOf('myfavData') != -1) {
        //     favObj = JSON.parse(getCookie("myfavData"));
        // }
        favObj[getTitle] = brandDetails;
        var favData = JSON.stringify(favObj);
        if($(this).parent('.gift-switch').hasClass('active')) {
            $(this).parent('.gift-switch').removeClass('active');
            delete favObj[getTitle];
            favData = JSON.stringify(favObj);
            if (document.cookie.indexOf('myFav') != -1) {
                var getCookieValue = getCookie("myFav");
                var isCheck = getCookieValue.indexOf(getTitle) !== -1;
                if(isCheck) {
                    var removeTitle = getCookieValue.replace(getTitle, '');
                    setCookie("myFav", removeTitle, 365);
                    localStorage.setItem('myfavItems', removeTitle);
                    $('#fav'+getTitle).remove();
                    localStorage.setItem('myfavData', favData);
                    //setCookie("myfavData", favData, 365);
                }
                $('#fav-'+ getTitle).remove();
            }
        } else {
            $(this).parent('.gift-switch').addClass('active');
            if (document.cookie.indexOf('myFav') == -1) {
                setCookie("myFav", getTitle, 365);
                localStorage.setItem('myfavItems', getTitle);
                localStorage.setItem('myfavData', favData);
                //setCookie("myfavData", favData, 365);
            } else {
                var getCookieValue = getCookie("myFav");
                var isCheck = getCookieValue.indexOf(getTitle) === -1;
                if(isCheck) {
                    var addTitle = getCookieValue + ', ' + getTitle;
                    setCookie("myFav", addTitle, 365);
                    localStorage.setItem('myfavItems', addTitle);
                    localStorage.setItem('myfavData', favData);
                   // setCookie("myfavData", favData, 365);
                }
            }
            //$('#favItems .fav-result').hide();
            $('#favItems').prepend('<div class="col-md-3 col-sm-6 col-xs-6 ml-2 fav-giftcard giftcard" ref-id="' + getTitle + '" id="fav-' + getTitle + '"><span class="close-card-details">x</span>' + getHTML +' </div>');
               
        }
    });

    
    $(document).on('click', '.search-icon', function(e) {
        if($('.searchText').val() !== '')
            {
                window.location = "https://www.purvice.com/purvice1/deals?frmSearchParam="+$('.searchText').val();
            }
    });

    $(document).on('keyup', '.searchText', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode == 13){
            if($('.searchText').val() !== '')
            {
                window.location = "https://www.purvice.com/purvice1/deals?frmSearchParam="+$('.searchText').val();
            }
            //do something
        }
    });

    $(document).on('click', '.fav-giftcard .close-card-details', function(e) {
        var getTitle = $(this).parent('.giftcard').attr('ref-id');
        var getCookieValue = getCookie("myFav");
                var isCheck = getCookieValue.indexOf(getTitle) !== -1;
                if(isCheck) {
                    var removeTitle = getCookieValue.replace(getTitle, '');
                    setCookie("myFav", removeTitle, 365);
                    localStorage.setItem('myfavItems', removeTitle);
                }
                if (localStorage.getItem("myfavData") !== null) {
                    var favObj = JSON.parse(localStorage.getItem('myfavData'));
                }
                delete favObj[getTitle];
                var favData = JSON.stringify(favObj);
                $('#fav'+getTitle).remove();
                localStorage.setItem('myfavData', favData);
                //setCookie("myfavData", favData, 365);
                $('.'+getTitle).find('.gift-switch').removeClass('active');
                $(this).parent('.giftcard').remove();
    });

    
     
});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function deleteCookie(name) {
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}