
export const environment = {
  production: false,
  envName: 'dev',
  baseUrl: "https://www.purvice.com/purvice-dev-api/",
  videoUrl: "https://www.youtube.com/embed/1nKl9hVUGkQ?autoplay=1",
  globalOnePayTerminalType: 2,
  globalOnePayTransactionType: 7,
  globalOnePayCurrency: 'USD',
  globalOnePayOrderIdAppend: 'purvice'
};
