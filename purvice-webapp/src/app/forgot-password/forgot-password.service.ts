import { Injectable, } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../services/http.service';
import { environment } from '../../environments/environment';

@Injectable()
export class ForgotPasswordService {

  constructor( private http: Http) { }

   forgotPassword(user) { 
       	let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Access-Control-Allow-Origin', '*');
        
		let options = new RequestOptions({ headers: headers });
             return this.http.post(environment.baseUrl + '/forgot', JSON.stringify(user),options)
			.map((response: Response) => {								
				return response.json();			
			})
	}

}
