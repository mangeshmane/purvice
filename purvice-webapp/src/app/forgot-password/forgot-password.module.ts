import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordService } from './forgot-password.service';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { CommonModule } from '@angular/common';
import { SharedService } from '../services/shared.service';
import { ForgotPasswordRoutingModule } from './forgot-password-routing.module';
import { ForgotPasswordComponent } from './forgot-password.component';
@NgModule({
  imports: [
    ForgotPasswordRoutingModule,
    BsDropdownModule,
    HttpModule,
    FormsModule,
    CommonModule,
    ToasterModule
  ],
  declarations: [ForgotPasswordComponent],
  providers: [
    ForgotPasswordService,
    ToasterService,
    SharedService
  ]
})
export class ForgotPasswordModule { }

