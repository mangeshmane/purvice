import { Component } from '@angular/core';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../services/shared.service';
import { ForgotPasswordService } from './forgot-password.service';

@Component({
  templateUrl: 'forgot-password.component.html'
})
export class ForgotPasswordComponent {
  public model: any = {};
  public username: string;
  public password: string;
  public showLoader: boolean;
  constructor(private ForgotService: ForgotPasswordService, private _router: Router, private toasterService: ToasterService) { }

  passwordForgot(model) {
    if (model.username != "") {
      this.showLoader = true;
      this.ForgotService.forgotPassword(model).subscribe(data => {

        if (data.message == "SUCCESS") {
          this.toasterService.pop('success', '', 'Please check your mail to reset your password');
          (<HTMLInputElement>document.getElementById("username")).value = "";
          (<HTMLInputElement>document.getElementById("resetPassBtn")).disabled = true;
        }
        else {
          this.toasterService.pop('error', data.message, data.explanation);          
        }


        this.showLoader = false;
      });
    }

  }


  gotoHome() {
    console.log("gotoHome..");
    this._router.navigate(['home']);
  }


}