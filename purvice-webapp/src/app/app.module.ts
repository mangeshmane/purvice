import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BlockUIModule } from 'ng-block-ui';
import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PopoverModule } from "ngx-popover";
import { TabsModule } from 'ngx-bootstrap/tabs';
import { Angular2SocialLoginModule } from "angular2-social-login";
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { Router } from "@angular/router";
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app.routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FullLayoutComponent } from './layouts/full-layout.component';
import { AdminLayoutComponent } from './layouts/admin-layout.component';
import { ImageUploadModule } from 'angular2-image-upload';
import { HttpModule, RequestOptions, XHRBackend } from '@angular/http';
import { HttpService } from './services/http.service';
import { AuthGuard } from './services/auth-guard-service';
import { AdminGuard } from './services/admin-guard-service';
import { SharedService } from './services/shared.service';
import { LayoutService } from './layouts/layout.service';
import { CheckoutGuard } from './services/checkout-guard-service';
declare var $: any;

export function httpServiceLoaderFactory(backend: XHRBackend, options: RequestOptions,router:Router)
 {  return new HttpService(backend, options,router)}

@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,    
    AppRoutingModule, 
    HttpModule,               
    BlockUIModule,
    Angular2SocialLoginModule,
    BsDropdownModule.forRoot(),
    PopoverModule,
    TabsModule.forRoot(),    
    ImageUploadModule.forRoot(),
    CommonModule,
    FormsModule,
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent, 
    SIDEBAR_TOGGLE_DIRECTIVES,
    AdminLayoutComponent
    
  ],
  providers: [
  	AuthGuard,
    AdminGuard,
    CheckoutGuard,
  	SharedService,  
    LayoutService,	
  	{provide: HttpService, useFactory: httpServiceLoaderFactory,
      deps: [XHRBackend, RequestOptions,Router] },{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
