import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { AboutusComponent } from './aboutus.component';
import { TermsofserviceComponent } from './terms-of-service.component';
import { FAQComponent } from './faq.component';
import { PrivacyComponent } from './privacy.component';
import { CopyrightComponent } from './copyright.component';
import { BraintreeComponent } from './braintree.component';
import { BuyGyftCardComponent } from './buy-gyft-card-with-credit-card.component';
import { BuyGyftCardWithPaypalComponent } from './buy-gyft-card-with-cpaypal.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      title: 'Home'
    }
  },
  {
    path: 'about-us',
    component: AboutusComponent,

    data: {
      title: 'Aboutus'
    }
  },
  {
    path: 'terms-of-service',
    component: TermsofserviceComponent,

    data: {
      title: 'TermsOfService'
    }
  },
  {
    path: 'faq',
    component: FAQComponent,

    data: {
      title: 'FAQ'
    }
  },
  {
    path: 'privacy',
    component: PrivacyComponent,

    data: {
      title: 'privacy'
    }
  },
  {
    path: 'copyright',
    component: CopyrightComponent,

    data: {
      title: 'copyright'
    }
  },
  {
    path: 'braintree',
    component: BraintreeComponent,

    data: {
      title: 'braintree'
    }
  },
  {
    path: 'buy-gift-cards-with-credit-cards',
    component: BuyGyftCardComponent,

    data: {
      title: 'buyGyftCard'
    }
  },  
  {
    path: 'buy-gyft-card-with-cpaypal',
    component: BuyGyftCardWithPaypalComponent,

    data: {
      title: 'buy-gyft-card-with-cpaypal'
    }
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }

