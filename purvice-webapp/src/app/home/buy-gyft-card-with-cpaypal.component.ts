import { Component} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HomeService } from './home.service';
@Component({
  templateUrl: 'buy-gyft-card-with-cpaypal.component.html'
})

export class BuyGyftCardWithPaypalComponent {
public videoStatus: boolean;
    constructor( private _router: Router,private homeService:HomeService) {
   
  }

  gotoGiftCardPage() {

    this._router.navigate(['../buy-gift-card']);
  }

  UpdateVideoStatus() {
    this.homeService.getVideoStatus().subscribe(data => {
      this.videoStatus = data.result.status;
      console.log(data);
    });
  }

}