import { Injectable, } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../services/http.service';
import { environment } from '../../environments/environment';

@Injectable()
export class HomeService {
    constructor(private http: HttpService) {

    }
    
    getAllRetailters(): Observable<any> {
        return this.http.get(environment.baseUrl+ 'retailters/getAll')
        .map((response: Response) => {
            return response
        })
    }

      getAllCards(): Observable<any> {
        return this.http.get(environment.baseUrl + 'item/search')
            .map((response: Response) => {
                return response.json()
            })
    }
    
    forgotPassword(user) { 
       	let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Access-Control-Allow-Origin', '*');
        
		let options = new RequestOptions({ headers: headers });
             return this.http.post(environment.baseUrl + '/forgot', JSON.stringify(user),options)
			.map((response: Response) => {								
				return response.json();			
			})
	}

       getVideoStatus(): Observable<any> {
        return this.http.get(environment.baseUrl + 'setting/search')
            .map((response: Response) => {
                return response.json()
            })
    }
    
  
}