import { Component} from '@angular/core';
import { Router } from '@angular/router';
@Component({
  templateUrl: 'copyright.component.html'
})

export class CopyrightComponent {

 constructor(private router: Router) { }
 redirectToTerms() {
     this.router.navigate(['./home/terms-of-service']);
  } 
   redirectToPrivacy() {  
     this.router.navigate(['./home/privacy']);
  }
   redirectToCopyright() {  
     this.router.navigate(['./home/copyright']);
  }
   redirectToAboutUs() {  
     this.router.navigate(['./home/about-us']);
  }
  redirectToFAQ() {  
    this.router.navigate(['./home/faq']);
 }
    onChange(page){
   if(page == "privacy")
   {
       this.router.navigate(['./home/privacy']);
   }
   else if(page == "termsofcondition"){
           this.router.navigate(['./home/terms-of-service']);
   }
   else if(page == "faq"){
    this.router.navigate(['./home/faq']);
   }
   else if(page == "about"){
      
          this.router.navigate(['./home/about-us']);
   }
   else{
         this.router.navigate(['./home/copyright']);
   } 
 }
}