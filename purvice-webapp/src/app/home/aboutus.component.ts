import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';
import { environment } from '../../environments/environment';
@Component({
  templateUrl: 'aboutus.component.html'
})

export class AboutusComponent implements OnInit {
  public videoLink: any = environment.videoUrl
  public videoStatus: boolean;

  ngOnInit() {
    this.HomeService.getVideoStatus().subscribe(data => {
      this.videoStatus = data.result.status;
      console.log(data);
    });
  }

  constructor(private HomeService: HomeService) {

  }
}