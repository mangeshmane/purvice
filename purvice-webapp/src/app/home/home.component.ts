import { Component, OnInit, ChangeDetectorRef,NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { environment } from '../../environments/environment';
import { HomeService } from './home.service';
declare var $: any;
import * as _ from "lodash";
var that;
@Component({
  templateUrl: 'home.component.html'
})
export class HomeComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  public id: string = 'qDuKsiwS5xw';
  public retailters: any = [];
  public user: any;
  public cards: any = [];
  public videoLink: any = environment.videoUrl
  public videoStatus:boolean;
  ngOnInit() {
    this.blockUI.start('Loading...');
    var d = new Date();
			var weekday = new Array(7);
			weekday[0] = "assets/img/sunday.jpg";
			weekday[1] = "assets/img/monday.jpg";
			weekday[2] = "assets/img/tuesday.jpg";
			weekday[3] = "assets/img/wednesday.jpg";
			weekday[4] = "assets/img/thursday.jpg";
			weekday[5] = "assets/img/friday.jpg";
			weekday[6] = "assets/img/saturday.jpg";
		
			var n = weekday[d.getDay()];
			document.getElementById("bannerImg").setAttribute("src", n);
    this.getAllCards().then((data) => {


    	var user;    	  
    		 try {
    	        user = JSON.parse(localStorage.getItem('user'));    			
    	    } catch (e) {
    	       user=null;    		
    	    }
    	    
      if (user)
        this.user = user;
      this.zone.run(() => {
        that.blockUI.stop();
        this.cdrf.detectChanges();
      });

    });


this.HomeService.getVideoStatus().subscribe(data => {
                       if(data.result!=null)
                          this.videoStatus = data.result.status;
                        console.log( data );      
               });


  }
  open() {
    $('#promotion-video').attr("src", this.videoLink);
    $('#youTubePlayer').modal('show');
  }
  constructor(private HomeService: HomeService,private _router: Router, private activeRoute: ActivatedRoute, private homeService: HomeService, private cdrf: ChangeDetectorRef,private zone:NgZone) {
    that = this;
    activeRoute.params.subscribe(params => { });
  }

  getAllCards() {
    return new Promise((resolve, reject) => {
      this.homeService.getAllCards().subscribe(data => {
        let unique: any = [];

        _.forEach(data.result, function (card, index) {
          that.extractPrice(card);
        });
        unique = _.uniqBy(data.result, function (e: any) {
          return e.brand.brandName;
        });

        _.forEach(unique, function (card:any, index) {
          let childData = [];
          childData = _.filter(data.result, function (o: any) {
            if (o.brand.brandName == unique[index].brand.brandName)
              return o;
          });

          if (childData.length > 0) {
            card.childData = childData;
            card.childData.sort(that.sort)
          }
        })
        _.forEach(unique, function (card:any, index) {

          if (card.minValue == null) {
            card.minValue = card.childData[0].fixedValue;
            card.maxValue = card.childData[card.childData.length - 1].fixedValue;
          }

        })

        this.cards = unique.slice(0, 8);
        resolve(data.result);
      })
    })
  }
  gotoGiftCardPage() {

    this._router.navigate(['../buy-gift-card'], { relativeTo: this.activeRoute });
  }


  goToDetail(card) {
    this._router.navigate(['../buy-gift-card/card-details', card.id], { relativeTo: this.activeRoute });

  }

  extractPrice(card) {
    if (card.minValue == null) {
      var value = card.rewardName.substr(card.rewardName.indexOf("$") + 1);
      card.rewardName = card.rewardName.substring(0, card.rewardName.indexOf('$'));
      card.fixedValue = parseInt(value);
      return card;
    }
    else
      return card;
  }

  sort(a, b) {

    if (a.fixedValue < b.fixedValue)
      return -1;
    if (a.fixedValue > b.fixedValue)
      return 1;
    return 0;
  }

  redirectToBraintree() {
    this._router.navigate(['./home/braintree']);
  }

}
