import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { HomeComponent } from './home.component';
import { AboutusComponent } from './aboutus.component';
import { TermsofserviceComponent } from './terms-of-service.component';
import { FAQComponent } from './faq.component';
import { PrivacyComponent } from './privacy.component';
import { CopyrightComponent } from './copyright.component';
import { HomeRoutingModule } from './home-routing.module';
import { HomeService } from './home.service';
import { BraintreeComponent } from './braintree.component';
import { BuyGyftCardComponent } from './buy-gyft-card-with-credit-card.component';
import { BuyGyftCardWithPaypalComponent } from './buy-gyft-card-with-cpaypal.component';

@NgModule({
  imports: [
    HomeRoutingModule,
    CommonModule,  HttpModule,
    FormsModule,
    ToasterModule
  ],
  declarations: [HomeComponent , AboutusComponent , TermsofserviceComponent , FAQComponent , PrivacyComponent , CopyrightComponent ,BraintreeComponent , BuyGyftCardComponent , BuyGyftCardWithPaypalComponent ],
   providers: [ HomeService ]
})
export class HomeModule { }
