import { Injectable, } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../services/http.service';
import { environment } from '../../environments/environment';

@Injectable()
export class VerifyService {

  constructor( private http: Http) { }

   verifyToken(token) {     
    return this.http.get(environment.baseUrl + 'verifyEmail/'+token )
      .map((res: Response) => {
        return res.json()
      })

  }

}
