import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { VerifyService } from './verify.service';
@Component({
	templateUrl: 'verify.component.html'
})
export class VerifyComponent implements OnInit {
	public showSuccess: boolean = false;
	public showError: boolean = false;
	public token: any;


	ngOnInit() {

		this.activatedRoute.params.subscribe(params => {
			this.token = params['token'];

			this.verifyToken(this.token).then((data: any) => {
				if (data == "SUCCESS") {
					this.showSuccess = true;
				}
				else {
					this.showError = true;
				}
				this.cdrf.detectChanges();
			})


		});
	}
	constructor(private _router: Router, private activatedRoute: ActivatedRoute, private verifyService: VerifyService, public cdrf: ChangeDetectorRef) {

	}

	verifyToken(token) {
		return new Promise((resolve, reject) => {
			this.verifyService.verifyToken(token).subscribe(data => {
				resolve(data.message);
			})
		})
	}

	goToLoginPage(model) {
		this._router.navigate(['login']);
	}

	goToHome() {
		this._router.navigate(['home']);
	}
}
