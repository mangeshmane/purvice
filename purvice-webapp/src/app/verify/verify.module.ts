import { NgModule ,} from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { VerifyRoutingModule } from './verify-routing.module';
import { VerifyComponent } from './verify.component';
import { VerifyService } from './verify.service';

@NgModule({
  imports: [
    VerifyRoutingModule,        
    HttpModule,
    FormsModule,
    CommonModule   
  ],
  providers:[VerifyService],
  declarations: [VerifyComponent] 
})
export class VerifyModule { }

