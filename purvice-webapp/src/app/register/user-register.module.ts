import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { HttpModule } from '@angular/http';
import { RegisterComponent } from './user-register.component';
import { RegisterRoutingModule } from './user-register-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImageUploadModule } from 'angular2-image-upload';
import { BlockUIModule } from 'ng-block-ui';
import { CommonModule } from '@angular/common';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { RegisterService } from './user-register.service';

@NgModule({
  imports: [
    RegisterRoutingModule,
    ChartsModule,
    BsDropdownModule,
    FormsModule,
    CommonModule,
    ImageUploadModule,
    ToasterModule,
    BlockUIModule,
    HttpModule
  ],
  declarations: [RegisterComponent],
  providers: [
    RegisterService,
    ToasterService
  ],
})
export class RegisterModule { }
