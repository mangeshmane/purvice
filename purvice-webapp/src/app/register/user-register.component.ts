import { Component } from '@angular/core';
import { RegisterService } from './user-register.service';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
var that;
@Component({
	templateUrl: 'user-register.component.html'
})
export class RegisterComponent {
	@BlockUI() blockUI: NgBlockUI;
	public model:any = {};
	public name: string;
	public email: string;
	public password: string;
	public phone: string;
	public showLoader: boolean = false;
	public disableRegister:boolean;
	constructor(private registerService: RegisterService, private toasterService: ToasterService, private _router: Router) {
		that = this;
	}


	ngOnInit(): void {
		var user = JSON.parse(localStorage.getItem('user'));
		var admin = JSON.parse(localStorage.getItem('admin'));
		if (user) {
			this._router.navigate(['home']);
		}
		else if (admin) {
			this._router.navigate(['admin']);
		}
	}

	doRegister(form) {
		this.showLoader = true;
		this.registerService.register(this.model).subscribe(data => {
			this.showLoader = false;
			console.log("REGISTRATION");
			console.log(data);
			if (data.message == "SUCCESS") {				
				console.log("Registration completed successfully");
				this.toasterService.pop('success', data.message, data.explanation);			   

			}
			else {
				//this.model={};
				this.toasterService.pop('error', data.message, data.explanation);
			}
			form.resetForm();
		});
	}

	gotoHome() {
		console.log("gotoHome..");
		this._router.navigate(['home']);
	}

}
