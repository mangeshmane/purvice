import { Injectable, } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../services/http.service';
import { environment } from '../../environments/environment';

@Injectable()
export class ResetPasswordService {

  constructor( private http: Http) { }

     check(token) { 
       
       return this.http.get(environment.baseUrl + 'resetPassword?token=' + token)
			.map((response: Response) => {								
				return response.json();			
			})
        
	}

	updateUser(token,data){
		   
       return this.http.post(environment.baseUrl + 'resetPassword?token=' + token,data)
			.map((response: Response) => {								
				return response.json();			
			})
	
	}

}
