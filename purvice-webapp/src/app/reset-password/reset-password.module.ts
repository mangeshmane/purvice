import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResetPasswordService } from './reset-password.service';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { SharedService } from '../services/shared.service';
import { ResetPasswordRoutingModule } from './reset-password-routing.module';
import { ResetPasswordComponent } from './reset-password.component';
import { CommonModule } from '@angular/common';
@NgModule({
  imports: [
    ResetPasswordRoutingModule,
    BsDropdownModule,
    HttpModule,
    FormsModule,
     CommonModule,
    ToasterModule
  ],
  declarations: [ResetPasswordComponent],
  providers: [
ResetPasswordService,
    ToasterService,
    SharedService
  ]
})
export class ResetPasswordModule { }

