import { Component, OnInit } from '@angular/core';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SharedService } from '../services/shared.service';
import { ResetPasswordService } from './reset-password.service';

@Component({
  templateUrl: 'reset-password.component.html'
})
export class ResetPasswordComponent implements OnInit {
  public model: any = {};
  public rePassword: string;
  public password: string;
  public token: string;
  public tokenStatus: boolean;
  public info: any;
  public passwordStatus: boolean;
  public passwordMatch: boolean = true;
  public showButton: boolean = false;
  public showLoader: boolean = false;

  constructor(private ResetService: ResetPasswordService, private activatedRoute: ActivatedRoute, private _router: Router, private toasterService: ToasterService) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.token = params.token;
      this.ResetService.check(this.token).subscribe(data => {
        this.info = data.result;
        if (data.explanation == "valid token") {

          document.getElementById("reset-password-window").style.display = "inline";

        } else {

          document.getElementById("warning-msg-parent").style.display = "inline";
        }

      });

    });

  }

  passwordForgot(model) {

    this.info.password = model.newpassword;
    this.showLoader = true;
    this.ResetService.updateUser(this.token, this.info).subscribe(data => {
      this.showLoader = false;
      if (data.message == "SUCCESS") {
        this.toasterService.pop('success', '', 'Your Password changed successfully');
        (<HTMLInputElement>document.getElementById("newpassword")).value = "";
        (<HTMLInputElement>document.getElementById("repeatepassword")).value = "";


        this._router.navigate(['./login']);

      }

    });


  }

  myMethod(model) {
    if (model.newpassword !== model.repeatepassword) {
      this.showButton = false
      this.passwordMatch = false
    }


    if (model.newpassword == model.repeatepassword) {
      this.passwordMatch = true
      this.showButton = true
    }


  }

  gotoHome() {
    this._router.navigate(['home']);
  }

}