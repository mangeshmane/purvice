import { Injectable, } from '@angular/core';
import { Http, Response, } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../../services/http.service';
import { environment } from '../../../environments/environment';
import { RequestOptions, Headers } from '@angular/http';
@Injectable()
export class CategoriesService {
    constructor(private http: HttpService) {

    }

    getAllCategories(): Observable<any> {
        return this.http.get(environment.baseUrl + 'category/all')
            .map((response: Response) => {
                return response.json();
            })
    }
    addCategory(data): Observable<any> {
        return this.http.post(environment.baseUrl + 'category/create', data)
            .map((response: Response) => {                
                return response.json();
            })
    }
    updateCategory(data): Observable<any> {
        return this.http.put(environment.baseUrl + 'category/update', data)
            .map((response: Response) => {
                return response.json();
            })
    }
    deleteCategory(id): Observable<any> {
        return this.http.delete(environment.baseUrl + 'category/' + id)
            .map((response: Response) => {
                return response.json();
            })
    }

    getAllItems(): Observable<any> {
        return this.http.get(environment.baseUrl + 'item/search')
            .map((response: Response) => {
                return response.json()
            })
    }

    assignItems(ids, object): Observable<any> {
        return this.http.put(environment.baseUrl + 'item/assignCategory?itemId=' + ids, object)
            .map((response: Response) => {
                return response.json()
            })
    }

    getItemsByCategory(id): Observable<any> {
        return this.http.get(environment.baseUrl + 'item/search?categoryId=' + id)
            .map((response: Response) => {
                return response.json()
            })
    }

    uploadImage(image) {
        let token = localStorage.getItem('authorization');
        let formData: FormData = new FormData();
        formData.append('file', image);
        let headers = new Headers()
        let options = new RequestOptions({ headers: headers });
        options.headers.set('Authorization', 'Bearer ' + token);
        let apiUrl1 = environment.baseUrl + "/upload";

        return this.http.post(apiUrl1, formData, options)
            .map((response: Response) => {
                return response.json()
            })
    }
}