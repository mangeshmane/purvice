import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { DataTableDirective } from 'angular-datatables';
import { CategoriesService } from './categories.service';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import * as _ from "lodash";
declare var $: any;
@Component({
  templateUrl: 'categories.component.html'
})
export class CategoryComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild(DataTableDirective)
  dtElement: any = DataTableDirective;
  dtTrigger: any = new Subject();
  constructor(private categoriesService: CategoriesService, private toasterService: ToasterService, private http: Http, private cdrf: ChangeDetectorRef) { }
  dtOptions: DataTables.Settings = {};
  public model: any = {};
  public categories: any = []
  public items: any = []
  public selectedCategory: any;
  public categoryAction: string;
  public filteredItems: any = [];
  public categoryItems: any = [];
  public itemsbycategory: any = [];
  public defaultItems: any = [];
  public uploaded: any = [];
  public uploadreult: any = [];
  public showData: boolean = false;
  public fileUploaded: boolean = false;
  public fileStatus: boolean = false;
  public showTable: boolean = false;
  public categoryTable: any;
  public categoryForm: any;
  public fontawesomeIcons = require('fontawesome-icons-list');
  public icons = require('fontawesome-icons-list');
  public iconList: any;
  public iconSearch: any;
  public setAll: boolean = false;

  ngOnInit(): void {
    this.iconList = this.fontawesomeIcons;
    this.blockUI.start('Loading...');
    this.getAllCategories().then((data) => {
      this.blockUI.stop();
    });
    this.getAllItems();

  }
  addCategory() {
    this.model = {};
    if (this.categoryForm) {
      this.categoryForm.resetForm();

    }
    $('#categoryModal').modal('show');
  }

  updateCategory(data) {
    this.model = _.cloneDeep(data);
    $('#categoryModal').modal('show');
  }

  assignCategory(data) {
    // this.items=_.cloneDeep(this.defaultItems);
    this.selectedCategory = data;
    this.setAll = false;

    //  $('#items').scrollTop(0);



    this.getItemsByCategory(data.id).then(
      (data: any) => {
        if (data) {         
          this.categoryItems = data;
          this.items = _.cloneDeep(this.defaultItems);
          for (let i = 0; i < this.items.length; i++) {
            for (let k = 0; k < this.categoryItems.length; k++) {
              if (this.items[i].id == this.categoryItems[k].id)
                this.items[i].checked = true;             
            }
          }

          console.log(data);
          $('#itemsModal').modal('show')

          this.setAll = data.length === this.defaultItems.length ? true : false

        }
        else {
          this.items = _.cloneDeep(this.defaultItems);
          $('#itemsModal').modal('show')
        }



        setTimeout(() => {         
          $('html, body').animate({ scrollTop: $('#itemsModal').offset().top }, 'slow');
          this.cdrf.detectChanges();
        }, 2000);


      })

  }

  saveItems() {
    let ids = "";
    this.filteredItems = this.items.filter(
      item => item.checked === true);
    for (let i = 0; i < this.filteredItems.length; i++) {
      if (i == this.filteredItems.length - 1)
        ids += this.filteredItems[i].id;
      else
        ids += this.filteredItems[i].id + ",";
    }
    this.categoriesService.assignItems(ids, this.selectedCategory).subscribe(data => {
      if (data.message == "SUCCESS") {
        this.toasterService.pop('success', data.message, "Items Assigned Successfully");
      }
      else {
        this.model = {};
        this.toasterService.pop('error', data.message, data.explanation);
      }
    })

  }

  getAllCategories() {
    return new Promise((resolve, reject) => {
      this.categories = [];
      if (this.categoryTable) {
        this.categoryTable.destroy();
      }
      this.categoriesService.getAllCategories().subscribe(data => {
        this.categories = data.result;
        this.showTable = true;
        this.cdrf.detectChanges();
        this.categoryTable = $('#categoryTable').DataTable({
          responsive: false,
          "pagingType": "full_numbers",
        });
        this.cdrf.detectChanges();
        console.log("Categories===============" + this.categories);
        resolve(data.result);
      })
    })
  }

  getAllItems() {
    this.categoriesService.getAllItems().subscribe(data => {
      this.items = data.result;
      this.defaultItems = _.cloneDeep(data.result);
    })
  }

  getItemsByCategory(id) {
    return new Promise((resolve, reject) => {
      this.categoriesService.getItemsByCategory(id).subscribe(
        (response) => {
          resolve(response.result);
        });
    })
  }


  submit(form) {
    this.categoryForm = form;
    if (this.categoryAction == "Add") {
      this.categoriesService.addCategory(this.model).subscribe(data => {
        if (data.message == "SUCCESS") {
          this.toasterService.pop('success', data.message, data.explanation);
          this.getAllCategories();
        }
        else {
          this.model = {};
          this.toasterService.pop('error', data.message, data.explanation);
        }
      })
    }
    else if (this.categoryAction == "Update") {

      this.categoriesService.updateCategory(this.model).subscribe(data => {
        console.log("Success message=======================", data.message);
        if (data.message == "SUCCESS") {
          this.toasterService.pop('success', data.message, data.explanation);
          this.getAllCategories();
        }
        else {
          this.model = {};
          this.toasterService.pop('error', data.message, data.explanation);
        }
      })
    }
    $('#categoryModal').modal('hide');
    this.categoryForm.resetForm();
  }


  deleteCategory(data) {
    this.categoriesService.deleteCategory(data.id).subscribe(data => {
      console.log("Success message=======================", data.message);
      if (data.message == "SUCCESS") {
        this.toasterService.pop('success', data.message, data.explanation);
        this.getAllCategories();
      }
      else {
        this.model = {};
        this.toasterService.pop('error', data.message, data.explanation);
      }
    })
  }


  fileChange(event) {

    let fileList: FileList = event.target.files;
    this.uploaded = fileList[0];
    if (this.uploaded) {
      this.fileStatus = true;
      this.cdrf.detectChanges();
    }
  }

  rerender(): void {

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  getAmt(name) {
    var amt = name.substr(name.indexOf("$") + 1);
    return amt;
  }

  removeCategory(category) {
    this.selectedCategory = category;
    $('#removeCategoryModal').modal('show')
  }
  showIcons() {
    this.iconSearch = null;
    this.iconList = this.icons;
    $('#iconModal').modal('show')

  }
  selectedIcon(icon) {

    this.model.categoryImage = icon;
    $('#iconModal').modal('hide');
  }

  close(form) {
    this.model = {};
    this.categoryForm = form;
    this.categoryForm.resetForm();
  }

  filterIcon(icon) {

    if (icon.length > 0) {
      let result = _.filter(this.icons, function (item: any) {
        return item.indexOf(icon) > -1;
      });
      this.iconList = result;
    }
    else {
      this.iconList = this.icons;
    }
  }

  setAllItems() {
    this.setAll = !this.setAll;
    if (this.setAll == true) {
      this.items.forEach(function (item) { item.checked = true; });
    }
    else {
      this.items.forEach(function (item) { item.checked = false; });
    }

    this.cdrf.detectChanges();
  }
}
