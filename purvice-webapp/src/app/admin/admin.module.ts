import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { CommonModule } from '@angular/common';
import { BlockUIModule } from 'ng-block-ui';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CategoryComponent } from './categories/categories.component';
import { DealCategoryComponent } from './deal-categories/deal-categories.component';
import { CategoriesService } from './categories/categories.service';
import { DealCategoriesService } from './deal-categories/deal-categories.service';
import { UserComponent } from './users/users.component';
import { OrdersComponent } from './orders/orders.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { UsersService } from './users/users.service';
import { OrdersService } from './orders/orders.service';
import { AdminProfileService } from './admin-profile/admin-profile.service';
import { Http, HttpModule, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { AdminRoutingModule } from './admin-routing.module';
import { HttpService } from '../services/http.service';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { Router } from "@angular/router";
import {TreeTableModule,TreeNode,SharedModule} from 'primeng/primeng';
import { PaginatorModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';

export function httpServiceLoaderFactory(backend: XHRBackend, options: RequestOptions,router:Router)
{ return new HttpService(backend, options,router) }
@NgModule({
  imports: [
    AdminRoutingModule,
    DataTablesModule,
    FormsModule,
    CommonModule,
    ToasterModule,
    BlockUIModule,
    HttpModule,
    TreeTableModule,
    SharedModule,
    PaginatorModule,
    DropdownModule
  ],
  declarations: [DashboardComponent, CategoryComponent, DealCategoryComponent, UserComponent , OrdersComponent , AdminProfileComponent],
  providers: [{
    provide: HttpService, useFactory: httpServiceLoaderFactory,
    deps: [XHRBackend, RequestOptions,Router]
  },
    ToasterService,
  {
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }, CategoriesService, UsersService , OrdersService ,AdminProfileService, DealCategoriesService ]
})
export class AdminModule { }
