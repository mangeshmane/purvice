import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { UsersService } from './users.service';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import * as _ from "lodash";
declare var $: any;
var that;
@Component({
  templateUrl: 'users.component.html'
})
export class UserComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  public model: any = {};
  public user: any = {};
  public users: any = []
  public userAction: string;
  public showTable: boolean = false;
  public userTable: any;
  public userForm: any;
  public userData = {
    "firstName": "",
    "lastName": "",
    "username": "",
    "password": "",
    "roles": []
  };
  public instance: any;
  public roles: any = [{ name: "USER", value: "USER" }, { name: "ADMIN", value: "ADMIN" }];

  constructor(private usersService: UsersService, private toasterService: ToasterService, private http: Http, public cdrf: ChangeDetectorRef) {
    that = this;
    

  }
  ngOnInit(): void {

    this.getAllUsers().then((data) => {
    });
  }

  addUser() {
    if (this.userForm) {
      this.userForm.resetForm();

    }
    this.model.roles = "ADMIN";
    this.instance = $('#userAddModal').modal('show');
  }

  updateUser(data) {
    this.model = _.cloneDeep(data);
    this.instance = $('#userAddModal').modal('show');
  }

  getAllUsers() {

    return new Promise((resolve, reject) => {
      this.blockUI.start('Loading...');
      this.users = [];
      if (this.userTable) {
        this.userTable.destroy();
      }
      this.usersService.getAllUsers().subscribe(data => {
        this.users = data.result;
        this.showTable = true;
        this.cdrf.detectChanges();
        this.userTable = $('#userTable').DataTable({
          responsive: false,
          "pagingType": "full_numbers",
        });
        this.cdrf.detectChanges();
        this.blockUI.stop();
        resolve(data.result);
      })
    })
  }

  submit(form) {
   
    this.userForm = form;
    this.userData.password = this.model.firstName;
    this.userData.roles[0] = this.model.roles instanceof Array?this.model.roles[0]:this.model.roles;
    this.userData.firstName = this.model.firstName;
    this.userData.lastName = this.model.lastName;
    this.userData.username = this.model.username;

    if (this.userAction == "Add") {
      this.usersService.registerUser(this.userData).subscribe(data => {
        if (data.message == "SUCCESS") {
          that.userForm.resetForm();
          that.toasterService.pop('success', data.message, data.explanation);
          that.getAllUsers();
        }
        else {
          that.userForm.resetForm();
          that.toasterService.pop('error', data.message, data.explanation);
        }
      })
    } else if (this.userAction == "Update") {
      this.usersService.updateUser(this.userData).subscribe(data => {
        if (data.message == "SUCCESS") {
          that.userForm.resetForm();
          that.toasterService.pop('success', data.message, data.explanation);
          that.getAllUsers();
        }
        else {
          that.userForm.resetForm();
          that.toasterService.pop('error', data.message, data.explanation);
        }
      })
    }
  }

  close(form) {
    this.model = {};
    this.userForm = form;
    this.userForm.resetForm();
  }

  getUser(data) {
    this.usersService.getUser(data.id).subscribe(data => {
      this.getAllUsers();
    })

  }





}
