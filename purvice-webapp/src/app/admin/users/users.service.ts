import { Injectable, } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../../services/http.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class UsersService {
    constructor(private http: HttpService) {

    }

    getAllUsers(): Observable<any> {
        return this.http.get(environment.baseUrl + 'user/all')
            .map((response: Response) => {
                return response.json();
            })
    }
    getUser(id): Observable<any> {
        return this.http.get(environment.baseUrl + 'user/' + id)
            .map((response: Response) => {
                return response.json();
            })
    }
    updateUser(data): Observable<any> {
        return this.http.put(environment.baseUrl + 'user/update', data)
            .map((response: Response) => {
                return response.json();
            })
    }
    deleteUser(id): Observable<any> {
        return this.http.delete(environment.baseUrl + 'user/' + id)
            .map((response: Response) => {
                return response.json();
            })
    }

    registerUser(data): Observable<any> {
        return this.http.post(environment.baseUrl + 'user/register', data)
            .map((response: Response) => {
                return response.json();
            })
    }

}