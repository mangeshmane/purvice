import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { TreeNode } from 'primeng/primeng';
import { DealCategoriesService } from './deal-categories.service';
import { SelectItem } from 'primeng/primeng';

import * as _ from "lodash";
declare var $: any;

@Component({
  templateUrl: 'deal-categories.component.html'
})
export class DealCategoryComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  public dealCategoryData: TreeNode[];
  public dealCategoryAction: string;
  public dealCategoryForm: any;
  public dealCategorymodel: any = {};
  public iconSearch: any;
  public iconList: any;
  public icons = require('fontawesome-icons-list');
  public fontawesomeIcons = require('fontawesome-icons-list');
  public selectedDealCategory: any;
  public totalRecords: number;
  public rowsPerPageOptions:any = [10,20,30];
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public queryParams: any={};
  public catParentId: number = 1;
  public csvfileName: string;
  public isImport: boolean = false;
  categoryMenu: SelectItem[];

  constructor(private dealCategoriesService: DealCategoriesService, private toasterService: ToasterService, private http: Http, private cdrf: ChangeDetectorRef) { 
    this.categoryMenu = [
      {label: 'Select Action', value: null},
      {label: 'Import Deal Category', value: 'Import Deal Category'}
  ];
  }


  ngOnInit(): void {
    this.queryParamsObject();
    this.iconList = this.fontawesomeIcons;
    this.blockUI.start('Loading...');
    this.getAllDealCategories(this.queryParams);
    this.blockUI.stop();
  }
  
  nodeExpand(event) {
    
  if (event.node) {
      this.blockUI.start();
       this.getDealCategoriesByParentId(event.node.data.id).then((data) => {
            event.node.children = this.prepareTreeTablesChild(data);
            this.blockUI.stop();
        });
    }
}

addDealCategory() {
  
  this.dealCategorymodel = {};
  if (this.dealCategoryForm) {
    this.dealCategoryForm.resetForm();
  }
  $('#dealCategoryModal').modal('show');
}
close(form) {
  this.dealCategorymodel = {};
  this.dealCategoryForm = form;
  this.dealCategoryForm.resetForm();
}

submit(form) {
  
  this.dealCategoryForm = form;
  if (this.dealCategoryAction == "Add Deal Category") {
    this.dealCategoriesService.addDealCategory(this.dealCategorymodel).subscribe(data => {
      if (data.message == "SUCCESS") {
        this.toasterService.pop('success', data.message, data.explanation);
        this.getAllDealCategories(this.queryParams);
      }
      else {
        this.dealCategorymodel = {};
        this.toasterService.pop('error', data.message, data.explanation);
      }
    })
  }else if (this.dealCategoryAction == "Add Deal Sub Category"){
    this.dealCategoriesService.addDealSubCategory(this.dealCategorymodel).subscribe(data => {
      if (data.message == "SUCCESS") {
        this.toasterService.pop('success', data.message, data.explanation);
        this.getAllDealCategories(this.queryParams);
      }
      else {
        this.dealCategorymodel = {};
        this.toasterService.pop('error', data.message, data.explanation);
      }
    })
  }else if (this.dealCategoryAction == "Edit Deal Category") {
    this.dealCategoriesService.updateDealCategory(this.dealCategorymodel).subscribe(data => {
      if (data.message == "SUCCESS") {
        this.toasterService.pop('success', data.message, data.explanation);
        this.getAllDealCategories(this.queryParams);
      }
      else {
        this.dealCategorymodel = {};
        this.toasterService.pop('error', data.message, data.explanation);
      }
    })
  }  
  $('#dealCategoryModal').modal('hide');
  this.dealCategoryForm.resetForm();
}
showIcons() {
  
  this.iconSearch = null;
  this.iconList = this.icons;
  $('#dealiconModal').modal('show')

}
filterIcon(icon) {
  
  if (icon.length > 0) {
    let result = _.filter(this.icons, function (item: any) {
      return item.indexOf(icon) > -1;
    });
    this.iconList = result;
  }
  else {
    this.iconList = this.icons;
  }
}
selectedIcon(icon) {

  this.dealCategorymodel.categoryImage = icon;
  $('#dealiconModal').modal('hide');
}
getAllDealCategories(queryParams) {
  return new Promise((resolve, reject) => {
    this.dealCategoryData = [];
   this.dealCategoriesService.getAllDealCategories(queryParams).subscribe(data => {
      this.totalRecords = data.tolalRecord;
      this.dealCategoryData = this.prepareTreeTablesRoot(data.result);
      this.cdrf.detectChanges();
      resolve(data.result);
    })
  })  
}

prepareTreeTablesRoot(dealCategory): any {
  var result = [];
  for (var i = 0; i < dealCategory.length; i++) {
      var data = {};
      dealCategory[i].disabled = true;
      data["data"] = dealCategory[i];
      data["leaf"] = false;
     // data["type"] = 'Category';
      result.push(data);
  }
  return result;
}
prepareTreeTablesChild(dealCategory): any {
  
  var result = [];
  for (var i = 0; i < dealCategory.length; i++) {
      var data = {};
      dealCategory[i].disabled = true;
      dealCategory[i].name = dealCategory[i].categoryName;

      data["data"] = dealCategory[i];
      data["leaf"] = false;
      result.push(data);
  }
  return result;
}
  getDealCategoriesByParentId(parentId): any {
    
    return new Promise((resolve, reject) => {
        this.dealCategoriesService.getDealCategoriesByParentId(parentId)
            .subscribe(
                (data) => {
                  resolve(data.result);
                });
    })
}
  deleteDealCategory(dealCategoryId) {
    
    return new Promise((resolve, reject) => {
      this.dealCategoriesService.deleteDealCategory(dealCategoryId).subscribe(
        (response) => {
          if (response.message == "SUCCESS") {
            this.toasterService.pop('success', response.message, response.explanation);
            this.getAllDealCategories(this.queryParams);
          }
          else {
            this.dealCategorymodel = {};
            this.toasterService.pop('error', response.message, response.explanation);
          }
        });
    })
  }
  addSubCategory(data){
    
    this.dealCategorymodel = {};
    if (this.dealCategoryForm) {
      this.dealCategoryForm.resetForm();
    }
    this.dealCategorymodel.parentId = data.data.id;
    $('#dealCategoryModal').modal('show');
  }
  updateDealCategory(data) {
    
    this.dealCategorymodel = _.cloneDeep(data.data);
    $('#dealCategoryModal').modal('show');
  }
  deleteCategory(data){
    
    this.selectedDealCategory = data.data.id;
    $('#removeDealCategoryModal').modal('show');
  }
  paginate(event){
    this.pageNumber = event.page + 1;
    this.pageSize = event.rows;
    this.queryParamsObject();
    this.getAllDealCategories(this.queryParams);
}
queryParamsObject(){
    this.queryParams.pageNumber = this.pageNumber; 
    this.queryParams.pageSize = this.pageSize; 
    this.queryParams.parentId = this.catParentId;
}
uploadCSVFile(formData, options): any {
  debugger
  return new Promise((resolve, reject) => {
    this.dealCategoriesService.importCSVFile(formData, options)
      .subscribe(
      (data) => {
        resolve(data._body);
      });

  })
}
importCSVFileDataService(fileName): any {
  debugger
  return new Promise((resolve, reject) => {
    this.dealCategoriesService.importCSVFileData(fileName)
      .subscribe(
      (data) => {
        resolve(data);
      });

  })
}

uploadFile(event) { 
  debugger  
  let fileList: FileList = event.target.files;
  if (fileList.length > 0) {
      let file: File = fileList[0];
    let formData: FormData = new FormData();
    formData.append('uploadFile', file, file.name);
    let fileName = file.name;
    this.csvfileName = fileName;
    this.uploadCSVFile(formData, fileName).then((response) => {
      let data = JSON.parse(response);
      if(data.message == "SUCCESS"){
        this.toasterService.pop('success', data.message, data.explanation);
        this.isImport = true;
      }else{
        this.toasterService.pop('error', data.message, data.explanation);
        this.isImport = false;
      }
    });
  }
}
importCSVFileData(fileName){
debugger
this.blockUI.start('Loading...');
this.importCSVFileDataService(fileName).then((response) => {
  if(response.message == "SUCCESS"){
    this.toasterService.pop('success', response.message, response.explanation);
  }else{
    this.toasterService.pop('error', response.message, response.explanation);
  }
  this.blockUI.stop();
});
}
importDealCategoryByCSV(){
  $('#CSVModal').modal('show');
}
changeEvent(event){
  if( event.value =="Import Deal Category"){
    this.importDealCategoryByCSV();
  }
}
}
