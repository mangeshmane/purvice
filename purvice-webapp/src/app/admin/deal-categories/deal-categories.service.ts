import { Injectable, } from '@angular/core';
import { Http, Response, } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../../services/http.service';
import { environment } from '../../../environments/environment';
import { RequestOptions, Headers } from '@angular/http';

@Injectable()
export class DealCategoriesService {
    constructor(private http: HttpService) {
    }

    addDealCategory(data): Observable<any> {
        
        return this.http.post(environment.baseUrl + 'dealcategory/create', data)
            .map((response: Response) => {                
                return response.json();
            })
    }
   getAllDealCategories(data): Observable<any> {
        return this.http.get(environment.baseUrl + 'dealcategory/getAllDealCategory?' + this.prepareQueryParams(data))
            .map((response: Response) => {
                return response.json();
            })
    }
    
	prepareQueryParams(data): any {
		var finalQuery = '';
		for (let key in data) {
			if (Array.isArray(data[key])) {
				let innerMap = data[key];
				for (let key2 in innerMap) {
					finalQuery += '&' + key + '=' + innerMap[key2];
				}
			} else {
				finalQuery += '&' + key + '=' + data[key];
			}
		}
		return finalQuery;
    }
    
    getDealCategoriesByParentId(parentId): Observable<any> {
        return this.http.get(environment.baseUrl + 'dealcategory/getDealCategoryByParentId?parentId=' + parentId)
            .map((response: Response) => {
                return response.json();
            })
    }
    updateDealCategory(dealCategory): Observable<any> {
        return this.http.put(environment.baseUrl + 'dealcategory/updateDealCategory', dealCategory)
            .map((response: Response) => {
                return response.json()
            })
    }
    deleteDealCategory(dealCategoryId): Observable<any> {
        return this.http.delete(environment.baseUrl + 'dealcategory/delete?dealCategoryId=' + dealCategoryId)
            .map((response: Response) => {
                return response.json()
            })
    }
    addDealSubCategory(dealCategory): Observable<any> {
        return this.http.post(environment.baseUrl + 'dealcategory/createSubCategory', dealCategory)
            .map((response: Response) => {                
                return response.json();
            })
    }
    importCSVFile(formData,options): Observable<any> {
        debugger
        return this.http.post(environment.baseUrl+ 'dealcategory/importCSVFile', formData, options)
            .map((response: Response) => {
                return response
            })
    }
    importCSVFileData(fileName): Observable<any> {
        debugger
        return this.http.get(environment.baseUrl + 'dealcategory/importCSVFileData?fileName=' + fileName)
            .map((response: Response) => {
                return response.json();
            })
    }
}