import { Component, OnInit, ChangeDetectorRef ,Injector} from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { AdminLayoutComponent } from "../../layouts/admin-layout.component";

import * as _ from "lodash";
declare var $: any;
var that;
import { AdminProfileService } from './admin-profile.service';


@Component({
  templateUrl: 'admin-profile.component.html'
})
export class AdminProfileComponent implements OnInit {
  public id: number;
  public userNameChange: string;
  public passInfo: any = {};
  public model: any = {};
  public firstNameStatus: boolean = true;
  public lastNameStatus: boolean = true;
  public currentPasswordStatus: boolean = false;
  public newPassStatus: boolean = false;
  public confirmPassStatus: boolean = false;
  public curentpasswarning: boolean = false;
  public newpasswarning: boolean = false;
  public confirmPassStore;
  public passwordMatch: boolean = true;
  public newPass: boolean = false;
  public confirmPass: boolean = false;
  public parentComponent:any;
  constructor(private AdminProfileService: AdminProfileService, private route: ActivatedRoute, private http: Http, private toasterService: ToasterService, private inj: Injector) {      
    this.parentComponent = this.inj.get(AdminLayoutComponent);
    console.log(this.parentComponent);
  }

  ngOnInit() {

    this.route
      .queryParams
      .subscribe(params => {
        this.id = params['userId'];

        this.AdminProfileService.getUser(this.id).subscribe(data => {
          console.log("inSideOnInit");
          console.log(data);
          this.model = data.result;
          this.userNameChange = this.model.username;

        })

      });

    document.getElementById("profile").style.opacity = "1";
    $('#confirmPassword').bind("cut copy paste", function (e) {
      e.preventDefault();
    });

  }

  doLogin(user) {

    this.AdminProfileService.updateUser(user).subscribe(dataresult => {
      if (dataresult.message == "SUCCESS") {
        this.parentComponent.updateAdmin(dataresult.result);
        this.toasterService.pop('success', '', 'Profile updated successfully');
      }

    })
  }


  changePass(oldPassword, newPass) {
    this.passInfo.username = this.userNameChange;
    this.passInfo.oldPassword = oldPassword;
    this.passInfo.newPassword = newPass;



    this.AdminProfileService.updatePassword(this.passInfo).subscribe(RESULT => {
      if (RESULT.message == "SUCCESS") {
        this.toasterService.pop('success', '', 'Password updated successfully');
      }
      if (RESULT.message == "FAIL") {
        this.toasterService.pop('error', '', 'Password not matched ');
      }
    })
  }

  firstnameStatus(event: any) {
    if (event.target.value.length == 0) {
      this.firstNameStatus = false;
    }
    else {
      this.firstNameStatus = true;
    }
  }

  lastnameStatus(event: any) {
    if (event.target.value.length == 0) {
      this.lastNameStatus = false;
    }
    else {
      this.lastNameStatus = true;
    }
  }



  curentpassStatus(event: any) {

    if (event.target.value.length == 0) {
      this.currentPasswordStatus = false;
      this.curentpasswarning = true;
    }
    else {
      this.currentPasswordStatus = true;
      this.curentpasswarning = false;
    }
  }

  newpassStatus(event: any) {

    if (event.target.value.length == 0) {
      this.newPassStatus = false;
      this.newpasswarning = true;
    }
    else {

      this.newPassStatus = true;
      this.newpasswarning = false;
      this.newPass = event.target.value;
    }


    if (this.confirmPassStore != undefined) {
      if (this.newPass == this.confirmPassStore) {
        this.passwordMatch = true;
      }
      else {
        this.passwordMatch = false;
      }
    }


  }


  confirmpassStatus(event: any) {

    if (event.target.value.length == 0) {
      this.confirmPassStatus = false;

    }
    else {
      this.confirmPassStatus = true;

      this.confirmPassStore = event.target.value;
    }


    if (this.newPass == this.confirmPassStore) {
      this.passwordMatch = true;
    }
    else {
      this.passwordMatch = false;
    }
  }



} 