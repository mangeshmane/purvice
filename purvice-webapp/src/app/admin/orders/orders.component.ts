import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { OrdersService } from './orders.service';
import * as _ from "lodash";
declare var $: any;
var that;

@Component({
  templateUrl: 'orders.component.html'
})
export class OrdersComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  public orders: any = {};
  public id: number;
  public showTable: boolean = false;
  public userTable: any;
  public indexId: number;
  public showPopUp: boolean = false;
  
 

  constructor(private OrderService:OrdersService,private route: ActivatedRoute, private http: Http,public cdrf: ChangeDetectorRef) {
   that = this;

}
 
   ngOnInit(): void {
       this.route
      .queryParams
      .subscribe(params => {
          this.id = params['id'];
          });

    this.getAllOrders( this.id ).then((data) => {      
    });
  }



   getAllOrders(id) {

    return new Promise((resolve, reject) => {
      this.blockUI.start('Loading...');
     //this.users = [];
      if (this.userTable) {
        this.userTable.destroy();
      }
      this.OrderService.getOrders(id).subscribe(data => {
        this.orders = data;
        console.log(this.orders);
        this.showTable = true;
        this.cdrf.detectChanges();
        this.userTable = $('#userTable').DataTable({
          responsive: false,
          "pagingType": "full_numbers",
        });
        this.cdrf.detectChanges();
        this.blockUI.stop();
        resolve(data.result);
      })
    })
  }

  showDetails(index){  
   
       this.indexId = index;
       this.showPopUp = true;
       this.cdrf.detectChanges();
      $('#ordarsModal').modal('show');
  }

  convertDate(timestamp){
    var date = new Date(timestamp);
    var date2  =new Date(date).toUTCString();
    var date3 =date2.split(' ').slice(0, 4).join(' ')
    return date3;
  }
}
