import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { CategoryComponent } from './categories/categories.component';
import { UserComponent } from './users/users.component';
import { OrdersComponent } from './orders/orders.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { DealCategoryComponent } from './deal-categories/deal-categories.component';

const routes: Routes = [
  { path: '', redirectTo: 'users', pathMatch: 'full' },
  {
    path: '',
    component: DashboardComponent,
    data: {
      title: 'Dashboard'
    }
  },
  {
    path: 'categories',
    component: CategoryComponent,
    data: {
      title: 'Categories'
    }
  },
  {
    path: 'users',
    component: UserComponent,
    data: {
      title: 'Users'
    }
  },
  {
    path: 'orders',
    component: OrdersComponent,
    data: {
      title: 'orders'
    }
  },
    {
    path: 'admin-profile',
    component: AdminProfileComponent,
    data: {
      title: 'admin profile'
    }
  },{
    path: 'deal-categories',
    component: DealCategoryComponent,
    data: {
      title: 'Deal Categories'
    }
  },
  

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
