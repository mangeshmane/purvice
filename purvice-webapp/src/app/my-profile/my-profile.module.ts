import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { SharedService } from '../services/shared.service';
import { MyProfileRoutingModule } from './my-profile-routing.module';
import { MyProfileComponent } from './my-profile.component';
import { CommonModule } from '@angular/common';
import { MyProfileService } from './my-profile.service';
@NgModule({
  imports: [
    MyProfileRoutingModule,
    BsDropdownModule,
    HttpModule,
    FormsModule,
     CommonModule,
    ToasterModule
  ],
  declarations: [MyProfileComponent],
  providers: [
   MyProfileService,
    ToasterService,
    SharedService
  ]
})
export class MyProfileModule { }

