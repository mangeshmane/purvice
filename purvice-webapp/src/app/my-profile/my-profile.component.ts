import { Component, OnInit, ChangeDetectorRef, Output, EventEmitter, Injector } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { FullLayoutComponent } from "../layouts/full-layout.component";

import * as _ from "lodash";
declare var $: any;
var that;
import { MyProfileService } from './my-profile.service';


@Component({
  templateUrl: 'my-profile.component.html'
})
export class MyProfileComponent implements OnInit {
  public id: number;
  public userNameChange: string;
  public passInfo: any = {};
  public model: any = {};
  public firstNameStatus: boolean = true;
  public lastNameStatus: boolean = true;
  public currentPasswordStatus: boolean = false;
  public newPassStatus: boolean = false;
  public confirmPassStatus: boolean = false;
  public curentpasswarning: boolean = false;
  public newpasswarning: boolean = false;
  public confirmPassStore;

  public passwordMatch: boolean = true;
  public newPass: boolean = false;
  public confirmPass: boolean = false;
  public parentComponent: any;
  public showLoader: boolean = false;
  public showUserLoader: boolean = false;
  public user: any;
  public hideProfile: boolean = false;
  public showEmail: boolean = false;
  @Output() someEvent = new EventEmitter<string>();
  constructor(private _router: Router, private MyProfileService: MyProfileService, private route: ActivatedRoute, private http: Http, private toasterService: ToasterService, private inj: Injector,private cdrf:ChangeDetectorRef) {
    this.parentComponent = this.inj.get(FullLayoutComponent);
    console.log(this.parentComponent);
  }

  ngOnInit() {
    var user = JSON.parse(localStorage.getItem('user'));

    if (user) {     
      this.user = user;
      this.user.roles[0] == "SOCIAL_USER" ? this.hideProfile = true : this.hideProfile = false;
      this.showEmail = this.user.emailId ? true : false;
      this.cdrf.detectChanges();
    }



    this.route
      .queryParams
      .subscribe(params => {
        this.id = params['userId'];

        this.MyProfileService.getUser(this.id).subscribe(data => {

          this.model = data.result;
          this.userNameChange = this.model.username;

        })

      });

    setTimeout(() => {

      if (!this.hideProfile)
        document.getElementById("profile").style.opacity = "1";
    }, 200);


    $('#confirmPassword').bind("cut copy paste", function (e) {
      e.preventDefault();
    });
  }

  doLogin(user) {
    this.showUserLoader = true;


    this.MyProfileService.updateUser(user).subscribe(dataresult => {
      if (dataresult.message == "SUCCESS") {
        this.parentComponent.updateUser(dataresult.result);
        this.toasterService.pop('success', '', 'Profile updated successfully');
      }
      this.showUserLoader = false;
    })
  }


  changePass(oldPassword, newPass) {
    this.showLoader = true;
    this.passInfo.username = this.userNameChange;
    this.passInfo.oldPassword = oldPassword;
    this.passInfo.newPassword = newPass;


    this.MyProfileService.updatePassword(this.passInfo).subscribe(RESULT => {
      if (RESULT.message == "SUCCESS") {
        this.toasterService.pop('success', '', 'Password updated successfully');

      }
      if (RESULT.message == "FAIL") {
        this.toasterService.pop('error', '', 'Password not matched ');
      }
      this.showLoader = false;
    })
  }

  firstnameStatus(event: any) {
    if (event.target.value.length == 0) {
      this.firstNameStatus = false;
    }
    else {
      this.firstNameStatus = true;
    }
  }

  lastnameStatus(event: any) {
    if (event.target.value.length == 0) {
      this.lastNameStatus = false;
    }
    else {
      this.lastNameStatus = true;
    }
  }




  curentpassStatus(event: any) {

    if (event.target.value.length == 0) {
      this.currentPasswordStatus = false;
      this.curentpasswarning = true;
    }
    else {
      this.currentPasswordStatus = true;
      this.curentpasswarning = false;
    }
  }

  newpassStatus(event: any) {

    if (event.target.value.length == 0) {
      this.newPassStatus = false;
      this.newpasswarning = true;
    }
    else {

      this.newPassStatus = true;
      this.newpasswarning = false;
      this.newPass = event.target.value;
    }

    if (this.confirmPassStore != undefined) {
      if (this.newPass == this.confirmPassStore) {
        this.passwordMatch = true;
      }
      else {
        this.passwordMatch = false;
      }
    }


  }

  confirmpassStatus(event: any) {

    if (event.target.value.length == 0) {
      this.confirmPassStatus = false;

    }
    else {
      this.confirmPassStatus = true;

      this.confirmPassStore = event.target.value;
    }


    if (this.newPass == this.confirmPassStore) {
      this.passwordMatch = true;
    }
    else {
      this.passwordMatch = false;
    }
  }



} 