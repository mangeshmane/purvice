import { Injectable, } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../services/http.service';
import { environment } from '../../environments/environment';

@Injectable()
export class MyProfileService {
constructor( private http: HttpService) { }

getUser(userId): Observable<any> {
        return this.http.get(environment.baseUrl + 'user/' + userId)
            .map((response: Response) => {
                return response.json();
            })
    } 

     updateUser(data): Observable<any> {
        return this.http.put(environment.baseUrl + 'user/update', data)
            .map((response: Response) => {
                return response.json();
            })
    }

      updatePassword(data): Observable<any> {
        return this.http.post(environment.baseUrl + 'user/changePassword', data)
            .map((response: Response) => {
                return response.json();
            })
    }

}
