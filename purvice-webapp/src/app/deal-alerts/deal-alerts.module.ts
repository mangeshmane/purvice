import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DealAlertsPopupComponent } from './deal-alerts-popup.component';
import { DealAlertsCategoryComponent } from './deal-alerts-category.component';
import { DealAlertsRoutingModule } from './deal-alerts-routing.module';
import { DealAlertsService } from './deal-alerts.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { DataTableModule, SharedModule, AutoCompleteModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { TopUserDealAlertsComponent } from './top-user-deal-alerts.component';
import { MyDealAlertsComponent } from './my-deal-alerts.component';
import { DealAlertsWorkComponent } from './deal-alerts-work.component';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { MultiSelectModule } from 'primeng/primeng';
import { NgSelectModule } from '@ng-select/ng-select';
import { TreeModule } from 'primeng/primeng';
import { ChipsModule } from 'primeng/primeng';

@NgModule({
  imports: [
    DealAlertsRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MultiselectDropdownModule,
    DataTableModule,
    SharedModule,
    ButtonModule,
    AutoCompleteModule,
    ToasterModule,
    MultiSelectModule,
    NgSelectModule,
    TreeModule,
    ChipsModule
  ],
  declarations: [DealAlertsPopupComponent, TopUserDealAlertsComponent, MyDealAlertsComponent, DealAlertsWorkComponent,DealAlertsCategoryComponent],
  providers: [
    DealAlertsService,
    ToasterService
  ]
})

export class DealAlertsModule { }
