import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Http } from '@angular/http';
import { DealAlertsService } from './deal-alerts.service';
import { IMultiSelectSettings, IMultiSelectTexts, IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import {SelectItem} from 'primeng/primeng';

import * as _ from "lodash";
declare var $: any;
var that;

@Component({
  selector: 'my-deal-alerts',
  templateUrl: 'my-deal-alerts.component.html'
})
export class MyDealAlertsComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  public filteredCountriesMultiple: any = [];
  public countriesData: any;
  public user: any;
  public keywordData: any = [];

  public filteredKeywordMultiple: any = [];
  public myDealAlerts: any = [];
  public dealAlerts: any = {};
  public dealAlerData: any = {};
  public addDealAlerData: any = {};
  public addDealAlertLoader: boolean = false;
  public isExpand: boolean = false;
  public queryParams: any={};
  public pageNumber: number = 1; 
  public pageSize: number = 10 ;
  public query: string;

  forumOptions: SelectItem[];
  whenOptions: SelectItem[];
  notificationOptions: SelectItem[];

  constructor(private toasterService: ToasterService,private route: ActivatedRoute, private http: Http, private _router: Router, private dealAlertsService: DealAlertsService, private cdref: ChangeDetectorRef) {
    that = this;

    this.forumOptions = [
      { label: 'Option 1', value: 'Option 1' },
      { label: 'Option 2', value: 'Option 2'},
    ]; 
    this.whenOptions = [
      { label: 'Instantly', value: 'Instantly' },
      { label: 'Daily', value: 'Daily' },
    ];
    this.notificationOptions = [
      { label: 'Email', value: 'Email' },
    ];
  }

  ngOnInit() {
    
    this.pageLoad();
    this.myRangeChane();
  }
  pageLoad(){
    
    this.user = JSON.parse(localStorage.getItem('user'));
    that.userDealAlert(that.user.id).then(
      (data: any) => {
       this.myDealAlerts =  data;
       for(var i = 0; i < this.myDealAlerts.length; i++){
        this.myDealAlerts[i]["isEditMyDealAlert"] = false;
        this.myDealAlerts[i]["isUneditMyDealAlert"] = true;
        if(this.myDealAlerts[i].active){
          this.myDealAlerts[i]["lable"] = "Pause";
          this.myDealAlerts[i]["icon"] = "fa-pause-circle-o";
        }else{
          this.myDealAlerts[i]["lable"] = "Resume";
          this.myDealAlerts[i]["icon"] = "fa fa-play";
        }
       }
     });
  }
  editMyDealAlert(dealAlertData,dt){
    
    that.keywordData = [];
    that.dealAlerts = that.editAlertFormat(dealAlertData);
    that.dealAlerts.frequencies = that.dealAlerts.frequencies[0].split(/\s*,\s*/);
    dealAlertData.isEditMyDealAlert = true;
    dealAlertData.isUneditMyDealAlert = false;
      dt.toggleRow(dealAlertData);
    that.myRangeChane();
  }
  editAlertFormat(data): any{
    
    that.dealAlerData = {};
    let tempData = {};
    let alert = []; 
    tempData = {name:data.alertTitle, imgAlt:data.alertTitle, source:data.alertTitle}
    that.dealAlerData["alertTitle"]  = tempData;
    that.keywordData.push(data.alertTitle);

    if(data.frequencies != undefined){
      alert = [];
      alert.push(data.frequencies);
      that.dealAlerData["frequencies"] = alert; 
   }if(data.notificationMethod != undefined){
    alert = [];
    alert.push(data.notificationMethod);
    that.dealAlerData["notificationMethod"] = alert;
  }if(data.targetForum != undefined){
    alert = [];
    alert.push(data.forumOptions);
    that.dealAlerData["targetForum"] = alert;
  }if(data.rating != undefined){
    that.dealAlerData["rating"] = data.rating;
  }if(data.name != undefined){
    that.dealAlerData["name"] = data.name;
  }
  that.dealAlerData["userId"] = that.user.id;
    return that.dealAlerData;
  }
  pauseMyDealAlert(myDealAlert){
    
    that.pauseDealAlert(myDealAlert.id).then(
      (data: any) => {
        that.pageLoad();
     });
  }
  shareMyDealAlert(){
    
  }
  deleteMyDealAlert(myDealAlert){
     
    that.deleteDealAlert(myDealAlert.id).then(
      (data: any) => {
        that.toasterService.pop(data.message=='SUCCESS' ? 'success' : 'error', 'Deal Alert:', data.explanation);
        that.pageLoad();
      });
  }
  cancelMyDealAlert(id){
    
    for(var myDealAlertslen = 0; myDealAlertslen < that.myDealAlerts.length; myDealAlertslen++){
      if(id == that.myDealAlerts[myDealAlertslen].id){
        that.myDealAlerts[myDealAlertslen].isEditMyDealAlert = false;
        that.myDealAlerts[myDealAlertslen].isUneditMyDealAlert = true;
      }
    }
  }

  validationField(dealAlerts): any {
    let Valid = {};
    if (dealAlerts.notificationMethod == undefined || dealAlerts.notificationMethod.length == 0) {
      Valid["Message"] = "You must have at least 1 notification selected";
      Valid["isValid"] = false;
    } else if (dealAlerts.frequencies == undefined || dealAlerts.frequencies.length == 0) {
      Valid["Message"] = "You must have at least 1 frequency selected";
      Valid["isValid"] = false;
    } else {
      Valid["isValid"] = true;
    }
    return Valid;
  }

  saveMyDealAlert(dealAlertData, id){
       let validating = that.validationField(dealAlertData);
       if (validating.isValid) {
         if (dealAlertData.alertTitle != undefined) {
           if (that.keywordData.length > 0) {
             $('#updateDealAlertModal').modal('show');
             that.addDealAlertLoader = true;
             if (this.user) {
               let dataAlert = that.addAlertFormat(dealAlertData, that.keywordData);
               var finalAlertData = [];
               for (var i = 0; i < dataAlert.alertTitle.length; i++) {
                 let finalData = {};
                 finalData["alertTitle"] = dataAlert.alertTitle[i];
                 if (dataAlert.frequencies != undefined)
                   finalData["frequencies"] = dataAlert.frequencies.toString();
                 if (dataAlert.notificationMethod != undefined)
                   finalData["notificationMethod"] = dataAlert.notificationMethod.toString();
                 if (dataAlert.targetForum != undefined)
                   finalData["targetForum"] = null;
                 if (dataAlert.rating != undefined)
                   finalData["rating"] = dataAlert.rating;
                 if (dataAlert.name != undefined)
                   finalData["name"] = dataAlert.name;
                 if(that.keywordData[i].url != null)
                   finalData["url"] = that.keywordData[i].url;
                 if(that.keywordData[i].imageUrl != null)
                   finalData["imageUrl"] = that.keywordData[i].imageUrl;
                 if(that.keywordData[i].category != null){
                   finalData["categoryName"] = that.keywordData[i].category;
                   finalData["keyword"] = "category";
                 }else{
                   finalData["keyword"] = "key";
                 }
                 finalData["userId"] = dataAlert.userId;
                 finalData["id"] = id;
                 finalAlertData.push(finalData);
               }
   
             /*  if (dataAlert.frequencies != undefined) {
                 if (dataAlert.frequencies.length > 1) {
                   for (var i = 0; i < dataAlert.alertTitle.length; i++) {
                     let finalData = {};
                     finalData["alertTitle"] = dataAlert.alertTitle[i];
                     if (dataAlert.frequencies != undefined)
                       finalData["frequencies"] = dataAlert.frequencies[1];
                     if (dataAlert.notificationMethod != undefined)
                       finalData["notificationMethod"] = dataAlert.notificationMethod[0];
                     if (dataAlert.targetForum != undefined)
                       finalData["targetForum"] = null;
                     if (dataAlert.rating != undefined)
                       finalData["rating"] = dataAlert.rating;
                     if (dataAlert.name != undefined)
                       finalData["name"] = dataAlert.name;
                     if(that.keywordData[i].url != null)
                       finalData["url"] = that.keywordData[i].url;
                     if(that.keywordData[i].imageUrl != null)
                       finalData["imageUrl"] = that.keywordData[i].imageUrl;
                     if(that.keywordData[i].category != null){
                         finalData["categoryName"] = that.keywordData[i].category;
                         finalData["keyword"] = "category";
                     }else{
                         finalData["keyword"] = "key";
                     }
                     finalData["userId"] = dataAlert.userId;
                     finalData["id"] = id;
                     finalAlertData.push(finalData);
                   }
                 }
               }*/
               that.showLoader = true;
               that.updateDealAlert(finalAlertData[0]).then(
                 (data: any) => {
                   that.dealAlerMessage = data.explanation;
                   that.showLoader = false;
                   if (data.message == 'SUCCESS') {
                    that.dealAlerts = data.result;
                     $('#updateDealAlertModal').modal('hide');
                     that.pageLoad();
                     that.addDealAlertLoader = false;
                     that.cancelMyDealAlert(id);
                   }
                 })
             }
           } else {
             that.isdealAlertMessage = true;
             that.dealAlertMessage = "Please enter a keyword to create your deal alert";
           }
         } else {
           that.isdealAlertMessage = true;
           that.dealAlertMessage = "Please enter a keyword to create your deal alert";
         }
       } else {
         that.errorMessage = validating.Message;
         $('#errorDealAlertModal').modal('show');
       }
  }
  addAlertFormat(data, keywordData): any{
    
    that.dealAlerData = [];
    for (var alertcnt = 0; alertcnt < keywordData.length; alertcnt++) {
      if (that.dealAlerData["alertTitle"]) {
        let alert = that.dealAlerData["alertTitle"];
        alert.push(keywordData[alertcnt]);
        that.dealAlerData["alertTitle"] = alert;
      } else {
        let alert = [];
        alert.push(keywordData[alertcnt]);
        that.dealAlerData["alertTitle"] = alert;
      }
    }
    if (data.frequencies != undefined) {
      for (var frequenciescnt = 0; frequenciescnt < data.frequencies.length; frequenciescnt++) {
        if (that.dealAlerData["frequencies"]) {
          let alert = that.dealAlerData["frequencies"];
          alert.push(data.frequencies[frequenciescnt]);
          that.dealAlerData["frequencies"] = alert;
        } else {
          let alert = [];
          alert.push(data.frequencies[frequenciescnt]);
          that.dealAlerData["frequencies"] = alert;
        }
      }
    } if (data.notificationMethod != undefined) {
      for (var notificationcnt = 0; notificationcnt < data.notificationMethod.length; notificationcnt++) {
        if (that.dealAlerData["notificationMethod"]) {
          let alert = that.dealAlerData["notificationMethod"];
          alert.push(data.notificationMethod[notificationcnt]);
          that.dealAlerData["notificationMethod"] = alert;
        } else {
          let alert = [];
          alert.push(data.notificationMethod[notificationcnt]);
          that.dealAlerData["notificationMethod"] = alert;
        }
      }
    } if (data.targetForum != undefined) {
      for (var targetForumcnt = 0; targetForumcnt < data.targetForum.length; targetForumcnt++) {
        if (that.dealAlerData["targetForum"]) {
          let alert = that.dealAlerData["targetForum"];
          alert.push(data.targetForum[targetForumcnt]);
          that.dealAlerData["targetForum"] = alert;
        } else {
          let alert = [];
          alert.push(data.targetForum[targetForumcnt]);
          that.dealAlerData["targetForum"] = alert;
        }
      }
    } if (data.rating != undefined) {
      that.dealAlerData["rating"] = data.rating;
    } if (data.name != undefined) {
      that.dealAlerData["name"] = data.name;
    }else {
      for(var i = 0; i < that.dealAlerData.alertTitle.length; i++){
        if (that.dealAlerData["name"]) {
          let alert = that.dealAlerData["name"];
          alert.push("Any "+that.dealAlerData.alertTitle[i]);
          that.dealAlerData["name"] = alert;
        } else {
          let alert = [];
          alert.push( "Any "+that.dealAlerData.alertTitle[i]);
          that.dealAlerData["name"] = alert;
        }
      }
    }
    that.dealAlerData["userId"] = that.user.id;
    return that.dealAlerData;
  }

  enterKeyEvent(event){
    
  }
  userDealAlert(userId) {
    
    return new Promise((resolve, reject) => {
      this.dealAlertsService.userDealAlert(userId).subscribe(
        (response) => {
          resolve(response.result);
        });
    })
  }
  pauseDealAlert(dealAlertId){
    return new Promise((resolve, reject) => {
      this.dealAlertsService.pauseDealAlert(dealAlertId).subscribe(
        (response) => {
          resolve(response);
        });
    })
  }
  deleteDealAlert(dealAlertId){
    return new Promise((resolve, reject) => {
      this.dealAlertsService.deleteDealAlert(dealAlertId).subscribe(
        (response) => {
          resolve(response);
        });
    })
  }

  filterKeywordMultiple(event) {
    
    if(event != null){
      this.query = event.target.value + event.key;
      this.queryParamObject();
    }
    that.getDealsByKeyword(this.queryParams).then(
      (data: any) => {
        this.filteredKeywordMultiple = this.filterKeyword(data);
      })
  }

  filterKeyword(dealAlertKeyword: any[]): any[] {
    
    for (let i = 0; i < dealAlertKeyword.length; i++) {
      if(dealAlertKeyword[i].id == 0){
        dealAlertKeyword[i]["key"] = "keyword";
      }else{
        dealAlertKeyword[i]["key"] = "category";
      }
  }
  return dealAlertKeyword;
}
queryParamObject(){
  
  that.queryParams.pageNumber = that.pageNumber; 
  that.queryParams.pageSize = that.pageSize; 
  that.queryParams.keyword = that.query;
}
onScroll(event){
  
  if(event.deltaY > 0){
    that.pageNumber = that.pageNumber + 1;
    that.queryParamObject();
    that.getDealsByKeyword(that.queryParams).then(
      (data: any) => {
      if(that.pageNumber == 1){
        that.filteredKeywordMultiple = [];
      }
      that.filteredKeywordMultiple = that.filteredKeywordMultiple.concat(that.filterKeyword(data));
    });
  }
}
backspace(event){
  
  that.query = that.query.substring(0, that.query.length - 1);
  that.pageNumber = 0;
  that.queryParamObject();
  if(that.query == ""){
    that.filteredKeywordMultiple = [];
  }else{
    that.filterKeywordMultiple(null);
  }
}
/*  filterKeywordMultiple(event){
    
    let query = event.query;
    that.getDealsByKeyword(query).then(
       (data: any) => {
        this.filteredKeywordMultiple = this.filterKeyword(query, data);
      })
  }
  filterKeyword(query, dealAlertKeyword: any[]):any[] {
    
    let filtered : any[] = [];
    for(let i = 0; i < dealAlertKeyword.length; i++) {
        dealAlertKeyword[i]["name"] = dealAlertKeyword[i].source;
        if(dealAlertKeyword[i].category != null){
          dealAlertKeyword[i]["key"] = "category";
        }else{
          dealAlertKeyword[i]["key"] = "keyword";
        }
        let searchKey = dealAlertKeyword[i];
        let searchData = searchKey.source.split(".");
        let length = searchData.length
        if(length > 2){
          if (searchData[length - 2].toLowerCase().indexOf(query.toLowerCase()) == 0) {
            filtered.push(searchKey);
          }
        }else{
          filtered.push(searchKey);
        }
    }
    return filtered;
}*/

getDealsByKeyword(keyword) {
  
  return new Promise((resolve, reject) => {
    this.dealAlertsService.getDealsByKeyword(keyword).subscribe(
      (response) => {
        resolve(response.result);
      });
  })
}
onSelect(event){

  if (event != undefined) {
    let data = event.target.innerText.split('>');
  if(data != ""){
    that.keywordData.push(data[data.length-1]);
  }else{
    that.keywordData.push(event.target.innerText);
  }
    that.dealAlerts.alertTitle = [];
    that.isdealAlertMessage = false;
  }
}

  updateDealAlert(data) {
    
    return new Promise((resolve, reject) => {
      this.dealAlertsService.updateDealAlert(data).subscribe(data => {
        resolve(data);
      })
    })
  }
  onRemove(removeData){
    
    for(var keyCnt=0; keyCnt < that.keywordData.length; keyCnt++){
      if(that.keywordData[keyCnt].id == removeData.id){
        that.keywordData.splice(keyCnt, 1);
      }
    }
  }

  myRangeChane() {
    
    var sheet = document.createElement('style'),
      $rangeInput = $('.range input'),
      prefs = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];

    document.body.appendChild(sheet);

    var getTrackStyle = function (el) {
      var curVal = el.value,
        val = (curVal - 1) * 16.666666667,
        style = '';

      // Set active label
      $('.range-labels li').removeClass('active selected');

      var curLabel = $('.range-labels').find('li:nth-child(' + curVal + ')');

      curLabel.addClass('active selected');
      curLabel.prevAll().addClass('selected');

      // Change background gradient
      for (var i = 0; i < prefs.length; i++) {
        // style += '.range {background: linear-gradient(to right, #37adbf 0%, #37adbf ' + val + '%, #fff ' + val + '%, #fff 100%)}';
        // style += '.range input::-' + prefs[i] + '{background: linear-gradient(to right, #37adbf 0%, #37adbf ' + val + '%, #b2b2b2 ' + val + '%, #b2b2b2 100%)}';
      }

      return style;
    }

    $rangeInput.on('input', function () {
      sheet.textContent = getTrackStyle(this);
    });

    // Change input value on label click
    $('.range-labels li').on('click', function () {
      var index = $(this).index();

      $rangeInput.val(index + 1).trigger('input');

    });
  }
}
