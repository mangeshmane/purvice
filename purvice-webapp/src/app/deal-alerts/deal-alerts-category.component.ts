import { Component, OnInit, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { DealAlertsService } from './deal-alerts.service';
import { ToasterService } from 'angular2-toaster';
import { TreeNode } from 'primeng/primeng';
import * as _ from "lodash";

declare var $: any;
var that;

@Component({
  selector: 'deal-alerts-category',
  templateUrl: 'deal-alerts-category.component.html'
})
export class DealAlertsCategoryComponent implements OnInit {

  dealCategory: TreeNode[];
  public loading: boolean;
  @Input()
  selectDealCategory: string[];
  public isTreeShow: boolean = false;
  public user: any;
  public success: string;
  public isPlace: boolean = false;
  public categoryId: any = [];
   @Output() refreshEvent = new EventEmitter<any>()
  constructor(private toasterService: ToasterService, private route: ActivatedRoute, private http: Http, private _router: Router, private dealAlertsService: DealAlertsService, private cdref: ChangeDetectorRef) {
    that = this;
  }

  ngOnInit() {
    this.loadTree();
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    } catch (e) {
      this.user = null;
    }
  }

  loadTree() {
    this.getAllDealCategoryByParentId().then(
      (data: any) => {
        this.dealCategory = this.prepareTreeTablesRoot(data);
      })
  }

  prepareTreeTablesRoot(dealCategory): any {
    
    var result = [];
    for (var i = 0; i < dealCategory.length; i++) {
      var data = {};
      dealCategory[i].disabled = true;
      data["data"] = dealCategory[i];
      data["label"] = dealCategory[i].categoryName;
      data["leaf"] = false;
      result.push(data);
    }
    return result;
  }
  prepareTreeTablesChild(dealCategory): any {
    
    var result = [];
    for (var i = 0; i < dealCategory.length; i++) {
      var data = {};
      dealCategory[i].disabled = true;
      dealCategory[i].name = dealCategory[i].categoryName;

      data["data"] = dealCategory[i];
      data["label"] = dealCategory[i].categoryName;
      data["leaf"] = false;
      result.push(data);
    }
    return result;
  }
  nodeExpand(event) {
    
    if (event.node) {
      this.getDealCategoriesByParentId(event.node.data.id).then((data) => {
        event.node.children = this.prepareTreeTablesChild(data);
      });
    }
  }
  nodeSelect(event, node) {
    
    this.isPlace = true;
    this.categoryId.push(event.node.data.id);
    if (event.node) {
      this.getDealById(event.node.data.id).then((data) => {
        if (node == undefined)
          node = [];
        node.push(data.categoryName);
        this.selectDealCategory = node;
      });
    }
  }
  onAdd() {
    
  }
  onFocus() {
    this.isTreeShow = true;
  }
  addAlert() {
    
    var finalAlertData = [];
    for (var i = 0; i < this.selectDealCategory.length; i++) {
      let alerts = this.selectDealCategory[i].split('>');
      let finalData = {};
      finalData["alertTitle"] = alerts[alerts.length - 1];
      finalData["frequencies"] = "Instantly";
      finalData["notificationMethod"] = "Email";
      finalData["targetForum"] = null;
      finalData["rating"] = "3";
      finalData["name"] = "Any " + alerts[alerts.length - 1];
      finalData["categoryName"] = alerts[alerts.length - 1];
      finalData["keyword"] = "category";
      finalData["categoryId"] = this.categoryId[i];
      finalData["createdBy"] = "category";
      finalData["userId"] = that.user.id;
      finalAlertData.push(finalData);
    }
    $('#addDealAlertCategoryModal').modal('show');
    that.createDealAlert(finalAlertData).then(
      (data: any) => {
        if (data.message == 'SUCCESS') {
          that.success = data.explanation;
          that.isTreeShow = false;
          that.isPlace = false;
          that.selectDealCategory = [];
          this.categoryId = [];
          $('#addDealAlertCategoryModal').modal('hide');
        }
        this.refreshEvent.emit('')
      })
  }
  cleare() {
    
    that.isTreeShow = false;
    that.isPlace = false;
    that.selectDealCategory = [];
  }
  eventHandler(event) {
    
    let query = event.target.value + event.key;
    that.searchDealCategory(query).then((data) => {
      that.dealCategory = [];
      that.dealCategory = that.prepareTreeTablesRoot(data.result);
    });
  }
  getAllDealCategoryByParentId() {
    
    return new Promise((resolve, reject) => {
      this.dealAlertsService.getAllDealCategoryByParentId().subscribe(
        (response) => {
          resolve(response.result);
        });
    })
  }
  getDealCategoriesByParentId(parentId): any {
    
    return new Promise((resolve, reject) => {
      this.dealAlertsService.getDealCategoriesByParentId(parentId)
        .subscribe(
          (data) => {
            resolve(data.result);
          });
    })
  }
  getDealById(parentId): any {
    
    return new Promise((resolve, reject) => {
      this.dealAlertsService.getDealById(parentId)
        .subscribe(
          (data) => {
            resolve(data.result);
          });
    })
  }
  searchDealCategory(keyWord): any {
    
    return new Promise((resolve, reject) => {
      this.dealAlertsService.searchDealCategory(keyWord)
        .subscribe(
          (data) => {
            resolve(data);
          });
    })
  }
  createDealAlert(data) {
    
    return new Promise((resolve, reject) => {
      this.dealAlertsService.createDealAlert(data).subscribe(data => {
        resolve(data);
      })
    })
  }
}
