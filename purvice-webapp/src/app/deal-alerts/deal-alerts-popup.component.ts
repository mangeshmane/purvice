import { Component, OnInit, ChangeDetectorRef, Output, EventEmitter, ViewChild, Directive, HostListener, AfterViewInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Http } from '@angular/http';
import { DealAlertsService } from './deal-alerts.service';
import { MyDealAlertsComponent } from './my-deal-alerts.component';
import { SelectItem } from 'primeng/primeng';
import { DealAlertsCategoryComponent } from './deal-alerts-category.component';

import * as _ from "lodash";

declare var $: any;
var that;

@Component({
  templateUrl: 'deal-alerts-popup.component.html'
})

export class DealAlertsPopupComponent implements OnInit {
 

  @BlockUI() blockUI: NgBlockUI;
  @ViewChild(MyDealAlertsComponent) myDealAlertsComponent: MyDealAlertsComponent;

  public model: any = {};
  public dealAlerts: any = {};
  public dealAlerData: any = {};
  public kewordData: any = {};
  public keywordSearchData: any = {};
  public optionButton: string = "More Options...";
  public isValid: boolean = false;
  public showLoader: boolean = false;
  public addDealAlertLoader: boolean = false;
  public isUserLogin: boolean = false;
  public user: any;
  public dealAlertMessage: string;
  public errorMessage: string;
  public isdealAlertMessage: boolean = false;

  public filteredKeywordMultiple: any = [];
  public dealAlertKeyword: any;
  public dealAlertKeywordData: any;
  public keywordData: any = [];
  public category: any = [];
  public keywordDataElement: any = [];
  public finalData: any = {};
  forumOptions: SelectItem[];
  whenOptions: SelectItem[];
  notificationOptions: SelectItem[];
  public queryParams: any={};
  public pageNumber: number = 1; 
  public pageSize: number = 10 ;
  public query: string;
  public searchAlertKey: any;
  selectDealCategory: string[];

  @ViewChild(DealAlertsCategoryComponent)
  public dealAlertsCategoryComponent: DealAlertsCategoryComponent;
  
  constructor(private activeRoute: ActivatedRoute, private http: Http, private _router: Router, private dealAlertsService: DealAlertsService, private cdref: ChangeDetectorRef) {
    that = this;

    this.forumOptions = [
      { label: 'Option 1', value: 'Option 1' },
      { label: 'Option 2', value: 'Option 2'},
    ]; 
    this.whenOptions = [
      { label: 'Instantly', value: 'Instantly' },
      { label: 'Daily', value: 'Daily' },
    ];
    this.notificationOptions = [
      { label: 'Email', value: 'Email' },
    ];

  }

  ngOnInit() {
    this.queryParamObject();
    this.setDefaultMultiselect();
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    } catch (e) {
      this.user = null;
    }
    if (this.user != null) {
      this.isUserLogin = true;
      if (JSON.parse(localStorage.getItem('dealAlert')) != null) {
        this.dealAlerts = JSON.parse(localStorage.getItem('dealAlert'));
        this.keywordData = JSON.parse(localStorage.getItem('keywordData'));
        that.isValid = localStorage.getItem('isValid');
        localStorage.removeItem("dealAlert");
        localStorage.removeItem("isValid");
        localStorage.removeItem("keywordData");
        that.myDealAlertsComponent.pageLoad();
      }
    }
this.rangeChane();

  }
  
  filterKeywordMultiple(event) {
    
    if(event != null){
      this.query = event.target.value + event.key;
      this.queryParamObject();
    }
    that.getDealsByKeyword(this.queryParams).then(
      (data: any) => {
        this.filteredKeywordMultiple = [];
        this.filteredKeywordMultiple = this.filterKeyword(data);
      })
  }

  filterKeyword(dealAlertKeyword: any[]): any[] {
    
    for (let i = 0; i < dealAlertKeyword.length; i++) {
      if(dealAlertKeyword[i].id == 0){
        dealAlertKeyword[i]["key"] = "keyword";
      }else{
        dealAlertKeyword[i]["key"] = "category";
      }
  }
  return dealAlertKeyword;
}


  setDefaultMultiselect() {
    that.dealAlerts["notificationMethod"] = ['Email'];
    that.dealAlerts["frequencies"] = ['Instantly'];
    that.dealAlerts["rating"] = 3;
  }

  toggleClick() {
    
    if (that.optionButton == "Fewer Options...") {
      that.optionButton = "More Options...";
      that.isValid = false;
    } else {
      that.optionButton = "Fewer Options...";
      that.isValid = true;
    }
    localStorage.setItem('isValid', that.isValid);
  }

  validationField(dealAlerts): any {
    
    let Valid = {};
    if (dealAlerts.notificationMethod == undefined || dealAlerts.notificationMethod.length == 0) {
      Valid["Message"] = "You must have at least 1 notification selected";
      Valid["isValid"] = false;
    } else if (dealAlerts.frequencies == undefined || dealAlerts.frequencies.length == 0) {
      Valid["Message"] = "You must have at least 1 frequency selected";
      Valid["isValid"] = false;
    } else {
      Valid["isValid"] = true;
    }
    return Valid;
  }
  addAlert(dealAlerts) {
    
    let validating = that.validationField(dealAlerts);
    if (validating.isValid) {
      if (dealAlerts.alertTitle != undefined) {
        if (that.keywordData.length > 0) {
          $('#addDealAlertModal').modal('show');
          that.addDealAlertLoader = true;
          if (this.user) {
            let dataAlert = that.addAlertFormat(dealAlerts, that.keywordData);
            var finalAlertData = [];
            for (var i = 0; i < dataAlert.alertTitle.length; i++) {
              let finalData = {};
              finalData["alertTitle"] = dataAlert.alertTitle[i];
              if (dataAlert.frequencies != undefined)
                finalData["frequencies"] = dataAlert.frequencies.toString();
              if (dataAlert.notificationMethod != undefined)
                finalData["notificationMethod"] = dataAlert.notificationMethod.toString();
              if (dataAlert.targetForum != undefined)
                finalData["targetForum"] = null;
              if (dataAlert.rating != undefined)
                finalData["rating"] = dataAlert.rating;
              if (dataAlert.name != undefined)
                finalData["name"] = dataAlert.name[i];
              if(that.keywordData[i].url != null)
                finalData["url"] = that.keywordData[i].url;
              if(that.keywordData[i].imageUrl != null)
                finalData["imageUrl"] = that.keywordData[i].imageUrl;
              if(that.keywordData[i].category != null){
                finalData["categoryName"] = that.keywordData[i].category;
                finalData["keyword"] = "category";
              }else{
                finalData["keyword"] = "key";
              }
              finalData["userId"] = dataAlert.userId;
              finalData["categoryId"] = dealAlerts.categoryId[i];
              finalData["createdBy"] = "keyword";
              finalAlertData.push(finalData);
            }

          /*  if (dataAlert.frequencies != undefined) {
              if (dataAlert.frequencies.length > 1) {
                for (var i = 0; i < dataAlert.alertTitle.length; i++) {
                  let finalData = {};
                  finalData["alertTitle"] = dataAlert.alertTitle[i];
                  if (dataAlert.frequencies != undefined)
                    finalData["frequencies"] = dataAlert.frequencies[1];
                  if (dataAlert.notificationMethod != undefined)
                    finalData["notificationMethod"] = dataAlert.notificationMethod[0];
                  if (dataAlert.targetForum != undefined)
                    finalData["targetForum"] = null;
                  if (dataAlert.rating != undefined)
                    finalData["rating"] = dataAlert.rating;
                  if (dataAlert.name != undefined)
                    finalData["name"] = dataAlert.name;
                  if(that.keywordData[i].url != null)
                    finalData["url"] = that.keywordData[i].url;
                  if(that.keywordData[i].imageUrl != null)
                    finalData["imageUrl"] = that.keywordData[i].imageUrl;
                  if(that.keywordData[i].category != null){
                      finalData["categoryName"] = that.keywordData[i].category;
                      finalData["keyword"] = "category";
                  }else{
                      finalData["keyword"] = "key";
                  }
                  finalData["userId"] = dataAlert.userId;
                  finalAlertData.push(finalData);
                }
              }
            } */

            that.showLoader = true;
            that.createDealAlert(finalAlertData).then(
              (data: any) => {
                that.dealAlerMessage = data.explanation;
                that.showLoader = false;
                if (data.message == 'SUCCESS') {
                  that.dealAlerts = {};
                  that.keywordData = [];
                  that.setDefaultMultiselect();
                  $('#addDealAlertModal').modal('hide');
                  that.addDealAlertLoader = false;
                  that.myDealAlertsComponent.pageLoad();
                }
              })
          } else {
            localStorage.setItem('dealAlert', JSON.stringify(dealAlerts));
            localStorage.setItem('keywordData', JSON.stringify(that.keywordData));
            this._router.navigate(['/login',], { relativeTo: this.activeRoute });
          }
        } else {
          that.isdealAlertMessage = true;
          that.dealAlertMessage = "Please enter a keyword to create your deal alert";
        }
      } else {
        that.isdealAlertMessage = true;
        that.dealAlertMessage = "Please enter a keyword to create your deal alert";
      }
    } else {
      that.errorMessage = validating.Message;
      $('#errorDealAlertModal').modal('show');
    }
  }

  addAlertFormat(data, keywordData): any {
    
    that.dealAlerData = [];
    for (var alertcnt = 0; alertcnt < keywordData.length; alertcnt++) {
      if (that.dealAlerData["alertTitle"]) {
        let alert = that.dealAlerData["alertTitle"];
        alert.push(keywordData[alertcnt]);
        that.dealAlerData["alertTitle"] = alert;
      } else {
        let alert = [];
        alert.push(keywordData[alertcnt]);
        that.dealAlerData["alertTitle"] = alert;
      }
    }
    if (data.frequencies != undefined) {
      for (var frequenciescnt = 0; frequenciescnt < data.frequencies.length; frequenciescnt++) {
        if (that.dealAlerData["frequencies"]) {
          let alert = that.dealAlerData["frequencies"];
          alert.push(data.frequencies[frequenciescnt]);
          that.dealAlerData["frequencies"] = alert;
        } else {
          let alert = [];
          alert.push(data.frequencies[frequenciescnt]);
          that.dealAlerData["frequencies"] = alert;
        }
      }
    } if (data.notificationMethod != undefined) {
      for (var notificationcnt = 0; notificationcnt < data.notificationMethod.length; notificationcnt++) {
        if (that.dealAlerData["notificationMethod"]) {
          let alert = that.dealAlerData["notificationMethod"];
          alert.push(data.notificationMethod[notificationcnt]);
          that.dealAlerData["notificationMethod"] = alert;
        } else {
          let alert = [];
          alert.push(data.notificationMethod[notificationcnt]);
          that.dealAlerData["notificationMethod"] = alert;
        }
      }
    } if (data.targetForum != undefined) {
      for (var targetForumcnt = 0; targetForumcnt < data.targetForum.length; targetForumcnt++) {
        if (that.dealAlerData["targetForum"]) {
          let alert = that.dealAlerData["targetForum"];
          alert.push(data.targetForum[targetForumcnt]);
          that.dealAlerData["targetForum"] = alert;
        } else {
          let alert = [];
          alert.push(data.targetForum[targetForumcnt]);
          that.dealAlerData["targetForum"] = alert;
        }
      }
    } if (data.rating != undefined) {
      that.dealAlerData["rating"] = data.rating;
    } if (data.name != undefined) {
      let alert = [];
          alert.push( data.name);
          that.dealAlerData["name"] = alert;
     // that.dealAlerData["name"] = data.name;
    }else {
      for(var i = 0; i < that.dealAlerData.alertTitle.length; i++){
        if (that.dealAlerData["name"]) {
          let alert = that.dealAlerData["name"];
          alert.push("Any "+that.dealAlerData.alertTitle[i]);
          that.dealAlerData["name"] = alert;
        } else {
          let alert = [];
          alert.push( "Any "+that.dealAlerData.alertTitle[i]);
          that.dealAlerData["name"] = alert;
        }
      }
    }
    that.dealAlerData["userId"] = that.user.id;
    return that.dealAlerData;
  }

  howWork() {
    
    that._router.navigate(['/deal-alerts/deal-alerts-work'], { relativeTo: that.activeRoute });
  }
  enterKeyEvent(event) {
    
  }

  onRemove(removeData) {
    
    for (var keyCnt = 0; keyCnt < that.keywordData.length; keyCnt++) {
      if (that.keywordData[keyCnt].id == removeData.id) {
        that.keywordData.splice(keyCnt, 1);
      }
    }
  }

  onSelectButton(event) {
    
  if(event.categoryName != undefined){
    let data = event.categoryName.split('>');
    that.keywordData.push(data[data.length-1]);
  }
  that.dealAlerts.alertTitle = [];
  that.isdealAlertMessage = false;
}
  createDealAlert(data) {
    
    return new Promise((resolve, reject) => {
      this.dealAlertsService.createDealAlert(data).subscribe(data => {
        resolve(data);
      })
    })
  }
  getDealsByKeyword(keyword) {
    
    return new Promise((resolve, reject) => {
      this.dealAlertsService.getDealsByKeyword(keyword).subscribe(
        (response) => {
          resolve(response.result);
        });
    })
  }
  rangeChane() {
    
    var sheet = document.createElement('style'),
      $rangeInput = $('.range input'),
      prefs = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];

    document.body.appendChild(sheet);

    var getTrackStyle = function (el) {
      var curVal = el.value,
        val = (curVal - 1) * 16.666666667,
        style = '';

      // Set active label
      $('.range-labels li').removeClass('active selected');

      var curLabel = $('.range-labels').find('li:nth-child(' + curVal + ')');

      curLabel.addClass('active selected');
      curLabel.prevAll().addClass('selected');

      // Change background gradient
      for (var i = 0; i < prefs.length; i++) {
        // style += '.range {background: linear-gradient(to right, #37adbf 0%, #37adbf ' + val + '%, #fff ' + val + '%, #fff 100%)}';
        // style += '.range input::-' + prefs[i] + '{background: linear-gradient(to right, #37adbf 0%, #37adbf ' + val + '%, #b2b2b2 ' + val + '%, #b2b2b2 100%)}';
      }

      return style;
    }

    $rangeInput.on('input', function () {
      sheet.textContent = getTrackStyle(this);
    });

    // Change input value on label click
    $('.range-labels li').on('click', function () {
      var index = $(this).index();

      $rangeInput.val(index + 1).trigger('input');

    });
  }
  queryParamObject(){
    
    that.queryParams.pageNumber = that.pageNumber; 
    that.queryParams.pageSize = that.pageSize; 
    that.queryParams.keyword = that.query;
  }
  onScroll(event){
    
    if(event.deltaY > 0){
      that.pageNumber = that.pageNumber + 1;
      that.queryParamObject();
      that.getDealsByKeyword(that.queryParams).then(
        (data: any) => {
        if(that.pageNumber == 1){
          that.filteredKeywordMultiple = [];
        }
        that.filteredKeywordMultiple = that.filteredKeywordMultiple.concat(that.filterKeyword(data));
      });
    }
}
backspace(event){
  
  that.query = that.query.substring(0, that.query.length - 1);
  that.pageNumber = 1;
  that.queryParamObject();
  if(that.query == ""){
    that.filteredKeywordMultiple = [];
  }else{
    that.filterKeywordMultiple(null);
  }
}
addAlertByCat(){
  
  that.dealAlertsCategoryComponent.addAlert();
 
}
pageLoad(value) {
  
 that.myDealAlertsComponent.pageLoad();
}
cleare(){
  
  that.dealAlertsCategoryComponent.cleare();
}
onChange(event){
  
}
}