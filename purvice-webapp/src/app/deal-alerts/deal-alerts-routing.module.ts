import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { DealAlertsPopupComponent } from './deal-alerts-popup.component';
import { TopUserDealAlertsComponent } from './top-user-deal-alerts.component';
import { MyDealAlertsComponent } from './my-deal-alerts.component';
import { DealAlertsWorkComponent } from './deal-alerts-work.component';
import { DealAlertsCategoryComponent } from './deal-alerts-category.component';

const routes: Routes = [
  {
    path: '',
    component: DealAlertsPopupComponent,
    data: {
      title: 'Deal Alerts'
    }
  },{
    path: 'top-user-deal-alerts', 
    component: TopUserDealAlertsComponent,    
    data: {
      title: 'TopUserDealAlerts'
    }
  },{
    path: 'my-deal-alerts', 
    component: MyDealAlertsComponent,    
    data: {
      title: 'MyDealAlerts'
    }
  },{
    path: 'deal-alerts-work', 
    component: DealAlertsWorkComponent,    
    data: {
      title: 'DealAlertsWork'
    }
  },{
    path: 'deal-alerts-category', 
    component: DealAlertsCategoryComponent,    
    data: {
      title: 'DealAlertsCategory'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DealAlertsRoutingModule {}
