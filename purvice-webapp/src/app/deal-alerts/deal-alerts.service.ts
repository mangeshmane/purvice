import { Injectable, } from '@angular/core';
import { Http, Response, } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../services/http.service';
import { environment } from '../../environments/environment';

@Injectable()
export class DealAlertsService {
    constructor(private http: HttpService) {
    }

    createDealAlert(data): Observable<any> {
        
        return this.http.post(environment.baseUrl + 'deals/dealalertsave', data)
            .map((response: Response) => {
                return response.json()
            })
    }

    getDealsByKeyword(data): Observable<any> {
        
        return this.http.get(environment.baseUrl + 'dealcategory/search?' + this.prepareQueryParams(data))
            .map((response: Response) => {
                return response.json();
            })
    }

    prepareQueryParams(data): any {
        
		var finalQuery = '';
		for (let key in data) {
			if (Array.isArray(data[key])) {
				let innerMap = data[key];
				for (let key2 in innerMap) {
					finalQuery += '&' + key + '=' + innerMap[key2];
				}
			} else {
				finalQuery += '&' + key + '=' + data[key];
			}
		}
		return finalQuery;
    }

    updateDealAlert(dealAlert): Observable<any> {
        
        return this.http.put(environment.baseUrl + 'deals/updateDealAlert', dealAlert)
            .map((response: Response) => {
                return response.json()
            })
    }

    pauseDealAlert(dealAlertId): Observable<any> {
        
        return this.http.get(environment.baseUrl + 'deals/pause?dealAlertId=' + dealAlertId)
            .map((response: Response) => {
                return response.json()
            })
    }
    deleteDealAlert(dealAlertId): Observable<any> {
        
        return this.http.get(environment.baseUrl + 'deals/delete?dealAlertId=' + dealAlertId)
            .map((response: Response) => {
                return response.json()
            })
    }
    userDealAlert(userId): Observable<any> {
        
        return this.http.get(environment.baseUrl + 'deals/userdealalert?userId=' + userId)
            .map((response: Response) => {
                return response.json()
            })
    }
    getAllDealCategoryByParentId(): Observable<any> {
        
        return this.http.get(environment.baseUrl + 'dealcategory/getAllDealCategorys')
            .map((response: Response) => {
                return response.json()
            })
    }
    getDealCategoriesByParentId(parentId): Observable<any> {
        
        return this.http.get(environment.baseUrl + 'dealcategory/getDealCategoryByParentId?parentId=' + parentId)
            .map((response: Response) => {
                return response.json();
            })
    }
    getDealById(id): Observable<any> {
        
        return this.http.get(environment.baseUrl + 'dealcategory/dealbyid?id=' + id)
            .map((response: Response) => {
                return response.json();
            })
    }
    searchDealCategory(keyWord): Observable<any> {
        
        return this.http.get(environment.baseUrl + 'dealcategory/searchDealCategory?keyWord=' + keyWord)
            .map((response: Response) => {
                return response.json();
            })
    }
}