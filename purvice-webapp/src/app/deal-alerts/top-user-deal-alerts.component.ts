import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Http } from '@angular/http';
import { DealAlertsService } from './deal-alerts.service';

import * as _ from "lodash";
declare var $: any;
var that;

@Component({
   selector: 'top-user-deal-alerts',
  templateUrl: 'top-user-deal-alerts.component.html'
})
export class TopUserDealAlertsComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

   public topUserDeal: any = [];

  constructor(private route: ActivatedRoute, private http: Http, private _router: Router, private dealAlertsService: DealAlertsService, private cdref: ChangeDetectorRef) {
    that = this;
  }

  ngOnInit() {
    this.topUserDeal = [
      {id: 1,imageURL: 'https://images-na.ssl-images-amazon.com/images/I/51wF9LA-YpL.jpg', dealdName: 'walmartimages', rating: '2+', isAddedAlert: true, addDealAlertButtonName: 'Add Deal Alert'},
      {id: 2,imageURL: 'https://img.bbystatic.com/BestBuy_US/images/products/6296/6296543_sa.jpg', dealdName: 'bbystati', rating: '3+', isAddedAlert: true, addDealAlertButtonName: 'Add Deal Alert'},
      {id: 3,imageURL: 'https://images-na.ssl-images-amazon.com/images/I/51A23TjGs8L._SCLZZZZZZZ__.jpg', dealdName: 'bbystati', rating: '4', isAddedAlert: true, addDealAlertButtonName: 'Add Deal Alert'},
      {id: 4,imageURL: 'https://img.bbystatic.com/BestBuy_US/images/products/6202/6202121_sa.jpg', dealdName: 'bbystati', rating: '1+', isAddedAlert: true, addDealAlertButtonName: 'Add Deal Alert'},
      {id: 5,imageURL: 'https://images-na.ssl-images-amazon.com/images/I/41%2BIyW3ApEL.jpg', dealdName: 'bbystati', rating: '3', isAddedAlert: true, addDealAlertButtonName: 'Add Deal Alert'},
      {id: 6,imageURL: 'https://img.bbystatic.com/BestBuy_US/images/products/5855/5855918_sa.jpg', dealdName: 'walmartimages', rating: '4', isAddedAlert: true, addDealAlertButtonName: 'Add Deal Alert'},
      {id: 7,imageURL: 'https://img.bbystatic.com/BestBuy_US/images/products/6301/6301161_sa.jpg', dealdName: 'Amazon', rating: '5', isAddedAlert: true, addDealAlertButtonName: 'Add Deal Alert'},
      {id: 8,imageURL: 'https://images-na.ssl-images-amazon.com/images/I/41SO%2BU3kGtL.jpg', dealdName: 'Amazon', rating: '4', isAddedAlert: true, addDealAlertButtonName: 'Add Deal Alert'},
      {id: 9,imageURL: 'https://img.bbystatic.com/BestBuy_US/images/products/6252/6252341_sa.jpg', dealdName: 'walmartimages', rating: '3', isAddedAlert: true, addDealAlertButtonName: 'Add Deal Alert'},
      {id: 10,imageURL: 'https://img.bbystatic.com/BestBuy_US/images/products/6099/6099903_sa.jpg', dealdName: 'bbystati', rating: '2', isAddedAlert: true, addDealAlertButtonName: 'Add Deal Alert'},
    ]
  }

  addAlert(alert){
    
    for(var topDealCounter = 0; topDealCounter < that.topUserDeal.length; topDealCounter++){
      if(that.topUserDeal[topDealCounter].id == alert.id){
        that.topUserDeal[topDealCounter].addDealAlertButtonName = "Alert Added";
        that.topUserDeal[topDealCounter].isAddedAlert = false;
      }
    }
  }
}
