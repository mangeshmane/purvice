import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Http } from '@angular/http';
import { DealAlertsService } from './deal-alerts.service';

import * as _ from "lodash";
declare var $: any;
var that;

@Component({
   selector: 'deal-alerts-work',
  templateUrl: 'deal-alerts-work.component.html'
})
export class DealAlertsWorkComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  constructor(private route: ActivatedRoute, private http: Http, private _router: Router, private dealAlertsService: DealAlertsService, private cdref: ChangeDetectorRef) {
    that = this;
  }

  ngOnInit() {
   
  }
  howWork() {
    that._router.navigate(['/deal-alerts'], { relativeTo: that.activeRoute });
  }
}
