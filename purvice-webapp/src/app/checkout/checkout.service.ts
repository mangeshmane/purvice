import { Injectable, } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../services/http.service';
import { environment } from '../../environments/environment';

@Injectable()
export class CheckoutService {

  constructor(private http: HttpService) { }

  getToken() {
    return this.http.get(environment.baseUrl + 'braintree/getClientToken')
      .map((res: Response) => {
        return res.json()
      })

  }
  getOrders(id) {
    return this.http.get(environment.baseUrl + 'order/' + id)
      .map((res: Response) => {
        return res.json()
      })

  }

  placeOrder(nounce, price,postalcode) {
    return this.http.post(environment.baseUrl + 'braintree/checkouts?payment_method_nonce=' + nounce + '&amount=' + price+'&postalcode='+postalcode, {})
      .map((res: Response) => {
        return res.json()
      })

  }

  completePayment(id) {
    return this.http.get(environment.baseUrl + 'braintree/checkouts/' + id)
      .map((res: Response) => {
        return res.json()
      })

  }

  updateOrder(order) {
    return this.http.put(environment.baseUrl + 'order/update', order)
      .map((res: Response) => {
        return res.json()
      })

  }

  getCountries() {
    return this.http.get(environment.baseUrl + 'billing-address/countries')
      .map((res: Response) => {
        return res.json()
      })

  }

  getStates() {
    return this.http.get(environment.baseUrl + 'billing-address/states')
      .map((res: Response) => {
        return res.json()
      })

  }

  getCities(id) {
    return this.http.get(environment.baseUrl + 'billing-address/cities/' + id)
      .map((res: Response) => {
        return res.json()
      })
  }

  billingAddress(data) {   
    return this.http.post(environment.baseUrl + 'billing-address/create', data)
      .map((res: Response) => {
        return res.json()
      })


  }

  getBillingAddress(id){
      return this.http.get(environment.baseUrl + 'billing-address/'+id)
      .map((res: Response) => {
        return res.json()
      })    
  }

  checkoutTransaction(data){
    return this.http.post(environment.baseUrl + 'braintree/checkoutTransaction', data)
    .map((res: Response) => {
      return res.json()
    })
  }
 
  updateTransaction(data){
    return this.http.put(environment.baseUrl + 'order/updateOrder', data)
    .map((res: Response) => {
      return res.json()
    })
  }

  getGlobalOnePayURL(){
    return this.http.get(environment.baseUrl + 'payment/get')
    .map((res: Response) => {
      return res.json()
    })    
}

getGlobalOnePayData(data){
  return this.http.post(environment.baseUrl + 'braintree/checkoutTransaction', data)
  .map((res: Response) => {
    return res["_body"];
  })   
}

}
