import { Component, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { CheckoutService } from './checkout.service';
import {Md5} from 'ts-md5/dist/md5';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import * as _ from "lodash";
import { environment } from '../../environments/environment';

declare var braintree: any;
declare var paypal: any;
declare var $: any;
var that;
@Component({
	templateUrl: 'checkout.component.html'
})
export class CheckoutComponent implements OnInit {
	@BlockUI() blockUI: NgBlockUI;
	public showSuccess: boolean = false;
	public showError: boolean = false;
	public token: any;
	public integration: any;
	public orderId: string;
	public cardCheckByCardNumber: string;
	public orders: any = [];
	public order: any = {};
	public disableSubmit: boolean = false;
	public processorResponseText: string;
	public paymentSuccess: boolean = false;
	public showOrderDetail: boolean = false;
	public paymentMessage: string;
	public paymentStatus: string;
	public showCards: boolean = true;
	public userId: number;
	public showPay: boolean = false;
	public disabled = false;
	public user: any;
	public admin: any;
	public paymentType: any;
	public methodSelected: boolean = true;
	public countries: any = [];
	public states: any = [];
	public cities: any = [];
	public month: any = [];
	public yearData: any = [];
	public disableStates: boolean = true;
	public disableCities: boolean = true;
	public billingAddress: any = {};
	public cardType: any;
	public state: string;
	public showFields: boolean = false;
	public orderStatus: string;
	public checkoutTransactionData: any = {
		year:"YY",
		month:"MM"
	};
	public updateData: any = {};
	public updateresponseData: any = {};
	public xml: string = '';
	public paymentStatusErrorMessage: string;
	public globalPayErrorMessage: string;
	public globalOnePayData: any = {};
	public responseGlobal: any  = {};
	ngOnInit() { 
		this.blockUI.start('Loading...');

		this.paymentType ='card';

		for(var totalMonth = 1; totalMonth < 13; totalMonth++){
			if(totalMonth < 10){
				this.month.push('0'+totalMonth);
			}else{
				this.month.push(totalMonth);
			}
		}
		
		var dt = new Date();
		for(var totalYear = dt.getFullYear(); totalYear <= dt.getFullYear() + 50; totalYear++){
			this.yearData.push(totalYear % 100);
		}

		var user;
		   var admin;
			 try {
		        user = JSON.parse(localStorage.getItem('user'));
				admin = JSON.parse(localStorage.getItem('admin'));
		    } catch (e) {
		       user=null;
			   admin=null;
		    }
		    
		if (user)
			this.user = user;
		else if (admin)
			this.admin = admin;
		this.zone.run(() => {
			this.cdrf.detectChanges();
		});
		this.activatedRoute.params.subscribe(params => {
			this.orderId = params["orderId"];
			this.getOrders(user.id).then((data: any) => {
				this.orders = data.result;
					this.order = data.result.filter(
					order => order.id === parseInt(this.orderId))[0];
				this.showOrderDetail = true;
				this.loadPage();
			});
		});

		localStorage.removeItem("allowCheckout");
		console.log("inside onInit");
		this.userId = user.id;
		console.log(this.userId);

		this.getStates().then(data => {
			this.checkoutService.getBillingAddress(this.user.id).subscribe(data => {
				if (data.result && data.result.state) {
					this.billingAddress = data.result;
					var state = _.filter(this.states, function (o: any) {
						if (o.name == that.billingAddress.state)
							return o;
					})[0];
					this.state = state ? state.name : "California";
					that.billingAddress.zipcode = that.billingAddress.zipCode;
				}
				this.showFields = true;
				this.cdrf.detectChanges();
			})

		});

		this.getGlobalOnePayURL().then(data => {
		});

		
	}
	constructor(private _router: Router, private activatedRoute: ActivatedRoute, private checkoutService: CheckoutService, public cdrf: ChangeDetectorRef, public zone: NgZone, public http: Http) {
		that = this;
	}

	getToken() {
		return new Promise((resolve, reject) => {
			this.checkoutService.getToken().subscribe(data => {
				resolve(data);
			})
		})


	}

	getOrders(id) {
		return new Promise((resolve, reject) => {
			this.checkoutService.getOrders(id).subscribe(data => {
				resolve(data);
			})
		})

	}

	loadPage() {
		this.getToken().then((data: any) => {
			var c = this;
			var checkout;
			that.blockUI.stop();
			var button;
			
			function handleFieldEvent(event) {

				var iconType = document.getElementsByClassName("icon-type")[0];
				iconType.className = "icon-type";
				if (event.card)
					$cardNumberContainer.addClass(event.card.type);
			}
			var $cardNumberContainer = $('#card-number');
			braintree.client.create({
				authorization: data.result
			}, function (err, clientInstance) {
				if (err) {
				//	console.error(err);
					return;
				}


				braintree.hostedFields.create({
					client: clientInstance,
					styles: {
						'input': {
							'font-size': '14px',
							'font-family': 'helvetica, tahoma, calibri, sans-serif',
							'color': '#3a3a3a'
						},
						':focus': {
							'color': 'black'
						}
					},
					fields: {
						number: {
							selector: '#card-number',
							placeholder: 'Enter Card Number'
						},
						cvv: {
							selector: '#cvv',
							placeholder: 'CVV'
						},
						expirationDate: {
							selector: '#expiration-date',
							placeholder: 'Expiry Date',
							maxlength: 4
						},
					}
				}, function (err, hostedFieldsInstance) {
					if (err) {
					//	console.error(err);
						return;
					}

					hostedFieldsInstance.on('validityChange', function (event) {

						var field = event.fields[event.emittedBy];

						let allState = hostedFieldsInstance.getState();
						var formValidation = Object.keys(allState.fields).every(function (key) {
							if (allState.fields[key].isValid) {
								return allState.fields[key].isValid;
							}
						});

						if (formValidation) {

						}
						if (field.isValid) {
							if (event.emittedBy === 'expirationDate') {
								$('#expiration-date').next('div').text('');
							} else if (event.emittedBy === 'number') {
								$('#card-number').next('div').text('');
							}
							else if (event.emittedBy === 'cvv') {
								$('#cvv').next('div').text('');
							}
						} else if (field.isPotentiallyValid) {
							$(field.container).parents('.form-group').removeClass('has-warning');
							$(field.container).parents('.form-group').removeClass('has-success');
							if (event.emittedBy === 'number') {
								$('#card-number').next('div').text('');
							}
						} else {
							$(field.container).parents('.form-group').addClass('has-warning');
							if (event.emittedBy === 'number') {
								$('#card-number').next('div').text('Please Enter valid card number.');
							}
							else if (event.emittedBy === 'expirationDate') {
								$('#expiration-date').next('div').text('Please Enter valid date.');
							}
							else if (event.emittedBy === 'cvv') {
								$('#cvv').next('div').text('Please Enter valid cvv');
							}
						}
					});

					hostedFieldsInstance.on('cardTypeChange', function (event) {
						if (event.cards.length === 1) {
							$('#card-type').text(event.cards[0].niceType);
							that.cardType = event.cards[0].type;

						} else {
							that.cardType = null;
							$('#card-type').text('Card');
						}
					});

					$('#cardForm').submit(function (event) {
						event.preventDefault();
						var error = "";					
						var state = hostedFieldsInstance.getState();
						var formValid = Object.keys(state.fields).every(function (key) {
							if (state.fields[key].isValid) {
								error = error + ", " + state.fields[key];
								return state.fields[key].isValid;
							}
							else {
								if (key == "number") {
									$('#card-number').addClass('braintree-hosted-fields-invalid');
									
								}
								else if (key == "cvv") {
									$('#cvv').addClass('braintree-hosted-fields-invalid');								

								}
								else if (key == "expirationDate") {
									$('#expiration-date').addClass('braintree-hosted-fields-invalid');
									
								}
								
							}
						});


						if (formValid) {
							hostedFieldsInstance.tokenize(function (err, payload) {

								if (err) {
									//console.error(err);
									return;
								}
								that.blockUIcdrf.start('Loading...');


								var postalCode = $('#postal-code').val();
								that.billingAddress.country = "US"
								that.billingAddress.state = that.state;
								that.billingAddress.zipCode = that.billingAddress.zipcode;
								that.checkoutService.billingAddress(that.billingAddress).subscribe(response => { })
								that.placeOrder(payload.nonce, that.order.amount, postalCode).then((data) => {									
									let responseData = data ? data : { processorResponseCode: "3000" };
									that.showStatus(responseData);
								})
							});
						} else {
							$('#errorModal').modal('show')
						}

					});
				});

				braintree.paypalCheckout.create({
					client: clientInstance
				}, function (paypalCheckoutErr, paypalCheckoutInstance) {

					if (paypalCheckoutErr) {
						console.error('Error creating PayPal Checkout:', paypalCheckoutErr);
						return;
					}

					paypal.Button.render({
						env: 'sandbox', // or 'production'
						commit: true, // This will add the transaction amount to the PayPal button

						payment: function () {
							return paypalCheckoutInstance.createPayment({
								flow: 'checkout',
								amount: that.order.amount,
								currency: 'USD',
								locale: 'en_US',
							});
						},

						onAuthorize: function (data, actions) {
							return paypalCheckoutInstance.tokenizePayment(data)
								.then(function (payload) {
									var postalCode = $('#postal-code').val();
									that.blockUI.start('Loading...');
									that.placeOrder(payload.nonce, that.order.amount, postalCode).then((data) => {
										let responseData = data ? data : { processorResponseCode: "3000" };
										that.showStatus(responseData);

									})
								});
						},

						onCancel: function (data) {
						//	console.log('checkout.js payment cancelled', JSON.stringify(data));
						},

						onError: function (err) {
							//console.error('checkout.js error', err);
						}
					}, '#paypal-button').then(function () {
					});

				});

			});



			this.cdrf.detectChanges();
		})


	}

	placeOrder(nounce, price, postalCode) {
		return new Promise((resolve, reject) => {
			this.checkoutService.placeOrder(nounce, price, postalCode).subscribe(data => {
				resolve(data.result);
			})
		})
	}

	completePayment(id) {
		return new Promise((resolve, reject) => {
			this.checkoutService.completePayment(id).subscribe(data => {
				resolve(data);
			})
		})
	}

	showStatus(data) {
		switch (data.processorResponseCode.charAt(0)) {
			case "":
				that.paymentMessage = "Your transaction is failed, please try again";
				that.paymentStatus = "Failed";
				break;
			case "1":
				that.paymentMessage = "Thank you for payment, your card is added to wallet";
				that.paymentStatus = "Successful";
				break;
			case "2":
			case "3":
				that.paymentMessage = "Your transaction is failed, please try again";
				that.paymentStatus = "Failed";
				break;
			case "4":
				that.paymentMessage = "Your transaction is in progress, check your wallet after sometime";
				that.paymentStatus = "In progress";
				break;
			default:
				that.paymentMessage = "Your transaction is in progress, check your wallet after sometime";
				that.paymentStatus = "In progress";

		}

		this.updateOrder(data).then((response: any) => {
			this.showCards = false;
			that.blockUI.stop();
			this.orderStatus = response.result.tangoOrderStatus;
		})



	}

	methodSelect() {
		this.checkoutTransactionData = {
			year:"YY",
			month:"MM"
		};
		if (!this.methodSelected) {
			this.methodSelected = !this.methodSelected
		}
	}


	updateOrder(data) {
		return new Promise((resolve, reject) => {
			var order: any = {};
			order.id = this.order.id;
			order.paymentTransactionId = data.id;
			this.checkoutService.updateOrder(order).subscribe(data => {
				resolve(data);
			})
		})

	}

	gotoHome() {
		console.log("gotoHome..");
		this._router.navigate(['home']);
	}


	getStates() {
		return new Promise((resolve, reject) => {
			this.checkoutService.getStates().subscribe(data => {
				that.states = data.result;
				that.cities = [];
				this.disableStates = false;
				resolve(data);
			})
		})
	}

	getGlobalOnePayURL() {
		return new Promise((resolve, reject) => {
			this.checkoutService.getGlobalOnePayURL().subscribe(data => {
				that.globalOnePayData = data.result;
			})
		})
	}

	getGlobalOnePayData(xmldata) {
		return new Promise((resolve, reject) => {
			this.checkoutService.getGlobalOnePayData(xmldata).subscribe(data => {
				var parser = new DOMParser();
				var xmlDoc = parser.parseFromString(JSON.parse(data).result,"text/xml");
				that.updateresponseData = that.xmlToJson(xmlDoc);
			 	that.updateTransaction(that.updateresponseData);
			})
		})
	}

	getCities(id) {
		return new Promise((resolve, reject) => {
			this.checkoutService.getCities(id).subscribe(data => {
				that.cities = data.result;
				this.disableCities = false;
				resolve(data);
			})
		})
	}

	goToMyWallet() {
		this._router.navigate(['/my-wallet'], { queryParams: { userId: this.userId } });
	}

    getDate(): any{
		var date = new Date(),
		  year = date.getFullYear(),
		  month = (date.getMonth() + 1).toString(),
		  formatedMonth = (month.length === 1) ? ("0" + month) : month,
		  day = date.getDate().toString(),
		  formatedDay = (day.length === 1) ? ("0" + day) : day,
		  hour = date.getHours().toString(),
		  formatedHour = (hour.length === 1) ? ("0" + hour) : hour,
		  minute = date.getMinutes().toString(),
		  formatedMinute = (minute.length === 1) ? ("0" + minute) : minute,
		  second = date.getSeconds().toString(),
		  formatedSecond = (second.length === 1) ? ("0" + second) : second,
		  miliseconds = date.getMilliseconds().toString(),
		  formatedMiliseconds = (miliseconds.length === 1) ? ("0" + miliseconds) : miliseconds;
		return formatedDay + "-" + formatedMonth + "-" + year + ":" + formatedHour + ':' + formatedMinute + ':' + formatedSecond + ':' + formatedMiliseconds;
	  }

	prepareData(data, address): any{
		var jsonData: any = {};
		var d = new Date();
		jsonData.ORDERID = environment.globalOnePayOrderIdAppend+this.orderId+d.getTime();
		jsonData.TERMINALID = that.globalOnePayData.globalOnePayTerminalId;
		jsonData.AMOUNT = this.order.amount;
		jsonData.DATETIME = this.getDate();
		jsonData.CARDNUMBER = data.CARDNUMBER;
		jsonData.CARDNUMBER = jsonData.CARDNUMBER.replace(/ +/g, "");
		jsonData.CARDTYPE = this.cardCheckByCardNumber;
		jsonData.CARDEXPIRY = data.month+data.year;
		jsonData.CARDHOLDERNAME = data.CARDHOLDERNAME;
		var hasStr = jsonData.TERMINALID + jsonData.ORDERID + jsonData.AMOUNT + jsonData.DATETIME + that.globalOnePayData.globalOnePaySecret;
		jsonData.HASH = Md5.hashStr(hasStr);
		jsonData.CURRENCY = environment.globalOnePayCurrency;
		jsonData.TERMINALTYPE = environment.globalOnePayTerminalType;
		jsonData.TRANSACTIONTYPE = environment.globalOnePayTransactionType;
		jsonData.CVV = data.CVV;
		jsonData.ADDRESS1 = address.addressLine1;
		if(address.addressLine2 != "")
			jsonData.ADDRESS2 = address.addressLine2;
		jsonData.POSTCODE = this.billingAddress.zipcode;
		jsonData.CITY = address.city;
		jsonData.REGION = address.state;
		jsonData.COUNTRY = address.country;
		return jsonData;
	}

    StringToXML(oString): any {
		return (new DOMParser()).parseFromString(oString, "text/xml");
	}

	paymentGateway(data, address){
		if(data.CARDNUMBER != undefined && data.CVV != undefined && data.year != "YY" && data.month != "MM" && data.CARDHOLDERNAME != undefined && data.CARDHOLDERNAME!=""
		&& address.addressLine1 != "" && address.addressLine1 != undefined && address.city != "" && address.city != undefined
		&& this.state != "" && this.state != undefined && this.state != "-- Select State --" && address.zipcode != "" && address.zipcode != undefined && address.zipcode.length >= 5
		&& this.cardCheckByCardNumber != ""){	
			var jsonData = this.prepareData(data, address);
			this.xml = '<?xml version="1.0" encoding="UTF-8"?>';
			this.xml += '<PAYMENT>';
			var xmlData = this.objectToXml(jsonData);

			that.getGlobalOnePayData(xmlData);
	}else{
		  if(data.CARDNUMBER == undefined ){
			this.globalPayErrorMessage = "The card number cannot be empty.";
		  }else if(data.month == "MM"){
			this.globalPayErrorMessage = "Please select valid month.";
		  }else if(data.year == "YY"){
			this.globalPayErrorMessage = "Please select valid year.";
		  }else if(data.CVV == undefined ){
			this.globalPayErrorMessage = "The CVV cannot be empty.";
		  }else if(data.CARDHOLDERNAME == undefined || data.CARDHOLDERNAME==""){
			this.globalPayErrorMessage = "Card holder name cannot be empty.";
		  }else if (address.addressLine1 == "" || address.addressLine1 == undefined){
			this.globalPayErrorMessage = "The address cannot be empty.";
		  }else if (address.city == "" || address.city == undefined){
			this.globalPayErrorMessage = "The City cannot be empty.";
		  }else if (this.state == "" || this.state == undefined || this.state == "-- Select State --"){
			this.globalPayErrorMessage = "Please select valid State.";
		  }else if (address.zipcode == "" || address.zipcode == undefined){
			this.globalPayErrorMessage = "The ZIP Code cannot be empty.";
		  }else if (address.zipcode.length <= 5){
			this.globalPayErrorMessage = "ZIP code must be 5 or 6 digit.";
		  }else if(this.cardCheckByCardNumber == ""){
			this.globalPayErrorMessage = "Card number is invalid, only visa and mastercard allowed";
		  }
		 $('#globalPayErrorModal').modal('show');
	} 
}


cardTypeChange(checkout){
	if(checkout.CARDNUMBER!=undefined)
		this.cardValidate(checkout.CARDNUMBER);
}
    objectToXml(obj): any {
        for (var prop in obj) {
            if (!obj.hasOwnProperty(prop)) {
                continue;
            }

            if (obj[prop] == undefined)
                continue;

            this.xml += "<" + prop + ">";
            if (typeof obj[prop] == "object")
                this.xml += this.objectToXml(new Object(obj[prop]));
            else
                this.xml += obj[prop];

            this.xml += "</" + prop + ">";
        }
		this.xml += '</PAYMENT>';
        return this.xml;
	}
	
	// Changes XML to JSON
 xmlToJson(xml): any {
	
	// Create the return object
	var obj = {};

	if (xml.nodeType == 1) { // element
		// do attributes
		if (xml.attributes.length > 0) {
		obj["@attributes"] = {};
			for (var j = 0; j < xml.attributes.length; j++) {
				var attribute = xml.attributes.item(j);
				obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
			}
		}
	} else if (xml.nodeType == 3) { // text
		obj = xml.nodeValue;
	}

	// do children
	if (xml.hasChildNodes()) {
		for(var i = 0; i < xml.childNodes.length; i++) {
			var item = xml.childNodes.item(i);
			var nodeName = item.nodeName;
			if (typeof(obj[nodeName]) == "undefined") {
				obj[nodeName] = this.xmlToJson(item);
			} else {
				if (typeof(obj[nodeName].push) == "undefined") {
					var old = obj[nodeName];
					obj[nodeName] = [];
					obj[nodeName].push(old);
				}
				obj[nodeName].push(this.xmlToJson(item));
			}
		}
	}
	return obj;
};

	showStatusPayment(data,orderData) {
		var postalCode = $('#postal-code').val();
		that.billingAddress.country = "US"
		that.billingAddress.state = that.state;
		that.billingAddress.zipCode = that.billingAddress.zipcode;
		that.checkoutService.billingAddress(that.billingAddress).subscribe(response => { })

		//that.paymentStatus = data.PAYMENTRESPONSE.RESPONSETEXT["#text"];
		that.paymentStatus = orderData.paymentStatus;
		that.orderStatus = orderData.tangoOrderStatus;
		that.showCards = false;
		switch (data.PAYMENTRESPONSE.RESPONSECODE["#text"]) { 
			case "A":
				that.paymentMessage = "Thank you for payment, your card is added to wallet";
				break;
			case "E":
				that.paymentMessage = " Accepted for later processing but your card information unknown";
				break;
			case "D":
				that.paymentMessage = "Your transaction is declined, please try again";
				break;
			case "R":
				that.paymentMessage = "Your transaction is referred, please try again";
				break;
		}
	}
	updateTransaction(data){			
		this.updateData.id = this.orderId;
		if(data.PAYMENTRESPONSE != undefined){
			this.blockUI.start();
			this.updateData.paymentTransactionId = data.PAYMENTRESPONSE.UNIQUEREF["#text"];
			this.updateData.paymentProcessorResponseCode = data.PAYMENTRESPONSE.RESPONSECODE["#text"];
			this.updateData.paymentrocessorResponseText = data.PAYMENTRESPONSE.RESPONSETEXT["#text"];
			
			this.updateData.avsResponse = data.PAYMENTRESPONSE.AVSRESPONSE["#text"];
			this.updateData.cvvResponse = data.PAYMENTRESPONSE.CVVRESPONSE["#text"];
			
			return new Promise((resolve, reject) => {
				this.checkoutService.updateTransaction(this.updateData).subscribe(respons => {
					this.blockUI.stop();
					this.showStatusPayment(data, respons.result);			
				})
			})
		}else{
			if(data.ERROR.ERRORSTRING["#text"].split(":")[0]=="cvc-minLength-valid")
				that.paymentStatusErrorMessage = "Enter valid CVV.";
			else
				that.paymentStatusErrorMessage = data.ERROR.ERRORSTRING["#text"];
			$('#showPaymentStatusModal').modal('show');
		}
	}

	cvvValidate(cvv){
		if(!Number.isInteger(parseInt(cvv))){
			this.checkoutTransactionData.cvv = null;
		}
	}

	cardValidate(accountNumber){
		var firstDigit = accountNumber.charAt(0);
		accountNumber = accountNumber.replace(/ +/g, "");
		if(!Number.isInteger(parseInt(accountNumber))){
			this.checkoutTransactionData.CARDNUMBER = null;
		}
		var result = "unknown";

		if (/(^5[1-5]{1})|(^2[2-7]{1})/.test(accountNumber)){
		  result = "MASTERCARD";
		}else if (firstDigit == '4'){
		  result = "VISA";
		}// else if (/^(?:34[0-9]{13}|37[0-9]{13})$/.test(accountNumber)){
		//   result = "AMEX";
		// }else if (/^(?:2131|1800|35\d{3})\d{11}$/.test(accountNumber)){
		// 	result = "JCB";
		// }else if (/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/.test(accountNumber)){
		// 	result = "DINERS";
		// }
			this.checkCardType(result);
	}

	checkCardType(type){
		switch (type){
			case "MASTERCARD":
				this.cardCheckByCardNumber = "MASTERCARD";
				break;

			case "VISA":
			    this.cardCheckByCardNumber = "VISA";
				break;

			// case "AMEX":
			// 	this.cardCheck = "AMEX";
			// 	break;

			// case "JCB":
			// 	this.cardCheck = "JCB";
			// 	break;

			// case "DINERS":
			// 	this.cardCheck = "DINERS";
			// 	break;
				
			case "unknown":
				this.cardCheckByCardNumber = "";
				break;
			default:
		}
	}

	_keyPress(event: any, cardNumber) {
		var ignoredKeys = [8, 32];
		if(cardNumber!=undefined && event.code!="Backspace"){
			if(cardNumber.length == 4 || cardNumber.length == 9 || cardNumber.length == 14 ){
				that.checkoutTransactionData.CARDNUMBER += ' ';
			}
		 }
		if (ignoredKeys.indexOf(event.which) >=0 || (event.which >= 48 && event.which < 58) || event.which === 32 || event.which===0) {
			return true;
		} else {
			event.preventDefault();
		}
	}

	_keyPressString(event: any){
		const pattern = /^[a-zA-Z ]*$/;
		let inputChar = String.fromCharCode(event.charCode);
	
		if (!pattern.test(inputChar)) {
		  // invalid character, prevent input
		  event.preventDefault();
		}
	}
}

