import { NgModule ,} from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CheckoutRoutingModule } from './checkout-routing.module';
import { CheckoutComponent } from './checkout.component';
import { CheckoutService } from './checkout.service';
import { BlockUIModule } from 'ng-block-ui';
@NgModule({
  imports: [
    CheckoutRoutingModule,        
    HttpModule,
    FormsModule,
    CommonModule,
    BlockUIModule   
  ],
  providers:[CheckoutService],
  declarations: [CheckoutComponent] 
})
export class CheckoutModule { }

