import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullLayoutComponent } from './layouts/full-layout.component';
import { AdminLayoutComponent } from './layouts/admin-layout.component';
import { AuthGuard } from './services/auth-guard-service';
import { AdminGuard } from './services/admin-guard-service';
import { CheckoutGuard } from './services/checkout-guard-service';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'hot-deal',
    pathMatch: 'full',
  },
  {
    path: 'admin',
    component: AdminLayoutComponent,
    canActivate: [AdminGuard],
    data: {
      title: 'Admin'
    },
    children: [
      {
        path: '',
        loadChildren: './admin/admin.module#AdminModule'
      },
    ]
  },
  {
    path: 'home',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: '',
        loadChildren: './home/home.module#HomeModule'
      },
      { path: '', redirectTo: '', pathMatch: 'full' },
    ]
  },
  {
    path: 'my-wallet',
    component: FullLayoutComponent,
    data: {
      title: 'My Wallet'
    },
    children: [
      {
        path: '',
        loadChildren: './my-wallet/my-wallet.module#MyWalletModule'
      },
      { path: '', redirectTo: '', pathMatch: 'full' },
    ]
  },
  {
    path: 'hot-deal',
    component: FullLayoutComponent,
    data: {
      title: 'Hot Deal'
    },
    children: [
      {
        path: '',
        loadChildren: './hot-deal/hot-deal.module#HotDealModule'
      },
      { path: '', redirectTo: '', pathMatch: 'full' },
    ]
  },
  {
    path: 'deal-alerts',
    component: FullLayoutComponent,
    data: {
      title: 'Deal Alerts'
    },
    children: [
      {
        path: '',
        loadChildren: './deal-alerts/deal-alerts.module#DealAlertsModule'
      },
      { path: '', redirectTo: '', pathMatch: 'full' },
    ]
  },
  {
    path: 'buy-gift-card',
    component: FullLayoutComponent,
    data: {
      title: 'Buy Gift Card'
    },
    children: [
      {
        path: '',
        loadChildren: './giftcards/giftcards.module#GiftcardsModule'
      },
      { path: '', redirectTo: '', pathMatch: 'full' },
    ]
  },
  {
    path: 'login',
    data: {
      title: 'Login'
    },
    children: [
      {
        path: '',
        loadChildren: './login/user-login.module#LoginModule'
      }
    ]
  },
  {
    path: 'register',
    data: {
      title: 'Register'
    },
    children: [
      {
        path: '',
        loadChildren: './register/user-register.module#RegisterModule'
      }
    ]
  },
  {
    path: 'forgot-password',
    data: {
      title: 'Forgot Password'
    },
    children: [
      {
        path: '',
        loadChildren: './forgot-password/forgot-password.module#ForgotPasswordModule'
      }
    ]
  },
  {
    path: 'reset-password',
    data: {
      title: 'Reset Password'
    },
    children: [
      {
        path: '',
        loadChildren: './reset-password/reset-password.module#ResetPasswordModule'
      }
    ]
  },
  {
    path: 'verify/:token',
    data: {
      title: 'Verify Registeration'
    },
    children: [
      {
        path: '',
        loadChildren: './verify/verify.module#VerifyModule'
      }
    ]
  },
  {
    path: 'checkout/:orderId',
    data: {
      title: 'Checkout'
    },
    canActivate: [CheckoutGuard],
    children: [
      {
        path: '',
        loadChildren: './checkout/checkout.module#CheckoutModule'
      }
    ]
  },
  {
    path: 'my-profile',
    component: FullLayoutComponent,
    data: {
      title: 'my-profile'
    },
    children: [
      {
        path: '',
        loadChildren: './my-profile/my-profile.module#MyProfileModule'
      },
      { path: '', redirectTo: '', pathMatch: 'full' },
    ]
  },
    {
    path: '404',
    component: FullLayoutComponent,
    data: {
      title: 'Page Not Found '
    },
    children: [
      {
        path: '',
        loadChildren: './common/page-not-found/page-not-found-module#PageNotFoundModule'
      },
      { path: '', redirectTo: '', pathMatch: 'full' },
    ]
  },
  {
    path: '**', redirectTo: '/404'
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
