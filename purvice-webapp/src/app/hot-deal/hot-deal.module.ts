import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HotDealComponent } from './hot-deal.component';
import { HotDealRoutingModule } from './hot-deal-routing.module';
import { HotDealService } from './hot-deal.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    HotDealRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

  ],
  declarations: [HotDealComponent],
  providers: [
    HotDealService
  ]
})

export class HotDealModule { }
