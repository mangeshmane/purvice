import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { HotDealComponent } from './hot-deal.component';

const routes: Routes = [
  {
    path: '',
    component: HotDealComponent,
    data: {
      title: 'Hot Deal'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HotDealRoutingModule {}
