import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';
@Injectable()
export class LoginService {
	constructor(private http: Http) {
	}

	login(user) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Access-Control-Allow-Origin', '*');
		let options = new RequestOptions({ headers: headers });
		return this.http.post(environment.baseUrl + 'authenticate', JSON.stringify(user), options)
			.map((response: Response) => {
				return response.json();
			})
	}


	register(user) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Access-Control-Allow-Origin', '*');
		let options = new RequestOptions({ headers: headers });
		return this.http.post(environment.baseUrl + 'register', JSON.stringify(user), options)
			.map((res: Response) => {
				return res.json()
			})
	}

	getUser(user) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		let options = new RequestOptions({ headers: headers });
		return this.http.get(environment.baseUrl + 'get?username=' + user, options)
			.map((res: Response) => {
				return res.json()
			})
	}

	socialLogin(user) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Access-Control-Allow-Origin', '*');
		let options = new RequestOptions({ headers: headers });
		return this.http.post(environment.baseUrl + 'login', JSON.stringify(user), options)
			.map((res: Response) => {
				return res.json()
			})
	}
}