import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginService } from './user-login.service';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { CommonModule } from '@angular/common';
import { Angular2SocialLoginModule } from "angular2-social-login";
import { LoginRoutingModule } from './user-login-routing.module';
import { SharedService } from '../services/shared.service';
import { LoginComponent } from './user-login.component';
var providers = {
    "google": {
      "clientId": "317584776494-m0kljch8285v0f858hv6uo3lg5t5tpln.apps.googleusercontent.com"
    }   ,
     "facebook": {
      "clientId": "1468590919853322",
      "apiVersion": "v2.10" 
    }
  };  
Angular2SocialLoginModule.loadProvidersScripts(providers);
@NgModule({
  imports: [
    LoginRoutingModule,    
    BsDropdownModule,
    HttpModule,
    FormsModule,
    CommonModule,
    ToasterModule
  ],
  declarations: [LoginComponent],
  providers: [
    LoginService,
    ToasterService,
    SharedService
  ]
})
export class LoginModule { }

