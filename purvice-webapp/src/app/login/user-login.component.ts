import { Component, ViewChild, ChangeDetectorRef, NgZone, ApplicationRef } from '@angular/core';
import { LoginService } from './user-login.service';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from "angular2-social-login";
import { SharedService } from '../services/shared.service';
var that;
@Component({
	templateUrl: 'user-login.component.html'
})
export class LoginComponent {
	public model: any = {};
	public username: string;
	public password: string;
	public showLoader: boolean = false;
	public user: any = {};
	public showGoogleLoader: boolean;
	public showFacebookLoader: boolean;
	ngOnInit(): void {
		var user = JSON.parse(localStorage.getItem('user'));
		var admin = JSON.parse(localStorage.getItem('admin'));
		if (user) {
			this._router.navigate(['home']);
		}
		else if (admin) {
			this._router.navigate(['admin']);
		}


	}

	ngAfterViewInit() {
		this.zone.run(() => {
			this.cdrf.detectChanges();
			this.applicationRef.tick();
			console.log("login view loaded");
		});


	}

	constructor(private loginService: LoginService, private toasterService: ToasterService, private _router: Router, private activeRoute: ActivatedRoute, public _auth: AuthService, public zone: NgZone, public cdrf: ChangeDetectorRef, public applicationRef: ApplicationRef) {
		that = this;
	}

	doLogin(model) {
		this.showLoader = true;
		if (model.username != "" && model.password != "") {
			this.loginService.login(model).subscribe(data => {
				var time_now = (new Date()).getTime();
				this.showLoader = false;
				console.log("data:===", data);
				if (data.result && data.result.token != null) {
					localStorage.setItem('authorization', data.result.token);
					if (data.result.user.roles[0] == "ADMIN") {
						localStorage.setItem('admin', JSON.stringify(data.result.user));
						localStorage.setItem('logginTime', JSON.stringify(time_now))
						this._router.navigate(["../admin"], { relativeTo: this.activeRoute });
					}
					else {
						localStorage.setItem('user', JSON.stringify(data.result.user));
						let userData: any = localStorage.getItem("selectedGiftDetails");
						let userDetail: any = localStorage.getItem("selectedGift");
						if (userData) {
							userData = JSON.parse(userData);
							if (userData.categoryId) {
								this._router.navigate(['/buy-gift-card'], { queryParams: { categoryId: userData.categoryId } })

								if (userData.query) {
									this._router.navigate(['/buy-gift-card'], { queryParams: { categoryId: userData.categoryId, query: userData.query } })
									if (userData.selectedCardId)
										this._router.navigate(['/buy-gift-card'], { queryParams: { categoryId: userData.categoryId, query: userData.query, cardId: userData.selectedCardId } })
								}
								else if (userData.selectedCardId)
									this._router.navigate(['/buy-gift-card'], { queryParams: { categoryId: userData.categoryId, cardId: userData.selectedCardId } })
							}
							else if (userData.query) {
								this._router.navigate(['/buy-gift-card'], { queryParams: { query: userData.query } })
								if (userData.selectedCardId) {
									this._router.navigate(['/buy-gift-card'], { queryParams: { query: userData.query, cardId: userData.selectedCardId } })
								}
							}
							else if (userData.selectedCardId)
								this._router.navigate(['/buy-gift-card'], { queryParams: { cardId: userData.selectedCardId } })

							else {
								this._router.navigate(['/buy-gift-card'])
							}

							localStorage.removeItem("selectedGiftDetails");
						}
						else if (userDetail) {
							userDetail = JSON.parse(userDetail);
							localStorage.removeItem("selectedGift");
							this._router.navigate(['/buy-gift-card/card-details', userDetail.selectedCardId], { relativeTo: this.activeRoute });
						}
						else {
							if(JSON.parse(localStorage.getItem('dealAlert')) != null){
								this._router.navigate(['/deal-alerts'], { relativeTo: this.activeRoute });
							}else{
								this._router.navigate(['home']);
							}
						}
						localStorage.setItem('logginTime', JSON.stringify(time_now))

					}
				} else {
					this.toasterService.pop('error', data.message, data.explanation);
				}
			});
		}
	}

	gotoHome() {
		localStorage.removeItem("selectedGiftDetails");
		localStorage.removeItem("selectedGift");
		console.log("gotoHome..");
		this._router.navigate(['home']);
	}

	gotoSignup() {
		localStorage.removeItem("selectedGiftDetails");
		localStorage.removeItem("selectedGift");

		this._router.navigate(['/register']);
	}

	goToPasswordPage() {
		localStorage.removeItem("selectedGiftDetails");
		localStorage.removeItem("selectedGift");
		this._router.navigate(["../forgot-password"], { relativeTo: this.activeRoute });
	}


	signIn(provider) {

		this._auth.login(provider).subscribe(
			(data: any) => {
				provider == "google" ? this.showGoogleLoader = true : this.showFacebookLoader = true;
				that.cdrf.detectChanges();
				var fullname = data.name.split(" ");
				that.user.firstName = fullname[0];
				that.user.lastName = fullname[1];
				that.user.password = data.email ? data.email : data.name;
				that.user.username = data.uid;
				that.user.emailId = data.email ? data.email : null;
				that.user.verify = true;
				console.log(data);

				that.socialLogin(that.user).then(data => {
					if (data.result && data.result.token != null) {
						localStorage.setItem('authorization', data.result.token);
						if (data.result.user.roles[0] == "ADMIN") {
							localStorage.setItem('admin', JSON.stringify(data.result.user));
							this._router.navigate(["../admin"], { relativeTo: this.activeRoute });
						}
						else {
							localStorage.setItem('user', JSON.stringify(data.result.user));
							let userData: any = localStorage.getItem("selectedGiftDetails");
							let userDetail: any = localStorage.getItem("selectedGift");
							if (userData) {
								userData = JSON.parse(userData);
								if (userData.categoryId) {
									this._router.navigate(['/buy-gift-card'], { queryParams: { categoryId: userData.categoryId } })

									if (userData.query) {
										this._router.navigate(['/buy-gift-card'], { queryParams: { categoryId: userData.categoryId, query: userData.query } })
										if (userData.selectedCardId)
											this._router.navigate(['/buy-gift-card'], { queryParams: { categoryId: userData.categoryId, query: userData.query, cardId: userData.selectedCardId } })
									}
									else if (userData.selectedCardId)
										this._router.navigate(['/buy-gift-card'], { queryParams: { categoryId: userData.categoryId, cardId: userData.selectedCardId } })
								}
								else if (userData.query) {
									this._router.navigate(['/buy-gift-card'], { queryParams: { query: userData.query } })
									if (userData.selectedCardId) {
										this._router.navigate(['/buy-gift-card'], { queryParams: { query: userData.query, cardId: userData.selectedCardId } })
									}
								}
								else if (userData.selectedCardId)
									this._router.navigate(['/buy-gift-card'], { queryParams: { cardId: userData.selectedCardId } })

								else {
									this._router.navigate(['/buy-gift-card'])
								}

								localStorage.removeItem("selectedGiftDetails");
							}
							else if (userDetail) {
								userDetail = JSON.parse(userDetail);
								localStorage.removeItem("selectedGift");
								this._router.navigate(['/buy-gift-card/card-details', userDetail.selectedCardId], { relativeTo: this.activeRoute });

							}
							else {
								if(JSON.parse(localStorage.getItem('dealAlert')) != null){
									this._router.navigate(['/deal-alerts'], { relativeTo: this.activeRoute });
								}else{
									this._router.navigate(['home']);
								}
							}
						}
					} else {
						this.toasterService.pop('error', data.message, data.explanation);
					}
					provider == "google" ? this.showGoogleLoader = false : this.showFacebookLoader = false;
						that.cdrf.detectChanges();
				})
			}
		)
	}

	logout() {
		this._auth.logout().subscribe(
			(data) => {
			})
	}

	socialLogin(model) {
		return new Promise((resolve, reject) => {
			this.loginService.socialLogin(model).subscribe(data => {
				resolve(data);
			})
		})
	}
}
