import { Injectable, } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../services/http.service';
import { environment } from '../../environments/environment';

@Injectable()
export class LayoutService {
    constructor(private http: HttpService) {

    }
    
    
 changeStatus(data): Observable<any> {
        return this.http.put(environment.baseUrl + 'setting/update', data)
            .map((response: Response) => {
                return response.json();
            })
    }

       getVideoStatus(): Observable<any> {
        return this.http.get(environment.baseUrl + 'setting/search')
            .map((response: Response) => {
                return response.json()
            })
    }


  
}