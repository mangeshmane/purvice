import { Component, OnInit, Input, ViewChild, OnDestroy, ChangeDetectorRef, NgZone } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { SharedService } from '../services/shared.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit, OnDestroy {

  public disabled = false;
  public user: any;
  public admin: any;
  public items: any = [];
  public classes: any = [];
  public hideProfile: boolean;
  public firstName: any;
  public isValid: boolean = true;
  constructor(private sharedService: SharedService, private _router: Router, private activeRoute: ActivatedRoute, private cdrf: ChangeDetectorRef, private zone: NgZone) {

  }

  ngOnInit(): void {
    console.log("============== FullLayoutComponent =================");

    this._router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
    var user;
    var admin;
    try {
      user = JSON.parse(localStorage.getItem('user'));
      admin = JSON.parse(localStorage.getItem('admin'));
    } catch (e) {
      user = null;
      admin = null;
    }

    if (user) {
      this.user = user;
      this.firstName = this.user.firstName.length > 6 ? this.user.firstName.substring(0, 5) + '...' : this.user.firstName;
      this.user.roles[0] == "SOCIAL_USER" ? this.hideProfile = true : this.hideProfile = false;
    }
    else if (admin)
      this.admin = admin;
    this.zone.run(() => {
      this.cdrf.detectChanges();
    });

  }


  nevigateToDeals() {
    
    this._router.navigate(['/hot-deal'], { relativeTo: this.activeRoute });
   // window.location.href = environment.baseUrl + 'deals';
  }

  gotoHome() {
    console.log("gotoHome..");
    this._router.navigate(['home']);
  }

  doLogout() {
    console.log("Logout called.....");
    localStorage.removeItem("user");
    localStorage.removeItem("admin");
    this._router.navigate(['login']);
  }

  gotoGiftCardPage() {
    this._router.navigate(['./buy-gift-card'], { relativeTo: this.activeRoute });
  }

  createDeals(dealAlertsPopover) {
    this._router.navigate(['/deal-alerts'], { relativeTo: this.activeRoute }).then(nav => {
       dealAlertsPopover.popover.hide();
     }, err => {
       console.log(err) 
     });
  }

  changeOfRoutes() {
    this.classes = document.querySelector('body').classList;
    var length = this.classes.length;
    if (this.classes[length - 1] !== 'sidebar-mobile-show')
      document.querySelector('body').classList.toggle('sidebar-hidden');

  }

  ngOnDestroy() {
    this.classes = document.querySelector('body').classList;
    var length = this.classes.length;
    if (this.classes[length - 1] == 'sidebar-mobile-show')
      document.querySelector('body').classList.toggle('sidebar-mobile-show');
  }

  goToDashboard() {
    this._router.navigate(['/admin'], { relativeTo: this.activeRoute });
  }

  goToLogin() {
    this.zone.run(() => {
      this._router.navigate(['/login'], { relativeTo: this.activeRoute });
    });

  }

  updateUser(user) {
    localStorage.setItem("user", JSON.stringify(user));
    this.user = user;

    this.firstName = this.user.firstName.length > 6 ? this.user.firstName.substring(0, 5) + '...' : this.user.firstName;
    this.cdrf.detectChanges();
  }

}
