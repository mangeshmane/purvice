import { Component, OnInit, Input, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../services/shared.service';
import { LayoutService } from '../layouts/layout.service';

import * as _ from "lodash";
declare var $: any;
var that;

@Component({
  selector: 'app-admin',
  templateUrl: './admin-layout.component.html'
})
export class AdminLayoutComponent implements OnInit, OnDestroy {
  public classes: any = [];
  public videoData: any = {};

  @Input()
  public displaysideBar: boolean = true;
  message: string;
  _timer: any;
  public adminId: number;
  public admin: any;
  public videoStatus: boolean;

  constructor(private LayoutService: LayoutService, private sharedService: SharedService, private _router: Router, private activeRoute: ActivatedRoute, private cdrf: ChangeDetectorRef) {

  }

  public disabled = false;
  public status: { isopen: boolean } = { isopen: false };

  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);

  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
    console.log("toggleDropdown");

  }

  ngOnInit(): void {
    this.admin = localStorage.getItem('admin');
    this.admin = JSON.parse(this.admin);
    this.adminId = this.admin.id;
    this.UpdateVideoStatus();
    console.log(this.adminId);
  }

  // gotoHome() {
  //   console.log("gotoHome..");
  //   this._router.navigate(['dashboard']);

  // }
  // goToUser() {

 
  //   this._router.navigate(["./users"], { relativeTo: this.activeRoute });
  // }

  // goToCategories() {


  //   document.getElementById("user").style.backgroundColor = "";
  //   document.getElementById("category").style.backgroundColor = "#20a8d8";

  //   this._router.navigate(["./categories"], { relativeTo: this.activeRoute });
  // }

  doLogout() {

    console.log("Logout called.....");
    localStorage.removeItem("admin");
    this._router.navigate(['login']);
  }

  changeOfRoutes() {
    this.classes = document.querySelector('body').classList;
    var length = this.classes.length;
    if (this.classes[length - 1] !== 'sidebar-hidden')
      document.querySelector('body').classList.toggle('sidebar-hidden');

  }

  ngOnDestroy() {
    this.classes = document.querySelector('body').classList;
    var length = this.classes.length;
    if (this.classes[length - 1] !== 'sidebar-hidden')
      document.querySelector('body').classList.toggle('sidebar-hidden');
  }

  showDetails() {


    $('#VideoModal').modal('show');
  }

  addprop1(e) {

    console.log(this.videoData);

    if (e.target.checked) {
      this.videoData.id = 1;
      this.videoData.status = true;
      this.videoStatus = true;
    }
    else {
      this.videoData.id = 1;
      this.videoData.status = false;
      this.videoStatus = false;
    }

    this.LayoutService.changeStatus(this.videoData).subscribe(data => {

      console.log(data);
    });


  }

  UpdateVideoStatus() {
    this.LayoutService.getVideoStatus().subscribe(data => {
      this.videoStatus = data.result.status;
      console.log(data);
    });
  }

  updateAdmin(admin) {        
    localStorage.setItem("admin", JSON.stringify(admin));
    this.admin = admin;
    this.cdrf.detectChanges();
  }

}
