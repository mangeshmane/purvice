import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GiftcardsService } from './giftcards.service';
import { SharedService } from '../services/shared.service';
import { FormGroup, FormBuilder } from "@angular/forms";
import * as _ from "lodash";
declare var $: any;
var that;
@Component({
  templateUrl: './friend-detail.component.html'
})
export class FriendDetailComponent implements OnInit {
  public id: any;
  public selectedCard: any = [];
  public cards: any = [];
  public sub: any;
  public showCard: any = false;
  public model2: any = {};
  public model: any = {};
  public order: any;
  public orders: any;
  public firstNameStatus: boolean = true;
  public EmailStatus: boolean = true;
  public firstNameCheckoutStatus: boolean = false;
  public CheckOutStatusEmail: boolean = false;
  public pageFrom;
  public userId: number;
  public updateOrderId: number;
  public updateOrderUtId: number;
  public updateOrderDate;
  public updateOrderAmt;
  public message:any;
  form: FormGroup;

  constructor(private giftcardsService: GiftcardsService, private _router: Router, private activeRoute: ActivatedRoute, private fb: FormBuilder, private sharedService: SharedService, private cdRef: ChangeDetectorRef) {
    that = this;
  }

  ngOnInit() {
    var user = JSON.parse(localStorage.getItem('user'));
    this.sub = this.activeRoute.params.subscribe(params => {
      this.id = params['cardId'];
      this.activeRoute.queryParams.subscribe((params: Params) => {
        var that = this;
        this.userId = params['userId'];

      })
      if (this.userId) {
        this.firstNameCheckoutStatus = true;
        this.CheckOutStatusEmail = true;
        this.giftcardsService.getUserOrder(this.userId).subscribe(data => {
          this.model2 = data.result;

          for (let i = 0; i < this.model2.length; i++) {
            if (this.model2[i].id == this.id) {
              this.model = this.model2[i]; break;
            }
          }

          this.id = this.model.utid;
          this.updateOrderId = this.model.id;

          this.updateOrderUtId = this.model.utid;

          this.updateOrderDate = this.model.orderDate;
          console.log(this.model.amount);
          this.updateOrderAmt = this.model.amount;
          this.model.scheduleValue = this.model.isscheduled == true ? 'schedule' : 'instance';
          if (this.model.isscheduled) {
            $("#datepicker").datepicker("setDate", new Date(this.model.scheduledDate));
            document.getElementById("show-hide-date-picker").style.visibility = "visible";
          }
          this.cdRef.detectChanges();
          this.showData();
        })

      }
      else {
        this.showData()

      }


    });







    $("#datepicker").datepicker({ minDate: 1 });


  }

  showData() {
    this.getAllCards().then((cards) => {
      this.selectedCard = _.find(this.cards, { utid: this.id });


      this.selectedCard.selectedPrice = localStorage.getItem("cardPrice")
      this.showCard = true;
    })
  }







  getOrders(id) {
    return new Promise((resolve, reject) => {
      this.giftcardsService.getOrders(id).subscribe(data => {
        resolve(data);
      })
    })
  }

  getAllCards() {
    return new Promise((resolve, reject) => {
      this.giftcardsService.getAllCards().subscribe(data => {
        this.cards = data.result;
        resolve(data.result);
      })
    })
  }

  placeOrder(data) {
    return new Promise((resolve, reject) => {
      this.giftcardsService.createOrder(data).subscribe(data => {
        resolve(data);
      })
    })
  }

  updateOrder(data) {
    return new Promise((resolve, reject) => {
      this.giftcardsService.updateUserOrder(data).subscribe(data => {
        resolve(data);
      })
    })
  }

  goToCards() {
    this._router.navigate(['../../../buy-gift-card'], { relativeTo: this.activeRoute });
  }

  hideDatePicker() {
    document.getElementById("show-hide-date-picker").style.visibility = "visible";
  }

  showDatePicker() {
    document.getElementById("show-hide-date-picker").style.visibility = "hidden";
  }


  createOrder(model, date) {
    var temp = date;
    var ScheduleDateForFriend = temp.replace(/(\d\d)\/(\d\d)\/(\d{4})/, "$3-$1-$2");



    if (this.userId) {
      console.log("Update");

      var order2 = {
        "id": this.updateOrderId,
        "utid": this.updateOrderUtId,
        "item": null,
        "orderDate": this.updateOrderDate,
        "paymentTransactionId": null,
        "paymentStatus": " ",
        "paymentMessage": " ",
        "amount": this.updateOrderAmt,
        "orderType": "Friend",
        "scheduledDate": ScheduleDateForFriend,
        "isscheduled": model.scheduleValue == 'schedule' ? true : false,
        "friendName": model.friendName,
        "friendEmailId": model.friendEmailId,
        "friendContact": model.friendContact,
        "friendMessage": model.friendMessage,
        "emailAttempt": " ",
        "emailMessage": " "
      }

      if(order2.isscheduled&&!ScheduleDateForFriend){
         that.message ="Schedule date is compulsory ";
          $('#resultError').modal('show');
      }
      else{
      this.updateOrder(order2).then((data: any) => {
      
        if (data.result.id == 0) {
          
          that.message = data.explanation;
          $('#resultError').modal('show');
        }
        else{
        localStorage.setItem('allowCheckout', "true");
        this._router.navigate(['/checkout', data.result.id], { relativeTo: this.activeRoute });
        }
      })
      }
    }
    else {
      console.log("creare");

      var order = {
        "utid": this.selectedCard.utid,
        "paymentTransactionId": null,
        "paymentStatus": null,
        "paymentMessage": null,
        "amount": this.selectedCard.selectedPrice,
        "orderType": "Friend",
        "scheduledDate": ScheduleDateForFriend,
        "isscheduled": model.scheduleValue == 'schedule' ? true : false,
        "friendName": model.friendName,
        "friendEmailId": model.friendEmailId,
        "friendContact": model.friendContact,
        "friendMessage": model.friendMessage,
        "emailAttempt": null,
        "emailMessage": null
      }
      this.placeOrder(order).then((data: any) => {


        if (data.result.id == 0) {
          
          that.message = data.explanation;
          $('#resultError').modal('show');
        }
        else{
        localStorage.setItem('allowCheckout', "true");
        this._router.navigate(['/checkout', data.result.id], { relativeTo: this.activeRoute });
        }
 
      })
    }
    /*
       this.placeOrder(order).then((data: any) => {
       
         localStorage.setItem('allowCheckout',"true");
         this._router.navigate(['/checkout', data.id], { relativeTo: this.activeRoute });
       })*/
  }


  friendName(event: any) {
    if (event.target.value.length == 0) {
      this.firstNameStatus = false;
      this.firstNameCheckoutStatus = false;
    }
    else {
      this.firstNameStatus = true;
      this.firstNameCheckoutStatus = true;
    }
  }


  friendEmail(event: any) {
    if (event.target.value.length == 0) {
      this.EmailStatus = false;
      this.CheckOutStatusEmail = false;
    }
    else {

      var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (event.target.value.match(mailformat)) {
        this.EmailStatus = true;
        this.CheckOutStatusEmail = true;

      }
      else {
        this.EmailStatus = false;
        this.CheckOutStatusEmail = false;

      }
    }
  }

}

