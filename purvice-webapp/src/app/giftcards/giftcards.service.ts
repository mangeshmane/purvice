import { Injectable, } from '@angular/core';
import { Http, Response, } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../services/http.service';
import { environment } from '../../environments/environment';

@Injectable()
export class GiftcardsService {
    constructor(private http: HttpService) {

    }


    getAllCategories(): Observable<any> {
        return this.http.get(environment.baseUrl + 'category/all')
            .map((response: Response) => {
                return response.json()
            })
    }

    getAllCards(): Observable<any> {
        return this.http.get(environment.baseUrl + 'item/search')
            .map((response: Response) => {
                return response.json()
            })
    }

    getAllBrands(): Observable<any> {
        return this.http.get(environment.baseUrl + 'brand/search')
            .map((response: Response) => {
                return response.json()
            })
    }


    getAllCardsByParameters(search): Observable<any> {
        return this.http.get(environment.baseUrl + 'item/search?' + search)
            .map((response: Response) => {
                return response.json()
            })
    }


    createOrder(data): Observable<any> {
        return this.http.post(environment.baseUrl + 'order/create', data)
            .map((response: Response) => {
                return response.json()
            })
    }

    getOrders(id) {
        return this.http.get(environment.baseUrl + 'order/' + id)
            .map((res: Response) => {
                return res.json()
            })

    }


    getUserOrder(Userid): Observable<any> {
        return this.http.get(environment.baseUrl + 'order/' + Userid)
            .map((response: Response) => {
                return response.json();
            })
    }


    updateUserOrder(data): Observable<any> {
        return this.http.put(environment.baseUrl + 'order/update', data)
            .map((response: Response) => {
                return response.json()
            })
    }

    updateUser(data): Observable<any> {
        return this.http.put(environment.baseUrl + 'user/update-email', data)
            .map((response: Response) => {
                return response.json()
            })
    }

      getVideoStatus(): Observable<any> {
        return this.http.get(environment.baseUrl + 'setting/search')
            .map((response: Response) => {
                return response.json()
            })
    }


}  