import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { GiftcardsService } from './giftcards.service';
import { SharedService } from '../services/shared.service';
import { Injectable } from '@angular/core';

import * as _ from "lodash";
declare var $: any;
var that;
@Component({
  templateUrl: './giftcards.component.html'
})
export class GiftcardsComponent implements OnInit {
  @ViewChild('panel', { read: ElementRef }) public panel: ElementRef;
  @BlockUI() blockUI: NgBlockUI;
  public retailters: any = [];
  public categories: any = [];
  public cards: any = [];
  public brands: any = [];
  public selectedBrand: any = {};
  public selectedCategory: any = {};
  public selectedCard: any = {};
  public searchText: string;
  public selectedCategoryName: string = " ";
  public selectedBrandName: string;
  public customArray: any = [];
  public viewCards: any = [];
  public cardParent: any = {};
  public viewCardParent: any = {};
  public counter: number
  public allowDecrement: boolean = true;
  public allowIncrement: boolean = true;
  public loadWithParams: boolean = true;
  public selectedCategoryId: any;
  public querySearch: any;
  public showAll: boolean = false;
  public card: any = {};
  public prices: any = [{ price: 5 }, { price: 10 }, { price: 25 }, { price: 50 }, { price: 100 },];
  public user: any;
  public model: any = {};
  public email: any;
  public emailStatus: string;
  public VerifyemailStatus: string;
  public emailStatusMatched: boolean = true;
  public _timeout: any;
  public showLoader: boolean;
  public showResultMessage: boolean;
  public noCardAvailable: boolean;
  public cardId: any;
  public selectCategory:any;
  public openPanel:boolean=false;
  public openCard:boolean=false;
  /**
     * delete cookie
     * @param name
     */
    public deleteCookie(name) {
      this.setCookie(name, '', -1);
  }

  /**
   * get cookie
   * @param {string} name
   * @returns {string}
   */
  public getCookie(name: string) {
      const ca: Array<string> = document.cookie.split(';');
      const caLen: number = ca.length;
      const cookieName = `${name}=`;
      let c: string;

      for (let i  = 0; i < caLen; i += 1) {
          c = ca[i].replace(/^\s+/g, '');
          if (c.indexOf(cookieName) === 0) {
              return c.substring(cookieName.length, c.length);
          }
      }
      return '';
  }

  /**
   * set cookie
   * @param {string} name
   * @param {string} value
   * @param {number} expireDays
   * @param {string} path
   */
  public setCookie(name: string, value: string, expireDays: number, path: string = '') {
      const d: Date = new Date();
      d.setTime(d.getTime() + expireDays * 24 * 60 * 60 * 1000);
      const expires = `expires=${d.toUTCString()}`;
      const cpath = path ? `; path=${path}` : '';
      document.cookie = `${name}=${value}; ${expires}${cpath}`;
  }

  /**
   * consent
   * @param {boolean} isConsent
   * @param e
   * @param {string} COOKIE
   * @param {string} EXPIRE_DAYS
   * @returns {boolean}
   */
  constructor(private giftcardsService: GiftcardsService, private _router: Router, private activeRoute: ActivatedRoute, public sharedService: SharedService, private zone: NgZone, private cdrf: ChangeDetectorRef) {
    that = this;
  }

  ngOnInit() {	 
		 try {
			 this.user = JSON.parse(localStorage.getItem('user'));	
	    } catch (e) {
	    	 this.user = null;	   
	    }
   
    this.blockUI.start('Loading...');

    this.getAllCategories().then((data) => {

      this.loadWithParameters();

    });

    $('#confirmEmail').bind("cut copy paste", function (e) {
      e.preventDefault();
    });

  }

  loadWithParameters() {
    this.activeRoute.queryParams.subscribe((params: Params) => {
      var that = this;
      if (this.loadWithParams) {
        this.loadWithParams = false;
        this.selectedCategoryId = params['categoryId'];
        this.querySearch = params['query'];
        this.cardId = parseInt(params['cardId']);
        this.selectedCategory = this.categories.filter(
          category => category.id === parseInt(this.selectedCategoryId))[0];
        this.selectedCategory = this.selectedCategory ? this.selectedCategory : {};
        this.selectedCategoryName = this.selectedCategory.categoryname ? this.selectedCategory.categoryname.charAt(0).toUpperCase() + this.selectedCategory.categoryname.slice(1) : this.selectedCategory.categoryname;

        this.searchText = this.querySearch;
        this.getAllCardsWithParameters(this.selectedCategoryId, this.querySearch).then((data: any) => {
          if (that.cardId) {
            _.forEach(this.cards, function (card, index) {
              var cardFound = false;
              _.forEach(card.cardData, function (data, key) {
                if (data.id === that.cardId) {
                  that.extendCard(card, data)
                  return;
                }


              })
            })

          }


        })
        that.zone.run(() => {

          that.blockUI.stop();
          that.cdrf.detectChanges();
        });
      }

    })
  }

  getAllCategories() {
    return new Promise((resolve, reject) => {
      this.giftcardsService.getAllCategories().subscribe(data => {

        this.categories = data.result;
        _.forEach(this.categories, function (category, key) {
          category.categoryname = category.categoryname.charAt(0).toUpperCase() + category.categoryname.slice(1);
        })

        this.categories.sort(function (a, b) {
          var textA = a.categoryname.toUpperCase();
          var textB = b.categoryname.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        resolve(data.result);
        setTimeout(function(){ 
          $(".owl-carousel").owlCarousel({
              loop:false,
              margin:20,
              responsiveClass:true,
              responsive:{
                  0:{
                      items:1,
                      nav:true
                  },
                  600:{
                    autoWidth:true,
                    nav:true
                  },
                  1000:{
                    autoWidth:true,
                      nav:true
                  }
              }
          }); }, 2000);
      })
    })
  }

  getAllCards() {
    return new Promise((resolve, reject) => {

      this.giftcardsService.getAllCards().subscribe(data => {
        let arrangedCards = []
        let unique: any = [];

        _.forEach(data.result, function (card, index) {
          that.extractPrice(card);
        });

        unique = _.uniqBy(data.result, function (e: any) {
          return e.brand.brandName;
        });

        _.forEach(unique, function (card, index) {
          let childData = [];
          childData = _.filter(data.result, function (o: any) {
            if (o.brand.brandName == unique[index].brand.brandName)
              return o;
          });

          if (childData.length > 0) {
            card.childData = childData;
            card.childData.sort(this.sort)
            card.minValue = card.childData[0].fixedValue;
            card.maxValue = card.childData[card.childData.length - 1].fixedValue;
          }
        })

        this.cards = this.arrangeCards(unique, 4);
        this.viewCards = this.arrangeCards(unique, 2);

        resolve(data.result);
        
      })
    })
  }

  arrangeCards(list, size) {

    if (list && (size === 4)) {
      var matrix = [], i, k;
      for (i = 0, k = -1; i < list.length; i++) {
        if (i % size === 0) {
          k++;
          matrix[k] = {};
          matrix[k].id = "card_" + k;
          matrix[k].showCard = false;
          matrix[k].cardData = [];
        }
        list[i].showCard = false;

        matrix[k].cardData.push(list[i]);
      }

      setTimeout(function(){
        if (localStorage.getItem("myfavItems") !== null) {
          var getFavList = localStorage.getItem('myfavItems');
          getFavList = getFavList.replace(/^\s+|\s+$/gm,'');
          var getFavItems = getFavList.split(',');
          console.log(getFavItems.length);
          for(var kh=0; kh < getFavItems.length; kh++) {
            if(getFavItems[kh] != '' && getFavItems[kh] != ' ' && getFavItems[kh] != undefined) {
              var getFavItemsto = getFavItems[kh].replace(/^\s+|\s+$/gm,'');
              $('.' + getFavItemsto).find('.gift-switch').addClass('active');
            }
          }
        }
        
        
       }, 1000);
      return matrix;
    }
    else {
      var matrix = [], i, k;
      for (i = 0, k = -1; i < list.length; i++) {
        if (i % size === 0) {
          k++;
          matrix[k] = {};
          matrix[k].id = "webCard_" + k;
          matrix[k].showCard = false;
          matrix[k].cardData = [];
        }
        list[i].showCard = false;

        matrix[k].cardData.push(list[i]);
      }

      return matrix;
    }

  }


  getAllCardsWithParameters(categoryId, searchText) {
    return new Promise((resolve, reject) => {
      this.cards = [];
      this.viewCards = [];
      this.showLoader = true;
      var search = "";
      if (categoryId) {
        search = search + "categoryId=" + categoryId;
        if (searchText) {
          search = search + "&query=" + searchText;
        }
      }
      else if (searchText) {
        search = search + "query=" + searchText;
      }

      this.giftcardsService.getAllCardsByParameters(search).subscribe(data => {
        let arrangedCards = []
        let unique: any = [];
        this.noCardAvailable = false;
        this.showLoader = false;

        if (data.result == null) {
          this.showResultMessage = true;
          if (this.selectedCategory)
            this.noCardAvailable = true;
          this.cdrf.detectChanges();

          this.showAllCards(this.selectedCategory ? this.selectedCategory : '');
        }

        else {
          this.showResultMessage = false;
          this.cdrf.detectChanges();
          _.forEach(data.result, function (card, index) {
            that.extractPrice(card);
          });
          unique = _.uniqBy(data.result, function (e: any) {
            return e.brand.brandName;
          });

          _.forEach(unique, function (card, index) {
            let childData = [];
            childData = _.filter(data.result, function (o: any) {
              if (o.brand.brandName == unique[index].brand.brandName)
                return o;
            });

            if (childData.length > 0) {
              card.childData = childData;
              card.childData.sort(that.sort)
            }
          })
          _.forEach(unique, function (card, index) {

            if (card.minValue == null) {
              card.minValue = card.childData[0].fixedValue;
              card.maxValue = card.childData[card.childData.length - 1].fixedValue;
            }

          })

          this.cards = this.arrangeCards(unique, 4);
          this.viewCards = this.arrangeCards(unique, 2);
          resolve(this.cards);
        }
        this.cdrf.detectChanges();

      })
    })
  }

  routingParameters(categoryId, searchText) {
    if (categoryId) {
      this._router.navigate(['/buy-gift-card'], { queryParams: { categoryId: categoryId } })

      if (searchText) {
        this._router.navigate(['/buy-gift-card'], { queryParams: { categoryId: categoryId, query: searchText } })
      }
      else {
        this._router.navigate(['/buy-gift-card'], { queryParams: { categoryId: categoryId } })
      }
    }
    else if (searchText) {
      this._router.navigate(['/buy-gift-card'], { queryParams: { query: searchText } })
    }
    else {
      this._router.navigate(['/buy-gift-card'])
    }

  }


  setCategory(categoryname) {
    if (categoryname !== "All Categories") {
      this.showAll = false;
      this.selectedCategoryName = categoryname ? categoryname.charAt(0).toUpperCase() + categoryname.slice(1) : categoryname;
      this.selectedCategory = _.cloneDeep(this.categories.filter(
        category => category.categoryname === categoryname)[0]);

      this.getAllCardsWithParameters(this.selectedCategory.id, this.searchText);
      this.routingParameters(this.selectedCategory.id, this.searchText);
    }
    else {

      this.getAllItems();
    }
  }

  setCard(card) {
    this.selectedCard = card;
  }

  getCards(data) {
    if (this._timeout) { //if there is already a timeout in process cancel it
      window.clearTimeout(this._timeout);
    }
    this._timeout = window.setTimeout(() => {
      this._timeout = null;
      this.zone.run(() => {
        this.getAllCardsWithParameters(this.selectedCategory.id, this.searchText);
        this.routingParameters(this.selectedCategory.id, this.searchText);
      });
    }, 700);

  }
  getAllItems() {
    this.showAll = true;
    this.selectedCategoryName = "All Categories";
    this.selectedCategory = {};
    this.selectedBrandName = null;
    this.selectedBrand = {};
    this.searchText = null
    this.getAllCardsWithParameters(this.selectedCategory.id, this.searchText);
    this.routingParameters(this.selectedCategory.id, this.searchText);
  }

  extendCard(parent, child) {
    var selectPriceFlag = false;
    this.selectedCard = child;
    if(this.selectedCard.selectedPrice < this.selectedCard.minValue){
      for(var i=0; i<this.prices.length; i++){
        if(Number(this.selectedCard.minValue) <= this.prices[i].price){
             this.selectedCard.selectedPrice = this.prices[i].price;
             selectPriceFlag = true;
             break;
           }
      }
      if(!selectPriceFlag){
        this.selectedCard.selectedPrice = this.prices[0].price;
       }else if(selectPriceFlag && this.selectedCard.selectedPrice < this.prices[0].price){
         this.selectedCard.selectedPrice = this.selectedCard.minValue;
       }
    }
    if (!this.selectedCard.selectedPrice){
        for(var i=0; i<this.prices.length; i++){
          if(Number(this.selectedCard.minValue) <= this.prices[i].price){
               this.selectedCard.selectedPrice = this.prices[i].price;
               selectPriceFlag = true;
               break;
             }
        }
        if(!selectPriceFlag){
          this.selectedCard.selectedPrice = this.prices[0].price;
         }else if(selectPriceFlag && this.selectedCard.selectedPrice < this.prices[0].price){
           this.selectedCard.selectedPrice = this.selectedCard.minValue;
         }
    }
    _.forEach(this.cards, function (card) {
      card.showCard = false
    }
    )
    _.forEach(this.viewCards, function (card, index) {
      var cardFound = false;
      _.forEach(card.cardData, function (data, key) {
        if (!cardFound) {
          if (data.id === child.id) {
            card.showCard = true;
            that.viewCardParent = card;
            cardFound = true
            return;
          }

          else
            card.showCard = false;
        }
      })
    })
    parent.showCard = true;
    that.cardParent = parent;
    if (this.selectedCard.fixedValue) {
      this.selectedCard.selectedPrice = this.selectedCard.childData[0].fixedValue;
      this.counter = this.selectedCard.selectedPrice ? this.selectedCard.selectedPrice : this.selectedCard.childData[0].fixedValue;
    }
    else
      this.counter = this.changeCounter(this.selectedCard.minValue, this.selectedCard.maxValue)
    this.cdrf.detectChanges();
    $('html, body').animate({
      scrollTop: $('#' + parent.id).offset().top
    }, 500);

    
    $('#accordion .collapse').collapse('hide');
    
    $('#accordionCard .collapse').collapse('hide');
  }

  extendViewCard(parent, child) {

    this.selectedCard = child;
    if (!this.selectedCard.selectedPrice)
      this.selectedCard.selectedPrice = this.prices[0].price;
    _.forEach(this.cards, function (card) {
      var cardFound = false;
      _.forEach(card.cardData, function (data, key) {
        if (!cardFound) {
          if (data.id === child.id) {
            card.showCard = true;
            that.cardParent = card;
            cardFound = true
            return;
          }
          else
            card.showCard = false;
        }
      })
    })

    _.forEach(this.viewCards, function (card) {
      card.showCard = false
    })
    parent.showCard = true;
    that.viewCardParent = parent;
    if (this.selectedCard.fixedValue) {
      this.selectedCard.selectedPrice = this.selectedCard.childData[0].fixedValue;
      this.counter = this.selectedCard.selectedPrice ? this.selectedCard.selectedPrice : this.selectedCard.childData[0].fixedValue;
    }
    else
      this.counter = this.changeCounter(this.selectedCard.minValue, this.selectedCard.maxValue)
    this.cdrf.detectChanges();
    $('html, body').animate({
      scrollTop: $('#' + parent.id).offset().top
    }, 500);

 
    $('#accordion .collapse').collapse('hide');    
    $('#accordionCard .collapse').collapse('hide');
  }

  hideDetails() {
    this.viewCardParent.showCard = false
    this.cardParent.showCard = false;
    this.openPanel=false;
    this.openCard=false;
  }

  goToDetail(card) {
    //  localStorage.setItem("cardPrice", this.selectedCard.selectedPrice)
    this._router.navigate(['./card-details', card.id], { relativeTo: this.activeRoute });

  }

  goToFriend(card) {   
    if (this.user) {
      this.sharedService.setGiftPrice(this.selectedCard.selectedPrice);
      if (this.selectedCard.valueType == "FIXED_VALUE") {
        this.card = this.selectedCard.childData.filter(
          card => card.fixedValue === this.selectedCard.selectedPrice)[0];
          if(this.card==undefined)
            this.card = card;
      }
      else {
        this.card = card;
      }
      // var order = this.getOrder(this.card);
      // this.placeOrder(order).then((data: any) => {
      localStorage.setItem("cardPrice", this.selectedCard.selectedPrice)
      this._router.navigate(['./friend-detail', this.card.utid], { relativeTo: this.activeRoute });
      // })
    }
    else {
      let data: any = {};
      data.categoryId = this.selectedCategory ? this.selectedCategory.id : null;
      data.query = this.searchText ? this.searchText : null;
      data.selectedCardId = this.selectedCard ? this.selectedCard.id : null;
      localStorage.setItem("selectedGiftDetails", JSON.stringify(data));
      this._router.navigate(['/login',], { relativeTo: this.activeRoute });
    }


  }

  changeCounter(min, max) {

    var result = max / min
    var count = 5;
    // if (result <= 100)
    //   count = 5;
    // else if (100 < result && result <= 250)
    //   count = 10;
    // else if (250 < result && result <= 500)
    //   count = 25;
    // else if (500 < result && result <= 1000)
    //   count = 50;
    // else if (1000 < result)
    //   count = 100;

    return count;
  }

  incrementPrice() {

    if (this.selectedCard.fixedValue) {
      if (this.selectedCard.selectedPrice < this.selectedCard.maxValue) {
        var index = _.findIndex(this.selectedCard.childData, { fixedValue: this.selectedCard.selectedPrice });
        this.allowIncrement = false;
        if (index !== this.selectedCard.childData.length)
          this.selectedCard.selectedPrice = this.selectedCard.childData[index + 1].fixedValue;
      }
      else {
        this.allowIncrement = true;
      }
    }
    else {
      if (this.selectedCard.selectedPrice !== this.prices[this.prices.length - 1].price) {
        var value = _.findIndex(this.prices, { price: this.selectedCard.selectedPrice });
        this.allowIncrement = false;
        this.selectedCard.selectedPrice = this.prices[value + 1].price;
      }
      else {
        //  this.selectedCard.selectedPrice=this.selectedCard.maxValue;
        this.allowIncrement = true;
      }
    }
  }
  decrementPrice() {
    if (this.selectedCard.fixedValue) {
      if (this.selectedCard.selectedPrice > this.selectedCard.minValue) {
        var index = _.findIndex(this.selectedCard.childData, { fixedValue: this.selectedCard.selectedPrice });
        this.allowDecrement = false;
        if (index !== this.selectedCard.childData.length)
          this.selectedCard.selectedPrice = this.selectedCard.childData[index - 1].fixedValue;
      }
      else {
        this.allowDecrement = true;
      }
    }
    else {
      if (this.selectedCard.selectedPrice !== this.prices[0].price) {
        var value = _.findIndex(this.prices, { price: this.selectedCard.selectedPrice });
        this.allowDecrement = false;
        this.selectedCard.selectedPrice = this.prices[value - 1].price;
      }
      else {
        this.allowDecrement = true;
      }
    }
  }

  extractPrice(card) {
    if (card.minValue == null) {
      var value = card.rewardName.substr(card.rewardName.indexOf("$") + 1);
      card.rewardName = card.rewardName.substring(0, card.rewardName.indexOf('$'));
      card.fixedValue = parseInt(value);
      return card;
    }
    else
      return card;
  }

  sort(a, b) {

    if (a.fixedValue < b.fixedValue)
      return -1;
    if (a.fixedValue > b.fixedValue)
      return 1;
    return 0;
  }

// 
  getOrder(card) {
    var order = {
      "utid": card.utid,
      "paymentTransactionId": null,
      "paymentStatus": null,
      "paymentMessage": null,
      "amount": card.selectedPrice ? card.selectedPrice : card.fixedValue,
      "orderType": "Self",
      "isscheduled": false,
      "friendName": null,
      "friendEmailId": null,
      "friendContact": null,
      "friendMessage": null,
      "emailAttempt": null,
      "emailMessage": null,
      "recipient": null
    }
    return order;
  }
  placeOrder(data) {
    return new Promise((resolve, reject) => {
      this.giftcardsService.createOrder(data).subscribe(data => {
        resolve(data.result);
      })
    })
  }


  buyForSelf(card) {
    if (this.user) {
      this.sharedService.setGiftPrice(this.selectedCard.selectedPrice);
      if (this.selectedCard.valueType == "FIXED_VALUE") {
        this.card = this.selectedCard.childData.filter(
          card => card.fixedValue === this.selectedCard.selectedPrice)[0];
          if(this.card==undefined)
            this.card = card;
      }
      else {
        this.card = card;
      }
      if (this.user.roles[0] == "SOCIAL_USER" && !this.user.emailId)
        this.showEmailModal();
      else
        this.submit(this.user);

    }
    else {
      let data: any = {};
      data.categoryId = this.selectedCategory ? this.selectedCategory.id : null;
      data.query = this.searchText ? this.searchText : null;
      data.selectedCardId = this.selectedCard ? this.selectedCard.id : null;
      localStorage.setItem("selectedGiftDetails", JSON.stringify(data));
      this._router.navigate(['/login',], { relativeTo: this.activeRoute });
    }
  }

  showEmailModal() {
    $('#emailModal').modal('show');
  }
  submit(form) {
    var order = this.getOrder(this.card);
    if (this.user.roles[0] == "SOCIAL_USER" && !this.user.emailId) {
      order.recipient = this.user.emailId ? this.user.emailId : this.model.email;
      this.giftcardsService.updateUser({ id: this.user.id, emailId: this.user.emailId ? this.user.emailId : this.model.email }).subscribe(data => {
        localStorage.setItem("user", JSON.stringify(data.result))
        this.user = JSON.parse(localStorage.getItem('user'));

      })
    }
    else if (this.user.roles[0] == "SOCIAL_USER")
      order.recipient = this.user.emailId;
    else
      order.recipient = this.user.username;
        this.placeOrder(order).then((data: any) => {
          localStorage.setItem("cardPrice", this.selectedCard.selectedPrice)
          localStorage.setItem('allowCheckout', "true");
          this._router.navigate(['/checkout', data.id], { relativeTo: this.activeRoute });
        })

    this.user = {};
  }

  close(form) {
    this.model = {};
    this.email = form;
    this.email.resetForm();
  }

  getEmailStatus(event: any) {
    this.emailStatus = event.target.value;
    if (this.VerifyemailStatus != undefined) {
      if (this.emailStatus != this.VerifyemailStatus) {
        this.emailStatusMatched = false;

      }
      else {
        this.emailStatusMatched = true;

      }
    }

  }

  getVerifyEmailStatus(event: any) {

    this.VerifyemailStatus = event.target.value;
    if (this.emailStatus != this.VerifyemailStatus) {
      this.emailStatusMatched = false;
    }
    else {
      this.emailStatusMatched = true;
    }

  }
  showAllCards(category) {
    let search = "";
    this.noCardAvailable = false;
    if (category.id)
      search = search + "categoryId=" + category.id;
    this.giftcardsService.getAllCardsByParameters(search).subscribe(data => {
      let arrangedCards = []
      let unique: any = [];
      if (data.result == null)
        this.noCardAvailable = true;
      this.showLoader = false;
      _.forEach(data.result, function (card, index) {
        that.extractPrice(card);
      });
      unique = _.uniqBy(data.result, function (e: any) {
        return e.brand.brandName;
      });

      _.forEach(unique, function (card, index) {
        let childData = [];
        childData = _.filter(data.result, function (o: any) {
          if (o.brand.brandName == unique[index].brand.brandName)
            return o;
        });

        if (childData.length > 0) {
          card.childData = childData;
          card.childData.sort(that.sort)
        }
      })
      _.forEach(unique, function (card, index) {

        if (card.minValue == null) {
          card.minValue = card.childData[0].fixedValue;
          card.maxValue = card.childData[card.childData.length - 1].fixedValue;
        }

      })

      this.cards = this.arrangeCards(unique, 4);
      this.viewCards = this.arrangeCards(unique, 2);



    })
    this.cdrf.detectChanges();
  }

}


