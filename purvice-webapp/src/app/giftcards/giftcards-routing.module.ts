import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GiftcardsComponent } from './giftcards.component';
import { FriendDetailComponent } from './friend-detail.component'
import { GiftcarddetailsComponent } from './giftcard-details.component';


const routes: Routes = [
  {
    path: '',
    component: GiftcardsComponent,

    data: {
      title: 'Giftcards'
    }
  },
{
    path: 'friend-detail/:cardId', 
    component: FriendDetailComponent,    
    data: {
      title: 'FriendDetail'
    }
  },
 
  {
    path: 'card-details/:cardId',
    component: GiftcarddetailsComponent,

    data: {
      title: 'GiftcardsDetails'
    }
  },

  {
    path: '**', redirectTo: '/404'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GiftcardsRoutingModule { }
