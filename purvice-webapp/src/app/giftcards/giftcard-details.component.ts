import { Component, OnInit, ChangeDetectorRef, } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { GiftcardsService } from './giftcards.service';
import { environment } from '../../environments/environment';
import { SharedService } from '../services/shared.service';
import * as _ from "lodash";
var that;
declare var $: any;
@Component({
  templateUrl: './giftcard-details.component.html'
})

export class GiftcarddetailsComponent implements OnInit {
  id: number;
  private sub: any;
  public Carddetail: any = [];
  public cards: any = [];
  public selectedCard: any = {};
  public feturedCards: any = [];
  public showDetails: boolean = false;
  public cdRef: any = ChangeDetectorRef;
  public allowDecrement: boolean = true;
  public allowIncrement: boolean = true;
  public counter: number;
  public card: any = {};
  public videoLink: any = environment.videoUrl;
  public user: any;
  public model: any = {};
  public videoStatus: boolean;
  public VerifyemailStatus: string;
  public emailStatusMatched: boolean = true;
  public emailStatus: string;
  public emailForm: any;
  public openPanel:boolean=false;
  public prices: any = [{ price: 5 }, { price: 10 }, { price: 25 }, { price: 50 }, { price: 100 }];
  constructor(private giftcardsService: GiftcardsService, private _router: Router, private activeRoute: ActivatedRoute, private route: ActivatedRoute, private sharedService: SharedService) {
    that = this;

  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['cardId'];
      this.getAllCards().then((data: any) => {
        let unique: any = [];
        let selectedCard = _.find(data.result, { id: this.id });
        if (!selectedCard) {
          this._router.navigate(['/buy-gift-card'], { relativeTo: this.activeRoute });
        }


        _.forEach(data.result, function (card, index) {
          that.extractPrice(card);
        });
        unique = _.uniqBy(data.result, function (e: any) {
          return e.brand.brandName;
        });

        _.forEach(unique, function (card, index) {
          let childData = [];
          childData = _.filter(data.result, function (o: any) {
            if (o.brand.brandName == unique[index].brand.brandName)
              return o;
          });

          if (childData.length > 0) {
            card.childData = childData;
            card.childData.sort(that.sort)
          }
        })
        _.forEach(unique, function (card, index) {

          if (card.minValue == null) {
            card.minValue = card.childData[0].fixedValue;
            card.maxValue = card.childData[card.childData.length - 1].fixedValue;
          }

        })

        this.cards = unique;
        this.selectedCard = _.find(this.cards, { id: this.id });

        if (this.selectedCard.fixedValue) {
          this.selectedCard.selectedPrice = this.selectedCard.childData[0].fixedValue;
          this.counter = this.selectedCard.selectedPrice ? this.selectedCard.selectedPrice : this.selectedCard.childData[0].fixedValue;
        }
        else {
          this.selectedCard.selectedPrice = 5;
          this.counter = this.changeCounter(this.selectedCard.minValue, this.selectedCard.maxValue)
          this.cdRef();
        }

        let filteredCards = _.filter(this.cards, function (o: any) {
          if (o.id != that.id)
            return o;
        });
        this.feturedCards = filteredCards.slice(0, 4);
        this.showDetails = true;
        this.cdRef();
      })
    });

    this.UpdateVideoStatus();
  }

  getAllCards() {
    return new Promise((resolve, reject) => {
      this.giftcardsService.getAllCards().subscribe((data) => {
        resolve(data);
        
      })
    })
  }

  htmlToPlaintext(html) {
    //return html ? String(html).replace(/<[^>]+>/gm, '') : '';
    html = String(html).replace(/<[^>]+>/gm, '');
    var entities = [
      ['amp', '&'],
      ['apos', '\''],
      ['#x27', '\''],
      ['#x2F', '/'],
      ['#39', '\''],
      ['#47', '/'],
      ['lt', '<'],
      ['gt', '>'],
      ['nbsp', ' '],
      ['quot', '"']
    ];

    for (var i = 0, max = entities.length; i < max; ++i) 
      html = html.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);
    return html;
    //return html ? String(html).replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '') : '';
  }

  extractPrice(card) {
    if (card.minValue == null) {
      var value = card.rewardName.substr(card.rewardName.indexOf("$") + 1);
      card.rewardName = card.rewardName.substring(0, card.rewardName.indexOf('$'));
      card.fixedValue = parseInt(value);
      return card;
    }
    else
      return card;
  }



  changeCounter(min, max) {

    var result = max / min
    var count = 5;
    return count;
  }

  // incrementPrice() {

  //   if (this.selectedCard.fixedValue) {
  //     if (this.selectedCard.selectedPrice < this.selectedCard.maxValue) {
  //       var index = _.findIndex(this.selectedCard.childData, { fixedValue: this.selectedCard.selectedPrice });
  //       this.allowIncrement = false;
  //       if (index !== this.selectedCard.childData.length)
  //         this.selectedCard.selectedPrice = this.selectedCard.childData[index + 1].fixedValue;
  //     }
  //     else {
  //       this.allowIncrement = true;
  //     }
  //   }
  //   else {
  //     if ((this.selectedCard.selectedPrice + this.counter) < this.selectedCard.maxValue) {
  //       this.allowIncrement = false;
  //       this.selectedCard.selectedPrice += this.counter;
  //     }
  //     else {
  //       this.allowIncrement = true;
  //     }
  //   }
  // }

  incrementPrice() {

    if (this.selectedCard.fixedValue) {
      if (this.selectedCard.selectedPrice < this.selectedCard.maxValue) {
        var index = _.findIndex(this.selectedCard.childData, { fixedValue: this.selectedCard.selectedPrice });
        this.allowIncrement = false;
        if (index !== this.selectedCard.childData.length)
          this.selectedCard.selectedPrice = this.selectedCard.childData[index + 1].fixedValue;
      }
      else {
        this.allowIncrement = true;
      }
    }
    else {
      if (this.selectedCard.selectedPrice !== this.prices[this.prices.length - 1].price) {
        var value = _.findIndex(this.prices, { price: this.selectedCard.selectedPrice });
        this.allowIncrement = false;
        this.selectedCard.selectedPrice = this.prices[value + 1].price;
      }
      else {
        //  this.selectedCard.selectedPrice=this.selectedCard.maxValue;
        this.allowIncrement = true;
      }
    }
  }
  decrementPrice() {
    if (this.selectedCard.fixedValue) {
      if (this.selectedCard.selectedPrice > this.selectedCard.minValue) {
        var index = _.findIndex(this.selectedCard.childData, { fixedValue: this.selectedCard.selectedPrice });
        this.allowDecrement = false;
        if (index !== this.selectedCard.childData.length)
          this.selectedCard.selectedPrice = this.selectedCard.childData[index - 1].fixedValue;
      }
      else {
        this.allowDecrement = true;
      }
    }
    else {
      if (this.selectedCard.selectedPrice !== this.prices[0].price) {
        var value = _.findIndex(this.prices, { price: this.selectedCard.selectedPrice });
        this.allowDecrement = false;
        this.selectedCard.selectedPrice = this.prices[value - 1].price;
      }
      else {
        this.allowDecrement = true;
      }
    }
  }
  // decrementPrice() {
  //   if (this.selectedCard.fixedValue) {
  //     if (this.selectedCard.selectedPrice > this.selectedCard.minValue) {
  //       var index = _.findIndex(this.selectedCard.childData, { fixedValue: this.selectedCard.selectedPrice });
  //       this.allowDecrement = false;
  //       if (index !== this.selectedCard.childData.length)
  //         this.selectedCard.selectedPrice = this.selectedCard.childData[index - 1].fixedValue;
  //     }
  //     else {
  //       this.allowDecrement = true;
  //     }
  //   }
  //   else {
  //     if ((this.selectedCard.selectedPrice - this.counter) > this.selectedCard.minValue) {
  //       this.allowDecrement = false;
  //       this.selectedCard.selectedPrice -= this.counter;
  //     }
  //     else {
  //       this.allowDecrement = true;
  //     }
  //   }
  // }


  sort(a, b) {

    if (a.fixedValue < b.fixedValue)
      return -1;
    if (a.fixedValue > b.fixedValue)
      return 1;
    return 0;
  }



  goToFriend(card) {    
    if (this.user) {
      this.sharedService.setGiftPrice(this.selectedCard.selectedPrice);
      if (this.selectedCard.valueType == "FIXED_VALUE") {
        this.card = this.selectedCard.childData.filter(
          card => card.fixedValue === this.selectedCard.selectedPrice)[0];
          if(this.card==undefined)
            this.card = card;
      }
      else {
        this.card = card;
      }
      var order = this.getOrder(this.card);
      //   this.placeOrder(order).then((data: any) => {
      localStorage.setItem("cardPrice", this.selectedCard.selectedPrice)
      this._router.navigate(['../../friend-detail', this.card.utid], { relativeTo: this.activeRoute });
      //  })

    }
    else {
      let data: any = {};
      data.selectedCardId = this.selectedCard ? this.selectedCard.id : null;
      localStorage.setItem("selectedGift", JSON.stringify(data));
      this._router.navigate(['/login'], { relativeTo: this.activeRoute });
    }
  }



  goToDetail(card) {
    this._router.navigate(['../../card-details', card.id], { relativeTo: this.activeRoute });

  }

  open() {
    $('#promotion-video').attr("src", this.videoLink);
    $('#youTubePlayer').modal('show');
  }

  placeOrder(data) {
    return new Promise((resolve, reject) => {
      this.giftcardsService.createOrder(data).subscribe(data => {
        resolve(data.result);
      })
    })
  }



  getOrder(card) {
    var order = {
      "utid": card.utid,
      "paymentTransactionId": null,
      "paymentStatus": null,
      "paymentMessage": null,
      "amount": card.selectedPrice ? card.selectedPrice : card.fixedValue,
      "orderType": "Self",
      "isscheduled": false,
      "friendName": null,
      "friendEmailId": null,
      "friendContact": null,
      "friendMessage": null,
      "emailAttempt": null,
      "emailMessage": null,
      "recipient": null
    }
    return order;
  }



  buyForSelf(card) {
    
    if (this.user) {
      this.sharedService.setGiftPrice(this.selectedCard.selectedPrice);
      if (this.selectedCard.valueType == "FIXED_VALUE") {
        this.card = this.selectedCard.childData.filter(
          card => card.fixedValue === this.selectedCard.selectedPrice)[0];
          if(this.card==undefined)
            this.card = card;
      }
      else {
        this.card = card;
      }
      if (this.user.roles[0] == "SOCIAL_USER" && !this.user.emailId)
        this.showEmailModal();
      else
        this.submit(this.user);

    }
    else {
      let data: any = {};
      data.selectedCardId = this.selectedCard ? this.selectedCard.id : null;
      localStorage.setItem("selectedGift", JSON.stringify(data));
      this._router.navigate(['/login',], { relativeTo: this.activeRoute });
    }
  }
  showEmailModal() {
    $('#emailModal').modal('show');
  }
  submit(form) {
    var order = this.getOrder(this.card);
    if (this.user.roles[0] == "SOCIAL_USER" && !this.user.emailId) {
      order.recipient = this.user.emailId ? this.user.emailId : this.model.email;
      this.giftcardsService.updateUser({ id: this.user.id, emailId: this.user.emailId ? this.user.emailId : this.model.email }).subscribe(data => {
        localStorage.setItem("user", JSON.stringify(data.result))
        this.user = JSON.parse(localStorage.getItem('user'));

      })
    }
    else if (this.user.roles[0] == "SOCIAL_USER")
      order.recipient = this.user.emailId;
    else
      order.recipient = this.user.username;

    this.placeOrder(order).then((data: any) => {
      localStorage.setItem("cardPrice", this.selectedCard.selectedPrice)
      localStorage.setItem('allowCheckout', "true");
      this._router.navigate(['/checkout', data.id], { relativeTo: this.activeRoute });
    })

    if (form.resetForm) {
      this.emailForm = form;
      this.emailForm.resetForm();
    }
    this.user = {};
    this.model = {};
  }

  UpdateVideoStatus() {
    this.giftcardsService.getVideoStatus().subscribe(data => {
      if(data.result!=null)
        this.videoStatus = data.result.status;
      console.log(data);
    });
  }


  getEmailStatus(event: any) {
    this.emailStatus = event.target.value;
    if (this.VerifyemailStatus != undefined) {
      if (this.emailStatus != this.VerifyemailStatus) {
        this.emailStatusMatched = false;

      }
      else {
        this.emailStatusMatched = true;

      }
    }

  }

  getVerifyEmailStatus(event: any) {

    this.VerifyemailStatus = event.target.value;
    if (this.emailStatus != this.VerifyemailStatus) {
      this.emailStatusMatched = false;
    }
    else {
      this.emailStatusMatched = true;
    }

  }
  close(form) {
    this.model = {};
    this.emailForm = form;
    this.emailForm.resetForm();
  }

}

