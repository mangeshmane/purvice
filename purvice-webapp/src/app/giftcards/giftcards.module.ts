import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { GiftcardsComponent } from './giftcards.component';
import { GiftcarddetailsComponent } from './giftcard-details.component';
import { GiftcardsService } from './giftcards.service';
import { GiftcardsRoutingModule } from './giftcards-routing.module';
import { FriendDetailComponent } from './friend-detail.component';

@NgModule({
  imports: [
    GiftcardsRoutingModule,
    CommonModule,
    FormsModule    
  ],
  providers: [
    GiftcardsService, FormBuilder
  ],
  declarations: [GiftcardsComponent, FriendDetailComponent, GiftcarddetailsComponent]
})
export class GiftcardsModule { }
