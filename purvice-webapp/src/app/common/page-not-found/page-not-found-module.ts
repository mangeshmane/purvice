import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FormBuilder } from '@angular/forms';

import { NotFoundComponent } from './page-not-found-component';
import { PageNotFoundRoutingModule } from './page-not-found-routing.module';
@NgModule({
  imports: [
    PageNotFoundRoutingModule,

  ],

  declarations: [NotFoundComponent]
})
export class PageNotFoundModule { }
