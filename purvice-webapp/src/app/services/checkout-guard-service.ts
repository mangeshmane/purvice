import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
@Injectable()

export class CheckoutGuard implements CanActivate {

    auth: any = {};

    constructor(private router: Router) {

    }

    canActivate() {        
        let checkout = localStorage.getItem('allowCheckout');
        if (checkout != null) {
            return true;
        }
        else {
          this.router.navigate(['/buy-gift-card']);
          //  return true;
        }


    }
}