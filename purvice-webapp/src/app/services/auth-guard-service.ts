import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
@Injectable()

export class AuthGuard implements CanActivate {

    auth: any = {};

    constructor(private router: Router) {

    }

    canActivate() {        
        let token = localStorage.getItem('authentication');	
        if (token != null) {     
            return true;
        }
        else {
          return false;
        }

    }
}