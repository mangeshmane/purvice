import { Injectable, OnInit, Injector } from '@angular/core';
import { Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { Router } from "@angular/router";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

var toasterData;
@Injectable()
export class HttpService extends Http implements OnInit {
  public router;


  ngOnInit() {

  }
  constructor(backend: XHRBackend, options: RequestOptions, router: Router) {

    super(backend, options);    
    this.router = router

  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    let token = localStorage.getItem('authorization');

    if (typeof url === 'string') {
      if (!options) {
        options = { headers: new Headers() };
      }
      options.headers.set('Authorization', 'Bearer ' + token);
    } else {

      url.headers.set('Authorization', 'Bearer ' + token);
    }
    return super.request(url, options).catch(this.catchAuthError(this.router));
  }

  private catchAuthError(router) {
    var error: any;
    return (res: Response) => {
      
      if (res.status === 401 || res.status === 403) {
        localStorage.removeItem("user");
        localStorage.removeItem("admin");
        router.navigate(['/login']);

        console.log(res);
      }
      else if (res.status === 404) {

      }
      else if (res.status === 405) {

      }
      return Observable.throw(res);
    };
  }
}