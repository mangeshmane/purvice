import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
@Injectable()

export class AdminGuard implements CanActivate {

    auth: any = {};

    constructor(private router: Router) {

    }

    canActivate() {                  
        let admin = localStorage.getItem('admin');	
        if (admin != null) {     
            return true;
        }
        else {
          return false;
        }

    }
}