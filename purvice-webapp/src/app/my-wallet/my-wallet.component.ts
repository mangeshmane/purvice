import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import { MyWalletService } from './my-wallet.service';
import * as _ from "lodash";
declare var $: any;
var that;
@Component({
  templateUrl: 'my-wallet.component.html'
})
export class MyWalletComponent implements OnInit {
  public totalGiftCards: number;
  public id: number;
  public totalAmmount: number;
  public orders: any = {};
  public ordersdumy: any = {};
  public orderData: any = {};
  public walletpage: number = 0;
  public searchStrng: string;
  public orderedCards: any;
  public filteredcards: any = [];
  public refreshedcards: any = [];
  public search: any;
  public linkSatus: boolean = true;
  public showLoader: boolean
  public showResultMessage: boolean;
  @BlockUI() blockUI: NgBlockUI;

  constructor(private route: ActivatedRoute, private http: Http, private MyWalletService: MyWalletService, private _router: Router, private cdref: ChangeDetectorRef) {
    that = this;
  }

  ngOnInit() {
    this.totalAmmount = 0;

    this.blockUI.start('Loading...');


    this.route
      .queryParams
      .subscribe(params => {
        this.id = params['userId'];
        this.loadData(this.id);


      });


  }

  loadData(id) {
    var amt;
    this.MyWalletService.getOrders(id).subscribe(data => {

      this.filteredcards = _.filter(data.result, function (item: any) {
        return item.tangoOrderStatus != "Cancelled";
      })


      this.orders.result = this.filteredcards.reverse();;
      console.log(this.orders);
      this.totalGiftCards = this.orders.result.length;
      this.orderedCards = this.orders.result;

      for (let i = 0; i < this.orders.result.length; i++) {
        this.totalAmmount = this.totalAmmount + this.orders.result[i].amount;

      }
      this.totalAmmount.toFixed(2);
      if (this.searchStrng)
        this.searchItem("event", this.searchStrng);

      this.blockUI.stop();
      this.cdref.detectChanges();

    })
  }





  goToFriend(order) {
    this.walletpage = 1;
    localStorage.setItem("cardPrice", order.amount);
    this._router.navigate(['./buy-gift-card/friend-detail', order.id], { queryParams: { userId: this.id } });
  }



  goToCardList() {
    this._router.navigate(['./buy-gift-card/']);
  }

  searchItem(event: any, search) {
    this.showLoader = true;
    this.searchStrng = search;
    this.searchStrng = this.searchStrng.charAt(0).toUpperCase() + this.searchStrng.slice(1);


    if (search.length != 0) {

      this.orders.result = _.filter(this.orderedCards, function (o: any) {

        return o.item.brand.brandName.indexOf(that.searchStrng) !== -1;
      });

    }
    else {
      this.orders.result = this.orderedCards;
    }
    if (this.orders.result.length == 0)
      this.showResultMessage = true;
    else
      this.showResultMessage = false;
    this.showLoader = false;

  }

  deleteOrder(orderId) {


    this.MyWalletService.removeOrder(this.orderData, orderId).subscribe(data1 => {

      if (data1.message == "SUCCESS") {
        this.loadData(this.id);
      }

    })





  }

  goToCheckout(data) {
    localStorage.setItem('allowCheckout', "true");
    this._router.navigate(['/checkout', data.id], { relativeTo: this.route });
  }



}
