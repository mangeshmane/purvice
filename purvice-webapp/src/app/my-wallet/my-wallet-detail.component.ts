import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import * as _ from "lodash";
import { MyWalletService } from './my-wallet.service';

declare var $: any;
var that;
@Component({
  templateUrl: 'my-wallet-detail.component.html'
})
export class MyWalletDetailComponent implements OnInit {

  public id: number;
  public orders: any = {};
  public orderId: number;
  public cardValue: number;
  public cardName: string;
  public cardImg: string;
  public orderType: string;
  public orderStatus: string;
  public payStatus: string;
  public orderid: number;
  public friendName: string;
  public friendEmail: string;
  public scheduleDate: string;
  public friendStatus: boolean;
  public scheduleDateStatus: boolean;
  public itemShortDescriotion: string;
  public order: any = {};
  public orderDate: any;
  public codeName: string;
  public claimCode: string;
  public redemptionUrl: any;
  public codeNumber: string;
  public code: string;

  // @BlockUI() blockUI: NgBlockUI;

  constructor(private route: ActivatedRoute, private http: Http, private MyWalletService: MyWalletService) {
    that = this;
  }

  ngOnInit() {
    console.log("Insode of Walletg details");
    this.route
      .queryParams
      .subscribe(params => {
        this.id = params['userId'];
        this.orderId = params['orderId'];
        this.MyWalletService.getOrders(this.id).subscribe(data => {
          this.orders = data;

          that.order = _.filter(this.orders.result, function (o: any) {
            return o.id == that.orderId;
          })[0];
          console.log(this.order);
          this.cardValue = this.order.amount;
          this.cardName = this.order.item.brand.brandName;
          this.cardImg = this.order.item.brand.imageUrls[5].imageURL;
          this.orderType = this.order.orderType;
          this.orderStatus = this.order.tangoOrderStatus;
          this.payStatus = this.order.paymentStatus;
          this.orderid = this.order.id;
          this.friendName = this.order.friendName;
          this.friendEmail = this.order.friendEmailId;
          this.scheduleDate = this.order.scheduledDate;
          this.itemShortDescriotion = this.order.item.brand.shortDescription;
          if (_.includes(this.order.claimCode, "URL")) {            
            that.codeName =this.order.claimCode.substring(0, this.order.claimCode.indexOf("http"));
            let url = this.order.claimCode.substring(this.order.claimCode.indexOf(":") + 2);
            this.redemptionUrl = url.substring(0, url.indexOf(" "));
            url = url.substring(url.indexOf(" "));
            let urlName = this.redemptionUrl.length > 40 ? this.redemptionUrl.substring(0, 40) + '...' : this.redemptionUrl;            
            this.order.claimCode = that.codeName + ' ' + '<a href=' + this.redemptionUrl + ' target="_blank">' +urlName + ' </a>' + url;
            //   this.order.claimCode.split(that.redemptionUrl).join('<a href='+that.redemptionUrl+'>'+that.redemptionUrl+' </a>');
            //     that.claimCode = this.order.claimCode;
            //     that.code = "Code Number";

            //     that.codeNumber = that.order.claimCode.substring(that.order.claimCode.lastIndexOf(":")+2);
          }
          // if (this.order.claimCode) {
          //   if (_.includes(this.order.claimCode, "Redemption Code")) {
          //     this.codeName = "Redemption Code";
          //     this.claimCode = this.order.claimCode.substring(this.order.claimCode.indexOf(":") + 2);
          //   }
          //   else if (_.includes(this.order.claimCode, "Redemption URL")) {
          //     this.codeName = "Redemption URL";
          //     this.redemptionUrl = this.order.claimCode.substring(this.order.claimCode.indexOf(":") + 2);
          //     this.claimCode = this.redemptionUrl.length > 40 ? this.redemptionUrl.substring(0, 40) + '...' : this.redemptionUrl;
          //   }
          //   else if (_.includes(this.order.claimCode, "PIN")) {
          //     that.codeName = "PIN";
          //     that.claimCode = this.order.claimCode;
          //     that.code = "Code Number";
          //     that.codeNumber = that.order.claimCode.substring(that.order.claimCode.lastIndexOf(":")+2);
          //   }
          //   else {
          //     this.codeName = "Claim Code";
          //     this.claimCode = this.order.claimCode.substring(this.order.claimCode.indexOf(":") + 2);

          //   }
          // }

          var tmp = document.getElementById("desrcpton");
          tmp.innerHTML = this.itemShortDescriotion;
          tmp.textContent || tmp.innerText;
          var date = new Date(this.scheduleDate);
          this.scheduleDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
          date = new Date(this.order.orderDate);
          this.orderDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();


          if (this.orderType == "Friend") {
            this.friendStatus = true;
          }
          else {
            this.friendStatus = false;
          }

          if (this.order.scheduledDate == null) {
            this.scheduleDateStatus = false;
          }
          else {
            this.scheduleDateStatus = true;
          }
        })

      });
  }



}
