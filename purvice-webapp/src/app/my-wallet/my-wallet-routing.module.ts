import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';

import { MyWalletComponent } from './my-wallet.component';
import { MyWalletDetailComponent } from './my-wallet-detail.component';
const routes: Routes = [
  {
    path: '',
    component: MyWalletComponent,
    data: {
      title: 'My Wallet'
    }
  },
  {
    path: 'wallet-detail',
    component: MyWalletDetailComponent,
    data: {
      title: 'My Wallet Detail'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyWalletRoutingModule {}
