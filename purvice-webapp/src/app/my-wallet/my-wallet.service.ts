import { Injectable, } from '@angular/core';
import { Http, Response, } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpService } from '../services/http.service';
import { environment } from '../../environments/environment';

@Injectable()
export class MyWalletService {
    constructor(private http: HttpService) {

    }
    
    getOrders(Userid): Observable<any> {
        return this.http.get(environment.baseUrl + 'order/' + Userid)
            .map((response: Response) => {
                return response.json();
            })
    }
    
    removeOrder(data,orderId): Observable<any> {
        return this.http.put(environment.baseUrl + 'order/'+orderId,data)
            .map((response: Response) => {
                return response.json();
            })
    } 

}