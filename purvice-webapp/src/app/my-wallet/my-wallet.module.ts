import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyWalletComponent } from './my-wallet.component';
import { MyWalletDetailComponent } from './my-wallet-detail.component';
import { MyWalletRoutingModule } from './my-wallet-routing.module';
import { MyWalletService } from './my-wallet.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    MyWalletRoutingModule,
    CommonModule,
    FormsModule
  ],
  declarations: [MyWalletComponent, MyWalletDetailComponent],
  providers: [
    MyWalletService
  ]
})
export class MyWalletModule { }
