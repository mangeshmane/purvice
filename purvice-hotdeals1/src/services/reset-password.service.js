import Env from '../environments/environment.js';
import axios from 'axios';

export const resetPasswordService = {
    check,
    updateUser
};

function updateUser(token,data){
return axios.post(Env.baseUrl + '/resetPassword?token='+ token,data,{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*"
      },
    })
    .then((response) => {
        return response.data;
    })
}

function check(token){
return axios.get(Env.baseUrl + '/resetPassword?token='+ token,{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*"
      },
    })
    .then((response) => {
        return response.data;
    })
}
