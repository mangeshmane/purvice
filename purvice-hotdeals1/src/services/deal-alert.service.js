
import Env from '../environments/environment.js';
import axios from 'axios';
import cookie from 'react-cookies';


export const dealAlertService = {
    getAllDealCategorys,
    getDealCategoryByParentId,
    searchDealCategory,
    dealAlertSave,
    getUserDealAlert,
    deleteDealAlert,
    pauseDealAlert,
    editDealAlert,
    getAllDealsByKeyword,
    getDealById,
    getDealCategoriesWithChild,
    getDealAlertById,
    getDealCategoriesByName,
    dealAlertSaveByShared
};


function getAllDealCategorys(){
return axios.get(Env.baseUrl + '/dealcategory/getAllDealCategorys',{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*"
      },
    })
    .then((response) => {
        return response.data;
    })
}

  function getDealCategoryByParentId(data){
    return axios.get(Env.baseUrl + '/dealcategory/getDealCategoryByParentId?parentId='+data,{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*"
      },
    })
    .then((response) => {
       return response.data;
    });
}

 function getDealById(id) {

    return axios.get(Env.baseUrl + '/dealcategory/dealbyid?id=' + id,{
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
        }).then((response) => {
           return response.data;
        });
}


function searchDealCategory(data){
return axios.get(Env.baseUrl + '/dealcategory/searchDealCategory?keyWord='+data,{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*"
      },
    })
    .then((response) => {
        return response.data;
    })
}

function dealAlertSave(data){
return axios.post(Env.baseUrl + '/dealalert/dealalertsave',data,{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*",
        "Authorization":"Bearer "+cookie.load("token"),
      },
    })
    .then((response) => {
        return response.data;
    })
}

function dealAlertSaveByShared(data){
    return axios.post(Env.baseUrl + '/dealalert/dealalertsavebysharedalert',data,{
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin":"*",
            "Authorization":"Bearer "+cookie.load("token"),
          },
        })
        .then((response) => {
            return response.data;
        })
    }

function getUserDealAlert(){
return axios.get(Env.baseUrl + '/dealalert/userdealalert?userId='+cookie.load("userId"),{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*",
        "Authorization":"Bearer "+cookie.load("token"),
      },
    })
    .then((response) => {
        return response.data;
    })
}

function deleteDealAlert(id){
return axios.get(Env.baseUrl + '/dealalert/delete?dealAlertId='+id,{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*",
        "Authorization":"Bearer " + cookie.load("token"),
      },
    })
    .then((response) => {
        return response.data;
    })
}

function getAllDealsByKeyword(param){
	let paramString = "";
	let categoryId ='';
	let tempString = ''
	Object.keys(param).forEach(function(key) {
		let value = param[key];
			if(value!= null && value!==""){
				if(paramString.length > 0){
					paramString += "&";
				}
				paramString += (key + "=" + value);
				if(key !=='pageNumber' && key !== 'pageSize') {
					tempString += (key + "=" + value);
				}
			}
	});
	return axios.get(Env.baseUrl + '/dealcategory/allsearch?=-firstSeen&'+paramString, {
		headers: {
            "Content-Type": "application/json",
            "Authorization":"Bearer " + cookie.load("token"),
            "Access-Control-Allow-Origin":"*",
			},
		})
		.then((response) => {
				return response.data;
		})
}

function pauseDealAlert(id){
return axios.get(Env.baseUrl + '/dealalert/pause?dealAlertId='+id,{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*",
        "Authorization":"Bearer "+cookie.load("token"),
      },
    })
    .then((response) => {
        return response.data;
    })
}

function editDealAlert(data){
return axios.put(Env.baseUrl + '/dealalert/updateDealAlert',data,{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*",
        "Authorization":"Bearer "+cookie.load("token"),
      },
    })
    .then((response) => {
        return response.data;
    })
}

function getDealCategoriesWithChild(){
    return axios.get(Env.baseUrl + '/dealcategory/allDealCategory',{
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
        })
        .then((response) => {
            return response.data;
        })
    }

    function getDealAlertById(dealAlertId){
        return axios.get(Env.baseUrl + '/dealalert/fetchbydealalertid?dealAlertId=' + dealAlertId, {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin":"*",
                "Authorization":"Bearer "+cookie.load("token"),
              },
            })
            .then((response) => {
                return response.data;
            })
        }
    
        function getDealCategoriesByName(categoryName){
            return axios.get(Env.baseUrl + '/dealcategory/getDealCategoryByName?categoryName=' + categoryName,{
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin":"*",
                    "Authorization":"Bearer "+cookie.load("token"),
                  },
                })
                .then((response) => {
                    return response.data;
                })
            }
