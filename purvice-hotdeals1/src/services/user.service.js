import Env from '../environments/environment.js';
import axios from 'axios';

export const userService = {
    login,
    logout,
    socialLogin,
};


function login(user){
return axios.post(Env.baseUrl + '/authenticate',user,{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*"
      },
    })
    .then((response) => {
        return response.data;
    })
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                window.location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}

function socialLogin(user){
return axios.post(Env.baseUrl + '/login',user,{
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*"
      },
    })
    .then((response) => {
        return response.data;
    })
}
