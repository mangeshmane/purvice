import ENV from '../environments/environment.js';
import axios from 'axios';
import cookie from 'react-cookies';

export const hotDealService = {
    getDeals,
    getDealsByIds,
	getAllDealCategorys,
	saveDealFeedback,
	getDealFeedbacks,
	getDealBySearchText,
	getDealCategoryByPriority,
	sendDealFeedback,
	getScraperDeals
};

function getScraperDeals(param){
	let paramString = "";
	let categoryId ='';
	let tempString = ''
	Object.keys(param).forEach(function(key) {
		let value = param[key];
		
		if(key == 'categoryId'){
			categoryId = value
		}else{
			if(value!= null && value!==""){
				if(paramString.length > 0){
					paramString += "&";
				}
				paramString += (key + "=" + value);
				if(key !=='pageNumber' && key !== 'pageSize') {
					tempString += (key + "=" + value);
				}
			}
		}
	});
	if(param.frmSearchParam != null && param.frmSearchParam !==undefined && param.frmSearchParam !==""){
	return fetch(`${ENV.baseUrl}/deals/globalsearchbyscrap?${paramString}`).then(response => {
		return response.json();
	})
}
}


function getDeals(param) {
	let paramString = "";
	let categoryId ='';
	let tempString = ''
	Object.keys(param).forEach(function(key) {
		let value = param[key];
		
		if(key == 'categoryId'){
			categoryId = value
		}else{
			if(value!= null && value!==""){
				if(paramString.length > 0){
					paramString += "&";
				}
				paramString += (key + "=" + value);
				if(key !=='pageNumber' && key !== 'pageSize') {
					tempString += (key + "=" + value);
				}
			}
		}
	});
	if(tempString!= ''){
		window.history.pushState({}, null, "#/hotdeals?"+tempString);
	}else{
		window.history.pushState({}, null, "#/hotdeals");
	}
	if(param.frmSearchParam != null && param.frmSearchParam !==undefined && param.frmSearchParam !==""){
		return fetch(`${ENV.baseUrl}/deals/globalsearch?${paramString}`).then(response => {
	    	return response.json();
			})
	} if(param.ratingFilterData != null && param.ratingFilterData !==undefined && param.ratingFilterData !==""){
		return axios.get(ENV.baseUrl + '/deals/dealsort?'+paramString,{
			headers: {
				"Content-Type": "application/json",
				"Access-Control-Allow-Origin":"*"
			  },
			})
			.then((response) => {
				return response.data;
			})
	} else{
		if(categoryId !==''){
			let deal = {};
			deal.category = categoryId;
			window.history.pushState({}, null, "#/hotdeals?categories="+categoryId);
			return axios.post(ENV.baseUrl + '/deals/getDealsByCategories?sortingCriteria=-firstSeen&'+paramString,deal,{
				headers: {
					"Content-Type": "application/json",
					"Access-Control-Allow-Origin":"*"
				  },
				})
				.then((response) => {
					return response.data;
				})
		}else{
			return axios.get(ENV.baseUrl + '/deals?sortingCriteria=-firstSeen&'+paramString,{
				headers: {
					"Content-Type": "application/json",
					"Access-Control-Allow-Origin":"*"
				  },
				})
				.then((response) => {
					return response.data;
				})
		}
		
	}
}

function getDealsByIds(list) {
	return fetch(`${ENV.baseUrl}/deals/fetchbyid?dealId=${list}`).then(response => {
    	return response.json();
    })
}

function getAllDealCategorys(){
	return axios.get(ENV.baseUrl + '/dealcategory/allDealCategory',{
	    headers: {
	        "Content-Type": "application/json",
	        "Access-Control-Allow-Origin":"*"
	      },
	    })
	    .then((response) => {
	        return response.data;
	    })
}

function saveDealFeedback (query, deal) {
	return axios.put(ENV.baseUrl + '/deals/updateDeal?'+query,deal,{
	    headers: {
	        "Content-Type": "application/json",
			"Access-Control-Allow-Origin":"*",
			"Authorization":"Bearer "+cookie.load("token"),
	      },
	    })
	    .then((response) => {
	        return response.data;
	    })
}

function getDealFeedbacks (userId) {
	return axios.get(ENV.baseUrl + '/dealfeedbac/dealFeedback?userId='+userId,{
	    headers: {
	        "Content-Type": "application/json",
			"Access-Control-Allow-Origin":"*",
			"Authorization":"Bearer "+cookie.load("token"),
	      },
	    })
	    .then((response) => {
	        return response.data;
	    })
}

function getDealBySearchText(param){
	let paramString = "";
	let categoryId ='';
	let tempString = ''
	Object.keys(param).forEach(function(key) {
		let value = param[key];
			if(value!= null && value!==""){
				if(paramString.length > 0){
					paramString += "&";
				}
				paramString += (key + "=" + value);
				if(key !=='pageNumber' && key !== 'pageSize') {
					tempString += (key + "=" + value);
				}
			}
	});
	return axios.get(ENV.baseUrl + '/deals/fetchbytextsearch?=-firstSeen&'+paramString, {
		headers: {
				"Content-Type": "application/json",
		"Access-Control-Allow-Origin":"*",
			},
		})
		.then((response) => {
				return response.data;
		})
}

function getDealCategoryByPriority() {
	return axios.get(ENV.baseUrl + '/dealcategory/getDealCategoryByPriority',{
	    headers: {
	        "Content-Type": "application/json",
			"Access-Control-Allow-Origin":"*",
	      },
	    })
	    .then((response) => {
	        return response.data;
	    })
}
function sendDealFeedback(data){
	return axios.post(ENV.baseUrl + '/deals/saveDealFeedback',data,{
		headers: {
			"Content-Type": "application/json",
			"Access-Control-Allow-Origin":"*"
		  },
		})
		.then((response) => {
			return response.data;
		})
    }