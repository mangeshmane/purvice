import Env from '../environments/environment.js';
import axios from 'axios';

export const verifyService = {
    verifyToken
};

function verifyToken(data) {
    return axios.get(Env.baseUrl + '/verifyEmail/'+data.token,{
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
    }).then((response) => {
            return response.data;
    })
}
