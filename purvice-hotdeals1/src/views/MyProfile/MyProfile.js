import React, { Component } from 'react';
import { Card, CardGroup, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Col, Row, Nav, NavItem, NavLink, TabContent, TabPane, Button } from 'reactstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import classnames from 'classnames';
import cookie from 'react-cookies';

import './MyProfile.css';
import './../HotDeals/HotDeals.css';

import { myProfileService } from '../../services/my-profile.service';

var divStyle = {
    color: "red",
    marginTop: "0%"
  };

class MyProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeTab: '1',
            firstName: '',
            lastName: '',
            userName: '',
            userId: '',
            currentPass: '',
            newPass: '',
            confirmPass: '',
            hideProfile: false,
            passwordMatch: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleLastChange = this.handleLastChange.bind(this);
        this.handleCurrentPassChange = this.handleCurrentPassChange.bind(this);
        this.handleNewPassChange = this.handleNewPassChange.bind(this);
        this.handleConfirmPassChange = this.handleConfirmPassChange.bind(this); 
        this.userLoad = this.userLoad.bind(this);
        this.getUser = this.getUser.bind(this);
        this.Update = this.Update.bind(this);
        this.updatePassword = this.updatePassword.bind(this);
        this.userLoad();
        this.getUser();
        
    }

    getUser(){
        myProfileService.getUser(this.state.userId).then(userData => {
            this.setState({firstName:userData.result.firstName});
            this.setState({lastName:userData.result.lastName});
            this.setState({userName:userData.result.username});
        })
    }

    userLoad(){
        var user = cookie.load("user");
        if(user !== undefined){
            this.state.userId = user.id;
            user.roles[0] == "SOCIAL_USER" ? this.state.hideProfile = true : this.state.hideProfile = false;
        }
    }

    handleConfirmPassChange(e) {
        this.setState({confirmPass: e.target.value});
        if(this.state.newPass != e.target.value)
            this.state.passwordMatch = true;
        else
            this.state.passwordMatch = false;
    }

    handleNewPassChange(e) {
        this.setState({newPass: e.target.value});
    }

    handleCurrentPassChange(e) {
        this.setState({currentPass: e.target.value});
    }

    handleLastChange(e) {
        this.setState({lastName: e.target.value});
    }

    handleChange(e) {
        this.setState({firstName: e.target.value});
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab,
            });
        }
    }

    updatePassword(e){
        var data = {
            oldPassword: this.state.currentPass,
            newPassword: this.state.newPass,
            username: this.state.userName 
          }
        myProfileService.updatePassword(data).then(user => {
            if(user.message == "SUCCESS")
                toast.success(user.explanation, { position: toast.POSITION.TOP_RIGHT });
            else
                toast.error(user.explanation, { position: toast.POSITION.TOP_RIGHT })
        })
    }

    Update(e){
        var user = cookie.load("user");
        var data = {
            createdDate: user.createdDate,
            emailId: user.emailId,
            firstName: this.state.firstName,
            id: this.state.userId,
            lastName: this.state.lastName,
            passwordResetToken: null,
            roles: user.roles,
            token: user.token,
            updateDate: user.updateDate,
            username: user.username,
            verify: true
          }
        myProfileService.updateUser(data).then(user => {
            if(user.message == "SUCCESS")
                toast.success(user.explanation, { position: toast.POSITION.TOP_RIGHT });
            else
                toast.error(user.explanation, { position: toast.POSITION.TOP_RIGHT })
        })
    }

    render() {
        return (
            <div className="mt-3 align-items-center">
                <ToastContainer autoClose={2500} />
                <Container>
                    <Row className="justify-content-center">
                        <Col lg="6" md="6">
                            <CardGroup>
                                <Card className="p-3">
                                    <h5 className="mb-4">Update Profile</h5>
                                   {!this.state.hideProfile &&
                                    <Nav tabs>
                                        <NavItem>
                                            <NavLink
                                                className={classnames({ active: this.state.activeTab === '1' })}
                                                onClick={() => { this.toggle('1'); }}>
                                                Profile Information
                                                        </NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink
                                                className={classnames({ active: this.state.activeTab === '4' })}
                                                onClick={() => { this.toggle('4'); }}>
                                                Password Information
                                                         </NavLink>
                                        </NavItem>
                                    </Nav>
                                   }

                                    {this.state.hideProfile &&
                                    <Nav tabs>
                                        <NavItem>
                                            <NavLink
                                                className={classnames({ active: this.state.activeTab === '1' })}
                                                onClick={() => { this.toggle('1'); }}>
                                                Profile Information
                                                        </NavLink>
                                        </NavItem>
                                    </Nav>
                                   }

                                {!this.state.hideProfile &&
                                    <TabContent activeTab={this.state.activeTab}>
                                        <TabPane tabId="1">
                                            {/* <Form onSubmit={this.Update}> */}
                                                <InputGroup className="mb-4">
                                                    <InputGroupAddon addonType="prepend">
                                                        <InputGroupText>
                                                           First Name
                                                        </InputGroupText>
                                                    </InputGroupAddon>
                                                    <Input type="text" name="fname" placeholder="First Name" value={this.state.firstName} onChange={this.handleChange}/>
                                                </InputGroup>
                                                <InputGroup className="mb-4">
                                                    <InputGroupAddon addonType="prepend">
                                                        <InputGroupText>
                                                           Last Name
                                                        </InputGroupText>
                                                    </InputGroupAddon>
                                                    <Input type="text" name="lname" placeholder="Last Name" value={this.state.lastName} onChange={this.handleLastChange}/>
                                                </InputGroup>
                                                <InputGroup className="mb-4">
                                                    <InputGroupAddon addonType="prepend">
                                                        <InputGroupText>
                                                           User Name
                                                        </InputGroupText>
                                                    </InputGroupAddon>
                                                    <Input type="text" name="uname" placeholder="User Name" value={this.state.userName} readOnly />
                                                </InputGroup>
                                                <InputGroup className="mb-4" style={{ width: '36%' }}>
                                                    <Button color="primary" block style={{ fontWeight: '600' }} onClick = {this.Update}>Update</Button>
                                                </InputGroup>
                                            {/* </Form> */}
                                        </TabPane>

                                        <TabPane tabId="4">
                                        {/* <Form onSubmit={this.Update}> */}
                                                <InputGroup className="mb-4">
                                                    <InputGroupAddon addonType="prepend">
                                                        <InputGroupText>
                                                           Current Password
                                                        </InputGroupText>
                                                    </InputGroupAddon>
                                                    <Input type="password" name="currentPass" placeholder="Current Password" value={this.state.currentPass} onChange={this.handleCurrentPassChange} />
                                                </InputGroup>
                                                <InputGroup className="mb-4">
                                                    <InputGroupAddon addonType="prepend">
                                                        <InputGroupText>
                                                            New   Password    
                                                        </InputGroupText>
                                                    </InputGroupAddon>
                                                    <Input type="password" name="newPass" placeholder="New Password" value={this.state.newPass} onChange={this.handleNewPassChange} />
                                                </InputGroup>
                                                <InputGroup className="mb-4">
                                                    <InputGroupAddon addonType="prepend">
                                                        <InputGroupText>
                                                          Confirm Password
                                                        </InputGroupText>
                                                    </InputGroupAddon>
                                                    <Input type="password" name="confirmPass" placeholder="Confirm Password" value={this.state.confirmPass} onChange={this.handleConfirmPassChange} />
                                                </InputGroup>
                    {this.state.passwordMatch && 
                         <InputGroup className="mb-4">
                         <InputGroupAddon addonType="prepend">
                             <InputGroupText style={divStyle}>
                             Password did not match
                             </InputGroupText>
                         </InputGroupAddon>
                     </InputGroup>
                      }
                                                <InputGroup className="mb-4" style={{ width: '36%' }}>
                                                    <Button color="primary" block style={{ fontWeight: '600' }} onClick = {this.updatePassword}>Update</Button>
                                                </InputGroup>
                                            {/* </Form> */}
                                         </TabPane>
                                    </TabContent>
                                }

                                    {this.state.hideProfile &&
                                    <TabContent activeTab={this.state.activeTab}>
                                        <TabPane tabId="1">
                                            {/* <Form onSubmit={this.Update}> */}
                                                <InputGroup className="mb-4">
                                                    <InputGroupAddon addonType="prepend">
                                                        <InputGroupText>
                                                           First Name
                                                        </InputGroupText>
                                                    </InputGroupAddon>
                                                    <Input type="text" name="fname" placeholder="First Name" value={this.state.firstName} onChange={this.handleChange}/>
                                                </InputGroup>
                                                <InputGroup className="mb-4">
                                                    <InputGroupAddon addonType="prepend">
                                                        <InputGroupText>
                                                           Last Name
                                                        </InputGroupText>
                                                    </InputGroupAddon>
                                                    <Input type="text" name="lname" placeholder="Last Name" value={this.state.lastName} onChange={this.handleLastChange}/>
                                                </InputGroup>
                                                <InputGroup className="mb-4">
                                                    <InputGroupAddon addonType="prepend">
                                                        <InputGroupText>
                                                           User Name
                                                        </InputGroupText>
                                                    </InputGroupAddon>
                                                     <Input type="text" name="uname" placeholder="User Name" value={this.state.userName} readOnly />  
                                                </InputGroup>
                                                <InputGroup className="mb-4" style={{ width: '36%' }}>
                                                    <Button color="primary" block style={{ fontWeight: '600' }} onClick = {this.Update}>Update</Button>
                                                </InputGroup>
                                            {/* </Form> */}
                                        </TabPane>
                                    </TabContent>
                                }

                                </Card>

                            </CardGroup>
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }

}

export default MyProfile;
