import React from 'react';
import { dealAlertService } from '../../services/deal-alert.service';
import '../DealAlert/dealAlert.css';
import HotDealCard from './HotDealCard';
import { Tree } from 'primereact/tree';
import { TreeSelect } from 'antd';
import { Col } from 'reactstrap';
import { hotDealService } from '../../services/hot-deal.service';

import AliceCarousel from 'react-alice-carousel';
//import "react-alice-carousel/lib/alice-carousel.css";

var hotDealNavBarRef;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;

class HotDealNavBar extends React.Component {

	constructor(props) {
		super(props);
		hotDealNavBarRef = this;
		this.toggle = this.toggle.bind(this);
		this.onRemoveFavDealClick = this.onRemoveFavDealClick.bind(this);

		this.state = {
			collapse: false,
			favDeals: [],
			categories: [],
			allCategories :[],
			loadingFavContent: false,
			isTreeShow: false,
			isStoreShow: false,
			isratingFilterShow: false,
			expandedKeys: {},
			value: [],
			selectedNodeKey: [],
			selectedStoreNodeKey: [],
			selectedRatingNodeKey: [],
			categoryFilter: [],
			storeFilter: [],
			ratingFilterData: [],
			categoryLength: 0,
			time: {
				minutes: 30,
				seconds: 0
			},
			store: [
				{ label: 'Amazon', value: 'www.amazon.com,amazon.com', key: 1 },
				{ label: 'Barnes & Nobles', value: 'www.barnesandnoble.com,barnesandnoble.com', key: 2 },
				{ label: 'Best Buy', value: 'www.bestbuy.com,api.bestbuy.com', key: 3 },
				{ label: 'Forever21', value: 'www.forever21.com,forever21.com', key: 4 },
				{ label: 'HalfPriceBooks', value: 'www.hpb.com,hpb.com', key: 5 },
				{ label: 'Macys', value: 'www.macys.com', key: 6 },
				{ label: 'Nordstrom', value: 'shop.nordstrom.com,www.nordstrom.com', key: 7 },
				{ label: 'Target', value: 'www.target.com', key: 8 },
				{ label: 'Walmart', value: 'www.walmart.com,c.affil.walmart.com', key: 9 },
				{ label: 'Zappos', value: 'www.zappos.com,zappos.com', key: 10 },
				{ label: 'Zara', value: 'www.zara.com,zara.com', key: 11 },
				{ label: 'HomeDepot', value: 'www.homedepot.com,homedepot.com', key: 12 },
				{ label: 'Lowes', value: 'www.lowes.com,lowes.com', key: 13 }
			],
			stores: [],
			selectedStores: [],
			searchValue :"",
			ratingFilter: [
				{ label: 'None', value: '', key: 1 },
				{ label: 'Likes - High to Low', value: 'Likes - High to Low', key: 2 },
				{ label: 'Dislikes - High to Low', value: 'Dislikes - High to Low', key: 3 },
				{ label: 'Likes - Low to High', value: 'Likes - Low to High', key: 4 },
				{ label: 'Dislikes - Low to High', value: 'Dislikes - Low to High', key: 5 },
				// { label: 'Popularity', value: 'Popularity', key: 5 },
			],
			ratingFilters: [],
			selectedRatings: [],
			selectedFilter: [],
			selectedCategoryFilter: [],
			removeChield: [],
			selectedCategoriesData: [],
			setTimerFlag : false,
			dealCategoryByPrioritys : [],
			selectedDealFilter : '',
			isCategorySelect : false,
			selectedTopDealFilter: false,
			selectedTopDeal: ''
		};


		this.prepareTreeTablesChild = this.prepareTreeTablesChild.bind(this)
		this.prepareTreeTablesRoot = this.prepareTreeTablesRoot.bind(this)
		this.loadOnExpand = this.loadOnExpand.bind(this);
		this.onSelectionChange = this.onSelectionChange.bind(this)
		this.onGlobalSearch = this.onGlobalSearch.bind(this)
		this.setTime = this.setTime.bind(this)
		this.updateDeal = this.updateDeal.bind(this);
		this.onStoreSelectionChange = this.onStoreSelectionChange.bind(this)
		this.onRatingSelectionChange = this.onRatingSelectionChange.bind(this)
		this.onLoadURL = this.onLoadURL.bind(this)
		this.onLoadCategoryURL = this.onLoadCategoryURL.bind(this)
		this.onLoadGLobalSearchURL = this.onLoadGLobalSearchURL.bind(this)
		this.onLoadDealCategoryURL = this.onLoadDealCategoryURL.bind(this)
		this.getParentDealCategory();
		this.getcategoryByPriority();
	}

	componentWillMount() {
		document.addEventListener('mousedown', this.handleClick, false);
		var that = this
		this.setState({setTimerFlag: false})
		setInterval(function () {
			let time = that.state.time
			let countDown = (time.minutes * 60) + (time.seconds)
			if (countDown == 0) {
				countDown = 1800;
				//Window.reload()
				window.location.reload()
			} else {
				countDown = countDown - 1
			}
			var minutes = Math.floor(countDown / 60);
			var seconds = Math.floor(countDown % 60);

			let times = {
				minutes: minutes,
				seconds: seconds
			}
			that.setState({ time: times })
			that.state.time = times
		}, 1000)

		var result = [];
		for (var i = 0; i < this.state.store.length; i++) {
			var data = {};
			data["data"] = this.state.store[i];
			data["key"] = this.state.store[i].key;
			data["label"] = this.state.store[i].label;
			result.push(data);
		}
		this.setState({ stores: result })
		let storeString = JSON.stringify(result)
		localStorage.setItem("stores", storeString)

		var ratingResult = [];
		for (var i = 0; i < this.state.ratingFilter.length; i++) {
			var data = {};
			data["data"] = this.state.ratingFilter[i];
			data["key"] = this.state.ratingFilter[i].key;
			data["label"] = this.state.ratingFilter[i].label;
			ratingResult.push(data);
		}
		this.setState({ ratingFilters: ratingResult })
		let ratingString = JSON.stringify(ratingResult)
		localStorage.setItem("ratingFilters", ratingString)
	}

	componentWillUnmount() {
		document.removeEventListener('mousedown', this.handleClick, false);
	}
	componentDidMount() {
		var that = this
// Set Launch Date (ms)
const launchDate = new Date().getTime(that.state.time.minutes+":"+that.state.time.seconds+":"+"00");

// Context object
const c = {
  context: {},
  values: {}, 
  times: {}
};

// Convert radians to degrees
function deg(d) {
 return (Math.PI/180)*d-(Math.PI/180)*90;
}

function deg2(d) {
 return (Math.PI/180)*d-(Math.PI/180)*90;
}

function render() {
  c.context.seconds.clearRect(0, 0, 200, 200);
  c.context.seconds.beginPath();
  c.context.seconds.strokeStyle = "#e93578";
   c.context.seconds.arc(85, 100, 70, deg(0), deg2(6 * (60 - c.times.seconds)*-1));
  c.context.seconds.lineWidth = 7;
  c.context.seconds.lineCap = "round"; 
  c.context.seconds.stroke();
  
  c.context.minutes.clearRect(0, 0, 200, 200);
  c.context.minutes.beginPath();
  c.context.minutes.strokeStyle = "#72bb53";
 // c.context.minutes.arc(85, 100, 60, deg(0), deg(-6 * (60 - (c.times.minutes * 2)))); //Anticlockwise
  c.context.minutes.arc(85, 100, 60, deg(0), deg(6 * (60 - c.times.minutes)));    //clockwise
  c.context.minutes.lineWidth = 12;
  c.context.minutes.lineCap = "round"; 
  c.context.minutes.stroke();
  
}

function init() {
  // Get 2D contexts
  c.context.seconds = document.getElementById('seconds-canvas').getContext('2d');
  c.context.minutes = document.getElementById('minutes-canvas').getContext('2d');
  // Get displayed values
  c.values.seconds2 = document.getElementById('seconds-value');
  c.values.minutes2 = document.getElementById('minutes-value');
  
  setInterval(function() {
    // Get todays date and time (ms)
    const now = new Date().getTime();

    // Get distance from now to launchDate
    //const distance = launchDate ;
 		//const distance2 = launchDate ;

		// Time calculations for canvas
		//c.times.minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		//c.times.seconds = Math.floor((distance % (1000 * 60)) / 1000);
		
   // c.values.minutes.innerText = c.times.minutes;
   // c.values.seconds.innerText = c.times.seconds;
    

    c.times.minutes = that.state.time.minutes;
		c.times.seconds = that.state.time.seconds;
		//	Time calculations for text
		c.times.minutes2 = that.state.time.minutes;
		c.times.seconds2 = that.state.time.seconds;

    c.values.minutes2.innerText = c.times.minutes2;
    c.values.seconds2.innerText = c.times.seconds2;
    
    render(); // Draw!
  }, 1000);
}

init();

	}
	updateDeal(deal, types) {
		this.props.updateFavDealFeedback(deal, types)
	}
	handleClick = (e) => {
		if (!this.node.contains(e.target)) {
			if (e.target.id === 'tree_div' || e.target.id === 'the_div' || e.target.id === 'store_tree_div' || e.target.id === 'store_the_div') {
			} else {

				this.state.isTreeShow = false
				this.setState({ isTreeShow: false }) 

				if (!this.nodes.contains(e.target)) {
					if (e.target.id === 'store_tree_divs') {
						let flag = this.state.isStoreShow
						this.state.isStoreShow = !flag
						this.setState({ isStoreShow: !flag })
						this.state.isratingFilterShow = false
						this.setState({ isratingFilterShow: false })
					} else {
						this.state.isStoreShow = false
						this.setState({ isStoreShow: false })
					}
				} if (!this.nodet.contains(e.target) && !this.nodes.contains(e.target)) {
					if (e.target.id === 'rating_tree_divs') { 
					let flagRating = this.state.isratingFilterShow
					this.state.isratingFilterShow = !flagRating
					this.setState({ isratingFilterShow: !flagRating })
					this.state.isStoreShow = false
					this.setState({ isStoreShow: false })
				} else {
					this.state.isratingFilterShow = false
					this.setState({ isratingFilterShow: false })
				}
			   } else {
					if (e.target.id === 'store_tree_divs') {
						if (this.state.isStoreShow) {
							this.state.isStoreShow = false
							this.setState({ isStoreShow: false })
							this.state.isratingFilterShow = false
							this.setState({ isratingFilterShow: false })
						} else {
							this.state.isStoreShow = true
							this.setState({ isStoreShow: true })
						}

					}
					if (e.target.id === 'rating_tree_divs') {
						if (this.state.isratingFilterShow) {
							this.state.isratingFilterShow = false
							this.setState({ isratingFilterShow: false })
						} else {
							this.state.isratingFilterShow = true
							this.setState({ isratingFilterShow: true })
						}

					}

				}

			}

			return;
		} else {
			if (e.target.id === 'tree_divs') {
				if (this.state.isTreeShow) {
					this.state.isTreeShow = false
					this.setState({ isTreeShow: false })
				} else {
					this.state.isTreeShow = true
					this.setState({ isTreeShow: true })
					this.state.isStoreShow = false
					this.setState({ isStoreShow: false })
					this.state.isratingFilterShow = false
					this.setState({ isratingFilterShow: false })
				}

			}

		}

	}
	setTime(countDownDate) {
		this.setState({setTimerFlag: true})
		var minutes = 30 - (Math.floor((countDownDate % (1000 * 60 * 60)) / (1000 * 60))) % 30;
		var seconds = 60 - (Math.floor((countDownDate % (1000 * 60)) / 1000)) % 60;

		let time = {
			minutes: minutes,
			seconds: seconds
		}
		this.setState({ time: time })
		this.state.time = time
	}
	toggle() {
		hotDealNavBarRef.setState({ collapse: !this.state.collapse });
		hotDealNavBarRef.setState({ loadingFavContent: true });
		//	setTimeout(function () {
		hotDealNavBarRef.setState({ loadingFavContent: false });
		//	}, 1500);
	}

	getcategoryByPriority() {
		hotDealService.getDealCategoryByPriority().then(dealCategory => {
			this.setState({dealCategoryByPrioritys:dealCategory.result});
			var data = {
				categoryImage:'question-mark.png',
				categoryName:'others',
				id:0
			}
			this.state.dealCategoryByPrioritys.push(data);
			this.setState(this.state);
		});
	}

	getParentDealCategory() {
		var storedCategories = localStorage.getItem("storedCategories")
		var allCategories = localStorage.getItem("allCategories")
		if(allCategories ===null || allCategories === ""){}
		else{
		this.state.allCategories = JSON.parse(allCategories)
		this.setState({allCategories : JSON.parse(allCategories)}) 
		}
		if (storedCategories === null || storedCategories === "") {
		dealAlertService.getDealCategoriesWithChild().then(category => {
		if (category.message === 'SUCCESS') {
		this.state.allCategories = category.result
		this.setState({allCategories : category.result})
		localStorage.setItem('allCategories', JSON.stringify(category.result))
		this.setState({ categories: this.prepareTreeTablesRoot(category.result) });
		this.setState({ categoryLength: this.state.categories.length })
		localStorage.setItem("storedCategoriesLength", this.state.categories.length)
		let categories = this.state.categories;
		let cat = []
		for (var i in categories) {
		categories[i].children = []
		for (var j in categories) {
		if (categories[j].data.parentId === categories[i].data.id) {
		categories[i].children.push(categories[j])
		}
		}
		}
		for (var k in categories) {
		if (categories[k].data.parentId === 0) {
		cat.push(categories[k])
		}
		}
		this.setState({ categories: cat })
		localStorage.setItem("storedCategories", JSON.stringify(cat))
		}
		});
		} else {
		storedCategories = JSON.parse(storedCategories)
		this.state.categoryLength = Number(localStorage.getItem("storedCategoriesLength"))
		this.setState({ categoryLength: Number(localStorage.getItem("storedCategoriesLength")) })
		this.state.categories = storedCategories
		this.setState({ categories: storedCategories })
		}
		
		}

	getFaviorateList() {
		return this.props.favDeals ? this.props.favDeals : [];
	}

	onRemoveFavDealClick(event) {
		this.props.deleteFromCookies("myFavDealsList", event.uniqueId, event.text);
	}

	showDropdown() {
	}

	onGlobalSearch() {
		this.setState({ selectedNodeKey: [] })
		this.setState({ selectedStoreNodeKey: [] })
		this.setState({ selectedRatingNodeKey: [] })
	}
	onRatingChange(e) {
		this.props.onRatingChange(e);
	}

	onDealRatingChange(e) {
		this.props.onDealRatingChange(e);
	}
	
	onStoreChange(e) {
		this.props.onStoreChange(e);
	}

	onDealCategoryChange(e){
		this.props.onDealCategoryChange(e);
	}

	onCategoryChange(e) {
		this.props.onCategoryChange(e);
	}

	loadOnExpand(event) {
		if (event.node) {

		}
	}

	prepareTreeTablesRoot(dealCategory) {
		var result = [];
		for (var i = 0; i < dealCategory.length; i++) {
			var data = {};
			data["data"] = dealCategory[i];
			data["key"] = dealCategory[i].id;
			data["label"] = dealCategory[i].categoryName;
			result.push(data);
		}
		return result;
	}

	prepareTreeTablesChild(dealCategory) {
		var result = [];
		for (var i = 0; i < dealCategory.length; i++) {
			var data = {};
			data["data"] = dealCategory[i];
			data["key"] = dealCategory[i].id;
			data["label"] = dealCategory[i].categoryName;
			data["leaf"] = false;
			result.push(data);
		}
		return result;
	}

	closeTree = (event) => {
		this.state.isTreeShow = false;
		this.setState({ isTreeShow: false });
	}

	handleSearch = (event) => {
		let query = event.target.value;
		let categoriesString = localStorage.getItem("storedCategories")
		let categories = JSON.parse(categoriesString)
		if (query !== "") {
			dealAlertService.searchDealCategory(query).then((data) => {
				this.setState({ categories: this.prepareTreeTablesRoot(data.result) });
				this.state.isTreeShow = true
				this.setState({ isTreeShow: true })
			});
		} else {
			this.setState({ categories: categories });
			this.state.isTreeShow = true
			this.setState({ isTreeShow: true })
		}
	}

	handleRatingSearch = (event) => {
		this.state.isratingFilterShow = true
		this.setState({ isratingFilterShow: true })
		let query = event.target.value;
		let ratingString = localStorage.getItem("ratingFilters")
		let ratings = JSON.parse(ratingString)
		let result = []
		if (query !== "") {
			for (var i in ratings) {
				var str = ratings[i].label.toLowerCase();
				var index = str.search(query);
				if (index !== -1) {
					result.push(ratings[i])
				}
			}
			this.setState({ ratingFilters: result })

		} else {
			this.setState({ ratingFilters: ratings })
		}
	}

	handleStoreSearch = (event) => {
		this.state.isStoreShow = true
		this.setState({ isStoreShow: true })
		let query = event.target.value;
		let storeString = localStorage.getItem("stores")
		let stores = JSON.parse(storeString)
		let result = []
		if (query !== "") {
			for (var i in stores) {
				var str = stores[i].label.toLowerCase();
				var index = str.search(query);
				if (index !== -1) {
					result.push(stores[i])
				}
			}
			this.setState({ stores: result })

		} else {
			this.setState({ stores: stores })
		}

	}

	onFocus = () => {
	}

	keyPress = (event) => {
		let keyPressed = event.which;
		if (keyPressed === this.state.KEY.enter ||
			(keyPressed === this.state.KEY.tab && event.target.value)) {
			event.preventDefault();
			//this.updateChips(event);
		} else if (keyPressed === this.state.KEY.backspace) {
			//let chips = this.state.chips;


		}
	}

	onSelectionChange(e) {  
		this.state.selectedNodeKey = e.value
		this.setState({ selectedNodeKey: e.value })
		let categoryFilter = []
		for (var i = 1; i <= this.state.categoryLength; i++) {
			if (e.value[i] != undefined) {
				if (e.value[i].checked) {
					categoryFilter.push(i)
				}
			}
		}
		this.state.isTreeShow = true
		this.setState({ isTreeShow: true })
		this.props.onCategoryChange(categoryFilter);
	}

	categoryOnUnselect = (event) => {
		for( var i = 0; i < this.state.selectedCategoryFilter.length; i++){ 
			if ( this.state.selectedCategoryFilter[i].selectedCategoryName === event.node.data.categoryName) {
				this.state.selectedCategoryFilter.splice(i, 1); 
				this.setState(this.state);
				var tempCat = this.state.selectedCategoryFilter
				localStorage.setItem("selectCat",  JSON.stringify(tempCat));
			}
		 }
	}
	onCategoryDselect(item) {
		var removeItem;
		this.setState({removeChield: []});
		let allCategories = this.state.allCategories
		for( var i = 0; i < this.state.selectedCategoryFilter.length; i++){ 
			if ( this.state.selectedCategoryFilter[i].selectedCategoryName === item.selectedCategoryName) {
				this.state.selectedCategoryFilter.splice(i, 1); 
				this.setState(this.state);
			}
		 }
		 for (var i in allCategories){
			 if(allCategories[i].categoryName === item.selectedCategoryName){
				removeItem = allCategories[i];
			 }
		 }
		// this.state.removeChield.push(removeItem);
		 for (var i in allCategories){
			if(allCategories[i].id === removeItem.id){
				this.state.removeChield.push(allCategories[i]);
				this.findChildren(allCategories[i].id);
			}
		 }
		 this.setState(this.state);

		  let nodeKey = this.state.selectedNodeKey
			 for(var j in nodeKey){
				 for(var i = 0; i < this.state.removeChield.length; i++){
					if(j === this.state.removeChield[i].id.toString()){
						nodeKey[j].checked = false;
						nodeKey[j].partialChecked = false;
						break;
					}
				 }	
			 }

			 this.state.selectedNodeKey = nodeKey
			  this.setState({ selectedNodeKey: nodeKey })

			let selectData = this.state.selectedNodeKey;
			 let categoryFilter = []
			 for(var i in selectData){
				 if(this.state.selectedNodeKey[i].checked){
					categoryFilter.push(i);
				 }
			 }
			this.props.onCategoryChange(categoryFilter);

	}

	findChildren(chieldId){
		let allCategories = this.state.allCategories
		for (var i in allCategories){
			if(allCategories[i].parentId === chieldId){
				this.state.removeChield.push(allCategories[i]);
				this.findSecondChildren(allCategories[i].id);
			}
		 }
	}
	findSecondChildren(chieldId){
		let allCategories = this.state.allCategories
		for (var i in allCategories){
			if(allCategories[i].parentId === chieldId){
				this.state.removeChield.push(allCategories[i]);
				this.findChildren(allCategories[i].id);
			}
		 }
	}

	onDealsClick(){
		this.setState({isCategorySelect: false});
		this.setState({selectedTopDealFilter: false});
		this.onDealCategoryChange(0);
	}

	onStoreDselect(item) {
		for( var i = 0; i < this.state.selectedFilter.length; i++){ 
			if ( this.state.selectedFilter[i].selectedFilterName === item.selectedFilterName) {
				this.state.selectedFilter.splice(i, 1); 
				this.setState(this.state);
			}
		 }
		var setUrlData = [];
		let stores = this.state.stores
		for (var i = 0; i < stores.length; i++){
			if( stores[i].data.label === item.selectedFilterName){
				this.state.selectedStoreNodeKey[i+1].checked = false;
				this.state.selectedStoreNodeKey[i+1].partialChecked = false;
				this.setState(this.state);
			}else if(this.state.selectedStoreNodeKey[i+1] !== undefined && this.state.selectedStoreNodeKey[i+1].checked === true){
				setUrlData.push(stores[i].data.value);
			}
		}
		this.props.onStoreChange(setUrlData);
	}

	onSelect = (event) => {
		let selectedCategories = event.node
		this.state.selectedCategoriesData = event.node
		this.setState({selectedCategoriesData: event.node})
		var data = {
			selectedCategoryName: event.node.data.categoryName,
		  }
		this.state.selectedCategoryFilter.push(data);
		this.setState(this.state);
		var tempCat = this.state.selectedCategoryFilter
		localStorage.setItem("selectCat", JSON.stringify(tempCat));
	}

	onStoreSelect = (event) => {
		let selectedStores = this.state.selectedStores
	}
	onRatingSelect = (event) => {
		let selectedRatings = this.state.selectedRatings
	}
	// top deal
	handleOnDragStart = (e) => {
		e.preventDefault()
	}

	state = {
		galleryItems: [1, 2, 3].map((i) => (<h2 key={i}>{i}</h2>)),
	  }
	 
	  responsive = {
		0: { items: 2 },
		375: {items: 3},
		425: { items: 5 },
		768: { items: 7 },
		1024: { items: 8 }
	}
	 
	onSlideChange(e) {
		console.debug('Item`s position during a change: ', e.item)
		console.debug('Slide`s position during a change: ', e.slide)
	}
	 
	onSlideChanged(e) {
		console.debug('Item`s position after changes: ', e.item)
		console.debug('Slide`s position after changes: ', e.slide)
	}

	onRatingSelectionChange(e){
		this.state.selectedRatingNodeKey = e.value
		this.setState({ selectedRatingNodeKey: e.value })
		let ratingString = localStorage.getItem("ratingFilters")
		let ratings = JSON.parse(ratingString)
		let ratingFilterData = [] 
		for (var i = 0; i <= ratings.length - 1; i++) {
			if (e.value !== undefined) {
				if (e.value === ratings[i].data.key) {
					ratingFilterData.push(ratings[i].data.value)
				}
			}
		}
		this.setState({ ratingFilterData: ratingFilterData })
		this.state.isratingFilterShow = false
		this.setState({ isratingFilterShow: false })
		this.props.onDealRatingChange(ratingFilterData);
	}
	
	onStoreSelectionChange(e) {
		this.state.selectedStoreNodeKey = e.value
		this.setState({ selectedStoreNodeKey: e.value })
		let storeString = localStorage.getItem("stores")
		let stores = JSON.parse(storeString)
		let storeFilter = []
		this.state.selectedFilter = [];
		this.setState(this.state);
		for (var i = 0; i <= stores.length - 1; i++) {
			if (e.value[i + 1] !== undefined) {
				if (e.value[i + 1].checked) {
					storeFilter.push(stores[i].data.value)
					var data = {
						selectedFilterName: stores[i].data.label,
					  }
					this.state.selectedFilter.push(data);
					this.setState(this.state);
				}
			}
		}
		this.setState({ storeFilter: storeFilter })
		this.state.isStoreShow = true
		this.setState({ isStoreShow: true })
		this.props.onStoreChange(storeFilter);
	}

	onDealCategoryClick(data){
		this.setState({selectedTopDeal: data.categoryName});
		this.setState({selectedTopDealFilter: true});
		this.onDealCategoryChange(data.id);
		this.setState({selectedDealFilter:'Top '+data.categoryName+' Deals '});
		this.setState({isCategorySelect: true});
	}

	onLoadURL(data){
		let stores = this.state.stores
		var nodeKey = []
		var store = data.split(",")
		var tempStoreArray =[]

		for(var i = 0; i < stores.length; i++){
			for(var j = 0; j < store.length; j++){
				if(stores[i].data.value.split(",")[0] === store[j]){
					var tempData = {
						selectedFilterName: stores[i].label
						}
					this.state.selectedFilter.push(tempData);
						this.setState(this.state);
				}
			}
		}


		for (var i in stores){
			var node = {
				checked: false,
				partialChecked: false
			}
			if(i==="0"){
				var tempNode = JSON.stringify(node)
				nodeKey.push(JSON.parse(tempNode));
			}
			for(var j in store){
				if(stores[i].data.value.includes(store[j])){
					if(!tempStoreArray.includes(store[i])){
						node.checked = true
						tempStoreArray.push(store[i])
					}

				}
			}
				nodeKey.push(node);
		}
		this.state.selectedStoreNodeKey = nodeKey
		this.setState({ selectedStoreNodeKey: nodeKey })
	}

	onLoadCategoryURL(data){
		let allCategories = this.state.allCategories
		var nodeKey = []
		var category = data.split(",")
		var expandedKeys ={}
		for (var i in allCategories){
			var node = {
				checked: false,
				partialChecked: false
			}
			if(i==="0"){
				var tempNode = JSON.stringify(node)
				nodeKey.push(JSON.parse(tempNode));
			}
				if(category.includes(allCategories[i].id.toString())){
						node.checked = true;
						var catId = allCategories[i].id.toString();
						while(allCategories[Number(catId)].parentId !== 0){
							for(var j in allCategories){
								if(allCategories[j].id === allCategories[Number(catId)].parentId){
									var cat = allCategories[j];
									expandedKeys[allCategories[catId].parentId]= true;
									catId = cat.parentId.toString();
									break;
								}
								
							}
							
						}
						
				}
				nodeKey.push(node);
		}
		this.state.expandedKeys = expandedKeys
		this.setState({expandedKeys :expandedKeys})
		this.state.selectedNodeKey = nodeKey
		this.setState({ selectedNodeKey: nodeKey })
		this.state.selectedCategoryFilter = JSON.parse(localStorage.getItem("selectCat"));
		this.setState(this.state);
	}

	onLoadDealCategoryURL(data){
		hotDealService.getDealCategoryByPriority().then(dealCategory => {
			for(var i = 0; i < dealCategory.result.length; i++){
				if(data === dealCategory.result[i].id.toString()){
					this.setState({selectedDealFilter:'Top '+dealCategory.result[i].categoryName+' Deals '});
					this.setState({isCategorySelect: true});
					this.setState({selectedTopDeal: dealCategory.result[i].categoryName});
					this.setState({selectedTopDealFilter: true});
				}
			}
		});
	}
	onLoadGLobalSearchURL(data){
		this.setState({searchValue :data})
	}
	render() {

		const ratings = [
			{ label: 'All', value: '' },
			{ label: '5 Thumbs', value: 'eq:5' },
			{ label: '4+ Thumbs', value: 'gt:4' },
			{ label: '3+ Thumbs', value: 'gt:3' },
			{ label: '2+ Thumbs', value: 'gt:2' },
			{ label: '1+ Thumbs', value: 'gt:1' }
		];

		return (
			<div>
				<section id="p-slider">
					<div className="container">
						<div className="row">
							<div className="col-lg-3 col-md-4" style={{'padding-right':'0','padding-left':'8px'}}>
								<div className="p-countdownclock">
									<h4 className="mb-0">New Deals Coming In</h4>
									<div className="countdown">
										<div className="container minutes">
											<canvas id="minutes-canvas" width="200" height="180"></canvas>
											<svg width="200" height="180">
												<circle id="outer" cx="100" cy="100" r="60" fill="#000" stroke-width="12" stroke="#72bb53" stroke-opacity="0.1"/>
											</svg>
											<div className="minuteslabel">
												<span id="minutes-value"></span><br/>
												<span>min</span>
											</div>
										</div>
										<div className="container seconds">
											<canvas id="seconds-canvas" width="200" height="180"></canvas>
											<svg width="200" height="180">
												<circle id="outer" cx="100" cy="100" r="70" fill="transparent" stroke-width="7" stroke="#e93578" stroke-opacity="0.1"/>
											</svg>
											<div className="secondslabel">
												<span id="seconds-value"></span><br/>
												<span>sec</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-9 col-md-8 d-none d-md-block" style={{'padding-right':'8px', 'padding-left':'8px'}}>
								<div id="carouselExampleIndicators" className="carousel slide " data-ride="carousel">
									<ul className="carousel-indicators mb-2">
										<li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
									</ul>
									<div className="carousel-inner">
										<div className="carousel-item active" style={{'background-color':'#edefec'}}>
											<img className="img-fluid" src={require('../../assets/img/test1.jpg')}/>
											<div class="carousel-caption" style={{'text-align':'right'}}>
												<h5>Premium</h5>
												<h1>Headphones</h1>
												<p>Shop now</p>
											</div>
										</div>
										<div className="carousel-item" style={{'background-color':'#e1eaef'}}>
											<img className="img-fluid" src={require('../../assets/img/test2.jpg')}/>
											<div class="carousel-caption" style={{'text-align':'right'}}>
												<h5>Over 200+</h5>
												<h1>Great Deals</h1>
												<p>While they last !</p>
											</div>
										</div>
										<div className="carousel-item" style={{'background-color':'#fafafa'}}>
											<img className="img-fluid" src={require('../../assets/img/test3.jpg')}/>
											<div class="carousel-caption" style={{'text-align':'right'}}>
												<h5>Online Exclusive Deals !</h5>
												<h1>Up to 50% off</h1>
												<p>On select samsung products</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section id="topdealcat">
					<div className="container pl-2 pr-2">
					<h4 className="mb-3"> Top Deal Categories </h4>
						<AliceCarousel
							items={this.state.dealCategoryByPrioritys}
							responsive={this.responsive}
							autoPlayInterval={2000}
							autoPlayDirection="ltr"
							autoPlay={true}
							fadeOutAnimation={true}
							disableAutoPlayOnAction={true}
							onSlideChange={this.onSlideChange}
							onSlideChanged={this.onSlideChanged}
							buttonsDisabled={false}
							dotsDisabled={true}>
							{this.state.dealCategoryByPrioritys.map((item, index) => (
								<div class="card" onClick={() => this.onDealCategoryClick(item)} style={{cursor: 'pointer'}}>
									<img class="card-img-top" src={require('../../assets/img/topdeals/'+item.categoryImage)}/>
									<h6 class="card-title">{item.categoryName}</h6>
								</div>
							))}
						</AliceCarousel>
					</div>
				</section>
				<section className="container pl-2 pr-2">
					<div className="clearfix">
						<div className="float-left p-filters">
							<div ref={nodes => this.nodes = nodes}>
								<button className="btn btn-warning d-none d-lg-block" id="store_tree_divs" type="text" autoComplete="off" onChange={this.handleStoreSearch} onClick={this.onStoreFocus}><i class="fas fa-store-alt" id="store_tree_divs"></i> Deal By Stores</button>
								<button className="btn btn-warning p-none-btn p-btn-size" id="store_tree_divs" type="text" autoComplete="off" onChange={this.handleStoreSearch} onClick={this.onStoreFocus}><i class="fas fa-store-alt" id="store_tree_divs"></i></button>								
								{this.state.isStoreShow ?
									<div className="p-stores">
										<Tree value={this.state.stores} onSelect={this.onStoreSelect} selectionMode="checkbox" selectionKeys={this.state.selectedStoreNodeKey}
											onSelectionChange={this.onStoreSelectionChange.bind(this)}/>
									</div>
									: null
								}
							</div>
							<div ref={node => this.node = node}>
								<button className="btn btn-warning ml-3 d-none d-lg-block" id="tree_divs" type="text" autoComplete="off" onChange={this.handleSearch} onClick={this.onFocus}><i class="fas fa-tasks" id="tree_divs"></i> Deal By Categories</button>
								<button className="btn btn-warning ml-3 p-none-btn p-btn-size" id="tree_divs" type="text" autoComplete="off" onChange={this.handleSearch} onClick={this.onFocus}><i class="fas fa-tasks" id="tree_divs"></i> </button>								
								{this.state.isTreeShow
									? <div className="p-categories ml-3">
										<Tree value={this.state.categories} onExpand={this.loadOnExpand}
											expandedKeys={this.state.expandedKeys}
											onToggle={e => this.setState({ expandedKeys: e.value })} onSelect={this.onSelect} onUnselect={this.categoryOnUnselect} selectionMode="checkbox" selectionKeys={this.state.selectedNodeKey}
											onSelectionChange={this.onSelectionChange.bind(this)} />
									</div>
									: null
								}
							</div>
							<div ref={nodet => this.nodet = nodet}>
								<button className="btn btn-warning ml-3 d-none d-lg-block" id="rating_tree_divs" type="text" autoComplete="off" onChange={this.handleRatingSearch} onClick={this.onFocus}><i class="fas fa-long-arrow-alt-down" id="rating_tree_divs"></i><i class="fas fa-long-arrow-alt-up" id="rating_tree_divs"></i> Deal Sort By</button>		
								<button className="btn btn-warning ml-3 p-none-btn p-btn-size" id="rating_tree_divs" type="text" autoComplete="off" onChange={this.handleRatingSearch} onClick={this.onFocus}><i class="fas fa-long-arrow-alt-down" id="rating_tree_divs"></i><i class="fas fa-long-arrow-alt-up" id="rating_tree_divs"></i></button>										
								 {this.state.isratingFilterShow ? 
								<div className="p-sort ml-3">
										<Tree value={this.state.ratingFilters} onSelect={this.onRatingSelect} selectionMode="single" selectionKeys={this.state.selectedRatingNodeKey}
											onSelectionChange={this.onRatingSelectionChange.bind(this)}/>
								</div>
							      : null	
							} 
							</div>
						</div>
						<div className="float-right">
								<button className="btn btn-warning d-none d-lg-block" onClick={this.toggle}> <i className="fa fa-heart"></i></button>
								<button className="btn btn-warning p-none-btn p-btn-size" onClick={this.toggle}> <i className="fa fa-heart"></i></button>
								<div class="p-circle">
									<div class="p-circlecontent">{this.getFaviorateList().length }</div>
								</div>
						</div>
					</div>
				</section>
				<div className="container pl-2 pr-2">
					{this.state.collapse
						? <div className="hotDealProducts">
					
									{(this.getFaviorateList().length > 0 && !this.state.loadingFavContent)
										&& <div className="row">
											{
												this.getFaviorateList().map((item, index) => (
													<Col sm="12" md="3" lg="2" className="p-cardwidth">
														<HotDealCard keys={'fav' + index} deal={item} deleteIcon={true} onRemoveFavDealClick={this.onRemoveFavDealClick} updateDeal={this.updateDeal} />
													</Col>
												))
											}
										</div>
									}
									{(!this.state.loadingFavContent && this.getFaviorateList().length == 0) &&
										<div className="favItems row" id="favItems"><div className="fav-result">Sorry, you don't have any Favorite Deals.</div></div>
									}
									{this.state.loadingFavContent &&
										<div className="favItems row" id="favItems"><div className="fav-result">Please wait...</div></div>
									}

								</div>
						
						: null
					}
				</div>

				{/* Start of Chipse  */}
					<div className="container pl-2 pr-2">
						{ this.state.selectedCategoryFilter.length !== 0 && !this.state.collapse &&
						<div className="mb-2">
							<b>Categories: </b>
							{this.state.selectedCategoryFilter.map((item, index) => (

							<span>
								<div className="tagBox mr-2"><span className="pl-2 pr-2">{item.selectedCategoryName}</span>
								<span className="mr-2 ml-2" onClick={() => this.onCategoryDselect(item)}><i className="fa fa-times" aria-hidden="true" style={{ 'cursor': 'pointer' }}></i></span>
								</div>
							</span>
							
							))}
						</div>
						}
						{ this.state.selectedFilter.length !== 0  && !this.state.collapse &&		
						<div className="mb-2">
							<b>Stores: </b>
							{this.state.selectedFilter.map((item, index) => (

							<span>
								<div className="tagBox mr-2"><span className="pl-2 pr-2">{item.selectedFilterName}</span>
								<span className="mr-2 ml-2" onClick={() => this.onStoreDselect(item)}><i className="fa fa-times" aria-hidden="true" style={{ 'cursor': 'pointer' }}></i></span>
								</div>
							</span>
							
							))}
						</div>
						}
						{ this.state.selectedTopDealFilter && 		
						<div className="mb-2">
							<b>Top Deal Category: </b>

							<span>
								<div className="tagBox mr-2"><span className="pl-2 pr-2">{this.state.selectedTopDeal}</span>
								<span className="mr-2 ml-2" onClick={() => this.onDealsClick()}><i className="fa fa-times" aria-hidden="true" style={{ 'cursor': 'pointer' }}></i></span>
								</div>
							</span>
							
						</div>
						}
					</div>
				{/* End of Chipse */}
				{/* <div className="container pl-2 pr-2">
				{ this.state.isCategorySelect &&
					<span><a onClick={() => this.onDealsClick()} className="deal-mouse-hover">Hot Deals </a><span> > </span> {this.state.selectedDealFilter}</span>
				}
				</div> */}
			</div>
		);
	}
}
export default HotDealNavBar;
