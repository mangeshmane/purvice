import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import logo from '../../../assets/img/purvice_logo_blue.jpg';
import facebook from '../../../assets/img/social_login.png';
import google from '../../../assets/img/google.png';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import './login.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { userService } from '../../../services/user.service';
import cookie from 'react-cookies';

var divStyle = {
  color: "red",
  marginTop: "0%"
};

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      submitted: false,
      loading: false,
      error: '',
      emailState: false,
      spinner: false,
      user: {
        firstName: '',
        lastName: '',
        password: '',
        username: '',
        emailId: '',
        verify: false

      }
    };
    this.handleChange = this.handleChange.bind(this);
    this.logIn = this.logIn.bind(this);
    this.socialLogin = this.socialLogin.bind(this);
    this.onFailed = this.onFailed.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
  }

componentDidMount(){
  let user = cookie.load("user")
  if(user!=null || user !== undefined){
    window.location = "#/hotdeals"
  }
}
  register = () => {
    this.props.history.push('/register');
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
    if (name === 'email') {
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
        this.setState({ emailState: false })
      } else {
        this.setState({ emailState: true })
      }
    }
  }

  socialLogin(userData, type) {
        if(type === 'facebook' && userData.name !== undefined){
          // removed && userData.state !== undefined from above if condition
          let statefbCopy = Object.assign({}, this.state);
          var fullname = userData.name.split(" ");
          statefbCopy.user.firstName = fullname[0];
          statefbCopy.user.lastName = fullname[1];
          statefbCopy.user.password = userData.reauthorize_required_in;
          statefbCopy.user.username = userData.name;
          statefbCopy.user.emailId = userData.name ? userData.name : null;
          statefbCopy.user.verify = true;
          this.setState(statefbCopy);
        }
        if(type === 'google'){
          let statesCopy = Object.assign({}, this.state);
          statesCopy.user.firstName = userData.profileObj.givenName;
          statesCopy.user.lastName = userData.w3.wea;
          statesCopy.user.password = userData.w3.U3 ? userData.w3.U3 : userData.profileObj.givenName;
          statesCopy.user.username = userData.w3.U3;
          statesCopy.user.emailId = userData.w3.U3 ? userData.w3.U3 : null;
          statesCopy.user.verify = true;
            this.setState(statesCopy);
        }

      if(this.state.user.verify){
        userService.socialLogin(this.state.user).then(data => {
          if (data.result && data.result.token != null) {
            toast.success("Login Successful", { position: toast.POSITION.TOP_RIGHT });
            cookie.save('username', data.result.user.firstName + " " + data.result.user.lastName, { path: '/' });
            cookie.save('userId', data.result.user.id, { path: '/' });
            cookie.save('user', data.result.user, { path: '/' });
            cookie.save('token', data.result.token, { path: '/' });
            var redirectPath = cookie.load("redirectPath");
            if(redirectPath){
              window.location = "#"+redirectPath;
            }else{
              setTimeout(function () { this.props.history.push('/hotdeals'); }.bind(this), 2500);
            }
          }
        })
      }
   }


  logIn(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const { email, password, returnUrl } = this.state;

    // stop here if form is invalid
    if (!(email && password)) {
      return;
    }
    this.setState({ spinner: true });
    this.setState({ loading: true });
    var data = {
      username: this.state.email,
      password: this.state.password
    }
    userService.login(data).then(user => {
      this.setState({ spinner: false });
      if (user.message === 'SUCCESS') {
        toast.success("Login Successful", { position: toast.POSITION.TOP_RIGHT });
        cookie.save('username', user.result.user.firstName + " " + user.result.user.lastName, { path: '/' });
        cookie.save('user', user.result.user, { path: '/' });
        cookie.save('userId', user.result.user.id, { path: '/' });
        cookie.save('token', user.result.token, { path: '/' });
        var redirectPath = cookie.load("redirectPath");
        if(redirectPath){
          window.location = "#"+redirectPath;
        }else{
          setTimeout(function () { this.props.history.push('/hotdeals'); }.bind(this), 2500);
        }
      } else {
        toast.error("Please enter valid username or password", { position: toast.POSITION.TOP_RIGHT })
      }
    },
      //error => this.setState({ error, loading: false })
    );
  }

  onFailed = (error) => {
    alert(error);
  };
  onSuccess = (response) => {
  };

  goToHotDeals = ()=>{
    this.props.history.push('/hotdeals');
  }

  render() {

    const responseFacebook = (response) => {
      this.socialLogin(response, 'facebook');
    } 

    const responseGoogle = (response) => {
      this.socialLogin(response, 'google');
    } 

    const { email, password, submitted, emailState, loading, error } = this.state;
    return (
      <div className="mt-5 align-items-center">
        <ToastContainer autoClose={2500} />
        <Container>
          <Row className="justify-content-center">
            <Col lg="4" md="5">
              <CardGroup>
                <Card className="p-3 login-section">
                  <CardBody>
                    <div style={{ display: 'flex', justifyContent: 'center' }}><img src={logo} style={{ height: '50px' }} onClick = {this.goToHotDeals} /></div>
                    <h4 style={{ display: 'flex', justifyContent: 'center', marginTop: '0.5rem', marginBottom: '1rem' }}>
                      Welcome back!
                  </h4>
                    <Form onSubmit={this.logIn}>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-envelope-letter"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" onChange={this.handleChange} name="email" placeholder="Email Address" />
                      </InputGroup>

                      {submitted && !email &&
                        <InputGroup className="mb-3">
                          <div style={divStyle} hidden={this.state.emailState}>
                            Please enter email address.
                          </div>
                        </InputGroup>
                      }
                      {emailState &&
                        <InputGroup className="mb-3">
                          <div style={divStyle} >
                            Please enter valid email address.
                          </div>
                        </InputGroup>
                      }

                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" onChange={this.handleChange} name="password" placeholder="Password" />
                      </InputGroup>

                      {submitted && !password &&
                        <InputGroup className="mb-3">
                          <div style={divStyle} hidden={this.state.passwordState}>
                            Please enter password.
                          </div>
                        </InputGroup>
                      }


                      <InputGroup className="mb-1">
                        <Button color="primary" block style={{ fontWeight: '600' }}><i class="fa fa-spinner fa-spin  float-right" hidden={!this.state.spinner}></i>Log In With Email</Button>
                      </InputGroup>


                      <div style={{ display: 'flex', justifyContent: 'center', cursor: 'pointer' }}>
                        <a style={{ textAlign: 'center', marginBottom: '1rem', marginTop: '1rem', color: '#53bbe2', fontWeight: '600' }}  onClick={ e => window.location = '#/forgot-password'}>Forgot Password?</a>
                      </div>

                      <div style={{ display: 'flex', justifyContent: 'center' }}>
                        <p className="subtitle fancy" ><span>or connect with</span></p>
                      </div>

                      <Row>
                        <Col xs="6" sm="6" md="6" lg="6">
                          <FacebookLogin
                            appId="2144831385807739"
                            apiVersion="v2.10" 
                            fields="name,email,picture"
                            cssClass="kep-login-facebook"
                            onFailure={responseFacebook}
                            callback={responseFacebook} 
                            icon="fa-facebook"
                            textButton="Facebook"/>,
                        </Col>
                        <Col xs="6" sm="6" md="6" lg="6">
                          {/* <GoogleLogin clientId="317584776494-m0kljch8285v0f858hv6uo3lg5t5tpln.apps.googleusercontent.com" buttonText="Google+" className="google_btn"
                            onSuccess={responseGoogle} onFailure={responseGoogle}
                          /> */}

                          <GoogleLogin clientId="317584776494-m0kljch8285v0f858hv6uo3lg5t5tpln.apps.googleusercontent.com" buttonText="Google+" 			className="google_btn" onSuccess={responseGoogle} onFailure={responseGoogle}/>
                        </Col>
                      </Row>
 


                      <p className="noAccount">Don't have an account? <a style={{ color: '#53bbe2' }} onClick={this.register}>Sign up now.</a></p>
                    </Form>
                    {error &&
                      <div className={'alert alert-danger'}>{error}</div>
                    }
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
          <hr />
          <Row className="justify-content-center">
            <Col lg="5">
              <p className="copy">Copyright © 2018 purvice,Inc. All Rights Reserved.</p>
            </Col>

            <Col lg="7">
              <p className="footer-link">
              	<a className="mr-1"  onClick = {() =>this.props.history.push('/about-us')}>About Us</a>
                <a className="mr-1"  onClick = {() =>this.props.history.push('/faq')}>FAQ</a>
                <a className="mr-1"  onClick = {() =>this.props.history.push('/privacy')}>Privacy</a>
                <a className="mr-1"  onClick = {() =>this.props.history.push('/terms')}>Terms</a>
                <a className="mr-1"  onClick = {() =>this.props.history.push('/copyright')}>Copyright</a>
              </p>
            </Col>
          </Row>


        </Container>


      </div>


    );
  }
}

export default Login;
