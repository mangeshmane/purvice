import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import logo from '../../../assets/img/purvice_logo_blue.jpg';
import facebook from '../../../assets/img/social_login.png';
import google from '../../../assets/img/google.png';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import '../Login/login.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { resetPasswordService } from '../../../services/reset-password.service';
import cookie from 'react-cookies';

var divStyle = {
  color: "red",
  marginTop: "0%"
};

class ResetPassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
        token: this.props.location.search.split("=")[1],
        password: '',
        passwordMatch: false,
        newPassword: '',
        validPassword: false,
        validToken:false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.passwordForgot = this.passwordForgot.bind(this);
    this.handleChangeNewPass = this.handleChangeNewPass.bind(this);
    this.verifyToken = this.verifyToken.bind(this);
    this.verifyToken();
  }
  
  verifyToken() {
    resetPasswordService.check(this.state.token+'888').then(data => { 
      if(data.explanation == "valid token"){
          this.state.validToken = true;
          this.setState({validToken:this.state.validToken});
      }else{
        this.state.validToken = false;
        this.setState({validToken:this.state.validToken});
      }
      },
    );
}

  handleChange(e){
    this.state.password =  e.target.value;
    if(e.target.value=="")
        this.state.validPassword = true;
    else
        this.state.validPassword = false;
  }

  handleChangeNewPass(e){
    this.state.newPassword =  e.target.value;
    if(this.state.password == e.target.value)
        this.state.passwordMatch = true;
    else    
        this.state.passwordMatch = false;
  }

  goToHotDeals = ()=>{
    this.props.history.push('/hotdeals');
  }

  passwordForgot(e){
    let password = this.state.password
    let matchingPassword = this.state.newPassword
    if(password === "" || matchingPassword === ""){
      toast.error("Password or Confirm password fields should not be empty.", { position: toast.POSITION.TOP_RIGHT });      
    }else {
      if(password===matchingPassword){
        var user = {
          password: this.state.password
      }
      resetPasswordService.updateUser(this.state.token,user).then(data => {
          if (data.message === 'SUCCESS') {
            toast.success("Your Password changed successfully", { position: toast.POSITION.TOP_RIGHT });
           // this.props.history.push('/hotdeals');
        setTimeout(() => {
          window.location = '#/login';
        }, 3000);
          } 
        },
          error => this.setState({ error, loading: false })
        );
      }else{
        toast.error("Password and Confirm password should be same.", { position: toast.POSITION.TOP_RIGHT });
      }
    }
    
    
}


  render() {
    const { email, password, submitted, emailState, loading, error } = this.state;
    return(
        <div className="mt-3 align-items-center">
        <ToastContainer autoClose={2500} />
        <Container>
        {/* {this.state.validToken && */}
          <Row className="justify-content-center">
            <Col lg="4" md="5">
              <CardGroup>
                <Card className="p-3 login-section">
                  <CardBody>
                    <div style={{ display: 'flex', justifyContent: 'center' }}><img src={logo} style={{ height: '50px' }} onClick = {this.goToHotDeals} /></div>
                    <h4 style={{ display: 'flex', justifyContent: 'center', marginTop: '0.5rem', marginBottom: '1rem' }}>
                    Create new password
                  </h4>
                    {/* <Form onSubmit={this.logIn}> */}
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" onChange={this.handleChange} name="password" placeholder="Password" />
                      </InputGroup>

                      {this.state.validPassword &&
                        <InputGroup className="mb-3">
                          <div style={divStyle} >
                            Please enter password.
                          </div>
                        </InputGroup>
                      }

                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" onChange={this.handleChangeNewPass} name="newPassword" placeholder="new password" />
                      </InputGroup>

                      {this.state.passwordMatch &&
                        <InputGroup className="mb-3">
                          <div style={divStyle}>
                          Password does not match
                          </div>
                        </InputGroup>
                      }

                      <InputGroup className="mb-1">
                        <Button color="primary" block onClick={this.passwordForgot.bind(this)} style={{ fontWeight: '600' }}><i class="fa fa-spinner fa-spin  float-right" hidden={!this.state.spinner}></i>Reset Your Password</Button>
                      </InputGroup>
        
                    {/* </Form> */}

                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
          {/* // }
          // {this.state.validToken == false && */}
    

          <hr />
          <Row className="justify-content-center">
            <Col lg="5">
              <p className="copy">Copyright © 2018 purvice,Inc. All Rights Reserved.</p>
            </Col>

            <Col lg="7">
              <p className="footer-link">
              	<a className="mr-1"  onClick = {() =>this.props.history.push('/about-us')}>About Us</a>
                <a className="mr-1"  onClick = {() =>this.props.history.push('/faq')}>FAQ</a>
                <a className="mr-1"  onClick = {() =>this.props.history.push('/privacy')}>Privacy</a>
                <a className="mr-1"  onClick = {() =>this.props.history.push('/terms')}>Terms</a>
                <a className="mr-1"  onClick = {() =>this.props.history.push('/copyright')}>Copyright</a>
              </p>
            </Col>
          </Row>


        </Container>


      </div>


  );
  }
}

export default ResetPassword;
