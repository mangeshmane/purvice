import React, { Component } from 'react';
import './Terms.css';
import Env from '../../../environments/environment.js';

class Terms extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
    	<React.Fragment>
	    	<section id="giftcards" className="top-padding-section">
	    	   <div className="content-width ">
	    	      <div className="row">
	    	         <div className="col">
	    	            <div className="cards">
	    	               <div className="row">
	    	                  <div className="container">
	    	                 	 <h2 className="section-title">Terms of Service</h2>
	    	                     <p className="static-page-text">       
	    	                        These Terms of Service (the "Terms") set forth the terms and conditions that govern access to, and use of, the websites and purvice mobile applications made available by purvice, Inc. ("purvice", "we," or "us") that enable a user to purchase, send or manage gift cards and receive the other services from purvice as described herein (collectively, the "Services").  These Terms are a legal agreement between you ("you," or "your") and purvice.  By clicking to "Accept" these Terms where this option is made available to you and/or by using our Services, you agree to be bound by these Terms.  These Terms do not alter in any way the terms or conditions of any other agreement you may have with any other party for products, services or otherwise.    
	    	                     </p>
	    	                     <p className="static-page-text">       
	    	                        purvice reserves the right to change or modify these Terms at any time and in our sole discretion.  If purvice makes changes to these Terms, we will provide notice of such changes, by providing notice through the Services, by e-mail and/or by updating these Terms (as indicated by the "Last Updated" date found at the top of these Terms) on the purvice website found at https://www.purvice.com/terms-of-service/.  You agree to receive notifications through these means and your continued use of the Services will indicate your acceptance of the revised Terms.  If you do not agree to any amended Terms, you must stop using the Services.    
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>1.Privacy Policy</h5>
	    	                     <p className="static-page-text"> 
	    	                        Please refer to our Privacy Policy for information about how we collect, use and disclose information about our users.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>2.Additional Terms</h5>
	    	                     <p className="static-page-text"> 
	    	                        If you purchase products (i.e., gift cards) from purvice, our Terms of Sale will apply.  These Terms of Sale are subject to change at any time and in our sole discretion, so you should review the Terms of Sale each time you make a purchase.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>3.Eligibility, Registration and Account</h5>
	    	                     <p className="static-page-text"> 
	    	                        Our Services are intended solely for users who are 13 years of age or older.  If you are under the age of 13, then you are not permitted to register for an account with purvice.  In addition, if you are between the ages of 13 and 18 (or the age of legal majority under applicable law), you may only use the Services under the supervision of a parent or legal guardian who registers for an account with purvice and thereby agrees to be bound by these Terms.  By using the Services, you represent and warrant that you are 13 years of age or older.
	    	                     </p>
	    	                     <p className="static-page-text"> 
	    	                        In order to use the Services, you will be required to register for an account with purvice.   You may only register for one account and you agree to: (i) provide accurate, current and complete information; (ii) maintain and promptly update your account information to keep it accurate, current and complete; (iii) maintain the security of your account and accept all risks of unauthorized access to your account; and (iv) promptly notify purvice if you discover or otherwise suspect that your account has been subject to hacking or other unauthorized use.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>4.Purvice Services Description</h5>
	    	                     <p className="static-page-text"> 
	    	                        Upload a gift card you own. You may upload information about gift cards you own to electronically manage those gift cards.  For example, depending on the features made available by the issuer of the gift card, after uploading information about your gift card, you may redeem or re-gift the gift card solely within the United States pursuant to the terms and conditions of the issuer of the gift card, view gift card details (e.g. check your gift card balance) to the extent the issuer of the gift card makes such details available to purvice, or visit the website of the issuer of the gift card.
	    	                     </p>
	    	                     <p className="static-page-text"> 
	    	                        Buy a gift card. You may buy a gift card for yourself.  The Terms of Sale will apply to the purchase of such gift cards and you agree to comply with such Terms of Sale.
	    	                     </p>
	    	                     <p className="static-page-text"> 
	    	                        Send a gift card. You may buy a gift card to send to a friend.  The Terms of Sale will apply to the purchase of such gift card and you agree to comply with such Terms of Sale.  purvice will use reasonable efforts to notify the recipient of the gift card you send in accordance with the method that you select to send the gift card.
	    	                     </p>
	    	                     <p className="static-page-text"> 
	    	                        Use your purvice Card Balance. When you register for an account with purvice and purchase our virtual purvice card, the amount that you load onto your virtual card will remain in your account until you purchase a gift card and apply that purvice Card Balance amount to your purchase. The Terms of Sale describe the use of and the restrictions associated with your purvice Card Balance, including, but not limited to, the $2,000 cap on your daily purvice Card Balance.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>5.Intellectual Property and Limited License</h5>
	    	                     <p className="static-page-text"> 
	    	                        Unless otherwise indicated by purvice, the Services and all content and other materials therein, including, without limitation, the purvice logo, the Terms of Sale and all other designs, text, graphics, pictures, information, data, software, sound files, other files made available within the Services and the selection and arrangement thereof, and any documentation or other ancillary material provided to you by or behalf of purvice (collectively, "purvice Content") are the proprietary property of purvice or our licensors or users and are protected by U.S. and international intellectual property laws.
	    	                     </p>
	    	                     <p className="static-page-text"> 
	    	                        purvice hereby grants you a limited, nonexclusive, non-sublicensable license to access and use the Services and purvice Content solely for the purpose of purchasing, sending or managing gift cards or receiving other services under these Terms; however, such license is subject to these Terms and you are not permitted to (a) sell, rent, lease, lend, redistribute, sublicense or make commercial use of the Services or the purvice Content; (b) copy, reverse engineer, decompile, disassemble or attempt to discover the source code of our Services or purvice Content; (c) modify, alter or otherwise make any derivative uses of the Services or the purvice Content, or any portion thereof; (d) remove, alter or obscure any copyright, trademark or other proprietary rights notice included in the Services or purvice Content; (e) use any data mining, robots or similar data gathering or extraction methods; (f) download (other than the page caching) any portion of the Services or the purvice Content, except as expressly permitted via the Services; and (g) use the Services or the purvice Content other than for their intended purposes.  Any use of the Services or the purvice Content other than as specifically authorized herein, without the prior written permission of purvice, is strictly prohibited and will terminate the license granted in this Section 5.  Such unauthorized use may also violate applicable laws, including without limitation, copyright and trademark laws and applicable communications regulations and statutes.  Unless explicitly stated by purvice, nothing in these Terms shall be construed as conferring any license to intellectual property rights, whether by estoppel, implication or otherwise.  This license is revocable at any time.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>6.Trademarks</h5>
	    	                     <p className="static-page-text"> 
	    	                        "purvice" and the purvice logo and any other purvice product or service names, logos or slogans are purvice's trademarks in the United States and in other countries, and may not be copied, imitated or used, in whole or in part, without the prior written permission of purvice. All other trademarks, registered trademarks, product names and company names or logos mentioned in the Services are the property of their respective owners and may not be used without permission of the applicable trademark holder.  Reference to any products, services, processes or other information, by name, trademark, manufacturer, supplier or otherwise does not constitute or imply endorsement, sponsorship or recommendation by purvice.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>7.Repeat Infringer Policy; Copyright  Complaints</h5>
	    	                     <p className="static-page-text"> 
	    	                        In accordance with the Digital Millennium Copyright Act ("DMCA") and other applicable laws, purvice has adopted a policy of terminating, in appropriate circumstances and at purvice's sole discretion, users who are deemed to be repeat infringers. purvice may also, in our sole discretion, limit access to the Services and/or terminate the accounts of any users who infringe any intellectual property rights of others, whether or not there is any repeat infringement. If you believe that anything on the Services infringes upon any copyright which you own or control, you may file a notification of such infringement with our Designated Agent as set forth below:  Email:  copyright@purvice.com Please see 17 U.S.C. Â§512(c)(3) for the requirements of a proper notification.  If you knowingly misrepresent in your notification that the material or activity is infringing, you will be liable for any damages, including costs and attorney's fees, incurred by us or the alleged infringer as the result of our relying upon such misrepresentation in removing or disabling access to the material or activity claimed to be infringing.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>8.Third-Party Services; Third-Party Materials</h5>
	    	                     <p className="static-page-text"> 
	    	                        purvice may provide links to third-party websites, apps, mobile services or other third-party services ("Third-Party Services") and may also display, link to or otherwise make available third-party content, data, information, events, apps or materials ("Third-Party Materials") on the Services. purvice does not endorse or control, and makes no representations or warranties of any kind, regarding any Third-Party Services or Third-Party Materials including, but not limited to, the content, accuracy, quality, nature, appropriateness, decency, functionality, performance, reliability, completeness, timeliness, validity, safety, legality or any other aspect thereof.  Your use of Third-Party Services and Third-Party Materials is at your own risk.  purvice is not responsible for any issues, legal or otherwise, that may result from your use of the Third-Party Services or Third-Party Materials, including any loss, damage or harm of any sort incurred as a result of your use of Third-Party Services or Third-Party Materials.  If you access or use any Third-Party Services or Third-Party Materials, purvice's terms and policies, including these Terms, no longer govern.  You should review the applicable terms and policies including, but not limited to, privacy and data gathering practices, of any Third-Party Service to which you navigate from the Services.  For the avoidance of doubt the terms and policies for Third-Party Services are solely between you and the Third-Party Services provider and not purvice.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>9.User Content</h5>
	    	                     <p className="static-page-text"> 
	    	                        You are solely responsible for all content you post, upload to, transmit, distribute, store, create or otherwise publish through the Services ("User Content").  The User Content you provide must comply with the rules set forth below.  These rules do not create any private right of action on the part of any third-party or any reasonable expectation that the Services will not contain any content that is prohibited by such rules.
	    	                     </p>
	    	                     <p className="static-page-text"> 
	    	                        You agree not to post, upload to, transmit, distribute, store, create or otherwise publish through the Services any of the following:
	    	                     </p>
	    	                     <ul>
	    	                        <li className="static-page-text">User Content that is libelous, defamatory, profane, obscene, pornographic, sexually explicit, indecent, lewd, vulgar, suggestive, violent, harassing, hateful, threatening, offensive, discriminatory, bigoted, abusive, inflammatory, invasive of privacy or publicity rights, fraudulent, deceptive or otherwise objectionable;</li>
	    	                        <li className="static-page-text">User Content that is illegal or unlawful, that would constitute, encourage or provide instructions for a criminal offense, violate the rights of any party, or otherwise create liability or violate any local, state, national or international law;</li>
	    	                        <li className="static-page-text">User Content that may infringe or violate any patent, trademark, trade secret, copyright, or other intellectual or other right of any party;</li>
	    	                        <li className="static-page-text">User Content that contains or depicts any statements, remarks or claims that do not reflect your honest views and experiences;</li>
	    	                        <li className="static-page-text">User Content that impersonates any person or entity or otherwise misrepresents your affiliation with a person or entity;</li>
	    	                        <li className="static-page-text">Private information of any third-party including, but not limited to, addresses, phone numbers, email addresses, Social Security numbers or credit card numbers;</li>
	    	                        <li className="static-page-text">Unsolicited promotions, political campaigning, or commercial messages (SPAM) or any chain messages;</li>
	    	                        <li className="static-page-text">User Content designed to deceive or trick the user of the Services;</li>
	    	                        <li className="static-page-text">Viruses, corrupted data or other harmful, disruptive or destructive files or code, script or other software designed to automate any functionality on the Services; or</li>
	    	                        <li className="static-page-text">User Content that, in the sole judgment of purvice, is objectionable.</li>
	    	                     </ul>
	    	                     <p className="static-page-text"> 
	    	                        Although purvice does not control and has no obligation to screen, edit or monitor any of the User Content posted, stored or uploaded on (or otherwise made available via) the Services, purvice reserves the right, and has absolute discretion, to remove, screen or edit any User Content posted, stored or uploaded on the Services at any time and for any reason without notice, and you are solely responsible for creating backup copies of and replacing any User Content you post, store or upload on (or otherwise make available via) the Services at your sole cost and expense.  purvice takes no responsibility and assumes no liability for any User Content posted, stored or uploaded on (or otherwise made available via) the Services.
	    	                     </p>
	    	                     <p className="static-page-text"> 
	    	                        Except for any Feedback you provide with respect to the Services, you retain ownership of the User Content you post, store or upload on (or otherwise make available via) the Services.  You hereby grant purvice and our affiliates a non-exclusive, royalty-free, perpetual, irrevocable and fully sublicensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, perform and display such User Content throughout the world in any manner or media, on or off the Services. You represent and warrant that: (i) you own and control all of the rights to the User Content that you post, store or upload on (or otherwise make available via) the Services or you otherwise have the right to make available such User Content via the Services and grant the rights granted in these Terms; (ii) the User Content that you post, store or upload on (or otherwise make available via) the Services is accurate and not misleading; and (iii) purvices use of the User Content you supply does not violate these Terms and will not violate any rights of, or cause injury to, any person or entity.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>10.User Conduct</h5>
	    	                     <p className="static-page-text"> 
	    	                        You are solely responsible for your conduct and the conduct of anyone who uses your account with respect to the Services, and you agree that you will not and will not permit anyone using your account to do any of the following in connection with the Services:
	    	                     </p>
	    	                     <ul>
	    	                        <li className="static-page-text">Use the Services in any manner that could interfere with, disrupt, negatively affect or inhibit other users from fully enjoying the Services or that could damage, disable, overburden or impair the functionality of the Services in any manner or in any way inconsistent with any documentation provided to or made available to you by purvice concerning the Services;</li>
	    	                        <li className="static-page-text">Flag content or report abuse for improper purposes or without good reason;</li>
	    	                        <li className="static-page-text">Use the Services for any illegal or unauthorized purpose or engage in, encourage, or promote any illegal activity, or any activity that violates these Terms or any other rules or polices established from time to time by purvice;</li>
	    	                        <li className="static-page-text">Attempt to indicate in any manner that you have a relationship with us or that we have endorsed you or any products or services for any purpose;</li>
	    	                        <li className="static-page-text">Create an account or post, store or upload (or otherwise make available) any User Content if you are not over 13 years of age;</li>
	    	                        <li className="static-page-text">Use or attempt to use another users account without authorization from such user and purvice;</li>
	    	                        <li className="static-page-text">Modify, adapt, hack or emulate the Services;</li>
	    	                        <li className="static-page-text">Use any robot, spider, crawler, scraper or other automated means or interface not provided by us to access the Services or to extract data;</li>
	    	                        <li className="static-page-text">Develop any third-party applications that interact with User Content or the Services without our prior consent;</li>
	    	                        <li className="static-page-text">Circumvent or attempt to circumvent any filtering, security measures or other features designed to protect the Services or third-parties; and</li>
	    	                        <li className="static-page-text">Infringe upon or violate the rights of purvice, our users or any third-party.</li>
	    	                     </ul>
	    	                     <p className="static-page-text"> 
	    	                        If your account is disabled, you, or anyone acting under your discretion, is/are strictly prohibited from creating another account with purvice.
	    	                     </p>
	    	                     <p className="static-page-text"> 
	    	                        purvice takes no responsibility and assumes no liability for any user conduct, mistakes, defamation, slander, libel, omissions, falsehoods, obscenity, pornography or profanity you may encounter while using the Services.  Your use of the Services is at your own risk. Any use of the Services in violation of the foregoing violates these Terms and may result in, among other things, termination or suspension of your rights to use the Services.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>11.Feedback</h5>
	    	                     <p className="static-page-text"> 
	    	                        You can submit questions, comments, suggestions, ideas, plans, notes, drawings, original or creative materials or other information or materials about purvice and the Services (collectively, "Feedback").  Feedback, whether submitted through the Services or otherwise, is non-confidential and shall become the sole property of purvice.  purvice shall own exclusive rights, including all intellectual property rights, in and to such Feedback and shall be entitled to the unrestricted use and dissemination of this Feedback for any purpose, commercial or otherwise, without acknowledgment or compensation to you.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>12.Disclaimers</h5>
	    	                     <p className="static-page-text"> 
	    	                        YOU EXPRESSLY ACKNOWLEDGE AND AGREE THAT YOUR USE OF THE SERVICES AND purvice CONTENT IS AT YOUR SOLE RISK AND THAT THE ENTIRE RISK AS TO SATISFACTORY QUALITY, PERFORMANCE, SAFETY, ACCURACY AND EFFORT IS WITH YOU. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE SERVICES AND purvice CONTENT ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS. purvice DISCLAIMS ANY AND ALL WARRANTIES AND REPRESENTATIONS (EXPRESS OR IMPLIED, ORAL OR WRITTEN) WITH RESPECT TO THE SERVICES AND THE purvice CONTENT CONTAINED THEREIN, INCLUDING ANY AND ALL: (I) IMPLIED WARRANTIES OF MERCHANTABILITY; (II) IMPLIED WARRANTIES OF FITNESS OR SUITABILITY FOR ANY PURPOSE (WHETHER OR NOT purvice KNOWS, HAS REASON TO KNOW, HAS BEEN ADVISED OR IS OTHERWISE AWARE OF ANY SUCH PURPOSE); AND (III) WARRANTIES OF NON-INFRINGEMENT OR CONDITION OF TITLE. purvice DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THE SERVICES WILL BE ACCURATE OR MEET YOUR REQUIREMENTS, THAT THE OPERATION OF THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ANY DEFECTS IN THE SERVICES WILL BE CORRECTED. NO ORAL OR WRITTEN INFORMATION, GUIDELINES OR ADVICE GIVEN BY purvice OR OUR AUTHORIZED REPRESENTATIVE WILL CREATE A WARRANTY.  SOME JURISDICTIONS DO NOT ALLOW THE DISCLAIMER OF IMPLIED TERMS IN CONTRACTS WITH CONSUMERS, SO SOME OR ALL OF THE DISCLAIMERS IN THIS SECTION 13 MAY NOT APPLY TO YOU.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>13.Indemnification</h5>
	    	                     <p className="static-page-text"> 
	    	                        You agree, at your sole expense, to defend, indemnify and hold us, our service providers and consultants, and our and their respective directors, employees and agents, harmless from and against any and all actual or threatened suits, actions, proceedings (at law or in equity), claims, damages, payments, deficiencies, fines, judgments, settlements, liabilities, losses, costs and expenses (including, but not limited to, reasonable attorney fees, costs, penalties, interest and disbursements) caused by, arising out of, resulting from, attributable to or in any way incidental to: (i) your use of the Services or purvice Content; (ii) your violation of these Terms or the rights of any third-party; or (iii) any User Content you post, upload, use, distribute, store or otherwise transmit on or through the Services.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>14.Limitation of Liability</h5>
	    	                     <p className="static-page-text"> 
	    	                        TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAW, IN NO EVENT WILL purvice BE LIABLE TO YOU OR ANY THIRD-PARTY FOR ANY INCIDENTAL, SPECIAL, INDIRECT, CONSEQUENTIAL, EXEMPLARY, OR PUNITIVE DAMAGES WHATSOEVER INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, LOSS OF DATA, BUSINESS INTERRUPTION OR ANY OTHER COMMERCIAL DAMAGES OR LOSSES, ARISING OUT OF OR RELATED TO THE SERVICES AND THE CONTENT THEREIN, INCLUDING BUT NOT LIMITED TO, THE GIFT CARDS, GIFT PROMOTIONS, RELATED PRODUCTS AND SERVICES (INCLUDING, BUT NOT LIMITED TO, THE REDEEMABILITY OF GIFT CARDS OR GIFT PROMOTIONS), USER CONTENT, purvice CONTENT, THIRD-PARTY SERVICES AND/OR THIRD-PARTY MATERIALS, HOWEVER CAUSED, REGARDLESS OF THE THEORY OF LIABILITY (CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE, WHETHER ACTIVE, PASSIVE OR IMPUTED), PRODUCT LIABILITY, STRICT LIABILITY OR OTHER THEORY) AND EVEN IF purvice HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THIS LIMITATION MAY NOT APPLY TO YOU. IN NO EVENT WILL purvice'S TOTAL LIABILITY, WHETHER IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE, WHETHER ACTIVE, PASSIVE OR IMPUTED), PRODUCT LIABILITY, STRICT LIABILITY OR OTHER THEORY, ARISING OUT OF OR RELATING TO THE USE OF OR INABILITY TO USE THE SERVICES EXCEED FIFTY DOLLARS ($50.00) (AS OPPOSED TO ANY OTHER FEES/COSTS INCLUDING, BUT NOT LIMITED TO, ANY FEES ASSOCIATED WITH YOUR DEVICE, THE COST TO PURCHASE AND OPERATE YOUR DEVICE, THE COST OF ANY GIFT CARDS OR THE COST OF ANY PRODUCTS OR SERVICES PURCHASED WITH A GIFT CARD).
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>15.Limitation of Liability</h5>
	    	                     <p className="static-page-text"> 
	    	                        purvice is based in the United States and our Services are subject to U.S. law.  We make no representations or warranties that the Services are appropriate or available for use in other locations.  If you choose to access or use the Services from locations outside the United States, you do so at your own risk and are responsible for compliance with all applicable laws, rules and regulations. The laws of the State of New York, excluding its conflicts of law rules, govern your use of the Services. Your use of the Services may also be subject to other local, state, national, or international laws. You agree that any action at law or in equity arising out of or relating to the Services or the Terms will be filed only in the state and federal courts located in Suffolk County, New York, and you irrevocably and unconditionally consent and submit to the exclusive jurisdiction of such courts over any suit, action or proceeding arising out of the Services (including, but not limited to, your use of the Services).
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>16.Termination or Modification of Services</h5>
	    	                     <p className="static-page-text"> 
	    	                        purvice reserves the right to change, suspend, remove, discontinue or disable access to the Services at any time and without notice.  In no event will purvice be liable for the removal of or disabling of access to any portion or feature of the Services.                         
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>17.Severability</h5>
	    	                     <p className="static-page-text"> 
	    	                        If any provision of these Terms shall be deemed unlawful, void or for any reason unenforceable, then that provision shall be deemed severable from these Terms and shall not affect the validity and enforceability of any remaining provisions.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>18.Waiver</h5>
	    	                     <p className="static-page-text"> 
	    	                        Enforcement of the Terms is solely in our discretion and our failure to enforce a provision in some instances does not constitute a waiver of our right to enforce such provision in other instances.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>19.Assignment</h5>
	    	                     <p className="static-page-text"> 
	    	                        purvice may assign these Terms and its rights or delegate its obligations under without your consent.  All provisions contained in these Terms shall extend to and be binding upon you and purvice's successors and assigns. You may not assign these Terms to another person or entity.
	    	                     </p>
	    	                     <h5 style={{'font-weight': 'bold','font-size': '18px'}}>20.Contact Us</h5>
	    	                     <p class="static-page-text"> 
	    	                        If you have any questions or concerns regarding these Terms or our Services, please contact us at support@purvice.com
	    	                     </p>
	    	                  </div>
	    	               </div>
	    	            </div>
	    	         </div>
	    	      </div>
	    	   </div>
	    	</section>
		</React.Fragment>
    );
  }
  
  open(){
	  
  }
  
}

export default Terms;
