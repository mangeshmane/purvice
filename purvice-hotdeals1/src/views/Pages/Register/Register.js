import React, { Component } from 'react';
import { Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import logo from '../../../assets/img/purvice_logo_blue.jpg';
import gift1 from '../../../assets/img/gift1.png';
import gift2 from '../../../assets/img/responsive.png';
import './register.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import Env from '../../../environments/environment.js';

var divStyle = {
  color: "red",
  marginTop: "0%"
};
class Register extends Component {
  toastId = null;
  constructor(props) {
    super(props);
    this.state = {
      error:'',
      firstname:false,
      lastname:false,
      passwordState:false,
      passwordStateInvalid:false,
      emailStateInvalid:false,
      emailState:false,
      spinner: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  logIn = ()=>{
    this.props.history.push('/login');
  }

  handleChange(e){
    const { name, value } = e.target;
    if(name === "firstName" ){
      if(value === ""){
      this.setState({firstname:true});
    }else{
      this.setState({firstname:false});
    }
  }
    if(name === "lastName"){
      if(value === ""){
      this.setState({lastname:true});
    }
    else{
    this.setState({lastname:false});
    }
  }
  if(name === "email"){
    if (value === "") {
    this.setState({emailState :true});
    this.setState({emailStateInvalid:false});

  }else if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)){
    this.setState({emailStateInvalid:true});
    this.setState({emailState :false});

  }else{
    this.setState({emailStateInvalid:false});
    this.setState({emailState:false});
  }
  }
  if(name === "password"){
    if(value === ""){
    this.setState({passwordState:true});
    this.setState({passwordStateInvalid:false});
  }else if(!/^\S*$/.test(value)){
    this.setState({passwordState:false});
    this.setState({passwordStateInvalid:true});
  }else{
    this.setState({passwordState:false});
    this.setState({passwordStateInvalid:false});
  }
  }
  this.setState({ [name]: value });
}

  handleSubmit(e){
    e.preventDefault();
    const { firstName, lastName, email, password, returnUrl } = this.state;

      if ((firstName && lastName && email && password)) {
      var data = {
        username:this.state.email,
        password:this.state.password,
        firstName:this.state.firstName,
        lastName:this.state.lastName
      }
      this.setState({spinner:true});
      axios.post(Env.baseUrl + '/register', data,{
        headers:  {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin":"*"
        }
      }).then(function (response) {
        this.setState({spinner:false});
        if (response.status === 200) {
          if(response.data.message === 'SUCCESS'){
            if (!toast.isActive(this.toastId)) {
            this.toastId = toast.success("Success Please check your mail and verify your account.", { position: toast.POSITION.TOP_RIGHT });
          }
          setTimeout(function () { this.props.history.push('/login'); }.bind(this), 3000);
        }else{
          if(response.data.message === 'FAIL'){
            if(response.data.explanation === 'Email address already used.Please use different Email address.'){
            if (!toast.isActive(this.toastId)) 
            this.toastId=toast.error("Email Address Already Exists", { position: toast.POSITION.TOP_RIGHT });
            }
      }
    }
  }
      }.bind(this)).catch(err => {
          if (!toast.isActive(this.toastId)) {
            this.toastId = toast.error(err, { position: toast.POSITION.TOP_RIGHT });
          }
      })
    }else{
      if (!toast.isActive(this.toastId)) 
      this.toastId=toast.error("Please Enter Your Details", { position: toast.POSITION.TOP_RIGHT });
    }
    
  }

  goToHotDeals = ()=>{
    this.props.history.push('/hotdeals');
  }

  render() {
    return (
      <div className="mt-5 align-items-center">
      <ToastContainer autoClose = {2500}/>
        <Container>
          <Row className="justify-content-center">
            <Col md="5" lg="5" xl="5">
              <Card className="mx-4 mt-1">
                <CardBody className="p-4">
                  <Form>
                  <div style={{ display: 'flex', justifyContent: 'center' }}><img src={logo} style = {{height:'50px',cursor:'pointer'}} alt="logo" onClick = {this.goToHotDeals}/></div>
                  <h5 style={{ display: 'flex', justifyContent: 'center',marginTop:'0.5rem',marginBottom:'1rem' }}>
                  It's Easy to Get Started And It's Free!
                  </h5>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="First Name" name = "firstName" autoComplete="firstname" onChange = {this.handleChange} />
                    </InputGroup>
                    {this.state.firstname && 
                      <InputGroup className="mb-3">
                      <div style={divStyle}>
                          Please enter first name.
                          </div>
                      </InputGroup>
                      }
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="Last Name" name = "lastName" autoComplete="lastname" onChange = {this.handleChange} />
                    </InputGroup>
                    {this.state.lastname && 
                      <InputGroup className="mb-3">
                      <div style={divStyle}>
                          Please enter last name.
                          </div>
                      </InputGroup>
                      }
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>@</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="Email Address" name = "email" autoComplete="email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" onChange = {this.handleChange} />
                    </InputGroup>
                    {this.state.emailStateInvalid && 
                      <InputGroup className="mb-3">
                      <div style={divStyle}>
                          Please enter valid email address.
                          </div>
                      </InputGroup>
                      }

                       {this.state.emailState && 
                      <InputGroup className="mb-3">
                      <div style={divStyle}>
                          Please enter email address.
                          </div>
                      </InputGroup>
                      }

                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" placeholder="Password" name = "password" autoComplete="new-password" onChange = {this.handleChange} />
                    </InputGroup>
      
                    {this.state.passwordState && 
                      <InputGroup className="mb-3">
                      <div style={divStyle}>
                          Please enter password.
                          </div>
                      </InputGroup>
                      }

                      {this.state.passwordStateInvalid && 
                      <InputGroup className="mb-3">
                      <div style={divStyle}>
                          Please enter valid password.
                          </div>
                      </InputGroup>
                      }
                    <Button color="primary" type = "submit"  block style={{fontWeight: '600'}} onClick={this.handleSubmit}><i  className="fa fa-spinner fa-spin  float-right" hidden = {!this.state.spinner}></i>Sign Up With Email</Button>
                    
                <InputGroup className="mb-3 mt-2">
                <p className="noAccount">Already have an account? <a onClick = {this.logIn} style = {{color:'#53bbe2',fontWeight: '600'}}>Log in now.</a></p>
                </InputGroup>
                  </Form>
                </CardBody>
              </Card>
              {this.state.error &&
                        <div className={'alert alert-danger'}>{this.state.error}</div>
                    }
            </Col>

            {/* <Col md="7" lg="7" xl="7">
                    <div className="register-page">
                        <h1>What is purvice?</h1>

                        <div className="list">
                            <div className="icon"><img src={gift1} alt="gift1"/></div>
                            <div>
                                <h3>Send gift cards</h3>
                                <p>Instantly send gift cards to friends and family. Choose from hundreds of popular retailers.</p>
                            </div>
                            <div className="icon"><img src={gift1} alt="login with facebook"/></div>
                            <div>
                                <h3>Manage from anywhere</h3>
                                <p>Save your gift cards in your purvice Wallet to track balances and access them from your computer
                                    or smartphone.</p>
                            </div>
                            <div className="icon"><img src={gift1} alt="login with facebook"/></div>
                            <div>
                                <h3>Redeem with ease</h3>
                                <p>Use your gift cards straight from your phone with the purvice app. Never forget to use your
                                    gift cards again!</p>
                            </div>
                        </div>

                        <img className="responsive" src={gift2} alt="gift2"/>
                    </div>
                </Col> */}
          


          </Row>

         <hr className="mt-5"/>
         <Row className="justify-content-center">
              <Col lg = "5">
            <p className="copy">Copyright © 2018 purvice,Inc. All Rights Reserved.</p>
            </Col>
            <Col lg = "7">
            <p className="footer-link">
	        	<a className="mr-1 link-hove"  onClick = {() =>this.props.history.push('/about-us')}>About Us</a>
	            <a className="mr-1 link-hove"  onClick = {() =>this.props.history.push('/faq')}>FAQ</a>
	            <a className="mr-1 link-hove"  onClick = {() =>this.props.history.push('/privacy')}>Privacy</a>
	            <a className="mr-1 link-hove"  onClick = {() =>this.props.history.push('/terms')}>Terms</a>
	            <a className="mr-1 link-hove"  onClick = {() =>this.props.history.push('/copyright')}>Copyright</a>
            </p>
            </Col>
            </Row>

        </Container>
      </div>
    );
  }
}

export default Register;



{/* <Container>
<Row className="justify-content-center">
  <Col lg="6" md="9">
    <Card className="mx-4" style={{ "width": "90%", "height": "100%", "margin": "0" }}>
      <CardBody className="card-body p-3">
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <div><img src={'assets/img/logo.png'} width="100" height="50" onClick={(e)=> this.redirectToHome(e)} style={{cursor: 'pointer'}}/></div>
        </div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Label check htmlFor="inline-checkbox1">
            <b>Create your account</b>
          </Label>
        </div><br />
        <Form action="" onSubmit={e => this.signUp(e)} encType="multipart/form-data" className="form-horizontal">
          <InputGroup className="mb-2">
            <InputGroupAddon> <InputGroupText><i className="icon-user"></i></InputGroupText></InputGroupAddon>
            <Input type="text" maxlength="20" onChange={this.handleFirstName.bind(this)} placeholder="First name" autoFocus />
          </InputGroup>
          <InputGroup className="mb-1">
            <div style={divStyle} hidden={this.state.fnameState}>
              {this.state.firstnameMessage}
            </div>
          </InputGroup>
          <InputGroup className="mb-2">
            <InputGroupAddon><InputGroupText><i className="icon-user"></i></InputGroupText></InputGroupAddon>
            <Input type="text" maxlength="20" onChange={this.handleLastName.bind(this)} placeholder="Last name" />
          </InputGroup>
          <InputGroup className="mb-1">
            <div style={divStyle} hidden={this.state.lnameState}>
              {this.state.lastnameMessage}
            </div>
          </InputGroup>
          <InputGroup className="mb-2">
            <InputGroupAddon><InputGroupText><i className="icon-envelope-letter"></i></InputGroupText></InputGroupAddon>
            <Input type="text" onChange={this.handleEmail.bind(this)} placeholder="Email" />
          </InputGroup>
          <InputGroup className="mb-1">
            <div style={divStyle} hidden={this.state.emailState}>
            Please enter valid email id.
                </div>
          </InputGroup>
          <InputGroup className="mb-2">
            <InputGroupAddon><InputGroupText><i className="icon-lock"></i></InputGroupText></InputGroupAddon>
            <Input type="password" onChange={this.handlePassword.bind(this)} placeholder="Password" />
          </InputGroup>
          <InputGroup className="mb-1">
            <div style={divStyle} hidden={this.state.passwordState}>
            <Label hidden={!this.state.passwordMessageState}>Please enter valid password.</Label>
              {this.state.passwordMessage}
            </div>
          </InputGroup>
          <InputGroup className="mb-2">
            <InputGroupAddon><InputGroupText><i className="icon-lock"></i></InputGroupText></InputGroupAddon>
            <Input type="password" onChange={this.handleCPassword.bind(this)} placeholder="Confirm Password" />
          </InputGroup>
          <InputGroup className="mb-1">
            <div style={divStyle} hidden={this.state.cpasswordState}>
            Your password and confirm password do not match.
                  </div>
          </InputGroup>
          <InputGroup className="mb-2">
            <Input type="select" onChange={this.selectCountry.bind(this)} name="select" id="exampleSelect" >
              <option>Select Country</option>
              {Country ? Country.map((ct, i) => (
                <option>{ct.name}</option>
              )) : 'Country not found'}
            </Input>
          </InputGroup>

          <InputGroup hidden={this.state.countrystate} className="mb-1">
            <div style={divStyle}>
              Please select country.
                  </div>
          </InputGroup>

          <InputGroup className="mb-2">
            <Col xs="12">
              <Input type="checkbox" onClick={this.termCondition.bind(this)} value="option1" />
              <span >I agree to Yinn's<a href="#/page?title=Terms%20Of%20Use"><u>  Terms & Conditions</u></a></span>
            </Col>
          </InputGroup>

          <InputGroup className="mb-1">
            <div style={divStyle} hidden={this.state.condvalidation}>
              Please agree terms & conditions.
            </div>
          </InputGroup>


          <Button color="warning" onSubmit={(e)=>this.signUp(e)} block><b>Register</b></Button><br />
        </Form>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Label check htmlFor="inline-checkbox1">
            <p>Already have an account? Sign in</p>
          </Label>
        </div>
        <Button color="warning" onClick={this.goToLogin} block><b>Sign In</b></Button>
      </CardBody>
      <CardFooter className="p-4" hidden = {cookie.load('is_user')=== 'seller'}>
        <div style={{ display: 'flex', justifyContent: 'center', marginTop: "-6%" }}>
          or Login using
          </div>
        <Row style={{ marginTop: "4%" }}>
        <Col xs="12" sm="6">
        <TwitterLogin loginUrl="http://yinn.ca/yinn/api/v1/auth/twitter"
          onFailure={this.onFailed}
          onSuccess={this.onSuccess}
          requestTokenUrl="http://yinn.ca/yinn/api/v1/auth/twitter/reverse"
          showIcon={true}
          customHeaders={customHeader}
          className ="twitter-btn"
          >
          <b>twitter</b> 
          </TwitterLogin> 
          </Col>
         
          <Col xs="12" sm="6">
            <GoogleLogin clientId="734749952478-f3089qa9em1sl5qc03kqukv839ebe1hv.apps.googleusercontent.com" buttonText="google" className="google_btn"
              onSuccess={responseGoogle} onFailure={responseGoogle}
            
          </Col>
        </Row>
      </CardFooter>
      <CardFooter hidden = {cookie.load('is_user')!= 'seller'}>
      </CardFooter>  
    </Card>
  </Col>
</Row>
</Container> */}
