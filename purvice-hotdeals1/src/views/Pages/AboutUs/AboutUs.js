import React, { Component } from 'react';
import './AboutUs.css';
import { aboutUsService } from '../../../services/about-us.service';
import Env from '../../../environments/environment.js';

class AboutUs extends Component {

  constructor(props) {
    super(props);
    this.state = {
    		videoLink: Env.videoUrl,
    		videoStatus: false
    };
    this.getVideoStatus();  
  }

  getVideoStatus(){
	  aboutUsService.getVideoStatus().then(response => {
	        this.state.videoStatus = response.result ? response.result.status : false;
	  });
  }
  render() {
    return (
    	<React.Fragment>
	    	<section id="giftcards" className="about-page-top-section" >
	    		<div className="container">
	    			<h1 className="section-title" style={{'color':'#ffffff'}}><br/><br/><br/>
	    	             We believe in gift cards made easy.
	    	        </h1><br/><br/>
	    	 		<h4 className="section-title" style={{'color':'#ffffff','font-weight':'bold'}}>
	    	             Contact us: support@purvice.com
	    	        </h4>      
	    	        
	    	        <br/><br/><br/>
	    		</div>
	    	</section>
	    	<div className="container about-us-parent-container" >
					 <p className="about-us-intro-note">
			         	Welcome to purvice! We're a digital gift card platform that enables you to buy, send, and manage gift cards from hundreds of top retailers.   
			         </p>
			         <p className="about-us-intro-note" style={{'margin-top':'10px'}}>
			         	&nbsp;
			         </p>
			</div>
			<div className="container about-us-middle-parent">
			    <div className="row">
			         <div className="col-4"></div>
			          <div className="col-4  about-page-middle-section-inner">
			             <img className="column-image" src={require('../../../assets/img/about-left.png')} alt="For You" />
			          <h3 className="section-column-title">For You</h3>
			          <p>Send, buy and manage gift cards from any device. Choose gift cards from hundreds of retailers.</p>
			          </div>
			    </div>
			</div>
			<hr/>
			{this.state.videoStatus
				? <div id="aboutus" className="container">
				    <p className="about-page-video-section-title">As seen on Ellen, Forbes, and more</p>
				    <div style={{'text-align':'center'}}>
				       <a href={this.state.videoLink} className="video-link" style={{'width':'280px','height':'157px'}}>
							<img className="video-placeholder" src={require('../../../assets/img/videoImage.jpg')} alt="purvice!" />
						</a>
				    </div>  
				 </div>
				: null
			}
			
			{this.state.videoStatus
				? <hr/>
				: null
			}
			
		</React.Fragment>
    );
  }
  
  open(){
	  
  }
  
}

export default AboutUs;
