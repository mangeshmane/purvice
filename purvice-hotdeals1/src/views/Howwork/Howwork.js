import React, { Component } from 'react';
import 'react-toastify/dist/ReactToastify.css';
import logo from '../../assets/img/logo_purvice.png';
import registerImg from '../../assets/img/register.jpg';
import categoryImg from '../../assets/img/category.jpg';
import alertsImg from '../../assets/img/alerts.jpg';

class Howwork extends Component {

    constructor(props) {
        super(props);
        this.state = {
 
        };
        
    }

    render() {
        return (
            <div className="align-items-center">
            <section id="how_work_height" className="work_layout pb-5 pt-5">
        <div className="container">
                <div className="logo_how mb-4" onClick = {() =>this.props.history.push('/hotdeals')} style={{'cursor': 'pointer'}}>
                    <img src={logo}/>
                </div>
                <div className="work_font mb-2">
                        <strong>Deal Alerts save you time by doing <br/> the deal hunting for you.</strong>
                </div>
                <div className="work_font_banner mb-4">
                        <p>Tell us what you’re looking for and we’ll send you an email or push <br/> notification when a matching deal is found.</p>
                </div>
                <div className="get_started_button">
                        <button className="btn" onClick = {() =>this.props.history.push('/deal-alert')}>Get Started</button> 
                </div>
        </div>
</section>
<section className="how_it_works pb-5 pt-5">    
        <div className="container text-center">
                <h3 className="mb-4">How It Works</h3>
                <div className="steps">
                        <div className="separator">
                                <h4 className="border">1</h4>
                        </div>
                        <br/>
                        <img src={registerImg}  className="mt-3 mb-3"/>
                        <p className="mt-3 mb-5">Register and Validate Your Purvice Account.</p>
                </div>
                <div className="steps">
                        <div className="separator">
                                <h4 className="border">2</h4>
                        </div>
                        <br/>
                        <img src={categoryImg}  className="mt-3 mb-3"/>
                        <p className="mt-3 mb-5">Create alerts for Category, Stores or Brands you're intrested in.</p>
                </div>
                <div className="steps">
                        <div className="separator">
                                <h4 className="border">3</h4>
                        </div>
                        <br/>
                        <img src={alertsImg}  className="mt-3 mb-3"/>
                        <p className="mt-3 mb-5 last_step">The deal that matches your alert criteria, we will notify you. <br/> You decide how and when you want to be notified.</p>
                </div>
        </div>
</section>
</div>
        );
    }

}

export default Howwork;
