import React, { Component } from 'react';
import './dealAlert.css';
import {Row,Col} from 'reactstrap';

class DealAlertDisplayComponent extends Component {

  constructor(props) {
    super(props)
     this.state = {
      deal: {},
    
     }
     if(this.props.deal){
       this.state.deal = this.props.deal;
       this.state.deal.createdDate = new Date(this.state.deal.createdDate)
     }
     this.updateDisplayComponent = this.updateDisplayComponent.bind(this)
    }

    updateDisplayComponent(data) {
      this.state.deal = data
      this.state.deal.createdDate = new Date(this.state.deal.createdDate)
    }

  render() {
    
    return(
        <div>
            <Row className="mt-3">
              <Col lg="1" md="3" sm="4">
                <h6 className = "deal pl-2 pr-2 ml-2">Rating </h6> 
              </Col>
              <Col lg="11" md="9" sm="8">
                <h6 className="tagBox pl-2 pr-2 ml-2">{this.state.deal.rating}</h6>  
              </Col>
            </Row> 
                <Row>
                <Col lg="1" md="3" sm="4">
                <h6 className = "deal pl-2 pr-2 ml-2">{this.state.deal.keyword} </h6> 
              </Col>
              <Col lg="11" md="9" sm="8">
              <h6 className="tagBox pl-2 pr-2 ml-2">{this.state.deal.categoryName}</h6>  
            </Col>
            </Row>
             
            <Row className="mb-3" >
              <Col lg="1" md="2" sm="4">
                <h6 className = "deal pl-2 pr-2 ml-2">Notified </h6> 
              </Col>
              <Col lg="11" md="9" sm="8">
                <h6 className="ml-2">{"Instant by '" + this.state.deal.notificationMethod + "'  in Hot Deals created on "+this.state.deal.createdDate.getFullYear()+ "-" + (this.state.deal.createdDate.getMonth() + 1) + "-" + this.state.deal.createdDate.getDate()}</h6>  
              </Col>
            </Row> 
                  
      </div>
        
    )
  }
}



export default DealAlertDisplayComponent;


