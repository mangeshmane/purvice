import React, { Component } from 'react';
import './dealAlert.css';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

import ForumSettingComponent from './ForumSettingComponent'
import CategorySettingComponent from './CategorySettingComponent'
import DealListComponent from './DealListComponent'
import { ToastContainer, toast } from 'react-toastify';
import { dealAlertService } from '../../services/deal-alert.service';
import cookie from 'react-cookies';

import './../HotDeals/HotDeals.css';

class DealAlert extends Component {
  toastId = null; 
  constructor(props) {
    super(props)
    this.state = {
      sharedDealAlert: false,
      dealAlert: null,
      sharedUser: null,
      alertTitle: '',
      categoryName: '',
      rating: '',
      userName: "",
      user: ""
    }
    this.child = React.createRef();
    this.onAlertAdd = this.onAlertAdd.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      sharedDealAlert: !this.state.sharedDealAlert,
    });
  }

  onAlertAdd(data) {
    this.child.current.updateList(data.result)
  }

  componentDidMount(){
    cookie.save('redirectPath', this.props.match.url, { path: '/' });
    this.setState({ user: cookie.load("user") });
    this.setState({ userName: cookie.load("username") });

    let params = this.props.match.params;
    if(params && params.id){
      dealAlertService.getDealAlertById(params.id).then(data => {
        if(data.message === 'SUCCESS'){
          this.setState({dealAlert: data.result[0]}) 
          if(this.state.user.id != this.state.dealAlert.userId){
            this.setState({
              sharedDealAlert: !this.state.sharedDealAlert,
            });
            this.setState({alertTitle: this.state.dealAlert.alertTitle});
            this.setState({categoryName: this.state.dealAlert.categoryName});
            this.setState({rating: this.state.dealAlert.rating});
            this.setState({userName: this.state.dealAlert.userName})
          }else{
           this.toastId = toast.success("Sorry, the deal alert can not be created with same user login. Try with different user login.", { position: toast.POSITION.TOP_RIGHT });
           setTimeout(function () { 
             window.location = "#/deal-alert";
          }, 2400);
          }
      }
    });
  }
}

createDealAlert = (deal) => {
  if(this.state.user.id != deal.userId){
    deal.userId = this.state.user.id;
    delete deal.id;
    dealAlertService.dealAlertSaveByShared(deal).then(data => {
      if(data.message === 'SUCCESS'){
        if(data.result[0].id == null){
          this.toastId = toast.success("Thise Deal Alert already exist in my save deal", { position: toast.POSITION.TOP_RIGHT });
        }else {
          this.toastId = toast.success("The Deal Alert has been created", { position: toast.POSITION.TOP_RIGHT });
        }
        setTimeout(function () { 
          cookie.remove('redirectPath');
          window.location = "#/deal-alert";
      }, 2400);
    }
  });
  }
}

  render() {

    return (
    <div className="container">
      <div className="row">
        <ToastContainer autoClose={2500} />
        {/* <section id="gifts" className="mt-4 gift-margin"> */}
        <div className="container mt-4">
              <div className="row friend-details mb-3rem" style={{borderTopColor: "#f9a71a",borderTopStyle: "solid", borderTopWidth: "7px"}}>
                  <div className="col-lg-8 col-sm-12 col-xs-12 mt-4 ml-4 no-left-margin">
                      <h2>Deal Alerts</h2>
                      <p><b>Let us do the deal hunting for you.</b> Tell us what you're looking for and we'll send you a notification when a matching deal is found.</p>
                 </div>

                  <div className="col-lg-10 col-sm-12 col-xs-12">
                    <ForumSettingComponent onAlertAdd={this.onAlertAdd} ></ForumSettingComponent>
                  </div>

                  {/* <div className="col-lg-10 col-sm-12 col-xs-12"> */}
                      <div className="col-lg-12 col-sm-12 col-xs-12 mb-2 mt-2">
                         <h3 className="mb-1 mt-3 ml-4"> Create Deal Alert for Category &amp; Sub-Category</h3>
                      <div className="content-section implementation">
                         <CategorySettingComponent onAlertAdd={this.onAlertAdd}></CategorySettingComponent>
                      </div>
                      </div>
                  {/* </div> */}

                  <div className="col-lg-12 col-sm-12 col-xs-12 mt-2 mb-2 ml-4 my-alert-drop-width">
                    <h3 className="mb-3 mt-3 my-aler-design">My Deal Alerts</h3>
                    <div className="content-section implementation">
                    <DealListComponent ref={this.child}></DealListComponent>
                  </div>

                </div>
            </div>
            </div>
          </div>
      {/* </section> */}


        <Modal isOpen={this.state.sharedDealAlert} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Deal Alert </ModalHeader>
          <ModalBody>
              <h4 style={{'font-weight': '700'}}>{this.state.userName}_ shared a Deal Alert with you!</h4>
              <div className="deal-alert-info-box">
                <div>

                </div>
                <div >
                    <strong class="alert-title">{this.state.alertTitle}</strong>
                    <strong>Category</strong>&nbsp;{this.state.categoryName}
                    &nbsp;&nbsp;<span class="middot">.</span>&nbsp;&nbsp;
                    <strong>Rating</strong>&nbsp;{this.state.rating} 
                </div>
              </div>
          </ModalBody>
          <ModalFooter>
            <Button color="primary"  onClick={(e) => this.createDealAlert(this.state.dealAlert)}>Add Deal Alert</Button>
          </ModalFooter>
        </Modal>
		</div>
    )
  }
}



export default DealAlert;


