import React, { Component } from 'react';
import { Button, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import './dealAlert.css';
import 'rc-collapse/assets/index.css';
import Collapse, { Panel } from 'rc-collapse';
import { dealAlertService } from '../../services/deal-alert.service';
import ForumSettingComponent from './ForumSettingComponent'
import CategorySettingComponent from './CategorySettingComponent'
import { ToastContainer, toast } from 'react-toastify';
import DealAlertDisplayComponent from './DealAlertDisplayComponent'
import Env from '../../environments/environment.js';
import { CopyToClipboard } from 'react-copy-to-clipboard';

class DealListComponent extends Component {
  toastId = null;  
  constructor(props) {
    super(props)
    this.state = {
      warning: false,
        accordion: true,
        activeKey: [],
        formDetailFlag : true,
        dealEditFlag :false,
        expandDetailsFlag :false,
        previousActiveKeyArray:[],
        dealData:[],
        deleteId:null,
        sharedURL:"",
        sharedModal: false,
        copied: false,
    }
    this.child = React.createRef();
    this.displayAlertchild = React.createRef();
    this.CategoryChild = React.createRef();
    this.updateList = this.updateList.bind(this)
    this.getUserDealAlert = this.getUserDealAlert.bind(this);
    this.getUserDealAlert();
    this.onAlertEdit = this.onAlertEdit.bind(this);
    this.updateDisplayComponent = this.updateDisplayComponent.bind(this);
    this.onCategoryAlertEdit = this.onCategoryAlertEdit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.toggle = this.toggle.bind(this);
    this.deltoggle = this.deltoggle.bind(this);
   }

   toggle() {
    this.setState({
      sharedModal: !this.state.sharedModal,
    });
    this.setState({ copied: false })
  }

  deltoggle() {
    this.setState({
      warning: !this.state.warning,
    });
  }

  onChange = (activeKey) => {
    if(!this.state.expandDetailsFlag && !this.state.dealEditFlag){
        this.setState({
          activeKey,
        });
      }
      if(activeKey != undefined && !this.state.expandDetailsFlag){
        this.setState({
          activeKey,
        });
      }

      let flag = this.state.dealEditFlag
      if(flag){
        this.state.formDetailFlag = true
        this.setState({formDetailFlag : true})
        this.state.dealEditFlag = false
        this.setState({dealEditFlag : false})
      }else{
        this.state.formDetailFlag = false
        this.setState({formDetailFlag : false})
        this.state.expandDetailsFlag = false
        this.setState({expandDetailsFlag : false})
      }
  }

  handleChange(e) {
    this.setState({sharedURL:e.target})
  }

  updateList(data) {
      let dealData = this.state.dealData
      data.push.apply(data, dealData)
      this.state.dealData = data
      this.setState({dealData :data})
  }

  cleare = () => {
    this.setState({
      warning: !this.state.warning,
    });
  }

  sharedCancel = () => {
    this.state.copied = false;
    this.setState({
      sharedModal: !this.state.sharedModal,
    });
  }

  handleDelete = (key) => {
    this.state.expandDetailsFlag = true
    this.setState({
      warning: !this.state.warning,
    });
    this.setState({deleteId: key})
  }

  deleteDealAlert = (key) => {
    dealAlertService.deleteDealAlert(key).then(data => {
      if(data.message === 'SUCCESS'){ 
        this.setState({
          warning: !this.state.warning,
        });
        if (!toast.isActive(this.toastId)) {
            this.toastId = toast.success("Deal alert deleted successfully", { position: toast.POSITION.TOP_RIGHT });
        }
        const dealData = [...this.state.dealData];
        this.setState({ dealData: dealData});
        this.getUserDealAlert();
      }
  });
  }
  

  handlePause = (key,btn) => {
    this.state.expandDetailsFlag = true
    dealAlertService.pauseDealAlert(key).then(data => {
      if(data.message === 'SUCCESS'){ 
        if(btn=="pause"){
          this.toastId =toast.success("Deal Alert have been paused", { position: toast.POSITION.TOP_RIGHT });  
        }else{
          this.toastId =toast.success("Deal Alert has Resumed",{ position: toast.POSITION.TOP_RIGHT });  
        }
        this.getUserDealAlert();       
      }
  });
  }

  handleShare = (key) => {
    this.state.expandDetailsFlag = true;
    this.shareData(key);
  }

  shareData = (key) => {
    this.setState({sharedURL:Env.sharedUrl+"#/deal-alert/"+key.id})
    this.setState({
      sharedModal: !this.state.sharedModal,
    });
  }

  handleEdit = (e,activeKey) => {
    if(this.state.accordion)
    {
    this.state.dealEditFlag = true;
    this.setState({dealEditFlag : true});
    }
  }
  
  dealAlertEdit =()=>{
      this.refs.categorySettingComponent.dealAlertEdit();
  }

  dealAlertEditByKeyword =()=>{
      this.refs.forumSettingComponent.dealAlertEdit();
  }

  getUserDealAlert(){
        dealAlertService.getUserDealAlert().then(data => {
            if(data.message === 'SUCCESS'){   
                this.setState({dealData:data.result});
            }
        });
   }

   parentCancelMethod = () =>{
        this.state.dealEditFlag = false
        this.setState({dealEditFlag : false})
        let flag = this.state.dealEditFlag
        if(flag){
        this.state.formDetailFlag = true
        this.setState({formDetailFlag : true})
        this.state.dealEditFlag = false
        this.setState({dealEditFlag : false})
        }else{
        this.state.formDetailFlag = false
        this.setState({formDetailFlag : false})
        this.state.expandDetailsFlag = false
        this.setState({expandDetailsFlag : false})
      }
   }
  
   onAlertEdit (data,index) {
    this.setState({
        activeKey : !this.state.activeKey
      });
    this.child.current.onAlertEditData(data.result)
  }

  updateDisplayComponent (data) {
     let dealData = this.state.dealData
     for(var i in dealData){
         if (dealData[i].id === data.id){
             dealData[i] = data;
             break
         }
     }
     this.state.dealData = dealData
     this.setState({dealData :dealData})
  }

  onCategoryAlertEdit (data) {
    this.setState({
        activeKey : !this.state.activeKey
      });
    this.CategoryChild.current.onCategoryAlertEditData(data.result)
  }

  render() {
    return(
      <div>
            {
            this.state.dealData.map((deal, index)=>(
                <Collapse
                    accordion={this.state.accordion}
                    onChange={this.onChange}
                    activeKey={this.state.activeKey}
                   
                >
                  
                    <Panel  key={index} extra={<span className="row">
                        <div className="col-lg-8 col-md-5 margin-top-panel">{deal.name === "" ? deal.alertTitle : deal.name}</div> 
                        <div className="col-lg-4 col-md-7">
                        <button className = "btn btn-primary btn-small" onClick = {this.handleEdit.bind(this,index)}><i className="fa fa-pencil" aria-hidden="true"></i> Edit</button>
                            &nbsp;
                            {deal.active === true &&
                                <button className = "btn btn-primary btn-small" onClick={(e) => this.handlePause(deal.id,'pause')}><i className="fa fa-pause" aria-hidden="true"></i> Pause</button>
                            }
                            {deal.active === false &&
                                <button className = "btn btn-primary pause-btn-size btn-small" onClick={(e) => this.handlePause(deal.id,'resume')}><i className="fa fa-play" aria-hidden="true"></i> Resume</button>
                            }
                            &nbsp;
                            
                                <button className = "btn btn-primary btn-small" onClick={() => this.handleShare(deal)}><i class="fas fa-share-alt" aria-hidden="true"></i> Share</button>
                            &nbsp;
                            
                                <button className = "btn btn-primary btn-small" onClick={(e) => this.handleDelete(deal.id)}><i class="fas fa-trash-alt" aria-hidden="true"></i> Delete</button>
                          </div>
                        </span>}>
                        {this.state.formDetailFlag &&
                        <Collapse defaultActiveKey={index} >
                            {
                                deal.createdBy === 'category' &&
                                <CategorySettingComponent deal={deal} parentCancelMethod={this.parentCancelMethod}  editButtons = {true} ref = "categorySettingComponent" ref={this.CategoryChild} updateDisplayComponent = {this.updateDisplayComponent} onCategoryAlertEdit = {this.onCategoryAlertEdit}></CategorySettingComponent>
                            }
                            {
                                deal.createdBy === 'keyword' &&
                                <ForumSettingComponent deal={deal} hideBtn = {true} parentCancelMethod={this.parentCancelMethod} ref = "forumSettingComponent" onAlertEdit = {this.onAlertEdit} ref={this.child} updateDisplayComponent = {this.updateDisplayComponent}></ForumSettingComponent>
                            }
                        </Collapse>
                        }
                        {!this.state.formDetailFlag &&
                         <Collapse defaultActiveKey={index}>
                                <DealAlertDisplayComponent deal={deal} ref={this.displayAlertchild} ></DealAlertDisplayComponent>
                        </Collapse>
                        }
                    </Panel>

                </Collapse>

           ))}

        {/* Delete conformation Modal  */}
        <Modal isOpen={this.state.warning} toggle={this.deltoggle} className={this.props.className}>
          <ModalHeader toggle={this.deltoggle}>Delete Deal Alert</ModalHeader>
          <ModalBody>
            Do you really want to delete this deal alert ?
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={(e) => this.deleteDealAlert(this.state.deleteId)}>Delete</Button>
            <Button color="secondary" onClick={this.cleare}>Cancel</Button>
          </ModalFooter>
        </Modal>

        {/* Shared deal alert Modal  */}
        <Modal isOpen={this.state.sharedModal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Share Deal Alert</ModalHeader>
          <ModalBody className="mb-4">
            Deals get better when shared with others.<br />

            <Input value={this.state.sharedURL}
              onChange={({ target: { value } }) => this.setState({ value, copied: false })} className="share-input"/>

            <CopyToClipboard text={this.state.sharedURL}
              onCopy={() => this.setState({ copied: true })}>
              <Button color="primary" className="share-button">Copy</Button>
            </CopyToClipboard>
            {this.state.copied ? <span className="share-copy">Copied to clipboard.</span> : null}
          </ModalBody>
        </Modal>

      </div>
 
    )
  }
}

export default DealListComponent;


