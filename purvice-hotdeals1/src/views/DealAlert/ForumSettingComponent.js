import React, { Component } from 'react';

import './dealAlert.css';
import { MultiSelect } from 'primereact/multiselect';
import { dealAlertService } from '../../services/deal-alert.service';
import { ToastContainer, toast } from 'react-toastify';
import { Slider } from 'antd';
import "antd/dist/antd.css";
import cookie from 'react-cookies';
import PopUp from './PopUp'
import ENV from '../../environments/environment.js';

class ForumSettingComponent extends Component {
  toastId = null;
  constructor(props) {
    super(props)
    this.state = {
      deal: {},
      alertTitle: "",
      notificationMethod: ['Email'],
      frequencies: ['Instantly'],
      target: [],
      keywords: [],
      rating: [1, 2, 3, 4, 5],
      currentRating : 3,
      defaultValue : 2,
      displayMoreOption: true,
      visibleMoreOption: false,
      editData: "",
      tempTreeKeywordMultiple: [],
      treeKeywordMultiple: [],
      shouldHide: true,
      searchKeyData: "",
      user:{},
      optionButton: "More Options...",
      isValid: false,
      selectedCategories :[],
      categoryName:"",
      editCategoryFlag :false,
      userName:"",
      pageNumber: 0,
      pageSize: 7,
      searchTextByFilter: '',
      
    }

      if(this.props.deal){
        this.state.deal = this.props.deal;
        this.state.alertTitle  =  this.state.deal.name;

        if(this.state.deal.rating)
        this.state.currentRating  =  this.state.deal.rating;
       
        if(this.state.deal.frequencies)
         this.state.frequencies  =  this.state.deal.frequencies.split(',');
        var item = {
        categoryImage:null,
        categoryName:this.state.deal.categoryName,
        description:null,
        key:"Keyword",
        tree_path:this.state.deal.categoryName
        }
        let keywords = [];
        keywords.push(item);
       this.state.keywords = keywords ;
       this.state.selectedCategories = keywords;
       this.state.editCategoryFlag = true;
      }

      this.popupChild = React.createRef();
      this.toggleMenu = this.toggleMenu.bind(this);
      this.getAllDealsByKeyword = this.getAllDealsByKeyword.bind(this);
      this.onCategorySearch = this.onCategorySearch.bind(this);
    //  this.getAllDealsByKeyword();
      this.saveDealAlerts = this.saveDealAlerts.bind(this);
      this.dealAlertEdit = this.dealAlertEdit.bind(this);
      this.onAlertEditData = this.onAlertEditData.bind(this);
      this.onCategorySearchByFilter = this.onCategorySearchByFilter.bind(this);
  }

  saveDealAlerts(data){
    this.state.keywords.push(data);
    this.setState(this.state);
    this.saveDealByKeyword();
  }
  toggleClick(){
    if (this.state.optionButton == "Fewer Options...") {
      this.state.optionButton = "More Options...";
      this.setState({optionButton: this.state.optionButton});
      this.state.isValid = false;
    } else {
      this.state.optionButton = "Fewer Options...";
      this.setState({optionButton: this.state.optionButton});
      this.state.isValid = true;
    }
    //localStorage.setItem('isValid', that.isValid);
  }
  componentDidMount() {
    this.setState({ user: cookie.load("user") });
    this.setState({ userName: cookie.load("username") });
  }

  componentWillMount(){
	  document.addEventListener('mousedown', this.handleClick, false);
  }

  componentWillUnmount(){
	  document.removeEventListener('mousedown', this.handleClick, false);
  }

  handleClick = (e)=>{
	if(this.node.contains(e.target)){
		return;
	}
	this.handleClickOutSide();
  }

  handleClickOutSide(){
	this.setState({ shouldHide: true });
  }

  handleTitle = (e) => {
    this.setState({ alertTitle: e.target.value });
  }

  toggleMenu() {
    this.setState({ displayMoreOption: !this.state.displayMoreOption })
  }

  handleSliderChange = (value) => {
    this.setState({ currentRating: value +1 });
  }

  handleScroll = e => {
    let element = e.target
    if (element.scrollHeight - element.scrollTop === element.clientHeight) {
      let param = {};
      param.pageNumber = this.state.pageNumber + 1 ;
      this.setState({pageNumber:param.pageNumber});
      param.pageSize = this.state.pageSize ? this.state.pageSize : ENV.defaultPageSize;
      param.searchText = this.state.searchKeyData ? this.state.searchKeyData : '';
      param.getText = 'handleScroll';
      this.getAllDealsByKeyword(param);
    }
  }

  getAllDealsByKeyword(param) {
    dealAlertService.getAllDealsByKeyword(param).then(data => {
     // this.setState({ tempTreeKeywordMultiple: data.result });
      this.setState({ treeKeywordMultiple: data.result });
      this.setState({pageNumber: this.state.pageNumber});
      this.onCategorySearchByFilter(this.state.searchKeyData,param.getText);
    });
  }

  onCategorySearch(event) {
    let searchText;
    if(event.target != undefined){
     searchText = event.target.value;
     this.state.searchKeyData = event.target.value;
    }else{
      searchText = event;
      this.state.searchKeyData = event;
    }
    let param = {};
    param.pageNumber = this.state.pageNumber ? this.state.pageNumber + 1 : 1;
    param.pageSize = this.state.pageSize ? this.state.pageSize : ENV.defaultPageSize;
    param.searchText = this.state.searchKeyData ? this.state.searchKeyData : '';
    param.getText = 'onCategorySearch';
    this.getAllDealsByKeyword(param);
  }

  onCategorySearchByFilter(searchText,text){
    if (searchText != "") {
      let data = this.state.treeKeywordMultiple.filter(obj => {
        let key = obj.tree_path.toLowerCase();
        if (key.includes(searchText.toLowerCase())) {
         

          let parts = obj.tree_path.split(new RegExp(`(${searchText})`, 'gi'));
          obj.path = <span> { parts.map((part, i) => 
              <span key={i} style={part.toLowerCase() === searchText.toLowerCase() ? { fontWeight: 'bold', 'text-decoration': 'underline' } : {} }>
                  { part }
              </span>)
          } </span>;


          obj.key = 'Category';
          if(text === "handleScroll"){
            this.state.tempTreeKeywordMultiple.push(obj);
          }
          return obj;
        }
      })
      if(text === "onCategorySearch"){
        this.setState({tempTreeKeywordMultiple:[]});
        this.state.tempTreeKeywordMultiple = data;
      }
      this.setState(this.state);
      this.setState({ shouldHide: false });
    } else {
      this.setState(this.state);
      this.setState({ shouldHide: true });
    }
  }

  howWork = () => {
    window.location = "#/howwork";
  }

  saveDealByKeyword = () => {
    let keywordData = this.state.keywords;
    let selectedCategories = this.state.selectedCategories
    if(selectedCategories.length >=1){
      for (var i in selectedCategories){
        keywordData.push(selectedCategories[i])
      }
    }
    var finalAlertData = [];
    let user = this.state.user
    if(keywordData.length >=1){
      if(user !== undefined || user != null){
        for (var i = 0; i < keywordData.length; i++) {
          let finalData = {};
          if(this.state.alertTitle !== ""){
          finalData["name"] = this.state.alertTitle;
          finalData["alertTitle"] = "Any " + this.state.alertTitle;
          }else{
            finalData["name"] = "";
            finalData["alertTitle"] = "Any " + keywordData[i].categoryName;
          }

          finalData["frequencies"] = this.state.frequencies.toString();
          finalData["notificationMethod"] = this.state.notificationMethod[0]
          finalData["rating"] = this.state.currentRating;
     
            finalData["categoryName"] = keywordData[i].categoryName;
            finalData["keyword"] =  keywordData[i].key;

          finalData["userId"] = this.state.user.id;
          finalData["userName"] = this.state.userName;
          finalData["categoryId"] = keywordData[i].id;
          finalData["createdBy"] = "keyword";
          finalAlertData.push(finalData);
        }
        dealAlertService.dealAlertSave(finalAlertData).then(data => {
          if (data.message === 'SUCCESS') {
            this.toastId = toast.success("The Deal Alert has been created", { position: toast.POSITION.TOP_RIGHT });
            finalAlertData = [];
            this.state.alertTitle = "";
            this.state.keywords = [];
            this.state.selectedCategories = [];
            this.props.onAlertAdd(data);
            this.setState({shouldHide: true});
            if (this.state.optionButton == "Fewer Options...") {
              this.state.optionButton = "More Options...";
              this.setState({optionButton: this.state.optionButton});
              this.state.isValid = false;
              this.setState({isValid:this.state.isValid});
            }
            this.state.frequencies = [];
            this.state.frequencies.push('Instantly');
          }
        });
      }else{
        window.location = '#/login';
      }
    }
    else{
      let searchKeyData = this.state.searchKeyData
      if(searchKeyData === ""){
        let message = "You can not save empty alert. Please select keyword"
        this.popupChild.current.togglePopup(message)
      }else{
        var data = {
          categoryImage: null,
          categoryName: searchKeyData,
          description: null,
          key: "Keyword",
          tree_path: searchKeyData
        }
        let message = "It looks like your alert settings may generate a lot of notifications. Are you sure you want to save it as is?"
        this.popupChild.current.togglePopup(message, data)
      }
    }
  }
  onCategoryDselect(item) {
    this.state.searchKeyData = "";
    this.state.selectedCategories = this.state.selectedCategories.filter((obj) => {
      if (obj.categoryName != item.categoryName) {
        return obj;
      }
    });
    this.setState(this.state);
  }

  onCategoryEdit(item){
    this.state.searchKeyData = item.categoryName;
    this.setState({searchKeyData:this.state.searchKeyData});
    //document.getElementById("myInput").value = item.categoryName;
    this.onCategorySearch(this.state.searchKeyData);
  }

  onKeyWordDselect(item) {
    this.state.searchKeyData = "";
    this.state.keywords = this.state.keywords.filter((obj) => {
      if (obj.categoryName != item.categoryName) {
        return obj;
      }
    });
    this.state.selectedCategories = this.state.selectedCategories.filter((obj) => {
      if (obj.categoryName != item.categoryName) {
        return obj;
      }
    });
    this.setState(this.state);
  }

  onCategorySelect(item){
    this.state.searchKeyData = "";
    if(this.state.keywords.length > 0 && typeof item == "string"){
      for(var i = 0; i < this.state.keywords.length; i++){
        if(this.state.keywords[i].categoryName.toLowerCase() == item.toLowerCase()){
          this.state.keywords.splice(i, 1);
        }
      }
      this.setState({keywords:this.state.keywords});
    }
    if(typeof item == "string"){
      var data = {
        categoryImage: null,
        categoryName: item,
        description: null,
        key: "Keyword",
        tree_path: item,
        category_divider: false
      }
      item = data;
      if(this.state.keywords.length ===0){
        this.state.keywords.push(item);
      }else{
        let message = "Would you like to replace your existing keyword for this alert or append the new term to it?\n *Appending to it will require all words to be matched to receive an alert."
        this.popupChild.current.openpup(message, item, false)
      }
      
    }else{
     item.category_divider = true
     var flag = true;
     if(this.state.selectedCategories.length > 0)
         flag = this.state.selectedCategories.includes(item)
     if(!flag){
       if(this.state.editCategoryFlag){
            let message = "Would you like to replace your existing category for this alert or append the new term to it?\n *Appending to it will require all words to be matched to receive an alert."
            this.popupChild.current.openpup(message, item, true)
       }else{
            
            this.state.selectedCategories.push(item);
       }
     }else{
     // this.state.keywords.push(item);
      this.state.selectedCategories.push(item);
      this.setState(this.state);
     }
    }
    
    this.setState(this.state);
    document.getElementById("myInput").value = '';
    this.setState(this.state);
    this.setState({shouldHide: true});
  }
  

  dealAlertEdit = () => {
    if(this.state.selectedCategories.length > 0){
          let categoryData=this.state.selectedCategories;
          var dealAlerData = [];
          let data = {};
          let frequencies = this.state.frequencies.toString()
          if(frequencies !== ""){
            for (var i = 0; i < categoryData.length; i++) {
              let finalData = {};
                if(this.state.alertTitle !== ""){
                finalData["name"] = this.state.alertTitle;
        
                }else{
                  finalData["name"] = "";
                  finalData["alertTitle"] = "Any " + categoryData[i].categoryName;
                }
              for (var frequenciescnt = 0; frequenciescnt < this.state.frequencies.length; frequenciescnt++) {
                if (dealAlerData["frequencies"]) {
                  let alert = dealAlerData["frequencies"];
                  alert.push(this.state.frequencies[frequenciescnt]);
                  dealAlerData["frequencies"] = alert;
                } else {
                  let alert = [];
                  alert.push(this.state.frequencies[frequenciescnt]);
                  dealAlerData["frequencies"] = alert;
                }
                  dealAlerData["categoryName"] = categoryData[i].categoryName;
                  dealAlerData["keyword"] = categoryData[i].key;
              }
        
              finalData["rating"] = this.state.currentRating;
        
        
                data = {
                  categoryName : categoryData[i].categoryName,
                  categoryId : categoryData[i].id,
                  id : this.props.deal.id,
                  alertTitle :finalData["alertTitle"],
                  frequencies: dealAlerData["frequencies"].toString(),
                  name : this.state.alertTitle,
                  rating: finalData["rating"],
                  notificationMethod : this.state.notificationMethod.toString(),
                  createdBy : "keywords",
                  keyword: categoryData[i].key,
                  userId : this.state.user.id,
                  userName : this.state.userName
                }
      }
          dealAlertService.editDealAlert(data).then(data => {
            if(data.message === 'SUCCESS'){
              this.setState({dealData:data.result});
                this.toastId =toast.success("Deal Alerts Changes have been saved", { position: toast.POSITION.TOP_RIGHT });
              this.props.onAlertEdit(data);
        }
        });
    }else{
      let message = "You can not save alert without frequency. Please select 'When'"
        this.popupChild.current.togglePopup(message)
    }
  }else{
    let message = "You can not save empty alert. Please select atleast one Catagory or Keyword "
    this.popupChild.current.togglePopup(message)
  }
}

    callDisplayComponent = () =>{
      this.props.parentCancelMethod();
    }

    appendKeyword = (data) => {
      let keyword = this.state.keywords[0]
      keyword.categoryName = keyword.categoryName + data.categoryName
      keyword.tree_path = keyword.tree_path + data.tree_path
      this.state.keywords.pop()
      this.state.keywords.push(keyword)
      this.setState(this.state);
    }
    resetKeyword = (data) => {
      this.state.keywords.pop()
      this.state.keywords.push(data)
      this.setState(this.state);
    }
    resetCategory = (data) => {
      data.category_divider = false;
      this.state.selectedCategories.pop()
      this.state.selectedCategories.push(data)
      this.setState(this.state);
    }
    onAlertEditData(data){
    this.state.dealData = data
   this.setState({dealData :data})
   this.props.updateDisplayComponent(data)
  }

  render() {
    const moreOptions = this.state.displayMoreOption === false ? "More Options..." : "Fewer Options...";
    const notification = [{ label: 'Email', value: 'Email' }];
    const frequencies = [{ label: 'Instantly', value: 'Instantly' }, { label: 'Daily', value: 'Daily' }];
    const target = [{ label: 'Option 1', value: 'Option 1' }, { label: 'Option 2', value: 'Option 2' }];

    return (
      <div>
        <div style={{ 'display': 'block' }}>
        <PopUp ref={this.popupChild} saveDealAlerts = {this.saveDealAlerts}
        resetKeyword = {this.resetKeyword} resetCategory = {this.resetCategory} appendKeyword = {this.appendKeyword}></PopUp>
        

          
        {
          !this.props.forumOptions &&

          <div className = "row">
          <div className="mb-3 ml-2rem">
          <h6 className = "ui-grid-col-2 deal pl-3 pr-4 alert-label" hidden = {this.state.selectedCategories.length===0 || this.state.editCategoryFlag}>Category </h6> 
          </div>
          <div>
          <div className="mb-3 ml-2rem">
            {this.state.selectedCategories.map((item, index) => (
              <span >
              { item.category_divider &&
              <span>
                <div className="tagBox mt-1"><span className="ml-2">{item.categoryName}</span>
                <span className="mr-2 ml-2" onClick={() => this.onCategoryEdit(item)}><i className="fa fa-pencil" aria-hidden="true" style={{ 'cursor': 'pointer' }}></i></span>
                <span className="mr-2 ml-2" onClick={() => this.onCategoryDselect(item)}><i className="fa fa-times" aria-hidden="true" style={{ 'cursor': 'pointer' }}></i></span>
              </div>
              <span className = "chips-span-or" hidden = {this.state.selectedCategories.length === index+1}>and</span>
              </span>
              }
                </span>
            ))}
          </div>
          </div>
          </div>
          
        }
          {
          !this.props.forumOptions &&
          <div className = "row">
          <div className="mb-3 ml-2rem">
          <h6 className = "ui-grid-col-2 deal pl-3 pr-4 alert-label" hidden = {this.state.keywords.length===0 || this.state.editCategoryFlag}>Keyword </h6> 
          </div>
          <div>
          <div className="mb-3 ml-2rem">
            {this.state.keywords.map((item, index) => (
              <span >
              { !item.category_divider &&
               <div className="tagBox mt-1"> <span className="ml-2">{item.categoryName}</span> 
                <span className="mr-2 ml-2" onClick={() => this.onCategoryEdit(item)}><i className="fa fa-pencil" aria-hidden="true" style={{ 'cursor': 'pointer' }}></i></span>
                <span className="mr-2 ml-2" onClick={() => this.onKeyWordDselect(item)}><i className="fa fa-times" aria-hidden="true" style={{ 'cursor': 'pointer' }}></i></span>
              </div>
              }
                </span>
            ))}
          </div>
          </div>
          </div>
          
        }
        </div>
       
        {
          !this.props.forumOptions &&
        <div className="col-lg-10 col-sm-12 col-xs-12" ref={node=>this.node=node}>
          <input type="text" id="myInput" className = "my-Input no-left-margin" autoComplete="off" placeholder="Add Keyword and/or Category" value = {this.state.searchKeyData}  onChange={this.onCategorySearch} />
          <ul className={this.state.shouldHide ? 'dropDownList' : 'dropDownLists'} onScroll={this.handleScroll}>
            <li onClick={() => this.onCategorySelect(this.state.searchKeyData)} style={{ 'cursor': 'pointer' }}>
              <div className="ui-helper-clearfix" style={{ 'border-bottom': '1px solid #D5D5D5' }}>
                <span className="m-2 pl-1 pr-1 drop-tag-box"> Keyword</span>
                <span className="m-2 highlight" style={{ 'font-size': '16px' }}> {this.state.searchKeyData}</span>
                <span className="mr-4 mt-2" style={{ 'float': 'right', 'font-size': '2.0em;' }}><i className="fa fa-2x fa-plus-square"
                  aria-hidden="true" style={{ 'color': '#999', 'vertical-align': 'middle' }}></i></span>
              </div>
            </li>
            {this.state.tempTreeKeywordMultiple.map((item, index) => (
              <li onClick={() => this.onCategorySelect(item)} style={{ 'cursor': 'pointer' }}>
                <div className="ui-helper-clearfix" style={{ 'border-bottom': '1px solid #D5D5D5' }}>
                  <span className="m-2 pl-1 pr-1 drop-tag-box"> {item.key}</span>
                  <span className="m-2 forum-keyword-setting"> {item.path}</span>
                  <span className="mr-4 mt-2" style={{ 'float': 'right', 'font-size': '2.0em;' }}><i className="fa fa-2x fa-plus-square"
                    aria-hidden="true" style={{ 'color': '#999', 'vertical-align': 'middle' }}></i></span>
                </div>
              </li>
            ))}
          </ul>
        </div>
        }
         <div className="col-lg-10 col-sm-12 col-xs-12 mt-2 ml-3 optionbutton-hover" style={{'color': '#20a8d8', 'font-style': 'normal', 'font-weight': '600'}} onClick={() => this.toggleClick()}>{this.state.optionButton}</div>
       {
         !this.props.forumOptions && this.state.isValid &&
         
        <div className="col-lg-12 col-sm-12 col-xs-12">
          {
            this.state.visibleMoreOption &&
            <a onClick={this.toggleMenu} style={{ color: '#53bbe2', fontWeight: '600' }}>{moreOptions}</a>
          }
 
          {
            this.state.displayMoreOption &&
            <div >
              <div className="col-lg-12 col-sm-12 col-xs-12 mt-2" style={{'max-width': '86.66667%'}}>
                <label><strong>Title / Short Name (optional)</strong></label><br />
                <input className="form-control" type="text" name="title" autoComplete="off" value = {this.state.alertTitle} onChange={this.handleTitle} />
              </div>
              <div className="col-md-10 col-sm-12 col-xs-12 mt-3 mb-2 row">
                <div className="col-lg-3 col-md-12 col-sm-12 col-xs-12" >
                  <label><strong>Target Forum</strong></label>
                  <div className="forumClass">
                    <MultiSelect value={this.state.target} options={target} filter={false} onChange={(e) => this.setState({ target: e.value })}
                      className="multiselect-width" placeholder="Choose" />
                  </div>
                </div>
                <div className="col-lg-3 col-md-12 col-sm-12 col-xs-12" >
                  <label><strong>Notification Method</strong></label>
                  <div className="forumClass">
                    <MultiSelect value={this.state.notificationMethod} options={notification} onChange={(e) => this.setState({ notificationMethod: e.value })}
                      className="multiselect-width" placeholder="Choose" />
                  </div>
                </div>
                <div className="col-lg-3 col-md-12 col-sm-12 col-xs-12" >
                  <label><strong>When</strong></label>
                  <div className="forumClass">
                    <MultiSelect value={this.state.frequencies} options={frequencies} onChange={(e) => this.setState({ frequencies: e.value })}
                      className="multiselect-width" placeholder="Choose" />
                  </div>
                </div>
                <div className="col-lg-3" ></div>
              </div>

              {!this.props.hideBtn &&
              <div className="col-lg-10 col-sm-12 col-xs-12 mt-2 rating_slider">
                <label><strong>Rating</strong></label><br />
                <div className="range-slider">
                  <Slider max={4} min={0} tooltipVisible={false} marks={this.state.rating} defaultValue={this.state.defaultValue} onChange={this.handleSliderChange} />
                </div>
              </div>
              }

              {
                this.props.hideBtn &&
                <div className="col-lg-10 col-sm-12 col-xs-12 mt-2 rating_slider">
                <label><strong>Rating</strong></label><br />
                <div className="range-slider">
                  <Slider max={4} min={0} tooltipVisible={false} marks={this.state.rating} defaultValue={this.state.currentRating - 1} onChange={this.handleSliderChange} />
                </div>
              </div>
              }
            </div>
          }
        </div>
       }
          { !this.props.forumOptions && ! this.props.hideBtn &&
          <div className="col-lg-10 col-sm-12 col-xs-12 mt-4 mb-4 ml-3">
          <button type="button" className="btn mr-1 px-4 mt-2 forum-button butoon-size" onClick={this.saveDealByKeyword}> Add Alert</button>
          <button type="button" className="btn px-4 mt-2 forum-button " onClick={this.howWork}>How it Works</button>
        </div>
          }
         {
            this.props.hideBtn &&
          <div className="col-lg-10 col-sm-12 col-xs-12 mt-4 mb-4">
          <button type="button "
            className="btn btn-lg btn-primary mr-1 px-4 mt-2 login-button forum-button" onClick = {this.dealAlertEdit}> Save Deal Alert</button>
          <button type="button"
            className="btn btn-primary btn-lg px-4 mt-2 login-button forum-button" onClick = {this.callDisplayComponent}>Cancel</button>
        </div>
          }

            { this.props.forumOptions &&
            <div>
              <div className="col-md-10 col-sm-12 col-xs-12 mt-2">
                <label><strong>Title / Short Name (optional)</strong></label><br />
                <input className="form-control" type="text" autoComplete="off" name="title" onChange={this.handleTitle} />
              </div>
              <div className="col-md-10 col-sm-12 col-xs-12 mt-2 mb-2 row">
                <div className="col-lg-4 col-md-12 col-sm-12 col-xs-12" >
                  <label><strong>Target Forum</strong></label>
                  <div className="forumClass">
                    <MultiSelect value={this.state.target} options={target} filter={false} onChange={(e) => this.setState({ target: e.value })}
                      style={{ minWidth: '12em' }} placeholder="Choose" />
                  </div>
                </div>
                <div className="col-lg-4 col-md-12 col-sm-12 col-xs-12" >
                  <label><strong>Notification Method</strong></label>
                  <div className="forumClass">
                    <MultiSelect value={this.state.notificationMethod} options={notification} onChange={(e) => this.setState({ notificationMethod: e.value })}
                      style={{ minWidth: '12em' }} placeholder="Choose" />
                  </div>
                </div>
                <div className="col-lg-4 col-md-12 col-sm-12 col-xs-12" >
                  <label><strong>When</strong></label>
                  <div className="forumClass">
                    <MultiSelect value={this.state.frequencies} options={frequencies} onChange={(e) => this.setState({ frequencies: e.value })}
                      style={{ minWidth: '12em' }} placeholder="Choose" />
                  </div>
                </div>
              </div>
              <div className="col-lg-10 col-sm-12 col-xs-12 mt-2 rating_slider">
                <label><strong>Rating</strong></label><br />
                <div className="range-slider">
                  <Slider max={4} min={0} tooltipVisible={false} marks={this.state.rating} defaultValue={this.state.defaultValue} onChange={this.handleSliderChange} />
                </div>
              </div>
            </div>
            } 

      </div>
    )
  }
}



export default ForumSettingComponent;


