import React, { Component } from 'react';
import './dealAlert.css';
import {Tree} from 'primereact/tree';
import { dealAlertService } from '../../services/deal-alert.service';
import { Input} from 'reactstrap';
import update from 'react-addons-update';
import { ToastContainer, toast } from 'react-toastify';
import cookie from 'react-cookies';
import { MultiSelect } from 'primereact/multiselect';
import { Slider } from 'antd';
import PopUp from './PopUp'
var categorySettingComponentRef;

class CategorySettingComponent extends Component {
   toastId = null; 
  constructor(props) {
    super(props)
     this.state = {
      deal: {},
      dealData:[],
      categories:[],
      chips: [],
      isTreeShow:false,
      tempTreeKeywordMultiple:[],
      treeKeywordMultiple:[],
      expandedKeys:{},
      frequencies:['Instantly'],
      notificationMethod:['Email'],
      rating : [1,2,3,4,5],
      currentRating : 3,
      defaultValue : 2,
      KEY:   {
        backspace: 8,
        tab:       9,
        enter:     13
      },
      chipEdit:false,
      user:{},
      editButtonFlag:false,
      disableChips:false,
      editData:{},
      showInput:true,
      alertTitle: "",
      editCategoryFlag :false,
      userName:""
     }
     if(this.props.deal){
       this.state.deal = this.props.deal;

       var item = {
        key:this.state.deal.id,
        label:this.state.deal.categoryName
        }
        let chips = [];
        chips.push(item);
       this.state.chips = chips ;

       this.state.alertTitle  =  this.state.deal.name;
        this.state.currentRating  =  this.state.deal.rating;
       
        if(this.state.deal.frequencies)
         this.state.frequencies  =  this.state.deal.frequencies.split(',');
      this.state.editCategoryFlag = true;
     }
     this.popupChild = React.createRef();
     this.loadOnExpand = this.loadOnExpand.bind(this);
     this.getCategories = this.getCategories.bind(this);
     this.dealAlertSave = this.dealAlertSave.bind(this);
     this.dealAlertEdit = this.dealAlertEdit.bind(this);
     this.getCategories();

    }

componentDidMount(){
  categorySettingComponentRef = this;
  this.setState({user:cookie.load("user")});
  this.setState({ userName: cookie.load("username") });
}    

getCategories(){
    dealAlertService.getAllDealCategorys().then(category => {
      if(category.message === 'SUCCESS'){   
          this.setState({categories:this.prepareTreeTablesRoot(category.result)});  
      }
    });
  }    

  
loadOnExpand(event) {
  if (event.node) {
      dealAlertService.getDealCategoryByParentId(event.node.data.id).then(subCategory => {
        if(subCategory.message === 'SUCCESS'){   
          event.node.children = this.prepareTreeTablesChild(subCategory.result);
          this.setState({children:event.node.children});
        }
    });
  }
}

prepareTreeTablesRoot(dealCategory){
  var result = [];
  for (var i = 0; i < dealCategory.length; i++) {
    var data = {};
    data["data"] = dealCategory[i];
    data["key"] = dealCategory[i].id;
    data["label"] = dealCategory[i].categoryName;
    data["leaf"] = false;
    result.push(data);
  }
  return result;
}

prepareTreeTablesChild(dealCategory){
  var result = [];
  for (var i = 0; i < dealCategory.length; i++) {
    var data = {};
    data["data"] = dealCategory[i];
    data["key"] = dealCategory[i].id;
    data["label"] = dealCategory[i].categoryName;
    data["leaf"] = false;
    result.push(data);
  }
  return result;
}

handleSearch=(event)=>{
    let query = event.target.value;
    dealAlertService.searchDealCategory(query).then((data) => {
      this.setState({categories: this.prepareTreeTablesRoot(data.result)});
    });
  }

onFocus=()=>{
  this.setState({isTreeShow : true});
  this.setState({disableChips:false});
}


keyPress=(event)=>{
  let keyPressed = event.which;
  if (keyPressed === this.state.KEY.enter ||
    (keyPressed === this.state.KEY.tab && event.target.value)) {
  event.preventDefault();
  this.updateChips(event);
} else if (keyPressed === this.state.KEY.backspace) {
  let chips = this.state.chips;

  if (!event.target.value && chips.length) {
    this.deleteChip(chips[chips.length - 1]);
  }
}
}

handleDeleteOptions(index){
 let chips = this.state.chips;
 if (chips.length) {
   this.deleteChip(chips[index]);
 }
}

deleteChip(chip) {
 let index = this.state.chips.indexOf(chip);
 
 if (index >= 0) {
   this.setState({
     chips: update(
       this.state.chips,
       {
         $splice: [[index, 1]]
       }
     )
   });
 }
}

handleDeleteChips=(index)=>{
 let chips = this.state.dealData;
 if (chips.length) {
   this.deleteChipData(chips[index]);
 }
}

deleteChipData(chip) {
let index = this.state.dealData.indexOf(chip);

if (index >= 0) {
  this.setState({
    dealData: update(
      this.state.dealData,
      {
        $splice: [[index, 1]]
      }
    )
  });
}
}

updateChips(event) {
 if (!this.props.max || 
     this.state.chips.length < this.props.max) { 
   let value = event.target.value;

   if (!value) return;

   let chip = value.trim().toLowerCase();

   if (chip && this.state.chips.indexOf(chip) < 0) {
     this.setState({
       chips: update(
         this.state.chips,
         {
           $push: [chip]
         }
       )
     });
   }
 }
 event.target.value = '';
}

onSelect=(event)=> {
  if(this.state.editCategoryFlag && this.state.chips.length > 0){
    let message = "Would you like to replace your existing category for this alert or append the new term to it?\n *Appending to it will require all words to be matched to receive an alert."
    this.popupChild.current.openpup(message, event.node, true)
  }else{
    if(!this.state.chips.includes(event.node)){
      this.state.chips.push(event.node);
    }
    this.setState({chips:this.state.chips});
    this.setState({isTreeShow:false});
  }
}
resetCategory = (data) => {
  this.state.chips.pop()
  var tempData = {
    key: data.data.id,
    label: data.data.categoryName
  }
  this.state.chips.push(tempData);
  this.setState(this.state);
}
dealAlertSave(){
  let categoryData=this.state.chips;
  var finalAlertData = [];
  var dealAlerData = [];
  let user = this.state.user
  if(categoryData.length >=1){
    if(user !== undefined || user != null){
      for (var i = 0; i < categoryData.length; i++) {
        let finalData = {};
        finalData["alertTitle"] = categoryData[i].label;
          if(this.state.alertTitle !== ""){
            finalData["name"] = this.state.alertTitle;
    
            }else{
              finalData["name"] = "";
              finalData["alertTitle"] = "Any " + categoryData[i].label;
            }
          
          for (var frequenciescnt = 0; frequenciescnt < this.state.frequencies.length; frequenciescnt++) {
            if (dealAlerData["frequencies"]) {
              let alert = dealAlerData["frequencies"];
              alert.push(this.state.frequencies[frequenciescnt]);
              dealAlerData["frequencies"] = alert;
            } else {
              let alert = [];
              alert.push(this.state.frequencies[frequenciescnt]);
              dealAlerData["frequencies"] = alert;
            }
          }
          finalData["frequencies"] = dealAlerData.frequencies.toString();
          finalData["notificationMethod"] = this.state.notificationMethod[0]
          finalData["rating"] = this.state.currentRating;
        finalData["userId"] = this.state.user.id;
        finalData["userName"] = this.state.userName;
        finalData["categoryId"] = categoryData[i].key
        finalData["categoryName"] = categoryData[i].label
        finalData["createdBy"] = "category";
        finalData["keyword"] = "category";
        finalAlertData.push(finalData);
      }
    
      dealAlertService.dealAlertSave(finalAlertData).then(data => {
        if(data.message === 'SUCCESS'){
          this.setState({dealData:data.result});
          this.toastId =toast.success("The Deal Alert has been created", { position: toast.POSITION.TOP_RIGHT });
          this.props.onAlertAdd(data);
          document.getElementById("tree_div").value = "";
          this.clearAll();
        }
      });
    }else{
      window.location = '#/login';
    } 
  }else{
    let message = "You can not save empty alert. Please select category"
        this.popupChild.current.togglePopup(message)
  }
     
}

dealAlertEdit(){

  if(this.state.chips.length > 0){
        let categoryData=this.state.chips;
        let data = {};
        var dealAlerData = [];
        let frequencies = this.state.frequencies.toString()
        if(frequencies !== ""){
          for (var i = 0; i < categoryData.length; i++) {
            if(this.state.alertTitle !== ""){
              dealAlerData["name"] = this.state.alertTitle;
        
              }else{
                dealAlerData["name"] = "";
                dealAlerData["alertTitle"] = "Any " + categoryData[i].label;
              }
            for (var frequenciescnt = 0; frequenciescnt < this.state.frequencies.length; frequenciescnt++) {
              if (dealAlerData["frequencies"]) {
                let alert = dealAlerData["frequencies"];
                alert.push(this.state.frequencies[frequenciescnt]);
                dealAlerData["frequencies"] = alert;
              } else {
                let alert = [];
                alert.push(this.state.frequencies[frequenciescnt]);
                dealAlerData["frequencies"] = alert;
              }
          }
          dealAlerData["rating"] = this.state.currentRating;
        
          data = {
            categoryName : categoryData[i].label,
            categoryId : this.state.selectedNodeKey,
            id : this.props.deal.id,
            alertTitle :dealAlerData["alertTitle"],
            frequencies: dealAlerData["frequencies"].toString(),
            name : dealAlerData["name"],
            rating: dealAlerData["rating"],
            notificationMethod : this.state.notificationMethod.toString(),
            createdBy : "category",
            keyword: "category",
            userId : this.state.user.id,
            userName : this.state.userName
          }
        }
              dealAlertService.editDealAlert(data).then(data => {
                if(data.message === 'SUCCESS'){
                  this.setState({dealData:data.result});
                  this.toastId =toast.success("Deal Alerts Changes have been saved", { position: toast.POSITION.TOP_RIGHT });
                // this.props.onAlertAdd(data);
                  //setTimeout(() => {window.location.reload()}, 2500);
                  this.props.onCategoryAlertEdit(data);
              }  
            });
        }else{
          let message = "You can not save alert without frequency. Please select 'When'"
            this.popupChild.current.togglePopup(message)
        }
    }else{
      let message = "You can not save empty alert. Please select atleast one Catagory.";
      this.popupChild.current.togglePopup(message)
    }
  }

handleEditDeleteOptions(){
  this.setState({chipEdit : true });
}

clearAll = () => {
  this.setState({chips:[]});
}

handleTitle = (e) => {
  this.setState({ alertTitle: e.target.value });
}

handleSliderChange = (value) => {
  this.setState({ currentRating: value + 1 });
}

callDisplayComponent = () =>{
  this.props.parentCancelMethod();
}

componentWillMount(){
  document.addEventListener('mousedown', this.handleClick, false);
}

componentWillUnmount(){
  document.removeEventListener('mousedown', this.handleClick, false);
}

onCategoryAlertEditData(data){
this.state.dealData = data
this.setState({dealData :data})
this.props.updateDisplayComponent(data)
}
handleClick = (e)=>{
if(!this.node.contains(e.target)){
  if(e.target.id === 'tree_div' || e.target.id === 'the_div_alert'){
  }else{
    this.state.isTreeShow = false
    this.setState({isTreeShow :false})
  }
  
  return;
}
}
  render() {
    
    let chips = this.state.chips.map((chip, index) => {
      return (
        <span>
        {!this.state.disableChips &&

<span>
<div className="tagBox mt-1"><span className="ml-2">{chip.label}</span>

<span className="mr-2 ml-2" onClick={this.handleDeleteOptions.bind(this,index)}><i className="fa fa-times" aria-hidden="true" style={{ 'cursor': 'pointer' }}></i></span>
</div>
<span className = "chips-span-or" hidden = {this.state.chips.length === index+1}>and</span>
</span>
}
</span>
      );
    });

    const notification = [{ label: 'Email', value: 'Email' }];
    const frequencies = [{ label: 'Instantly', value: 'Instantly' }, { label: 'Daily', value: 'Daily' }];
    const target = [{ label: 'Option 1', value: 'Option 1' }, { label: 'Option 2', value: 'Option 2' }];


    return(
      <div className="col-lg-8 col-sm-12 col-xs-12 ml-3 category-setting-width">
                <PopUp ref={this.popupChild} resetCategory = {this.resetCategory} ></PopUp>
              <div className="chip">
                      {chips}
                </div>
                <div ref={node=>this.node=node} id = "tree_div1">
	      			<Input id="tree_div" type="text" onKeyDown= {this.keyPress} onChange = {this.handleSearch} onClick={this.onFocus} placeholder="Add one or more categories"/> 
	                {this.state.isTreeShow
	      			 ? <div id = "the_div_alert" >
                 <Tree value={this.state.categories} onExpand={this.loadOnExpand}
	                    expandedKeys={this.state.expandedKeys} 
	                    onToggle={e => this.setState({expandedKeys: e.value})}  onSelect={this.onSelect}   selectionMode="single" selectionKeys={this.state.selectedNodeKey} onSelectionChange={e => this.setState({selectedNodeKey: e.value})} className = "tree-list" />
                 </div>
	      			 : null
	                }
	  			</div>
                
                {
                  this.props.editButtons && 
                <div >
                <div className="col-md-10 col-sm-12 col-xs-12 mt-2">
                <label><strong>Title / Short Name (optional)</strong></label><br />
                <input className="form-control" type="text" name="title" autoComplete="off" value = {this.state.alertTitle} onChange={this.handleTitle} />
              </div>
              <div className="col-md-10 col-sm-12 col-xs-12 mt-2 mb-2 row">
                <div className="col-lg-4 col-md-12 col-sm-12 col-xs-12" >
                  <label><strong>Target Forum</strong></label>
                  <div className="forumClass">
                    <MultiSelect className="multiselect-width" value={this.state.target} options={target} filter={false} onChange={(e) => this.setState({ target: e.value })}
                     placeholder="Choose" />
                  </div>
                </div>
                <div className="col-lg-4 col-md-12 col-sm-12 col-xs-12" >
                  <label><strong>Notification Method</strong></label>
                  <div className="forumClass">
                    <MultiSelect className="multiselect-width" value={this.state.notificationMethod} options={notification} onChange={(e) => this.setState({ notificationMethod: e.value })}
                     placeholder="Choose" />
                  </div>
                </div>
                <div className="col-lg-4 col-md-12 col-sm-12 col-xs-12" >
                  <label><strong>When</strong></label>
                  <div className="forumClass">
                    <MultiSelect className="multiselect-width" value={this.state.frequencies} options={frequencies} onChange={(e) => this.setState({ frequencies: e.value })}
                    placeholder="Choose" />
                  </div>
                </div>
              </div>
              <div className="col-lg-10 col-sm-12 col-xs-12 mt-2 rating_slider">
                <label><strong>Rating</strong></label><br />
                <div className="range-slider">
                  <Slider max={4} min={0} tooltipVisible={false} marks={this.state.rating} defaultValue={this.state.currentRating - 1} onChange={this.handleSliderChange} />
                </div>
              </div>

              {
                !this.props.editButtons && 
                <div className="col-lg-10 col-sm-12 col-xs-12 mt-2 rating_slider">
                <label><strong>Rating</strong></label><br />
                <div className="range-slider">
                  <Slider max={4} min={0} tooltipVisible={false} marks={this.state.rating} defaultValue={this.state.defaultValue} onChange={this.handleSliderChange} />
                </div>
              </div>
              }

              <div>
              <button type="button"
                className="btn btn-lg btn-primary mr-1 px-4 mt-2 mb-2 login-button forum-button" onClick = {this.dealAlertEdit}> Save Deal Alert</button>
              <button type="button"
                  className="btn btn-primary btn-lg login-button mr-1 px-4 mt-2 mb-2 forum-button"  onClick = {this.callDisplayComponent}>Cancel</button>
              </div> 
            </div>
            }
              
              
                {
                  !this.props.editButtons && 
                <div>
                    <button type="button " className="btn btn-lg btn-primary mr-1 px-4 forum-button butoon-size mt-2" onClick = {this.dealAlertSave}> Add Alert</button>
                    <button type="button" className="btn btn-primary btn-lg px-4  forum-button butoon-size mt-2" onClick = {this.clearAll}>Clear All</button>
                </div>  
                }
     
      </div>
    )
  }
}



export default CategorySettingComponent;


