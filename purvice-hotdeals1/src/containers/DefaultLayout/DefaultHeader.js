import React, { Component } from 'react';
import { Badge, Nav, NavItem, NavLink,Popover, PopoverHeader, PopoverBody,DropdownToggle,DropdownItem,DropdownMenu } from 'reactstrap';
import PropTypes from 'prop-types';
import { HashRouter, Route, Switch,Link } from 'react-router-dom';
import cookie from 'react-cookies';
import { AppNavbarBrand, AppSidebarToggler,AppHeaderDropdown } from '@coreui/react';
import logo from '../../assets/img/brand/logo_purvice.png'
import { UncontrolledTooltip } from 'reactstrap';
//import sygnet from '../../assets/img/brand/sygnet.svg'
import { dealAlertService } from '../../services/deal-alert.service';
import './DefaultLayout.css'
import $ from "jquery";
var defaultHeaderRef;
const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      popoverOpen: false,
      loggedInUser:false,
      userName:'',
      firstInitial:'',
      searchValue :'',
      addBeautyTitle :'Add Deal Alert',
      addTravelTitle :'Add Deal Alert',
      addHomeTitle :'Add Deal Alert',
      addElectronicsTitle :'Add Deal Alert',
      addBooksTitle :'Add Deal Alert',
      addClothingTitle :'Add Deal Alert',
      isAddDealalert: false,
      toggleCSSHot : '',
      toggleCSSDeal : '',
      toggleCSSTravel : ''
    };
    this.handleSearch = this.handleSearch.bind(this)
    this.onMouseOverCSSHotDeal = this.onMouseOverCSSHotDeal.bind(this)
    this.onMouseOverCSSDealAlert = this.onMouseOverCSSDealAlert.bind(this)
    this.onMouseOverCSSTravel = this.onMouseOverCSSTravel.bind(this)
    this.onMouseOutCSS = this.onMouseOutCSS.bind(this)
    this.login = this.login.bind(this)
    this.signUp = this.signUp.bind(this)
  }
  
  componentDidMount(){
    // var user = cookie.load("_id")
	  defaultHeaderRef = this;
    var name = cookie.load("username")
    if(name != undefined){
      this.setState({loggedInUser : true})
      this.setState({userName : name})
      this.state.firstInitial = name.split(" ")[0].charAt(0)+name.split(" ")[1].charAt(0);
      this.setState({firstInitial: this.state.firstInitial});
    }
    
      $(".global-search-input-box").keypress(function(e){
    	 if (e.keyCode == 13) {
    		 if(window.location.hash !== "#/hotdeals"){
    			 window.location = '#/hotdeals';
        	 }
    	 }
	  });
	  
	  $(".global-search-button").click(function(e){
		  if(window.location.hash !== "#/hotdeals"){
			  window.location = '#/hotdeals';
		  }
	  })
	  
	$("body").mouseup(function(e){
		var subject = $(".popover"); 
		if(e.target.id != subject.attr('id') && !subject.has(e.target).length){
			defaultHeaderRef.setState({popoverOpen: false});
		}
	});
  }

  onMouseOverCSSTravel(e){
    this.state.toggleCSSTravel = 'header-menu-hover'
    this.setState({toggleCSSTravel : 'header-menu-hover'})
  }

  onMouseOverCSSHotDeal(e){
    this.state.toggleCSSHot = 'header-menu-hover'
    this.setState({toggleCSSHot : 'header-menu-hover'})
  }

  onMouseOverCSSDealAlert(e){
    this.state.toggleCSSDeal = 'header-menu-hover'
    this.setState({toggleCSSDeal : 'header-menu-hover'})
  }
  
  handleSearch(e){
    this.setState({searchValue :e.target.value})
  }
  logout() {
    cookie.remove('username', { path: '/' })
    cookie.remove('token', { path: '/' })
    cookie.remove('user', { path: '/' })
    cookie.remove('redirectPath', { path: '/' })
    localStorage.setItem("auth_token",null)
    window.location = '#/login';
  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  login = ()=>{
    window.location = '#/login';
  }
  
  signUp = ()=>{
    window.location = '#/register';
  }
  
  addDealAlert = (categoryName)=>{
    var finalAlertData = [];
    var dealAlertdata = {};
    var userData = cookie.load("user")
    if(userData != undefined){
        dealAlertService.getDealCategoriesByName(categoryName).then(data => {
          if(data.message === 'SUCCESS'){
            dealAlertdata = {
              categoryName : data.result[0].categoryName,
              alertTitle :"Any "+data.result[0].categoryName,
              categoryId : data.result[0].id,
              frequencies: "Instantly",
              rating: 3,
              notificationMethod : "Email",
              createdBy : "category",
              keyword: "category",
              userId : userData.id,
              userName : this.state.userName,
              name: ""
            }
            finalAlertData.push(dealAlertdata);
            dealAlertService.dealAlertSave(finalAlertData).then(data => {
              if(data.message === 'SUCCESS'){
              if(categoryName=='Beauty')
               this.setState({addBeautyTitle: 'Alert Added'});
              if(categoryName=='Travel')
                this.setState({addTravelTitle: 'Alert Added'});
              if(categoryName=='Home')
                this.setState({addHomeTitle: 'Alert Added'});
              if(categoryName=='Electronics')
               this.setState({addElectronicsTitle: 'Alert Added'});
              if(categoryName=='Books')
                this.setState({addBooksTitle: 'Alert Added'});
              if(categoryName=='Clothing')
                this.setState({addClothingTitle: 'Alert Added'});
            }
               this.setState({isAddDealalert: true});
            });
        }
      });
    }else{
      window.location = '#/login';
    }
  }

  dealAlert = ()=>{
    window.location = "#/deal-alert";
    this.setState({popoverOpen: false});
  }
  // redirectToHotDeals = () =>{
  //   window.location = '#/hotdeals';
  // }

  onMouseOutCSS(e){
    this.state.toggleCSSHot = this.state.toggleCSSDeal= this.state.toggleCSSTravel =  ''
    this.setState({toggleCSSHot : ''})
    this.setState({toggleCSSDeal : ''})
    this.setState({toggleCSSTravel : ''})
  }
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (

       <div>

        {/* <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand onClick = {this.redirectToHotDeals.bind(this)}
          full={{ src: logo, width: 120, height: 30, alt: 'Purvice Logo' }}
        /> */}

        {/* <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink href="/" className = {'header-nav-css-hot-deal '+this.state.toggleCSSHot} onMouseOver={this.onMouseOverCSSHotDeal.bind(this)} onMouseLeave = {this.onMouseOutCSS.bind(this)}>Hot Deals</NavLink>
          </NavItem>
          <NavItem className="px-3">
          <NavLink id="Popover1" onClick = {this.dealAlert} className = {'header-nav-css-deal-alert '+this.state.toggleCSSDeal} onMouseOver={this.onMouseOverCSSDealAlert.bind(this)} onMouseLeave = {this.onMouseOutCSS.bind(this)}>Deal Alerts</NavLink>
        
          </NavItem>
          <NavItem className="px-3">
            <NavLink  href="https://travel.purvice.com/" className = {'header-nav-css-travel '+this.state.toggleCSSTravel} onMouseOver={this.onMouseOverCSSTravel.bind(this)} onMouseLeave = {this.onMouseOutCSS.bind(this)}>Travel</NavLink>
          </NavItem>
        </Nav>          
        <Nav className="ml-auto" navbar>

          <AppHeaderDropdown  className="d-md-down-none" direction="down">
            <span className = "login-btn" hidden={this.state.loggedInUser} onClick={ e => window.location = '#/register'}><i style={{"color" : "#ffc107"}} className="fa fa-lg fa-user"></i> <strong className="on-hover">Signup Up Free </strong></span>&nbsp;&nbsp;             
            <span className = "login-btn"  hidden={this.state.loggedInUser} onClick={ e => window.location = '#/login'}><i style={{"color" : "#ffc107"}} className="fa fa-lg fa-sign-in"></i> <strong className="on-hover">Login</strong></span>
            </AppHeaderDropdown>
          <AppHeaderDropdown direction="down">
          
            <DropdownToggle nav className="mr-4" >
            <span hidden={!this.state.loggedInUser}>
            <span className="dot-circle " id="Tooltip"><h6 style={{'margin-top': '5px','margin-right': '1px', 'color': 'white'}}>{this.state.firstInitial}</h6></span>
            &nbsp;<i className="fa fa-caret-down" aria-hidden="true"></i>

            <UncontrolledTooltip placement="bottom" target="Tooltip">
              <a style={{'float':'left'}}>Hi, </a><br/><p>{this.state.userName }</p>
            </UncontrolledTooltip>
      </span>
      <span hidden={this.state.loggedInUser} className="d-lg-none" display="md" mobile>
      <i className="fa fa-lg fa-user-circle-o img-avatar" aria-hidden="true"></i>
            &nbsp;<i className="fa fa-caret-down" aria-hidden="true"></i>

      </span>
            </DropdownToggle>
            <div className="drop-align">
              <DropdownMenu>
                  <DropdownItem hidden={!this.state.loggedInUser} onClick={ e => window.location = '#/myprofile'}><i style= {{"color":"#20b8e5"}} className="fa fa-lg fa-user" ></i><b> My Profile </b></DropdownItem>
                  
                  <DropdownItem hidden={!this.state.loggedInUser} ><i style={{"color" : "#20b8e5"}} className="fa fa-lg fa-bell-o"></i> 
                      <b>My Deals Alert  </b>
                  </DropdownItem>
                  <DropdownItem hidden={!this.state.loggedInUser} onClick={this.logout.bind(this)}><i style={{"color" : "#20b8e5"}} className="fa fa-lg fa-sign-out"></i> 
                      <b>Logout </b>
                  </DropdownItem>
                  <DropdownItem hidden={this.state.loggedInUser} onClick={this.login.bind(this)}><i style={{"color" : "#20b8e5"}} className="fa fa-lg fa-sign-in"></i> 
                      <b>Login </b>
                  </DropdownItem>
                  <DropdownItem hidden={this.state.loggedInUser} onClick={this.signUp.bind(this)}><i style={{"color" : "#20b8e5"}} className="fa fa-user-circle-o"></i> 
                      <b>Sign Up </b>
                  </DropdownItem>
              </DropdownMenu>
            </div>
          </AppHeaderDropdown>

       

        </Nav>*/}
        
      </div> 
    );
  }

goToLoginPage(){
    window.location= "#/login";
}
goToSignUp(){
  window.location= "#/register";
}
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
