import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import cookie from 'react-cookies';
import $ from "jquery";
import ENV from '../../environments/environment.js';
import {
  AppAside,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav
} from '@coreui/react';
// sidebar nav config
import navigation from '../../_nav';
// routes config
import routes from '../../routes';
import './DefaultLayout.css'
import { hotDealService } from '../../services/hot-deal.service';

const DefaultAside = React.lazy(() => import('./DefaultAside'));
const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

class DefaultLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedInUser:false,
      tempDealData: [],
      dealData: [],
      shouldHide: false,
      firstInitial:'',
      selectOnSuggession: '',
      pageNumber: 0,
      pageSize: 10,
      searchTextByFilter: '',
      totalSuggesionrecord: 0
    };
    this.onSuggession = this.onSuggession.bind(this);
    this.loadOnDealData = this.loadOnDealData.bind(this);
    this.onSuggessionLoadData = this.onSuggessionLoadData.bind(this);
    //this.loadOnDealData();
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  componentDidMount(){
    var name = cookie.load("username")
    if(name != undefined){
      this.setState({loggedInUser : true})
      this.setState({userName : name})
      this.state.firstInitial = name.split(" ")[0].charAt(0)+name.split(" ")[1].charAt(0);
      this.setState({firstInitial: this.state.firstInitial});
    }
    var that = this;
    $(".global-search-input-box").keypress(function(e){
        that.setState({ shouldHide: false });
          	 if (e.keyCode == 13) {
          		 if(window.location.hash !== "#/hotdeals"){
          			 window.location = '#/hotdeals';
              	 }
          	 }
        });
      	  
        $(".global-search-button").click(function(e){
          that.setState({ shouldHide: false });
    		  if(window.location.hash !== "#/hotdeals"){
      			  window.location = '#/hotdeals';
      		  }
        })
  }

  loadOnDealData(param){
    debugger
    hotDealService.getDealBySearchText(param).then(dealsResponse => {
      this.setState({ dealData: dealsResponse.result });
      this.setState({pageNumber: this.state.pageNumber});
      this.setState({totalSuggesionrecord: dealsResponse.tolalRecord});
      this.onSuggessionLoadData(this.state.searchTextByFilter,param.getText);
    })
  }
  onSuggession(event){
    if(this.state.searchTextByFilter !== event.target.value){
      this.setState({pageNumber:0});
    }
    this.state.searchTextByFilter = event.target.value;
    let param = {};
    param.pageNumber = this.state.pageNumber ? this.state.pageNumber + 1 : 1;
    param.pageSize = this.state.pageSize ? this.state.pageSize : ENV.defaultPageSize;
    param.searchText = this.state.searchTextByFilter ? this.state.searchTextByFilter : '';
    param.getText = 'onSuggession';
    this.loadOnDealData(param);
  }
  onSuggessionLoadData(value,text){
    let searchText = value;
    if (searchText != "") {
      let data= this.state.dealData.filter(obj => {
        let key = obj.text.substring(0,40).toLowerCase();
        if (key.includes(searchText.toLowerCase())) {
         

          let parts = obj.text.substring(0,40).split(new RegExp(`(${searchText})`, 'gi'));
          obj.path = <span> { parts.map((part, i) => 
              <span key={i} style={part.toLowerCase() === searchText.toLowerCase() ? { fontWeight: 'bold', 'text-decoration': 'underline' } : {} }>
                  { part }
              </span>)
          } </span>;
          if(text === "handleScroll"){
            this.state.tempDealData.push(obj);
          }
          return obj;
        }
      })
      if(text === "onSuggession"){
        this.setState({tempDealData:[]});
        this.state.tempDealData = data;
      }

      if(this.state.tempDealData.length > 0){
        this.setState(this.state);
        this.setState({ shouldHide: true });
      }else{
        this.setState(this.state);
        this.setState({ shouldHide: false });
      }
    }else{
      this.state.tempDealData = [];
      this.setState(this.state);
      this.setState({ shouldHide: false });
    }

  }

  signOut(e) {
    e.preventDefault()
    this.props.history.push('/login');
  }

  hotDeal = ()=>{
    this.props.history.push('/hotdeals');
  }

  dealAlert = ()=>{
    this.props.history.push('/deal-alert');
  }
  
  travel = ()=>{
    //this.props.history.push('/hotdeals')
  }
  
  login = ()=>{
    window.location = '#/login';
  }
  
  signUp = ()=>{
    window.location = '#/register';
  }
  
  redirectToHotDeals = () =>{
    if(window.location.hash.split("?")[0] !== "#/hotdeals"){
      window.location = '#/hotdeals';
     }else{
      this.props.history.push('');
      window.location.reload();
     }
  }

  dealAlert = ()=>{
    if(window.location.hash.split("?")[0] !== "#/deal-alert"){
      window.location = '#/deal-alert';
     }else{
      window.location.reload();
     }
  }

  clickOnSugesion = (data)=>{
   $("#global-search-input-box").val(data.currentTarget.innerText.split("\n")[0]);
   this.setState({ shouldHide: false });
   localStorage.setItem("selectOnSuggession", "selectSugessionItem");
   localStorage.setItem("value", $("#global-search-input-box").val());
   if(window.location.hash.split("?")[0] !== "#/hotdeals"){
     window.location = '#/hotdeals';
    }else{
     window.location.reload();
    }
  }
  logout() {
    cookie.remove('username', { path: '/' })
    cookie.remove('token', { path: '/' })
    cookie.remove('user', { path: '/' })
    cookie.remove('redirectPath', { path: '/' })
    localStorage.setItem("auth_token",null)
    window.location = '#/login';
  }

  handleScroll = e => {
    let element = e.target
    if (element.scrollHeight - element.scrollTop === element.clientHeight) {
    //let record = this.state.totalSuggesionrecord 
    //record = record - 10;
     let param = {};
      param.pageNumber = this.state.pageNumber + 1 ;
      this.setState({pageNumber:param.pageNumber});
      param.pageSize = this.state.pageSize ? this.state.pageSize : ENV.defaultPageSize;
      param.searchText = this.state.searchTextByFilter ? this.state.searchTextByFilter : '';
      param.getText = 'handleScroll';
      this.loadOnDealData(param);
    }
  }

  render() {
    return (
      <div style={{backgroundColor:'#fff'}} >
           
    <div id="p-topheader" className="fixed-top">
      <div className="navbar">
        <a className="navbar-brand" onClick = {this.redirectToHotDeals.bind(this)}>
          <img src={require('../../assets/img/logo_purvice.png')} width="160" alt="Logo"/>
        </a>
        <div className="p-topsearchbar d-none d-lg-block">
          <form className="form-inline"> 
             <input autoComplete="off" id="global-search-input-box" className="ng-pristine ng-valid ng-touched form-control p-input global-search-input-box" type="search" placeholder="Search For Deals" aria-label="Search"  onChange={this.onSuggession}  /> 
            <button className="btn btn-warning global-search-button" type="submit"><i className="fas fa-search"></i></button>

        <ul className={this.state.shouldHide ? 'dropDownTextList' : 'dropDownTextLists'} onScroll={this.handleScroll}>
            {this.state.tempDealData.map((item, index) => (
              <li style={{ 'cursor':'pointer' }}>
                <div style={{'display':'inline'}} onClick = {this.clickOnSugesion.bind(this)}>
                  <span className="mt-3 forum-keyword-setting">{item.path}</span><br/>
                  <span className="forum-keyword-setting p-fromlink" style={{ 'color': '#20a8d8' }}>from {item.source}</span>
                </div>
              </li>
            ))}
          </ul>
          </form>
        </div>
        <div className="p-action" hidden={this.state.loggedInUser}>
          <a className="mr-2 p-none-btn" id="search" href="#" data-toggle="modal" data-target="#pSearch">
            <span className="p-icon">
              <i className="fas fa-search"></i>
            </span>
          </a>

          <div className="modal" id="pSearch" tabIndex="-1" role="dialog" aria-labelledby="pSearchLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-header">
                <input autoComplete="off" id="global-search-input-box" className="ng-pristine ng-valid ng-touched form-control p-input global-search-input-box" type="search" placeholder="Search For Deals" aria-label="Search"  onChange={this.onSuggession}/> 
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>
            </div>
            <ul className={this.state.shouldHide ? 'dropDownTextList1' : 'dropDownTextLists1'}>
              {this.state.tempDealData.map((item, index) => (
                <li style={{ 'cursor':'pointer' }}>
                  <div style={{'display':'inline'}}  onClick = {this.clickOnSugesion.bind(this)}>
                    <span className="mt-3 forum-keyword-setting">{item.path}</span><br/>
                    <span className="forum-keyword-setting p-fromlink" style={{ 'color': '#20a8d8' }}>from {item.source}</span>
                  </div>
                </li>
              ))}
            </ul>
          </div>
          <a className="mr-2" onClick={ e => window.location = '#/register'}>
            <span className="p-icon">
              <i className="fas fa-user"></i>
            </span>
            <span className="p-icontext ml-1 d-none d-lg-inline-block">Sign Up Free</span>
          </a>
          <a  onClick={ e => window.location = '#/login'}>
            <span className="p-icon">
              <i className="fas fa-sign-in-alt"></i>
            </span>
            <span className="p-icontext ml-1 d-none d-lg-inline-block">Login</span>
          </a>
        </div>
        <div hidden={!this.state.loggedInUser} >
          <a className="mr-2 p-none-btn" id="searchin" href="#" data-toggle="modal" data-target="#psearch">
            <span className="p-icon">
              <i className="fas fa-search"></i>
            </span>
          </a>

          <div className="modal" id="psearch" tabIndex="-1" role="dialog" aria-labelledby="psearchLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-header">
                <input autoComplete="off" id="global-search-input-box" className="ng-pristine ng-valid ng-touched form-control p-input global-search-input-box" type="search" placeholder="Search For Deals" aria-label="Search"  onChange={this.onSuggession}/> 
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>
            </div>
            <ul className={this.state.shouldHide ? 'dropDownTextList1' : 'dropDownTextLists1'}>
            {this.state.tempDealData.map((item, index) => (
              <li style={{ 'cursor':'pointer' }}>
                <div style={{'display':'inline'}}  onClick = {this.clickOnSugesion.bind(this)}>
                  <span className="mt-3 forum-keyword-setting">{item.path}</span><br/>
                  <span className="forum-keyword-setting p-fromlink" style={{ 'color': '#20a8d8' }}>from {item.source}</span>
                </div>
              </li>
            ))}
          </ul>
          </div>
          <a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{'color': '#fff'}}>
            <span className="dot-circle" id="Tooltip">
              <h5 style={{'margin-top': '0.5em','color': '#fff'}}>{this.state.firstInitial}</h5> 
            </span> 
          </a>
          <div className="dropdown-menu">
            <a className="dropdown-item" onClick={ e => window.location = '#/myprofile'}><i className="fa fa-lg fa-user"></i> My Profile</a>
            <a className="dropdown-item" onClick = {this.dealAlert}><i className="fa fa-lg fa-bell-o"></i> My Deals Alert</a>
            <a className="dropdown-item" onClick={this.logout.bind(this)}><i className="fa fa-lg fa-sign-out"></i> Logout</a>
          </div>
        </div>
      </div>
    </div>
         
          
    <nav className="navbar navbar-expand-lg navbar-light">
      <button className="navbar-toggler pl-0 pr-0" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <a className="nav-item nav-link active pl-0" onClick = {this.redirectToHotDeals.bind(this)}>Hot Deals</a>
          <a className="nav-item nav-link" onClick = {this.dealAlert}>Deal Alerts</a>
          <a className="nav-item nav-link" href="https://travel.purvice.com/">Travel</a>
        </div>
      </div>
    </nav>


        <div>
          <AppSidebar fixed display="d-lg-none d-xl-block">
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
            <AppSidebarNav navConfig={navigation} {...this.props} />
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            {/* <Container> */}
              <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => (
                          <route.component {...props} />
                        )} />
                    ) : (null);
                  })}
                  <Redirect from="/" to="/hotdeals" />
                </Switch>
              </Suspense>
            {/* </Container> */}
          </main>
          <AppAside fixed>
            <Suspense fallback={this.loading()}>
              <DefaultAside />
            </Suspense>
          </AppAside>
        </div>
        
        <Suspense fallback={this.loading()}>
	        <DefaultFooter />
	      </Suspense>
      </div>
    );
  }
}

export default DefaultLayout;
