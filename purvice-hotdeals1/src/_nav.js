export default {
  items: [
    {
      name: 'Hot Deal',
      url: '/hotdeals',
      icon: 'icon-home',
    },
    {
      name: 'Deal Alert',
      url: '/deal-alert',
      icon: 'icon-pencil',
    },
    {
      name: 'Travel',
      url: '/travel',
      icon: 'icon-globe',
      attributes: { target: '_blank', rel: "noopener" },
    }
  ],
};
